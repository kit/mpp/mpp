#!/bin/bash

WORKSPACE=$(ws_find mpp-build-${CI_JOB_NAME})

rm -rf "$CI_PROJECT_DIR/mpp-data"
git clone \
    -c user.name="${GITLAB_USER_NAME}" \
    -c user.email="${GITLAB_USER_EMAIL}" \
    https://gitlab-ci-token:${MPP_DATA_ACCESS}@$CI_SERVER_HOST/kit/mpp/mpp-data.git "$CI_PROJECT_DIR/mpp-data" \
    --single-branch \
    -b benchmarks || exit 1

cp -r "$WORKSPACE/build/data/bench/"* "$CI_PROJECT_DIR/mpp-data/"

cd "$CI_PROJECT_DIR/mpp-data"
git pull
git add -f --all
git commit -m "Updated data for $CI_JOB_NAME at $(date)"
git push
cd -

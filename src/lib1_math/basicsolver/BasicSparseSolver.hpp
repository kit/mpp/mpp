#ifndef BASICSPARSESOLVER_HPP
#define BASICSPARSESOLVER_HPP

#include "GlobalDefinitions.hpp"
#include "SparseRMatrix.hpp"

class SparseSolver {
public:
  virtual void Solve(double *, int nrhs = 1) {};

  virtual void Solve(double *, const double *) {};

  virtual ~SparseSolver() {}

  virtual void test_output(){};
};

SparseSolver *GetSparseSolver(SparseRMatrix &, const std::string &name = "SuperLU");

#endif // BASICSPARSESOLVER_HPP

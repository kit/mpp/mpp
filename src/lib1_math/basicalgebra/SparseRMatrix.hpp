#ifndef SPARSERMATRIX_HPP
#define SPARSERMATRIX_HPP

#include <algorithm>
#include <cstddef>
#include <iterator>
#include <vector>

#include "GlobalDefinitions.hpp"
#include "RMatrix.hpp"


#ifdef BUILD_IA

#include "IAInterval.hpp"

#endif

template<typename REAL = double>
class SparseRMatrixT {
  /**
   * Uses Compressed sparse row (CSR) format to store the data
   * Note that all diagonal entries are stored (independent of their value) at the
   * the beginning of each row
   */
  /// contains entries (all diagonal entries and non zero off diagonal entries)
  std::vector<REAL> a;
  /// contains indices (wrt a) of the diagonal entries, i.e. a new row starts at each index
  std::vector<int> d;
  /// contains the column index of the corresponding entry (in a)
  std::vector<int> col;
  /// number of rows
  int Nrows;
  /// number of colums
  int Ncols;

  inline int computeNcols() const {
    auto endColIt = std::next(std::cbegin(col), d[Nrows]);
    auto maxElementIt = std::max_element(std::cbegin(col), endColIt);
    if (maxElementIt == endColIt) { return 0; }
    return *maxElementIt + 1;
  }

  template<typename REAL1>
  inline int computeNumberOfEntries(const RMatrixT<REAL1> &A) {
    int numEntries = std::min(A.rows(), A.cols());
    for (int i = 0; i < A.rows(); ++i)
      for (int j = 0; j < A.cols(); ++j)
        if (i != j)
          if (!mpp_ba::isNearZero(A[i][j])) ++numEntries;
    return numEntries;
  }
protected:
  inline void SetComputedNcols() { Ncols = computeNcols(); }
public:
  template<typename REAL1>
  SparseRMatrixT(const SparseRMatrixT<REAL1> &A) :
      a(A.Values()), d(A.Indices()), col(A.Columns()), Nrows(A.rows()), Ncols(A.cols()) {}

  template<typename REAL1>
  SparseRMatrixT(const RMatrixT<REAL1> &A) :
      a(computeNumberOfEntries(A)), d(A.rows() + 1), col(a.size()), Nrows(A.rows()),
      Ncols(A.cols()) {
    int cnt = 0;
    for (int i = 0; i < Nrows; ++i) {
      d[i] = cnt;
      if (i < Ncols) {
        a[cnt] = A[i][i];
        col[cnt] = i;
        ++cnt;
      }
      for (int j = 0; j < Ncols; ++j)
        if (i != j)
          if (!mpp_ba::isNearZero(A[i][j])) {
            a[cnt] = A[i][j];
            col[cnt] = j;
            ++cnt;
          }
    }
    d[Nrows] = cnt;
  }

  // TODO: should be deleted or at least protected
  SparseRMatrixT(int rows, int numEntries, int cols = -1) :
      a(numEntries, REAL{}), d(rows + 1), col(numEntries), Nrows(rows), Ncols(cols) {}

  inline RMatrixT<REAL> ToRMatrix() const {
    RMatrixT<REAL> A(Nrows, Ncols);
    int cnt = 0;
    for (int i = 0; i < Nrows; ++i)
      for (; cnt < d[i + 1]; ++cnt)
        A[i][col[cnt]] = a[cnt];
    return A;
  }

  inline REAL operator()(int i, int j) const {
    for (int cnt = d[i]; cnt < d[i + 1]; ++cnt) {
      if (col[cnt] == j) return a[cnt];
    }
    return REAL{};
  }

  inline void SetZero() {
    const int m = d[Nrows];
    std::fill(std::begin(a), std::next(std::begin(a), m), REAL{});
    std::fill(std::begin(col), std::next(std::begin(col), m), 0);
    std::fill(std::begin(d), std::prev(std::end(d), 1), 0);
  }

  inline void resize(int rows, int numEntries) {
    Nrows = rows;
    Ncols = -1;
    a = std::vector<REAL>(numEntries);
    d = std::vector<int>(rows + 1);
    col = std::vector<int>(numEntries);
    d[rows] = numEntries;
  }

  inline const std::vector<REAL> &Values() const noexcept { return a; }

  inline const std::vector<int> &Indices() const noexcept { return d; }

  inline const std::vector<int> &Columns() const noexcept { return col; }

  inline int rows() const noexcept { return Nrows; }

  inline int cols() const noexcept { return Ncols; }

  inline int NumberOfEntries() const { return d[Nrows]; }

  inline RVectorT<REAL> diag() const {
    const std::size_t size = std::min(Nrows, Ncols);
    RVectorT<REAL> r(size);
    std::transform(std::cbegin(d), std::next(std::cbegin(d), size), std::begin(r),
                   [&](const auto &value) { return a[value]; });
    return r;
  }

  template<typename REAL1>
  inline void diag(const RVectorT<REAL1> &r) {
    if (r.size() != std::min(Ncols, Nrows)) THROW("Size does not fit")
    for (auto rIt = std::cbegin(r), dIt = std::cbegin(d); rIt != std::cend(r); ++rIt, ++dIt) {
      d[*dIt] = *rIt;
    }
  }

  inline void CheckDiagonal() {
    for (int i = 0; i < Nrows; ++i)
      if (mpp_ba::isNearZero(a[d[i]])) a[d[i]] = 1.0;
  }

  inline void plusMatVec(REAL *b, const REAL *u) const {
    REAL val{};
    for (int i = 0; i < Nrows; ++i) {
      val = 0.0;
      for (int k = d[i]; k < d[i + 1]; ++k) {
        val += a[k] * u[col[k]];
      }
      b[i] += val;
    }
  }

  inline void minusMatVec(REAL *b, const REAL *u) const {
    REAL val{};
    for (int i = 0; i < Nrows; ++i) {
      val = 0.0;
      for (int k = d[i]; k < d[i + 1]; ++k) {
        val += a[k] * u[col[k]];
      }
      b[i] -= val;
    }
  }

  void compress();

  inline int find(int i, int j) const {
    for (int e = d[i]; e < d[i + 1]; ++e)
      if (j == col[e]) return e;
    return -1;
  }

  template<typename VECTOR>
  void GaussSeidel(VECTOR &u, const VECTOR &b, bool shift = false) const {
    for (int i = 0; i < Nrows; ++i) {
      double r = b[i], diag = 0;
      for (int k = d[i]; k < d[i + 1]; ++k) {
        int j = col[k];
        if (j < i) r -= a[k] * u[j];
        else if (j == i) diag = a[k];
      }
      if ((shift) && (diag == 0.0)) diag = 1;
      u[i] = r / diag;
    }
  }

  template<typename VECTOR>
  void BackwardGaussSeidel(VECTOR &u, const VECTOR &b, bool shift = false) const {
    for (int i = Nrows - 1; i >= 0; --i) {
      double r = b[i], diag = 0;
      for (int k = d[i + 1] - 1; k >= d[i]; --k) {
        int j = col[k];
        if (j > i) r -= a[k] * u[j];
        else if (j == i) diag = a[k];
      }
      if ((shift) && (diag == 0.0)) diag = 1;
      u[i] = r / diag;
    }
  }

  //================================================================================================
  // Deprecated code: Should be deleted or at least protected
  //================================================================================================
  inline const REAL *nzval() const noexcept { return a.data(); }

  inline REAL *nzval() noexcept { return a.data(); }

  inline REAL nzval(int i) const { return a[i]; }

  inline REAL &nzval(int i) { return a[i]; }

  inline const int *rowind() const noexcept { return d.data(); }

  inline int *rowind() noexcept { return d.data(); }

  inline int rowind(int i) const { return d[i]; }

  inline int &rowind(int i) { return d[i]; }

  inline const int *colptr() const noexcept { return col.data(); }

  inline int *colptr() noexcept { return col.data(); }

  inline int colptr(int i) const { return col[i]; }

  inline int &colptr(int i) { return col[i]; }

  inline int size() const noexcept { return rows(); } // TODO: remove

  inline int Size() const { return NumberOfEntries(); } // TODO: remove

  void changeToDiagonal(int j);
};

template<typename REAL>
std::ostream &operator<<(std::ostream &os, const SparseRMatrixT<REAL> &A) {
  if constexpr (std::is_same_v<REAL, double>) os << beginD;
  for (int i = 0; i < A.rows(); ++i) {
    if (i != 0) {
      os << "\n";
      if constexpr (std::is_same_v<REAL, double>) os << beginD;
    }
    for (int j = 0; j < A.cols(); ++j)
      os << A(i, j) << " ";
  }
  if constexpr (std::is_same_v<REAL, double>) os << endD;
  return os;
}

using SparseRMatrix = SparseRMatrixT<double>;

#endif // SPARSERMATRIX_HPP

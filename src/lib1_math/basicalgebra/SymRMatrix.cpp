#include "SymRMatrix.hpp"

#include <cmath>
#include <format>

#include "Parallel.hpp"
#include "SaveLoad.hpp"

//==================================================================================================
// BLAS/LAPACK routines
//==================================================================================================
// factorization of symmetric matrix
extern "C" void dsytrf_(char *UPLO, int *N, void *A, int *LDA, int *IPIV, void *WORK, void *LWORK,
                        int *INFO);
// Invert matrix using factorization
extern "C" void dsytri_(char *UPLO, int *N, void *A, int *LDA, int *IPIV, void *WORK, int *INFO);

//==================================================================================================

template<typename REAL>
void SymRMatrixT<REAL>::Accumulate(int commSplit) {
  PPM->SumOnCommSplit(z, commSplit);
}

template<>
SymRMatrixT<double> &SymRMatrixT<double>::Invert() {
  double A[dim * dim];
  for (int i = 0; i < dim; ++i)
    for (int j = 0; j < dim; ++j)
      A[i * dim + j] = (*this)(i, j);

  int I[dim];
  int info;
  int lwork = 2 * dim;
  double work[2 * lwork];
  char uplo = 'L';
  dsytrf_(&uplo, &dim, A, &dim, I, work, &lwork, &info);
  if (info < 0) {
    THROW(std::format("Error in LAPACK: {}-th argument had an illegal value", -info))
  } else if (info > 0) {
    THROW(std::format("Error in LAPACK: A({},{} is exactly zero.", -info, -info))
  }
  dsytri_(&uplo, &dim, A, &dim, I, work, &info);
  if (info < 0) {
    THROW(std::format("Error in LAPACK: {}-th argument had an illegal value", -info))
  } else if (info > 0) {
    THROW(std::format("Error in LAPACK: A({},{} is exactly zero.", -info, -info))
  }

  for (int i = 0; i < dim; ++i)
    for (int j = i; j < dim; ++j)
      (*this)(i, j) = A[i * dim + j];

  return *this;
}

template<typename REAL>
Saver &SymRMatrixT<REAL>::save(Saver &saver) const {
  saver << Dim();
  for (const auto &element : z) {
    saver << element;
  }
  return saver;
}

template<typename REAL>
Loader &SymRMatrixT<REAL>::load(Loader &loader) {
  int N;
  loader >> N;
  resize(N);
  for (auto &element : z) {
    loader >> element;
  }
  return loader;
}

template class SymRMatrixT<double>;

#ifdef BUILD_IA

template class SymRMatrixT<IAInterval>;

SymRMatrix mid(const IASymRMatrix &IA_A) {
  SymRMatrix A(IA_A.Dim());
  for (int i = 0; i < A.rows(); ++i)
    for (int j = 0; j < A.cols(); ++j)
      A(mid(IA_A(i, j)), i, j);
  return A;
}

SymRMatrix sup(const IASymRMatrix &IA_A) {
  SymRMatrix A(IA_A.Dim());
  for (int i = 0; i < A.rows(); ++i)
    for (int j = 0; j < A.cols(); ++j)
      A(sup(IA_A(i, j)), i, j);
  return A;
}

SymRMatrix inf(const IASymRMatrix &IA_A) {
  SymRMatrix A(IA_A.Dim());
  for (int i = 0; i < A.rows(); ++i)
    for (int j = 0; j < A.cols(); ++j)
      A(inf(IA_A(i, j)), i, j);
  return A;
}

#endif // BUILD_IA

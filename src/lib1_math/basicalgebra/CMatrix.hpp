#ifndef CMATRIX_H
#define CMATRIX_H

#include <algorithm>
#include <cstddef>
#include <functional>
#include <initializer_list>
#include <iterator>
#include <numeric>
#include <vector>

#include "CVector.hpp"
#include "HermCMatrix.hpp"
#include "RMatrix.hpp"

template<typename REAL = double>
class CMatrixT {
  using COMPLEX = COMPLEX_TYPE<REAL>;
protected:
  std::vector<COMPLEX> z; // rowwise entries
  int Nrows;
  int Ncols;
public:
  CMatrixT() : z(0, COMPLEX{}), Nrows(0), Ncols(0) {}

  explicit constexpr CMatrixT(int dim) : z(dim * dim, COMPLEX{}), Nrows(dim), Ncols(dim) {}

  constexpr CMatrixT(int rows, int cols) : z(rows * cols, COMPLEX{}), Nrows(rows), Ncols(cols) {}

  constexpr CMatrixT(const COMPLEX &b, int rows, int cols) :
      z(rows * cols, b), Nrows(rows), Ncols(cols) {}

  template<typename COMPLEX1>
  CMatrixT(const std::initializer_list<std::initializer_list<COMPLEX1>> &v) :
      z(v.begin()->size() * v.size()), Nrows(v.size()), Ncols(v.begin()->size()) {
    auto colIt = std::begin(z);
    for (const auto &row : v) {
      colIt = std::copy(std::cbegin(row), std::cend(row), colIt);
    }
  }

  template<typename COMPLEX1>
  CMatrixT(const std::vector<std::vector<COMPLEX1>> &v) :
      z(v.begin()->size() * v.size()), Nrows(v.size()), Ncols(v.begin()->size()) {
    auto colIt = std::begin(z);
    for (const auto &row : v) {
      colIt = std::copy(std::cbegin(row), std::cend(row), colIt);
    }
  }

  template<typename REAL1>
  explicit CMatrixT(const CMatrixT<REAL1> &A) :
      z(A.begin(), A.end()), Nrows(A.rows()), Ncols(A.cols()) {}

  template<typename REAL1>
  explicit CMatrixT(const RMatrixT<REAL1> &A) :
      z(A.begin(), A.end()), Nrows(A.rows()), Ncols(A.cols()) {}

  template<typename REAL1>
  explicit CMatrixT(const HermCMatrixT<REAL1> &A) :
      z(A.Dim() * A.Dim(), COMPLEX{}), Nrows(A.Dim()), Ncols(A.Dim()) {
    for (int i = 0; i < A.Dim(); ++i) {
      z[i * Ncols + i] = A(i, i);
      for (int j = 0; j < i; ++j) {
        z[i * Ncols + j] = A(i, j);
        z[j * Ncols + i] = A(j, i);
      }
    }
  }

  template<typename REAL1>
  explicit CMatrixT(const SymRMatrixT<REAL1> &A) :
      z(A.Dim() * A.Dim(), COMPLEX{}), Nrows(A.Dim()), Ncols(A.Dim()) {
    for (int i = 0; i < A.Dim(); ++i) {
      z[i * Ncols + i] = A(i, i);
      for (int j = 0; j < i; ++j) {
        z[i * Ncols + j] = A(i, j);
        z[j * Ncols + i] = A(j, i);
      }
    }
  }

  template<typename REAL1>
  explicit CMatrixT(const AntisymRMatrixT<REAL1> &A) :
      z(A.Dim() * A.Dim(), COMPLEX{}), Nrows(A.Dim()), Ncols(A.Dim()) {
    for (int i = 0; i < A.Dim(); ++i)
      for (int j = 0; j < i; ++j) {
        z[i * Ncols + j] = A(i, j);
        z[j * Ncols + i] = A(j, i);
      }
  }

  template<typename MATRIX_A, typename MATRIX_B>
  CMatrixT(const MATRIX_A &A, const MATRIX_B &B) :
      z(A.rows() * B.cols(), COMPLEX{}), Nrows(A.rows()), Ncols(B.cols()) {
    if (A.cols() != B.rows()) THROW("Size in matrix multiplication does not fit!")
    for (int i = 0; i < Nrows; ++i)
      for (int j = 0; j < Ncols; ++j)
        for (int k = 0; k < A.cols(); ++k)
          z[i * Ncols + j] += A(i, k) * B(k, j);
  }

  template<typename REAL1>
  explicit CMatrixT(const CVectorT<REAL1> &d) :
      z(d.size() * d.size(), COMPLEX{}), Nrows(d.size()), Ncols(d.size()) {
    auto diagIt = std::begin(z);
    for (const auto &element : d) {
      *diagIt = element;
      std::advance(diagIt, Ncols + 1);
    }
  }

  template<typename REAL1>
  explicit CMatrixT(const RVectorT<REAL1> &d) :
      z(d.size() * d.size(), COMPLEX{}), Nrows(d.size()), Ncols(d.size()) {
    auto diagIt = std::begin(z);
    for (const auto &element : d) {
      *diagIt = element;
      std::advance(diagIt, Ncols + 1);
    }
  }

  inline CMatrixT &operator=(const COMPLEX &b) {
    std::fill(std::begin(z), std::end(z), b);
    return *this;
  }

  template<typename REAL1>
  inline CMatrixT &operator=(const CMatrixT<REAL1> &A) {
    Nrows = A.rows();
    Ncols = A.cols();
    z.assign(std::cbegin(A), std::cend(A));
    return *this;
  }

  template<typename REAL1>
  inline CMatrixT &operator=(const RMatrixT<REAL1> &A) {
    Ncols = A.cols();
    Nrows = A.rows();
    z = std::vector<COMPLEX>(std::cbegin(A), std::cend(A));
    return *this;
  }

  template<typename REAL1>
  inline CMatrixT &operator=(const HermCMatrixT<REAL1> &A) {
    resize(A.Dim());
    for (int i = 0; i < A.Dim(); ++i) {
      z[i * Ncols + i] = A(i, i);
      for (int j = 0; j < i; ++j) {
        z[i * Ncols + j] = A(i, j);
        z[j * Ncols + i] = A(j, i);
      }
    }
    return *this;
  }

  template<typename REAL1>
  inline CMatrixT &operator=(const SymRMatrixT<REAL1> &A) {
    resize(A.Dim());
    for (int i = 0; i < A.Dim(); ++i) {
      z[i * Ncols + i] = A(i, i);
      for (int j = 0; j < i; ++j) {
        z[i * Ncols + j] = A(i, j);
        z[j * Ncols + i] = A(j, i);
      }
    }
    return *this;
  }

  template<typename REAL1>
  inline CMatrixT &operator=(const AntisymRMatrixT<REAL1> &A) {
    resize(A.Dim());
    for (int i = 0; i < A.Dim(); ++i)
      for (int j = 0; j < i; ++j) {
        z[i * Ncols + j] = A(i, j);
        z[j * Ncols + i] = A(j, i);
      }
    return *this;
  }

  template<typename REAL1>
  inline CMatrixT &operator+=(const CMatrixT<REAL1> &b) {
    if (Nrows != b.rows() || Ncols != b.cols()) THROW("Size does not fit!")
    std::transform(std::cbegin(z), std::cend(z), std::cbegin(b), std::begin(z), std::plus<>());
    return *this;
  }

  template<typename REAL1>
  inline CMatrixT &operator+=(const HermCMatrixT<REAL1> &b) {
    if (Nrows != b.Dim() || Ncols != b.Dim()) THROW("Size does not fit!")
    for (int i = 0; i < Nrows; ++i)
      for (int j = 0; j < Ncols; ++j)
        z[i * Ncols + j] += b(i, j);
    return *this;
  }

  template<typename REAL1>
  inline CMatrixT &operator+=(const RMatrixT<REAL1> &b) {
    if (Nrows != b.rows() || Ncols != b.cols()) THROW("Size does not fit!")
    std::transform(std::cbegin(z), std::cend(z), std::cbegin(b), std::begin(z), std::plus<>());
    return *this;
  }

  template<typename REAL1>
  inline CMatrixT &operator+=(const SymRMatrixT<REAL1> &b) {
    if (Nrows != b.Dim() || Ncols != b.Dim()) THROW("Size does not fit!")
    for (int i = 0; i < Nrows; ++i)
      for (int j = 0; j < Ncols; ++j)
        z[i * Ncols + j] += b(i, j);
    return *this;
  }

  template<typename REAL1>
  inline CMatrixT &operator+=(const AntisymRMatrixT<REAL1> &b) {
    if (Nrows != b.Dim() || Ncols != b.Dim()) THROW("Size does not fit!")
    for (int i = 0; i < Nrows; ++i)
      for (int j = 0; j < Ncols; ++j)
        z[i * Ncols + j] += b(i, j);
    return *this;
  }

  template<typename REAL1>
  inline CMatrixT &operator-=(const CMatrixT<REAL1> &b) {
    if (Nrows != b.rows() || Ncols != b.cols()) THROW("Size does not fit!")
    std::transform(std::cbegin(z), std::cend(z), std::cbegin(b), std::begin(z), std::minus<>());
    return *this;
  }

  template<typename REAL1>
  inline CMatrixT &operator-=(const HermCMatrixT<REAL1> &b) {
    if (Nrows != b.Dim() || Ncols != b.Dim()) THROW("Size does not fit!")
    for (int i = 0; i < Nrows; ++i)
      for (int j = 0; j < Ncols; ++j)
        z[i * Ncols + j] -= b(i, j);
    return *this;
  }

  template<typename REAL1>
  inline CMatrixT &operator-=(const RMatrixT<REAL1> &b) {
    if (Nrows != b.rows() || Ncols != b.cols()) THROW("Size does not fit!")
    std::transform(std::cbegin(z), std::cend(z), std::cbegin(b), std::begin(z), std::minus<>());
    return *this;
  }

  template<typename REAL1>
  inline CMatrixT &operator-=(const SymRMatrixT<REAL1> &b) {
    if (Nrows != b.Dim() || Ncols != b.Dim()) THROW("Size does not fit!")
    for (int i = 0; i < Nrows; ++i)
      for (int j = 0; j < Ncols; ++j)
        z[i * Ncols + j] -= b(i, j);
    return *this;
  }

  template<typename REAL1>
  inline CMatrixT &operator-=(const AntisymRMatrixT<REAL1> &b) {
    if (Nrows != b.Dim() || Ncols != b.Dim()) THROW("Size does not fit!")
    for (int i = 0; i < Nrows; ++i)
      for (int j = 0; j < Ncols; ++j)
        z[i * Ncols + j] -= b(i, j);
    return *this;
  }

  inline CMatrixT &operator*=(const COMPLEX &b) {
    std::transform(std::cbegin(z), std::cend(z), std::begin(z),
                   [&b](const auto &value) { return value * b; });
    return *this;
  }

  // This is a component wise operation - not a matrix multiplication!
  template<typename REAL1>
  inline CMatrixT &operator*=(const CMatrixT<REAL1> &b) {
    if (Nrows != b.rows() || Ncols != b.cols()) THROW("Size does not fit!")
    std::transform(std::cbegin(z), std::cend(z), std::cbegin(b), std::begin(z),
                   std::multiplies<>());
    return *this;
  }

  inline CMatrixT &operator/=(const COMPLEX &b) {
    std::transform(std::cbegin(z), std::cend(z), std::begin(z),
                   [&b](const auto &value) { return value / b; });
    return *this;
  }

  inline auto operator[](int row) { return std::next(std::begin(z), row * Ncols); }

  inline auto operator[](int row) const { return std::next(std::cbegin(z), row * Ncols); }

  inline COMPLEX &operator()(int row, int col) { return z[row * Ncols + col]; }

  inline COMPLEX &operator()(int index) { return z[index]; }

  inline const COMPLEX &operator()(int row, int col) const { return z[row * Ncols + col]; }

  template<typename COMPLEX1>
  inline void operator()(const COMPLEX1 &b, int row, int col) {
    z[row * Ncols + col] = b;
  }

  inline CVectorT<REAL> row(int i) const {
    CVectorT<REAL> r(Ncols);
    const auto rowBegin = std::next(std::cbegin(z), i * Ncols);
    std::copy(rowBegin, std::next(rowBegin, Ncols), std::begin(r));
    return r;
  }

  inline CVectorT<REAL> col(int j) const {
    CVectorT<REAL> c(Nrows);
    auto colIt = std::next(std::cbegin(z), j);
    for (auto &element : c) {
      element = *colIt;
      std::advance(colIt, Ncols);
    }
    return c;
  }

  inline int size() const noexcept { return z.size(); }

  inline int rows() const noexcept { return Nrows; }

  inline int cols() const noexcept { return Ncols; }

  inline auto begin() const noexcept { return z.begin(); }

  inline auto begin() noexcept { return z.begin(); }

  inline auto end() const noexcept { return z.end(); }

  inline auto end() noexcept { return z.end(); }

  inline const std::vector<COMPLEX> &Data() const noexcept { return z; }

  inline std::vector<COMPLEX> &Data() noexcept { return z; }

  inline void resize(int rows, int cols) {
    z = std::vector<COMPLEX>(rows * cols, COMPLEX{});
    Nrows = rows;
    Ncols = cols;
  }

  inline void resize(int dim) { resize(dim, dim); }

  inline CVectorT<REAL> diag() const {
    const std::size_t size = std::min(Nrows, Ncols);
    CVectorT<REAL> d(size);
    auto diagIt = std::cbegin(z);
    for (auto &element : d) {
      element = *diagIt;
      std::advance(diagIt, Ncols + 1);
    }
    return d;
  }

  template<typename REAL1>
  inline void diag(const CVectorT<REAL1> &d) {
    resize(d.size());
    auto diagIt = std::begin(z);
    for (const auto &element : d) {
      *diagIt = element;
      std::advance(diagIt, Ncols + 1);
    }
  }

  template<typename REAL1>
  inline void diag(const RVectorT<REAL1> &d) {
    resize(d.size());
    auto diagIt = std::begin(z);
    for (const auto &element : d) {
      *diagIt = element;
      std::advance(diagIt, Ncols + 1);
    }
  }

  void Accumulate(int commSpit = 0);

  CMatrixT &conj();

  RMatrixT<REAL> real() const;

  RMatrixT<REAL> imag() const;

  inline CMatrixT &transpose() {
    CMatrixT<REAL> b(*this);
    resize(b.Ncols, b.Nrows);
    for (int i = 0; i < Nrows; ++i)
      for (int j = 0; j < Ncols; ++j)
        z[i * Ncols + j] = b[j][i];
    return *this;
  }

  inline COMPLEX Mean() const {
    COMPLEX sum = std::accumulate(std::cbegin(z), std::cend(z), COMPLEX{});
    return sum / z.size();
  }

  REAL Variance() const;

  template<typename REAL1>
  inline void Insert(const CMatrixT<REAL1> &B, int row, int col) {
    if (row + B.rows() >= Nrows || col + B.cols() >= Ncols) {
      THROW("Cannot insert matrix: size of B too large")
    }
    for (int i = 0; i < B.rows(); ++i)
      for (int j = 0; j < B.cols(); ++j)
        z[(row + i) * Ncols + col + j] = B[i][j];
  }

  template<typename REAL1>
  inline void InsertRow(const CVectorT<REAL1> &v, int row, int startCol = 0) {
    if (row >= Nrows || startCol + v.size() > Ncols) {
      THROW("Cannot insert row: size of v too large")
    }
    auto destIt = std::next(std::begin(z), row * Ncols + startCol);
    std::copy(std::cbegin(v), std::cend(v), destIt);
  }

  template<typename REAL1>
  inline void InsertCol(const CVectorT<REAL1> &v, int col, int startRow = 0) {
    if (col >= Ncols || startRow + v.size() > Nrows) {
      THROW("Cannot insert column: size of v too large")
    }
    auto colIt = std::next(std::begin(z), startRow * Ncols + col);
    for (const auto &element : v) {
      *colIt = element;
      std::advance(colIt, Ncols);
    }
  }

  CMatrixT &adjoint();

  inline CMatrixT &Identity() {
    std::fill(std::begin(z), std::end(z), COMPLEX(0));
    for (std::size_t i = 0; i < std::min(Nrows, Ncols); ++i) {
      z[i * Ncols + i] = COMPLEX(1);
    }
    return *this;
  }

  CMatrixT &Invert();

  Saver &save(Saver &saver) const;

  Loader &load(Loader &loader);
};

template<typename REAL>
inline bool operator==(const CMatrixT<REAL> &A, const CMatrixT<REAL> &B) {
  if (A.rows() != B.rows() || A.cols() != B.cols()) return false;
  return (A.real() == B.real()) && (A.imag() == B.imag());
}

template<typename REAL>
inline bool operator==(const CMatrixT<REAL> &A, const HermCMatrixT<REAL> &B) {
  return A == CMatrixT<REAL>(B);
}

template<typename REAL>
inline bool operator==(const HermCMatrixT<REAL> &A, const CMatrixT<REAL> &B) {
  return CMatrixT<REAL>(A) == B;
}

template<typename REAL>
inline bool operator==(const CMatrixT<REAL> &A, const RMatrixT<REAL> &B) {
  return A == CMatrixT<REAL>(B);
}

template<typename REAL>
inline bool operator==(const RMatrixT<REAL> &A, const CMatrixT<REAL> &B) {
  return CMatrixT<REAL>(A) == B;
}

template<typename REAL>
inline bool operator==(const CMatrixT<REAL> &A, const SymRMatrixT<REAL> &B) {
  return A == CMatrixT<REAL>(B);
}

template<typename REAL>
inline bool operator==(const SymRMatrixT<REAL> &A, const CMatrixT<REAL> &B) {
  return CMatrixT<REAL>(A) == B;
}

template<typename REAL>
inline bool operator==(const CMatrixT<REAL> &A, const AntisymRMatrixT<REAL> &B) {
  return A == CMatrixT<REAL>(B);
}

template<typename REAL>
inline bool operator==(const AntisymRMatrixT<REAL> &A, const CMatrixT<REAL> &B) {
  return CMatrixT<REAL>(A) == B;
}

template<typename REAL>
inline bool operator!=(const CMatrixT<REAL> &A, const CMatrixT<REAL> &B) {
  return !(A == B);
}

template<typename REAL>
inline bool operator!=(const CMatrixT<REAL> &A, const HermCMatrixT<REAL> &B) {
  return !(A == B);
}

template<typename REAL>
inline bool operator!=(const HermCMatrixT<REAL> &A, const CMatrixT<REAL> &B) {
  return !(A == B);
}

template<typename REAL>
inline bool operator!=(const CMatrixT<REAL> &A, const RMatrixT<REAL> &B) {
  return !(A == B);
}

template<typename REAL>
inline bool operator!=(const RMatrixT<REAL> &A, const CMatrixT<REAL> &B) {
  return !(A == B);
}

template<typename REAL>
inline bool operator!=(const CMatrixT<REAL> &A, const SymRMatrixT<REAL> &B) {
  return !(A == B);
}

template<typename REAL>
inline bool operator!=(const SymRMatrixT<REAL> &A, const CMatrixT<REAL> &B) {
  return !(A == B);
}

template<typename REAL>
inline bool operator!=(const CMatrixT<REAL> &A, const AntisymRMatrixT<REAL> &B) {
  return !(A == B);
}

template<typename REAL>
inline bool operator!=(const AntisymRMatrixT<REAL> &A, const CMatrixT<REAL> &B) {
  return !(A == B);
}

template<typename REAL>
inline CMatrixT<REAL> operator-(const CMatrixT<REAL> &a) {
  CMatrixT<REAL> c(a);
  return c *= -1;
}

template<typename REAL>
inline CMatrixT<REAL> operator+(const CMatrixT<REAL> &a, const CMatrixT<REAL> &b) {
  CMatrixT<REAL> c(a);
  return c += b;
}

template<typename REAL>
inline CMatrixT<REAL> operator+(const CMatrixT<REAL> &a, const RMatrixT<REAL> &b) {
  CMatrixT<REAL> c(a);
  return c += b;
}

template<typename REAL>
inline CMatrixT<REAL> operator+(const RMatrixT<REAL> &a, const CMatrixT<REAL> &b) {
  CMatrixT<REAL> c(a);
  return c += b;
}

template<typename REAL>
inline CMatrixT<REAL> operator+(const CMatrixT<REAL> &a, const HermCMatrixT<REAL> &b) {
  CMatrixT<REAL> c(a);
  return c += b;
}

template<typename REAL>
inline CMatrixT<REAL> operator+(const HermCMatrixT<REAL> &a, const CMatrixT<REAL> &b) {
  CMatrixT<REAL> c(a);
  return c += b;
}

template<typename REAL>
inline CMatrixT<REAL> operator+(const CMatrixT<REAL> &a, const SymRMatrixT<REAL> &b) {
  CMatrixT<REAL> c(a);
  return c += b;
}

template<typename REAL>
inline CMatrixT<REAL> operator+(const SymRMatrixT<REAL> &a, const CMatrixT<REAL> &b) {
  CMatrixT<REAL> c(a);
  return c += b;
}

template<typename REAL>
inline CMatrixT<REAL> operator+(const CMatrixT<REAL> &a, const AntisymRMatrixT<REAL> &b) {
  CMatrixT<REAL> c(a);
  return c += b;
}

template<typename REAL>
inline CMatrixT<REAL> operator+(const AntisymRMatrixT<REAL> &a, const CMatrixT<REAL> &b) {
  CMatrixT<REAL> c(a);
  return c += b;
}

template<typename REAL>
inline CMatrixT<REAL> operator-(const CMatrixT<REAL> &a, const CMatrixT<REAL> &b) {
  CMatrixT<REAL> c(a);
  return c -= b;
}

template<typename REAL>
inline CMatrixT<REAL> operator-(const CMatrixT<REAL> &a, const RMatrixT<REAL> &b) {
  CMatrixT<REAL> c(a);
  return c -= b;
}

template<typename REAL>
inline CMatrixT<REAL> operator-(const RMatrixT<REAL> &a, const CMatrixT<REAL> &b) {
  CMatrixT<REAL> c(a);
  return c -= b;
}

template<typename REAL>
inline CMatrixT<REAL> operator-(const CMatrixT<REAL> &a, const HermCMatrixT<REAL> &b) {
  CMatrixT<REAL> c(a);
  return c -= b;
}

template<typename REAL>
inline CMatrixT<REAL> operator-(const HermCMatrixT<REAL> &a, const CMatrixT<REAL> &b) {
  CMatrixT<REAL> c(a);
  return c -= b;
}

template<typename REAL>
inline CMatrixT<REAL> operator-(const CMatrixT<REAL> &a, const SymRMatrixT<REAL> &b) {
  CMatrixT<REAL> c(a);
  return c -= b;
}

template<typename REAL>
inline CMatrixT<REAL> operator-(const SymRMatrixT<REAL> &a, const CMatrixT<REAL> &b) {
  CMatrixT<REAL> c(a);
  return c -= b;
}

template<typename REAL>
inline CMatrixT<REAL> operator-(const CMatrixT<REAL> &a, const AntisymRMatrixT<REAL> &b) {
  CMatrixT<REAL> c(a);
  return c -= b;
}

template<typename REAL>
inline CMatrixT<REAL> operator-(const AntisymRMatrixT<REAL> &a, const CMatrixT<REAL> &b) {
  CMatrixT<REAL> c(a);
  return c -= b;
}

template<typename REAL>
inline CMatrixT<REAL> operator*(const CMatrixT<REAL> &a, const CMatrixT<REAL> &b) {
  return CMatrixT<REAL>(a, b);
}

template<typename REAL>
inline CMatrixT<REAL> operator*(const CMatrixT<REAL> &a, const RMatrixT<REAL> &b) {
  return CMatrixT<REAL>(a, b);
}

template<typename REAL>
inline CMatrixT<REAL> operator*(const RMatrixT<REAL> &a, const CMatrixT<REAL> &b) {
  return CMatrixT<REAL>(a, b);
}

template<typename REAL>
inline CMatrixT<REAL> operator*(const HermCMatrixT<REAL> &a, const HermCMatrixT<REAL> &b) {
  return CMatrixT<REAL>(a, b);
}

template<typename REAL>
inline CMatrixT<REAL> operator*(const CMatrixT<REAL> &a, const HermCMatrixT<REAL> &b) {
  return CMatrixT<REAL>(a, b);
}

template<typename REAL>
inline CMatrixT<REAL> operator*(const HermCMatrixT<REAL> &a, const CMatrixT<REAL> &b) {
  return CMatrixT<REAL>(a, b);
}

template<typename REAL>
inline CMatrixT<REAL> operator*(const CMatrixT<REAL> &a, const SymRMatrixT<REAL> &b) {
  return CMatrixT<REAL>(a, b);
}

template<typename REAL>
inline CMatrixT<REAL> operator*(const SymRMatrixT<REAL> &a, const CMatrixT<REAL> &b) {
  return CMatrixT<REAL>(a, b);
}

template<typename REAL>
inline CMatrixT<REAL> operator*(const CMatrixT<REAL> &a, const AntisymRMatrixT<REAL> &b) {
  return CMatrixT<REAL>(a, b);
}

template<typename REAL>
inline CMatrixT<REAL> operator*(const AntisymRMatrixT<REAL> &a, const CMatrixT<REAL> &b) {
  return CMatrixT<REAL>(a, b);
}

template<typename REAL>
inline CVectorT<REAL> operator*(const CMatrixT<REAL> &a, const CVectorT<REAL> &v) {
  CVectorT<REAL> w(a.rows());
  auto colIt = std::cbegin(a);
  for (auto &element : w) {
    element = std::inner_product(std::cbegin(v), std::cend(v), colIt, COMPLEX_TYPE<REAL>{});
    std::advance(colIt, a.cols());
  }
  return w;
}

template<typename REAL>
inline CVectorT<REAL> operator*(const CMatrixT<REAL> &a, const RVectorT<REAL> &v) {
  CVectorT<REAL> w(a.rows());
  auto colIt = std::cbegin(a);
  for (auto &element : w) {
    element = std::inner_product(std::cbegin(v), std::cend(v), colIt, COMPLEX_TYPE<REAL>{});
    std::advance(colIt, a.cols());
  }
  return w;
}

template<typename REAL, typename T>
inline CMatrixT<REAL> operator*(const CMatrixT<REAL> &a, const T &b) {
  CMatrixT<REAL> c(a);
  return c *= b;
}

template<typename REAL, typename T>
inline CMatrixT<REAL> operator*(const T &b, const CMatrixT<REAL> &a) {
  CMatrixT<REAL> c(a);
  return c *= b;
}

template<typename REAL, typename T>
inline CMatrixT<REAL> operator/(const CMatrixT<REAL> &a, const T &b) {
  CMatrixT<REAL> c(a);
  return c /= b;
}

template<typename REAL>
inline CMatrixT<REAL> conj(const CMatrixT<REAL> &a) {
  CMatrixT<REAL> b(a);
  return b.conj();
}

template<typename REAL>
inline RMatrixT<REAL> real(const CMatrixT<REAL> &A) {
  return A.real();
}

template<typename REAL>
inline RMatrixT<REAL> imag(const CMatrixT<REAL> &v) {
  return v.imag();
}

template<typename REAL>
inline CMatrixT<REAL> transpose(const CMatrixT<REAL> &a) {
  CMatrixT<REAL> b(a);
  return b.transpose();
}

template<typename REAL>
inline CMatrixT<REAL> invert(const CMatrixT<REAL> &a) {
  CMatrixT<REAL> b(a);
  return b.Invert();
}

template<typename REAL>
inline CMatrixT<REAL> adjoint(const CMatrixT<REAL> &a) {
  CMatrixT<REAL> b(a);
  return b.adjoint();
}

template<typename REAL>
inline Saver &operator<<(Saver &saver, const CMatrixT<REAL> &A) {
  return A.save(saver);
}

template<typename REAL>
inline Loader &operator>>(Loader &loader, CMatrixT<REAL> &A) {
  return A.load(loader);
}

template<typename REAL>
inline std::ostream &operator<<(std::ostream &os, const CMatrixT<REAL> &v) {
  for (int i = 0; i < v.rows(); ++i) {
    if (i != 0) os << "\n";
    for (int j = 0; j < v.cols(); ++j)
      os << v(i, j) << " ";
  }
  return os;
}

using CMatrix = CMatrixT<double>;

#ifdef BUILD_IA

using IACMatrix = CMatrixT<IAInterval>;

CMatrix mid(const IACMatrix &);

inline IACMatrix operator+(const IACMatrix &A, const CMatrix &B) {
  IACMatrix C(A);
  return C += B;
}

inline IACMatrix operator+(const CMatrix &A, const IACMatrix &B) {
  IACMatrix C(A);
  return C += B;
}

inline IACMatrix operator+(const IACMatrix &A, const HermCMatrix &B) {
  IACMatrix C(A);
  return C += B;
}

inline IACMatrix operator+(const HermCMatrix &A, const IACMatrix &B) {
  IACMatrix C(A);
  return C += B;
}

inline IACMatrix operator+(const IACMatrix &A, const RMatrix &B) {
  IACMatrix C(A);
  return C += B;
}

inline IACMatrix operator+(const RMatrix &A, const IACMatrix &B) {
  IACMatrix C(A);
  return C += B;
}

inline IACMatrix operator+(const IACMatrix &A, const SymRMatrix &B) {
  IACMatrix C(A);
  return C += B;
}

inline IACMatrix operator+(const SymRMatrix &A, const IACMatrix &B) {
  IACMatrix C(A);
  return C += B;
}

inline IACMatrix operator+(const IACMatrix &A, const AntisymRMatrix &B) {
  IACMatrix C(A);
  return C += B;
}

inline IACMatrix operator+(const AntisymRMatrix &A, const IACMatrix &B) {
  IACMatrix C(A);
  return C += B;
}

inline IACMatrix operator-(const IACMatrix &A, const CMatrix &B) {
  IACMatrix C(A);
  return C -= B;
}

inline IACMatrix operator-(const CMatrix &A, const IACMatrix &B) {
  IACMatrix C(A);
  return C -= B;
}

inline IACMatrix operator-(const IACMatrix &A, const HermCMatrix &B) {
  IACMatrix C(A);
  return C -= B;
}

inline IACMatrix operator-(const HermCMatrix &A, const IACMatrix &B) {
  IACMatrix C(A);
  return C -= B;
}

inline IACMatrix operator-(const IACMatrix &A, const RMatrix &B) {
  IACMatrix C(A);
  return C -= B;
}

inline IACMatrix operator-(const RMatrix &A, const IACMatrix &B) {
  IACMatrix C(A);
  return C -= B;
}

inline IACMatrix operator-(const IACMatrix &A, const SymRMatrix &B) {
  IACMatrix C(A);
  return C -= B;
}

inline IACMatrix operator-(const SymRMatrix &A, const IACMatrix &B) {
  IACMatrix C(A);
  return C -= B;
}

inline IACMatrix operator-(const IACMatrix &A, const AntisymRMatrix &B) {
  IACMatrix C(A);
  return C -= B;
}

inline IACMatrix operator-(const AntisymRMatrix &A, const IACMatrix &B) {
  IACMatrix C(A);
  return C -= B;
}

inline IACMatrix operator*(const IACMatrix &A, const CMatrix &B) { return IACMatrix(A, B); }

inline IACMatrix operator*(const CMatrix &A, const IACMatrix &B) { return IACMatrix(A, B); }

inline IACMatrix operator*(const IACMatrix &A, const RMatrix &B) { return IACMatrix(A, B); }

inline IACMatrix operator*(const RMatrix &A, const IACMatrix &B) { return IACMatrix(A, B); }

inline IACMatrix operator*(const IACMatrix &A, const HermCMatrix &B) { return IACMatrix(A, B); }

inline IACMatrix operator*(const HermCMatrix &A, const IACMatrix &B) { return IACMatrix(A, B); }

inline IACMatrix operator*(const IACMatrix &A, const SymRMatrix &B) { return IACMatrix(A, B); }

inline IACMatrix operator*(const SymRMatrix &A, const IACMatrix &B) { return IACMatrix(A, B); }

inline IACMatrix operator*(const IACMatrix &A, const AntisymRMatrix &B) { return IACMatrix(A, B); }

inline IACMatrix operator*(const AntisymRMatrix &A, const IACMatrix &B) { return IACMatrix(A, B); }

inline IACVector operator*(const IACMatrix &a, const CVector &v) {
  IACVector w(a.rows());
  auto colIt = std::cbegin(a);
  for (auto &element : w) {
    element = std::inner_product(std::cbegin(v), std::cend(v), colIt, COMPLEX_TYPE<IAInterval>{});
    std::advance(colIt, a.cols());
  }
  return w;
}

inline IACVector operator*(const CMatrix &a, const IACVector &v) {
  IACVector w(a.rows());
  auto colIt = std::cbegin(a);
  for (auto &element : w) {
    element = std::inner_product(std::cbegin(v), std::cend(v), colIt, COMPLEX_TYPE<IAInterval>{});
    std::advance(colIt, a.cols());
  }
  return w;
}

inline IACVector operator*(const IACMatrix &a, const RVector &v) {
  IACVector w(a.rows());
  auto colIt = std::cbegin(a);
  for (auto &element : w) {
    element = std::inner_product(std::cbegin(v), std::cend(v), colIt, COMPLEX_TYPE<IAInterval>{});
    std::advance(colIt, a.cols());
  }
  return w;
}

inline IACVector operator*(const CMatrix &a, const IARVector &v) {
  IACVector w(a.rows());
  auto colIt = std::cbegin(a);
  for (auto &element : w) {

    element = std::inner_product(std::cbegin(v), std::cend(v), colIt, COMPLEX_TYPE<IAInterval>{},
                                 std::plus<>(), [](const auto &first, const auto &second) {
                                   return first * IACInterval(second);
                                 });
    std::advance(colIt, a.cols());
  }
  return w;
}

#endif // BUILD_IA

#endif

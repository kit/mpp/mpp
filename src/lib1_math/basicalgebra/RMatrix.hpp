#ifndef RMATRIX_H
#define RMATRIX_H

#include <algorithm>
#include <cstddef>
#include <format>
#include <functional>
#include <initializer_list>
#include <iterator>
#include <numeric>
#include <vector>

#include "AntisymRMatrix.hpp"
#include "SymRMatrix.hpp"

template<typename REAL = double>
class RMatrixT {
protected:
  std::vector<REAL> z{}; // rowwise entries
  int Nrows;
  int Ncols;
public:
  RMatrixT() : z(0, REAL{}), Nrows(0), Ncols(0) {}

  explicit constexpr RMatrixT(int dim) : z(dim * dim, REAL{}), Nrows(dim), Ncols(dim) {}

  explicit constexpr RMatrixT(const REAL &b, int dim) : z(dim * dim, b), Nrows(dim), Ncols(dim) {}

  constexpr RMatrixT(int rows, int cols) : z(rows * cols, REAL{}), Nrows(rows), Ncols(cols) {}

  constexpr RMatrixT(const REAL &b, int rows, int cols) :
      z(rows * cols, b), Nrows(rows), Ncols(cols) {}

  template<typename REAL1>
  RMatrixT(const std::initializer_list<std::initializer_list<REAL1>> &v) :
      z(v.begin()->size() * v.size()), Nrows(v.size()), Ncols(v.begin()->size()) {
    auto colIt = std::begin(z);
    for (const auto &row : v) {
      colIt = std::copy(std::cbegin(row), std::cend(row), colIt);
    }
  }

  template<typename REAL1>
  RMatrixT(const std::vector<std::vector<REAL1>> &v) :
      z(v.begin()->size() * v.size()), Nrows(v.size()), Ncols(v.begin()->size()) {
    auto colIt = std::begin(z);
    for (const auto &row : v) {
      colIt = std::copy(std::cbegin(row), std::cend(row), colIt);
    }
  }

  template<typename REAL1>
  explicit RMatrixT(const RMatrixT<REAL1> &A) :
      z(A.begin(), A.end()), Nrows(A.rows()), Ncols(A.cols()) {}

  template<typename REAL1>
  explicit RMatrixT(const SymRMatrixT<REAL1> &A) :
      z(A.Dim() * A.Dim()), Nrows(A.Dim()), Ncols(A.Dim()) {
    for (int i = 0; i < A.Dim(); ++i) {
      z[i * Ncols + i] = A(i, i);
      for (int j = 0; j < i; ++j) {
        z[i * Ncols + j] = A(i, j);
        z[j * Ncols + i] = A(i, j);
      }
    }
  }

  template<typename REAL1>
  explicit RMatrixT(const AntisymRMatrixT<REAL1> &A) :
      z(A.Dim() * A.Dim()), Nrows(A.Dim()), Ncols(A.Dim()) {
    for (int i = 0; i < A.Dim(); ++i)
      for (int j = 0; j < i; ++j) {
        z[i * Ncols + j] = A(i, j);
        z[j * Ncols + i] = A(j, i);
      }
  }

  template<typename MATRIX_A, typename MATRIX_B>
  RMatrixT(const MATRIX_A &A, const MATRIX_B &B) :
      z(A.rows() * B.cols(), REAL{}), Nrows(A.rows()), Ncols(B.cols()) {
    if (A.cols() != B.rows()) THROW("Size in matrix multiplication does not fit!")
    for (int i = 0; i < Nrows; ++i)
      for (int j = 0; j < Ncols; ++j)
        for (int k = 0; k < A.cols(); ++k)
          z[i * Ncols + j] += A(i, k) * B(k, j);
  }

  template<typename REAL1>
  explicit constexpr RMatrixT(const RVectorT<REAL1> &d) :
      z(d.size() * d.size(), REAL{}), Nrows(d.size()), Ncols(d.size()) {
    auto diagIt = std::begin(z);
    for (const auto &element : d) {
      *diagIt = element;
      std::advance(diagIt, Ncols + 1);
    }
  }

  inline RMatrixT &operator=(const REAL &b) {
    std::fill(std::begin(z), std::end(z), b);
    return *this;
  }

  template<typename REAL1>
  inline RMatrixT &operator=(const RMatrixT<REAL1> &A) {
    Nrows = A.rows();
    Ncols = A.cols();
    z.assign(std::cbegin(A), std::cend(A));
    return *this;
  }

  //    RMatrixT &operator=(RMatrixT &&A) {
  //        (*this) = std::move(A);
  //        return *this;
  //    }

  template<typename REAL1>
  inline RMatrixT &operator=(const SymRMatrixT<REAL1> &A) {
    resize(A.Dim());
    for (int i = 0; i < A.Dim(); ++i) {
      z[i * Ncols + i] = A(i, i);
      for (int j = 0; j < i; ++j) {
        z[i * Ncols + j] = A(i, j);
        z[j * Ncols + i] = A(i, j);
      }
    }
    return *this;
  }

  template<typename REAL1>
  inline RMatrixT &operator=(const AntisymRMatrixT<REAL1> &A) {
    resize(A.Dim());
    for (int i = 0; i < A.Dim(); ++i)
      for (int j = 0; j < i; ++j) {
        z[i * Ncols + j] = A(i, j);
        z[j * Ncols + i] = A(j, i);
      }
    return *this;
  }

  template<typename REAL1>
  inline RMatrixT &operator+=(const RMatrixT<REAL1> &A) {
    if (Nrows != A.rows() || Ncols != A.cols()) THROW("Size does not fit!")
    std::transform(std::cbegin(z), std::cend(z), std::cbegin(A), std::begin(z), std::plus<>());
    return *this;
  }

  template<typename REAL1>
  inline RMatrixT &operator+=(const SymRMatrixT<REAL1> &A) {
    if (Nrows != A.Dim() || Ncols != A.Dim()) THROW("Size does not fit!")
    for (int i = 0; i < Nrows; ++i)
      for (int j = 0; j < Ncols; ++j)
        z[i * Ncols + j] += A(i, j);
    return *this;
  }

  template<typename REAL1>
  inline RMatrixT &operator+=(const AntisymRMatrixT<REAL1> &A) {
    if (Nrows != A.Dim() || Ncols != A.Dim()) THROW("Size does not fit!")
    for (int i = 0; i < Nrows; ++i)
      for (int j = 0; j < Ncols; ++j)
        z[i * Ncols + j] += A(i, j);
    return *this;
  }

  template<typename REAL1>
  inline RMatrixT &operator-=(const RMatrixT<REAL1> &A) {
    if (Nrows != A.rows() || Ncols != A.cols()) THROW("Size does not fit!")
    std::transform(std::cbegin(z), std::cend(z), std::cbegin(A), std::begin(z), std::minus<>());
    return *this;
  }

  template<typename REAL1>
  inline RMatrixT &operator-=(const SymRMatrixT<REAL1> &A) {
    if (Nrows != A.Dim() || Ncols != A.Dim()) THROW("Size does not fit!")
    for (int i = 0; i < Nrows; ++i)
      for (int j = 0; j < Ncols; ++j)
        z[i * Ncols + j] -= A(i, j);
    return *this;
  }

  template<typename REAL1>
  inline RMatrixT &operator-=(const AntisymRMatrixT<REAL1> &A) {
    if (Nrows != A.Dim() || Ncols != A.Dim()) THROW("Size does not fit!")
    for (int i = 0; i < Nrows; ++i)
      for (int j = 0; j < Ncols; ++j)
        z[i * Ncols + j] -= A(i, j);
    return *this;
  }

  inline RMatrixT &operator*=(const REAL &b) {
    std::transform(std::cbegin(z), std::cend(z), std::begin(z),
                   [&b](const auto &value) { return value * b; });
    return *this;
  }

  inline RMatrixT &operator/=(const REAL &b) {
    std::transform(std::cbegin(z), std::cend(z), std::begin(z),
                   [&b](const auto &value) { return value / b; });
    return *this;
  }

  inline auto operator[](int row) { return std::next(std::begin(z), row * Ncols); }

  inline auto operator[](int row) const { return std::next(std::cbegin(z), row * Ncols); }

  inline REAL &operator()(int row, int col) { return z[row * Ncols + col]; }

  inline REAL &operator()(int index) { return z[index]; }

  inline const REAL &operator()(int row, int col) const { return z[row * Ncols + col]; }

  inline void operator()(const REAL &b, int row, int col) { z[row * Ncols + col] = b; }

  inline RVectorT<REAL> row(int i) const {
    RVectorT<REAL> r(Ncols);
    const auto rowBegin = std::next(std::cbegin(z), i * Ncols);
    std::copy(rowBegin, std::next(rowBegin, Ncols), std::begin(r));
    return r;
  }

  inline RVectorT<REAL> col(int j) const {
    RVectorT<REAL> c(Nrows);
    auto colIt = std::next(std::cbegin(z), j);
    for (auto &element : c) {
      element = *colIt;
      std::advance(colIt, Ncols);
    }
    return c;
  }

  inline int size() const noexcept { return z.size(); }

  inline int rows() const noexcept { return Nrows; }

  inline int cols() const noexcept { return Ncols; }

  inline auto begin() const noexcept { return z.begin(); }

  inline auto begin() noexcept { return z.begin(); }

  inline auto end() const noexcept { return z.end(); }

  inline auto end() noexcept { return z.end(); }

  inline const std::vector<REAL> &Data() const noexcept { return z; }

  inline std::vector<REAL> &Data() noexcept { return z; }

  inline REAL normSqr() const {
    return std::accumulate(std::cbegin(z), std::cend(z), REAL{},
                           [](const auto &first, const auto &second) {
                             return first + second * second;
                           });
  }

  REAL norm() const;

  inline RVectorT<REAL> diag() {
    const std::size_t size = std::min(Nrows, Ncols);
    RVectorT<REAL> d(size);
    auto diagIt = std::cbegin(z);
    for (auto &element : d) {
      element = *diagIt;
      std::advance(diagIt, Ncols + 1);
    }
    return d;
  }

  template<typename REAL1>
  inline void diag(const RVectorT<REAL1> &d) {
    resize(d.size(), d.size());
    auto diagIt = std::begin(z);
    for (const auto &element : d) {
      *diagIt = element;
      std::advance(diagIt, Ncols + 1);
    }
  }

  template<typename REAL1>
  inline RVectorT<REAL> multiplyWith(const RVectorT<REAL1> &v) const {
    if (v.Dim() != Ncols) {
      THROW(std::format("Dimension does not fit: Matcols: {} Vectordim: {}", Ncols, v.Dim()))
    }
    RVectorT<REAL> w(Nrows);
    auto colIt = std::cbegin(z);
    for (auto &element : w) {
      element = std::inner_product(std::cbegin(v), std::cend(v), colIt, REAL{});
      std::advance(colIt, Ncols);
    }
    return w;
  }

  template<typename REAL1>
  inline RVectorT<REAL> multipliedWith(const RVectorT<REAL1> &v) const {
    if (v.Dim() != Nrows) THROW("Dimension does not fit!")
    RVectorT<REAL> w(Ncols);
    for (int i = 0; i < Ncols; ++i) {
      w[i] = v * col(i);
    }
    return w;
  }

  inline void resize(int rows, int cols) {
    z = std::vector<REAL>(rows * cols, REAL{});
    Nrows = rows;
    Ncols = cols;
  }

  inline void resize(int dim) { resize(dim, dim); }

  void Accumulate(int commSplit = 0);

  template<typename REAL1>
  inline void Insert(const RMatrixT<REAL1> &B, int row, int col) {
    if (row + B.rows() >= Nrows || col + B.cols() >= Ncols) {
      THROW("Cannot insert matrix: size of B too large")
    }
    for (int i = 0; i < B.rows(); ++i)
      for (int j = 0; j < B.cols(); ++j)
        z[(row + i) * Ncols + col + j] = B[i][j];
  }

  template<typename REAL1>
  inline void InsertRow(const RVectorT<REAL1> &v, int row, int startCol = 0) {
    if (row >= Nrows || startCol + v.size() > Ncols) {
      THROW("Cannot insert row: size of v too large")
    }
    auto destIt = std::next(std::begin(z), row * Ncols + startCol);
    std::copy(std::cbegin(v), std::cend(v), destIt);
  }

  template<typename REAL1>
  inline void InsertCol(const RVectorT<REAL1> &v, int col, int startRow = 0) {
    if (col >= Ncols || startRow + v.size() > Nrows) {
      THROW("Cannot insert column: size of v too large")
    }
    auto colIt = std::next(std::begin(z), startRow * Ncols + col);
    for (const auto &element : v) {
      *colIt = element;
      std::advance(colIt, Ncols);
    }
  }

  inline REAL Mean() const {
    REAL sum = std::accumulate(std::cbegin(z), std::cend(z), REAL{});
    return sum / z.size();
  }

  inline REAL Variance() const {
    REAL m2{};
    REAL diff{};
    REAL correction{};
    REAL mean = this->Mean();
    for (auto const &value : z) {
      diff = (value - mean);
      m2 += diff * diff;
      correction += diff;
    }
    return (m2 - correction * correction / z.size()) / (z.size() - 1);
  }

  inline REAL Trace() const {
    REAL tr{};
    if (Nrows != Ncols) THROW("Trace only defined for square matrices!")
    auto diagIt = std::begin(z);
    for (int i = 0; i < Nrows; ++i) {
      tr += *diagIt;
      std::advance(diagIt, Ncols + 1);
    }
    return tr;
  }

  REAL NormOne() const;

  REAL NormInfty() const;

  inline RMatrixT<REAL> &transpose() {
    RMatrixT<REAL> b(*this);
    this->resize(b.Ncols, b.Nrows);
    for (int i = 0; i < Ncols; ++i)
      for (int j = 0; j < Nrows; ++j)
        (*this)[j][i] = b[i][j];
    return *this;
  }

  inline RMatrixT<REAL> &Identity() {
    std::fill(std::begin(z), std::end(z), REAL(0));
    for (std::size_t i = 0; i < std::min(Nrows, Ncols); ++i) {
      z[i * Ncols + i] = REAL(1);
    }
    return *this;
  }

  RMatrixT<REAL> &Invert();

  RMatrixT<REAL> &Exp(bool deg13 = false);

  RMatrixT<REAL> &Phi1(RVector &Evaluated, bool deg13 = false);

  RMatrixT<REAL> &Phi1();

  RMatrixT<REAL> &Phi2();

  RMatrixT<REAL> &Phi3();

  void SaddlePoint(const RMatrixT &A, const RMatrixT &B);

  Saver &save(Saver &saver) const;

  Loader &load(Loader &loader);

  RMatrixT<double> &Exp(RVector &Evaluated, bool deg13 = false);
};

template<typename REAL>
inline bool operator==(const RMatrixT<REAL> &A, const RMatrixT<REAL> &B) {
  if (A.rows() != B.rows() || A.cols() != B.cols()) return false;
  for (int i = 0; i < A.Data().size(); ++i)
    if (!mpp_ba::isNear(A.Data()[i], B.Data()[i])) return false;
  return true;
}

template<typename REAL>
inline bool operator==(const RMatrixT<REAL> &A, const SymRMatrixT<REAL> &B) {
  return A == RMatrixT<REAL>(B);
}

template<typename REAL>
inline bool operator==(const SymRMatrixT<REAL> &A, const RMatrixT<REAL> &B) {
  return RMatrixT<REAL>(A) == B;
}

template<typename REAL>
inline bool operator==(const RMatrixT<REAL> &A, const AntisymRMatrixT<REAL> &B) {
  return A == RMatrixT<REAL>(B);
}

template<typename REAL>
inline bool operator==(const AntisymRMatrixT<REAL> &A, const RMatrixT<REAL> &B) {
  return RMatrixT<REAL>(A) == B;
}

template<typename REAL>
inline bool operator!=(const RMatrixT<REAL> &A, const RMatrixT<REAL> &B) {
  return !(A == B);
}

template<typename REAL>
inline bool operator!=(const RMatrixT<REAL> &A, const SymRMatrixT<REAL> &B) {
  return !(A == B);
}

template<typename REAL>
inline bool operator!=(const SymRMatrixT<REAL> &A, const RMatrixT<REAL> &B) {
  return !(A == B);
}

template<typename REAL>
inline bool operator!=(const RMatrixT<REAL> &A, const AntisymRMatrixT<REAL> &B) {
  return !(A == B);
}

template<typename REAL>
inline bool operator!=(const AntisymRMatrixT<REAL> &A, const RMatrixT<REAL> &B) {
  return !(A == B);
}

template<typename REAL>
inline RMatrixT<REAL> operator-(const RMatrixT<REAL> &a) {
  RMatrixT<REAL> c(a);
  return c *= -1;
}

template<typename REAL>
inline RMatrixT<REAL> operator+(const RMatrixT<REAL> &a, const RMatrixT<REAL> &b) {
  RMatrixT<REAL> c(a);
  return c += b;
}

template<typename REAL>
inline RMatrixT<REAL> operator+(const RMatrixT<REAL> &a, const SymRMatrixT<REAL> &b) {
  RMatrixT<REAL> c(a);
  return c += b;
}

template<typename REAL>
inline RMatrixT<REAL> operator+(const SymRMatrixT<REAL> &a, const RMatrixT<REAL> &b) {
  RMatrixT<REAL> c(a);
  return c += b;
}

template<typename REAL>
inline RMatrixT<REAL> operator+(const RMatrixT<REAL> &a, const AntisymRMatrixT<REAL> &b) {
  RMatrixT<REAL> c(a);
  return c += b;
}

template<typename REAL>
inline RMatrixT<REAL> operator+(const AntisymRMatrixT<REAL> &a, const RMatrixT<REAL> &b) {
  RMatrixT<REAL> c(a);
  return c += b;
}

template<typename REAL>
inline RMatrixT<REAL> operator-(const RMatrixT<REAL> &a, const RMatrixT<REAL> &b) {
  RMatrixT<REAL> c(a);
  return c -= b;
}

template<typename REAL>
inline RMatrixT<REAL> operator-(const RMatrixT<REAL> &a, const SymRMatrixT<REAL> &b) {
  RMatrixT<REAL> c(a);
  return c -= b;
}

template<typename REAL>
inline RMatrixT<REAL> operator-(const SymRMatrixT<REAL> &a, const RMatrixT<REAL> &b) {
  RMatrixT<REAL> c(a);
  return c -= b;
}

template<typename REAL>
inline RMatrixT<REAL> operator-(const RMatrixT<REAL> &a, const AntisymRMatrixT<REAL> &b) {
  RMatrixT<REAL> c(a);
  return c -= b;
}

template<typename REAL>
inline RMatrixT<REAL> operator-(const AntisymRMatrixT<REAL> &a, const RMatrixT<REAL> &b) {
  RMatrixT<REAL> c(a);
  return c -= b;
}

template<typename REAL>
inline RMatrixT<REAL> operator*(const RMatrixT<REAL> &a, const RMatrixT<REAL> &b) {
  return RMatrixT<REAL>(a, b);
}

template<typename REAL>
inline RMatrixT<REAL> operator*(const SymRMatrixT<REAL> &a, const SymRMatrixT<REAL> &b) {
  return RMatrixT<REAL>(a, b);
}

template<typename REAL, typename REAL1>
inline RMatrixT<REAL> operator*(const RMatrixT<REAL> &a, const SymRMatrixT<REAL1> &b) {
  return RMatrixT<REAL>(a, b);
}

template<typename REAL, typename REAL1>
inline RMatrixT<REAL> operator*(const SymRMatrixT<REAL1> &a, const RMatrixT<REAL> &b) {
  return RMatrixT<REAL>(a, b);
}

template<typename REAL>
inline RMatrixT<REAL> operator*(const AntisymRMatrixT<REAL> &a, const AntisymRMatrixT<REAL> &b) {
  return RMatrixT<REAL>(a, b);
}

template<typename REAL, typename REAL1>
inline RMatrixT<REAL> operator*(const RMatrixT<REAL> &a, const AntisymRMatrixT<REAL1> &b) {
  return RMatrixT<REAL>(a, b);
}

template<typename REAL, typename REAL1>
inline RMatrixT<REAL> operator*(const AntisymRMatrixT<REAL1> &a, const RMatrixT<REAL> &b) {
  return RMatrixT<REAL>(a, b);
}

template<typename REAL>
inline RVectorT<REAL> operator*(const RMatrixT<REAL> &a, const RVectorT<REAL> &v) {
  return a.multiplyWith(v);
}

template<typename REAL>
inline RVectorT<REAL> operator*(const RVectorT<REAL> &v, const RMatrixT<REAL> &a) {
  return a.multipliedWith(v);
}

template<typename REAL, typename REAL1>
inline RMatrixT<REAL> operator*(const RMatrixT<REAL> &a, const REAL1 &b) {
  RMatrixT<REAL> c(a);
  return c *= b;
}

template<typename REAL, typename REAL1>
inline RMatrixT<REAL> operator*(const REAL1 &b, const RMatrixT<REAL> &a) {
  RMatrixT<REAL> c(a);
  return c *= b;
}

template<typename REAL, typename REAL1>
inline RMatrixT<REAL> operator/(const RMatrixT<REAL> &a, const REAL1 &b) {
  RMatrixT<REAL> c(a);
  return c /= b;
}

template<typename REAL>
inline RMatrixT<REAL> transpose(const RMatrixT<REAL> &a) {
  RMatrixT<REAL> b(a);
  return b.transpose();
}

template<typename REAL>
inline RMatrixT<REAL> invert(const RMatrixT<REAL> &a) {
  RMatrixT<REAL> b(a);
  return b.Invert();
}

template<typename REAL>
inline RMatrixT<REAL> product(const RVectorT<REAL> &v, const RVectorT<REAL> &w) {
  RMatrixT<REAL> A(v.size(), w.size());
  auto colMtxIt = std::begin(A);
  for (const auto &rowElement : v) {
    std::transform(std::cbegin(w), std::cend(w), colMtxIt,
                   [&](const auto &colElement) { return rowElement * colElement; });
    std::advance(colMtxIt, w.size());
  }
  return A;
}

template<typename REAL>
inline Saver &operator<<(Saver &saver, const RMatrixT<REAL> &A) {
  return A.save(saver);
}

template<typename REAL>
inline Loader &operator>>(Loader &loader, RMatrixT<REAL> &A) {
  return A.load(loader);
}

template<typename REAL>
std::ostream &operator<<(std::ostream &os, const RMatrixT<REAL> &A) {
  if constexpr (std::is_same_v<REAL, double>) os << beginD;
  for (int i = 0; i < A.rows(); ++i) {
    if (i != 0) {
      os << "\n";
      if constexpr (std::is_same_v<REAL, double>) os << beginD;
    }
    for (int j = 0; j < A.cols(); ++j)
      os << A(i, j) << " ";
  }
  if constexpr (std::is_same_v<REAL, double>) os << endD;
  return os;
}

// LAPACK-like Methods because the real LAPACK seems to slow down the calculations...
void LUDecomp(int n, double *Mat, int *ipv);
void invertSmallMatrix(int n, double *a);
void FwdBwdSubstitution(int n, double *Mat, int *piv, double *rhs);
void applySmallMatrix(int n, double *u, const double *a, const double *b);

using RMatrix = RMatrixT<double>;

#ifdef BUILD_IA

using IARMatrix = RMatrixT<IAInterval>;

RMatrix mid(const IARMatrix &);

RMatrix sup(const IARMatrix &);

RMatrix inf(const IARMatrix &);

inline IARMatrix operator+(const IARMatrix &A, const RMatrix &B) {
  IARMatrix C(A);
  return C += B;
}

inline IARMatrix operator+(const RMatrix &A, const IARMatrix &B) {
  IARMatrix C(A);
  return C += B;
}

inline IARMatrix operator+(const IARMatrix &A, const SymRMatrix &B) {
  IARMatrix C(A);
  return C += B;
}

inline IARMatrix operator+(const SymRMatrix &A, const IARMatrix &B) {
  IARMatrix C(A);
  return C += B;
}

inline IARMatrix operator+(const IARMatrix &A, const AntisymRMatrix &B) {
  IARMatrix C(A);
  return C += B;
}

inline IARMatrix operator+(const AntisymRMatrix &A, const IARMatrix &B) {
  IARMatrix C(A);
  return C += B;
}

inline IARMatrix operator-(const IARMatrix &A, const RMatrix &B) {
  IARMatrix C(A);
  return C -= B;
}

inline IARMatrix operator-(const RMatrix &A, const IARMatrix &B) {
  IARMatrix C(A);
  return C -= B;
}

inline IARMatrix operator-(const IARMatrix &A, const SymRMatrix &B) {
  IARMatrix C(A);
  return C -= B;
}

inline IARMatrix operator-(const SymRMatrix &A, const IARMatrix &B) {
  IARMatrix C(A);
  return C -= B;
}

inline IARMatrix operator-(const IARMatrix &A, const AntisymRMatrix &B) {
  IARMatrix C(A);
  return C -= B;
}

inline IARMatrix operator-(const AntisymRMatrix &A, const IARMatrix &B) {
  IARMatrix C(A);
  return C -= B;
}

inline IARVector operator*(const IARMatrix &A, const RVector &v) { return A.multiplyWith(v); }

inline IARVector operator*(const RMatrix &A, const IARVector &v) {
  IARMatrix B(A);
  return B.multiplyWith(v);
}

inline IARMatrix operator*(const IARMatrix &a, const RMatrix &b) { return IARMatrix(a, b); }

inline IARMatrix operator*(const RMatrix &a, const IARMatrix &b) { return IARMatrix(a, b); }

inline IARMatrix operator*(const IARMatrix &a, const SymRMatrix &b) { return IARMatrix(a, b); }

inline IARMatrix operator*(const SymRMatrix &a, const IARMatrix &b) { return IARMatrix(a, b); }

inline IARMatrix operator*(const IASymRMatrix &a, const RMatrix &b) { return IARMatrix(a, b); }

inline IARMatrix operator*(const RMatrix &a, const IASymRMatrix &b) { return IARMatrix(a, b); }

inline IARMatrix operator*(const IARMatrix &a, const AntisymRMatrix &b) { return IARMatrix(a, b); }

inline IARMatrix operator*(const AntisymRMatrix &a, const IARMatrix &b) { return IARMatrix(a, b); }

inline IARMatrix operator*(const IAAntisymRMatrix &a, const RMatrix &b) { return IARMatrix(a, b); }

inline IARMatrix operator*(const RMatrix &a, const IAAntisymRMatrix &b) { return IARMatrix(a, b); }

#endif // BUILD_IA

#endif

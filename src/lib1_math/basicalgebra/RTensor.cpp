#include "RTensor.hpp"

#include <vector>

template<typename REAL>
REAL RTensorT<REAL>::Variance() const {
  REAL m2{};
  REAL diff{};
  REAL correction{};
  REAL mean = this->Mean();
  for (auto const &value : z) {
    diff = (value - mean);
    m2 += diff * diff;
    correction += diff;
  }
  return (m2 - correction * correction / z.size()) / (z.size() - 1);
}

template class RTensorT<double>;

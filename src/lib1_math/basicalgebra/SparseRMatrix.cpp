#include "SparseRMatrix.hpp"

#include <algorithm>
#include <cmath>
#include <utility>
#include <vector>

template<typename REAL>
void SparseRMatrixT<REAL>::compress() {
  int numEntries = std::min(Nrows, Ncols);
  for (int i = 0; i < Nrows; ++i)
    for (int k = d[i] + 1; k < d[i + 1]; ++k)
      if (!mpp_ba::isNearZero(a[k])) ++numEntries;
  std::vector<REAL> a0(numEntries);
  std::vector<int> d0(Nrows + 1);
  std::vector<int> col0(numEntries);
  d0[Nrows] = numEntries;

  int nn = 0;
  for (int i = 0; i < Nrows; ++i) {
    int k = d[i];
    d0[i] = nn;
    a0[nn] = a[k];
    col0[nn] = col[k];
    ++nn;
    for (int k = d[i] + 1; k < d[i + 1]; ++k)
      if (!mpp_ba::isNearZero(a[k])) {
        a0[nn] = a[k];
        col0[nn] = col[k];
        ++nn;
      }
  }
  //  mout << "Compressed matrix. Before: " << d[Nrows] << ", after: " << nn << "."
  //       << " Ratio: " << double(nn) / d[Nrows] << endl;

  a = std::move(a0);
  d = std::move(d0);
  col = std::move(col0);
}

template<typename REAL>
void SparseRMatrixT<REAL>::changeToDiagonal(int j) {
  std::vector<REAL> aa;
  aa.resize(a.size());
  std::vector<int> dd;
  dd.resize(d.size());
  std::vector<int> colcol;
  colcol.resize((col.size()));
  for (int i = 0; i < Nrows; ++i) {
    int k = (*this).colptr((*this).rowind(i));
    REAL val = (*this).nzval((*this).rowind(i));
    bool sorted = false;
    for (int l = (*this).rowind(i) + 1; l < (*this).rowind(i + 1); ++l) {
      if (colptr(l) == i + j) {
        colcol[this->rowind(i)] = i + j;
        aa[this->rowind(i)] = a[l];
        colcol[l] = colptr(l - 1);
        aa[l] = a[l - 1];
      } else {
        if (k > colptr(l)) {
          colcol[l] = col[l];
          aa[l] = a[l];
        } else if (k < colptr(l) && colptr(l) < (i + j)) {
          if (!sorted) {
            colcol[l] = k;
            aa[l] = a[(*this).rowind(i)];
            sorted = true;
          } else {
            colcol[l] = colptr(l - 1);
            aa[l] = a[l - 1];
          }
        } else {
          colcol[l] = colptr(l);
          aa[l] = a[l];
        }
      }
    }
  }
  a = aa;
  col = colcol;
}


template class SparseRMatrixT<double>;

#ifdef BUILD_IA
template class SparseRMatrixT<IAInterval>;

#endif

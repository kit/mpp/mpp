#include "HermCMatrix.hpp"

#include <algorithm>
#include <complex>
#include <format>
#include <iterator>

#include "AntisymRMatrix.hpp"
#include "CFunctions.hpp"
#include "Parallel.hpp"
#include "SaveLoad.hpp"
#include "SymRMatrix.hpp"

//==================================================================================================
// BLAS/LAPACK routines
//==================================================================================================
// factorization of symmetric matrix
extern "C" void zsytrf_(char *UPLO, int *N, void *A, int *LDA, int *IPIV, void *WORK, void *LWORK,
                        int *INFO);
// Invert matrix using factorization
extern "C" void zsytri_(char *UPLO, int *N, void *A, int *LDA, int *IPIV, void *WORK, int *INFO);

//==================================================================================================

template<typename REAL>
COMPLEX_TYPE<REAL> HermCMatrixT<REAL>::operator()(int i, int j) {
  if (i >= j) return z[(i * (i + 1)) / 2 + j];
  else return baConj(z[(j * (j + 1)) / 2 + i]);
}

template<typename REAL>
COMPLEX_TYPE<REAL> HermCMatrixT<REAL>::operator()(int i, int j) const {
  if (i >= j) return z[(i * (i + 1)) / 2 + j];
  else return baConj(z[(j * (j + 1)) / 2 + i]);
}

template<typename REAL>
void HermCMatrixT<REAL>::operator()(const COMPLEX &a, int i, int j) {
  if (i == j) {
    if (baImag(a) == REAL{}) z[(i * (i + 1)) / 2 + j] = a;
    else THROW("Diagonal must be real!")
  } else if (i > j) z[(i * (i + 1)) / 2 + j] = a;
  else if (i < j) z[(j * (j + 1)) / 2 + i] = baConj(a);
}

template<typename REAL>
HermCMatrixT<REAL> &HermCMatrixT<REAL>::conj() {
  std::transform(std::cbegin(z), std::cend(z), std::begin(z),
                 [](const auto &value) { return baConj(value); });
  return *this;
}

template<typename REAL>
SymRMatrixT<REAL> HermCMatrixT<REAL>::real() const {
  SymRMatrixT<REAL> re(dim);
  std::transform(std::cbegin(z), std::cend(z), std::begin(re),
                 [](const auto &value) { return baReal(value); });
  return re;
}

template<typename REAL>
AntisymRMatrixT<REAL> HermCMatrixT<REAL>::imag() const {
  AntisymRMatrixT<REAL> im(dim);
  for (int i = 0; i < dim; ++i)
    for (int j = i + 1; j < dim; ++j)
      im(baImag((*this)(i, j)), i, j);
  return im;
}

template<typename REAL>
void HermCMatrixT<REAL>::Accumulate(int commSplit) {
  PPM->SumOnCommSplit(z, commSplit);
}

template<typename REAL>
HermCMatrixT<REAL> &HermCMatrixT<REAL>::transpose() {
  return conj();
}

template<>
HermCMatrixT<double> &HermCMatrixT<double>::Invert() {
  std::complex<double> A[dim * dim];
  for (int i = 0; i < dim; ++i)
    for (int j = 0; j < dim; ++j)
      A[i * dim + j] = (*this)(i, j);

  int I[dim];
  int info;
  int lwork = 2 * dim;
  std::complex<double> work[2 * lwork];
  char uplo = 'L';
  zsytrf_(&uplo, &dim, A, &dim, I, work, &lwork, &info);
  if (info < 0) {
    THROW(std::format("Error in LAPACK: {}-th argument had an illegal value", -info))
  } else if (info > 0) {
    THROW(std::format("Error in LAPACK: A({},{} is exactly zero.", -info, -info))
  }
  zsytri_(&uplo, &dim, A, &dim, I, work, &info);
  if (info < 0) {
    THROW(std::format("Error in LAPACK: {}-th argument had an illegal value", -info))
  } else if (info > 0) {
    THROW(std::format("Error in LAPACK: A({},{} is exactly zero.", -info, -info))
  }

  for (int i = 0; i < dim; ++i)
    for (int j = i; j < dim; ++j)
      (*this)(A[i * dim + j], i, j);

  return *this;
}

template<typename REAL>
Saver &HermCMatrixT<REAL>::save(Saver &saver) const {
  saver << Dim();
  for (const auto &element : z) {
    saver << element;
  }
  return saver;
}

template<typename REAL>
Loader &HermCMatrixT<REAL>::load(Loader &loader) {
  int N;
  loader >> N;
  resize(N);
  for (auto &element : z) {
    loader >> element;
  }
  return loader;
}

template class HermCMatrixT<double>;

#ifdef BUILD_IA

template class HermCMatrixT<IAInterval>;

HermCMatrix mid(const IAHermCMatrix &IA_A) {
  HermCMatrix A(IA_A.Dim());
  for (int i = 0; i < A.rows(); ++i)
    for (int j = 0; j < A.cols(); ++j)
      A(mid(IA_A(i, j)), i, j);
  return A;
}

#endif // BUILD_IA
#ifndef RVector_H
#define RVector_H

#include <algorithm>
#include <cstddef>
#include <functional>
#include <initializer_list>
#include <iterator>
#include <numeric>
#include <vector>

#include "Assertion.hpp"
#include "Logging.hpp"


#ifdef BUILD_IA

#include "IAInterval.hpp"

#endif

namespace mpp_ba {
void SetTolerance(double tol = 1e-12);

double GetTolerance();

bool isNear(double a, double b);

bool isNearZero(double a);

#ifdef BUILD_IA

bool isNear(const IAInterval &a, const IAInterval &b);

bool isNearZero(const IAInterval &a);

#endif

template<typename REAL, typename REAL1>
using enable_if_not_same =
    typename std::enable_if<!std::is_same<typename std::decay<REAL1>::type, REAL>::value>::type;

template<typename REAL, typename REAL1, typename RETURN_TYPE>
using enable_if_constructible =
    typename std::enable_if<std::is_constructible<REAL, REAL1>::value, RETURN_TYPE>::type;
} // namespace mpp_ba

class Saver;

class Loader;

template<typename REAL = double>
class RVectorT {
protected:
  std::vector<REAL> z{};
public:
  RVectorT() = default;

  explicit RVectorT(const size_t length) : z(length) {}

  RVectorT(const RVectorT &v) : z(v.z) {}

  RVectorT(RVectorT &&v) : z(std::move(v.z)) {}

  /// RVector of other type
  template<typename REAL1, typename Constraint = mpp_ba::enable_if_not_same<REAL, REAL1>>
  RVectorT(const RVectorT<REAL1> &v) : z(v.size()) {
    for (size_t i = 0; i < v.size(); ++i)
      z[i] = v[i];
  }

  /// selects part of v (indices included)
  RVectorT(const RVectorT &v, const size_t startIdx, const size_t endIdx) :
      z(v.begin() + startIdx, v.begin() + endIdx) {}

  template<typename REAL1, typename Constraint = mpp_ba::enable_if_constructible<REAL, REAL1, REAL>>
  RVectorT(const REAL1 &a, const size_t length) : z(length, a) {}

  template<typename REAL1, typename Constraint = mpp_ba::enable_if_constructible<REAL, REAL1, REAL>>
  RVectorT(const std::initializer_list<REAL1> &v) : z(v.begin(), v.end()) {}

  template<typename REAL1, typename Constraint = mpp_ba::enable_if_constructible<REAL, REAL1, REAL>>
  RVectorT(const std::vector<REAL1> &v) : z(v.begin(), v.end()) {}

  template<typename REAL1>
  inline mpp_ba::enable_if_constructible<REAL, REAL1, RVectorT> &operator=(const REAL1 &b) {
    std::fill(z.begin(), z.end(), b);
    return *this;
  }

  inline RVectorT &operator=(const RVectorT &v) {
    if (this != &v) z = v.z;
    return *this;
  }

  inline RVectorT &operator=(RVectorT &&v) noexcept {
    z = std::move(v.z);
    return *this;
  }

  /// RVector of other type
  template<typename REAL1, typename Constraint = mpp_ba::enable_if_not_same<REAL, REAL1>>
  inline RVectorT &operator=(const RVectorT<REAL1> &v) {
    z.assign(std::cbegin(v), std::cend(v));
    return *this;
  }

  template<typename REAL1>
  inline mpp_ba::enable_if_constructible<REAL, REAL1, RVectorT> &operator+=(const REAL1 &a) {
    std::transform(std::cbegin(z), std::cend(z), std::begin(z),
                   [&a](const auto &value) { return value + a; });
    return *this;
  }

  template<typename REAL1>
  inline RVectorT &operator+=(const RVectorT<REAL1> &v) {
    if constexpr (DebugLevel > 0) {
      if (z.size() != v.size()) THROW("Size does not fit!")
    }
    std::transform(std::cbegin(z), std::cend(z), std::cbegin(v), std::begin(z), std::plus<>());
    return *this;
  }

  template<typename REAL1>
  inline mpp_ba::enable_if_constructible<REAL, REAL1, RVectorT> &operator-=(const REAL1 &a) {
    std::transform(std::cbegin(z), std::cend(z), std::begin(z),
                   [&a](const auto &value) { return value - a; });
    return *this;
  }

  template<typename REAL1>
  inline RVectorT &operator-=(const RVectorT<REAL1> &v) {
    if constexpr (DebugLevel > 0) {
      if (z.size() != v.size()) THROW("Size does not fit!")
    }
    std::transform(std::cbegin(z), std::cend(z), std::cbegin(v), std::begin(z), std::minus<>());
    return *this;
  }

  template<typename REAL1>
  inline mpp_ba::enable_if_constructible<REAL, REAL1, RVectorT> &operator*=(const REAL1 &a) {
    if constexpr (std::is_same_v<REAL, double>) {
      if (a == 1.0) return *this;
    }
    std::transform(std::cbegin(z), std::cend(z), std::begin(z),
                   [a](const auto &value) { return value * a; });
    return *this;
  }

  // This a component wise operation
  template<typename REAL1>
  inline RVectorT &operator*=(const RVectorT<REAL1> &v) {
    if constexpr (DebugLevel > 0) {
      if (z.size() != v.size()) THROW("Size does not fit!")
    }
    std::transform(std::cbegin(z), std::cend(z), std::cbegin(v), std::begin(z),
                   std::multiplies<>());
    return *this;
  }

  // This a component wise operation
  template<typename REAL1>
  inline RVectorT &operator/=(const RVectorT<REAL1> &v) {
    if constexpr (DebugLevel > 0) {
      if (z.size() != v.size()) THROW("Size does not fit!")
    }
    std::transform(std::cbegin(z), std::cend(z), std::cbegin(v), std::begin(z), std::divides<>());
    return *this;
  }

  template<typename REAL1>
  inline mpp_ba::enable_if_constructible<REAL, REAL1, RVectorT> &operator/=(const REAL1 &a) {
    if constexpr (std::is_same_v<REAL, double>) {
      if (a == 1.0) return *this;
    }
    std::transform(std::cbegin(z), std::cend(z), std::begin(z),
                   [a](const auto &value) { return value / a; });
    return *this;
  }

  inline void removeLast() noexcept { z.pop_back(); }

  inline const std::vector<REAL> &asVector() const noexcept { return z; }

  inline std::vector<REAL> &asVector() noexcept { return z; }

  inline size_t Dim() const noexcept { return z.size(); }

  inline size_t size() const noexcept { return z.size(); }

  inline void resize(const size_t N) { z.resize(N); }

  inline REAL &operator[](const size_t i) { return z[i]; }

  inline const REAL &operator[](const size_t i) const { return z[i]; }

  inline const REAL *operator()() const noexcept { return z.data(); }

  inline REAL *operator()() noexcept { return z.data(); }

  inline auto begin() const noexcept { return z.begin(); }

  inline auto begin() noexcept { return z.begin(); }

  inline auto rbegin() const noexcept { return z.rbegin(); }

  inline auto end() const noexcept { return z.end(); }

  inline auto end() noexcept { return z.end(); }

  inline auto rend() const noexcept { return z.rend(); }

  inline auto front() const noexcept { return z.front(); }

  inline REAL &back() noexcept { return z.back(); }

  inline const REAL &back() const noexcept { return z.back(); }

  inline auto push_back(const REAL &a) { return z.push_back(a); }

  inline void clear() noexcept { z.clear(); }

  template<typename REAL1>
  inline auto append(const RVectorT<REAL1> &v) {
    return z.insert(z.end(), v.begin(), v.end());
  }

  template<typename REAL1>
  inline void insert(const RVectorT<REAL1> &v, const size_t start) {
    if (start + v.size() > z.size()) { THROW("Cannot insert row: size of v too large") }
    std::copy(std::cbegin(v), std::cend(v), std::next(std::begin(z), start));
  }

  inline const std::vector<REAL> &Data() const noexcept { return z; }

  inline REAL normSqr() const {
    return std::inner_product(std::cbegin(z), std::cend(z), std::cbegin(z), REAL{});
  }

  REAL norm() const;

  REAL normAccumulated(int commSplit = 0) const;

  inline REAL normOnes() const { return std::accumulate(std::cbegin(z), std::cend(z), REAL{}); }

  inline REAL Mean() const {
    REAL sum = std::accumulate(std::cbegin(z), std::cend(z), REAL{});
    return sum / z.size();
  }

  REAL Variance() const;

  inline REAL Min() const {
    if constexpr (std::is_same_v<REAL, double>) {
      const auto minIt = std::min_element(std::cbegin(z), std::cend(z));
      if (minIt != std::cend(z)) { return *minIt; }
      return infty;
    } else THROW("Min not implemented")
  }

  REAL MinAccumulated(int commSplit = 0) const;

  inline REAL Max() const {
    if constexpr (std::is_same_v<REAL, double>) {
      const auto maxIt = std::max_element(std::cbegin(z), std::cend(z));
      if (maxIt != std::cend(z)) { return *maxIt; }
      return -infty;
    } else THROW("Max not implemented")
  }

  REAL MaxAccumulated(int commSplit = 0) const;

  void Accumulate(int commSplit = 0);

  inline void CleanZero() {
    std::replace_if(
        std::begin(z), std::end(z), [](const auto &value) { return mpp_ba::isNear(value, REAL{}); },
        REAL{});
  }

  Saver &save(Saver &saver) const;

  Loader &load(Loader &loader);

  template<typename REAL1>
  inline void Multiply(const REAL &b, const RVectorT<REAL1> &v) {
    if constexpr (DebugLevel > 0) {
      if (z.size() != v.size()) THROW("Size does not fit!")
    }
    std::transform(std::cbegin(v), std::cend(v), std::begin(z),
                   [&b](const auto &value) { return value * b; });
  }

  template<typename REAL1>
  inline void MultiplyPlus(const REAL &b, const RVectorT<REAL1> &v) {
    if constexpr (DebugLevel > 0) {
      if (z.size() != v.size()) THROW("Size does not fit!")
    }
    std::transform(std::cbegin(z), std::cend(z), std::cbegin(v), std::begin(z),
                   [&b](const auto &first, const auto &second) { return first + second * b; });
  }

  template<typename REAL1>
  inline void MultiplyMinus(const REAL &b, const RVectorT<REAL1> &v) {
    if constexpr (DebugLevel > 0) {
      if (z.size() != v.size()) THROW("Size does not fit!")
    }
    std::transform(std::cbegin(z), std::cend(z), std::cbegin(v), std::begin(z),
                   [&b](const auto &first, const auto &second) { return first - second * b; });
  }
};

template<typename REAL>
inline bool operator==(const RVectorT<REAL> &x, const RVectorT<REAL> &y) {
  if (x.size() != y.size()) return false;
  return std::equal(std::cbegin(x), std::cend(x), std::cbegin(y),
                    [](const auto &a, const auto &b) { return mpp_ba::isNear(a, b); });
}

template<typename REAL>
inline bool operator!=(const RVectorT<REAL> &x, const RVectorT<REAL> &y) {
  return !(x == y);
}

template<typename REAL>
inline RVectorT<REAL> operator-(const RVectorT<REAL> &v) {
  RVectorT<REAL> z(v);
  return z *= -1.0;
}

template<typename REAL>
inline RVectorT<REAL> operator+(const RVectorT<REAL> &v, const RVectorT<REAL> &w) {
  RVectorT<REAL> z(v);
  return z += w;
}

template<typename REAL>
inline RVectorT<REAL> operator-(const RVectorT<REAL> &v, const RVectorT<REAL> &w) {
  RVectorT<REAL> z(v);
  return z -= w;
}

template<typename REAL, typename REAL1>
inline mpp_ba::enable_if_constructible<REAL, REAL1, RVectorT<REAL>>
operator*(const REAL1 &b, const RVectorT<REAL> &v) {
  RVectorT<REAL> z(v);
  return z *= b;
}

template<typename REAL, typename REAL1>
inline mpp_ba::enable_if_constructible<REAL, REAL1, RVectorT<REAL>>
operator*(const RVectorT<REAL> &v, const REAL1 &b) {
  RVectorT<REAL> z(v);
  return z *= b;
}

template<typename REAL, typename REAL1>
inline mpp_ba::enable_if_constructible<REAL, REAL1, RVectorT<REAL>>
operator/(const RVectorT<REAL> &v, const REAL1 &b) {
  RVectorT<REAL> z(v);
  return z /= b;
}

template<typename REAL>
inline REAL operator*(const RVectorT<REAL> &v, const RVectorT<REAL> &w) {
  if constexpr (DebugLevel > 0) {
    if (v.size() != w.size()) THROW("Size does not fit!")
  }
  return std::inner_product(std::cbegin(v), std::cend(v), std::cbegin(w), REAL{});
}

template<typename REAL>
inline REAL normSqr(const RVectorT<REAL> &v) {
  return v.normSqr();
}

template<typename REAL>
inline REAL norm(const RVectorT<REAL> &v) {
  return v.norm();
}

template<typename REAL>
inline Saver &operator<<(Saver &saver, const RVectorT<REAL> &v) {
  return v.save(saver);
}

template<typename REAL>
inline Loader &operator>>(Loader &loader, RVectorT<REAL> &v) {
  return v.load(loader);
}

template<typename REAL>
std::ostream &operator<<(std::ostream &os, const RVectorT<REAL> &v) {
  if constexpr (std::is_same_v<REAL, double>) os << beginD;
  for (size_t i = 0; i < v.size(); ++i) {
    if (i == 0) os << v[i];
    else {
      os << "\n" << v[i];
      if constexpr (std::is_same_v<REAL, double>) os << beginD;
    }
  }
  if constexpr (std::is_same_v<REAL, double>) os << endD;
  return os;
}

using RVector = RVectorT<double>;

/// Data vector for fem vectors and matrices
using BasicVector = RVectorT<double>;

#ifdef BUILD_IA

using IARVector = RVectorT<IAInterval>;

RVector mid(const IARVector &);

RVector sup(const IARVector &);

RVector inf(const IARVector &);

inline IARVector operator+(const IARVector &a, const RVector &b) {
  IARVector w(a);
  return w += b;
}

inline IARVector operator+(const RVector &a, const IARVector &b) {
  IARVector w(a);
  return w += b;
}

inline IARVector operator-(const IARVector &a, const RVector &b) {
  IARVector w(a);
  return w -= b;
}

inline IARVector operator-(const RVector &a, const IARVector &b) {
  IARVector w(a);
  return w -= b;
}

inline IAInterval operator*(const IARVector &a, const RVector &b) {
  if (a.size() != b.size()) THROW("Size does not fit!")
  return std::inner_product(std::cbegin(a), std::cend(a), std::cbegin(b), IAInterval());
}

inline IAInterval operator*(const RVector &a, const IARVector &b) {
  if (a.size() != b.size()) THROW("Size does not fit!")
  return std::inner_product(std::cbegin(a), std::cend(a), std::cbegin(b), IAInterval());
}

#endif // BUILD_IA

#endif
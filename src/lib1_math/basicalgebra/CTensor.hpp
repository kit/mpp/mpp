#ifndef MLUQ_CTENSOR_H
#define MLUQ_CTENSOR_H

#include <algorithm>
#include <functional>
#include <initializer_list>
#include <iterator>
#include <numeric>
#include <vector>

#include "CMatrix.hpp"
#include "RTensor.hpp"

template<typename REAL = double>
class CTensorT {
  using COMPLEX = COMPLEX_TYPE<REAL>;
protected:
  std::vector<COMPLEX> z{};
  int NFirstDimension;
  int NSecondDimension;
  int NThirdDimension;
public:
  CTensorT() : z(0, COMPLEX{}), NFirstDimension(0), NSecondDimension(0), NThirdDimension(0) {}

  explicit constexpr CTensorT(int dim) :
      z(dim * dim * dim, COMPLEX{}), NFirstDimension(dim), NSecondDimension(dim),
      NThirdDimension(dim) {}

  explicit constexpr CTensorT(const COMPLEX &b, int dim) :
      z(dim * dim * dim, b), NFirstDimension(dim), NSecondDimension(dim), NThirdDimension(dim) {}

  constexpr CTensorT(int FirstDimension, int SecondDimension, int ThirdDimension) :
      z(FirstDimension * SecondDimension * ThirdDimension, COMPLEX{}),
      NFirstDimension(FirstDimension), NSecondDimension(SecondDimension),
      NThirdDimension(ThirdDimension) {}

  constexpr CTensorT(const COMPLEX &b, int FirstDimension, int SecondDimension,
                     int ThirdDimension) :
      z(FirstDimension * SecondDimension * ThirdDimension, b), NFirstDimension(FirstDimension),
      NSecondDimension(SecondDimension), NThirdDimension(ThirdDimension) {}

  template<typename COMPLEX1>
  CTensorT(const std::initializer_list<std::initializer_list<std::initializer_list<COMPLEX1>>> &v) :
      z(v.begin()->begin()->size() * v.begin()->size() * v.size()),
      NFirstDimension(v.begin()->begin()->size()), NSecondDimension(v.begin()->size()),
      NThirdDimension(v.size()) {
    for (int i = 0; i < NFirstDimension; i++) {
      for (int j = 0; j < NSecondDimension; j++) {
        for (int k = 0; k < NThirdDimension; k++) {
          z[i + j * NFirstDimension + k * NFirstDimension * NSecondDimension] =
              (std::next(std::next(v.begin(), i)->begin(), j)->begin())[k];
        }
      }
    }
  }

  template<typename COMPLEX1>
  CTensorT(const std::vector<std::vector<std::vector<COMPLEX1>>> &v) :
      z(v.begin()->begin()->size() * v.begin()->size() * v.size()),
      NFirstDimension(v.begin()->begin()->size()), NSecondDimension(v.begin()->size()),
      NThirdDimension(v.size()) {
    for (int i = 0; i < NFirstDimension; i++) {
      for (int j = 0; j < NSecondDimension; j++) {
        for (int k = 0; k < NThirdDimension; k++) {
          z[i + j * NFirstDimension + k * NFirstDimension * NSecondDimension] =
              (std::next(std::next(v.begin(), i)->begin(), j)->begin())[k];
        }
      }
    }
  }

  template<typename REAL1>
  explicit CTensorT(const CTensorT<REAL1> &A) :
      z(A.begin(), A.end()), NFirstDimension(A.FirstDimension()),
      NSecondDimension(A.SecondDimension()), NThirdDimension(A.ThirdDimension()) {}

  template<typename REAL1>
  explicit CTensorT(const RTensorT<REAL1> &A) :
      z(A.begin(), A.end()), NFirstDimension(A.FirstDimension()),
      NSecondDimension(A.SecondDimension()), NThirdDimension(A.ThirdDimension()) {}

  template<typename REAL1>
  explicit CTensorT(const CTensorT<REAL1> &A, const CTensorT<REAL1> &B) :
      z(A.begin(), A.end()), NFirstDimension(A.FirstDimension()),
      NSecondDimension(A.SecondDimension()), NThirdDimension(A.ThirdDimension()) {
    if (NFirstDimension != B.FirstDimension() || NSecondDimension != B.SecondDimension()
        || NThirdDimension != B.ThirdDimension())
      THROW("Size does not fit!")
    std::transform(std::cbegin(z), std::cend(z), std::cbegin(B), std::begin(z),
                   std::multiplies<>());
  }

  template<typename REAL1>
  explicit CTensorT(const RTensorT<REAL1> &A, const CTensorT<REAL1> &B) :
      z(B.begin(), B.end()), NFirstDimension(B.FirstDimension()),
      NSecondDimension(B.SecondDimension()), NThirdDimension(B.ThirdDimension()) {
    if (NFirstDimension != A.FirstDimension() || NSecondDimension != A.SecondDimension()
        || NThirdDimension != A.ThirdDimension())
      THROW("Size does not fit!")
    std::transform(std::cbegin(z), std::cend(z), std::cbegin(A), std::begin(z),
                   std::multiplies<>());
  }

  template<typename REAL1>
  explicit CTensorT(const CTensorT<REAL1> &A, const RTensorT<REAL1> &B) :
      z(A.begin(), A.end()), NFirstDimension(A.FirstDimension()),
      NSecondDimension(A.SecondDimension()), NThirdDimension(A.ThirdDimension()) {
    if (NFirstDimension != B.FirstDimension() || NSecondDimension != B.SecondDimension()
        || NThirdDimension != B.ThirdDimension())
      THROW("Size does not fit!")
    std::transform(std::cbegin(z), std::cend(z), std::cbegin(B), std::begin(z),
                   std::multiplies<>());
  }

  template<typename REAL1>
  inline CTensorT &operator=(const CTensorT<REAL1> &A) {
    resize(A.FirstDimension(), A.SecondDimension(), A.ThirdDimension());
    z.assign(std::cbegin(A), std::cend(A));
    return *this;
  }

  inline CTensorT &operator=(const COMPLEX &b) {
    std::fill(std::begin(z), std::end(z), b);
    return *this;
  }

  template<typename REAL1>
  inline CTensorT &operator+=(const CTensorT<REAL1> &A) {
    if (NFirstDimension != A.FirstDimension() || NSecondDimension != A.SecondDimension()
        || NThirdDimension != A.ThirdDimension())
      THROW("Size does not fit!")
    std::transform(std::cbegin(z), std::cend(z), std::cbegin(A), std::begin(z), std::plus<>());
    return *this;
  }

  template<typename REAL1>
  inline CTensorT &operator-=(const CTensorT<REAL1> &A) {
    if (NFirstDimension != A.FirstDimension() || NSecondDimension != A.SecondDimension()
        || NThirdDimension != A.ThirdDimension())
      THROW("Size does not fit!")
    std::transform(std::cbegin(z), std::cend(z), std::cbegin(A), std::begin(z), std::minus<>());
    return *this;
  }

  inline CTensorT &operator*=(const COMPLEX &b) {
    std::transform(std::cbegin(z), std::cend(z), std::begin(z),
                   [&b](const auto &value) { return value * b; });
    return *this;
  }

  inline CTensorT &operator/=(const COMPLEX &b) {
    std::transform(std::cbegin(z), std::cend(z), std::begin(z),
                   [&b](const auto &value) { return value / b; });
    return *this;
  }

  // Todo: Next to operators are only a draft

  inline auto operator[](int i) { return std::next(z.begin(), i * NFirstDimension); } //????

  inline auto operator[](int i) const { return std::next(z.begin(), i * NFirstDimension); } //????

  inline COMPLEX &operator()(int FirstComponent, int SecondComponent, int ThirdComponent) {
    return z[FirstComponent + SecondComponent * NFirstDimension
             + ThirdComponent * NFirstDimension * NSecondDimension];
  }

  // Todo: Arguments need to be refactored. Is it useful to have these functions?

  inline CMatrixT<REAL> FirstComponent(int i) const {
    CMatrixT<REAL> c(NSecondDimension, NThirdDimension);
    for (int j = 0; j < NSecondDimension; ++j) {
      for (int k = 0; k < NThirdDimension; ++k) {
        c(j, k) = z[i + j * NFirstDimension + k * NFirstDimension * NSecondDimension];
      }
    }
    return c;
  }

  inline CMatrixT<REAL> SecondComponent(int j) const {
    CMatrixT<REAL> c(NFirstDimension, NThirdDimension);
    for (int i = 0; i < NFirstDimension; ++i) {
      for (int k = 0; k < NThirdDimension; ++k) {
        c(i, k) = z[i + j * NFirstDimension + k * NFirstDimension * NSecondDimension];
      }
    }
    return c;
  }

  inline CMatrixT<REAL> ThirdComponent(int k) const {
    CMatrixT<REAL> c(NFirstDimension, NSecondDimension);
    for (int i = 0; i < NFirstDimension; ++i) {
      for (int j = 0; j < NSecondDimension; ++j) {
        c(i, j) = z[i + j * NFirstDimension + k * NFirstDimension * NSecondDimension];
      }
    }
    return c;
  }

  template<typename REAL1>
  void InsertMatrixThirdDimension(const CMatrixT<REAL1> &v, int ThirdComponent, int startFirst = 0,
                                  int startSecond = 0) {
    if (ThirdComponent >= NThirdDimension || startFirst + v.cols() > NFirstDimension
        || startSecond + v.rows() > NSecondDimension) {
      THROW("Cannot insert Matrix: size of v too large")
    }
    for (int i = 0; i < v.cols(); ++i) {
      for (int j = 0; j < v.rows(); ++j) {
        z[i + startFirst + (j + startSecond) * NFirstDimension
          + ThirdComponent * NFirstDimension * NSecondDimension] = v(i, j);
      }
    }
  }

  inline int size() const noexcept { return z.size(); }

  inline int FirstDimension() const noexcept { return NFirstDimension; }

  inline int SecondDimension() const noexcept { return NSecondDimension; }

  inline int ThirdDimension() const noexcept { return NThirdDimension; }

  inline auto begin() const noexcept { return z.begin(); }

  inline auto begin() noexcept { return z.begin(); }

  inline auto end() const noexcept { return z.end(); }

  inline auto end() noexcept { return z.end(); }

  inline void resize(int FirstComponents, int SecondComponents, int ThirdComponents) {
    z = std::vector<COMPLEX>(FirstComponents * SecondComponents * ThirdComponents, COMPLEX{});
    NFirstDimension = FirstComponents;
    NSecondDimension = SecondComponents;
    NThirdDimension = ThirdComponents;
  }

  inline void resize(int dim) { resize(dim, dim, dim); }

  inline const std::vector<COMPLEX> &Data() const noexcept { return z; }

  inline std::vector<COMPLEX> &Data() noexcept { return z; }

  RTensorT<REAL> real() const;

  RTensorT<REAL> imag() const;

  inline COMPLEX Mean() const {
    COMPLEX sum = std::accumulate(std::cbegin(z), std::cend(z), COMPLEX{});
    return sum / z.size();
  }

  REAL Variance() const;
};

template<typename REAL>
inline CTensorT<REAL> operator-(const CTensorT<REAL> &a) {
  CTensorT<REAL> c(a);
  return c *= -1;
}

template<typename REAL>
inline CTensorT<REAL> operator+(const CTensorT<REAL> &a, const CTensorT<REAL> &b) {
  CTensorT<REAL> c(a);
  return c += b;
}

template<typename REAL>
inline CTensorT<REAL> operator+(const CTensorT<REAL> &a, const RTensorT<REAL> &b) {
  CTensorT<REAL> c(b);
  return c += a;
}

template<typename REAL>
inline CTensorT<REAL> operator+(const RTensorT<REAL> &a, const CTensorT<REAL> &b) {
  CTensorT<REAL> c(a);
  return c += b;
}

template<typename REAL>
inline CTensorT<REAL> operator-(const CTensorT<REAL> &a, const CTensorT<REAL> &b) {
  CTensorT<REAL> c(a);
  return c -= b;
}

template<typename REAL>
inline CTensorT<REAL> operator-(const CTensorT<REAL> &a, const RTensorT<REAL> &b) {
  CTensorT<REAL> c(a);
  CTensorT<REAL> d(b);
  return c -= d;
}

template<typename REAL>
inline CTensorT<REAL> operator-(const RTensorT<REAL> &a, const CTensorT<REAL> &b) {
  CTensorT<REAL> c(a);
  return c -= b;
}

template<typename REAL> // elementwise multiplication
inline CTensorT<REAL> operator*(const CTensorT<REAL> &a, const CTensorT<REAL> &b) {
  return CTensorT<REAL>(a, b);
}

template<typename REAL> // elementwise multiplication
inline CTensorT<REAL> operator*(const CTensorT<REAL> &a, const RTensorT<REAL> &b) {
  return CTensorT<REAL>(a, b);
}

template<typename REAL> // elementwise multiplication
inline CTensorT<REAL> operator*(const RTensorT<REAL> &a, const CTensorT<REAL> &b) {
  return CTensorT<REAL>(a, b);
}

template<typename REAL, typename COMPLEX1>
inline CTensorT<REAL> operator*(const CTensorT<REAL> &a, const COMPLEX1 &b) {
  CTensorT<REAL> c(a);
  return c *= b;
}

template<typename REAL, typename COMPLEX1>
inline CTensorT<REAL> operator*(const COMPLEX1 &b, const CTensorT<REAL> &a) {
  CTensorT<REAL> c(a);
  return c *= b;
}

template<typename REAL, typename COMPLEX1>
inline CTensorT<REAL> operator/(const CTensorT<REAL> &a, const COMPLEX1 &b) {
  CTensorT<REAL> c(a);
  return c /= b;
}

template<typename REAL>
inline bool operator==(const CTensorT<REAL> &A, const CTensorT<REAL> &B) {
  if (A.FirstDimension() != B.FirstDimension() || A.SecondDimension() != B.SecondDimension()
      || A.ThirdDimension() != B.ThirdDimension())
    return false;
  return (A.real() == B.real()) && (A.imag() == B.imag());
}

template<typename REAL>
inline bool operator==(const CTensorT<REAL> &A, const RTensorT<REAL> &B) {
  return A == CTensorT<REAL>(B);
}

template<typename REAL>
inline bool operator==(const RTensorT<REAL> &A, const CTensorT<REAL> &B) {
  return CTensorT<REAL>(A) == B;
}

using CTensor = CTensorT<double>;

#endif // MLUQ_CTENSOR_HPP

#include "AntisymRMatrix.hpp"

#include <format>
#include <string>

#include "Parallel.hpp"
#include "RMatrix.hpp"
#include "SaveLoad.hpp"

template<>
AntisymRMatrixT<double> &AntisymRMatrixT<double>::Invert() {
  if (dim % 2 == 1) Exit(std::format("Antisymmetric matrix not invertible for dim= {}", dim));

  double a[dim * dim];
  for (int i = 0; i < dim; ++i)
    for (int j = 0; j < dim; ++j)
      a[i * dim + j] = (*this)(i, j);

  int I[dim];
  int info;
  int lwork = 2 * dim;
  double work[2 * lwork];
  dgetrf_(&dim, &dim, a, &dim, I, &info);
  if (info < 0) {
    THROW(std::format("Error in LAPACK: {}-th argument had an illegal value", -info))
  } else if (info > 0) {
    THROW(std::format("Error in LAPACK: A({},{} is exactly zero.", -info, -info))
  }
  dgetri_(&dim, a, &dim, I, work, &lwork, &info);
  if (info < 0) {
    THROW(std::format("Error in LAPACK: {}-th argument had an illegal value", -info))
  } else if (info > 0) {
    THROW(std::format("Error in LAPACK: A({},{} is exactly zero.", -info, -info))
  }

  for (int i = 1; i < dim; ++i)
    for (int j = 0; j < i; ++j)
      (*this)(a[i * dim + j], i, j);

  return *this;
}

template<typename REAL>
void AntisymRMatrixT<REAL>::Accumulate(int commSplit) {
  PPM->SumOnCommSplit(z, commSplit);
}

template<typename REAL>
Saver &AntisymRMatrixT<REAL>::save(Saver &saver) const {
  saver << Dim();
  for (const auto &element : z) {
    saver << element;
  }
  return saver;
}

template<typename REAL>
Loader &AntisymRMatrixT<REAL>::load(Loader &loader) {
  int N;
  loader >> N;
  resize(N);
  for (auto &element : z) {
    loader >> element;
  }
  return loader;
}

template class AntisymRMatrixT<double>;

#ifdef BUILD_IA

template class AntisymRMatrixT<IAInterval>;

AntisymRMatrix mid(const IAAntisymRMatrix &IA_A) {
  AntisymRMatrix A(IA_A.Dim());
  for (int i = 0; i < A.rows(); ++i)
    for (int j = 0; j < A.cols(); ++j)
      A(mid(IA_A(i, j)), i, j);
  return A;
}

RMatrix sup(const IAAntisymRMatrix &IA_A) {
  RMatrix A(IA_A.Dim());
  for (int i = 0; i < A.rows(); ++i)
    for (int j = 0; j < A.cols(); ++j)
      A[i][j] = sup(IA_A(i, j));
  return A;
}

RMatrix inf(const IAAntisymRMatrix &IA_A) {
  RMatrix A(IA_A.Dim());
  for (int i = 0; i < A.rows(); ++i)
    for (int j = 0; j < A.cols(); ++j)
      A[i][j] = inf(IA_A(i, j));
  return A;
}

#endif // BUILD_IA
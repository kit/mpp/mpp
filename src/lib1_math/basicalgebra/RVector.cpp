#include "RVector.hpp"

#include <algorithm>
#include <cstddef>
#include <iterator>

#include "Parallel.hpp"
#include "SaveLoad.hpp"

namespace mpp_ba {
double tolerance = 1e-12;

void SetTolerance(double tol) { tolerance = tol; }

double GetTolerance() { return tolerance; }

bool isNear(double a, double b) { return abs(a - b) < tolerance; }

bool isNearZero(double a) { return abs(a) < tolerance; }

#ifdef BUILD_IA

bool isNear(const IAInterval &a, const IAInterval &b) {
  return (abs(inf(a) - inf(b)) < tolerance) && (abs(sup(a) - sup(b)) < tolerance);
}

bool isNearZero(const IAInterval &a) {
  return (abs(inf(a)) < tolerance) && (abs(sup(a)) < tolerance);
}

#endif
} // namespace mpp_ba

template<typename REAL>
REAL RVectorT<REAL>::norm() const {
  return sqrt(normSqr());
}

template<typename REAL>
REAL RVectorT<REAL>::normAccumulated(int commSplit) const {
  return sqrt(PPM->SumOnCommSplit(normSqr(), commSplit));
}

template<typename REAL>
REAL RVectorT<REAL>::MinAccumulated(int commSplit) const {
  if constexpr (std::is_same_v<REAL, double>) {
    return PPM->Min(Min(), commSplit);
  } else THROW("Min not implemented")
}

template<typename REAL>
REAL RVectorT<REAL>::MaxAccumulated(int commSplit) const {
  if constexpr (std::is_same_v<REAL, double>) {
    return PPM->Max(Max(), commSplit);
  } else THROW("Max not implemented")
}

template<typename REAL>
void RVectorT<REAL>::Accumulate(int commSplit) {
  PPM->SumOnCommSplit(z, commSplit);
}

template<typename REAL>
Saver &RVectorT<REAL>::save(Saver &saver) const {
  saver << size();
  for (const auto &element : z) {
    saver << element;
  }
  return saver;
}

template<typename REAL>
Loader &RVectorT<REAL>::load(Loader &loader) {
  size_t N;
  loader >> N;
  resize(N);
  for (auto &element : z) {
    loader >> element;
  }
  return loader;
}

template<typename REAL>
REAL RVectorT<REAL>::Variance() const {
  REAL m2{};
  REAL diff{};
  REAL correction{};
  REAL mean = this->Mean();
  for (auto const &value : z) {
    diff = (value - mean);
    m2 += diff * diff;
    correction += diff;
  }
  return (m2 - correction * correction / z.size()) / (z.size() - 1);
}

template class RVectorT<double>;

#ifdef BUILD_IA

template class RVectorT<IAInterval>;

RVector mid(const IARVector &IA_v) {
  RVector v(IA_v.size());
  std::transform(std::cbegin(IA_v), std::cend(IA_v), std::begin(v),
                 [](const auto &value) { return mid(value); });
  return v;
}

RVector sup(const IARVector &IA_v) {
  RVector v(IA_v.size());
  std::transform(std::cbegin(IA_v), std::cend(IA_v), std::begin(v),
                 [](const auto &value) { return sup(value); });
  return v;
}

RVector inf(const IARVector &IA_v) {
  RVector v(IA_v.size());
  std::transform(std::cbegin(IA_v), std::cend(IA_v), std::begin(v),
                 [](const auto &value) { return inf(value); });
  return v;
}

#endif // BUILD_IA
#include "CVector.hpp"

#include <algorithm>
#include <cmath>
#include <iterator>
#include <numeric>

#include "CFunctions.hpp"
#include "Parallel.hpp"
#include "RVector.hpp"
#include "SaveLoad.hpp"

template<typename REAL>
CVectorT<REAL> &CVectorT<REAL>::conj() {
  std::transform(std::cbegin(z), std::cend(z), std::begin(z),
                 [](const auto &value) { return baConj(value); });
  return *this;
}

template<typename REAL>
RVectorT<REAL> CVectorT<REAL>::real() const {
  RVectorT<REAL> re(z.size());
  std::transform(std::cbegin(z), std::cend(z), std::begin(re),
                 [](const auto &value) { return baReal(value); });
  return re;
}

template<typename REAL>
RVectorT<REAL> CVectorT<REAL>::imag() const {
  RVectorT<REAL> im(z.size());
  std::transform(std::cbegin(z), std::cend(z), std::begin(im),
                 [](const auto &value) { return baImag(value); });
  return im;
}

template<typename REAL>
REAL CVectorT<REAL>::normSqr() const {
  return std::accumulate(std::cbegin(z), std::cend(z), REAL{},
                         [](const auto &first, const auto &second) {
                           return first + baAbsSqr(second);
                         });
}

template<typename REAL>
REAL CVectorT<REAL>::norm() const {
  return sqrt(normSqr());
}

template<typename REAL>
void CVectorT<REAL>::Accumulate(int commSplit) {
  PPM->SumOnCommSplit(z, commSplit);
}

template<typename REAL>
Saver &CVectorT<REAL>::save(Saver &saver) const {
  saver << size();
  for (const auto &element : z) {
    saver << element;
  }
  return saver;
}

template<typename REAL>
Loader &CVectorT<REAL>::load(Loader &loader) {
  int N;
  loader >> N;
  resize(N);
  for (auto &element : z) {
    loader >> element;
  }
  return loader;
}

template<typename REAL>
REAL CVectorT<REAL>::Variance() const {
  REAL var_real = real().Variance();
  REAL var_imag = imag().Variance();
  return var_real + var_imag;
}

template class CVectorT<double>;

#ifdef BUILD_IA

template class CVectorT<IAInterval>;

CVector mid(const IACVector &IA_v) {
  CVector v(IA_v.size());
  std::transform(std::cbegin(IA_v), std::cend(IA_v), std::begin(v),
                 [](const auto &value) { return mid(value); });
  return v;
}

#endif // BUILD_IA
#ifndef CVECTOR_H
#define CVECTOR_H

#include "RVector.hpp"

#include <algorithm>
#include <complex>
#include <functional>
#include <iterator>
#include <numeric>
#include <vector>

#ifdef BUILD_IA

#include "IACInterval.hpp"

template<typename T>
using COMPLEX_TYPE = typename std::conditional<
    std::is_same_v<T, double>, std::complex<double>,
    typename std::conditional<std::is_same_v<T, IAInterval>, IACInterval, void>::type>::type;

#else

template<typename T>
using COMPLEX_TYPE =
    typename std::conditional<std::is_same_v<T, double>, std::complex<double>, void>::type;

#endif

namespace std {
inline std::complex<double> operator*(const std::complex<double> &a, int b) {
  return a * double(b);
}

inline std::complex<double> operator*(int a, const std::complex<double> &b) {
  return double(a) * b;
}

inline std::complex<double> operator/(const std::complex<double> &a, int b) {
  return a / double(b);
}
} // namespace std

template<typename REAL = double>
class CVectorT {
  using COMPLEX = COMPLEX_TYPE<REAL>;
protected:
  std::vector<COMPLEX> z;
public:
  CVectorT() = default;

  explicit CVectorT(int length) : z(length, 0.0) {}

  template<typename REAL1>
  explicit CVectorT(const CVectorT<REAL1> &v) : z(std::cbegin(v), std::cend(v)) {}

  template<typename REAL1>
  explicit CVectorT(const RVectorT<REAL1> &v) : z(std::cbegin(v), std::cend(v)) {}

  template<typename COMPLEX1>
  CVectorT(const std::vector<COMPLEX1> &v) : z(std::cbegin(v), std::cend(v)) {}

  template<typename T>
  constexpr CVectorT(const T &a, int length) : z(length, a) {}

  template<typename T>
  inline CVectorT &operator=(const T &a) {
    std::fill(std::begin(z), std::end(z), a);
    return *this;
  }

  template<typename REAL1>
  inline CVectorT &operator=(const CVectorT<REAL1> &v) {
    z.assign(std::cbegin(v), std::cend(v));
    return *this;
  }

  template<typename REAL1>
  inline CVectorT &operator=(const RVectorT<REAL1> &v) {
    z = std::vector<COMPLEX>(std::cbegin(v), std::cend(v));
    return *this;
  }

  template<typename T>
  inline CVectorT &operator+=(const T &a) {
    for (auto &element : z) {
      element += a;
    }
    return *this;
  }

  template<typename REAL1>
  inline CVectorT &operator+=(const CVectorT<REAL1> &v) {
    if (z.size() != v.size()) THROW("Size does not fit!")
    std::transform(std::cbegin(z), std::cend(z), std::cbegin(v), std::begin(z), std::plus<>());
    return *this;
  }

  template<typename REAL1>
  inline CVectorT &operator+=(const RVectorT<REAL1> &v) {
    if (z.size() != v.size()) THROW("Size does not fit!")
    std::transform(std::cbegin(z), std::cend(z), std::cbegin(v), std::begin(z), std::plus<>());
    return *this;
  }

  template<typename T>
  inline CVectorT &operator-=(const T &a) {
    for (auto &element : z) {
      element -= a;
    }
    return *this;
  }

  template<typename REAL1>
  inline CVectorT &operator-=(const CVectorT<REAL1> &v) {
    if (z.size() != v.size()) THROW("Size does not fit!")
    std::transform(std::cbegin(z), std::cend(z), std::cbegin(v), std::begin(z), std::minus<>());
    return *this;
  }

  template<typename REAL1>
  inline CVectorT &operator-=(const RVectorT<REAL1> &v) {
    if (z.size() != v.size()) THROW("Size does not fit!")
    std::transform(std::cbegin(z), std::cend(z), std::cbegin(v), std::begin(z), std::minus<>());
    return *this;
  }

  template<typename T>
  inline CVectorT &operator*=(const T &a) {
    std::transform(std::cbegin(z), std::cend(z), std::begin(z),
                   [a](const auto &value) { return value * a; });
    return *this;
  }

  template<typename REAL1>
  inline CVectorT &operator*=(const RVectorT<REAL1> &v) {
    std::transform(std::cbegin(z), std::cend(z), std::cbegin(v), std::begin(z),
                   std::multiplies<>());
    return *this;
  }

  // This a component wise operation
  template<typename REAL1>
  inline CVectorT &operator*=(const CVectorT<REAL1> &v) {
    std::transform(std::cbegin(z), std::cend(z), std::cbegin(v), std::begin(z),
                   std::multiplies<>());
    return *this;
  }

  template<typename T>
  inline CVectorT &operator/=(const T &a) {
    std::transform(std::cbegin(z), std::cend(z), std::begin(z),
                   [a](const auto &value) { return value / a; });
    return *this;
  }

  inline const std::vector<COMPLEX> &asVector() const noexcept { return z; }

  inline std::vector<COMPLEX> &asVector() noexcept { return z; }

  inline int Dim() const noexcept { return z.size(); }

  inline int size() const noexcept { return z.size(); }

  inline void resize(int N) { z.resize(N); }

  inline COMPLEX &operator[](int i) { return z[i]; }

  inline const COMPLEX &operator[](int i) const { return z[i]; }

  inline auto begin() const noexcept { return z.begin(); }

  inline auto begin() noexcept { return z.begin(); }

  inline auto rbegin() const noexcept { return z.rbegin(); }

  inline auto end() const noexcept { return z.end(); }

  inline auto end() noexcept { return z.end(); }

  inline auto rend() const noexcept { return z.rend(); }

  inline auto push_back(const COMPLEX &a) { return z.push_back(a); }

  inline void clear() noexcept { z.clear(); }

  inline auto append(const CVectorT &v) {
    return z.insert(std::end(z), std::cbegin(v), std::cend(v));
  }

  CVectorT &conj();

  RVectorT<REAL> real() const;

  RVectorT<REAL> imag() const;

  REAL normSqr() const;

  REAL norm() const;

  inline COMPLEX Mean() const {
    COMPLEX sum = std::accumulate(std::cbegin(z), std::cend(z), COMPLEX{});
    return sum / z.size();
  }

  REAL Variance() const;

  void Accumulate(int commSplit = 0);

  Saver &save(Saver &saver) const;

  Loader &load(Loader &loader);
};

template<typename REAL>
inline bool operator==(const CVectorT<REAL> &x, const CVectorT<REAL> &y) {
  if (x.size() != y.size()) return false;
  return (x.real() == y.real()) && (x.imag() == y.imag());
}

template<typename REAL, typename REAL1>
inline bool operator==(const CVectorT<REAL> &x, const RVectorT<REAL1> &y) {
  return x == CVectorT<REAL>(y);
}

template<typename REAL, typename REAL1>
inline bool operator==(const RVectorT<REAL1> &x, const CVectorT<REAL> &y) {
  return CVectorT<REAL>(x) == y;
}

template<typename REAL>
inline bool operator!=(const CVectorT<REAL> &x, const CVectorT<REAL> &y) {
  return !(x == y);
}

template<typename REAL, typename REAL1>
inline bool operator!=(const CVectorT<REAL> &x, const RVectorT<REAL1> &y) {
  return !(x == y);
}

template<typename REAL, typename REAL1>
inline bool operator!=(const RVectorT<REAL1> &x, const CVectorT<REAL> &y) {
  return !(x == y);
}

template<typename REAL>
inline CVectorT<REAL> operator-(const CVectorT<REAL> &v) {
  CVectorT<REAL> z(v);
  return z *= -1.0;
}

template<typename REAL>
inline CVectorT<REAL> operator+(const CVectorT<REAL> &v, const CVectorT<REAL> &w) {
  CVectorT<REAL> z(v);
  return z += w;
}

template<typename REAL>
inline CVectorT<REAL> operator+(const CVectorT<REAL> &v, const RVectorT<REAL> &w) {
  CVectorT<REAL> z(v);
  return z += w;
}

template<typename REAL>
inline CVectorT<REAL> operator+(const RVectorT<REAL> &v, const CVectorT<REAL> &w) {
  CVectorT<REAL> z(v);
  return z += w;
}

template<typename REAL>
inline CVectorT<REAL> operator-(const CVectorT<REAL> &v, const CVectorT<REAL> &w) {
  CVectorT<REAL> z(v);
  return z -= w;
}

template<typename REAL>
inline CVectorT<REAL> operator-(const CVectorT<REAL> &v, const RVectorT<REAL> &w) {
  CVectorT<REAL> z(v);
  return z -= w;
}

template<typename REAL>
inline CVectorT<REAL> operator-(const RVectorT<REAL> &v, const CVectorT<REAL> &w) {
  CVectorT<REAL> z(v);
  return z -= w;
}

template<typename REAL, typename REAL1>
inline CVectorT<REAL> operator*(const REAL1 &b, const CVectorT<REAL> &v) {
  CVectorT<REAL> z(v);
  return z *= b;
}

template<typename REAL, typename REAL1>
inline CVectorT<REAL> operator*(const CVectorT<REAL> &v, const REAL1 &b) {
  CVectorT<REAL> z(v);
  return z *= b;
}

template<typename REAL, typename REAL1>
inline CVectorT<REAL> operator/(const CVectorT<REAL> &v, const REAL1 &b) {
  CVectorT<REAL> z(v);
  return z /= b;
}

template<typename REAL>
inline COMPLEX_TYPE<REAL> operator*(const CVectorT<REAL> &v, const CVectorT<REAL> &w) {
  if (v.size() != w.size()) THROW("Size does not fit!")
  return std::inner_product(std::cbegin(v), std::cend(v), std::cbegin(w), COMPLEX_TYPE<REAL>{},
                            std::plus<>(), [](const auto &first, const auto &second) {
                              return first * conj(second);
                            });
}

template<typename REAL>
inline COMPLEX_TYPE<REAL> operator*(const CVectorT<REAL> &v, const RVectorT<REAL> &w) {
  if (v.size() != w.size()) THROW("Size does not fit!")
  return std::inner_product(std::cbegin(v), std::cend(v), std::cbegin(w), COMPLEX_TYPE<REAL>{},
                            std::plus<>(), std::multiplies<>());
}

template<typename REAL>
inline COMPLEX_TYPE<REAL> operator*(const RVectorT<REAL> &v, const CVectorT<REAL> &w) {
  return std::inner_product(std::cbegin(v), std::cend(v), std::cbegin(w), COMPLEX_TYPE<REAL>{},
                            std::plus<>(), [](const auto &first, const auto &second) {
                              return first * conj(second);
                            });
}

template<typename REAL>
inline CVectorT<REAL> conj(const CVectorT<REAL> &v) {
  CVectorT<REAL> w(v);
  return w.conj();
}

template<typename REAL>
inline RVectorT<REAL> real(const CVectorT<REAL> &v) {
  return v.real();
}

template<typename REAL>
inline RVectorT<REAL> imag(const CVectorT<REAL> &v) {
  return v.imag();
}

template<typename REAL>
inline REAL normSqr(const CVectorT<REAL> &v) {
  return v.normSqr();
}

template<typename REAL>
inline REAL norm(const CVectorT<REAL> &v) {
  return v.norm();
}

template<typename REAL>
inline Saver &operator<<(Saver &saver, const CVectorT<REAL> &v) {
  return v.save(saver);
}

template<typename REAL>
inline Loader &operator>>(Loader &loader, CVectorT<REAL> &v) {
  return v.load(loader);
}

template<typename REAL>
inline std::ostream &operator<<(std::ostream &os, const CVectorT<REAL> &v) {
  for (int i = 0; i < v.size(); ++i) {
    if (i == 0) os << v[i];
    else os << "\n" << v[i];
  }
  return os;
}

using CVector = CVectorT<>;

#ifdef BUILD_IA

using IACVector = CVectorT<IAInterval>;

CVector mid(const IACVector &);

inline IACVector operator+(const IACVector &v, const CVector &w) {
  IACVector z(v);
  return z += w;
}

inline IACVector operator+(const CVector &v, const IACVector &w) {
  IACVector z(v);
  return z += w;
}

inline IACVector operator+(const IACVector &v, const RVector &w) {
  IACVector z(v);
  return z += w;
}

inline IACVector operator+(const RVector &v, const IACVector &w) {
  IACVector z(v);
  return z += w;
}

inline IACVector operator-(const IACVector &v, const CVector &w) {
  IACVector z(v);
  return z -= w;
}

inline IACVector operator-(const CVector &v, const IACVector &w) {
  IACVector z(v);
  return z -= w;
}

inline IACVector operator-(const IACVector &v, const RVector &w) {
  IACVector z(v);
  return z -= w;
}

inline IACVector operator-(const RVector &v, const IACVector &w) {
  IACVector z(v);
  return z -= w;
}

inline IACInterval operator*(const IACVector &v, const CVector &w) {
  if (v.size() != w.size()) THROW("Size does not fit!")
  return std::inner_product(std::cbegin(v), std::cend(v), std::cbegin(w), IACInterval(),
                            std::plus<>(), [](const auto &first, const auto &second) {
                              return first * conj(second);
                            });
}

inline IACInterval operator*(const CVector &v, const IACVector &w) {
  if (v.size() != w.size()) THROW("Size does not fit!")
  return std::inner_product(std::cbegin(v), std::cend(v), std::cbegin(w), IACInterval(),
                            std::plus<>(), [](const auto &first, const auto &second) {
                              return first * conj(second);
                            });
}

inline IACInterval operator*(const CVector &v, const IARVector &w) {
  if (v.size() != w.size()) THROW("Size does not fit!")
  return std::inner_product(std::cbegin(v), std::cend(v), std::cbegin(w), IACInterval(),
                            std::plus<>(), [](const auto &first, const auto &second) {
                              return IACInterval(first) * second;
                            });
}

inline IACInterval operator*(const IACVector &v, const RVector &w) {
  if (v.size() != w.size()) THROW("Size does not fit!")
  return std::inner_product(std::cbegin(v), std::cend(v), std::cbegin(w), IACInterval());
}

inline IACInterval operator*(const RVector &v, const IACVector &w) {
  if (v.size() != w.size()) THROW("Size does not fit!")
  return std::inner_product(std::cbegin(v), std::cend(v), std::cbegin(w), IACInterval(),
                            std::plus<>(), [](const auto &first, const auto &second) {
                              return first * conj(second);
                            });
}

#endif // BUILD_IA

#endif

#include "CTensor.hpp"
#include "CFunctions.hpp"
#include "RTensor.hpp"

#include <algorithm>
#include <iterator>
#include <vector>

template<typename REAL>
RTensorT<REAL> CTensorT<REAL>::real() const {
  RTensorT<REAL> re(NFirstDimension, NSecondDimension, NThirdDimension);
  std::transform(std::cbegin(z), std::cend(z), std::begin(re.Data()),
                 [](const auto &value) { return baReal(value); });
  return re;
}

template<typename REAL>
RTensorT<REAL> CTensorT<REAL>::imag() const {
  RTensorT<REAL> im(NFirstDimension, NSecondDimension, NThirdDimension);
  std::transform(std::cbegin(z), std::cend(z), std::begin(im.Data()),
                 [](const auto &value) { return baImag(value); });
  return im;
}

template<typename REAL>
REAL CTensorT<REAL>::Variance() const {
  REAL var_real = real().Variance();
  REAL var_imag = imag().Variance();
  return var_real + var_imag;
}

template class CTensorT<double>;

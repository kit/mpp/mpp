#ifndef SYMRMATRIX_H
#define SYMRMATRIX_H

#include <algorithm>
#include <functional>
#include <iterator>
#include <vector>

#include "RVector.hpp"

template<typename REAL = double>
class SymRMatrixT {
  std::vector<REAL> z;
  int dim;
public:
  SymRMatrixT() : dim(0) {}

  explicit SymRMatrixT(int dim) : dim(dim), z((dim * (dim + 1)) / 2, REAL{}) {}

  template<typename REAL1>
  constexpr SymRMatrixT(const REAL1 &a, int dim) : dim(dim), z((dim * (dim + 1)) / 2, a) {}

  template<typename REAL1>
  explicit SymRMatrixT(const SymRMatrixT<REAL1> &A) : dim(A.Dim()), z(A.size()) {
    z.assign(std::cbegin(A), std::cend(A));
  }

  template<typename REAL1>
  inline SymRMatrixT &operator=(const REAL1 &a) {
    std::fill(std::begin(z), std::end(z), a);
    return *this;
  }

  template<typename REAL1>
  inline SymRMatrixT &operator=(const SymRMatrixT<REAL1> &A) {
    dim = A.Dim();
    z.assign(std::cbegin(A), std::cend(A));
    return *this;
  }

  template<typename REAL1>
  inline SymRMatrixT &operator+=(const SymRMatrixT<REAL1> &A) {
    if (dim != A.Dim()) THROW("Size does not fit!")
    std::transform(std::cbegin(z), std::cend(z), std::cbegin(A), std::begin(z), std::plus<>());
    return *this;
  }

  template<typename REAL1>
  inline SymRMatrixT &operator-=(const SymRMatrixT<REAL1> &A) {
    if (dim != A.Dim()) THROW("Size does not fit!")
    std::transform(std::cbegin(z), std::cend(z), std::cbegin(A), std::begin(z), std::minus<>());
    return *this;
  }

  template<typename REAL1>
  inline SymRMatrixT &operator*=(const REAL1 &a) {
    std::transform(std::cbegin(z), std::cend(z), std::begin(z),
                   [a](const auto &value) { return value * a; });
    return *this;
  }

  template<typename REAL1>
  inline SymRMatrixT &operator/=(const REAL1 &a) {
    std::transform(std::cbegin(z), std::cend(z), std::begin(z),
                   [a](const auto &value) { return value / a; });
    return *this;
  }

  inline RVectorT<REAL> row(int i) const {
    RVectorT<REAL> r(dim);
    for (int j = 0; j < dim; ++j)
      r[j] = (*this)(i, j);
    return r;
  }

  inline RVectorT<REAL> col(int j) const {
    RVectorT<REAL> c(dim);
    for (int i = 0; i < dim; ++i)
      c[i] = (*this)(i, j);
    return c;
  }

  inline int size() const noexcept { return z.size(); }

  inline int Dim() const noexcept { return dim; }

  inline int rows() const noexcept { return dim; }

  inline int cols() const noexcept { return dim; }

  inline REAL &operator()(int i, int j) {
    if (i >= j) return z[(i * (i + 1)) / 2 + j];
    else return z[(j * (j + 1)) / 2 + i];
  }

  inline const REAL &operator()(int i, int j) const {
    if (i >= j) return z[(i * (i + 1)) / 2 + j];
    else return z[(j * (j + 1)) / 2 + i];
  }

  inline REAL &operator[](int i) { return z[i]; }

  inline const REAL &operator[](int i) const { return z[i]; }

  template<typename REAL1>
  inline void operator()(const REAL1 &a, int i, int j) {
    if (i >= j) z[(i * (i + 1)) / 2 + j] = a;
    else z[(j * (j + 1)) / 2 + i] = a;
  }

  inline auto begin() const noexcept { return z.begin(); }

  inline auto begin() noexcept { return z.begin(); }

  inline auto end() const noexcept { return z.end(); }

  inline auto end() noexcept { return z.end(); }

  template<typename REAL1>
  inline RVectorT<REAL> multiplyWith(const RVectorT<REAL1> &v) const {
    if (v.Dim() != dim) THROW("Dimension does not fit!")
    RVectorT<REAL> y(dim);
    for (int n = 0; n < dim; ++n)
      for (int k = 0; k < dim; ++k)
        y[n] += (*this)(n, k) * v[k];
    return y;
  }

  inline void resize(int dim) {
    z.resize((dim * (dim + 1)) / 2);
    this->dim = dim;
  }

  void Accumulate(int commSplit = 0);

  inline SymRMatrixT<REAL> &Identity() {
    std::fill(std::begin(z), std::end(z), REAL{});
    for (int i = 0; i < dim; i++)
      (*this)(i, 1) = REAL(1);
    return *this;
  }

  SymRMatrixT<REAL> &Invert();

  Saver &save(Saver &saver) const;

  Loader &load(Loader &loader);
};

template<typename REAL>
inline bool operator==(const SymRMatrixT<REAL> &A, const SymRMatrixT<REAL> &B) {
  if (A.Dim() != B.Dim()) return false;
  return std::equal(std::cbegin(A), std::cend(A), std::cbegin(B),
                    [](const auto &a, const auto &b) { return mpp_ba::isNear(a, b); });
  return true;
}

template<typename REAL>
inline bool operator!=(const SymRMatrixT<REAL> &A, const SymRMatrixT<REAL> &B) {
  return !(A == B);
}

template<typename REAL>
inline SymRMatrixT<REAL> operator+(const SymRMatrixT<REAL> &A, const SymRMatrixT<REAL> &B) {
  SymRMatrixT<REAL> AB(A);
  return AB += B;
}

template<typename REAL>
inline SymRMatrixT<REAL> operator-(const SymRMatrixT<REAL> &A, const SymRMatrixT<REAL> &B) {
  SymRMatrixT<REAL> AB(A);
  return AB -= B;
}

template<typename REAL>
inline SymRMatrixT<REAL> operator-(const SymRMatrixT<REAL> &A) {
  SymRMatrixT<REAL> B(A);
  return B *= -1.0;
}

template<typename REAL, typename REAL1>
inline SymRMatrixT<REAL> operator*(const REAL1 &b, const SymRMatrixT<REAL> &A) {
  SymRMatrixT<REAL> B(A);
  return B *= b;
}

template<typename REAL, typename REAL1>
inline SymRMatrixT<REAL> operator*(const SymRMatrixT<REAL> &A, const REAL1 &b) {
  SymRMatrixT<REAL> B(A);
  return B *= b;
}

template<typename REAL, typename REAL1>
inline SymRMatrixT<REAL> operator/(const SymRMatrixT<REAL> &A, const REAL1 &b) {
  SymRMatrixT<REAL> B(A);
  return B /= b;
}

template<typename REAL>
inline RVectorT<REAL> operator*(const SymRMatrixT<REAL> &A, const RVectorT<REAL> &v) {
  return A.multiplyWith(v);
}

template<typename REAL>
inline SymRMatrixT<REAL> invert(const SymRMatrixT<REAL> &A) {
  SymRMatrixT<REAL> B(A);
  return B.Invert();
}

template<typename REAL>
inline Saver &operator<<(Saver &saver, const SymRMatrixT<REAL> &A) {
  return A.save(saver);
}

template<typename REAL>
inline Loader &operator>>(Loader &loader, SymRMatrixT<REAL> &A) {
  return A.load(loader);
}

template<typename REAL>
std::ostream &operator<<(std::ostream &os, const SymRMatrixT<REAL> &A) {
  if constexpr (std::is_same_v<REAL, double>) os << beginD;
  for (int i = 0; i < A.Dim(); ++i) {
    if (i != 0) {
      os << "\n";
      if constexpr (std::is_same_v<REAL, double>) os << beginD;
    }
    for (int j = 0; j < A.Dim(); ++j)
      os << A(i, j) << " ";
  }
  if constexpr (std::is_same_v<REAL, double>) os << endD;
  return os;
}

typedef SymRMatrixT<> SymRMatrix;

#ifdef BUILD_IA

using IASymRMatrix = SymRMatrixT<IAInterval>;

SymRMatrix mid(const IASymRMatrix &);

SymRMatrix sup(const IASymRMatrix &);

SymRMatrix inf(const IASymRMatrix &);

inline IASymRMatrix operator+(const IASymRMatrix &A, const SymRMatrix &B) {
  IASymRMatrix C(A);
  return C += B;
}

inline IASymRMatrix operator+(const SymRMatrix &A, const IASymRMatrix &B) {
  IASymRMatrix C(A);
  return C += B;
}

inline IASymRMatrix operator-(const IASymRMatrix &A, const SymRMatrix &B) {
  IASymRMatrix C(A);
  return C -= B;
}

inline IASymRMatrix operator-(const SymRMatrix &A, const IASymRMatrix &B) {
  IASymRMatrix C(A);
  return C -= B;
}

inline IARVector operator*(const IASymRMatrix &A, const RVector &v) { return A.multiplyWith(v); }

inline IARVector operator*(const SymRMatrix &A, const IARVector &v) {
  IASymRMatrix B(A);
  return B.multiplyWith(v);
}

#endif // BUILD_IA

#endif

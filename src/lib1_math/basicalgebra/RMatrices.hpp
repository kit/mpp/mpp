#ifndef RMATRICES_HPP
#define RMATRICES_HPP

#include <algorithm>
#include <functional>
#include <iterator>
#include <numeric>
#include <vector>

#include "RMatrix.hpp"

template<typename REAL = double>
class RMatricesT {
  std::vector<RMatrixT<REAL>> matrices{};
public:
  RMatricesT(int size) : matrices(size) {}

  inline void resize(int idx, int rows, int cols) { matrices[idx].resize(rows, cols); }

  inline void resize(int size) { matrices.resize(size); }

  inline int size() const noexcept { return matrices.size(); }

  inline RMatrixT<REAL> &operator[](int s) { return matrices[s]; }

  inline const RMatrixT<REAL> &operator[](int s) const { return matrices[s]; }

  inline int rows() const {
    int n = 0;
    auto firstFilledMatrix = std::find_if(std::cbegin(matrices), std::cend(matrices),
                                          [](const auto &matrix) { return matrix.row() > 0; });
    if (firstFilledMatrix != std::cend(matrices)) { n = firstFilledMatrix->rows(); }
    if (std::any_of(firstFilledMatrix, std::cend(matrices),
                    [n](const auto &matrix) { return matrix.rows() > 0 && matrix.rows() != n; })) {
      THROW("wrong dim!") // TODO: this should be checked in advance
    }
    return n;
  }

  inline int cols(int idx) const { return matrices[idx].cols(); }

  inline int cols() const {
    return std::accumulate(std::cbegin(matrices), std::cend(matrices), 0, std::plus<>(),
                           [](const auto &matrix) { return matrix.cols(); });
  }

  inline void set(int idx, const RMatrixT<REAL> &B) { matrices[idx] = B; }

  inline RMatrixT<REAL> Combine() const {
    RMatrixT<REAL> A(rows(), cols());
    int j = 0;
    for (int s = 0; s < size(); ++s)
      for (int k = 0; k < cols(s); ++j, ++k)
        for (int i = 0; i < rows(); ++i)
          A(i, j) = matrices[s][i][k];
    return A;
  }
};

using RMatrices = RMatricesT<double>;

#endif // RMATRICES_HPP

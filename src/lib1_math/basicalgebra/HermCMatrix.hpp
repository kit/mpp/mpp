#ifndef HERMCMATRIX_H
#define HERMCMATRIX_H

#include <algorithm>
#include <functional>
#include <iterator>
#include <vector>

#include "AntisymRMatrix.hpp"
#include "CVector.hpp"
#include "SymRMatrix.hpp"

template<typename REAL = double>
class HermCMatrixT {
  using COMPLEX = COMPLEX_TYPE<REAL>;
  std::vector<COMPLEX> z;
  int dim;
public:
  HermCMatrixT() : dim(0) {}

  explicit HermCMatrixT(int dim) : dim(dim), z((dim * (dim + 1)) / 2, COMPLEX{}) {}

  template<typename REAL1>
  explicit HermCMatrixT(const SymRMatrixT<REAL1> &A) :
      dim(A.Dim()), z(std::cbegin(A), std::cend(A)) {}

  template<typename REAL1>
  explicit HermCMatrixT(const HermCMatrixT<REAL1> &A) :
      dim(A.Dim()), z(std::cbegin(A), std::cend(A)) {}

  template<typename REAL1>
  inline HermCMatrixT &operator=(const SymRMatrixT<REAL1> &A) {
    dim = A.Dim();
    z = std::vector<COMPLEX>(std::cbegin(A), std::cend(A));
    return *this;
  }

  template<typename REAL1>
  inline HermCMatrixT &operator=(const HermCMatrixT<REAL1> &A) {
    dim = A.Dim();
    z.assign(std::cbegin(A), std::cend(A));
    return *this;
  }

  template<typename REAL1>
  inline HermCMatrixT &operator+=(const SymRMatrixT<REAL1> &A) {
    if (dim != A.Dim()) THROW("Size does not fit!")
    std::transform(std::cbegin(z), std::cend(z), std::cbegin(A), std::begin(z), std::plus<>());
    return *this;
  }

  template<typename REAL1>
  inline HermCMatrixT &operator+=(const HermCMatrixT<REAL1> &A) {
    if (dim != A.Dim()) THROW("Size does not fit!")
    std::transform(std::cbegin(z), std::cend(z), std::cbegin(A), std::begin(z), std::plus<>());
    return *this;
  }

  template<typename REAL1>
  inline HermCMatrixT &operator-=(const SymRMatrixT<REAL1> &A) {
    if (dim != A.Dim()) THROW("Size does not fit!")
    std::transform(std::cbegin(z), std::cend(z), std::cbegin(A), std::begin(z), std::minus<>());
    return *this;
  }

  template<typename REAL1>
  inline HermCMatrixT &operator-=(const HermCMatrixT<REAL1> &A) {
    if (dim != A.Dim()) THROW("Size does not fit!")
    std::transform(std::cbegin(z), std::cend(z), std::cbegin(A), std::begin(z), std::minus<>());
    return *this;
  }

  template<typename REAL1>
  inline HermCMatrixT &operator*=(const REAL1 &a) {
    std::transform(std::cbegin(z), std::cend(z), std::begin(z),
                   [a](const auto &value) { return value * a; });
    return *this;
  }

  template<typename REAL1>
  inline HermCMatrixT &operator/=(const REAL1 &a) {
    std::transform(std::cbegin(z), std::cend(z), std::begin(z),
                   [a](const auto &value) { return value / a; });
    return *this;
  }

  template<typename REAL1>
  inline CVectorT<REAL> multiplyWith(const RVectorT<REAL1> &v) const {
    if (v.Dim() != dim) THROW("Dimension does not fit!")
    CVectorT<REAL> y(dim);
    for (int n = 0; n < dim; ++n)
      for (int k = 0; k < dim; ++k)
        y[n] += (*this)(n, k) * v[k];
    return y;
  }

  template<typename REAL1>
  inline CVectorT<REAL> multiplyWith(const CVectorT<REAL1> &v) const {
    if (v.Dim() != dim) THROW("Dimension does not fit!")
    CVectorT<REAL> y(dim);
    for (int n = 0; n < dim; ++n)
      for (int k = 0; k < dim; ++k)
        y[n] += (*this)(n, k) * v[k];
    return y;
  }

  inline CVectorT<REAL> row(int i) const {
    CVectorT<REAL> r(dim);
    for (int j = 0; j < dim; ++j)
      r[j] = (*this)(i, j);
    return r;
  }

  inline CVectorT<REAL> col(int j) const {
    CVectorT<REAL> c(dim);
    for (int i = 0; i < dim; ++i)
      c[i] = (*this)(i, j);
    return c;
  }

  inline int size() const noexcept { return z.size(); }

  inline int Dim() const noexcept { return dim; }

  inline int rows() const noexcept { return dim; }

  inline int cols() const noexcept { return dim; }

  COMPLEX operator()(int i, int j);

  COMPLEX operator()(int i, int j) const;

  inline COMPLEX &operator[](int i) { return z[i]; }

  inline const COMPLEX &operator[](int i) const { return z[i]; }

  void operator()(const COMPLEX &a, int i, int j);

  inline auto begin() const noexcept { return z.begin(); }

  inline auto begin() noexcept { return z.begin(); }

  inline auto end() const noexcept { return z.end(); }

  inline auto end() noexcept { return z.end(); }

  inline void resize(int dim) {
    z.resize((dim * (dim + 1)) / 2);
    this->dim = dim;
  }

  HermCMatrixT &conj();

  SymRMatrixT<REAL> real() const;

  AntisymRMatrixT<REAL> imag() const;

  void Accumulate(int commSplit = 0);

  HermCMatrixT &transpose();

  inline const HermCMatrixT &adjoint() const { return *this; }

  HermCMatrixT &Identity() {
    std::fill(std::begin(z), std::end(z), REAL{});
    for (int i = 0; i < dim; i++)
      (*this)(COMPLEX(1), i, 1);
    return *this;
  }

  HermCMatrixT &Invert();

  Saver &save(Saver &saver) const;

  Loader &load(Loader &loader);
};

template<typename REAL>
inline bool operator==(const HermCMatrixT<REAL> &A, const HermCMatrixT<REAL> &B) {
  if (A.Dim() != B.Dim()) return false;
  return (A.real() == B.real()) && (A.imag() == B.imag());
}

template<typename REAL, typename REAL1>
inline bool operator==(const HermCMatrixT<REAL> &A, const SymRMatrixT<REAL1> &B) {
  return A == HermCMatrixT<REAL>(B);
}

template<typename REAL, typename REAL1>
inline bool operator==(const SymRMatrixT<REAL1> &A, const HermCMatrixT<REAL> &B) {
  return HermCMatrixT<REAL>(A) == B;
}

template<typename REAL>
inline bool operator!=(const HermCMatrixT<REAL> &A, const HermCMatrixT<REAL> &B) {
  return !(A == B);
}

template<typename REAL, typename REAL1>
inline bool operator!=(const HermCMatrixT<REAL> &A, const SymRMatrixT<REAL1> &B) {
  return !(A == B);
}

template<typename REAL, typename REAL1>
inline bool operator!=(const SymRMatrixT<REAL1> &A, const HermCMatrixT<REAL> &B) {
  return !(A == B);
}

template<typename REAL>
inline HermCMatrixT<REAL> operator+(const HermCMatrixT<REAL> &A, const HermCMatrixT<REAL> &B) {
  HermCMatrixT<REAL> AB(A);
  return AB += B;
}

template<typename REAL>
inline HermCMatrixT<REAL> operator+(const SymRMatrixT<REAL> &A, const HermCMatrixT<REAL> &B) {
  HermCMatrixT<REAL> AB(A);
  return AB += B;
}

template<typename REAL>
inline HermCMatrixT<REAL> operator+(const HermCMatrixT<REAL> &A, const SymRMatrixT<REAL> &B) {
  HermCMatrixT<REAL> AB(A);
  return AB += B;
}

template<typename REAL>
inline HermCMatrixT<REAL> operator-(const HermCMatrixT<REAL> &A, const HermCMatrixT<REAL> &B) {
  HermCMatrixT<REAL> AB(A);
  return AB -= B;
}

template<typename REAL>
inline HermCMatrixT<REAL> operator-(const SymRMatrixT<REAL> &A, const HermCMatrixT<REAL> &B) {
  HermCMatrixT<REAL> AB(A);
  return AB -= B;
}

template<typename REAL>
inline HermCMatrixT<REAL> operator-(const HermCMatrixT<REAL> &A, const SymRMatrixT<REAL> &B) {
  HermCMatrixT<REAL> AB(A);
  return AB -= B;
}

template<typename REAL>
inline HermCMatrixT<REAL> operator-(const HermCMatrixT<REAL> &A) {
  HermCMatrixT<REAL> B(A);
  return B *= -1.0;
}

template<typename REAL, typename REAL1>
inline HermCMatrixT<REAL> operator*(const REAL1 &b, const HermCMatrixT<REAL> &A) {
  HermCMatrixT<REAL> B(A);
  return B *= b;
}

template<typename REAL, typename REAL1>
inline HermCMatrixT<REAL> operator*(const HermCMatrixT<REAL> &A, const REAL1 &b) {
  HermCMatrixT<REAL> B(A);
  return B *= b;
}

template<typename REAL, typename REAL1>
inline HermCMatrixT<REAL> operator/(const HermCMatrixT<REAL> &A, const REAL1 &b) {
  HermCMatrixT<REAL> B(A);
  return B /= b;
}

template<typename REAL>
inline CVectorT<REAL> operator*(const HermCMatrixT<REAL> &A, const RVectorT<REAL> &v) {
  return A.multiplyWith(v);
}

template<typename REAL>
inline CVectorT<REAL> operator*(const HermCMatrixT<REAL> &A, const CVectorT<REAL> &v) {
  return A.multiplyWith(v);
}

template<typename REAL>
inline SymRMatrixT<REAL> real(const HermCMatrixT<REAL> &A) {
  return A.real();
}

template<typename REAL>
inline AntisymRMatrixT<REAL> imag(const HermCMatrixT<REAL> &v) {
  return v.imag();
}

template<typename REAL>
inline HermCMatrixT<REAL> conj(const HermCMatrixT<REAL> &A) {
  HermCMatrixT<REAL> B(A);
  return B.conj();
}

template<typename REAL>
inline HermCMatrixT<REAL> transpose(const HermCMatrixT<REAL> &A) {
  HermCMatrixT<REAL> B(A);
  return B.transpose();
}

template<typename REAL>
inline HermCMatrixT<REAL> invert(const HermCMatrixT<REAL> &A) {
  HermCMatrixT<REAL> B(A);
  return B.Invert();
}

template<typename REAL>
inline const HermCMatrixT<REAL> &adjoint(const HermCMatrixT<REAL> &A) {
  return A.adjoint();
}

template<typename REAL>
inline Saver &operator<<(Saver &saver, const HermCMatrixT<REAL> &A) {
  return A.save(saver);
}

template<typename REAL>
inline Loader &operator>>(Loader &loader, HermCMatrixT<REAL> &A) {
  return A.load(loader);
}

template<typename REAL>
inline std::ostream &operator<<(std::ostream &os, const HermCMatrixT<REAL> &v) {
  for (int i = 0; i < v.Dim(); ++i) {
    if (i != 0) os << "\n";
    for (int j = 0; j < v.Dim(); ++j)
      os << v(i, j) << " ";
  }
  return os;
}

using HermCMatrix = HermCMatrixT<double>;

#ifdef BUILD_IA

using IAHermCMatrix = HermCMatrixT<IAInterval>;

HermCMatrix mid(const IAHermCMatrix &);

inline IAHermCMatrix operator+(const IAHermCMatrix &A, const HermCMatrix &B) {
  IAHermCMatrix C(A);
  return C += B;
}

inline IAHermCMatrix operator+(const HermCMatrix &A, const IAHermCMatrix &B) {
  IAHermCMatrix C(A);
  return C += B;
}

inline IAHermCMatrix operator+(const IAHermCMatrix &A, const SymRMatrix &B) {
  IAHermCMatrix C(A);
  return C += B;
}

inline IAHermCMatrix operator+(const SymRMatrix &A, const IAHermCMatrix &B) {
  IAHermCMatrix C(A);
  return C += B;
}

inline IAHermCMatrix operator-(const IAHermCMatrix &A, const HermCMatrix &B) {
  IAHermCMatrix C(A);
  return C -= B;
}

inline IAHermCMatrix operator-(const HermCMatrix &A, const IAHermCMatrix &B) {
  IAHermCMatrix C(A);
  return C -= B;
}

inline IAHermCMatrix operator-(const IAHermCMatrix &A, const SymRMatrix &B) {
  IAHermCMatrix C(A);
  return C -= B;
}

inline IAHermCMatrix operator-(const SymRMatrix &A, const IAHermCMatrix &B) {
  IAHermCMatrix C(A);
  return C -= B;
}

inline IACVector operator*(const IAHermCMatrix &A, const RVector &v) { return A.multiplyWith(v); }

inline IACVector operator*(const HermCMatrix &A, const IARVector &v) {
  IAHermCMatrix B(A);
  return B.multiplyWith(v);
}

inline IACVector operator*(const IAHermCMatrix &A, const CVector &v) { return A.multiplyWith(v); }

inline IACVector operator*(const HermCMatrix &A, const IACVector &v) {
  IAHermCMatrix B(A);
  return B.multiplyWith(v);
}

#endif // BUILD_IA

#endif

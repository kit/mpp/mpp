#include "CMatrix.hpp"

#include "Assertion.hpp"
#include "CFunctions.hpp"
#include "CVector.hpp"
#include "Parallel.hpp"
#include "RMatrix.hpp"
#include "SaveLoad.hpp"

#include <algorithm>
#include <complex>
#include <format>
#include <iterator>
#include <string>
#include <vector>

//==================================================================================================
// BLAS/LAPACK routines
//==================================================================================================
// LU factorization
extern "C" void zgetrf_(int *M, int *N, void *A, int *LDA, int *IPIV, int *INFO);
// Invert matrix using LU factorization
extern "C" void zgetri_(int *N, void *A, int *LDA, int *IPIV, void *WORK, int *LWORK, int *INFO);

//==================================================================================================

template<typename REAL>
void CMatrixT<REAL>::Accumulate(int commSplit) {
  PPM->SumOnCommSplit(z, commSplit);
}

template<typename REAL>
CMatrixT<REAL> &CMatrixT<REAL>::conj() {
  std::transform(std::cbegin(z), std::cend(z), std::begin(z),
                 [](const auto &value) { return baConj(value); });
  return *this;
}

template<typename REAL>
RMatrixT<REAL> CMatrixT<REAL>::real() const {
  RMatrixT<REAL> re(Nrows, Ncols);
  std::transform(std::cbegin(z), std::cend(z), std::begin(re),
                 [](const auto &value) { return baReal(value); });
  return re;
}

template<typename REAL>
RMatrixT<REAL> CMatrixT<REAL>::imag() const {
  RMatrixT<REAL> im(Nrows, Ncols);
  std::transform(std::cbegin(z), std::cend(z), std::begin(im),
                 [](const auto &value) { return baImag(value); });
  return im;
}

template<typename REAL>
REAL CMatrixT<REAL>::Variance() const {
  REAL var_real = real().Variance();
  REAL var_imag = imag().Variance();
  return var_real + var_imag;
}

template<typename REAL>
CMatrixT<REAL> &CMatrixT<REAL>::adjoint() {
  transpose();
  conj();
  return *this;
}

template<>
CMatrixT<double> &CMatrixT<double>::Invert() {
  if (Nrows != Ncols) THROW("Nonquadratic matix in invert")

  int I[Nrows];
  int info;
  int lwork = 2 * Nrows;
  std::complex<double> work[2 * lwork];
  zgetrf_(&Nrows, &Nrows, z.data(), &Nrows, I, &info);
  if (info < 0) {
    THROW(std::format("Error in LAPACK: {}-th argument had an illegal value", -info))
  } else if (info > 0) {
    THROW(std::format("Error in LAPACK: A({},{} is exactly zero.", -info, -info))
  }
  zgetri_(&Nrows, z.data(), &Nrows, I, work, &lwork, &info);
  if (info < 0) {
    THROW(std::format("Error in LAPACK: {}-th argument had an illegal value", -info))
  } else if (info > 0) {
    THROW(std::format("Error in LAPACK: A({},{} is exactly zero.", -info, -info))
  }

  return *this;
}

template<typename REAL>
Saver &CMatrixT<REAL>::save(Saver &saver) const {
  saver << Nrows << Ncols;
  for (const auto &element : z) {
    saver << element;
  }
  return saver;
}

template<typename REAL>
Loader &CMatrixT<REAL>::load(Loader &loader) {
  int r, c;
  loader >> r >> c;
  resize(r, c);
  for (auto &element : z) {
    loader >> element;
  }
  return loader;
}

template class CMatrixT<double>;

#ifdef BUILD_IA

template class CMatrixT<IAInterval>;

CMatrix mid(const IACMatrix &IA_A) {
  CMatrix A(IA_A.rows(), IA_A.cols());
  std::transform(std::cbegin(IA_A), std::cend(IA_A), std::begin(A),
                 [](const auto &value) { return mid(value); });
  return A;
}

#endif // BUILD_IA

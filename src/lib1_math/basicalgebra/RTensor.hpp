#ifndef MLUQ_RTENSOR_H
#define MLUQ_RTENSOR_H

#include <algorithm>
#include <functional>
#include <initializer_list>
#include <iterator>
#include <vector>

#include "RMatrix.hpp"

template<typename REAL = double>
class RTensorT {
  std::vector<REAL> z{};
  int NFirstDimension;
  int NSecondDimension;
  int NThirdDimension;
public:
  RTensorT() : z(0, REAL{}), NFirstDimension(0), NSecondDimension(0), NThirdDimension(0) {}

  explicit constexpr RTensorT(int dim) :
      z(dim * dim * dim, REAL{}), NFirstDimension(dim), NSecondDimension(dim),
      NThirdDimension(dim) {}

  explicit constexpr RTensorT(const REAL &b, int dim) :
      z(dim * dim * dim, b), NFirstDimension(dim), NSecondDimension(dim), NThirdDimension(dim) {}

  constexpr RTensorT(int FirstDimension, int SecondDimension, int ThirdDimension) :
      z(FirstDimension * SecondDimension * ThirdDimension, REAL{}), NFirstDimension(FirstDimension),
      NSecondDimension(SecondDimension), NThirdDimension(ThirdDimension) {}

  constexpr RTensorT(const REAL &b, int FirstDimension, int SecondDimension, int ThirdDimension) :
      z(FirstDimension * SecondDimension * ThirdDimension, b), NFirstDimension(FirstDimension),
      NSecondDimension(SecondDimension), NThirdDimension(ThirdDimension) {}

  template<typename REAL1>
  RTensorT(const std::initializer_list<std::initializer_list<std::initializer_list<REAL1>>> &v) :
      z(v.begin()->begin()->size() * v.begin()->size() * v.size()),
      NFirstDimension(v.begin()->begin()->size()), NSecondDimension(v.begin()->size()),
      NThirdDimension(v.size()) {
    for (int i = 0; i < NFirstDimension; i++) {
      for (int j = 0; j < NSecondDimension; j++) {
        for (int k = 0; k < NThirdDimension; k++) {
          z[i + j * NFirstDimension + k * NFirstDimension * NSecondDimension] =
              (std::next(std::next(v.begin(), i)->begin(), j)->begin())[k];
        }
      }
    }
  }

  template<typename REAL1>
  RTensorT(const std::vector<std::vector<std::vector<REAL1>>> &v) :
      z(v.begin()->begin()->size() * v.begin()->size() * v.size()),
      NFirstDimension(v.begin()->begin()->size()), NSecondDimension(v.begin()->size()),
      NThirdDimension(v.size()) {
    for (int i = 0; i < NFirstDimension; i++) {
      for (int j = 0; j < NSecondDimension; j++) {
        for (int k = 0; k < NThirdDimension; k++) {
          z[i + j * NFirstDimension + k * NFirstDimension * NSecondDimension] =
              (std::next(std::next(v.begin(), i)->begin(), j)->begin())[k];
        }
      }
    }
  }

  template<typename REAL1>
  explicit RTensorT(const RTensorT<REAL1> &A) :
      z(A.begin(), A.end()), NFirstDimension(A.FirstDimension()),
      NSecondDimension(A.SecondDimension()), NThirdDimension(A.ThirdDimension()) {}

  template<typename REAL1>
  explicit RTensorT(const RTensorT<REAL1> &A, const RTensorT<REAL1> &B) :
      z(A.begin(), A.end()), NFirstDimension(A.FirstDimension()),
      NSecondDimension(A.SecondDimension()), NThirdDimension(A.ThirdDimension()) {
    if (NFirstDimension != B.FirstDimension() || NSecondDimension != B.SecondDimension()
        || NThirdDimension != B.ThirdDimension())
      THROW("Size does not fit!")
    std::transform(std::cbegin(z), std::cend(z), std::cbegin(B), std::begin(z),
                   std::multiplies<>());
  }

  template<typename REAL1>
  inline RTensorT &operator=(const RTensorT<REAL1> &A) {
    NFirstDimension = A.FirstDimension();
    NSecondDimension = A.SecondDimension();
    NThirdDimension = A.ThirdDimension();

    z = std::vector<REAL>(A.begin(), A.end());
    return *this;
  }

  inline RTensorT &operator=(const REAL &b) {
    std::fill(std::begin(z), std::end(z), b);
    return *this;
  }

  template<typename REAL1>
  inline RTensorT &operator+=(const RTensorT<REAL1> &A) {
    if (NFirstDimension != A.FirstDimension() || NSecondDimension != A.SecondDimension()
        || NThirdDimension != A.ThirdDimension())
      THROW("Size does not fit!")
    std::transform(std::cbegin(z), std::cend(z), std::cbegin(A), std::begin(z), std::plus<>());
    return *this;
  }

  template<typename REAL1>
  inline RTensorT &operator-=(const RTensorT<REAL1> &A) {
    if (NFirstDimension != A.FirstDimension() || NSecondDimension != A.SecondDimension()
        || NThirdDimension != A.ThirdDimension())
      THROW("Size does not fit!")
    std::transform(std::cbegin(z), std::cend(z), std::cbegin(A), std::begin(z), std::minus<>());
    return *this;
  }

  inline RTensorT &operator*=(const REAL &b) {
    std::transform(std::cbegin(z), std::cend(z), std::begin(z),
                   [&b](const auto &value) { return value * b; });
    return *this;
  }

  inline RTensorT &operator/=(const REAL &b) {
    std::transform(std::cbegin(z), std::cend(z), std::begin(z),
                   [&b](const auto &value) { return value / b; });
    return *this;
  }

  // Todo: Next to operators are only a draft

  inline auto operator[](int i) { return std::next(z.begin(), i * NFirstDimension); } //????

  inline auto operator[](int i) const { return std::next(z.begin(), i * NFirstDimension); } //????

  inline REAL &operator()(int FirstComponent, int SecondComponent, int ThirdComponent) {
    return z[FirstComponent + SecondComponent * NFirstDimension
             + ThirdComponent * NFirstDimension * NSecondDimension];
  }

  inline auto &operator()(int FirstComponent, int SecondComponent, int ThirdComponent) const {
    return z[FirstComponent + SecondComponent * NFirstDimension
             + ThirdComponent * NFirstDimension * NSecondDimension];
  }

  // Todo: Arguments need to be refactored. Is it useful to have these functions?

  inline RMatrixT<REAL> FirstComponent(int i) const {
    RMatrixT<REAL> c(NSecondDimension, NThirdDimension);
    for (int j = 0; j < NSecondDimension; ++j) {
      for (int k = 0; k < NThirdDimension; ++k) {
        c(j, k) = z[i + j * NFirstDimension + k * NFirstDimension * NSecondDimension];
      }
    }
    return c;
  }

  inline RMatrixT<REAL> SecondComponent(int j) const {
    RMatrixT<REAL> c(NFirstDimension, NThirdDimension);
    for (int i = 0; i < NFirstDimension; ++i) {
      for (int k = 0; k < NThirdDimension; ++k) {
        c(i, k) = z[i + j * NFirstDimension + k * NFirstDimension * NSecondDimension];
      }
    }
    return c;
  }

  inline RMatrixT<REAL> ThirdComponent(int k) const {
    RMatrixT<REAL> c(NFirstDimension, NSecondDimension);
    for (int i = 0; i < NFirstDimension; ++i) {
      for (int j = 0; j < NSecondDimension; ++j) {
        c(i, j) = z[i + j * NFirstDimension + k * NFirstDimension * NSecondDimension];
      }
    }
    return c;
  }

  template<typename REAL1>
  inline void InsertMatrixThirdDimension(const RMatrixT<REAL1> &v, int ThirdComponent,
                                         int startFirst = 0, int startSecond = 0) {
    if (ThirdComponent >= NThirdDimension || startFirst + v.cols() > NFirstDimension
        || startSecond + v.rows() > NSecondDimension) {
      THROW("Cannot insert Matrix: size of v too large")
    }
    for (int i = 0; i < v.cols(); ++i) {
      for (int j = 0; j < v.rows(); ++j) {
        z[i + startFirst + (j + startSecond) * NFirstDimension
          + ThirdComponent * NFirstDimension * NSecondDimension] = v(i, j);
      }
    }
  }

  inline int size() const noexcept { return z.size(); }

  inline int FirstDimension() const noexcept { return NFirstDimension; }

  inline int SecondDimension() const noexcept { return NSecondDimension; }

  inline int ThirdDimension() const noexcept { return NThirdDimension; }

  inline auto begin() const noexcept { return z.begin(); }

  inline auto begin() noexcept { return z.begin(); }

  inline auto end() const noexcept { return z.end(); }

  inline auto end() noexcept { return z.end(); }

  inline void resize(int FirstComponents, int SecondComponents, int ThirdComponents) {
    z = std::vector<REAL>(FirstComponents * SecondComponents * ThirdComponents, REAL{});
    NFirstDimension = FirstComponents;
    NSecondDimension = SecondComponents;
    NThirdDimension = ThirdComponents;
  }

  inline void resize(int dim) { resize(dim, dim, dim); }

  inline const std::vector<REAL> &Data() const noexcept { return z; }

  inline std::vector<REAL> &Data() noexcept { return z; }

  inline REAL Mean() const {
    REAL sum = std::accumulate(std::cbegin(z), std::cend(z), REAL{});
    return sum / z.size();
  }

  REAL Variance() const;
};

template<typename REAL>
inline RTensorT<REAL> operator-(const RTensorT<REAL> &a) {
  RTensorT<REAL> c(a);
  return c *= -1;
}

template<typename REAL>
inline RTensorT<REAL> operator+(const RTensorT<REAL> &a, const RTensorT<REAL> &b) {
  RTensorT<REAL> c(a);
  return c += b;
}

template<typename REAL>
inline RTensorT<REAL> operator-(const RTensorT<REAL> &a, const RTensorT<REAL> &b) {
  RTensorT<REAL> c(a);
  return c -= b;
}

template<typename REAL> // elementwise multiplication
inline RTensorT<REAL> operator*(const RTensorT<REAL> &a, const RTensorT<REAL> &b) {
  return RTensorT<REAL>(a, b);
}

template<typename REAL, typename REAL1>
inline RTensorT<REAL> operator*(const RTensorT<REAL> &a, const REAL1 &b) {
  RTensorT<REAL> c(a);
  return c *= b;
}

template<typename REAL, typename REAL1>
inline RTensorT<REAL> operator*(const REAL1 &b, const RTensorT<REAL> &a) {
  RTensorT<REAL> c(a);
  return c *= b;
}

template<typename REAL, typename REAL1>
inline RTensorT<REAL> operator/(const RTensorT<REAL> &a, const REAL1 &b) {
  RTensorT<REAL> c(a);
  return c /= b;
}

template<typename REAL>
inline bool operator==(const RTensorT<REAL> &A, const RTensorT<REAL> &B) {
  if (A.FirstDimension() != B.FirstDimension() || A.SecondDimension() != B.SecondDimension()
      || A.ThirdDimension() != B.ThirdDimension())
    return false;
  return std::equal(std::cbegin(A), std::cend(A), std::cbegin(B),
                    [](const auto &a, const auto &b) { return mpp_ba::isNear(a, b); });
  return true;
}

using RTensor = RTensorT<double>;

#endif // MLUQ_RTENSOR_H

#ifndef MPP_COMPONENT_H
#define MPP_COMPONENT_H

#include <string>

enum class COMPONENT {
  V_X,
  V_Y,
  V_Z,
  P0,
  P1,
  P2,
  P3,
  P4,
  P5,
  S0_x,
  S0_y,
  S0_xy,
  S1_x,
  S1_y,
  S1_xy,
  S2_x,
  S2_y,
  S2_xy,
  S3_x,
  S3_y,
  S3_xy,
  S4_x,
  S4_y,
  S4_xy
};

std::string to_string(COMPONENT comp);

bool isVelocity(COMPONENT comp);

int toVelocityIndex(COMPONENT comp);

COMPONENT GetDampingComponent(int i);


#endif // MPP_COMPONENT_H

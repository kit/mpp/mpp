#ifndef _PROBLEM_H_
#define _PROBLEM_H_

#include <memory>
#include <variant>

#include "Component.hpp"
#include "Config.hpp"
#include "Meshes.hpp"
#include "MeshesCreator.hpp"

struct DefaultMesh {};

/**
 * Defines the mesh a problem should use.
 *
 * Holds either a mesh, a creator for a mesh, the name of a mesh to use
 * or `DefaultMesh`, an indicator that the problem should choose the mesh
 * as appropriate
 */
using MeshesVariant = std::variant<std::shared_ptr<Meshes>, MeshesCreator, std::string, DefaultMesh>;

[[nodiscard]] inline MeshesVariant useMeshes(MeshesVariant meshes, std::string defaultMeshName) {
  if (std::holds_alternative<DefaultMesh>(meshes)) {
    return defaultMeshName;
  }
  return meshes;
}

class IProblem {
protected:
  int verbose = 1;

  std::shared_ptr<Meshes> meshes = nullptr;
public:
  [[deprecated]]
  explicit IProblem() {
    Config::Get("ProblemVerbose", verbose);
    std::string meshesName = "";
    Config::Get("Mesh", meshesName);
    if (!meshesName.empty()) {
      meshes = MeshesCreator(meshesName).CreateShared();
    } else {
      Warning("Mesh not initialized in Problem!")
    }
  }

  explicit IProblem(MeshesVariant meshesVariant) {
    if (std::holds_alternative<std::shared_ptr<Meshes>>(meshesVariant)) {
      meshes = get<std::shared_ptr<Meshes>>(meshesVariant);
    } else if (std::holds_alternative<std::string>(meshesVariant)) {
      std::string meshesName = get<std::string>(meshesVariant);
      if (meshesName.empty()) { Config::Get("Mesh", meshesName); }
      meshes = MeshesCreator(meshesName).CreateShared();
    } else if (std::holds_alternative<MeshesCreator>(meshesVariant)) {
      meshes = std::get<MeshesCreator>(meshesVariant).CreateShared();
    } else {
      THROW("MeshesVariant not found.")
    }
    Config::Get("ProblemVerbose", verbose);
  }

  explicit IProblem(IProblem &otherProblem) :
      meshes(otherProblem.meshes), verbose(otherProblem.verbose) {}

  virtual ~IProblem() = default;

  virtual bool HasExactSolution() const { return false; }

  void CreateMeshes(MeshesCreator meshesCreator) {
    std::string injectedMeshName = meshesCreator.GetMeshName();
    std::shared_ptr<CoarseGeometry> cGeo;
    if (injectedMeshName.empty()) {
      cGeo = meshes->Settings().coarseGeometry;
    } else {
      cGeo = CreateCoarseGeometryShared(injectedMeshName);
    }
    meshes = meshesCreator.WithCoarseGeometry(cGeo).CreateShared();
  }

  const CoarseGeometry &Domain() const { return meshes->Domain(); };

  const Meshes &GetMeshes() const {
    if (meshes == nullptr) Exit("Meshes not initialized") return *meshes;
  }

  bool IsMeshInitialized() const { return meshes != nullptr; }

  const Mesh &GetMesh(int level) const { return (*meshes)[level]; }

  const Mesh &GetMesh(MeshIndex level) const { return (*meshes)[level]; }

  [[nodiscard]]
  std::shared_ptr<Meshes> IntoMeshes() const {
    if (meshes == nullptr) Exit("Meshes not initialized") return this->meshes;
  }

  virtual std::string Name() const = 0;
};

#endif

#ifndef LEVELPAIR_HPP
#define LEVELPAIR_HPP

#include <compare>
#include <cstddef>
#include <format>
#include <functional>
#include <ostream>
#include <sstream>
#include <string>

struct MeshIndex {
  int space = -1;
  int time = -1;
  int adaptivityLevel = 0;
  int commSplit = 0;
  int reDistributeLevel = 0;

  MeshIndex NextInAdaptivity() const {
    return MeshIndex{space, time, adaptivityLevel + 1, commSplit, reDistributeLevel};
  };

  MeshIndex NextInReDistribution() const {
    return MeshIndex{space, time, adaptivityLevel, commSplit, reDistributeLevel + 1};
  };

  MeshIndex CoarserInAdaptivity() const {
    return MeshIndex{space, time, adaptivityLevel - 1, commSplit, reDistributeLevel};
  };

  MeshIndex WithoutAdaptivity() const { return MeshIndex{space, time, 0, commSplit, reDistributeLevel}; };

  MeshIndex NextInSpace() const { return MeshIndex{space + 1, time, adaptivityLevel, commSplit, reDistributeLevel}; }

  MeshIndex CoarserInSpace() const {
    return MeshIndex{space - 1, time, adaptivityLevel, commSplit, reDistributeLevel};
  }

  MeshIndex NextInTime() const { return MeshIndex{space, time + 1, adaptivityLevel, commSplit, reDistributeLevel}; }

  MeshIndex CoarserInTime() const { return MeshIndex{space, time - 1, adaptivityLevel, commSplit, reDistributeLevel}; }

  static MeshIndex Next(const MeshIndex from, const MeshIndex to) {
    MeshIndex difference = {to.space - from.space, to.time - from.time,
                            to.adaptivityLevel - from.adaptivityLevel,0,
                            to.reDistributeLevel - from.reDistributeLevel};
    return {from.space + (difference.space > 0), from.time + (difference.time > 0),
            from.adaptivityLevel + (difference.adaptivityLevel > 0),
    from.reDistributeLevel + (difference.reDistributeLevel > 0)};
  }

  MeshIndex CoarserInBoth() const { return MeshIndex{space - 1, time - 1, adaptivityLevel, commSplit, reDistributeLevel}; }

  MeshIndex WithCommSplit(const int _commSplit) const {
    return MeshIndex{space, time, adaptivityLevel, _commSplit, reDistributeLevel};
  }

  MeshIndex WithLevel(const int _level) const {
    return MeshIndex{_level, time, adaptivityLevel, commSplit, reDistributeLevel};
  }

  MeshIndex WithSpaceAndTimeLevel(const int spaceLevel, int timeLevel) const {
    return MeshIndex{spaceLevel, timeLevel, adaptivityLevel, commSplit, reDistributeLevel};
  }

  MeshIndex WithReDistributeLevel(const int redistLevel) {
    return MeshIndex{space, time, adaptivityLevel, commSplit, redistLevel};
  }

  std::string str() const {
    std::stringstream ss;
    ss << "[" << space << ", " << time << ", " << adaptivityLevel << ", " << commSplit <<  ", " << reDistributeLevel << "]";
    return ss.str();
  }

  std::strong_ordering operator<=>(const MeshIndex &other) const {
    if (space != other.space) { return space <=> other.space; }
    if (time != other.time) { return time <=> other.time; }
    if (adaptivityLevel != other.adaptivityLevel) {
      return adaptivityLevel <=> other.adaptivityLevel;
    }
    if (reDistributeLevel != other.reDistributeLevel) {
      return reDistributeLevel <=> other.reDistributeLevel;
    }
    return std::strong_ordering::equivalent;
  }
};

inline bool operator==(const MeshIndex &l1, const MeshIndex &l2) {
  return (l1.space == l2.space) && (l1.time == l2.time)
         && (l1.adaptivityLevel == l2.adaptivityLevel) && (l1.commSplit == l2.commSplit)
  && (l1.reDistributeLevel == l2.reDistributeLevel);
}

inline bool operator!=(const MeshIndex &l1, const MeshIndex &l2) { return !(l1 == l2); }

namespace std {

template<>
struct hash<MeshIndex> {
  size_t operator()(const MeshIndex &levels) const {
    return size_t(levels.space * 10000 + levels.time + levels.adaptivityLevel * 10
                  + 100 * levels.commSplit + 1000000 * levels.reDistributeLevel);
  }
};

} // namespace std

inline std::string to_string(const MeshIndex &level) {
  if (level.time < 0) return std::format("[ {} ]", level.space);
  if (level.adaptivityLevel > 0 || level.reDistributeLevel > 0) {
    return std::format("[ {} | {} | {} | {} | {}]", level.space, level.time, level.adaptivityLevel, level.commSplit, level.reDistributeLevel);
  }
  return std::format("[ {} | {} ]", level.space, level.time);
}

inline std::ostream &operator<<(std::ostream &os, MeshIndex level) {
  return os << to_string(level);
}

[[deprecated("Use MeshIndex instead of LevelPair.")]]
typedef MeshIndex LevelPair;

#endif // LEVELPAIR_HPP

#ifndef ROW_HPP
#define ROW_HPP

#include <sys/types.h>
#include <cstddef>
#include <cstdint>
#include <unordered_map>

#include "Point.hpp"

class RowEntry {
  /// number of the Entry with respect to all Entries (will be set in Rows)
  ssize_t id = -1;
  /// index later used as start index in the BasicVector for a Matrix
  ssize_t entryIndex = -1;
public:
  RowEntry() = default;

  void Id(const ssize_t _id) { id = _id; }

  ssize_t Id() const { return id; }

  void EntryIndex(const ssize_t index) { entryIndex = index; }

  ssize_t EntryIndex() const { return entryIndex; }

  friend std::ostream &operator<<(std::ostream &s, const RowEntry &entry);
};

class Row {
public:
  using RowEntryMap = std::unordered_map<Point, RowEntry>;
  using RowEntryIterator = std::unordered_map<Point, RowEntry>::iterator;
  using RowEntryIterator_const = std::unordered_map<Point, RowEntry>::const_iterator;
private:
  /// number of the entry with respect to all entries
  ssize_t id = -1;
  /// index later used as start index in the BasicVector for a Vector
  ssize_t index = -1;
  /// degrees of freedom in the Row, i.e., for each Entry
  uint16_t dofs = 0;
  /// map containing the entries
  RowEntryMap entries{};
public:
  Row() = default;

  explicit Row(const uint16_t numberOfDofs) : dofs(numberOfDofs) {}

  void Id(const size_t _id) { id = _id; }

  ssize_t Id() const { return id; }

  void Index(const size_t _index) { index = _index; }

  ssize_t Index() const { return index; }

  void NumberOfDofs(const uint16_t _dofs) { dofs = _dofs; }

  uint16_t NumberOfDofs() const { return dofs; }

  void InsertEntry(const Point &z) {
    if (!entries.contains(z)) entries[z] = RowEntry();
  }

  RowEntryMap &Entries() { return entries; }

  const RowEntryMap &Entries() const { return entries; }

  RowEntryIterator_const EntriesBegin() const { return entries.begin(); }

  RowEntryIterator_const EntriesEnd() const { return entries.end(); }

  RowEntryIterator_const FindEntry(const Point &z) const { return entries.find(z); }

  size_t NumberOfEntries() const { return entries.size(); }

  ssize_t EntryId(const Point &z) const { return entries.find(z)->second.Id(); }

  ssize_t EntryIdChecked(const Point &z) const;

  ssize_t EntryIndex(const Point &z) const { return entries.find(z)->second.EntryIndex(); }

  ssize_t EntryIndexChecked(const Point &z) const;

  template<typename S>
  friend LogTextStream<S> &operator<<(LogTextStream<S> &s, const Row &R);
};

class Rows : public std::unordered_map<Point, Row> {
public:
  using RowMap = std::unordered_map<Point, Row>;
  using RowIterator = std::unordered_map<Point, Row>::iterator;
  using RowIterator_const = std::unordered_map<Point, Row>::const_iterator;
private:
  /// total number of dofs (sum over all entries) -> size of Matrix
  size_t totalDofs = 0;
  /// total number of entries
  size_t totalEntries = 0;
  /// sum of number of dofs over all rows
  size_t sumDofs = 0;
public:
  Rows() = default;

  /// size of Matrix
  size_t NumberOfDofsTotal() const { return totalDofs; }

  size_t SumOfDoFs() const { return sumDofs; }

  size_t NumberOfEntriesTotal() const { return totalEntries; }

  void Insert(const Point &x, const uint16_t numberOfDofs) {
    if (!this->contains(x)) (*this)[x] = Row(numberOfDofs);
  }

  void InsertInfty(const uint16_t numberOfDofs);

  void AddEntryFixedOrder(const Point &_row, const Point &_entry) {
    (*this)[_row].InsertEntry(_entry);
  }

  void AddEntry(const Point &x, const Point &y);

  /// initializes the ids and indices of the entries. Call after ALL entries are inserted
  void InitIndices(bool);

  ssize_t RowId(const Point &z) const { return find(z)->second.Id(); }

  ssize_t RowIdChecked(const Point &z) const;

  ssize_t RowIndex(const Point &z) const { return find(z)->second.Index(); }

  uint16_t RowNumberOfDofs(const Point &z) const { return find(z)->second.NumberOfDofs(); }

  size_t RowNumberOfEntries(const Point &z) const { return find(z)->second.NumberOfEntries(); }

  ssize_t Index(const RowIterator_const &r0, const RowIterator_const &r1) const;

  ssize_t IndexChecked(const RowIterator_const &r0, const RowIterator_const &r1) const;

  ssize_t DoubleIndexChecked(const RowIterator_const &r0, const RowIterator_const &r1) const;
};

#endif // ROW_HPP

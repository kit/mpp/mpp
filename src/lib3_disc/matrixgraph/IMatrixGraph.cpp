#include "IMatrixGraph.hpp"

#include <sys/types.h>
#include <cstddef>
#include <cstdint>
#include <vector>

#include "MixedDoF.hpp"

void IMatrixGraph::InitIndexData() {
  _rows.InitIndices(singleEntries);
  const size_t rowCount = _rows.size();
  index = std::vector<size_t>(rowCount + 1);
  diag = std::vector<size_t>(rowCount + 1);
  n = std::vector<uint16_t>(rowCount);
  column = std::vector<ssize_t>(_rows.NumberOfEntriesTotal() + 1);
  matentry = std::vector<ssize_t>(_rows.NumberOfEntriesTotal() + 1);
  if (singleEntries) singleentry = std::vector<bool>(_rows.NumberOfEntriesTotal() + 1);
  std::vector<row> r(rowCount);
  for (row ri = rows(); ri != rows_end(); ++ri) {
    r[ri.Id()] = ri;
    n[ri.Id()] = ri.NumberOfDofs();
  }
  size_t d = 0;
  index[0] = 0;
  diag[0] = d;
  matentry[d] = 0;
  for (size_t i = 0; i < rowCount; ++i) {
    index[i + 1] = index[i] + n[i];
    column[d] = i;
    matentry[d] = r[i].Index();
    ++d;
    const double t = r[i]().t();
    for (entry e = r[i].entries(); e != r[i].entries_end(); ++e) {
      column[d] = e.Id();
      matentry[d] = e.EntryIndex();
      if (singleEntries) singleentry[d] = (e().t() != t);
      ++d;
    }
    diag[i + 1] = d;
  }
  N = index[rowCount];
}

void IMatrixGraph::AddCell(const Cell &c, int depth) {
  std::vector<Point> sp = dof->GetStoragePoints(c);
  std::vector<short> allocSizes = dof->AllocationSizesAtStoragePoints(c);
  for (size_t i = 0; i < sp.size(); ++i) {
    _rows.Insert(sp[i], allocSizes[i]);
  }

  switch (depth) {
  case 0:
    return;
  case 1: {
    for (size_t i = 1; i < sp.size(); ++i)
      for (size_t j = 0; j < i; ++j)
        _rows.AddEntry(sp[i], sp[j]);
    break;
  }
  default:
    THROW("Only depth 0 and 1 are implemented!")
  }
}

void IMatrixGraph::AddCells(int depth) {
  for (cell c = cells(); c != cells_end(); ++c)
    AddCell(*c, depth);
}

void IMatrixGraph::AddOverlapCells(int depth) {
  for (cell c = overlap(); c != overlap_end(); ++c)
    AddCell(*c, depth);
}

void IMatrixGraph::Init() {
  _rows.InsertInfty(dof->n_infty());
  if (parallel()) { InitParallel(); }
  if (identify()) { InitIdentify(); }
  InitIndexData();
}

Buffer &operator>>(Buffer &buffer, RowIDCommunicationPairHelper &pair) {
  buffer >> pair.ReceivingCellMid >> pair.ReceivingRowIndex >> pair.DofSize
      >> pair.IsReceivingMaster >> pair.master;
  return buffer;
}

Buffer &operator<<(Buffer &buffer, const RowIDCommunicationPairHelper &pair) {
  buffer << pair.ReceivingCellMid << pair.ReceivingRowIndex << pair.DofSize
         << pair.IsReceivingMaster << pair.master;
  return buffer;
}

void IMatrixGraph::InitRowIDCommunicationList() const {
  Comm.clear();
  ExchangeBuffer exBuf(CommSplit());
  for (procset p = GetProcSets().procsets(); p != GetProcSets().procsets_end(); ++p) {
    bool isMaster = p.master() == PPM->Proc();
    auto R = (*this).find_row(p());
    const ssize_t id = R.Id();
    for (size_t k = 0; k < p.size(); ++k) {
      exBuf.Send(p[k]) << RowIDCommunicationPairHelper{p(), Index(id), n[id], isMaster, p.master()};
    }
  }
  exBuf.Communicate();
  for (int q = 0; q < PPM->Size(CommSplit()); ++q) {
    if (q == PPM->Proc(CommSplit())) continue;
    while (exBuf.Receive(q).size() < exBuf.Receive(q).Size()) {
      RowIDCommunicationPairHelper pairhelp;
      exBuf.Receive(q) >> pairhelp;
      Comm.push_back(RowIDCommunicationPair{q, pairhelp.ReceivingRowIndex, pairhelp.DofSize,
                                            index[(*this).find_row(pairhelp.ReceivingCellMid).Id()],
                                            pairhelp.IsReceivingMaster,
                                            pairhelp.master == PPM->Proc()});
    }
  }
}

void IMatrixGraph::InitParallel() {
  for (row r = rows(); r != rows_end(); ++r) {
    procset p = meshAndOverlapProcSets.Find(r());
    if (p != meshAndOverlapProcSets.End()) procSets.Copy(p);
  }
  for (cell c = cells(); c != cells_end(); ++c)
    SetProcSetsCell(*c);
  for (cell c = overlap(); c != overlap_end(); ++c)
    SetProcSetsOverlapCell(*c);
  if (dof->n_infty()) procSets.AddInfty();

  ExchangeBuffer E(CommSplit());
  for (procset p = procSets.procsets(); p != procSets.procsets_end(); ++p) {
    row r = find_row(p());
    if (r == row(_rows.end())) continue;
    if (r.NumberOfDofs() == 1) continue;
    for (size_t j = 0; j < p.size(); ++j) {
      intproc q = p[j];
      if (q == PPM->Proc(CommSplit())) continue;
      E.Send(q) << p() << r.NumberOfDofs();
    }
  }
  E.Communicate();
  for (short q = 0; q < PPM->Size(CommSplit()); ++q) {
    while (E.Receive(q).size() < E.ReceiveSize(q)) {
      Point z;
      uint16_t n;
      E.Receive(q) >> z >> n;
      Rows::RowIterator r = _rows.find(z);
      if (r != _rows.end()) r->second.NumberOfDofs(n);
    }
  }
}

void IMatrixGraph::InitIdentify() {
  for (identifyset is = mesh.identifysets(); is != mesh.identifysets_end(); ++is) {
    row r = find_row(is());
    if (r != rows_end()) identifySets.Insert(is);
  }
  for (cell c = cells(); c != cells_end(); ++c)
    IdentifyCell(*c);
  for (cell c = overlap(); c != overlap_end(); ++c)
    IdentifyOverlapCell(*c);
  for (identifyset is = mesh.identifysets(); is != mesh.identifysets_end(); ++is) {
    row r = find_row(is());
    if (r != rows_end()) identifySets.Insert(is);
  }
  for (identifyset is = identifySets.identifysets(); is != identifySets.identifysets_end(); ++is) {
    for (size_t i = 0; i < is.size(); ++i) {
      procset p = procSets.find_procset(is[i]);
      if (p == procSets.procsets_end()) continue;
      for (size_t j = 0; j < is.size(); ++j)
        procSets.Add(is[j], p);
    }
  }
  for (identifyset is = identifySets.identifysets(); is != identifySets.identifysets_end(); ++is) {
    uint16_t n = _rows.RowNumberOfDofs(is());
    for (size_t i = 0; i < is.size(); ++i)
      _rows.Insert(is[i], n);
    procset p = procSets.find_procset(is());
    if (p == procSets.procsets_end()) continue;
    for (size_t i = 0; i < is.size(); ++i)
      procSets.Copy(p, is[i]);
  }
  for (identifyset is = mesh.identifysets(); is != mesh.identifysets_end(); ++is) {
    row r = find_row(is());
    if (r == rows_end()) continue;
    identifySets.Insert(is);
    if (is.master()) continue;
    for (size_t i = 0; i < is.size(); ++i) {
      row rr = find_row(is[i]);
      if (rr == rows_end()) continue;
      for (entry e = rr.entries(); e != rr.entries_end(); ++e)
        _rows.AddEntry(r(), e());
    }
  }
  for (row r = rows(); r != rows_end(); ++r) {
    for (entry e = r.entries(); e != r.entries_end(); ++e) {
      identifyset is = identifySets.find_identifyset(e());
      if (is == identifySets.identifysets_end()) continue;
      if (is.master()) continue;
      row r0 = find_row(is[0]);
      _rows.AddEntry(r(), r0());
    }
  }
  for (identifyset is = identifySets.identifysets(); is != identifySets.identifysets_end(); ++is) {
    if (is.master()) continue;
    row r2 = find_row(is());
    row r0 = find_row(is[0]);
    for (entry e = r2.entries(); e != r2.entries_end(); ++e)
      _rows.AddEntry(e(), r0());
    _rows.AddEntry(r2(), r0());
  }
}

void IMatrixGraph::SetProcSetsCell(const Cell &c) {
  procset pc = meshAndOverlapProcSets.Find(c());
  // set procsets on faces
  for (int i = 0; i < c.Faces(); ++i) {
    procset pf = meshAndOverlapProcSets.Find(c.Face(i));
    if (pf == meshAndOverlapProcSets.End()) continue;
    std::vector<Point> z = dof->GetStoragePoints(c);
    for (int l = 0; l < c.FaceEdges(i); ++l) {
      procset pfe = meshAndOverlapProcSets.Find(c.FaceEdge(i, l));
      if (pfe == meshAndOverlapProcSets.End()) continue;
      for (int k = 0; k < dof->NumberOfStoragePointsOnEdge(c, c.faceedge(i, l)); ++k) {
        int nk = dof->IdOfStoragePointOnEdge(c, c.faceedge(i, l), k);
        procSets.Add(z[nk], pfe);
      }
    }
    for (int k = 0; k < dof->NumberOfStoragePointsOnFace(c, i); ++k) {
      int j = dof->IdOfStoragePointOnFace(c, i, k);
      procSets.Add(z[j], pf);
    }
  }
  // set procsets on edges
  for (int i = 0; i < c.Edges(); ++i) {
    procset p = meshAndOverlapProcSets.Find(c.Edge(i));
    if (p == meshAndOverlapProcSets.End()) continue;
    std::vector<Point> z = dof->GetStoragePoints(c);
    for (int k = 0; k < dof->NumberOfStoragePointsOnEdge(c, i); ++k) {
      int nk = dof->IdOfStoragePointOnEdge(c, i, k);
      procSets.Add(z[nk], p);
    }
  }
  // set procsets in cell
  if (pc != meshAndOverlapProcSets.End()) {
    std::vector<Point> z = dof->GetStoragePoints(c);
    for (size_t i = 0; i < z.size(); ++i) {
      procSets.Append(z[i], pc);
    }
  }
}

void IMatrixGraph::SetProcSetsOverlapCell(const Cell &c) {
  procset p = meshAndOverlapProcSets.Find(c());
  Assert(p != meshAndOverlapProcSets.End());

  // set procsets on faces
  for (int i = 0; i < c.Faces(); ++i) {
    procset pf = meshAndOverlapProcSets.Find(c.Face(i));
    if (pf == meshAndOverlapProcSets.End()) continue;
    std::vector<Point> z = dof->GetStoragePoints(c);
    for (int l = 0; l < c.FaceEdges(i); ++l) {
      procset pfe = meshAndOverlapProcSets.Find(c.FaceEdge(i, l));
      if (pfe == meshAndOverlapProcSets.End()) continue;
      for (int k = 0; k < dof->NumberOfStoragePointsOnEdge(c, c.faceedge(i, l)); ++k) {
        int nk = dof->IdOfStoragePointOnEdge(c, c.faceedge(i, l), k);
        procSets.Append(z[nk], pfe);
      }
    }
    for (int k = 0; k < dof->NumberOfStoragePointsOnFace(c, i); ++k) {
      int j = dof->IdOfStoragePointOnFace(c, i, k);
      procSets.Append(z[j], pf);
    }
  }
  // set procsets in cell
  std::vector<Point> z = dof->GetStoragePoints(c);
  for (size_t i = 0; i < z.size(); ++i) {
    procSets.Append(z[i], p);
  }
}

void IMatrixGraph::IdentifyCell(const Cell &c) {
  for (int i = 0; i < c.Faces(); ++i) {
    identifyset is = mesh.find_identifyset(c.Face(i));
    if (is == mesh.identifysets_end()) continue;
    int mode = mesh.BoundaryFacePart(c.Face(i));
    std::vector<Point> z = dof->GetStoragePoints(c);
    for (int k = 0; k < dof->NumberOfStoragePointsOnFace(c, i); ++k) {
      int j = dof->IdOfStoragePointOnFace(c, i, k);
      identifySets.Identify(z[j], mode);
      procset p = meshAndOverlapProcSets.Find(z[j]);
      if (p == meshAndOverlapProcSets.End()) continue;
      procSets.Add(z[j], p);
    }
    procset p = meshAndOverlapProcSets.Find(is());
    if (p == meshAndOverlapProcSets.End()) continue;
    for (int k = 0; k < dof->NumberOfStoragePointsOnFace(c, i); ++k) {
      int j = dof->IdOfStoragePointOnFace(c, i, k);
      procSets.Add(z[j], p);
    }
  }
}

void IMatrixGraph::IdentifyOverlapCell(const Cell &c) {
  for (int i = 0; i < c.Faces(); ++i) {
    identifyset is = mesh.find_identifyset(c.Face(i));
    if (is == mesh.identifysets_end()) continue;
    int mode = mesh.BoundaryFacePart(c.Face(i));
    std::vector<Point> z = dof->GetStoragePoints(c);
    for (int k = 0; k < dof->NumberOfStoragePointsOnFace(c, i); ++k) {
      int j = dof->IdOfStoragePointOnFace(c, i, k);
      identifySets.Identify(z[j], mode);
      procset p = meshAndOverlapProcSets.Find(z[j]);
      if (p == meshAndOverlapProcSets.End()) continue;
      procSets.Append(z[j], p);
    }
    procset p = meshAndOverlapProcSets.Find(is());
    if (p == meshAndOverlapProcSets.End()) continue;
    for (int k = 0; k < dof->NumberOfStoragePointsOnFace(c, i); ++k) {
      int j = dof->IdOfStoragePointOnFace(c, i, k);
      procSets.Append(z[j], p);
    }
  }
}

size_t IMatrixGraph::pSize() const {
  size_t cnt = 0;
  for (const auto &[p, r] : _rows)
    if (procSets.master(p)) cnt += r.NumberOfDofs();
  return PPM->SumOnCommSplit(cnt, CommSplit());
}

size_t IMatrixGraph::pMatrixSize() const { return PPM->SumOnCommSplit(Size(), CommSplit()); }

bool IMatrixGraph::SingleEntry(const size_t i) const {
  if (singleEntries) return singleentry[i];
  return false;
}

const SubVectorMask &IMatrixGraph::Mask(const char *name) const { return dof->GetSubVector(name); }

void IMatrixGraph::PrintInfo() const {
  MatrixGraphMemoryInfo<double> vectorMemInfo = VectorMemoryInfo();
  MatrixGraphMemoryInfo<double> matrixMemInfo = MatrixMemoryInfo();
  mout.PrintInfo("Matrix Graph", verbose, PrintInfoEntry("DoF", dof->Name()),
#ifndef USE_SPACETIME
                 PrintInfoEntry("Steps", GetMesh().steps()),
#endif
                 PrintInfoEntry("Problem size", vectorMemInfo.totalSize),
                 PrintInfoEntry("Vector Mem in MB", vectorMemInfo.memTotal_MB, 3),
                 PrintInfoEntry("Vector Mem max on proc in MB", vectorMemInfo.memMaxOnProc_MB, 3),
                 PrintInfoEntry("Vector Mem max on proc in GB", vectorMemInfo.memMaxOnProc_GB, 3),
                 PrintInfoEntry("Vector Total Memory in MB", vectorMemInfo.memTotal_MB, 3),
                 PrintInfoEntry("Vector Total Memory in GB", vectorMemInfo.memTotal_GB, 3),
                 PrintInfoEntry("Vector Memory balancing factor", vectorMemInfo.memBalance, 3),
                 PrintInfoEntry("Matrix size", matrixMemInfo.totalSize, 2),
                 PrintInfoEntry("Matrix Mem in MB", matrixMemInfo.memTotal_MB, 2),
                 PrintInfoEntry("Matrix Mem max on proc in MB", matrixMemInfo.memMaxOnProc_MB, 2),
                 PrintInfoEntry("Matrix Mem max on proc in GB", matrixMemInfo.memMaxOnProc_GB, 2),
                 PrintInfoEntry("Matrix Total Memory in MB", matrixMemInfo.memTotal_MB, 2),
                 PrintInfoEntry("Matrix Total Memory in GB", matrixMemInfo.memTotal_GB, 2),
                 PrintInfoEntry("Matrix Memory balancing factor", matrixMemInfo.memBalance, 2));
}

MatrixGraphMemoryInfo<double> IMatrixGraph::VectorMemoryInfo() const {
  return MatrixGraphMemoryInfo<double>(pSize(), size(), CommSplit(), "VectorMemoryInfo");
}

void IMatrixGraph::PrintVectorMemoryInfo() const { VectorMemoryInfo().PrintInfo(); }

MatrixGraphMemoryInfo<double> IMatrixGraph::MatrixMemoryInfo() const {
  return MatrixGraphMemoryInfo<double>(pMatrixSize(), Size(), CommSplit(), "MatrixMemoryInfo");
}

void IMatrixGraph::PrintMatrixMemoryInfo() const { MatrixMemoryInfo().PrintInfo(); }

template<typename S>
LogTextStream<S> &operator<<(LogTextStream<S> &s, const IMatrixGraph &G) {
  s << "DoFs" << endl;
  for (size_t i = 0; i < G._rows.size(); ++i)
    s << G.Dof(i) << " ";
  s << endl << "index" << endl;
  for (size_t i = 0; i <= G._rows.size(); ++i)
    s << G.Index(i) << " ";
  s << endl << "diag" << endl;
  for (size_t i = 0; i <= G._rows.size(); ++i)
    s << G.Diag(i) << " ";
  s << endl << "column" << endl;
  for (size_t i = 0; i < G._rows.NumberOfEntriesTotal(); ++i)
    s << G.Column(i) << " ";
  s << endl << "entry" << endl;
  for (size_t i = 0; i < G._rows.NumberOfEntriesTotal(); ++i)
    s << G.Entry(i) << " ";
  return s << endl
           << "Rows: " << endl
           << G._rows << "ProcSets: " << G.procSets.size() << endl
           << G.procSets.ref() << "IdentifySets: " << endl
           << G.identifySets.size() << endl
           << G.identifySets.ref();
}

rows::rows(const IMatrixGraph &g, const Cell &c) {
  if (g.IsSpaceTime()) {
    resize(1);
    (*this)[0] = g.find_row(c());
    return;
  }
  std::vector<Point> z = g.GetDoF().GetStoragePoints(c);
  resize(z.size());
  for (size_t i = 0; i < size(); ++i) {
    (*this)[i] = g.find_row(z[i]);
  }
}

rows::rows(int n, const IMatrixGraph &g, const Cell &c) {
  if (g.IsSpaceTime()) {
    resize(1);
    (*this)[0] = g.find_row(c());
    return;
  }
  std::vector<Point> z = MixedDoF::Cast(g.GetShapeDoF()).GetStoragePoints(n, c);
  resize(z.size());
  for (size_t i = 0; i < size(); ++i) {
    (*this)[i] = g.find_row(z[i]);
  }
}

rows::rows(const IMatrixGraph &g, const Cell &c, int face) {
  std::vector<Point> z = g.GetDoF().GetStoragePoints(c);
  resize(g.GetDoF().NumberOfStoragePointsOnFace(c, face));
  for (size_t i = 0; i < size(); ++i) {
    (*this)[i] = g.find_row(z[g.GetDoF().IdOfStoragePointOnFace(c, face, i)]);
  }
}

rows::rows(const IMatrixGraph &g, const Cell &c, int face, int n) {
  const MixedDoF &mixedDoF = MixedDoF::Cast(g.GetShapeDoF());
  std::vector<Point> z = mixedDoF.GetStoragePoints(n, c);
  resize(mixedDoF.NumberOfStoragePointsOnFace(n, c, face));
  for (size_t i = 0; i < size(); ++i) {
    (*this)[i] = g.find_row(z[mixedDoF.IdOfStoragePointOnFace(n, c, face, i)]);
  }
}

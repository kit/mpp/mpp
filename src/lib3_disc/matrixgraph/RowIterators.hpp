#ifndef ROWITERATORS_HPP
#define ROWITERATORS_HPP

#include <sys/types.h>
#include <cstddef>
#include <cstdint>
#include <unordered_map>

#include "Row.hpp"

class entry : public std::unordered_map<Point, RowEntry>::const_iterator {
public:
  entry() {}

  entry(std::unordered_map<Point, RowEntry>::const_iterator e) :
      std::unordered_map<Point, RowEntry>::const_iterator(e) {}

  const Point &operator()() const { return (*this)->first; }

  ssize_t Id() const { return (*this)->second.Id(); }

  ssize_t EntryIndex() const { return (*this)->second.EntryIndex(); }

  friend std::ostream &operator<<(std::ostream &s, const entry &e) {
    return s << e->first << " | " << e->second;
  }

  // ================================
  // TODO: Use methods above instead
  // ================================
  [[deprecated("Use EntryIndex instead")]]
  ssize_t GetEntry() const {
    return EntryIndex();
  }
};

class row : public std::unordered_map<Point, Row>::const_iterator {
public:
  row() {}

  row(std::unordered_map<Point, Row>::const_iterator r) :
      std::unordered_map<Point, Row>::const_iterator(r) {}

  const Point &operator()() const { return (*this)->first; }

  size_t NumberOfEntries() const { return (*this)->second.NumberOfEntries(); }

  ssize_t Id() const { return (*this)->second.Id(); }

  ssize_t Index() const { return (*this)->second.Index(); }

  uint16_t NumberOfDofs() const { return (*this)->second.NumberOfDofs(); }

  ssize_t EntryIndex(const Point &z) const { return (*this)->second.EntryIndex(z); }

  ssize_t EntryIndexChecked(const Point &z) const { return (*this)->second.EntryIndexChecked(z); }

  template<typename S>
  friend LogTextStream<S> &operator<<(LogTextStream<S> &s, const row &r) {
    return s << r->first << " | " << r->second;
  }

  friend bool operator<(const row &r0, const row &r1) { return (r0() < r1()); }

  // ================================
  // TODO: Use methods above instead
  // ================================
  entry entries() const { return entry((*this)->second.EntriesBegin()); }

  entry entries_end() const { return entry((*this)->second.EntriesEnd()); }

  entry find_entry(const Point &z) const { return entry((*this)->second.FindEntry(z)); }

  [[deprecated("Use NumberOfDofs instead")]]
  uint16_t n() const {
    return (*this)->second.NumberOfDofs();
  }

  [[deprecated("Use NumberOfEntries instead")]]
  size_t size() const {
    return (*this)->second.NumberOfEntries();
  }

  [[deprecated("Use Index instead")]]
  ssize_t GetEntry() const {
    return Index();
  }

  [[deprecated("Use EntryIndex instead")]]
  ssize_t GetEntry(const Point &z) const {
    return EntryIndex(z);
  }

  [[deprecated("Use EntryIndexChecked instead")]]
  ssize_t GetEntryX(const Point &z) const {
    return EntryIndexChecked(z);
  }
};

#endif // ROWITERATORS_HPP

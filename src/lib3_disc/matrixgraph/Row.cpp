#include "Row.hpp"

#include <sys/types.h>
#include <algorithm>
#include <cstdint>
#include <ostream>
#include <sstream>
#include <vector>

std::ostream &operator<<(std::ostream &s, const RowEntry &e) {
  return s << "Entry: id= " << e.id << "; index= " << e.entryIndex;
}

ssize_t Row::EntryIdChecked(const Point &z) const {
  auto entry = entries.find(z);
  if (entry == entries.end()) return -1;
  return entry->second.Id();
}

ssize_t Row::EntryIndexChecked(const Point &z) const {
  auto entry = entries.find(z);
  if (entry == entries.end()) return -1;
  return entry->second.EntryIndex();
}

template<typename S>
LogTextStream<S> &operator<<(LogTextStream<S> &s, const Row &R) {
  return s << "Row: id= " << R.id << "; index= " << R.index << "; dofs= " << R.dofs << endl
           << R.entries;
}

ssize_t Rows::RowIdChecked(const Point &z) const {
  auto r = find(z);
  if (r == end()) return -1;
  return r->second.Id();
}

void Rows::InsertInfty(const uint16_t numberOfDofs) {
  if (numberOfDofs == 0) return;
  Insert(Infty, numberOfDofs);
  auto r_infty = find(Infty);
  for (auto r = begin(); r != end(); ++r) {
    if (r != r_infty) AddEntry(r->first, Infty);
  }
}

void Rows::AddEntry(const Point &x, const Point &y) {
  if (y < x) AddEntryFixedOrder(x, y);
  else AddEntryFixedOrder(y, x);
}

void Rows::InitIndices(bool singleEntries) {
  using Iterator = std::unordered_map<Point, Row>::iterator;

  std::vector<Iterator> rowsVec{};
  rowsVec.reserve(size());
  for (Iterator rowIt = begin(); rowIt != end(); ++rowIt)
    rowsVec.push_back(rowIt);
  std::sort(rowsVec.begin(), rowsVec.end(),
            [](const Iterator &r0, const Iterator &r1) { return r0->first < r1->first; });
  for (size_t id = 0; id < rowsVec.size(); ++id)
    rowsVec[id]->second.Id(id);

  totalEntries = 0;
  totalDofs = 0;
  sumDofs = 0;
  for (Iterator &it : rowsVec) {
    Row &r0 = it->second;
    r0.Index(totalDofs);
    ++totalEntries;
    totalDofs += r0.NumberOfDofs() * r0.NumberOfDofs();
    sumDofs += r0.NumberOfDofs();
    const double t = it->first.t();
    for (auto &[p1, e1] : r0.Entries()) {
      const auto iterator = find(p1);
      // Row corresponding to RowEntry point e1
      if constexpr (DebugLevel > 0) {
        if (iterator == end()) {
          std::stringstream s;
          s << "Point " << p1 << " could not be found in iterator!";
          THROW(s.str());
        }
      }
      const Row &r1 = iterator->second;
      e1.Id(r1.Id());
      e1.EntryIndex(totalDofs);
      ++totalEntries;
      if (singleEntries && (p1.t() != t)) { // Why?
        totalDofs += r0.NumberOfDofs() * r1.NumberOfDofs();
      } else {
        totalDofs += 2 * r0.NumberOfDofs() * r1.NumberOfDofs();
      }
    }
  }
}

ssize_t Rows::Index(const RowIterator_const &r0, const RowIterator_const &r1) const {
  if (r0 == r1) /// index of the diagonal block corresponding to r0 (and r1)
    return r0->second.Index();
  if (r1->first < r0->first) /// index of the non-diagonal block corresponding to r0 and r1
    return r0->second.EntryIndex(r1->first);

  return r1->second.EntryIndex(r0->first) + r0->second.NumberOfDofs() * r1->second.NumberOfDofs();
  // TODO: Check, if this is faster
  //  if (r0 == r1) /// index of the diagonal block corresponding to r0 (and r1)
  //    return r0->second.Index();
  //  if (r1->second.Id() < r0->second.Id()) /// index of the non-diagonal block
  //  corresponding to r0 and r1
  //    return r0->second.EntryIndex(r1->first);
  //  return r1->second.EntryIndex(r0->first) + r0->second.NumberOfDofs() *
  //  r1->second.NumberOfDofs();
}

ssize_t Rows::IndexChecked(const RowIterator_const &r0, const RowIterator_const &r1) const {
  if (r0 == r1) return r0->second.Index();
  if (r1->first < r0->first) return r0->second.EntryIndexChecked(r1->first);
  ssize_t k = r1->second.EntryIndexChecked(r0->first);
  if (k == -1) return -1;
  return k + r0->second.NumberOfDofs() * r1->second.NumberOfDofs();
  // TODO: Check, if this is faster
  //  if (r0 == r1)
  //    return r0->second.Index();
  //  if (r1->second.Id() < r0->second.Id())
  //    return r0->second.EntryIndexChecked(r1->first);
  //  int k = r1->second.EntryIndexChecked(r0->first);
  //  if (k == -1) return -1;
  //  return k + r0->second.NumberOfDofs() * r1->second.NumberOfDofs();
}

ssize_t Rows::DoubleIndexChecked(const RowIterator_const &r0, const RowIterator_const &r1) const {
  if (r0->second.Id() == r1->second.Id()) return r0->second.Index();
  if (r1->second.Id() < r0->second.Id()) return r0->second.EntryIndexChecked(r1->first);
  else return r1->second.EntryIndexChecked(r0->first);
}

#ifndef CYCLICPRECONDITIONER_HPP
#define CYCLICPRECONDITIONER_HPP

#include <memory>
#include "PreconditionerBase.hpp"

class CyclicPreconditioner : public Preconditioner {
private:
  std::vector<std::shared_ptr<Preconditioner>> preconditioners;
  std::vector<int> cycle_indices;
  mutable int current_cycle_index = 0;
public:
  void multiply(Vector &u, const Vector &b) const override;

  explicit CyclicPreconditioner(const std::string &prefix = "");

  CyclicPreconditioner(const std::vector<std::shared_ptr<Preconditioner>> &preconditioners,
                       const std::vector<int> &cycle_indices);

  void Construct(const Matrix &M) override;

  void Destruct() override;

  void reset() override;

  std::string Name() const override;

  void transpose() const override;
};

#endif // CYCLICPRECONDITIONER_HPP

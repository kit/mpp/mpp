#ifndef _PRECONDITIONER_H_
#define _PRECONDITIONER_H_

#include "Assemble.hpp"
#include "PreconditionerBase.hpp"
#include "Sparse.hpp"
#include "TimeDate.hpp"

std::string PCName(const std::string &prefix = "");

Preconditioner *GetPC();

Preconditioner *GetPC(const std::string &);

Preconditioner *GetPCByPrefix(const std::string &);

std::unique_ptr<Preconditioner> GetPCUnique();

#endif // of #ifndef _PRECONDITIONER_H_

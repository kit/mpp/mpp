#ifndef JACOBI_HPP
#define JACOBI_HPP

#include "PreconditionerBase.hpp"
#include "SparseRMatrix.hpp"

class Jacobi : public Preconditioner {
protected:
  Vector *D;
  double theta;
  bool shift_special;
public:
  Jacobi();

  void Construct(const Matrix &A) override;

  void Destruct() override;

  ~Jacobi() override { Destruct(); }

  void multiply(Vector &u, const Vector &b) const override;

  std::string Name() const override { return "Jacobi"; }

  friend std::ostream &operator<<(std::ostream &s, const Jacobi &Jac) {
    return s << Matrix(*(Jac.D));
  }
};

class DampedJacobi : public Jacobi {
  double damp;
public:
  DampedJacobi() {
    damp = 1.0;
    Config::Get("PreconditionerDamp", damp);
  }

  ~DampedJacobi() override { Destruct(); }

  void multiply(Vector &u, const Vector &b) const override;

  std::string Name() const override { return "DampedJacobi"; }
};

class BlockJacobi : public Preconditioner {
protected:
  double *dd;
  std::unordered_map<int, int> dd_entry;
  int blocksize;
public:
  BlockJacobi();

  void Construct(const Matrix &A) override;

  void Construct_Matrix_free(const Matrix &A);

  void Destruct() override;

  ~BlockJacobi() override { Destruct(); }

  void multiply(Vector &u, const Vector &b) const override;

  void multiply_transpose(Vector &u, const Vector &b) const override;

  std::string Name() const override { return "BlockJacobi"; }
};

class PointBlockJacobi : public Preconditioner {
protected:
  std::vector<double> dd;
  std::vector<size_t> dd_entry;
  double theta;
  bool LAPACKFlag = false;
public:
  PointBlockJacobi();

  void apply(size_t n, double *u, const double *a, const double *b) const;

  void Construct(const Matrix &A) override;

  void Destruct() override;

  ~PointBlockJacobi() override { Destruct(); }

  void multiply(Vector &u, const Vector &b) const override;

  void multiply_transpose(Vector &u, const Vector &b) const override;

  std::string Name() const override { return "PointBlockJacobi"; }
};

class PointBlockJacobi_dG : public Preconditioner {
protected:
  Matrix *D;
  double theta;
public:
  PointBlockJacobi_dG();

  void Construct(const Matrix &A) override;

  void Destruct() override;

  ~PointBlockJacobi_dG() override { Destruct(); }

  void multiply(Vector &u, const Vector &b) const override;

  std::string Name() const override { return "PointBlockJacobi_dG"; }

  friend std::ostream &operator<<(std::ostream &s, const PointBlockJacobi_dG &Jac) {
    return s << *(Jac.D);
  }
};

class CellWiseJacobi : public Preconditioner {
protected:
  std::unordered_map<Point, SparseRMatrix> InvMap;
  double theta;
public:
  CellWiseJacobi();

  void Construct(const Matrix &A) override;

  void Destruct() override;

  ~CellWiseJacobi() override { Destruct(); }

  void multiply(Vector &u, const Vector &b) const override;

  std::string Name() const override { return "CellWiseJacobi"; }
};
#endif // JACOBI_HPP

#ifndef RICHARDSON_HPP
#define RICHARDSON_HPP

#include <format>

#include "PreconditionerBase.hpp"

class Richardson : public Preconditioner {
  double damp;
public:
  Richardson() {
    damp = 1.0;
    Config::Get("PreconditionerDamp", damp);
  }

  void Construct(const Matrix &A) override;

  void multiply(Vector &u, const Vector &b) const override;

  virtual std::string Name() const { return std::format("Richardson[Damp={}]", damp); }

  void Destruct() override;

  ~Richardson() override { Destruct(); }
};

#endif

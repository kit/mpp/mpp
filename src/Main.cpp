#include "Main.hpp"
#include "AdaptiveStudy.hpp"
#include "ConvergenceStudy.hpp"
#include "SingleExperiment.hpp"
#include "m++.hpp"

int main(int argv, char **argc) {
  Config::SetConfPath(std::string(ProjectMppDir) + "/conf/");
  Mpp::initialize(&argv, argc);

  Config::PrintInfo();

  std::string run = "single";
  Config::Get("run", run);

  if (run == "single") RunSingleExperimentForValues(PDESolverConfig());
  else if (run == "convergence") RunConvergenceExperimentForValues(PDESolverConfig());
  else if (run == "adaptive") RunAdaptiveExperimentForValues(PDESolverConfig(), AdaptiveConfig());
  else if (run == "adaptive_convergence") ; // TODO

#if USE_SPACETIME
  else if (run == "FWIST") FWIExperimentST();
#endif

#if BUILD_UQ
  else if (run == "SGD") {
    if constexpr (AGGREGATE_FOR_SOLUTION) {
      SGDExperiment();
    }
  }
  else if (run == "MLMC") MLMCExperiment();
  else if (run == "MC") MCExperiment();
  else if (run == "SMC") SMCExperiment();
#endif
  return 0;
}

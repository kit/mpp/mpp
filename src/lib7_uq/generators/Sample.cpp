#include "Sample.hpp"

std::map<std::string, double> SampleData::createQoIs(const Solution &sol, const std::string &key) {
  std::map<std::string, double> qoIs;
  for (const auto &[qoi, value] : sol.values) {
    auto result = qoIs.insert(std::make_pair(std::format("{}({})", qoi, key), value));
    if (!result.second) { Exit(std::format("Duplicate key found: {}", key)); }
  }
  return qoIs;
}

std::ostream &operator<<(std::ostream &s, const SampleData &sol) {
  s << sol.id << " ";
  for (const auto &pair : sol.qoIs) {
    s << pair.first << "=" << pair.second << " ";
  }
  return s;
}

std::vector<std::string> SampleData::GetAllRdFKeys() const {
  std::vector<std::string> keys;
  for (const auto &[key, value] : randFields) {
    keys.emplace_back(key);
  }
  return keys;
}

void SampleData::PrintInfo() const {
  std::vector<InfoEntry<double>> e;
  for (const auto &[key, qoI]: qoIs) {
    e.emplace_back(InfoEntry(key, qoI));
  }
  mout.PrintInfo("SampleData", 1, e);
}

std::vector<std::string> SampleData::GetAllQoIKeys() const {
  std::vector<std::string> keys;
  for (const auto &[key, value] : qoIs) {
    keys.emplace_back(key);
  }
  return keys;
}
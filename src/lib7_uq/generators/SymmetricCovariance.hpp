#ifndef M_COVARIANCEFUNCTION_H
#define M_COVARIANCEFUNCTION_H

#include <format>
#include "Assertion.hpp"
#include "CMatrix.hpp"
#include "CVector.hpp"
#include "Config.hpp"
#include "RMatrix.hpp"
#include "RTensor.hpp"
#include "RVector.hpp"


/*
 * Todo padding should be done here
 */

RVector MOCE(RVector &toepRow, RVector &toepCol);

RMatrix BMOCE(RMatrix &toepRows, RMatrix &toepCols);

RTensor BBMOCE(RTensor &toepRows, RTensor &toepCols);

template<typename T>
class SymmetricCovariance {
public:
  virtual double Function(double *tau) = 0;

  virtual void ToeplitzMatrix(T &toepRow, T &toepCol) = 0;

  virtual T EmbeddedToeplitzMatrix(T &toepRow, T &toepCol) = 0;

  virtual ~SymmetricCovariance() {};
};

template<int SDim>
class MaternCovariance : public SymmetricCovariance<RMatrix> {
  using T = RMatrix;

  double sigma = 1.0;

  double nu;

  double lambda;

  MaternCovariance() {
    if constexpr (SDim == 1) nu = 1.5;
    if constexpr (SDim == 2) nu = 1;
    if constexpr (SDim == 3) nu = 0.5;
  }

  // Todo: std::array<double, SDim> r;
  double Function(double *tau) override {
    const double factor = std::pow(sigma, 2) / std::pow(2, 1 - nu) / std::tgamma(nu);
    const double kappa = std::sqrt(std::pow(2, SDim) * nu) / lambda;
    const double norm = std::sqrt(std::pow(kappa * tau[0], 2) + std::pow(kappa * tau[1], 2));
    return factor * std::pow(norm, nu) * std::cyl_bessel_k(nu, norm);
  }
};

class CovarianceFunction1D : public SymmetricCovariance<RVector> {
private:
  using T = RVector;

  double sigma = 1.0;

  std::vector<double> lambda = {0.3, 0.3};

  double smoothing = 1.0;

  double norm(const double *x) { return std::abs(x[0]); }

public:
  CovarianceFunction1D() {
    Config::Get("sigma", sigma);
    Config::Get("lambda", lambda);
    Config::Get("smoothing", smoothing);
  }

  CovarianceFunction1D(double sigma, double lambda, double smoothing)
    : sigma(sigma), lambda(1, lambda), smoothing(smoothing) {}

  double Function(double *tau) override {
    tau[0] /= lambda[0];
    return std::pow(sigma, 2) * std::exp(-std::pow(norm(tau), smoothing));
  }

  void ToeplitzMatrix(T &toepRow, T &toepCol) override;

  T EmbeddedToeplitzMatrix(T &toepRow, T &toepCol) override {
    ToeplitzMatrix(toepRow, toepCol);
    return MOCE(toepRow, toepCol);
  }
};

class CovarianceFunction2D : public SymmetricCovariance<RMatrix> {
public:
  using T = RMatrix;

  int norm_p = 2;

  double sigma = 1.0;

  std::vector<double> lambda = {0.15, 0.15};

  double smoothing = 1.5;

  double norm(const double *x) {
    if (norm_p == 1) return std::abs(x[0]) + std::abs(x[1]);
    if (norm_p == 2) return sqrt(std::pow(x[0], 2) + std::pow(x[1], 2));
    else Warning("p not valid, 2-norm is taken")
    return 2;
  }

  CovarianceFunction2D() {
    Config::Get("sigma", sigma);
    Config::Get("norm_p", norm_p);
    Config::Get("lambda", lambda);
    Config::Get("smoothing", smoothing);
  }

  CovarianceFunction2D(double sigma, std::vector<double> lambda,
                       double smoothing, int norm_p)
    : sigma(sigma), lambda(lambda), smoothing(smoothing), norm_p(norm_p) {}

  double Function(double *tau) override {
    tau[0] /= lambda[0];
    tau[1] /= lambda[1];
//      np.power(linalg.norm(tau, ord=self.p) / self.lam, self.alpha))
    return std::pow(sigma, 2) * std::exp(-std::pow(norm(tau), smoothing));
  }

  void ToeplitzMatrix(T &toepRows, T &toepCols) override;

  T EmbeddedToeplitzMatrix(T &toepRows, T &toepCols) override {
    ToeplitzMatrix(toepRows, toepCols);
    return BMOCE(toepRows, toepCols);
  }

  std::string ToString() const {
    return std::format("norm_p={}  sigma={}  lambda={}  lambda={}  smoothing={}", norm_p, sigma,
                       lambda[0], lambda[1], smoothing);
  }
};


class CovarianceFunction3D : public SymmetricCovariance<RTensor> {
private:
  using T = RTensor;

  int norm_p = 2;

  double sigma = 1.0;

  std::vector<double> lambda = {0.15, 0.15, 0.15};

  double smoothing = 1.5;

  double norm(const double *x) {
    if (norm_p == 1) return std::abs(x[0]) + std::abs(x[1]) + std::abs(x[2]);
    if (norm_p == 2) return sqrt(std::pow(x[0], 2) + std::pow(x[1], 2) + std::pow(x[2], 2));
    else Warning("p not valid, 2-norm is taken")
    return 2;
  }

public:
  CovarianceFunction3D() {
    Config::Get("sigma", sigma);
    Config::Get("norm_p", norm_p);
    Config::Get("lambda", lambda);
    Config::Get("smoothing", smoothing);
  }

  CovarianceFunction3D(double sigma, std::vector<double> lambda,
                       double smoothing, int norm_p)
    : sigma(sigma), lambda(lambda), smoothing(smoothing), norm_p(norm_p) {}

  double Function(double *tau) override {
    tau[0] /= lambda[0];
    tau[1] /= lambda[1];
    tau[2] /= lambda[2];
    return std::pow(sigma, 2) * std::exp(-std::pow(norm(tau), smoothing));
  }

  void ToeplitzMatrix(T &toepRows, T &toepCols) override;
//need to look if input needs to be changed
  T EmbeddedToeplitzMatrix(T &toepRows, T &toepCols) override {
    ToeplitzMatrix(toepRows, toepCols);
    return BBMOCE(toepRows, toepCols);
  }

  std::string ToString() const {
    return std::format("norm_p={}  sigma={}  lambda={}  lambda={}  lambda={}  smoothing={}", norm_p,
                       sigma, lambda[0], lambda[1], lambda[2], smoothing);
  }
};


std::vector<double> linspace(const double &start, const double &end, int num);


#endif //M_COVARIANCEFUNCTION_H

#include <cmath>

#include "Random.hpp"

int Random::GTYPE = 1;

int Random::SEED = 985456376;

double Random::firstUniform = 0.0;

double Random::secUniform = 0.0;

double Random::firstNormal = 0.0;

double Random::secNormal = 0.0;

std::map<int, Sprng *> Random::sprngMap = {};

void Random::Initialize() {
  if (sprngMap.empty()) {
    for (int commSplit = 0; commSplit <= PPM->MaxCommSplit(); commSplit++) {
      sprngMap[commSplit] = SelectType(GTYPE);
      sprngMap[commSplit]->init_sprng(PPM->Color(commSplit), (int)pow(2, commSplit),
                                      SEED + commSplit, SPRNG_DEFAULT);
    }
  }
}

double Random::Uniform(int commSplit) { return sprngMap[commSplit]->sprng(); }

double Random::Uniform(int commSplit, double leftBnd, double rightBnd) {
  return leftBnd + Random::Uniform(commSplit) * (rightBnd - leftBnd);
}

RVector Random::Uniform(int commSplit, int size, double leftBnd, double rightBnd) {
  RVector results(size);
  for (int i = 0; i < size; i++)
    results[i] = Random::Uniform(commSplit, leftBnd, rightBnd);
  return results;
}

RMatrix Random::Uniform(int commSplit, int rows, int cols, double leftBnd, double rightBnd) {
  RMatrix results(rows, cols);
  for (int i = 0; i < rows; i++) {
    results.InsertRow(Random::Uniform(commSplit, cols, leftBnd, rightBnd), i);
  }
  return results;
}

double Random::Poisson(int commSplit, double lambda) {
  // direct method for poisson random number generation using uniform random variables
  // Luc Devroye (1986): Non-Uniform Random Variate Generation, Springer-Verlag, New York.
  double P = 1.0;
  double N = -1.0;

  do {
    P = P * Random::Uniform(commSplit);
    N = N + 1.0;
  } while (P > std::exp(-lambda));

  return N;
}

RVector Random::Poisson(int commSplit, int size, double lambda) {
  RVector results(size);
  double sum = 0.0;
  for (int i = 0; i < size; i++) {
    results[i] = Random::Poisson(commSplit, lambda);
    sum += results[i];
  }
  return results;
}

RVector Random::PoissonNormalized(int commSplit, int size, double lambda) {
  RVector results(size);
  for (int i = 0; i < size; i++) {
    results[i] = Random::Poisson(commSplit, lambda);
  }

  results = results / results.Max();

  return results;
}

RMatrix Random::Poisson(int commSplit, int rows, int cols, double lambda) {
  RMatrix results(rows, cols);
  for (int i = 0; i < rows; i++) {
    results.InsertRow(Random::Poisson(commSplit, cols, lambda), i);
  }
  return results;
}

RMatrix Random::PoissonNormalized(int commSplit, int rows, int cols, double lambda) {
  double maxRV = 1;

  RMatrix results(rows, cols);

  for (int i = 0; i < rows; i++) {
    RVector sample = Random::Poisson(commSplit, cols, lambda);
    results.InsertRow(sample, i);
    if (maxRV < sample.Max()) { maxRV = sample.Max(); }
  }

  results = results / maxRV;

  return results;
}

double Random::boxMuller(int commSplit) {
  if (firstUniform == 0.0 && secUniform == 0.0) {
    firstUniform = Random::Uniform(commSplit);
    secUniform = Random::Uniform(commSplit);
    firstNormal = sqrt(-2.0 * log(firstUniform)) * cos(2.0 * M_PI * secUniform);
    secNormal = sqrt(-2.0 * log(firstUniform)) * sin(2.0 * M_PI * secUniform);
    return firstNormal;
  } else {
    firstUniform = 0.0;
    secUniform = 0.0;
    return secNormal;
  }
}

double Random::Normal(int commSplit) { return boxMuller(commSplit); }

std::complex<double> Random::ComplexNormal(int commSplit) {
  return {Random::Normal(commSplit), Random::Normal(commSplit)};
}

RVector Random::Normal(int commSplit, int size) {
  RVector results(size);
  for (int i = 0; i < size; i++)
    results[i] = Random::Normal(commSplit);
  return results;
}

CVector Random::ComplexNormal(int commSplit, int size) {
  CVector results(size);
  for (int i = 0; i < size; i++)
    results[i] = Random::ComplexNormal(commSplit);
  return results;
}

RMatrix Random::Normal(int commSplit, int rows, int cols) {
  RMatrix results(rows, cols);
  for (int i = 0; i < rows; i++) {
    results.InsertRow(Random::Normal(commSplit, cols), i);
  }
  return results;
}

CMatrix Random::ComplexNormal(int commSplit, int rows, int cols) {
  CMatrix results(rows, cols);
  for (int i = 0; i < rows; i++) {
    results.InsertRow(Random::ComplexNormal(commSplit, cols), i);
  }
  return results;
}

RTensor Random::Normal(int commSplit, int fstComp, int secComp, int thirdComp) {
  RTensor results(fstComp, secComp, thirdComp);
  for (int i = 0; i < thirdComp; i++) {
    results.InsertMatrixThirdDimension(Random::Normal(commSplit, fstComp, secComp), i);
  }
  return results;
}

CTensor Random::ComplexNormal(int commSplit, int fstComp, int secComp, int thirdComp) {
  CTensor results(fstComp, secComp, thirdComp);
  for (int i = 0; i < thirdComp; i++) {
    results.InsertMatrixThirdDimension(Random::ComplexNormal(commSplit, fstComp, secComp), i);
  }
  return results;
}

Vector::Vector(std::shared_ptr<const IDiscretization> disc, MeshIndex levels, DIST_TYPE distType) :
    VectorMatrixBase(std::move(disc), levels),
    dirichletFlags(std::make_shared<DirichletFlags>(VectorMatrixBase::size())),
    bcEquations(nullptr) {
  switch (distType) {
  case DIST_TYPE::NORMAL:
    data = Random::Normal(PPM->MaxCommSplit(), VectorMatrixBase::size());
    break;
  case DIST_TYPE::UNIFORM:
    data = Random::Uniform(PPM->MaxCommSplit(), VectorMatrixBase::size(), 0, 1);
    break;
  case DIST_TYPE::POISSON:
    data = Random::Poisson(PPM->MaxCommSplit(), VectorMatrixBase::size(), 1.);
    break;
  case DIST_TYPE::POISSONNORM:
    data = Random::PoissonNormalized(PPM->MaxCommSplit(), VectorMatrixBase::size(), 1.);
    break;
  case DIST_TYPE::COMPLEX_NORMAL:
    Exit("Not implemented, this should be generalized anyways")
  }
  MakeConsistent();
  this->accumulateFlag = true;
}

Vector::Vector(std::shared_ptr<const IDiscretization> disc, MeshIndex levels, DIST_TYPE distType,
               std::vector<double> params) :
    VectorMatrixBase(std::move(disc), levels),
    dirichletFlags(std::make_shared<DirichletFlags>(VectorMatrixBase::size())),
    bcEquations(nullptr) {
  switch (distType) {
  case DIST_TYPE::UNIFORM:
    data = Random::Uniform(PPM->MaxCommSplit(), VectorMatrixBase::size(), params[0], params[1]);
    break;
  case DIST_TYPE::POISSON:
    data = Random::Poisson(PPM->MaxCommSplit(), VectorMatrixBase::size(), params[0]);
    break;
  case DIST_TYPE::POISSONNORM:
    data = Random::PoissonNormalized(PPM->MaxCommSplit(), VectorMatrixBase::size(), params[0]);
    break;
  }
  MakeConsistent();
  this->accumulateFlag = true;
}
#ifndef SAMPLE_HPP
#define SAMPLE_HPP

#include <format>
#include <string>
#include <utility>
#include "PDESolver.hpp"

struct EllipticSolution;

struct SampleID {
  int fLevel{};

  int cLevel{};

  int number{};

  bool coarse{};

  int commSplit{};

  SampleID() = default;

  SampleID(int level, int number, bool coarse) :
      fLevel(level), cLevel((level > 0) ? (level - 1) : 0), number(number), coarse(coarse) {}

  SampleID(int level, int number, bool coarse, int commSplit) :
      fLevel(level), cLevel((level > 0) ? (level - 1) : 0), number(number), coarse(coarse),
      commSplit(commSplit) {}

  SampleID WithCommSplit(int _commSplit) {
    this->commSplit = _commSplit;
    return *this;
  }

  MeshIndex GetMeshIndex() const {
#ifdef USE_SPACETIME
    if (coarse) return {cLevel, cLevel};
    else return {fLevel, fLevel};
#else
    if (coarse) return {cLevel, -1, 0, commSplit};
    else return {fLevel, -1, 0, commSplit};
#endif
  }

  std::string IdString() const { return std::format(".{}.{}.{}", fLevel, (int)coarse, number); }

  friend std::ostream &operator<<(std::ostream &s, const SampleID &id) {
    return s << id.IdString();
  }
};

// Todo: make class and only provide the necessary functions
struct SampleData {
  double C = 0.0; // Cost to compute sample solution

  //  std::map<std::string, double> costs; Todo?

  SampleID id;

  double weight = 1.0;

  std::map<std::string, Vector> randFields;

  std::map<std::string, double> qoIs;

  template<typename T>
  SampleData &Insert(const std::string &key, const T &value) {
    if constexpr (std::is_same_v<T, Vector>) {
      auto result = randFields.insert(std::make_pair(key, value));
      if (!result.second) {                                //
        Exit(std::format("Duplicate key found: {}", key)); //
      }
    } else if constexpr (std::is_same_v<T, Solution> || std::is_same_v<T, EllipticSolution>) {
      auto result = randFields.insert(std::make_pair(key, value.vector));
      if (!result.second) {                                //
        Exit(std::format("Duplicate key found: {}", key)); //
      }
      if (!qoIs.empty()) {
        for (const auto &[qoi, valueQoI] : value.values) {
          auto resultQoI = qoIs.insert(std::make_pair(std::format("{}({})", qoi, key), valueQoI));
          if (!resultQoI.second) { Exit(std::format("Duplicate key found: {}", key)); }
        }
      } else {
        qoIs = createQoIs(value, key);
      }
    } else {
      auto result = qoIs.insert(std::make_pair(key, value));
      if (!result.second) {                                //
        Exit(std::format("Duplicate key found: {}", key)); //
      }
    }
    return *this;
  }

  template<typename T>
  void Append(const std::string &key, const T &value) {
    if constexpr (std::is_same_v<T, Vector>) {
      auto result = randFields.insert(std::make_pair(key, value));
      if (!result.second) {                                //
        Exit(std::format("Duplicate key found: {}", key)); //
      }
    } else if constexpr (std::is_same_v<T, Solution>) {
      auto resultRdF = randFields.insert(key, value.vector);
      if (!resultRdF.second) {                             //
        Exit(std::format("Duplicate key found: {}", key)); //
      }
      for (const auto &[qoi, valueQoI] : value.values) {
        auto resultQoI = qoIs.insert(std::make_pair(std::format("{}({})", qoi, key), valueQoI));
        if (!resultQoI.second) { Exit(std::format("Duplicate key found: {}", key)); }
      }
    } else {
      auto result = qoIs.insert(std::make_pair(key, value));
      if (!result.second) {                                //
        Exit(std::format("Duplicate key found: {}", key)); //
      }
    }
  }

  template<typename T>
  void Erase(const std::string &key) {
    if constexpr (std::is_same_v<T, Vector>) {
      auto it = randFields.find(key);
      if (it == randFields.end()) {                  //
        Exit(std::format("Key not found: {}", key)); //
      }
      randFields.erase(key);
    } else if constexpr (std::is_same_v<T, double>) {
      auto it = qoIs.find(key);
      if (it == qoIs.end()) {                        //
        Exit(std::format("Key not found: {}", key)); //
      }
      randFields.erase(key);
    } else {
      Exit("Type not supported");
    }
  }

  template<typename T>
  const T &Get(const std::string &key) const {
    if constexpr (std::is_same_v<T, Vector>) {
      auto it = randFields.find(key);
      if (it == randFields.end()) {                  //
        Exit(std::format("Key not found: {}", key)); //
      }
      return it->second;
    } else if constexpr (std::is_same_v<T, double>) {
      auto it = qoIs.find(key);
      if (it == qoIs.end()) {                        //
        Exit(std::format("Key not found: {}", key)); //
      }
      return it->second;
    } else {
      Exit("Type not supported");
    }
  }

  template<typename T>
  const std::map<std::string, T> &Map() const {
    if constexpr (std::is_same_v<T, Vector>) {
      return randFields;
    } else if constexpr (std::is_same_v<T, double>) {
      return qoIs;
    } else {
      Exit("Type not supported");
    }
  }

  std::vector<std::string> GetAllQoIKeys() const;

  std::vector<std::string> GetAllRdFKeys() const;

  SampleData() = default;

  SampleData &operator=(const SampleData &other) {
    if (this == &other) return *this; // self-assignment check
    C = other.C;
    weight = other.weight;
    id = other.id;
    randFields = other.randFields;
    qoIs = other.qoIs;
    return *this;
  }

  SampleData(SampleID id) : id(std::move(id)) {}

  const SampleID &Id() const { return id; }

  std::string IdString() const { return id.IdString(); }

  SampleData(const SampleData &sampleData) :
      C(sampleData.C), weight(sampleData.weight), id(sampleData.id),
      randFields(sampleData.randFields), qoIs(sampleData.qoIs) {}

  SampleData(SampleData &&sampleData) :
      C(sampleData.C), weight(sampleData.weight), id(std::move(sampleData.id)),
      randFields(std::move(sampleData.randFields)), qoIs(std::move(sampleData.qoIs)) {}

  SampleData(const std::string &key, const SampleID &id, double qoi) : id(id), qoIs({{key, qoi}}) {}

  SampleData(const std::string &key, const SampleID &id,
             std::shared_ptr<const IDiscretization> disc) :
      id(id), randFields({{key, Vector(0.0, disc, id.GetMeshIndex())}}) {}

  SampleData(const std::string &key, const SampleID &id, const Vector &u) :
      id(id), randFields({{key, u}}) {}

  SampleData(const std::string &key, const SampleID &id, const Solution &solution) :
      id(id), randFields({{key, solution.vector}}), qoIs(createQoIs(solution, key)) {}

  friend std::ostream &operator<<(std::ostream &s, const SampleData &sol);

  void PrintInfo() const;

private:
  static std::map<std::string, double> createQoIs(const Solution &sol, const std::string &key);
};

typedef std::optional<SampleData> OptionalSampleData;

typedef std::optional<Vector> OptionalVector;

#endif // SAMPLE_HPP

#include "Optimizer.hpp"

Vector Optimizer::admissibleMapping(const Vector &control) const {
  Vector admissibleControl(control); // Todo: Check for DGElements
  for (row r = control.rows(); r != control.rows_end(); ++r) {
    for (int indexOfDof = 0; indexOfDof < r.NumberOfDofs(); indexOfDof++) {
      if (control(r, indexOfDof) < sgdConf.admissibleLowerBound) {
        admissibleControl(r, indexOfDof) = sgdConf.admissibleLowerBound;
      } else if (control(r, indexOfDof) > sgdConf.admissibleUpperBound) {
        admissibleControl(r, indexOfDof) = sgdConf.admissibleUpperBound;
      }
    }
  }
  return admissibleControl;
}

Vector Optimizer::prolongateOnNewEstimate(const Vector &newEstimate, const Vector &control) const {
  if (newEstimate.Level() == control.Level()) {
    return control;
  } else {
    Vector newControl(newEstimate);
    GetTransfer(control, newControl)->Prolongate(control, newControl);
    return newControl;
  }
}

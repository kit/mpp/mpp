#include "ContinuationData.hpp"

void ContinuationExponents::ComputeDelta(const ContinuationData &contData) {
  const auto &_rmseY = contData.rmse;

  std::vector<double> logRelBudget, logRmseY, logRmseV;
  for (int i = 1; i < logRelBudget.size(); i++) {
    logRelBudget.push_back(std::log2(contData.relTimeCosts[i]));
    logRmseY.push_back(-std::log2(logRmseY[i]));
    logRmseV.push_back(-std::log2(logRmseV[i]));
  }

  deltaCT = ft::linearFit(logRelBudget, logRmseY).first;
  deltaMem = ft::linearFit(logRelBudget, logRmseV).first;
}

std::vector<int> ContinuationData::computeDeltaOfSamplesPerLevel(const EstimatorMap &estMap) {
  auto sizeDeltas = samplesPerLevel.empty() ? 0 : samplesPerLevel.back().size();
  std::vector<int> sampleDeltas(std::max(estMap.GetMVector().size(), sizeDeltas));
  for (int level = 0; level < sampleDeltas.size(); level++) {
    int samplesSoFar = 0;
    for (const auto &samples : samplesPerLevel) {
      samplesSoFar += samples[level];
    }
    sampleDeltas[level] = estMap.GetMVector()[level] - samplesSoFar;
  }
  return sampleDeltas;
}

void ContinuationData::computeDelta() {}

void ContinuationData::PrintInfo() const {
  if (IsEmpty()) return;

  mout.PrintInfo("Continuation", 1,                              //
                 InfoEntry("Estimation Rounds", Size()),         //
                 InfoEntry("CommSplits", commSplits),            //
                 InfoEntry("SamplesPerLevel", samplesPerLevel)); //

  std::vector<InfoEntry<std::vector<double>>> e;
  e.emplace_back(InfoEntry("Epsilons", epsilons));
  e.emplace_back(InfoEntry("TimeCosts", timeCosts));
  e.emplace_back(InfoEntry("MemoCosts", memoCosts));
  e.emplace_back(InfoEntry("RelTimeCosts", relTimeCosts));
  e.emplace_back(InfoEntry("RelMemoCosts", relMemoCosts));
  for (const auto &[key, err] : num) {
    e.emplace_back(InfoEntry(std::format("RMSE({})", key), rmse.at(key)));
    e.emplace_back(InfoEntry(std::format("MSE({})", key), mse.at(key)));
    e.emplace_back(InfoEntry(std::format("NUM({})", key), num.at(key)));
    e.emplace_back(InfoEntry(std::format("SAM({})", key), sam.at(key)));
  }
  mout.PrintInfo("ContinuationData", 1, e);
}
#ifndef MLMC_MC_HPP
#define MLMC_MC_HPP

#include <utility>
#include "ConfigStructsUQ.hpp"
#include "MeshesCreator.hpp"
#include "MultiXFEM.hpp"
#include "WelfordAggregate.hpp"

/*
 * Estimator interface for SingleLevelEstimator and MultiLevelEstimator
 */

class Estimator {
public:
  virtual const SampleData WithdrawEstimate() = 0;

  virtual Estimator &Method(const OptionalVector &input) = 0;

  virtual void UpdateSampleAmount(std::vector<int> levels, std::vector<int> requestedSamples) {};

  virtual void UpdateSampleAmount(int requestedSamples) {};

  virtual void UpdateEpsilon(double epsilon) {};

  virtual ~Estimator() = default;
};

/*
 * A SingleLevelEstimator can represent a Monte Carlo,
 *    quasi-Monte Carlo or a Stochastic Collocation method
 *    depending on the sampling protocol defined in MultiXFEM
 */

class SingleLevelEstimator : public Estimator {
private:
  int plotting = 0; // 0 is no plotting,
                    // 1 is after total update
  int verbose = 1;  // 2 is after parallel update
                    // 3 is after comm update
  int dM = 0;       // -1 is only after the first total update
                    // -2 is only after the first parallel and total update
  int dMcomm = 0;   // -3 is only after the first comm, parallel and total update

  int commSplit = 0;

  WelfordAggregate aggregate;

  SLEstimatorConfig slEstmConf;

  std::shared_ptr<MultiXFEM> mxFEM;

  int maxSamplesByMem = std::numeric_limits<int>::max();

  const int &initLevel = slEstmConf.initLevel;

  const bool &parallel = slEstmConf.parallel;

  void updateSamplesIfNeeded();

  void updateOnComm(const SampleData &fData, const OptionalSampleData &cData);
public:
  // Another constructor hell /\ //
  explicit SingleLevelEstimator(WelfordAggregate aggregate) :
      SingleLevelEstimator(SLEstimatorConfig(), std::move(aggregate)) {}

  explicit SingleLevelEstimator(const SLEstimatorConfig &slEstmConf,
                                std::shared_ptr<MultiXFEM> mxFEM = nullptr) :
      SingleLevelEstimator(slEstmConf, WelfordAggregate(), std::move(mxFEM)) {}

  SingleLevelEstimator(const SingleLevelEstimator &singleLevelEstimator) :
      SingleLevelEstimator(singleLevelEstimator.slEstmConf, singleLevelEstimator.aggregate) {}

  SingleLevelEstimator() : SingleLevelEstimator(SLEstimatorConfig(), WelfordAggregate()) {}

  SingleLevelEstimator(const SLEstimatorConfig &slEstmConf, WelfordAggregate aggregate,
                       std::shared_ptr<MultiXFEM> mxFEM = nullptr) :
      slEstmConf(slEstmConf), aggregate(std::move(aggregate)),
      mxFEM(mxFEM ? std::move(mxFEM) :
                    CreateSharedMXFEM(this->slEstmConf.mxFEMConfig
                                          .WithCoarseLevel(slEstmConf.onlyFine ?
                                                               slEstmConf.initLevel :
                                                               slEstmConf.initLevel - 1)
                                          .WithFineLevel(slEstmConf.initLevel))) {

    double memBeforeConstruction = MemoryLogger::MemoryUsage();
    maxSamplesByMem = slEstmConf.maxSamplesByMem;

    Config::Get("SLEstimatorVerbose", verbose);
    Config::Get("SLEstimatorPlotting", plotting);

    UpdateSampleAmount(slEstmConf.initSamples);

    aggregate.UpdateMemoryCost(MemoryLogger::MemoryUsage() - memBeforeConstruction); //
    vout(2) << "Additional memory usage in constructor " << DOUT(aggregate.memoCost) //
            << " in level " << initLevel << endl;                                    //
  }

  Estimator &Method(const OptionalVector &input) override;

  const SampleData WithdrawEstimate() override;

  void UpdateSampleAmount(int requestedSamples) override;

  void UpdateSampleAmount(int requestedSamples, int maxSamplesByMem);

  void ReInitAggregate() { aggregate = WelfordAggregate(); }

  void UpdateCost(double newTimeCost, double newMemoryCost);

  void ResetExpectedMemoryCost();

  void UpdateParallel();

  void UpdateErrors();

  void UpdateTotal();

  // Const methods
  int Index() const;

  int CommSplit() const { return commSplit; }

  int SamplesToCompute() const { return dM; }

  size_t CellsOn(const MeshIndex &meshIndex) const;

  int Level() const { return slEstmConf.initLevel; }

  int SamplesToComputeOnComm() const { return dMcomm; }

  int MaxSamplesByMem() const { return maxSamplesByMem; }

  std::shared_ptr<MultiXFEM> GetMXFEM() const { return mxFEM; }

  bool MXFEMIsInitialized() const { return (mxFEM != nullptr); }

  const WelfordAggregate &GetAggregate() const { return aggregate; }

  static std::string Name() { return "SingleLevelEstimator"; }

  void PrintInfo() const;
};


#endif // MLMC_MC_HPP
#include "MultiXFEM.hpp"
#include "ForwardSampling.hpp"
#include "OptimalControlSampling.hpp"
#include "SPDESampling.hpp"
#include "TestSampling.hpp"

#if USE_TASMANIAN
#include "SparseGridSampling.hpp"
#endif


std::shared_ptr<MultiXFEM> CreateSharedMXFEM(const MultiXFEMConfig &conf) {
  if (conf.name == "StochasticLaplace1D")                              // 1D Forward
    return std::make_shared<StochasticLaplace1D>(conf);                //
  if (conf.name == "SPDESamplingOnInterval")                           //
    return std::make_shared<SPDESamplingOnInterval>(conf);             //
                                                                       //
  if (conf.name == "StochasticLaplace2D")                              // 2D Forward
    return std::make_shared<StochasticLaplace2D>(conf);                //
  if (conf.name == "StochasticLaplace2DTest")                          //
    return std::make_shared<StochasticLaplace2DTest>(conf);            //
  if (conf.name == "UniformDistributionLaplace2D")                     //
    return std::make_shared<UniformDistributionLaplace2D>(conf);       //
                                                                       //
  if (conf.name == "SPDESamplingOnSquare")                             // 2D Forward with SPDE
    return std::make_shared<SPDESamplingOnSquare>(conf);               //
  if (conf.name == "EllipticDarcyLogNormalSPDE2D")                     //
    return std::make_shared<EllipticDarcyLogNormalSPDE2D>(conf);       //
  if (conf.name == "MixedDarcyLogNormalSPDE2D")                        //
    return std::make_shared<MixedDarcyLogNormalSPDE2D>(conf);          //
  if (conf.name == "TransportMixedDarcyLogNormalSPDE2D")               //
    return std::make_shared<TransportMixedDarcyLogNormalSPDE2D>(conf); //
                                                                       //
  if (conf.name == "OCStochasticLaplace2D")                            // 2D with optimal control
    return std::make_shared<OCStochasticLaplace2D>(conf);              //
  if (conf.name == "OCLaplace2D")                                      //
    return std::make_shared<OCLaplace2D>(conf);                        //
  if (conf.name == "OCRiemann2D")                                      //
    return std::make_shared<OCRiemann2D>(conf);                        //
  //  if (conf.problemName == "OCSquare500")                           // Todo include again
  //    return std::make_shared<OCSquare500>(conf);                    //
  //  if (conf.problemName == "OCStochasticTransport") //
  //    return std::make_shared<OCStochasticTransport>(conf);        //

  if (conf.name == "StochasticLaplace3DWithSPDESampling")               // 3D Forward with SPDE
    return std::make_shared<StochasticLaplace3DWithSPDESampling>(conf); //
  if (conf.name == "SPDESamplingOnHexahedron")                          //
    return std::make_shared<SPDESamplingOnHexahedron>(conf);            //
                                                                        //
  if (conf.name == "ProcEvaluationMXFEM")                               // Test classes for
    return std::make_shared<ProcEvaluationMXFEM>(conf);                 // TestSingleLevelEstimator
  if (conf.name == "ZeroEvaluationMXFEM")                               //
    return std::make_shared<ZeroEvaluationMXFEM>(conf);                 //
  if (conf.name == "ModuloIndexEvaluationMXFEM")                        //
    return std::make_shared<ModuloIndexEvaluationMXFEM>(conf);          //
  if (conf.name == "NormalLagrangeMXFEM")                               //
    return std::make_shared<NormalLagrangeMXFEM>(conf);                 //
  if (conf.name == "NormalDGMXFEM")                                     //
    return std::make_shared<NormalDGMXFEM>(conf);                       //

#if USE_TASMANIAN
  if (conf.name == "StochasticSparseGridLaplace2D")
    return std::make_shared<StochasticSparseGridLaplace2D>(conf);
#endif

  return nullptr;
}

std::ostream &operator<<(std::ostream &s, const MultiXFEMConfig &conf) {
  return s << DOUT(conf.fLevel) << DOUT(conf.cLevel) << DOUT(conf.commSplit) << DOUT(conf.name);
}

OptionalVector MultiXFEM::distributeInput(const OptionalVector &opt, const MeshIndex &meshIndex) {
  (*meshes)[MeshIndex(meshIndex)];               // creates mesh on requested MeshIndex
  if (opt == std::nullopt) return std::nullopt;  // returns if no input is given
  const Vector &input = opt.value();             // set reference input on optional Vector
  const int &maxCommSplit = PPM->MaxCommSplit(); // short notation
  const int &commSplit = meshIndex.commSplit;    // short notation

  // Todo: This seems wrong as it is defined on pair:
  // (input_level, requested_commSplit) which can be too large if input has large level
  Vector dist(0.0, input.GetSharedDisc(), input.Level().WithCommSplit(commSplit));

  if (meshIndex == input.Level()) return opt;

  if constexpr (DebugLevel > 1) {
    const int &proc = PPM->Proc(0);
    const int &distLevel = dist.SpaceLevel();
    const int &inputLevel = input.SpaceLevel();
    mpp::PlotData("U", std::format("Contr.Input.{}.{}", inputLevel, input.CommSplit()), input);
    mpp::PlotData("U", std::format("Contr.Dist.{}.{}.{}", distLevel, commSplit, proc), dist);
  }

  for (row r = dist.rows(); r != dist.rows_end(); r++) {
    for (int indexOfDof = 0; indexOfDof < r.NumberOfDofs(); indexOfDof++) {
      if (input.find_row(r()) != input.rows_end()) {
        dist(r(), indexOfDof) = input(r(), indexOfDof);
      }
    }
  }

  if constexpr (DebugLevel > 1) {
    const int &proc = PPM->Proc(0);
    const int &distLevel = dist.SpaceLevel();
    mpp::PlotData("U", std::format("Contr.Set.{}.{}.{}", distLevel, commSplit, proc), dist);
  }

  // Todo: Is meaningful, this has to be checked!
  // Or rather just selection?
  //    for (auto row = out.rows(); row != out.rows_end(); row++) {
  //      for (int indexOfDof = 0; indexOfDof < row.NumberOfDofs(); indexOfDof++) {
  //        out(row, indexOfDof) = dist(row, indexOfDof);
  //      }
  //    }

  // clang-format off - The block below transfers the input to the correct level
  Vector out(0.0, dist.GetSharedDisc(), meshIndex);
  if (meshIndex.space < input.SpaceLevel()) {
    GetTransfer(out, dist)->Restrict(out, dist);
  } else if (meshIndex.space > input.SpaceLevel()) {
    GetTransfer(dist, out)->Prolongate(dist, out);
  } else {
    out = dist;
  }
  // clang-format on


  if constexpr (DebugLevel > 1) {
    const int &proc = PPM->Proc(0);
    const int &outLevel = meshIndex.space;
    mpp::PlotData("U", std::format("Contr.Trans.{}.{}.{}", outLevel, commSplit, proc), out);
  }

  for (int distLevel = maxCommSplit - commSplit; distLevel <= maxCommSplit - 1; distLevel++) {
    for (auto row = out.rows(); row != out.rows_end(); row++) {
      for (int indexOfDof = 0; indexOfDof < row.NumberOfDofs(); indexOfDof++) {
        out(row, indexOfDof) = PPM->SumOn(out(row, indexOfDof), 0, distLevel);

        if constexpr (DebugLevel > 1) {
          const int &d = distLevel;
          const int &proc = PPM->Proc(0);
          const int &outLevel = meshIndex.space;
          mpp::PlotData("U", std::format("Contr.Sum.{}.{}.{}.{}", outLevel, commSplit, proc, d),
                        out);
        }
      }
    }
  }

  if constexpr (DebugLevel > 1) {
    const int level = out.SpaceLevel();
    const int proc = PPM->Proc(0);
    mpp::PlotData("U", std::format("Contr.Out.{}.{}.{}", level, commSplit, proc), out);
  }

  return std::make_optional(std::move(out));
}

#ifndef LEVELMAPS_HPP
#define LEVELMAPS_HPP

#include <cmath>
#include <limits>
#include <map>
#include <utility>
#include <vector>

#include "ErrorData.hpp"
#include "SingleLevelEstimator.hpp"

class EstimatorMap;

struct Exponents {
  std::map<std::string, double> alpha;       // this one is for qoIs
  std::map<std::string, double> alphaInner;  // this one is just for testing
  std::map<std::string, double> alphaOuter;  // this one is for rdFs
                                             //
  std::map<std::string, double> beta;        // this one is for qoIs
  std::map<std::string, double> betaInner;   // this one is just for testing
  std::map<std::string, double> betaOuter;   // this one is just for testing
  std::map<std::string, double> betaBochner; // this one is for rdFs

  double gammaCT = 0.0;
  double gammaMem = 0.0;

  void ComputeAlpha(const EstimatorMap &mcMap);

  void ComputeBeta(const EstimatorMap &mcMap);

  void ComputeGamma(const EstimatorMap &mcMap);

  double GetObjAlpha(const std::string &key) const;

  double GetObjBeta(const std::string &key) const;

  double GetMemoGamma(const std::string &key) const;

  double GetTimeGamma(const std::string &key) const;

  friend std::ostream &operator<<(std::ostream &s, const Exponents &exponents);
private:
  double regression(const std::vector<int> &levels, const std::vector<double> &data, double factor);
};

class EstimatorMap {
private:
  friend class TestEstimatorMap;

  Exponents exponents;

  MultilevelErrors errors;

  MLEstimatorConfig mlConf;

  double memoryOffset = 0.0;

  std::shared_ptr<MultiXFEM> mxFEM;

  std::map<int, SingleLevelEstimator> estimators{};

  std::map<int, SingleLevelEstimator> fill();

  std::map<int, SingleLevelEstimator> fill(const std::map<int, WelfordAggregate> &aggregateMap);
public:
  EstimatorMap() : mlConf(MLEstimatorConfig()) {};

  explicit EstimatorMap(MLEstimatorConfig mlEstmConf) :
      mlConf(std::move(mlEstmConf)),
      mxFEM(CreateSharedMXFEM(
          this->mlConf.slEstmConf.mxFEMConfig.WithCoarseLevel(this->mlConf.initLevels.at(0))
              .WithFineLevel(this->mlConf.initLevels.back()))),
      estimators(fill()) {};

  explicit EstimatorMap(const std::map<int, WelfordAggregate> &aggregateMap,
                        MLEstimatorConfig mlEstmConf = MLEstimatorConfig()) :
      mlConf(std::move(mlEstmConf)), estimators(fill(aggregateMap)) {};

  bool OpenSamples();

  double TimeCost() const;

  double MemoryCost() const;

  double MemoryOffset() const;

  void PostSmoothing();

  void ResetSampleAmount();

  void UpdateExponentsAndErrors();

  SampleData GetEstimate(const SampleID &id = SampleID()) const;

  double TimeCostPrediction() const;

  double MemoCostPrediction() const;

  double MemoryConstant() const;

  double SynchronizationEfficiency() const;

  double CostWithoutSyncLosses() const;

  double SampleCounterFactor(double epsilon, double theta);

  void UpdateSampleCounterOnMap(double epsilon, double theta);

  void UpdateSampleCounterOnMap(std::vector<int> samples);

  int MaxSamplesByMem(const SingleLevelEstimator &estimator) const;

  MLEstimatorConfig GetMLEstimatorConfig() const { return mlConf; };

  int MaxSamplesByMem(double memOnLevel) const;

  int OptimalMOnLevel(int level, double epsilon, double theta);

  int OptimalMOnLevel(double samErr, double meanC, double epsilon, double theta);

  bool UpdateNewLevel(double epsilon, double theta, int newLevel);

  void UpdateMemoryOffset(double totalMemory);

  const MultilevelErrors &GetErrors() const { return errors; };

  const Exponents &GetExponents() const { return exponents; };

#ifdef AGGREGATE_FOR_SOLUTION
  Vector MeanField(const std::string &key) const;

  Vector SVarField(const std::string &key) const;
#endif

  auto clear() { estimators.clear(); }

  auto end() { return estimators.end(); }

  auto rend() { return estimators.rend(); }

  auto begin() { return estimators.begin(); }

  auto rbegin() { return estimators.rbegin(); }

  auto size() const { return estimators.size(); }

  auto end() const { return estimators.end(); }

  auto rend() const { return estimators.end(); }

  auto begin() const { return estimators.begin(); }

  auto rbegin() const { return estimators.rbegin(); }

  auto operator[](int level) { return estimators[level]; }

  auto find(int level) { return estimators.find(level); }

  auto at(int level) const { return estimators.at(level); }

  auto erase(int level) { return estimators.erase(level); }

  auto insert(std::pair<int, SingleLevelEstimator> &&pair) { estimators.insert(pair); }

  friend std::ostream &operator<<(std::ostream &s, const EstimatorMap &estMap);

  /*
   * GetKeys functions
   */

  std::vector<std::string> GetRdFKeys() const;

  std::vector<std::string> GetAllRdFKeys() const;

  std::vector<std::string> GetDelRdFKeys() const;

  std::vector<std::string> GetQoIKeys() const;

  std::vector<std::string> GetAllQoIKeys() const;

  std::vector<std::string> GetDelQoIKeys() const;

  std::vector<std::string> GetOutKeys() const;

  std::vector<std::string> GetAllOutKeys() const;

  std::vector<std::string> GetDelOutKeys() const;

  std::vector<std::string> GetInKeys() const;

  std::vector<std::string> GetAllInKeys() const;

  std::vector<std::string> GetDelInKeys() const;

  /*
   * Start GetVector functions
   */

  std::vector<double> GetMeanOuterNormRdF(const std::string &key) const;

  std::vector<double> GetsVarOuterNormRdF(const std::string &key) const;

  std::vector<double> GetSkewOuterNormRdF(const std::string &key) const;

  std::vector<double> GetKurtOuterNormRdF(const std::string &key) const;

  std::vector<double> GetMeanOuterNormDelRdF(const std::string &key) const;

  std::vector<double> GetSVarOuterNormDelRdF(const std::string &key) const;

  std::vector<double> GetSkewOuterNormDelRdF(const std::string &key) const;

  std::vector<double> GetKurtOuterNormDelRdF(const std::string &key) const;

  // Inner U
  std::vector<double> GetMeanInnerNormRdF(const std::string &key) const;

  std::vector<double> GetSVarInnerNormRdF(const std::string &key) const;

  std::vector<double> GetSkewInnerNormRdF(const std::string &key) const;

  std::vector<double> GetKurtInnerNormRdF(const std::string &key) const;

  // Inner V
  std::vector<double> GetMeanInnerNormDelRdF(const std::string &key) const;

  std::vector<double> GetSVarInnerNormDelRdF(const std::string &key) const;

  std::vector<double> GetSkewInnerNormDelRdF(const std::string &key) const;

  std::vector<double> GetKurtInnerNormDelRdF(const std::string &key) const;

  // Bochner
  std::vector<double> GetBochnerNormRdF(const std::string &key) const;

  std::vector<double> GetBochnerNormDelRdF(const std::string &key) const;

  std::vector<double> GetBochnerNormQoI(const std::string &key) const;

  std::vector<double> GetBochnerNormDelQoI(const std::string &key) const;

  std::vector<double> GetMeanQoI(const std::string &key) const;

  std::vector<double> GetSVarQoI(const std::string &key) const;

  std::vector<double> GetSkewQoI(const std::string &key) const;

  std::vector<double> GetKurtQoI(const std::string &key) const;

  std::vector<double> GetTotalTimeCost(const std::string &key = "todo") const;

  std::vector<double> GetTotalMemoCost(const std::string &key = "todo") const;

  std::vector<double> GetMemoCostPerSample(const std::string &key = "todo") const;

  std::vector<double> GetTimeCostPerSample(const std::string &key = "todo") const;

  std::vector<int> GetOverallMaxCommSplits() const;

  std::vector<int> GetCommSplits() const;

  std::vector<int> GetLevels() const;

  std::vector<int> GetdMs() const;

  std::vector<int> GetMVector() const;

  typedef std::function<double(const SingleLevelEstimator &)> ExtractFct;

  template<typename T>
  std::vector<T> GetVector(const std::string &key, ExtractFct extractor) const {
    std::vector<T> result;
    for (auto const &[level, estimator] : estimators) {
      try {
        result.emplace_back(extractor(estimator));
      } catch (const std::out_of_range &e) { Exit(std::format("Key {} not found", key)) }
    }
    return result;
  }
};

#endif // LEVELMAPS_HPP

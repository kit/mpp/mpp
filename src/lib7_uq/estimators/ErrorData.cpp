#include "ErrorData.hpp"
#include "EstimatorMap.hpp"
#include "WelfordAggregate.hpp"

void ErrorData::EstimateErrors(const WelfordAggregate &aggregate) {
  estimateNumeric(aggregate);
  estimateStochastic(aggregate);
  iterateOverMaps(aggregate);
}

void MultilevelErrors::EstimateErrors(const EstimatorMap &mcMap) {
  estimateNumeric(mcMap);
  estimateStochastic(mcMap);
  iterateOverMaps(mcMap);
}

void ErrorData::estimateNumeric(const WelfordAggregate &aggregate) {
  for (const auto &key : aggregate.GetDelQoIKeys()) {
    // divided by (pow(2.0, alpha) - 1), alpha is assumed to be 1 since not known
    disc[key] = aggregate.qoI.at(key).GetMean();
  }
  if constexpr (AGGREGATE_FOR_SOLUTION) {
    for (const auto &key : aggregate.GetDelOutKeys()) {
      // divided by (pow(2.0, alpha) - 1), alpha is assumed to be 1 since not known
      disc[key] = aggregate.outerNorms.at(key).mean;
    }
  }
}

void ErrorData::estimateStochastic(const WelfordAggregate &aggregate) {
  for (const auto &key : aggregate.GetAllQoIKeys()) {
    const auto &sVar = aggregate.qoI.at(key).GetSVar();
    sam[key] = sVar / double(aggregate.ctr.M);
  }
  if constexpr (AGGREGATE_FOR_SOLUTION) {
    for (const auto &key : aggregate.GetAllRdFKeys()) {
      const auto &bochner = aggregate.rdF.at(key).GetBochner();
      sam[key] = bochner / double(aggregate.ctr.M);
    }
  }
}

void ErrorData::iterateOverMaps(const WelfordAggregate &aggregate) {
  for (const auto &key : aggregate.GetErrors().GetErrorKeys()) {
    num[key] = disc.at(key) * disc.at(key);
  }
  for (const auto &key : aggregate.GetQoIKeys()) {
    const std::string delKey = std::format("\u2206{}", key);
    mse[delKey] = sam.at(delKey) + num.at(delKey);
    mse[key] = sam.at(key) + num.at(delKey);
    rmse[delKey] = sqrt(mse.at(delKey));
    rmse[key] = sqrt(mse.at(key));
  }
  for (const auto &innerKey : aggregate.GetAllRdFKeys()) {
    for (const auto &[key, err] : num) {
      if (key.find(std::format("({})", innerKey)) != std::string::npos) {
        mse[key] = sam.at(innerKey) + err;
        rmse[key] = sqrt(mse.at(key));
      }
    }
  }
}

void MultilevelErrors::iterateOverMaps(const EstimatorMap &mcMap) {
  for (const auto &key : mcMap.GetErrors().GetErrorKeys()) {
    num[key] = disc.at(key) * disc.at(key);
//  }
//  for (const auto &[key, err] : sam) {
    mse[key] = sam.at(key) + num.at(key);
    rmse[key] = sqrt(mse.at(key));
  }
//  for (const auto &innerKey : mcMap.GetAllRdFKeys()) {
//    for (const auto &[key, num] : num) {
//      if (key.find(std::format("({})", innerKey)) != std::string::npos) {
//        mse[key] = sam.at(std::format("L2({})", innerKey)) + num;
//        rmse[key] = sqrt(mse.at(key));
//      }
//    }
//  }
}

double ErrorData::GetRMSE(const std::string &key) const {
  if (rmse.find(key) != rmse.end()) return rmse.at(key);
  else Exit(std::format("Key {} not found", key))
}

double ErrorData::GetMSE(const std::string &key) const {
  if (mse.find(key) != mse.end()) return mse.at(key);
  else Exit(std::format("Key {} not found", key))
}

double ErrorData::GetSAM(const std::string &key) const {
  if (sam.find(key) != sam.end()) return sam.at(key);
  else Exit(std::format("Key {} not found", key))
}

double ErrorData::GetNUM(const std::string &key) const {
  if (num.find(key) != num.end()) return num.at(key);
  else Exit(std::format("Key {} not found", key))
}

double ErrorData::GetDISC(const std::string &key) const {
  if (disc.find(key) != disc.end()) return disc.at(key);
  else Exit(std::format("Key {} not found", key))
}

std::vector<std::string> ErrorData::GetErrorKeys() const {
  // take disc for keys as these are the first that are computed
  std::vector<std::string> keys;
  for (const auto &[key, value] : disc)
    keys.push_back(key);
  return keys;
}

void MultilevelErrors::estimateNumeric(const EstimatorMap &mcMap) {
  for (const auto &key : mcMap.GetDelQoIKeys()) {
    const auto &meansY = mcMap.GetMeanQoI(key);
    const auto &alphaY = mcMap.GetExponents().alpha.at(key);
    disc[key] = estimateNumeric(mcMap, meansY, alphaY);
  }
  if constexpr (AGGREGATE_FOR_SOLUTION) {
    for (const auto &key : mcMap.GetDelOutKeys()) {
      const auto &meansV = mcMap.GetMeanOuterNormDelRdF(key);
      const auto &alphaV = mcMap.GetExponents().alphaOuter.at(key);
      disc[key] = estimateNumeric(mcMap, meansV, alphaV);
    }
  }
}

double MultilevelErrors::estimateNumeric(const EstimatorMap &mcMap, const Vec &mean,
                                         double alpha) const {
  if (mean.empty()) return 0.0;
  double err = 0.0;
  int integer = (int)mcMap.size() - 2;
  for (int i = 1; i < mcMap.GetLevels().size(); i++) {
    err = std::max(std::abs(mean[i]) / (pow(2.0, alpha) - 1) / (pow(2.0, integer)), err);
    integer--;
  }
  return err;
}

void MultilevelErrors::estimateStochastic(const EstimatorMap &mcMap) {
  for (const auto &key : mcMap.GetDelQoIKeys()) {
    const auto &sVarsY = mcMap.GetSVarQoI(key);
    sam[key] = estimateStoch(mcMap, sVarsY);
  }
  if constexpr (AGGREGATE_FOR_SOLUTION) {
    for (const auto &key : mcMap.GetDelRdFKeys()) {
      const auto &bochner = mcMap.GetBochnerNormDelRdF(key);
      sam[std::format("L2({})", key)] = estimateStoch(mcMap, bochner);
    }
  }
}

double MultilevelErrors::estimateStoch(const EstimatorMap &mcMap,
                                       const std::vector<double> &boch) const {
  if (boch.empty()) return 0.0;
  auto ctr = mcMap.GetMVector();
  double err = 0.0;
  for (int i = 0; i < ctr.size(); i++) {
    err += boch[i] / double(ctr[i]);
  }
  return err;
}
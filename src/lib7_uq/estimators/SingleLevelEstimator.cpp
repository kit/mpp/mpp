#include "SingleLevelEstimator.hpp"
#include <format>
#include <utility>
#include "MemoryLogger.hpp"
#include "Plotting.hpp"

Estimator &SingleLevelEstimator::Method(const OptionalVector &input) {
  mout.StartBlock(std::format("{}Estimator{}", parallel ? "Parallel" : "Serial", initLevel));

  double start = MPI_Wtime();
  double memBeforeExecution = MemoryLogger::MemoryUsage();
  int maxInt = std::numeric_limits<int>::max();

  if (slEstmConf.epsilon != 0.0) { vout(1) << "epsilon=" << slEstmConf.epsilon << " "; }
  vout(1) << "dM=" << dM << " dMcomm=" << dMcomm << " commSplit=" << commSplit;
  if (maxSamplesByMem != maxInt) { vout(1) << " maxSamplesByMem=" << maxSamplesByMem; }
  vout(1) << " M=" << aggregate.ctr.M << endl;
  if (slEstmConf.logMemory) MemoryLogger::LogMemory();

  while (dM > 0) {
    while (dMcomm > 0) {
      const SampleID fineID(initLevel, Index(), false, commSplit);
      const SampleID coarseID(initLevel, Index(), true, commSplit);

      const auto fineData = mxFEM->RunProtocol(fineID, input);
      const auto coarseData = !slEstmConf.onlyFine ?
                                  OptionalSampleData(mxFEM->RunProtocol(coarseID, input)) :
                                  std::nullopt;

      // after solving the first PDE on the coarsest level,
      // the memory measurement is started to avoid
      // measuring the memory of initialization
      // Consequence: measured memory on level 0 might be zero
      // and should be excluded in estimating exponents
      if (slEstmConf.onlyFine && aggregate.ctr.M == 0) {
        MemoryLogger::Tare();
        memBeforeExecution = MemoryLogger::MemoryUsage();
      }

      updateOnComm(fineData, coarseData);
      dMcomm--;
    }

    if (slEstmConf.logMemory) { MemoryLogger::LogMemory(); }
    if (!slEstmConf.delayParallelUpdate) { UpdateParallel(); }
    if (!slEstmConf.delayTotalUpdate) {
      UpdateTotal();
      UpdateErrors();
    }
    updateSamplesIfNeeded();
    if (slEstmConf.logMemory) { MemoryLogger::LogMemory(); }
  }

  plotting = plotting < 0 ? 1 : plotting;
  UpdateCost(MPI_Wtime() - start, MemoryLogger::MemoryUsage() - memBeforeExecution);
  mout.EndBlock(verbose == 0);
  return *this;
}

const SampleData SingleLevelEstimator::WithdrawEstimate() {
  SampleData sampleData;

  if constexpr (AGGREGATE_FOR_SOLUTION) {
    sampleData.Insert("E(forward)", aggregate.rdF.at("forward").GetTotal().mean);
    sampleData.Insert("V(forward)", aggregate.rdF.at("forward").GetTotal().sVar);
    sampleData.Insert("E(backward)", aggregate.rdF.at("backward").GetTotal().mean);
    sampleData.Insert("V(backward)", aggregate.rdF.at("backward").GetTotal().sVar);
  }

  // Todo: insert option to only give back E(backward) with single or batch of samples

  for (auto &key : aggregate.GetQoIKeys())
    sampleData.Insert(std::format("E({})", key), aggregate.qoI.at(key).GetTotal().mean);

  sampleData.Insert("TotalMemoCostSum", aggregate.memoCost);
  sampleData.Insert("TotalTimeCostSum", aggregate.timeCost);
  sampleData.Insert("TotalCPUTimeCostSum", aggregate.timeCost * PPM->Size(0));

  ReInitAggregate();
  return sampleData;
}

void SingleLevelEstimator::updateSamplesIfNeeded() {
  if (slEstmConf.epsilon == 0.0) {
    dM = 0;
  } else {
    const std::string key = (slEstmConf.objective.find("\u2206") == std::string::npos) ? //
                                std::format("\u2206", slEstmConf.objective) :            //
                                slEstmConf.objective;                                    //
    if (aggregate.errors.GetDISC(key) < slEstmConf.epsilon) {
      if (aggregate.errors.GetRMSE(slEstmConf.objective) > slEstmConf.epsilon) {
        vout(1) << "updateOnComm sample amount" << endl;
        const auto &sVar = aggregate.qoI.at(slEstmConf.objective).GetSVar();
        int optimalM = (int)(std::ceil(sVar / (std::pow(slEstmConf.epsilon, 2))));
        UpdateSampleAmount(optimalM - aggregate.ctr.M);
      } else {
        dM = 0;
      }
    } else {
      Warning("Numerical error is too big to reach error bound")
    }
  }
}

void SingleLevelEstimator::UpdateSampleAmount(int requestedSamples) {
  UpdateSampleAmount(requestedSamples, slEstmConf.maxSamplesByMem);
}

void SingleLevelEstimator::UpdateSampleAmount(int requestedSamples, int _maxSamplesByMem) {
  if (requestedSamples <= 0) {
    this->commSplit = 0;
    this->dMcomm = 0;
    this->dM = 0;
    return;
  }

  if (!slEstmConf.parallel) {
    this->commSplit = 0;
    this->dMcomm = requestedSamples;
    this->dM = requestedSamples;
    return;
  }

  this->maxSamplesByMem = _maxSamplesByMem;
  const int min = std::min(std::min(PPM->Size(0), requestedSamples), maxSamplesByMem);
  if (min == PPM->Size(0) || requestedSamples) this->commSplit = (int)ceil(log2(min));
  else this->commSplit = (int)floor(log2(min));
  // ensure that commSplit only increases as simulation runs
  this->commSplit = std::max(this->commSplit, GetAggregate().overallMaxCommSplit);
  this->dM = requestedSamples;
  const int twoPowCommSplit = std::pow(2, this->commSplit);
  const int mod = requestedSamples % twoPowCommSplit;
  if (mod != 0) this->dM += (twoPowCommSplit - mod); // oversampling to equalize load distr.
  this->dMcomm = ceil((double)requestedSamples / twoPowCommSplit); // splitting up the work
}

int SingleLevelEstimator::Index() const {
  const auto &ctr = aggregate.ctr;
  return ctr.M + ctr.Mcomm + (dM / PPM->NumberOfCommunicators(commSplit)) * PPM->Color(commSplit);
}

void SingleLevelEstimator::updateOnComm(const SampleData &fData, const OptionalSampleData &cData) {
  const double start = MPI_Wtime();
  aggregate.UpdateOnComm(fData, cData);

  if (verbose > 4) {
    mout << endl << "Data after updateOnComm()" << endl;
    pout << aggregate << endl;
  }

  if constexpr (AGGREGATE_FOR_SOLUTION) {
    if (abs(plotting) > 2) {
      VtuPlot vtuPlot;
      for (const auto &key : fData.GetAllRdFKeys()) {
        vtuPlot.AddData(std::format("fine({})", key), fData.Get<Vector>(key));
      }
      for (const auto &key : aggregate.GetAllRdFKeys()) {
        vtuPlot.AddData(std::format("E({})", key), aggregate.rdF.at(key).GetComm().mean);
        vtuPlot.AddData(std::format("V({})", key), aggregate.rdF.at(key).GetComm().sVar);
      }
      vtuPlot.PlotFile(std::format("Comm.{}.{}", initLevel, Index()));
    }
  }
  vout(2) << std::format("Commitor update took {:.4f} seconds", MPI_Wtime() - start) << endl;
}

void SingleLevelEstimator::UpdateParallel() {
  const double start = MPI_Wtime();
  aggregate.UpdateParallel(commSplit);

  if (verbose > 3) {
    mout << endl << "Data after UpdateParallel()" << endl;
    pout << aggregate << endl;
  }

  if constexpr (AGGREGATE_FOR_SOLUTION) {
    if (abs(plotting) > 1) {
      VtuPlot vtuPlot;
      for (const auto &key : aggregate.GetAllRdFKeys()) {
        vtuPlot.AddData(std::format("E({})", key), aggregate.rdF.at(key).GetPara().mean);
        vtuPlot.AddData(std::format("V({})", key), aggregate.rdF.at(key).GetPara().sVar);
      }
      vtuPlot.PlotFile(std::format("Para.{}.{}", initLevel, Index()));
    }
  }
  vout(2) << std::format("Parallel update took {:.4f} seconds", MPI_Wtime() - start) << endl;
}

void SingleLevelEstimator::UpdateTotal() {
  const double start = MPI_Wtime();
  aggregate.UpdateTotal();

  if (verbose > 2) {
    mout << endl << "Data after UpdateTotal()" << endl;
    pout << aggregate << endl;
  }

  if constexpr (AGGREGATE_FOR_SOLUTION) {
    if (abs(plotting) > 0) {
      VtuPlot vtuPlot;
      for (const auto &key : aggregate.GetAllRdFKeys()) {
        vtuPlot.AddData(std::format("E({})", key), aggregate.rdF.at(key).GetTotal().mean);
        vtuPlot.AddData(std::format("V({})", key), aggregate.rdF.at(key).GetTotal().sVar);
      }
      vtuPlot.PlotFile(std::format("Total.{}.{}", initLevel, Index()));
    }
  }
  vout(2) << std::format("TotalAcc update took {:.4f} seconds", MPI_Wtime() - start) << endl;
}

void SingleLevelEstimator::UpdateErrors() {
  if (slEstmConf.delayTotalUpdate) return;
  aggregate.UpdateErrors();
  if (verbose > 2) pout << aggregate.errors << endl;
}

void SingleLevelEstimator::UpdateCost(double newTimeCost, double newMemoryCost) {
  aggregate.UpdateTimeCost(newTimeCost);
  aggregate.UpdateMemoryCost(newMemoryCost);
  if (verbose > 2) {
    pout << DOUT(aggregate.costPerSample) << " " << DOUT(aggregate.cost.at("C").GetMean()) << endl;
    pout << DOUT(aggregate.timeCost) << " ";
    pout << DOUT(aggregate.cost.at("C").GetMean() * aggregate.ctr.M) << endl;
    mout << DOUT(newMemoryCost) << " " << DOUT(aggregate.memoCost) << endl;
  }
}

size_t SingleLevelEstimator::CellsOn(const MeshIndex &meshIndex) const {
  if (mxFEM->GetMeshes().Contains(meshIndex))
    return mxFEM->GetMeshes()[meshIndex].CellCountGeometry();
  return 0;
}

void SingleLevelEstimator::ResetExpectedMemoryCost() { aggregate.expectedMemoryCost = 0.0; }

void SingleLevelEstimator::PrintInfo() const {
  std::vector<PrintInfoEntry<double>> e;
  const auto &errors = aggregate.errors;
  for (const auto &key : aggregate.GetDelQoIKeys()) {
    e.emplace_back(InfoEntry(std::format("NUM({})", key), errors.num.at(key)));
  }
  for (const auto &key : aggregate.GetAllQoIKeys()) {
    e.emplace_back(InfoEntry(std::format("RMSE({})", key), errors.rmse.at(key)));
    e.emplace_back(InfoEntry(std::format("MSE({})", key), errors.mse.at(key)));
    e.emplace_back(InfoEntry(std::format("SAM({})", key), errors.sam.at(key)));
  }
  mout.PrintInfo("Final Errors", 1, e);
  e.clear();

  // --------------------------

  for (const auto &key : aggregate.GetAllQoIKeys()) {
    e.emplace_back(InfoEntry(std::format("E({})", key), aggregate.qoI.at(key).GetMean()));
    e.emplace_back(InfoEntry(std::format("V({})", key), aggregate.qoI.at(key).GetSVar()));
    e.emplace_back(InfoEntry(std::format("BL2({})", key), aggregate.qoI.at(key).GetBochner(), 2));
    e.emplace_back(InfoEntry(std::format("S({})", key), aggregate.qoI.at(key).GetSkew(), 2));
    e.emplace_back(InfoEntry(std::format("K({})", key), aggregate.qoI.at(key).GetKurt(), 2));
  }

  if constexpr (AGGREGATE_FOR_SOLUTION) {
    for (const auto &key : aggregate.GetAllRdFKeys()) {
      e.emplace_back(InfoEntry(std::format("BL2({})", key), aggregate.rdF.at(key).GetBochner()));
    }

    for (auto key : aggregate.GetAllOutKeys()) {
      std::string tmp = key;
      tmp.insert(key.find("(") + 1, "E(");
      tmp += ")";
      e.emplace_back(InfoEntry(tmp, aggregate.outerNorms.at(key).mean));
      tmp = tmp.substr(0, tmp.find("E(")) + "V(" + tmp.substr(tmp.find("E(") + 2);
      e.emplace_back(InfoEntry(tmp, aggregate.outerNorms.at(key).sVar));
      tmp = tmp.substr(0, tmp.find("V(")) + "S(" + tmp.substr(tmp.find("V(") + 2);
      e.emplace_back(InfoEntry(tmp, aggregate.outerNorms.at(key).skew, 2));
      tmp = tmp.substr(0, tmp.find("S(")) + "K(" + tmp.substr(tmp.find("S(") + 2);
      e.emplace_back(InfoEntry(tmp, aggregate.outerNorms.at(key).kurt, 2));
    }
  }
  mout.PrintInfo("Estimates", 1, e);
}
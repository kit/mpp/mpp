#ifndef MPP_CONFIGSTRUCTSUQ_H
#define MPP_CONFIGSTRUCTSUQ_H

#include "PDESolver.hpp"

struct MultiXFEMConfig {
  int fLevel = 0; // Todo: remove this, information is in SampleID

  int cLevel = 0; // Todo: remove this, information is in SampleID

  int commSplit = 0; // Todo: remove this, information is in SampleID

  std::string name{};

  std::vector<PDESolverConfig> pdeSolverConfigs;

  MultiXFEMConfig(std::string problemName) : name(std::move(problemName)) {}

  MultiXFEMConfig() : pdeSolverConfigs({PDESolverConfig()}) {
    if (!Config::IsInitialized()) return;

    Config::Get("Problem", name);
    Config::Get("level", fLevel);
    Config::Get("plevel", cLevel);
  }

  MultiXFEMConfig WithFineLevel(int level);

  MultiXFEMConfig WithProblem(std::string problem);

  MultiXFEMConfig WithCoarseLevel(int level);

  MultiXFEMConfig WithCommSplit(int communicationSplit);

  MultiXFEMConfig WithPDESolverConfigs(const std::vector<PDESolverConfig> &conf);
};

std::ostream &operator<<(std::ostream &s, const MultiXFEMConfig &conf);

struct SLEstimatorConfig {
  int initLevel = 0;

  int coarseLevel = 0;

  int initSamples = 0;

  double epsilon = 0.0;

  bool parallel = true;

  bool onlyFine = false;

  bool logMemory = true;

  bool controlMemory = false;

  // Needed also for tests
  bool delayTotalUpdate = false;
  bool delayParallelUpdate = false;

  std::string objective = {};

  std::string quantity = "L2";

  MultiXFEMConfig mxFEMConfig;

  int maxSamplesByMem = std::numeric_limits<int>::max();

  SLEstimatorConfig(int level, int samples, double epsilon, bool onlyFine) :
      initLevel(level), initSamples(samples), epsilon(epsilon), onlyFine(onlyFine) {}

  explicit SLEstimatorConfig() {
    if (!Config::IsInitialized()) return;

    Config::Get("Level", initLevel);
    Config::Get("epsilon", epsilon);
    Config::Get("OnlyFine", onlyFine);
    Config::Get("Quantity", quantity);
    Config::Get("Samples", initSamples);
    Config::Get("LogMemory", logMemory);
    Config::Get("Objective", objective);
    Config::Get("CoarseLevel", coarseLevel);
    Config::Get("ParallelEstimator", parallel);
    Config::Get("ControlMemory", controlMemory);
    Config::Get("MaxSamplesByMem", maxSamplesByMem);
    Config::Get("DelayTotalUpdate", delayTotalUpdate);
    Config::Get("DelayParallelUpdate", delayParallelUpdate);
  }

  SLEstimatorConfig &WithInitLevel(int level);

  SLEstimatorConfig &WithInitSamples(int samples);

  SLEstimatorConfig &WithEpsilon(double targetEpsilon);

  SLEstimatorConfig &WithMaxSamplesByMem(int maxSamples);

  SLEstimatorConfig &WithParallel(bool parallelEstimator);

  SLEstimatorConfig &WithOnlyFine(bool onlyComputeOnFine);

  SLEstimatorConfig &WithObjective(const std::string &obj);

  SLEstimatorConfig &WithDelayTotalUpdate(bool delayTotUpdate);

  SLEstimatorConfig &WithDelayParallelUpdate(bool delayParaUpdate);

  SLEstimatorConfig &WithMultiSampleFEMConfig(const MultiXFEMConfig &conf);
};

std::ostream &operator<<(std::ostream &s, const SLEstimatorConfig &conf);

struct MLEstimatorConfig {
  double eta = 0.9;

  double theta = 0.5;

  double epsilon = 0.0;

  double timeBudget = 0.0;

  std::vector<int> initLevels{};

  std::vector<int> initSamples{};

  SLEstimatorConfig slEstmConf;

  std::string quantity = "L2";

  std::string objective;

  std::string dataKey;

  double ratioMemoBudgetReserved = 0.7;

  double ratioTimeBudgetReserved = 0.5;

  double memoBudget = std::numeric_limits<double>::max();

  explicit MLEstimatorConfig() {
    if (!Config::IsInitialized()) return;

    std::string wTimeStr = "00:00:00";
    Config::Get("initLevels", initLevels);
    Config::Get("initSamples", initSamples);
    Config::Get("Objective", objective);
    Config::Get("epsilon", epsilon);
    Config::Get("WTime", wTimeStr);
    Config::Get("theta", theta);
    Config::Get("eta", eta);
    Config::Get("ratioMemoBudgetReserved", ratioMemoBudgetReserved);
    Config::Get("ratioTimeBudgetReserved", ratioTimeBudgetReserved);

    dataKey = objectiveStr2DataStr();

    std::string memoryBudgetString = "inf";
    Config::Get("MemoryBudget", memoryBudgetString);
    if (memoryBudgetString != "inf") {
      TRY {
        memoBudget = ratioMemoBudgetReserved * convertBudgetStringToDouble(memoryBudgetString);
      }
      CATCH("Wrong memory budget format: " + memoryBudgetString)
    }

    TRY {
      timeBudget = ratioTimeBudgetReserved
                   * (stoi(wTimeStr.substr(0, 2)) * 3600 + stoi(wTimeStr.substr(3, 2)) * 60
                      + stoi(wTimeStr.substr(6, 2)));
    }
    CATCH("Wrong time format")
  }

  MLEstimatorConfig &WithSLEstimatorConfig(const SLEstimatorConfig &conf);

  MLEstimatorConfig &WithInitSamples(const std::vector<int> &samples);

  MLEstimatorConfig &WithInitLevel(const std::vector<int> &levels);

  MLEstimatorConfig &WithTimeBudget(double timeBudgetAsDouble);

  MLEstimatorConfig &WithObjective(const std::string &obj);

  MLEstimatorConfig &WithMemoryBudget(double memoryBudgetAsDouble);

  MLEstimatorConfig &WithTheta(double biasVarianceTradeOff);

  MLEstimatorConfig &WithEpsilon(double targetEpsilon);

  MLEstimatorConfig &WithEta(double errorReduction);

  void PrintInfo() const;

private:
  std::string objectiveStr2DataStr();

  double convertBudgetStringToDouble(const std::string &str);
};

std::ostream &operator<<(std::ostream &s, const MLEstimatorConfig &conf);

enum class EstimatorType { BATCHED, MULTILEVEL };

enum class OptimizerType { CONSTANT, DECREASING, ADAM };

enum class BatchSizeControl { FIX, EPSILON, BUDGET };

struct SGDConfig {
  double targetCost = 0.0;

  double betaAdam = 0.9;

  double betaAdam2 = 0.99;

  double defaultStepSize;

  double epsilonAdam = 1e-8; // used to ensure that we don't divide by zero

  double epsilonTarget = 1e-3;

  double polyakAveragingFraction = 0.0;

  MLEstimatorConfig mlEstmConf;

  int maxGradientSteps = std::numeric_limits<int>::max();

  BatchSizeControl batchSizeCtl = BatchSizeControl::FIX;

  EstimatorType estimatorType = EstimatorType::BATCHED;

  OptimizerType optimizerType = OptimizerType::CONSTANT;

  double admissibleLowerBound = -std::numeric_limits<double>::max();

  double admissibleUpperBound = std::numeric_limits<double>::max();

  explicit SGDConfig() {
    if (!Config::IsInitialized()) return;

    std::string optimizerString, estimatorString, batchSizeCtlStr;

    Config::Get("targetCost", targetCost);
    Config::Get("betaAdam", betaAdam);
    Config::Get("betaAdam2", betaAdam2);
    Config::Get("gammaADAM", defaultStepSize);
    Config::Get("epsilonAdam", epsilonAdam);
    Config::Get("breakpointEpsilon", epsilonTarget);
    Config::Get("maxGradientSteps", maxGradientSteps);
    Config::Get("OptimizerType", optimizerString);
    Config::Get("EstimatorType", estimatorString);
    Config::Get("BatchSizeControl", batchSizeCtlStr);
    Config::Get("admissibleLowerBound", admissibleLowerBound); // not found in config
    Config::Get("admissibleUpperBound", admissibleUpperBound); // not found in config
    Config::Get("polyakAveragingFraction", polyakAveragingFraction);
    Config::Get("defaultStepSize", defaultStepSize);

    if (optimizerString == "adam") optimizerType = OptimizerType::ADAM;
    else if (optimizerString == "constant") optimizerType = OptimizerType::CONSTANT;
    else if (optimizerString == "decreasing") optimizerType = OptimizerType::DECREASING;

    if (estimatorString == "batched") estimatorType = EstimatorType::BATCHED;
    else if (estimatorString == "multilevel") estimatorType = EstimatorType::MULTILEVEL;

    if (batchSizeCtlStr == "fix") batchSizeCtl = BatchSizeControl::FIX;
    else if (batchSizeCtlStr == "epsilon") batchSizeCtl = BatchSizeControl::EPSILON;
    else if (batchSizeCtlStr == "budget") batchSizeCtl = BatchSizeControl::BUDGET;

  }

  SGDConfig WithMLEstimatorConfig(const MLEstimatorConfig &conf);

  SGDConfig WithBetaAdam(double beta);

  SGDConfig WithEstimatorType(EstimatorType estimatorType);

  SGDConfig WithBetaAdam2(double beta);

  SGDConfig WithDefaultStepsize(double defaultValue);

  SGDConfig WithEpsilonAdam(double epsilon);

  SGDConfig WithGradientEstimatorType(EstimatorType estimatorType);

  SGDConfig WithMaxGradientSteps(int gradientSteps);

  SGDConfig WithOptimizerType(OptimizerType optType);

  SGDConfig WithAveragingFraction(double fraction);

  SGDConfig WithAdmissibleBounds(double lowerBound, double upperBound);

  SGDConfig WithBatchSize(std::map<int, int> batchSize);

  SGDConfig WithBatchSize(int batchSize);

  SGDConfig WithAdmissibleLowerBound(double lowerBound);

  SGDConfig WithAdmissibleUpperBound(double upperBound);

  SGDConfig WithEpsilonTarget(double epsilon);

  SGDConfig WithTargetCost(double cost);
};


#endif // MPP_CONFIGSTRUCTSUQ_H

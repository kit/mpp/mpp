#include "WelfordAggregate.hpp"
#include <format>
#include "Plotting.hpp"
#include "Transfers.hpp"

template<typename T>
WelfordDelta<T>::WelfordDelta(const T &newValue) {
  one = newValue;
  ComputeDeltas();
}

template<typename T>
WelfordDelta<T>::WelfordDelta(const T &newValue, const T &oldMean) {
  one = newValue;
  one -= oldMean;
  ComputeDeltas();
}

template<>
WelfordDelta<Vector>::WelfordDelta(const Vector &newValue, const Vector &oldMean) :
    one(newValue), two(0.0, newValue), three(0.0, newValue), four(0.0, newValue) {
  one -= oldMean;
  ComputeDeltas();
}

template<>
WelfordDelta<Vector>::WelfordDelta(const Vector &newValue) :
    one(newValue), two(0.0, newValue), three(0.0, newValue), four(0.0, newValue) {
  ComputeDeltas();
}

template<typename T>
void WelfordDelta<T>::ComputeDeltas() {
  // sOMe FuNny MatHs
  two = one;
  two *= one;
  three = one;
  three *= two;
  four = two;
  four *= two;

  if constexpr (std::is_same<T, Vector>::value) {
    // Todo Bochner norm has to be implemented other than L2
    bochnerDelta = norms::Quantity(two, "L2");
  } else {
    bochnerDelta = two;
  }
}

// Todo there ware too many constructors, its annoying

template<>
WelfordData<Vector>::WelfordData(Vector mean, Vector sVar, double bochnerNorm, Vector skew,
                                 Vector kurt, Vector powSum2, double bochnerPowSum2, Vector powSum3,
                                 Vector powSum4, double weight, double weight2) :
    mean(mean), sVar(sVar), boch(bochnerNorm), skew(skew), kurt(kurt), powSum2(powSum2),
    powSumBoch(bochnerPowSum2), powSum3(powSum3), powSum4(powSum4), weight(weight),
    weight2(weight2) {}

template<>
WelfordData<double>::WelfordData(double mean, double sVar, double bochnerNorm, double skew,
                                 double kurt, double powSum2, double bochnerPowSum2, double powSum3,
                                 double powSum4, double weight, double weight2) :
    mean(mean), sVar(sVar), boch(bochnerNorm), kurt(kurt), skew(skew), powSum2(powSum2),
    powSumBoch(bochnerPowSum2), powSum3(powSum3), powSum4(powSum4), weight(weight),
    weight2(weight2) {}

template<>
WelfordData<double>::WelfordData(const double &newValue) :
    mean(newValue), sVar(newValue), boch(0.0), skew(newValue), kurt(newValue), powSum2(newValue),
    powSumBoch(0.0), powSum3(newValue), powSum4(newValue), weight(0.0), weight2(0.0) {
  Reset();
}

template<>
WelfordData<Vector>::WelfordData(const Vector &newValue) :
    mean(newValue), sVar(newValue), boch(0.0), skew(newValue), kurt(newValue), powSum2(newValue),
    powSumBoch(0.0), powSum3(newValue), powSum4(newValue), weight(0.0), weight2(0.0) {
  Reset();
}

template<>
WelfordData<double>::WelfordData(const WelfordData<double> &welfordData) :
    mean(welfordData.mean), sVar(welfordData.sVar), boch(welfordData.boch), skew(welfordData.skew),
    kurt(welfordData.kurt), powSum2(welfordData.powSum2), powSumBoch(welfordData.powSumBoch),
    powSum3(welfordData.powSum3), powSum4(welfordData.powSum4), weight(welfordData.weight),
    weight2(welfordData.weight2) {}

template<>
WelfordData<Vector>::WelfordData(const WelfordData<Vector> &welfordData) :
    mean(welfordData.mean), sVar(welfordData.sVar), boch(welfordData.boch), skew(welfordData.skew),
    kurt(welfordData.kurt), powSum2(welfordData.powSum2), powSumBoch(welfordData.powSumBoch),
    powSum3(welfordData.powSum3), powSum4(welfordData.powSum4), weight(welfordData.weight),
    weight2(welfordData.weight2) {}

template<>
WelfordData<double>::WelfordData(double value, const WelfordData<double> &welfordData) :
    mean(value), sVar(value), boch(value), skew(value), kurt(value), powSum2(value),
    powSumBoch(value), powSum3(value), powSum4(value), weight(0.0), weight2(0.0) {}

template<>
WelfordData<Vector>::WelfordData(double value, const WelfordData<Vector> &welfordData) :
    mean(value, welfordData.mean), sVar(value, welfordData.sVar), boch(value),
    skew(value, welfordData.skew), kurt(value, welfordData.kurt),
    powSum2(value, welfordData.powSum2), powSumBoch(value), powSum3(value, welfordData.powSum3),
    powSum4(value, welfordData.powSum4), weight(0.0), weight2(0.0) {}

template<>
WelfordData<double>::WelfordData(double value, const WelfordData<double> &welfordData,
                                 int commSplit) :
    mean(value), sVar(value), boch(value), skew(), kurt(value), powSum2(value), powSumBoch(value),
    powSum3(value), powSum4(value), weight(welfordData.weight), weight2(welfordData.weight2) {}

template<>
WelfordData<Vector>::WelfordData(double value, const WelfordData<Vector> &welfordData,
                                 int commSplit) :
    mean(Vector(value, welfordData.mean.GetSharedDisc(),
                welfordData.mean.Level().WithCommSplit(commSplit))),
    sVar(Vector(value, welfordData.sVar.GetSharedDisc(),
                welfordData.sVar.Level().WithCommSplit(commSplit))),
    boch(value), skew(Vector(value, welfordData.skew.GetSharedDisc(),
                             welfordData.skew.Level().WithCommSplit(commSplit))),
    kurt(Vector(value, welfordData.kurt.GetSharedDisc(),
                welfordData.kurt.Level().WithCommSplit(commSplit))),
    powSum2(Vector(value, welfordData.powSum2.GetSharedDisc(),
                   welfordData.powSum2.Level().WithCommSplit(commSplit))),
    powSumBoch(value), powSum3(Vector(value, welfordData.powSum3.GetSharedDisc(),
                                      welfordData.powSum3.Level().WithCommSplit(commSplit))),
    powSum4(Vector(value, welfordData.powSum4.GetSharedDisc(),
                   welfordData.powSum4.Level().WithCommSplit(commSplit))),
    weight(welfordData.weight), weight2(welfordData.weight2) {}

template<typename T>
void Aggregate<T>::UpdateTotal() {
  if (total == nullptr) { total = std::make_shared<WelfordData<T>>(0.0, *para); }

  total->weight += para->weight;
  total->weight2 += para->weight2;

  WelfordDelta<T> delta(para->mean, total->mean);

  total->ComputeMean(total->weight - para->weight, para->weight, delta);
  total->ComputePowSum2(total->weight - para->weight, para->weight, *para, delta);
  total->ComputePowSum3(total->weight - para->weight, para->weight, *para, delta);
  total->ComputePowSum4(total->weight - para->weight, para->weight, *para, delta);

  total->ComputeSVar();
  total->ComputeSkew();
  total->ComputeKurt();

  para.reset();
}

template<typename T>
auto sumOnDistLevel(const T &value, int distLevel) {
  if constexpr (std::is_same<T, double>::value) {
    return PPM->SumOn(value, 0, distLevel);
  } else {
    Vector sumOnCommSplit(0.0, value.GetSharedDisc(),
                          value.Level().WithCommSplit(value.CommSplit()));
    for (row r = sumOnCommSplit.rows(); r != sumOnCommSplit.rows_end(); r++) {
      for (int indexOfDof = 0; indexOfDof < r.NumberOfDofs(); indexOfDof++) {
        double valueForSum = value(r(), indexOfDof);
        sumOnCommSplit(r(), indexOfDof) = PPM->SumOn(valueForSum, 0, distLevel);
      }
    }
    return sumOnCommSplit;
  }
}

template<typename T>
T setSolution(const T &value, int commSplitOfReturn) {
  if constexpr (std::is_same<T, double>::value) {
    return value;
  } else {
    Vector returnVector(0.0, value.GetSharedDisc(), value.Level().WithCommSplit(commSplitOfReturn));
    for (row r = returnVector.rows(); r != returnVector.rows_end(); r++) {
      for (int indexOfDof = 0; indexOfDof < r.NumberOfDofs(); indexOfDof++) {
        if (value.find_row(r()) != value.rows_end()) {
          returnVector(r(), indexOfDof) = value(r(), indexOfDof);
        } else {
          Exit("Distributions of different commSplits are not consistent")
        }
      }
    }
    return returnVector;
  }
}

template<typename T>
void Aggregate<T>::UpdateParallel(int commSplit) {
  para = std::make_shared<WelfordData<T>>(0.0, *comm);
  para->weight = PPM->SumAcrossComm(comm->weight, commSplit);
  para->weight2 = PPM->SumAcrossComm(comm->weight2, commSplit);
  para->mean = comm->mean;
  para->powSum2 = comm->powSum2;
  para->powSum3 = comm->powSum3;
  para->powSum4 = comm->powSum4;
  para->powSumBoch = comm->powSumBoch;

  if constexpr (DebugLevel > 2 && AGGREGATE_FOR_SOLUTION) {
    if constexpr (std::is_same<T, Vector>::value) {
      if (typeid(*this) == typeid(AggregateSolution)) {
        std::string nameSubStr = std::to_string(para->mean.SpaceLevel()) + ".InitPara.";
        nameSubStr += std::to_string(PPM->Proc(0));
        mpp::PlotData("U", "U." + nameSubStr, para->mean);
      }
    }
  }

  for (int i = PPM->MaxCommSplit() - 1; i >= PPM->MaxCommSplit() - commSplit; i--) {
    double W_lr = PPM->SumOnCommSplit(comm->weight, i);
    T copy = para->mean;
    if (PPM->Color(0, i) == PPM->Proc(0)) copy *= -1.0;

    if constexpr (DebugLevel > 2 && AGGREGATE_FOR_SOLUTION) {
      if constexpr (std::is_same<T, Vector>::value) {
        if (typeid(*this) == typeid(AggregateSolution)) {
          std::string nameSubStr = std::to_string(para->mean.SpaceLevel()) + ".in.";
          nameSubStr += std::to_string(i) + "." + std::to_string(PPM->Proc(0));
          mpp::PlotData("U", "in." + nameSubStr, copy);
        }
      }
    }

    T out = sumOnDistLevel(copy, i);

    if constexpr (DebugLevel > 2 && AGGREGATE_FOR_SOLUTION) {
      if constexpr (std::is_same<T, Vector>::value) {
        if (typeid(*this) == typeid(AggregateSolution)) {
          std::string nameSubStr = std::to_string(para->mean.SpaceLevel()) + ".out.";
          nameSubStr += std::to_string(i) + "." + std::to_string(PPM->Proc(0));
          mpp::PlotData("U", "out." + nameSubStr, out);
        }
      }
    }

    if (PPM->Color(0, i) != PPM->Proc(0)) out *= -1.0;
    WelfordDelta<T> delta(out);
    double W = PPM->SumOnCommSplit(comm->weight, i + 1);
    para->powSum2 = sumOnDistLevel(para->powSum2, i);
    para->powSum3 = sumOnDistLevel(para->powSum3, i);
    para->powSum4 = sumOnDistLevel(para->powSum4, i);
    para->powSumBoch = sumOnDistLevel(para->powSumBoch, i);

    para->ComputeMean((W_lr - W), W, delta);
    para->ComputePowSum2(W_lr - W, W, delta);
    para->ComputePowSum3(W_lr - W, W, delta);
    para->ComputePowSum4(W_lr - W, W, delta);

    if constexpr (DebugLevel > 2 && AGGREGATE_FOR_SOLUTION) {
      if constexpr (std::is_same<T, Vector>::value) {
        if (typeid(*this) == typeid(AggregateSolution)) {
          std::string nameSubStr = std::to_string(para->mean.SpaceLevel()) + ".CommUpdate.";
          nameSubStr += std::to_string(i) + "." + std::to_string(PPM->Proc(0));
          mpp::PlotData("U", "U." + nameSubStr, para->mean);
        }
      }
    }
  }

  auto tempPara = std::make_shared<WelfordData<T>>(0.0, *para, 0);
  tempPara->mean = setSolution(para->mean, 0);
  tempPara->powSum2 = setSolution(para->powSum2, 0);
  tempPara->powSum3 = setSolution(para->powSum3, 0);
  tempPara->powSum4 = setSolution(para->powSum4, 0);
  tempPara->powSumBoch = setSolution(para->powSumBoch, 0);
  para.reset(); // tempPara is needed since vectors are on different commSplits
  para = std::make_shared<WelfordData<T>>(*tempPara);

  para->ComputeSVar();
  para->ComputeSkew();
  para->ComputeKurt();

  comm.reset();
}

template<typename T>
void Aggregate<T>::UpdateTemplateOnComm(const T &newValue, double newWeight) {
  bool computeMoments = false;
  if (comm == nullptr) {
    comm = std::make_shared<WelfordData<T>>(newValue);
  } else {
    computeMoments = true;
  }

  comm->weight += newWeight;
  comm->weight2 += newWeight * newWeight;

  WelfordDelta<T> delta(newValue, comm->mean);
  comm->ComputeMean(comm->weight - newWeight, newWeight, delta);
  comm->ComputePowSum2(comm->weight - newWeight, newWeight, delta);
  comm->ComputePowSum3(comm->weight - newWeight, newWeight, delta);
  comm->ComputePowSum4(comm->weight - newWeight, newWeight, delta);

  if (computeMoments) {
    comm->ComputeSVar();
    comm->ComputeSkew();
    comm->ComputeKurt();
  }
}

void WelfordAggregate::UpdateTimeCost(double newTimeCost) {
  timeCost += PPM->Max(newTimeCost, 0);
  costPerSample = timeCost / ctr.M;
}

void WelfordAggregate::UpdateMemoryCost(double newMemoryCost) { memoCost += newMemoryCost; }

void WelfordAggregate::UpdateOnComm(const SampleData &fData, const OptionalSampleData &cData) {
  ctr.Mcomm++;

  cost["C"].UpdateOnComm("", fData, cData);

  for (const auto &[key, component] : fData.Map<double>()) {
    qoI[std::format("\u2206{}", key)].UpdateOnComm(key, fData, cData);
    qoI[key].UpdateOnComm(key, fData);
  }
  if constexpr (AGGREGATE_FOR_SOLUTION) {
    for (const auto &[key, component] : fData.Map<Vector>()) {
      rdF[key].UpdateOnComm(key, fData);
      const OptionalVector &cOpt = cData ? OptionalVector(cData->Get<Vector>(key)) : std::nullopt;
      Vector delRdF = rdF[std::format("\u2206{}", key)].UpdateOnComm(key, fData, cOpt);
      for (const auto norm : quantities) {
        innerNorms[std::format("{}({})", norm, key)].UpdateOnComm(fData, key, norm);
        innerNorms[std::format("{}(\u2206{})", norm, key)].UpdateOnComm(fData.weight, delRdF, norm);
      }
    }
  }
}

void WelfordAggregate::UpdateParallel(int commSplit) {
  ctr.Mpara = PPM->SumAcrossComm(ctr.Mcomm, commSplit);
  overallMaxCommSplit = std::max(overallMaxCommSplit, commSplit);

  for (const auto &[key, component] : cost) { cost[key].UpdateParallel(commSplit); }
  for (const auto &key : GetAllQoIKeys()) { qoI[key].UpdateParallel(commSplit); }

  if constexpr (AGGREGATE_FOR_SOLUTION) {
    for (const auto &key : GetAllRdFKeys()) {
      rdF[key].UpdateParallel(commSplit);
      for (const auto norm : quantities) {
        innerNorms[std::format("{}({})", norm, key)].UpdateParallel(commSplit);
      }
    }
  }
  ctr.Mcomm = 0;
}

void WelfordAggregate::UpdateTotal() {
  ctr.M += ctr.Mpara;

  for (const auto &[key, component] : cost) { cost[key].UpdateTotal(); }
  for (const auto &key : GetAllQoIKeys()) { qoI[key].UpdateTotal(); }

  if constexpr (AGGREGATE_FOR_SOLUTION) {
    for (const auto &key : GetAllRdFKeys()) {
      rdF[key].UpdateTotal();

      for (const auto norm : quantities) {
        innerNorms[std::format("{}({})", norm, key)].UpdateTotal();
        outerNorms[std::format("{}({})", norm, key)].UpdateOuterNorms(rdF.at(key), norm);
      }
    }
  }
  ctr.Mpara = 0;
}

SampleData WelfordAggregate::ToSampleData() const { return SampleData(); }

double WelfordAggregate::GetBochner(const std::string &key) const {
  if constexpr (AGGREGATE_FOR_SOLUTION) {
    std::string localKey = key;
    if (auto pos = localKey.find("("); pos != std::string::npos && key.find("\u2206") != 0) {
      localKey = localKey.erase(0, pos + 1);
      localKey.pop_back();
    }
    if (rdF.find(localKey) != rdF.end()) return rdF.at(localKey).GetBochner();
  }
  if (qoI.find(key) != qoI.end()) return qoI.at(key).GetBochner();
  Exit(std::format("Key {} not found", key))
}

std::ostream &operator<<(std::ostream &s, const WelfordAggregate &aggregate) {
  const auto &a = aggregate;
  s << aggregate.ctr << endl;

  for (const auto &[key, entry] : a.qoI) {
    if (entry.TotalInitialized()) s << std::format("{} total: ", key) << entry.GetTotal();
    if (entry.ParaInitialized()) s << std::format("{}  para: ", key) << entry.GetPara();
    if (entry.CommInitialized()) s << std::format("{}  comm: ", key) << entry.GetComm();
  }

  for (const auto &[key, entry] : a.cost) {
    if (entry.TotalInitialized()) s << std::format("Cost total {}: ", key) << entry.GetTotal();
    if (entry.ParaInitialized()) s << std::format("Cost para {}: ", key) << entry.GetPara();
    if (entry.CommInitialized()) s << std::format("Cost comm {}: ", key) << entry.GetComm();
  }

  return s;
}

std::vector<std::string> WelfordAggregate::GetAllRdFKeys() const {
  std::vector<std::string> keys;
  for (const auto &[key, value] : rdF) {
    keys.emplace_back(key);
  }
  return keys;
}

std::vector<std::string> WelfordAggregate::GetAllQoIKeys() const {
  std::vector<std::string> keys;
  for (const auto &[key, value] : qoI) {
    keys.emplace_back(key);
  }
  return keys;
}

std::vector<std::string> WelfordAggregate::GetDelQoIKeys() const {
  std::vector<std::string> keys;
  for (const auto &[key, value] : qoI) {
    if (key.find("\u2206") != std::string::npos) keys.emplace_back(key);
  }
  return keys;
}

std::vector<std::string> WelfordAggregate::GetDelRdFKeys() const {
  std::vector<std::string> keys;
  for (const auto &[key, value] : rdF) {
    if (key.find("\u2206") != std::string::npos) keys.emplace_back(key);
  }
  return keys;
}

std::vector<std::string> WelfordAggregate::GetRdFKeys() const {
  std::vector<std::string> keys;
  for (const auto &[key, value] : rdF) {
    if (key.find("\u2206") == std::string::npos) keys.emplace_back(key);
  }
  return keys;
}

std::vector<std::string> WelfordAggregate::GetQoIKeys() const {
  std::vector<std::string> keys;
  for (const auto &[key, value] : qoI) {
    if (key.find("\u2206") == std::string::npos) keys.emplace_back(key);
  }
  return keys;
}

std::vector<std::string> WelfordAggregate::GetOutKeys() const {
  std::vector<std::string> keys;
  for (const auto &[key, value] : outerNorms) {
    if (key.find("\u2206") == std::string::npos) keys.emplace_back(key);
  }
  return keys;
}

std::vector<std::string> WelfordAggregate::GetAllOutKeys() const {
  std::vector<std::string> keys;
  for (const auto &[key, value] : outerNorms) {
    keys.emplace_back(key);
  }
  return keys;
}

std::vector<std::string> WelfordAggregate::GetDelOutKeys() const {
  std::vector<std::string> keys;
  for (const auto &[key, value] : outerNorms) {
    if (key.find("\u2206") != std::string::npos) keys.emplace_back(key);
  }
  return keys;
}

std::vector<std::string> WelfordAggregate::GetInKeys() const {
  std::vector<std::string> keys;
  for (const auto &[key, value] : innerNorms) {
    if (key.find("\u2206") == std::string::npos) keys.emplace_back(key);
  }
  return keys;
}

std::vector<std::string> WelfordAggregate::GetAllInKeys() const {
  std::vector<std::string> keys;
  for (const auto &[key, value] : innerNorms) {
    keys.emplace_back(key);
  }
  return keys;
}

std::vector<std::string> WelfordAggregate::GetDelInKeys() const {
  std::vector<std::string> keys;
  for (const auto &[key, value] : innerNorms) {
    if (key.find("\u2206") != std::string::npos) keys.emplace_back(key);
  }
  return keys;
}
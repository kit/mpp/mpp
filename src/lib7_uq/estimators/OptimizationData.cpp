#include "OptimizationData.hpp"

void OptimizationData::PrintInfo() const {
  mout.PrintInfo("Optimization", 1, PrintInfoEntry("L2(z_k)", controlL2Norms),
                 PrintInfoEntry("E(L2(gradient))", gradientL2Norms),
                 PrintInfoEntry("E(j)", targetFunctionalL2Norms),
                 PrintInfoEntry("timePerStep", timeCosts), PrintInfoEntry("memory", memoryCost),
                 PrintInfoEntry("rmse", rmse), PrintInfoEntry("mse", mse),
                 PrintInfoEntry("sam", sam), PrintInfoEntry("num", num),
                 PrintInfoEntry("alpha", alpha), PrintInfoEntry("beta", beta),
                 PrintInfoEntry("gammaCT", gammaCT), PrintInfoEntry("gammaMem", gammaMem));
  // Todo: Maybe define output parameters by some dateVerbose or SGD verbose
}

#ifndef WELFORDAGGREGATE_HPP
#define WELFORDAGGREGATE_HPP

#include <utility>

#include "Config.hpp"
#include "Parallel.hpp"
#include "Sample.hpp"
#include "ScalarElement.hpp"
#include "Transfers.hpp"
#include "ErrorData.hpp"

struct SampleCounter {

  int M = 0;

  int Mpara = 0;

  int Mcomm = 0;

  friend std::ostream &operator<<(std::ostream &s, const SampleCounter &ctr) {
    return s << "M=" << ctr.M << " Mpara=" << ctr.Mpara << " Mcomm=" << ctr.Mcomm;
  }
};

template<typename T>
struct WelfordDelta {
  T one;
  T two;
  T three;
  T four;
  double bochnerDelta;

  WelfordDelta(const T &newValue);

  WelfordDelta(const T &newValue, const T &oldMean);

  void ComputeDeltas();

  friend std::ostream &operator<<(std::ostream &s, const WelfordDelta &welfordDelta) {
    return s << "delta1=" << welfordDelta.one << " delta2=" << welfordDelta.two
             << " delta3=" << welfordDelta.three << " delta4=" << welfordDelta.four << endl;
  }
};

template<typename T>
struct WelfordData {
  T mean;
  T sVar;
  T skew;
  T kurt;
  double boch;

  T powSum2;
  T powSum3;
  T powSum4;
  double weight;
  double weight2;
  double powSumBoch;

  WelfordData(const double &newValue);

  WelfordData(const Vector &newValue);

  WelfordData(const WelfordData<double> &welfordData);

  WelfordData(const WelfordData<Vector> &welfordData);

  WelfordData(double value, const WelfordData<double> &welfordData);

  WelfordData(double value, const WelfordData<Vector> &welfordData);

  WelfordData(double value, const WelfordData<double> &welfordData, int commSplit);

  WelfordData(double value, const WelfordData<Vector> &welfordData, int commSplit);

  WelfordData(double mean, double sVar, double bochnerNorm, double skew, double kurt,
              double powSum2, double bochnerPowSum2, double powSum3, double powSum4, double weight,
              double weight2);

  WelfordData(Vector mean, Vector sVar, double bochnerNorm, Vector skew, Vector kurt,
              Vector powSum2, double bochnerPowSum2, Vector powSum3, Vector powSum4, double weight,
              double weight2);

  T ComputeSqrt(const T &value) const {
    if constexpr (std::is_same<T, double>::value) return std::sqrt(value);
    if constexpr (std::is_same<T, Vector>::value) return ComponentSqrt(value);
    Exit("Type not supported")
  }

  inline void Reset() {
    mean = 0.0;
    sVar = 0.0;
    boch = 0.0;
    skew = 0.0;
    kurt = 0.0;
    powSum2 = 0.0;
    powSum3 = 0.0;
    powSum4 = 0.0;
    powSumBoch = 0.0;
  }

  inline void ComputeMean(double lW, double rW, const WelfordDelta<T> &delta) {
    double Wlr = (lW + rW);
    T deltaTmp = delta.one;
    deltaTmp *= (rW / Wlr);
    mean += deltaTmp;
  }

  inline void ComputeSVar() {
    sVar = powSum2;
    sVar *= 1.0 / (weight - weight / weight2);
    boch = powSumBoch;
    boch *= 1.0 / (weight - weight / weight2);
  }

  inline void ComputeSkew() {
    skew = powSum3;
    skew *= sqrt(weight);
    T denominator = ComputeSqrt(powSum2);
    denominator *= powSum2;
    skew /= denominator;
  }

  inline void ComputeKurt() {
    kurt = powSum4;
    kurt *= weight;
    kurt /= powSum2;
    kurt /= powSum2;
  }

  inline void ComputePowSum2(double lW, double rW, const WelfordDelta<T> &delta) {
    const double Wlr = (lW + rW);

    T delta2Tmp = delta.two;
    delta2Tmp *= (lW * rW) / Wlr;
    powSum2 += delta2Tmp;

    double bochnerPowSum2Tmp = delta.bochnerDelta;
    bochnerPowSum2Tmp *= (lW * rW) / Wlr;
    powSumBoch += bochnerPowSum2Tmp;
  }

  inline void ComputePowSum2(double lW, double rW, const WelfordData<T> &otherData,
                             const WelfordDelta<T> &delta) {
    ComputePowSum2(lW, rW, delta);
    powSum2 += otherData.powSum2;
    powSumBoch += otherData.powSumBoch;
  }

  inline void ComputePowSum3(double lW, double rW, const WelfordDelta<T> &delta) {
    double Wlr = (rW + lW);
    T ldelta3Tmp = delta.three;
    T rdelta3Tmp = delta.three;
    ldelta3Tmp *= lW * rW * rW * (-rW) / (Wlr * Wlr * Wlr);
    rdelta3Tmp *= rW * lW * lW * lW / (Wlr * Wlr * Wlr);

    T ldeltaTmp = delta.one;
    ldeltaTmp *= 3 * (-rW) / Wlr;
    ldeltaTmp *= powSum2;

    powSum3 += ldelta3Tmp;
    powSum3 += rdelta3Tmp;
    powSum3 += ldeltaTmp;
  }

  inline void ComputePowSum3(double lW, double rW, const WelfordData<T> &otherData,
                             const WelfordDelta<T> &delta) {
    ComputePowSum2(lW, rW, delta);
    double Wlr = (rW + lW);
    T rdeltaTmp = delta.one;
    rdeltaTmp *= 3 * lW / Wlr;
    rdeltaTmp *= otherData.powSum2;

    powSum3 += otherData.powSum3;
    powSum3 += rdeltaTmp;
  }

  inline void ComputePowSum4(double lW, double rW, const WelfordDelta<T> &delta) {
    double Wlr = (rW + lW);
    T ldelta4Tmp = delta.four;
    T rdelta4Tmp = delta.four;
    ldelta4Tmp *= lW * pow(rW / Wlr, 4);
    rdelta4Tmp *= rW * pow(lW / Wlr, 4);

    T ldeltaTmp = delta.one;
    ldeltaTmp *= 4 * (-rW) / Wlr;
    ldeltaTmp *= powSum3;

    T ldelta2Tmp = delta.two;
    ldelta2Tmp *= 6 * pow(rW / Wlr, 2);
    ldelta2Tmp *= powSum2;

    powSum4 += ldelta4Tmp;
    powSum4 += rdelta4Tmp;
    powSum4 += ldelta2Tmp;
    powSum4 += ldeltaTmp;
  }

  inline void ComputePowSum4(double lW, double rW, const WelfordData<T> &otherData,
                             const WelfordDelta<T> &delta) {
    ComputePowSum4(lW, rW, delta);
    double Wlr = (rW + lW);
    T rdeltaTmp = delta.one;
    rdeltaTmp *= 4 * lW / Wlr;
    rdeltaTmp *= otherData.powSum3;

    T rdelta2Tmp = delta.two;
    rdelta2Tmp *= 6 * pow(rW / Wlr, 2);
    rdelta2Tmp *= otherData.powSum2;

    powSum4 += otherData.powSum4;
    powSum4 += rdeltaTmp;
    powSum4 += rdelta2Tmp;
  }

  friend std::ostream &operator<<(std::ostream &s, const WelfordData &welfordData) {
    return s << "mean=" << welfordData.mean << " sVar=" << welfordData.sVar
             << " boch=" << welfordData.boch << " skew=" << welfordData.skew
             << " kurt=" << welfordData.kurt << endl;
  }
};

template<typename T>
class Aggregate {
  friend class QuantityOfInterest;

  std::shared_ptr<WelfordData<T>> comm;

  std::shared_ptr<WelfordData<T>> para;

  std::shared_ptr<WelfordData<T>> total;
public:
  Aggregate() = default;

  Aggregate(const WelfordData<double> &total) :
      total(std::make_shared<WelfordData<double>>(total)) {}

  Aggregate(const WelfordData<Vector> &total) :
      total(std::make_shared<WelfordData<Vector>>(total)) {}

  void UpdateTemplateOnComm(const T &newValue, double newWeight);

  void UpdateParallel(int commSplit);

  void UpdateTotal();

  virtual void Dummy() {};

  const WelfordData<T> &GetTotal() const {
    if (total) return *total;
    Exit("Total WelfordData not initialized.")
  }

  const WelfordData<T> &GetPara() const {
    if (para) return *para;
    Exit("Para WelfordData not initialized.")
  }

  const WelfordData<T> &GetComm() const {
    if (comm) return *comm;
    Exit("Comm WelfordData not initialized.")
  }

  T GetMean() const {
    if (total) return (total->mean);
    if constexpr (std::is_same<T, double>::value) return 0.0;
    else Exit("Total not initialized")
  }

  T GetSVar() const {
    if (total) return (total->sVar);
    if constexpr (std::is_same<T, double>::value) return 0.0;
    else Exit("Total not initialized")
  }

  T GetSkew() const {
    if (total) return (total->skew);
    if constexpr (std::is_same<T, double>::value) return 0.0;
    else Exit("Total not initialized")
  }

  T GetKurt() const {
    if (total) return (total->kurt);
    if constexpr (std::is_same<T, double>::value) return 0.0;
    else Exit("Total not initialized")
  }

  double GetBochner() const {
    if (total) return (total->boch);
    if constexpr (std::is_same<T, double>::value) return 0.0;
    else Exit("Total not initialized")
  }

  T GetPowSum2() const {
    if (total) return (total->powSum2);
    if constexpr (std::is_same<T, double>::value) return 0.0;
    else Exit("Total not initialized")
  }

  T GetPowSum3() const {
    if (total) return (total->powSum3);
    if constexpr (std::is_same<T, double>::value) return 0.0;
    else Exit("Total not initialized")
  }

  T GetPowSum4() const {
    if (total) return (total->powSum4);
    if constexpr (std::is_same<T, double>::value) return 0.0;
    else Exit("Total not initialized")
  }

  bool TotalInitialized() const {
    if (total) return true;
    return false;
  }

  bool ParaInitialized() const {
    if (para) return true;
    return false;
  }

  bool CommInitialized() const {
    if (comm) return true;
    return false;
  }

  double GetWeight() const { return total->weight; }

  double GetWeight2() const { return total->weight2; }

  friend class WelfordAggregate;
};

struct QuantityOfInterest : public Aggregate<double> {

  QuantityOfInterest() = default;

  QuantityOfInterest(const WelfordData<double> &total) : Aggregate(total) {}

  void UpdateOnComm(const std::string &key, const SampleData &fineSample) {
    UpdateTemplateOnComm(fineSample.Get<double>(key), fineSample.weight);
  }

  void UpdateOnComm(const std::string &key, const SampleData &fineSample,
                    const OptionalSampleData &coarseSample) {
    const auto &fineQoI = fineSample.Get<double>(key);
    if (coarseSample == std::nullopt) {
      UpdateTemplateOnComm(fineQoI, fineSample.weight);
      return;
    }
    UpdateTemplateOnComm((fineQoI - coarseSample.value().Get<double>(key)), fineSample.weight);
  }
};

struct CostInSeconds : public Aggregate<double> {

  CostInSeconds() = default;

  CostInSeconds(const WelfordData<double> &total) : Aggregate(total) {}

  void UpdateOnComm(const std::string &key, const SampleData &fineSample,
                    const OptionalSampleData &coarseSample) {
    if (coarseSample == std::nullopt) {
      UpdateTemplateOnComm(fineSample.C, 1.0);
      return;
    }
    UpdateTemplateOnComm((fineSample.C + coarseSample.value().C), 1.0);
  }
};

#ifdef AGGREGATE_FOR_SOLUTION

struct OuterNorms {
  double mean = 0.0;
  double sVar = 0.0;
  double skew = 0.0;
  double kurt = 0.0;

  OuterNorms() {}

  void UpdateOuterNorms(Aggregate<Vector> vectorAggregate, std::string norm) {
    mean = norms::Quantity(vectorAggregate.GetMean(), norm);
    sVar = norms::Quantity(vectorAggregate.GetSVar(), norm);
    skew = norms::Quantity(vectorAggregate.GetSkew(), norm);
    kurt = norms::Quantity(vectorAggregate.GetKurt(), norm);
  }
};

struct AggregateSolution : public Aggregate<Vector> {
  AggregateSolution() {}

  void UpdateOnComm(const std::string &key, const SampleData &fData) {
    UpdateTemplateOnComm(fData.Get<Vector>(key), fData.weight);
  }

  Vector UpdateOnComm(const std::string &key, const SampleData &fData,
                      const OptionalVector &cData) {
    // Edge case for the lowest level
    if (cData == std::nullopt) {
      UpdateTemplateOnComm(fData.Get<Vector>(key), fData.weight);
      return fData.Get<Vector>(key);
    }

    Vector prolongatedSample(fData.Get<Vector>(key));
    Vector fineSampleWithoutBC(fData.Get<Vector>(key));
    prolongatedSample.ClearDirichletFlags(); // To remove boundary conditions
    fineSampleWithoutBC.ClearDirichletFlags();
    auto transfer = GetTransfer(cData.value(), prolongatedSample, "Matrix");
    transfer->Prolongate(cData.value(), prolongatedSample);
    prolongatedSample *= -1;
    prolongatedSample += fineSampleWithoutBC;
    UpdateTemplateOnComm(prolongatedSample, fData.weight);
    return prolongatedSample;
  }
};

struct InnerNorms : Aggregate<double> {
  InnerNorms() = default;

  void UpdateOnComm(const SampleData &fData, const std::string &key, std::string norm) {
    UpdateTemplateOnComm(norms::Quantity(fData.Get<Vector>(key), norm), fData.weight);
  }

  void UpdateOnComm(double weight, const Vector &delRdF, std::string norm) {
    UpdateTemplateOnComm(norms::Quantity(delRdF, norm), weight);
  }
};

#endif

class WelfordAggregate {
public:
  ErrorData errors;

  SampleCounter ctr;

  double timeCost = 0.0;

  double memoCost = 0.0;

  double costPerSample = 0.0;

  int overallMaxCommSplit = 0;

  // This one should always be zero;
  // only if a new level is appended this is used
  double expectedMemoryCost = 0.0;

  std::vector<std::string> quantities = {"L2"};

  std::map<std::string, CostInSeconds> cost;

#ifdef AGGREGATE_FOR_SOLUTION
  std::map<std::string, AggregateSolution> rdF;

  std::map<std::string, OuterNorms> outerNorms;

  std::map<std::string, InnerNorms> innerNorms;
#endif

  std::map<std::string, QuantityOfInterest> qoI;

  WelfordAggregate(SampleCounter ctr, CostInSeconds C, QuantityOfInterest qoI, double cost,
                   double costPerSample, double memoryCost, std::string key) :
      ctr(ctr), cost({{key, std::move(C)}}), qoI({{key, std::move(qoI)}}), timeCost(cost),
      costPerSample(costPerSample), memoCost(memoryCost) {}

  WelfordAggregate() {}

  void UpdateTotal();

  void UpdateErrors();

  void UpdateParallel(int commSplit);

  void UpdateTimeCost(double newTimeCost);

  void UpdateMemoryCost(double newMemoryCost);

  void UpdateOnComm(const SampleData &fData, const OptionalSampleData &cData);

  double GetBochner(const std::string &key) const;

  const ErrorData &GetErrors() const { return errors; };

  std::vector<std::string> GetRdFKeys() const;

  std::vector<std::string> GetAllRdFKeys() const;

  std::vector<std::string> GetDelRdFKeys() const;

  std::vector<std::string> GetQoIKeys() const;

  std::vector<std::string> GetAllQoIKeys() const;

  std::vector<std::string> GetDelQoIKeys() const;

  std::vector<std::string> GetOutKeys() const;

  std::vector<std::string> GetAllOutKeys() const;

  std::vector<std::string> GetDelOutKeys() const;

  std::vector<std::string> GetInKeys() const;

  std::vector<std::string> GetAllInKeys() const;

  std::vector<std::string> GetDelInKeys() const;

  SampleData ToSampleData() const;

  friend std::ostream &operator<<(std::ostream &s, const WelfordAggregate &aggregate);
};

#endif // WELFORDAGGREGATE_HPP

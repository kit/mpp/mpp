#include "StochasticGradientDescent.hpp"
#include "Plotting.hpp"

void StochasticGradientDescent::Method() {
  mout.StartBlock("StochasticGradientDescent");

  // clang-format off
  if (sgdConf.epsilonTarget == 0.0) { vout(1) << "Kmax=" << sgdConf.maxGradientSteps; }
  else { vout(1) << "epsilon=" << sgdConf.epsilonTarget; }
  vout(1) << " initial gradient estimate k=0" << endl;
  // clang-format on

  SampleData sampleData = stateAdjointEstimator->Method(std::nullopt).WithdrawEstimate();
  std::unique_ptr<Vector> control = std::make_unique<Vector>(optimizer->InitStep(sampleData));
  updateSampleAmount();

  vout(2) << std::format("target={:.5f} ", optData.targetFunctionalL2Norms.back())
          << std::format("gradientNorm={:.5f}", optData.gradientL2Norms.back()) << std::endl;

  while (!stopCriteria()) {
    vout(1) << "Optimization step k=" << optData.Size() << endl;

    sampleData = stateAdjointEstimator->Method(*control).WithdrawEstimate();
    control = std::make_unique<Vector>(optimizer->Step(*control, sampleData));
    updateSampleAmount();

    vout(2) << std::format("target={:.5f} ", optData.targetFunctionalL2Norms.back())
            << std::format("gradientNorm={:.5f} ", optData.gradientL2Norms.back())
            << std::format("L2Error target={:.5f}", sampleData.qoIs.at("E(L2Error(forward))"))
            << std::endl;

    PPM->Barrier(0);

    if (plotting > 0) {
      // Todo: Need to differ between Lagrange and DG at some point.
      //  Or: can we find a way to plot DGVectors appropriately without assemble
      VtuPlot vtuPlot;
      vtuPlot.AddData(std::format("control.{}", optData.Size()), *control);
      //      vtuPlot.AddData(std::format("descent.{}", optData.Size()), descent);
      if (plotting > 1) {
        const Vector &fwdEstm = sampleData.Get<Vector>("E(forward)");
        const Vector &backEstm = sampleData.Get<Vector>("E(backward)");
        vtuPlot.AddData(std::format("State.{}", optData.Size()), fwdEstm);
        vtuPlot.AddData(std::format("Adjoint.{}", optData.Size()), backEstm);
      }
      // Todo: Check for a way to save all in one
      // File with steps as "time" indicator
      vtuPlot.PlotFile(std::format("Optimizer.{}", optData.Size()));
    }
  }

  mout.EndBlock(verbose == 0);
}

bool StochasticGradientDescent::stopCriteria() const {
  if (sgdConf.maxGradientSteps != std::numeric_limits<int>::max()) {
    if (optData.Size() >= sgdConf.maxGradientSteps) return true;
    else return false;
  }

  if (sgdConf.epsilonTarget == 0.0) {
    if (optData.gradientL2Norms.back() > sgdConf.epsilonTarget) return false;
    else return true;
  }

  // Todo include computing time as stopping criterion
  Exit("Unknown stopping criterion")
}

void StochasticGradientDescent::updateSampleAmount() {
  // Todo return boolean if sample amount is feasible in compute budget
  if (sgdConf.estimatorType == EstimatorType::BATCHED) {
    if (sgdConf.batchSizeCtl == BatchSizeControl::FIX) {
      stateAdjointEstimator->UpdateSampleAmount(sgdConf.mlEstmConf.slEstmConf.initSamples);
    }
  } else if (sgdConf.estimatorType == EstimatorType::MULTILEVEL) {
    if (sgdConf.batchSizeCtl == BatchSizeControl::FIX) {
      const std::vector<int> &samples = sgdConf.mlEstmConf.initSamples;
      const std::vector<int> &levels = sgdConf.mlEstmConf.initLevels;
      stateAdjointEstimator->UpdateSampleAmount(levels, samples);
    } else if (sgdConf.batchSizeCtl == BatchSizeControl::EPSILON) {
      const double factor = 1.0;
      stateAdjointEstimator->UpdateEpsilon(factor * optData.gradientL2Norms.back());
    }
  }
}

void StochasticGradientDescent::averagingSolution(const Vector &u) const {
  //  if (sgdConf.polyakAveragingFraction < 1.0) { sum_of_stepsizes = std::make_unique<double>(0.0);
  //  } // used to be in method std::unique_ptr<double> sum_of_stepsizes; // used to be a class
  //  member if (alpha < 1.0 && Iteration() >= maxGradientSteps * alpha) {
  //    double sz = StepSize();
  //    u_T.solution.vector += sz * u_k.solution.vector;
  //    (*sum_of_stepsizes) += sz;
  //  }
}

void StochasticGradientDescent::PrintResults() const {
  if (optData.IsEmpty()) return;
  optData.PrintInfo();
}

std::unique_ptr<Estimator> StochasticGradientDescent::createEstimator() {
  if (sgdConf.estimatorType == EstimatorType::BATCHED)
    return std::make_unique<SingleLevelEstimator>(sgdConf.mlEstmConf.slEstmConf);
  if (sgdConf.estimatorType == EstimatorType::MULTILEVEL)
    return std::make_unique<MultiLevelEstimator>(sgdConf.mlEstmConf);
  Exit("Unknown estimator type")
}

std::unique_ptr<Optimizer> StochasticGradientDescent::createOptimizer() {
  if (sgdConf.optimizerType == OptimizerType::CONSTANT) {
    return std::make_unique<ConstantStepSizeOptimizer>(sgdConf);
  } else if (sgdConf.optimizerType == OptimizerType::DECREASING) {
    return std::make_unique<DecreasingStepSizeOptimizer>(sgdConf);
  } else if (sgdConf.optimizerType == OptimizerType::ADAM) {
    return std::make_unique<AdamOptimizer>(sgdConf);
  } else {
    Exit("Unknown Optimizer")
  }
}

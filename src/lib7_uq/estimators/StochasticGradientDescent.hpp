#ifndef MLUQ_MAINSGD_HPP
#define MLUQ_MAINSGD_HPP

#include "ConfigStructsUQ.hpp"
#include "MultiLevelEstimator.hpp"
#include "OptimizationData.hpp"
#include "Optimizer.hpp"
#include "SingleLevelEstimator.hpp"

/*
 * The StochasticGradientDescent class can represent
 *    a standard SGD, a batched-SGD, or a multi-level SGD
 *    depending on the estimator used for the gradient
 */

class StochasticGradientDescent {
private:
  int plotting = 0;

  int verbose = 1;

  SGDConfig sgdConf;

  std::unique_ptr<Optimizer> optimizer;

  std::unique_ptr<Estimator> stateAdjointEstimator;

  OptimizationData &optData;
public:
  StochasticGradientDescent() : StochasticGradientDescent(SGDConfig()) {}

  StochasticGradientDescent(SGDConfig sgdConfig) :
      sgdConf(std::move(sgdConfig)), stateAdjointEstimator(createEstimator()),
      optimizer(createOptimizer()), optData(optimizer->optData) {
    Config::Get("SGDVerbose", verbose);
    Config::Get("SGDPlotting", plotting);
  }

  const OptimizationData &GetOptData() const { return optData; };

  void PrintResults() const;

  void Method();
private:
  std::unique_ptr<Estimator> createEstimator();

  std::unique_ptr<Optimizer> createOptimizer();

  void updateSampleAmount();

  void averagingSolution(const Vector &u) const;

  bool stopCriteria() const;
};

class BatchedSGD : public StochasticGradientDescent {
public:
  BatchedSGD(SGDConfig sgdConfig) :
      StochasticGradientDescent(std::move(sgdConfig.WithEstimatorType(EstimatorType::BATCHED))) {}
};

class MultilevelSGD : public StochasticGradientDescent {
public:
  MultilevelSGD(SGDConfig sgdConfig) :
      StochasticGradientDescent(std::move(sgdConfig.WithEstimatorType(EstimatorType::MULTILEVEL))) {
  }
};


#endif // MLUQ_MAINSGD_HPP

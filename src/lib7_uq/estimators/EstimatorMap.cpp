#include "EstimatorMap.hpp"

double Exponents::regression(const std::vector<int> &levels, const std::vector<double> &data,
                             double factor) {
  if (data.empty()) return 0.0; // to avoid crash in TestEstimatorMap
  std::vector<double> transformed;
  // skip the first level, since delta is not computed there
  std::vector<double> levelsAsDouble(levels.begin() + 1, levels.end());
  for (int i = 1; i < levels.size(); i++) {
    transformed.push_back(factor * std::log2(std::abs(data[i])));
  }
  return ft::linearFit(levelsAsDouble, transformed).first;
}

void Exponents::ComputeAlpha(const EstimatorMap &mcMap) {
  const auto &_levels = mcMap.GetLevels();
  for (const auto &key : mcMap.GetDelQoIKeys()) {
    alpha[key] = regression(_levels, mcMap.GetMeanQoI(key), -1.0);
  }
  if constexpr (AGGREGATE_FOR_SOLUTION) {
    for (const auto &key : mcMap.GetDelOutKeys()) {
      alphaOuter[key] = regression(_levels, mcMap.GetMeanOuterNormDelRdF(key), -1.0);
    }
    for (const auto &key : mcMap.GetDelInKeys()) {
      alphaInner[key] = regression(_levels, mcMap.GetMeanInnerNormDelRdF(key), -1.0);
    }
  }
}

void Exponents::ComputeBeta(const EstimatorMap &mcMap) {
  const auto &_levels = mcMap.GetLevels();
  for (const auto &key : mcMap.GetDelQoIKeys()) {
    beta[key] = regression(mcMap.GetLevels(), mcMap.GetSVarQoI(key), -1.0);
  }
  if constexpr (AGGREGATE_FOR_SOLUTION) {
    for (const auto &key : mcMap.GetDelOutKeys()) {
      betaOuter[key] = regression(_levels, mcMap.GetSVarOuterNormDelRdF(key), -1.0);
    }
    for (const auto &key : mcMap.GetDelInKeys()) {
      betaInner[key] = regression(_levels, mcMap.GetSVarInnerNormDelRdF(key), -1.0);
    }
    // Todo, Bochner has to be adapted to other norms
    for (const auto &key : mcMap.GetDelRdFKeys()) {
      std::string localKey = std::format("L2({})", key);
      betaBochner[localKey] = regression(_levels, mcMap.GetBochnerNormDelRdF(key), -1.0);
    }
    for (const auto &key : mcMap.GetDelQoIKeys()) {
      betaBochner[key] = regression(_levels, mcMap.GetBochnerNormDelQoI(key), -1.0);
    }
  }
}

void Exponents::ComputeGamma(const EstimatorMap &mcMap) {
  const auto &_levels = mcMap.GetLevels();
  gammaCT = regression(_levels, mcMap.GetTimeCostPerSample("C"), 1.0);
  gammaMem = regression(_levels, mcMap.GetMemoCostPerSample("C"), 1.0);
}

void WelfordAggregate::UpdateErrors() { errors.EstimateErrors(*this); }

double Exponents::GetObjAlpha(const std::string &key) const {
  if (alpha.find(key) != alpha.end()) return alpha.at(key);
  else if (alphaOuter.find(key) != alphaOuter.end()) return alphaOuter.at(key);
  else Exit(std::format("Key {} not found", key))
}

double Exponents::GetObjBeta(const std::string &key) const {
  if (beta.find(key) != beta.end()) return beta.at(key);
  else if (betaBochner.find(key) != betaBochner.end()) return betaBochner.at(key);
  else Exit(std::format("Key {} not found", key))
}

double Exponents::GetMemoGamma(const std::string &key) const {
  return gammaMem;
  Exit(std::format("Key {} not found", key))
}

double Exponents::GetTimeGamma(const std::string &key) const {
  return gammaCT;
  Exit(std::format("Key {} not found", key))
}

std::ostream &operator<<(std::ostream &s, const Exponents &exponents) {
  for (const auto &[key, value] : exponents.alpha) {
    s << std::format("alpha({})", key) << "=" << std::setprecision(3) << value << " ";
  }
  s << std::endl;
  for (const auto &[key, value] : exponents.alphaOuter) {
    s << std::format("alphaOuter({})", key) << "=" << std::setprecision(3) << value << " ";
  }
  s << std::endl;
  for (const auto &[key, value] : exponents.beta) {
    s << std::format("beta({})", key) << "=" << std::setprecision(3) << value << " ";
  }
  s << std::endl;
  for (const auto &[key, value] : exponents.betaBochner) {
    s << std::format("betaBochner({})", key) << "=" << std::setprecision(3) << value << " ";
  }
  s << std::endl;
  s << " gammaCT=" << std::setprecision(3) << exponents.gammaCT;
  s << " gammaMem=" << std::setprecision(3) << exponents.gammaMem;
  return s;
}

void EstimatorMap::UpdateExponentsAndErrors() {
  exponents.ComputeAlpha(*this);
  exponents.ComputeBeta(*this);
  exponents.ComputeGamma(*this);
  errors.EstimateErrors(*this);
}

void EstimatorMap::ResetSampleAmount() {
  for (auto &[level, estimator] : estimators)
    estimator.UpdateSampleAmount(0);
}

double EstimatorMap::SampleCounterFactor(double epsilon, double theta) {
  double factor = 0.0;
  for (auto &[level, estimator] : estimators) {
    const auto &agg = estimator.GetAggregate();
    if constexpr (AGGREGATE_FOR_SOLUTION) {
      factor += sqrt(agg.GetBochner(mlConf.dataKey) * agg.costPerSample);
    } else {
      factor += sqrt(agg.qoI.at(mlConf.dataKey).GetSVar() * agg.costPerSample);
    }
  }
  factor *= pow(sqrt(theta) * epsilon, -2);
  return factor;
}

int EstimatorMap::OptimalMOnLevel(int level, double epsilon, double theta) {
  const double meanC = estimators[level].GetAggregate().costPerSample;
  const auto &agg = estimators[level].GetAggregate();
  if constexpr (AGGREGATE_FOR_SOLUTION) {
    const double &boch = agg.GetBochner(mlConf.dataKey);
    return OptimalMOnLevel(boch, meanC, epsilon, theta);
  } else {
    const double &sVarY = agg.qoI.at(mlConf.dataKey).GetSVar();
    return OptimalMOnLevel(sVarY, meanC, epsilon, theta);
  }
}

int EstimatorMap::OptimalMOnLevel(double samErr, double meanC, double epsilon, double theta) {
  return (int)(ceil(SampleCounterFactor(epsilon, theta) * sqrt(samErr / meanC)));
}

int EstimatorMap::MaxSamplesByMem(const SingleLevelEstimator &estimator) const {
  const double &memOnLevel = estimator.GetAggregate().memoCost;
  int maxSamplesByMem = std::numeric_limits<int>::max();
  if (mlConf.memoBudget != std::numeric_limits<double>::max()) {
    maxSamplesByMem = MaxSamplesByMem(memOnLevel);
  }
  return std::max(maxSamplesByMem, 1);
}

void EstimatorMap::UpdateSampleCounterOnMap(double epsilon, double theta) {
  for (auto &[level, estimator] : estimators) {
    int dM = OptimalMOnLevel(level, epsilon, theta) - estimator.GetAggregate().ctr.M;
    if (dM == 1) dM++; // Variance is otherwise not computable
    estimator.UpdateSampleAmount(dM, MaxSamplesByMem(estimator));
  }
  PostSmoothing();
}

void EstimatorMap::UpdateSampleCounterOnMap(std::vector<int> samples) {
  int index = 0;
  for (auto &[level, estimator] : estimators) {
    estimator.UpdateSampleAmount(samples[index], MaxSamplesByMem(estimator));
    index++;
  }
  PostSmoothing();
}

void EstimatorMap::PostSmoothing() {
  for (auto &[level, estimator] : estimators) {
    if (level == estimators.begin()->first) continue;
    if (level == estimators.rbegin()->first) break;
    int expectedMonLevel = estimator.GetAggregate().ctr.M + estimator.SamplesToCompute();
    int expectedMafterLevel = estimators[level + 1].GetAggregate().ctr.M;
    expectedMafterLevel += estimators[level + 1].SamplesToCompute();
    int expectedMbeforeLevel = estimators[level - 1].GetAggregate().ctr.M;
    expectedMbeforeLevel += estimators[level - 1].SamplesToCompute();

    if (expectedMonLevel <= expectedMafterLevel) {
      int avg = ((int)pow(2, ((log2(expectedMafterLevel) + log2(expectedMbeforeLevel)) / 2.0)));
      estimator.UpdateSampleAmount(avg - estimator.GetAggregate().ctr.M);
    }
  }
}

int EstimatorMap::MaxSamplesByMem(double memOnLevel) const {
  return (int)std::floor((mlConf.memoBudget - MemoCostPrediction()) / memOnLevel);
}

bool EstimatorMap::UpdateNewLevel(double epsilon, double theta, int newLevel) {
  const auto &aggregateBelow = estimators[newLevel - 1].GetAggregate();
  const auto costGrowth = pow(2.0, exponents.gammaCT);
  const auto sErrReduction = pow(2.0, -exponents.GetObjBeta(mlConf.dataKey));
  const auto sErr = sErrReduction * aggregateBelow.GetBochner(mlConf.dataKey);
  const auto costPerSample = costGrowth * aggregateBelow.costPerSample;
  const auto optimalMOnLevel = OptimalMOnLevel(sErr, costPerSample, epsilon, theta);
  const auto costPrediction = costPerSample * optimalMOnLevel;
  const auto leftOverBudget = mlConf.timeBudget - TimeCost();

  if (mlConf.timeBudget != 0.0 && costPrediction >= leftOverBudget) { return false; }

  const auto doubleLimit = std::numeric_limits<double>::max();
  const double memCostGrowth = pow(2.0, exponents.gammaMem);
  const double mem4NewLevel = estimators[newLevel - 1].GetAggregate().memoCost * memCostGrowth;

  int maxSamplesByMem = std::numeric_limits<int>::max();
  if (mlConf.memoBudget != doubleLimit) { maxSamplesByMem = MaxSamplesByMem(mem4NewLevel); }

  if (mlConf.memoBudget != doubleLimit && maxSamplesByMem < 1) { return false; }

  estimators.emplace(std::piecewise_construct, std::forward_as_tuple(newLevel),
                     std::forward_as_tuple(mlConf.slEstmConf.WithInitLevel(newLevel)
                                               .WithMaxSamplesByMem(maxSamplesByMem)
                                               .WithInitSamples(std::max(2, optimalMOnLevel))
                                               .WithOnlyFine(false),
                                           mxFEM));

  return true;
}

std::map<int, SingleLevelEstimator> EstimatorMap::fill() {
  std::map<int, SingleLevelEstimator> _estimators{};
  for (auto level_index = 0; level_index < mlConf.initLevels.size(); level_index++) {
    _estimators.emplace(std::piecewise_construct,
                        std::forward_as_tuple(mlConf.initLevels[level_index]),
                        std::forward_as_tuple(mlConf.slEstmConf
                                                  .WithInitSamples(mlConf.initSamples[level_index])
                                                  .WithInitLevel(mlConf.initLevels[level_index])
                                                  .WithEpsilon(0.0)
                                                  .WithOnlyFine((level_index == 0)),
                                              mxFEM));
  }
  return _estimators;
}

std::map<int, SingleLevelEstimator>
EstimatorMap::fill(const std::map<int, WelfordAggregate> &aggregateMap) {
  std::map<int, SingleLevelEstimator> _estimators{};
  for (auto const &[level, aggregate] : aggregateMap) {
    _estimators.emplace(std::piecewise_construct, std::forward_as_tuple(level),
                        std::forward_as_tuple(SLEstimatorConfig().WithInitLevel(level), aggregate));
  }
  return _estimators;
}

double EstimatorMap::TimeCost() const {
  double cost = 0.0;
  for (auto &[level, estimator] : estimators)
    cost += estimator.GetAggregate().timeCost;
  return cost;
}

double EstimatorMap::MemoryCost() const {
  double memCost = 0.0;
  for (const auto &[level, estimator] : estimators)
    memCost += estimator.GetAggregate().memoCost;
  return memCost;
}

#include "Transfers.hpp"

Vector EstimatorMap::MeanField(const std::string &key) const {
  if (!AGGREGATE_FOR_SOLUTION) { Exit("Mean Field not available without AGGREGATE_FOR_SOLUTION") }

  const auto &mean0 = estimators.at(mlConf.initLevels[0]).GetAggregate().rdF.at(key).GetMean();
  auto coarseMeanField = std::make_shared<Vector>(mean0);
  const auto levels = GetLevels();
  for (int i = 1; i < estimators.size(); i++) {
    const auto &meanLevel = estimators.at(levels[i]).GetAggregate().rdF.at(key).GetMean();
    Vector transferCoarseField(0.0, meanLevel);
    Vector fineField(meanLevel);
    auto transfer = GetTransfer(*coarseMeanField, transferCoarseField, "Matrix");
    transfer->Prolongate(*coarseMeanField, transferCoarseField);
    transferCoarseField += fineField;
    coarseMeanField = std::make_shared<Vector>(transferCoarseField);
  }
  return *coarseMeanField;
}

Vector EstimatorMap::SVarField(const std::string &key) const {
  if (!AGGREGATE_FOR_SOLUTION) { Exit("SVar Field not available without AGGREGATE_FOR_SOLUTION") }

  const auto &sVar0 = estimators.at(mlConf.initLevels[0]).GetAggregate().rdF.at(key).GetSVar();
  const auto levels = GetLevels();
  const auto ctrs = GetMVector();
  auto coarseSVarField = std::make_shared<Vector>(sVar0);
  *coarseSVarField *= 1.0 / ctrs[0];
  for (int i = 1; i < estimators.size(); i++) {
    const auto &sVarLevel = estimators.at(levels[i]).GetAggregate().rdF.at(key).GetSVar();
    Vector transferCoarseField(0.0, sVarLevel);
    Vector fineField(sVarLevel);
    auto transfer = GetTransfer(*coarseSVarField, transferCoarseField, "Matrix");
    transfer->Prolongate(*coarseSVarField, transferCoarseField);
    fineField *= 1.0 / ctrs[i];
    transferCoarseField += fineField;
    coarseSVarField = std::make_shared<Vector>(transferCoarseField);
  }
  return *coarseSVarField;
}

double EstimatorMap::TimeCostPrediction() const {
  double costPrediction = 0.0;
  for (auto const &[level, estimator] : estimators)
    costPrediction += estimator.SamplesToCompute() * estimator.GetAggregate().costPerSample;
  return costPrediction;
}

double EstimatorMap::MemoCostPrediction() const {
  // Only to prevent crashes in estimator map test
  for (auto const &[level, estimator] : estimators) {
    if (!estimator.MXFEMIsInitialized()) return 0.0;
  }

  double memoryConstant = MemoryConstant();
  double memoryPrediction = memoryOffset;
  for (auto const &[level, estimator] : estimators) {
    if (estimator.GetAggregate().expectedMemoryCost == 0.0) {
      for (int split = 0; split <= PPM->MaxCommSplit(); split++) {
        if (estimator.GetMXFEM()->GetMeshes().Contains(MeshIndex{level, -1, 0, split})) {
          memoryPrediction += memoryConstant * estimator.CellsOn(MeshIndex{level, -1, 0, split});
        }
      }
    } else {
      memoryPrediction += estimator.GetAggregate().expectedMemoryCost;
    }
  }
  return memoryPrediction;
}

double EstimatorMap::MemoryConstant() const {
  double totalMemory = MemoryLogger::MemoryUsage() - memoryOffset;
  int totalNumberOfCells = 0;
  for (auto const &[level, estimator] : estimators) {
    for (int split = 0; split <= PPM->MaxCommSplit(); split++) {
      if (estimator.GetMXFEM()->GetMeshes().Contains(MeshIndex{level, -1, 0, split}))
        totalNumberOfCells += estimator.CellsOn(MeshIndex{level, -1, 0, split});
    }
  }
  return totalMemory / totalNumberOfCells;
}

double EstimatorMap::CostWithoutSyncLosses() const {
  double costWithoutSyncLosses = 0.0;
  for (auto &[level, estimator] : estimators)
    costWithoutSyncLosses +=
        estimator.GetAggregate().cost.at("C").GetMean() * estimator.GetAggregate().ctr.M;
  return costWithoutSyncLosses;
}

double EstimatorMap::SynchronizationEfficiency() const {
  return CostWithoutSyncLosses() / TimeCost();
}

bool EstimatorMap::OpenSamples() {
  for (auto const &[level, estimator] : estimators)
    if (estimator.SamplesToCompute() > 0) return true;
  return false;
}

std::vector<int> EstimatorMap::GetMVector() const {
  std::vector<int> mVector;
  for (auto const &[level, estimator] : estimators)
    mVector.push_back(estimator.GetAggregate().ctr.M);
  return mVector;
}

double EstimatorMap::MemoryOffset() const { return memoryOffset; }

void EstimatorMap::UpdateMemoryOffset(double totalMemory) {
  memoryOffset = (totalMemory - MemoryCost());
}

std::ostream &operator<<(std::ostream &s, const EstimatorMap &estMap) {
  return s << "Level=" << vec2str(estMap.GetLevels()) << endl
           << "M_lvl=" << vec2str(estMap.GetMVector())
           << endl
           //           << "MeanY=" << vec2str(estMap.GetMeanDelQoI("Y")) << endl
           << "MeanQ=" << vec2str(estMap.GetMeanQoI("Q"))
           << endl
           //           << "SVarY=" << vec2str(estMap.GetSVarDelQoI("Y")) << endl
           << "SVarQ=" << vec2str(estMap.GetSVarQoI("Q"))
           << endl
           //           << "SkewY=" << vec2str(estMap.GetSkewDelQoI("Y")) << endl
           //           << "KurtY=" << vec2str(estMap.GetKurtDelQoI("Y")) << endl
           << "MeanC=" << vec2str(estMap.GetTimeCostPerSample("C")) << endl
           << "TCost=" << vec2str(estMap.GetTimeCostPerSample("C")) << endl;
}

/*
 * Start of GetVector functions
 */

std::vector<int> EstimatorMap::GetdMs() const { // takes no key, as shared for all components
  return GetVector<int>("", [&](const SingleLevelEstimator &estimator) {
    return estimator.SamplesToCompute();
  });
}

std::vector<int> EstimatorMap::GetCommSplits() const { // same as above
  return GetVector<int>("", [&](const SingleLevelEstimator &estimator) {
    return estimator.CommSplit();
  });
}

std::vector<int> EstimatorMap::GetOverallMaxCommSplits() const { // same as above
  return GetVector<int>("", [&](const SingleLevelEstimator &estimator) {
    return estimator.GetAggregate().overallMaxCommSplit;
  });
}

std::vector<int> EstimatorMap::GetLevels() const {                       //
  return GetVector<int>("", [&](const SingleLevelEstimator &estimator) { //
    return estimator.Level();                                            //
  });                                                                    //
}

std::vector<double> EstimatorMap::GetMeanQoI(const std::string &key) const {
  return GetVector<double>(key, [&](const SingleLevelEstimator &estimator) {
    return estimator.GetAggregate().qoI.at(key).GetMean();
  });
}

std::vector<double> EstimatorMap::GetSVarQoI(const std::string &key) const {
  return GetVector<double>(key, [&](const SingleLevelEstimator &estimator) {
    return estimator.GetAggregate().qoI.at(key).GetSVar();
  });
}

std::vector<double> EstimatorMap::GetSkewQoI(const std::string &key) const {
  return GetVector<double>(key, [&](const SingleLevelEstimator &estimator) {
    return estimator.GetAggregate().qoI.at(key).GetSkew();
  });
}

std::vector<double> EstimatorMap::GetKurtQoI(const std::string &key) const {
  return GetVector<double>(key, [&](const SingleLevelEstimator &estimator) {
    return estimator.GetAggregate().qoI.at(key).GetKurt();
  });
}

std::vector<double> EstimatorMap::GetTimeCostPerSample(const std::string &key) const {
  return GetVector<double>(key, [&](const SingleLevelEstimator &estimator) {
    return estimator.GetAggregate().costPerSample;
  });
}

std::vector<double> EstimatorMap::GetMemoCostPerSample(const std::string &key) const {
  std::vector<double> memVectorSS = GetTotalMemoCost(key);
  int index = 0;
  for (const auto &[level, estimator] : estimators) {
    memVectorSS[index] /= std::pow(2, estimator.GetAggregate().overallMaxCommSplit);
    index++;
  }
  return memVectorSS;
}

std::vector<double> EstimatorMap::GetTotalTimeCost(const std::string &key) const {
  return GetVector<double>(key, [&](const SingleLevelEstimator &estimator) {
    return estimator.GetAggregate().timeCost;
  });
}

std::vector<double> EstimatorMap::GetTotalMemoCost(const std::string &key) const {
  return GetVector<double>(key, [&](const SingleLevelEstimator &estimator) {
    return estimator.GetAggregate().memoCost;
  });
}

#ifdef AGGREGATE_FOR_SOLUTION

// outer
std::vector<double> EstimatorMap::GetMeanOuterNormRdF(const std::string &key) const {
  return GetVector<double>(key, [&](const SingleLevelEstimator &estimator) {
    return estimator.GetAggregate().outerNorms.at(key).mean;
  });
}

std::vector<double> EstimatorMap::GetsVarOuterNormRdF(const std::string &key) const {
  return GetVector<double>(key, [&](const SingleLevelEstimator &estimator) {
    return estimator.GetAggregate().outerNorms.at(key).sVar;
  });
}

std::vector<double> EstimatorMap::GetSkewOuterNormRdF(const std::string &key) const {
  return GetVector<double>(key, [&](const SingleLevelEstimator &estimator) {
    return estimator.GetAggregate().outerNorms.at(key).skew;
  });
}

std::vector<double> EstimatorMap::GetKurtOuterNormRdF(const std::string &key) const {
  return GetVector<double>(key, [&](const SingleLevelEstimator &estimator) {
    return estimator.GetAggregate().outerNorms.at(key).kurt;
  });
}

std::vector<double> EstimatorMap::GetMeanOuterNormDelRdF(const std::string &key) const {
  auto localKey = (key.find("\u2206") == std::string::npos) ? std::format("\u2206{}", key) : key;
  return GetVector<double>(localKey, [&](const SingleLevelEstimator &estimator) {
    return estimator.GetAggregate().outerNorms.at(localKey).mean;
  });
}

std::vector<double> EstimatorMap::GetSVarOuterNormDelRdF(const std::string &key) const {
  auto localKey = (key.find("\u2206") == std::string::npos) ? std::format("\u2206{}", key) : key;
  return GetVector<double>(localKey, [&](const SingleLevelEstimator &estimator) {
    return estimator.GetAggregate().outerNorms.at(localKey).sVar;
  });
}

std::vector<double> EstimatorMap::GetSkewOuterNormDelRdF(const std::string &key) const {
  auto localKey = (key.find("\u2206") == std::string::npos) ? std::format("\u2206{}", key) : key;
  return GetVector<double>(localKey, [&](const SingleLevelEstimator &estimator) {
    return estimator.GetAggregate().outerNorms.at(localKey).skew;
  });
}

std::vector<double> EstimatorMap::GetKurtOuterNormDelRdF(const std::string &key) const {
  auto localKey = (key.find("\u2206") == std::string::npos) ? std::format("\u2206{}", key) : key;
  return GetVector<double>(localKey, [&](const SingleLevelEstimator &estimator) {
    return estimator.GetAggregate().outerNorms.at(localKey).kurt;
  });
}

// inner
std::vector<double> EstimatorMap::GetMeanInnerNormRdF(const std::string &key) const {
  return GetVector<double>(key, [&](const SingleLevelEstimator &estimator) {
    return estimator.GetAggregate().innerNorms.at(key).GetMean();
  });
}

std::vector<double> EstimatorMap::GetSVarInnerNormRdF(const std::string &key) const {
  return GetVector<double>(key, [&](const SingleLevelEstimator &estimator) {
    return estimator.GetAggregate().innerNorms.at(key).GetSVar();
  });
}

std::vector<double> EstimatorMap::GetSkewInnerNormRdF(const std::string &key) const {
  return GetVector<double>(key, [&](const SingleLevelEstimator &estimator) {
    return estimator.GetAggregate().innerNorms.at(key).GetSkew();
  });
}

std::vector<double> EstimatorMap::GetKurtInnerNormRdF(const std::string &key) const {
  return GetVector<double>(key, [&](const SingleLevelEstimator &estimator) {
    return estimator.GetAggregate().innerNorms.at(key).GetKurt();
  });
}

std::vector<double> EstimatorMap::GetMeanInnerNormDelRdF(const std::string &key) const {
  auto localKey = (key.find("\u2206") == std::string::npos) ? std::format("\u2206{}", key) : key;
  return GetVector<double>(localKey, [&](const SingleLevelEstimator &estimator) {
    return estimator.GetAggregate().innerNorms.at(localKey).GetMean();
  });
}

std::vector<double> EstimatorMap::GetSVarInnerNormDelRdF(const std::string &key) const {
  auto localKey = (key.find("\u2206") == std::string::npos) ? std::format("\u2206{}", key) : key;
  return GetVector<double>(localKey, [&](const SingleLevelEstimator &estimator) {
    return estimator.GetAggregate().innerNorms.at(localKey).GetSVar();
  });
}

std::vector<double> EstimatorMap::GetSkewInnerNormDelRdF(const std::string &key) const {
  auto localKey = (key.find("\u2206") == std::string::npos) ? std::format("\u2206{}", key) : key;
  return GetVector<double>(localKey, [&](const SingleLevelEstimator &estimator) {
    return estimator.GetAggregate().innerNorms.at(localKey).GetSkew();
  });
}

std::vector<double> EstimatorMap::GetKurtInnerNormDelRdF(const std::string &key) const {
  auto localKey = (key.find("\u2206") == std::string::npos) ? std::format("\u2206{}", key) : key;
  return GetVector<double>(localKey, [&](const SingleLevelEstimator &estimator) {
    return estimator.GetAggregate().innerNorms.at(localKey).GetKurt();
  });
}

// Bochner
std::vector<double> EstimatorMap::GetBochnerNormDelRdF(const std::string &key) const {
  auto localKey = (key.find("\u2206") == std::string::npos) ? std::format("\u2206{}", key) : key;
  return GetVector<double>(localKey, [&](const SingleLevelEstimator &estimator) {
    return estimator.GetAggregate().rdF.at(localKey).GetBochner();
  });
}

std::vector<double> EstimatorMap::GetBochnerNormRdF(const std::string &key) const {
  return GetVector<double>(key, [&](const SingleLevelEstimator &estimator) {
    return estimator.GetAggregate().rdF.at(key).GetBochner();
  });
}

std::vector<double> EstimatorMap::GetBochnerNormDelQoI(const std::string &key) const {
  auto localKey = (key.find("\u2206") == std::string::npos) ? std::format("\u2206{}", key) : key;
  return GetVector<double>(localKey, [&](const SingleLevelEstimator &estimator) {
    return estimator.GetAggregate().qoI.at(localKey).GetBochner();
  });
}

std::vector<double> EstimatorMap::GetBochnerNormQoI(const std::string &key) const {
  return GetVector<double>(key, [&](const SingleLevelEstimator &estimator) {
    return estimator.GetAggregate().qoI.at(key).GetBochner();
  });
}

#endif

std::vector<std::string> EstimatorMap::GetAllRdFKeys() const {
  return estimators.rbegin()->second.GetAggregate().GetAllRdFKeys();
}

std::vector<std::string> EstimatorMap::GetDelRdFKeys() const {
  return estimators.rbegin()->second.GetAggregate().GetDelRdFKeys();
}

std::vector<std::string> EstimatorMap::GetAllQoIKeys() const {
  return estimators.rbegin()->second.GetAggregate().GetAllQoIKeys();
}

std::vector<std::string> EstimatorMap::GetDelQoIKeys() const {
  return estimators.rbegin()->second.GetAggregate().GetDelQoIKeys();
}

std::vector<std::string> EstimatorMap::GetRdFKeys() const {
  return estimators.rbegin()->second.GetAggregate().GetRdFKeys();
}

std::vector<std::string> EstimatorMap::GetQoIKeys() const {
  return estimators.rbegin()->second.GetAggregate().GetQoIKeys();
}

std::vector<std::string> EstimatorMap::GetOutKeys() const {
  return estimators.rbegin()->second.GetAggregate().GetOutKeys();
}

std::vector<std::string> EstimatorMap::GetAllOutKeys() const {
  return estimators.rbegin()->second.GetAggregate().GetAllOutKeys();
}

std::vector<std::string> EstimatorMap::GetDelOutKeys() const {
  return estimators.rbegin()->second.GetAggregate().GetDelOutKeys();
}

std::vector<std::string> EstimatorMap::GetInKeys() const {
  return estimators.rbegin()->second.GetAggregate().GetInKeys();
}

std::vector<std::string> EstimatorMap::GetAllInKeys() const {
  return estimators.rbegin()->second.GetAggregate().GetAllInKeys();
}

std::vector<std::string> EstimatorMap::GetDelInKeys() const {
  return estimators.rbegin()->second.GetAggregate().GetDelInKeys();
}

SampleData EstimatorMap::GetEstimate(const SampleID &id) const {
  SampleData sampleData(id);
  if constexpr (AGGREGATE_FOR_SOLUTION) {
    for (const auto &key : GetDelRdFKeys()) {
      auto position = key.find("\u2206");
      std::string localKey = key;
      localKey.erase(position, 3);
      sampleData.Insert(std::format("E({})", localKey), MeanField(key));
      sampleData.Insert(std::format("V({})", localKey), SVarField(key));
    }
  }

  std::map<std::string, double> map;
  for (const auto &key : GetDelQoIKeys()) {
    auto position = key.find("\u2206");
    std::string localKey = key;
    localKey.erase(position, 3);
    for (auto &[level, estimator] : estimators) {
      map[localKey] += estimator.GetAggregate().qoI.at(key).GetMean();
    }
  }

  for (auto &key : GetQoIKeys())
    sampleData.Insert(std::format("E({})", key), map.at(key));

  sampleData.Insert("MemoryOffset", MemoryOffset());
  sampleData.Insert("TotalMemoCostSum", MemoryCost());
  sampleData.Insert("TotalTimeCostSum", TimeCost());
  sampleData.Insert("TotalCPUTimeCostSum", TimeCost() * PPM->Size(0));
  sampleData.Insert("Synchronization Efficiency", SynchronizationEfficiency());

  sampleData.Insert("rmse", errors.GetRMSE(mlConf.dataKey));
  sampleData.Insert("mse", errors.GetMSE(mlConf.dataKey));
  sampleData.Insert("sam", errors.GetSAM(mlConf.dataKey));
  sampleData.Insert("num", errors.GetNUM(mlConf.dataKey));

  sampleData.Insert("alpha", exponents.GetObjAlpha(mlConf.dataKey));
  sampleData.Insert("beta", exponents.GetObjBeta(mlConf.dataKey));
  sampleData.Insert("gammaCT", exponents.GetTimeGamma(mlConf.dataKey));
  sampleData.Insert("gammaMem", exponents.GetMemoGamma(mlConf.dataKey));

  return sampleData;
}

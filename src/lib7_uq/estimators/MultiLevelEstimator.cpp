#include "MultiLevelEstimator.hpp"
#include <ranges>
#include "Plotting.hpp"

Estimator &MultiLevelEstimator::Method(const OptionalVector &input) {
  if (epsilon == 0.0 && mlEstmConf.timeBudget == 0.0) MLE(input);
  else if (epsilon != 0.0 && mlEstmConf.timeBudget == 0.0) AMLE(input);
  else if (epsilon == 0.0 && mlEstmConf.timeBudget != 0.0) BMLE(input);
  else { Exit("Configuration error") }
  return *this;
}

void MultiLevelEstimator::MLE(const OptionalVector &input) {
  mout.StartBlock("MultiLevelEstimator");
  vout(1) << "dM0=" << vec2str(estMap.GetdMs()) << endl;

  if (!levelLoop(input)) { Exit("No initial samples") }

  mout.EndBlock(verbose == 0);
}

void MultiLevelEstimator::AMLE(const OptionalVector &input) {
  mout.StartBlock("AdaptiveMultiLevelEstimator");             //
  vout(1) << "TargetEpsilon=" << epsilon                      //
          << " and dM0=" << vec2str(estMap.GetdMs()) << endl; //

  if (!levelLoop(input)) { Exit("No initial samples") }

  while (!targetEpsilonReached())
    if (appendLevelIfNeeded() || updateSamplesIfNeeded())
      if (!levelLoop(input)) continue;

  mout.EndBlock(verbose == 0);
}

void MultiLevelEstimator::BMLE(const OptionalVector &input) {
  mout.StartBlock("BudgetedMultiLevelEstimator");             //
  vout(1) << "TimeBudget=" << mlEstmConf.timeBudget           //
          << " MemoryBudget=" << mlEstmConf.memoBudget        //
          << " and dM0=" << vec2str(estMap.GetdMs()) << endl; //

  if (!levelLoop(input)) { Exit("No initial samples") }

  epsilon = mlEstmConf.eta * estMap.GetErrors().GetRMSE(mlEstmConf.dataKey);
  contData.PushBack(estMap, epsilon);

  while (!timeBudgetIsExhausted()) {
    if (appendLevelIfNeeded() || updateSamplesIfNeeded()) {
      if (expectedToBeFeasible()) {
        if (levelLoop(input)) { contData.PushBack(estMap, epsilon); }
      } else if (tryUpdateEpsilonOrSampleNumbers()) continue;
      else break;
    }
    epsilon = mlEstmConf.eta * epsilon;
  }

  mout.EndBlock(verbose == 0);
}

bool MultiLevelEstimator::levelLoop(const OptionalVector &input) {
  if (!estMap.OpenSamples()) return false;

  mout.StartBlock("LevelLoop");
  vout(1) << "Start with expected time " << estMap.TimeCostPrediction() << " seconds,";
  vout(1) << " and expected memory " << estMap.MemoCostPrediction() << " Mbyte" << endl;
  if (verbose >= 2) MemoryLogger::LogMemory();

  std::map<int, std::pair<double, double>> timeMap{};
  for (auto &iter : estMap) {
    if (iter.second.SamplesToCompute() > 0) {
      timeMap[iter.first].first = MPI_Wtime();
      iter.second.Method(input);
      timeMap[iter.first].second = MPI_Wtime();
    }
  }

  updateData(timeMap);
  double totalMemUsage = MemoryLogger::TotalMemoryUsage();
  estMap.UpdateMemoryOffset(totalMemUsage);

  bool tB = mlEstmConf.timeBudget != 0.0;
  bool mB = mlEstmConf.memoBudget != std::numeric_limits<double>::max();
  vout(2) << estMap << endl;
  vout(1) << "Objective=" << mlEstmConf.objective << " | ";
  vout(1) << "alpha=" << estMap.GetExponents().GetObjAlpha(mlEstmConf.dataKey);
  vout(1) << " beta=" << estMap.GetExponents().GetObjBeta(mlEstmConf.dataKey);
  vout(1) << " gammaCT=" << estMap.GetExponents().GetTimeGamma(mlEstmConf.dataKey);
  vout(1) << " gammaMem=" << estMap.GetExponents().GetMemoGamma(mlEstmConf.dataKey);
  vout(1) << endl << "epsilon=" << epsilon;
  vout(1) << " rmse=" << estMap.GetErrors().GetRMSE(mlEstmConf.dataKey);
  vout(1) << " | mse=" << estMap.GetErrors().GetMSE(mlEstmConf.dataKey);
  vout(1) << " sam=" << estMap.GetErrors().GetSAM(mlEstmConf.dataKey);
  vout(1) << " num=" << estMap.GetErrors().GetNUM(mlEstmConf.dataKey);
  vout(1) << endl << "cost_time=" << estMap.TimeCost();
  vout(1) << " cost_mem_total=" << totalMemUsage;
  vout(1) << " cost_mem_sum=" << estMap.MemoryCost();
  vout(1) << " mem_offset=" << estMap.MemoryOffset() << endl;
  if (tB) { vout(1) << "timeCost/timeBudget=" << estMap.TimeCost() / mlEstmConf.timeBudget << " "; }
  if (mB) { vout(1) << "memoCost/memoBudget=" << totalMemUsage / mlEstmConf.memoBudget << " "; }
  if (mB || mB) { vout(1) << endl; }

  if constexpr (AGGREGATE_FOR_SOLUTION) {
    if (plotting >= 1) {
      VtuPlot vtuPlot;
      const size_t &estRnd = contData.epsilons.size();
      for (const auto &key : estMap.GetRdFKeys()) {
        vtuPlot.AddData(std::format("MeanField.{}", key), estMap.MeanField(key));
        vtuPlot.AddData(std::format("SVarField.{}", key), estMap.SVarField(key));
      }
      vtuPlot.PlotFile(std::format("Estimates.{}", estRnd));
    }
  }

  mout.EndBlock(verbose == 0);
  return true;
}

void MultiLevelEstimator::updateData(std::map<int, std::pair<double, double>> timeMap) {
  for (auto &iter : std::ranges::reverse_view(estMap)) {
    if (iter.second.SamplesToCompute() > 0) {
      if (mlEstmConf.slEstmConf.delayParallelUpdate) { iter.second.UpdateParallel(); }
      if (mlEstmConf.slEstmConf.delayTotalUpdate) {
        iter.second.UpdateTotal();
        iter.second.UpdateErrors();
      }
    }
    iter.second.ResetExpectedMemoryCost();
  }
  estMap.UpdateExponentsAndErrors();
}

bool MultiLevelEstimator::timeBudgetIsExhausted() {
  if (0.95 * mlEstmConf.timeBudget <= estMap.TimeCost()) {
    vout(1) << "Budget is exhausted over 95%" << endl;
    return true;
  }
  return false;
}

bool MultiLevelEstimator::targetEpsilonReached() {
  return (estMap.GetErrors().GetRMSE(mlEstmConf.dataKey) < epsilon);
}

bool MultiLevelEstimator::expectedToBeFeasible() {
  const double timeCostPrediction = estMap.TimeCostPrediction();
  const double memoCostPrediction = estMap.MemoCostPrediction();
  const double leftOverBudget = mlEstmConf.timeBudget - estMap.TimeCost();

  if (timeCostPrediction >= leftOverBudget) {                               //
    vout(1) << "TimeCostPrediction=" << timeCostPrediction                  //
            << " leftOverBudget=" << leftOverBudget << endl;                //
    return false;                                                           //
  } else if (memoCostPrediction >= mlEstmConf.memoBudget) {                 //
    vout(1) << "MemoCostPrediction=" << memoCostPrediction                  //
            << " memoryBudget=" << mlEstmConf.memoBudget                    //
            << " memoryOffset=" << estMap.MemoryOffset() << endl;           //
    return false;                                                           //
  } else {                                                                  //
    attemptCtr = 0;                                                         //
    if (timeCostPrediction != 0.0)                                          //
      vout(1) << "Estimation round i=" << contData.epsilons.size() << endl; //
    return true;                                                            //
  }
}

bool MultiLevelEstimator::tryUpdateEpsilonOrSampleNumbers() {
  if (attemptCtr <= 7) {
    attemptCtr++;
    vout(2) << "Epsilons=" << vec2str(contData.epsilons) << " " << epsilon << endl;
    epsilon = 0.5 * (contData.epsilons.back() + epsilon);
    estMap.ResetSampleAmount();
    vout(2) << "Increased epsilon=" << epsilon << endl;
    return true;
  } else {
    vout(1) << "Can't increase epsilon more " << epsilon << endl;
    return false;
  }
}

bool MultiLevelEstimator::appendLevelIfNeeded() {
  if (estMap.GetErrors().GetDISC(mlEstmConf.dataKey) < sqrt(1 - mlEstmConf.theta) * epsilon) {
    return false;
  }
  if (estMap.UpdateNewLevel(epsilon, mlEstmConf.theta, estMap.rbegin()->first + 1)) {
    vout(1) << "Appended level=" << estMap.rbegin()->first
            << " with dM=" << estMap.rbegin()->second.SamplesToCompute() << " | epsilon=" << epsilon
            << endl;

    return true;
  } else {
    tryUpdateEpsilonOrSampleNumbers();
    return false;
  }
}

bool MultiLevelEstimator::updateSamplesIfNeeded() {
  if (estMap.GetErrors().GetSAM(mlEstmConf.dataKey) < mlEstmConf.theta * epsilon * epsilon) {
    return false;
  }
  estMap.UpdateSampleCounterOnMap(epsilon, mlEstmConf.theta);
  vout(1) << "dM=" << vec2str(estMap.GetdMs()) << " | epsilon=" << epsilon << endl;
  return true;
}

const SampleData MultiLevelEstimator::WithdrawEstimate() {
  SampleData sampleData(estMap.GetEstimate());
  auto openSamplesForNextRound = estMap.GetMVector();
  const auto openLevelsForNextRound = estMap.GetLevels();
  for (auto &iter : estMap) {
    iter.second.ReInitAggregate();
  }
  UpdateSampleAmount(openLevelsForNextRound, openSamplesForNextRound);

  return sampleData;
}

void MultiLevelEstimator::UpdateSampleAmount(std::vector<int> levels,
                                             std::vector<int> requestedSamples) {
  estMap.UpdateSampleCounterOnMap(requestedSamples);
}

std::string outKey4Map(const std::string &key, const std::string &moment) {
  std::string tmp = key;
  tmp.insert(key.find("(") + 1, std::format("{}(", moment));
  tmp += ")";
  return tmp;
}

void MultiLevelEstimator::PrintMultilevel() const {
  std::vector<InfoEntry<std::vector<int>>> intEntries;
  intEntries.emplace_back(InfoEntry("Used Levels", estMap.GetLevels()));
  intEntries.emplace_back(InfoEntry("Max CommSplits", estMap.GetOverallMaxCommSplits()));
  intEntries.emplace_back(InfoEntry("Used Samples", estMap.GetMVector()));
  mout.PrintInfo("Index", 1, intEntries);

  std::vector<InfoEntry<std::vector<double>>> e;
  e.emplace_back(InfoEntry("TotalTimeCost", estMap.GetTotalTimeCost()));
  e.emplace_back(InfoEntry("TotalMemoCost", estMap.GetTotalMemoCost()));
  e.emplace_back(InfoEntry("E(TotalTimeCost)", estMap.GetTimeCostPerSample()));
  e.emplace_back(InfoEntry("E(TotalMemoCost)", estMap.GetMemoCostPerSample()));
  mout.PrintInfo("Costs", 1, e);

  e.clear();
  for (const auto &key : estMap.GetAllQoIKeys()) {
    e.emplace_back(InfoEntry(std::format("E({})", key), estMap.GetMeanQoI(key)));
    e.emplace_back(InfoEntry(std::format("V({})", key), estMap.GetSVarQoI(key)));
    e.emplace_back(InfoEntry(std::format("S({})", key), estMap.GetSkewQoI(key), 2));
    e.emplace_back(InfoEntry(std::format("K({})", key), estMap.GetKurtQoI(key), 2));
  }

  if constexpr (AGGREGATE_FOR_SOLUTION) {
    for (const auto &key : estMap.GetAllRdFKeys()) {
      e.emplace_back(InfoEntry(std::format("BL2({})", key), estMap.GetBochnerNormRdF(key)));
    }
    for (const auto &key : estMap.GetAllOutKeys()) {
      e.emplace_back(InfoEntry(outKey4Map(key, "E"), estMap.GetMeanOuterNormRdF(key)));
      e.emplace_back(InfoEntry(outKey4Map(key, "V"), estMap.GetsVarOuterNormRdF(key)));
      e.emplace_back(InfoEntry(outKey4Map(key, "S"), estMap.GetSkewOuterNormRdF(key), 2));
      e.emplace_back(InfoEntry(outKey4Map(key, "K"), estMap.GetKurtOuterNormRdF(key), 2));
    }
    for (const auto &key : estMap.GetAllInKeys()) {
      e.emplace_back(InfoEntry(std::format("E({})", key), estMap.GetMeanInnerNormRdF(key)));
      e.emplace_back(InfoEntry(std::format("V({})", key), estMap.GetSVarInnerNormRdF(key)));
      e.emplace_back(InfoEntry(std::format("S({})", key), estMap.GetSkewInnerNormRdF(key), 2));
      e.emplace_back(InfoEntry(std::format("K({})", key), estMap.GetKurtInnerNormRdF(key), 2));
    }
  }
  mout.PrintInfo("Multilevel", 1, e);
}

void MultiLevelEstimator::PrintExponent() const {
  std::vector<InfoEntry<double>> e;
  const auto &exponents = estMap.GetExponents();
  for (const auto &key : estMap.GetDelQoIKeys()) {
    e.emplace_back(InfoEntry(std::format("alpha({})", key), exponents.alpha.at(key)));
    e.emplace_back(InfoEntry(std::format("beta({})", key), exponents.beta.at(key)));
  }
  e.emplace_back(InfoEntry("gammaCT", exponents.gammaCT));
  e.emplace_back(InfoEntry("gammaMem", exponents.gammaMem));
  if constexpr (AGGREGATE_FOR_SOLUTION) {
    for (const auto &key : estMap.GetDelOutKeys()) {
      e.emplace_back(InfoEntry(std::format("alphaOut({})", key), exponents.alphaOuter.at(key)));
      e.emplace_back(InfoEntry(std::format("betaOut({})", key), exponents.betaOuter.at(key), 2));
    }
    for (const auto &key : estMap.GetDelInKeys()) {
      e.emplace_back(InfoEntry(std::format("alphaIn({})", key), exponents.alphaInner.at(key), 1));
      e.emplace_back(InfoEntry(std::format("betaIn({})", key), exponents.betaInner.at(key), 1));
    }
    for (const auto &key : estMap.GetDelRdFKeys()) {
      e.emplace_back(InfoEntry(std::format("betaBoch({})", key),
                               exponents.betaBochner.at(std::format("L2({})", key))));
    }
  }
  mout.PrintInfo("Exponents", verbose, e);
}

void MultiLevelEstimator::PrintInfo() const {
  mlEstmConf.PrintInfo();
  estMap.GetErrors().PrintInfo();
  estMap.GetEstimate().PrintInfo();
  PrintExponent();
  PrintMultilevel();
  contData.PrintInfo();
}

#ifndef MPP_ERRORDATA_H
#define MPP_ERRORDATA_H

#include "Config.hpp"

class WelfordAggregate;

class EstimatorMap;

struct ErrorData {
  std::map<std::string, double> rmse;
  std::map<std::string, double> disc;
  std::map<std::string, double> sam;
  std::map<std::string, double> num;
  std::map<std::string, double> mse;

  void EstimateErrors(const WelfordAggregate &aggregate);

  double GetRMSE(const std::string &key) const;

  double GetMSE(const std::string &key) const;

  double GetSAM(const std::string &key) const;

  double GetNUM(const std::string &key) const;

  double GetDISC(const std::string &key) const;

  std::vector<std::string> GetErrorKeys() const;

  friend std::ostream &operator<<(std::ostream &s, const ErrorData &errors) {
    for (const auto &key : errors.GetErrorKeys()) {                           //
      s << std::format("Key:{}", key)                                         //
        << " rmse=" << errors.rmse.at(key) << " disc=" << errors.disc.at(key) //
        << " sam=" << errors.sam.at(key) << " num=" << errors.num.at(key)     //
        << " mse=" << errors.mse.at(key) << endl;                             //
    }
    return s;
  }

  void PrintInfo() const {
    std::vector<InfoEntry<double>> e;
    for (const auto &key : GetErrorKeys()) {
      e.emplace_back(InfoEntry(std::format("RMSE({})", key), rmse.at(key)));
      e.emplace_back(InfoEntry(std::format("MSE({})", key), mse.at(key)));
      e.emplace_back(InfoEntry(std::format("NUM({})", key), num.at(key)));
      e.emplace_back(InfoEntry(std::format("SAM({})", key), sam.at(key)));
    }
    mout.PrintInfo("Errors", 1, e);
  }
private:
  void iterateOverMaps(const WelfordAggregate &aggregate);

  void estimateNumeric(const WelfordAggregate &aggregate);

  void estimateStochastic(const WelfordAggregate &aggregate);
};

struct MultilevelErrors : public ErrorData {
  void EstimateErrors(const EstimatorMap &mcMap);
private:
  typedef std::vector<double> Vec;

  void iterateOverMaps(const EstimatorMap &mcMap);

  void estimateNumeric(const EstimatorMap &mcMap);

  void estimateStochastic(const EstimatorMap &mcMap);

  double estimateNumeric(const EstimatorMap &mcMap, const Vec &mean, double alpha) const;

  double estimateStoch(const EstimatorMap &mcMap, const Vec &boch) const;
};

#endif // MPP_ERRORDATA_H

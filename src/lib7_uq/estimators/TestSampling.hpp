#ifndef MPP_TESTSAMPLING_H
#define MPP_TESTSAMPLING_H

class LagrangeTestMXFEM : public MultiXFEM {
public:
  explicit LagrangeTestMXFEM(const MultiXFEMConfig &conf) :
      MultiXFEM("Square", conf),                           //
      disc(std::make_shared<const LagrangeDiscretization>( //
          *meshes, conf.pdeSolverConfigs[0].degree         //
      )) {}                                                //
protected:
  std::shared_ptr<const LagrangeDiscretization> disc;
};

class ProcEvaluationMXFEM : public LagrangeTestMXFEM {
public:
  explicit ProcEvaluationMXFEM(const MultiXFEMConfig &conf) : LagrangeTestMXFEM(conf) {}

  SampleData protocol(const SampleID &id, const OptionalVector &opt) override {
    return SampleData(Name(), id, PPM->Proc(0));
  }

  std::string Name() const override { return "ProcEvaluationMXFEM"; }
};

class ZeroEvaluationMXFEM : public LagrangeTestMXFEM {
public:
  explicit ZeroEvaluationMXFEM(const MultiXFEMConfig &conf) : LagrangeTestMXFEM(conf) {}

  SampleData protocol(const SampleID &id, const OptionalVector &opt) override {
    return SampleData(Name(), id, 0.0);
  }

  std::string Name() const override { return "ZeroEvaluationMXFEM"; }
};

class ModuloIndexEvaluationMXFEM : public LagrangeTestMXFEM {
public:
  explicit ModuloIndexEvaluationMXFEM(const MultiXFEMConfig &conf) : LagrangeTestMXFEM(conf) {}

  SampleData protocol(const SampleID &id, const OptionalVector &opt) override {
    return SampleData(Name(), id, (id.number % 2) ? 1.0 : -1.0);
  }

  std::string Name() const override { return "ModuloIndexEvaluationMXFEM"; }
};

class NormalLagrangeMXFEM : public LagrangeTestMXFEM {
public:
  explicit NormalLagrangeMXFEM(const MultiXFEMConfig &conf) : LagrangeTestMXFEM(conf) {}

protected:
  SampleData protocol(const SampleID &id, const OptionalVector &opt) override {
    auto y = Random::Normal(id.commSplit);
    SampleData sampleData("u", id, disc);
    sampleData.qoIs["Q"] = id.coarse ? 0.0 : y;
    sampleData.randFields.at("u") = id.coarse ? 0.0 : y;

    if (!id.coarse) {
      auto &vector = sampleData.randFields.at("u");
      for (row r = vector.rows(); r != vector.rows_end(); r++) {
        vector(r, 0) = -r()[1] + y;
      }
    }

    return sampleData;
  }

  std::string Name() const override { return "NormalLagrangeProtocol"; }
};

class NormalDGMXFEM : public MultiXFEM {
public:
  explicit NormalDGMXFEM(const MultiXFEMConfig &conf) :
      MultiXFEM("Square", conf),
      disc(std::make_unique<DGDiscretization>(*meshes, conf.pdeSolverConfigs[0].degree)) {}

protected:
  std::shared_ptr<DGDiscretization> disc;

  SampleData protocol(const SampleID &id, const OptionalVector &opt) override {
    auto y = Random::Normal(id.commSplit);
    SampleData sampleData("u", id, disc);
    sampleData.qoIs["Q"] = id.coarse ? 0.0 : y;
    sampleData.randFields.at("u") = id.coarse ? 0.0 : y;

    if (!id.coarse) {
      auto &vector = sampleData.randFields.at("u");
      for (row r = vector.rows(); r != vector.rows_end(); r++) {
        vector(r, 0) = -r()[1] + y;
      }
    }

    return sampleData;
  }

  std::string Name() const override { return "NormalDGMXFEM"; }
};

#endif // MPP_TESTSAMPLING_H

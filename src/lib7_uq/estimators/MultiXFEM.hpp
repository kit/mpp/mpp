#ifndef MPP_MULTIXFEM_HPP
#define MPP_MULTIXFEM_HPP

#include "ConfigStructsUQ.hpp"
#include "Sample.hpp"

class MultiXFEM {
protected:
  int verbose = 0;

  // Todo move into config
  std::string domainName{};

  MultiXFEMConfig conf;

  std::shared_ptr<Meshes> meshes;

  virtual SampleData protocol(const SampleID &id, const OptionalVector &input) = 0;

  virtual SampleData subProtocol(const SampleID &id, const OptionalVector &input) {
    return SampleData();
  };

  OptionalVector distributeInput(const OptionalVector &opt, const MeshIndex &meshIndex);
public:
  MultiXFEM(std::string domainName, MultiXFEMConfig conf) :
      domainName(std::move(domainName)), conf(std::move(conf)) {

    Config::Get("MXFEMVerbose", verbose);

    meshes = MeshesCreator(this->domainName)
                 .WithPLevel(this->conf.cLevel)
                 .WithLevel(this->conf.fLevel)
                 .CreateShared();
  }

  MultiXFEM(std::shared_ptr<Meshes> meshes, MultiXFEMConfig conf) :
      meshes(meshes), domainName(meshes->Name()), conf(std::move(conf)) {
    Config::Get("MSFEMVerbose", verbose);
  }

  SampleData RunProtocol(const SampleID &id, const OptionalVector &input = std::nullopt) {
    mout.StartBlock("MX-FEM");

    const double start = MPI_Wtime();
    vout(1) << "Run Protocol " << Name() << id.IdString() << endl;
    const auto distInput = distributeInput(input, id.GetMeshIndex());
    SampleData sampleData(protocol(id, distInput));
    sampleData.C = MPI_Wtime() - start;

    mout.EndBlock(verbose == 0);
    return sampleData;
  }

  const Meshes &GetMeshes() const { return *meshes; }

  virtual std::string Name() const = 0;

  virtual ~MultiXFEM() = default;
};

std::shared_ptr<MultiXFEM> CreateSharedMXFEM(const MultiXFEMConfig &conf);

#endif // MPP_MULTIXFEM_HPP
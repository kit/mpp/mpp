#ifndef MPP_OPTIMALCONTROLSAMPLING_H
#define MPP_OPTIMALCONTROLSAMPLING_H

#include "CirculantEmbedding.hpp"
#include "EllipticPDESolver.hpp"
#include "MixedPDESolver.hpp"
#include "MultiXFEM.hpp"
#include "SPDESampling.hpp"
#include "TransportPDESolver.hpp"

class OCStochasticLaplace2D : public MultiXFEM {
public:
  double targetCost = 0.0;

  explicit OCStochasticLaplace2D(const MultiXFEMConfig &conf) :
      pdeSolver(PDESolverConfig().WithDegree(1).WithModel("LagrangeElliptic")),
      MultiXFEM("Square", conf), spdeSampler(this->meshes, conf) {
    Config::Get("targetCost", targetCost);
  }

protected:
  SPDESamplingOnSquare spdeSampler;

  EllipticPDESolver pdeSolver;

  SampleData protocol(const SampleID &id, const OptionalVector &control) override {
    SampleData sampleData(subProtocol(id, control));
    const auto &state = sampleData.Get<Vector>("forward");
    const auto &y = sampleData.Get<Vector>("grf");

    auto adjointPDEProblem = EllipticPDEProblemBuilder()
        .WithPermeability([&y](const Point &x, const cell &c) -> Tensor {
          return (0.005 + std::exp(evaluate::value(x, y, *c))) * One;
        })
        .WithSolution([](const Point &x) -> double { return 0.0; })
        .WithLoad([&state](const Point &x, const cell &c) {
          return evaluate::value(x, state, *c)
                 - sin(2.0 * M_PI * x[0]) * sin(2.0 * M_PI * x[1]);
        })
        .WithFlux([](const Point &x) -> VectorField { return {0.0, 0.0}; })
        .WithHasExactSolution([]() -> bool { return true; })
        .WithName([&id]() { return "OCStochasticLaplace2D.Adjoint" + id.IdString(); })
        .WithMeshes(meshes)
        .BuildShared();

    auto adjointSolution = pdeSolver.Run(adjointPDEProblem, id.GetMeshIndex());
    sampleData.Erase<Vector>("grf");

    return sampleData.Insert("backward", adjointSolution);
  }

  SampleData subProtocol(const SampleID &id, const OptionalVector &control) override {
    SampleData sampleData(spdeSampler.DrawSample(id));
    const auto &y = sampleData.Get<Vector>("grf");

    auto statePDEProblem = EllipticPDEProblemBuilder()
        .WithPermeability([&y](const Point &x, const cell &c) -> Tensor {
          return (0.005 + std::exp(evaluate::value(x, y, *c))) * One;
        })
        .WithSolution([](const Point &x) -> double {
          return sin(2.0 * M_PI * x[0]) * sin(2.0 * M_PI * x[1]);
        })
        .WithLoad([&control](const Point &x, const cell &c) {
          if (control == std::nullopt) {
            return 0.0;
          } else {
            return evaluate::value(x, control.value(), *c);
          }
        })
        .WithFlux([](const Point &x) -> VectorField { return {0.0, -1.0}; })
        .WithHasExactSolution([]() -> bool { return true; })
        .WithName([&id]() { return "OCStochasticLaplace2D.State" + id.IdString(); })
        .WithMeshes(meshes)
        .BuildShared();

    auto stateSolution = pdeSolver.Run(statePDEProblem, id.GetMeshIndex());

    sampleData.Insert("forward", stateSolution);
    sampleData.Insert("j", std::pow(stateSolution.values["L2Error"], 2) / 2);

    return sampleData;
  }

  std::string Name() const override { return "OCStochasticLaplace2D"; }

};

class OCLaplace2D : public MultiXFEM {
public:
  double targetCost = 0.0;

  explicit OCLaplace2D(const MultiXFEMConfig &conf) :
      pdeSolver(PDESolverConfig().WithDegree(1).WithModel("LagrangeElliptic")),
      MultiXFEM("Square", conf) {
    Config::Get("targetCost", targetCost);
  }

protected:
  EllipticPDESolver pdeSolver;

  SampleData protocol(const SampleID &id, const OptionalVector &control) override {
    SampleData sampleData(std::move(subProtocol(id, control)));
    const auto &state = sampleData.Get<Vector>("forward");

    auto adjointPDEProblem = EllipticPDEProblemBuilder()
        .WithPermeability([](const Point &x, const cell &c) -> Tensor {
          return One;
        })
        .WithSolution([](const Point &x) -> double { return 0.0; })
        .WithLoad([&state](const Point &x, const cell &c) {
          return evaluate::value(x, state, *c)
                 - sin(2.0 * M_PI * x[0]) * sin(2.0 * M_PI * x[1]);
        })
        .WithFlux([](const Point &x) -> VectorField { return {0.0, 0.0}; })
        .WithHasExactSolution([]() -> bool { return true; })
        .WithName([&id]() { return "OCLaplace2D.Adjoint" + id.IdString(); })
        .WithMeshes(meshes)
        .BuildShared();

    auto adjointSolution = pdeSolver.Run(adjointPDEProblem, id.GetMeshIndex());

    return sampleData.Insert("backward", adjointSolution);
  }

  SampleData subProtocol(const SampleID &id, const OptionalVector &control) override {
    auto statePDEProblem =
        EllipticPDEProblemBuilder()
            .WithPermeability([](const Point &x, const cell &c) -> Tensor { return One; })
            .WithSolution([](const Point &x) -> double {
              return sin(2.0 * M_PI * x[0]) * sin(2.0 * M_PI * x[1]);
            })
            .WithLoad([&control](const Point &x, const cell &c) {
              if (control == std::nullopt) {
                return 0.0;
              } else {
                return evaluate::value(x, control.value(), *c);
              }
            })
            .WithFlux([](const Point &x) -> VectorField { return {0.0, -1.0}; })
            .WithHasExactSolution([]() -> bool { return true; })
            .WithName([&id]() { return "OCLaplace2D.State" + id.IdString(); })
            .WithMeshes(meshes)
            .BuildShared();

    auto stateSolution = pdeSolver.Run(statePDEProblem, id.GetMeshIndex());

    SampleData sampleData("forward", id, stateSolution);
    sampleData.Insert("j", std::pow(stateSolution.values["L2Error"], 2) / 2);

    return sampleData;
  }

  std::string Name() const override { return "OCLaplace2D"; }
};

class OCRiemann2D : public MultiXFEM {
public:
  double targetCost = 0.0;

  explicit OCRiemann2D(const MultiXFEMConfig &conf) :
      MultiXFEM("UnitSquare", conf), pdeSolver(conf.pdeSolverConfigs[0]) {

    Config::Get("targetCost", targetCost);
  }

protected:
  TransportPDESolver pdeSolver;

  static double distX(double x, double d) { return abs(0.5 - x) / (3 * d); };

  static double distY(double y, double d) { return 2 * abs(1.5 * d - y) / d; }

  static double distTwoPoints(double x, double y, double d) {
    return std::min(sqrt(pow(0.2 - x, 2) + pow(0.1 - y, 2)) / d,
                    sqrt(pow(0.9 - x, 2) + pow(0.1 - y, 2)) / d);
  }

  SampleData protocol(const SampleID &id, const OptionalVector &control) override {
    SampleData sampleData(subProtocol(id, control));
    const auto &state = sampleData.Get<Vector>("state");

    double d = 1.0 / 16.0;
    bool smooth = false;
    bool twoPoint = false;
    double endTime = 0.7;

    Config::Get("smooth", smooth);
    Config::Get("twoPoint", twoPoint);
    Config::Get<double>("EndTime", endTime);

    auto adjointPDEProblem =
        TransportPDEProblemBuilder()
            .WithMeshes(meshes)
            .WithName([&id]() { return "OCRiemann2D.Adjoint" + id.IdString(); })
            .WithHasExactSolution(false)
            .WithSolution([&state, d, smooth, twoPoint](double t, const Cell &C,
                                                        const Point &x) -> double {
              auto stateValue = evaluate::value(x, state, C);
              if (twoPoint and (distTwoPoints(x[0], x[1], d) > 1)) return stateValue;
              if (twoPoint) return stateValue - (1 + cos(std::numbers::pi * distTwoPoints(x[0], x[1], d)));
              if (distX(x[0], d) > 1) return stateValue;
              if (distY(x[1], d) > 1) return stateValue;
              if (smooth)
                return stateValue - (1 + cos(std::numbers::pi * distX(x[0], d))) *
                                    (1 + cos(std::numbers::pi * distY(x[1], d)));
              else return stateValue - 1.0;
            })
            .WithFlux([](const MeshIndex &level, const Cell &c, const Point &x) -> VectorField {
              return {0.0, -1.0, 0.0};
            })
            .WithCalculateFwdProblem(false)
            .WithTimeDomain({0.0, endTime})
            .BuildShared();

    auto adjointSolution = pdeSolver.Run(adjointPDEProblem, id.GetMeshIndex());

    if (control != std::nullopt) adjointSolution.vector += targetCost * control.value();

    return sampleData.Insert("adjoint", adjointSolution.vector);
  }

  SampleData subProtocol(const SampleID &id, const OptionalVector &control) override {
    double d = 1.0 / 16.0;
    bool smooth = false;
    bool twoPoint = false;
    double endTime = 0.7;

    Config::Get("smooth", smooth);
    Config::Get("twoPoint", twoPoint);
    Config::Get<double>("EndTime", endTime);

    auto statePDEProblem =
        TransportPDEProblemBuilder()
            .WithMeshes(meshes)
            .WithName([&id]() { return "OCRiemann2D.State" + id.IdString(); })
            .WithHasExactSolution(true)
            .WithSolution([&control, d, smooth, twoPoint](double t, const Cell &C, const Point &x) {
              if (t == 0)
                if (control == std::nullopt) return 0.0;
                else { return evaluate::value(x, control.value(), C); }
              else {
                if (twoPoint and (distTwoPoints(x[0], x[1], d) > 1)) return 0.0;
                if (twoPoint) return (1 + cos(std::numbers::pi * distTwoPoints(x[0], x[1], d)));
                if (distX(x[0], d) > 1) return 0.0;
                if (distY(x[1], d) > 1) return 0.0;
                if (smooth)
                  return (1 + cos(std::numbers::pi * distX(x[0], d))) *
                         (1 + cos(std::numbers::pi * distY(x[1], d)));
                else return 1.0;
              }
            })
            .WithFlux([](const MeshIndex &level, const Cell &c, const Point &x) -> VectorField {
              return {0.0, -1.0, 0.0};
            })
            .WithCalculateFwdProblem(true)
            .WithTimeDomain({0.0, endTime})
            .BuildShared();

    auto stateSolution = pdeSolver.Run(statePDEProblem, id.GetMeshIndex());

    SampleData sampleData("forward", id, stateSolution);
    sampleData.Insert("L2", std::pow(stateSolution.values["Error"], 2) / 2);

    return sampleData;
  }

  std::string Name() const override { return "OCRiemann2D"; }
};

// Todo reinclude
//class OCSquare500 : public MultiXFEM {
//public:
//  double targetCost = 0.0;
//
//  explicit OCSquare500(const MultiXFEMConfig &conf) :
//      MultiXFEM("Square500", std::move(conf)), hybridPDESolver(PDESolverConfig()),
//      transportPDESolver(PDESolverConfig().WithDegree(1)) {
//
//    targetCost = 0.0;
//
//    Config::Get("targetCost", targetCost);
//  }
//protected:
//  HybridPDESolver hybridPDESolver;
//
//  TransportPDESolver transportPDESolver;
//
//  static double distX(double x, double d) { return abs(0.5 - x) / (3 * d); };
//
//  static double distY(double y, double d) { return 2 * abs(1.5 * d - y) / d; }
//
//  static double distTwoPoints(double x, double y, double d) {
//    return min(sqrt(pow(0.2 - x, 2) + pow(0.1 - y, 2)) / d,
//               sqrt(pow(0.9 - x, 2) + pow(0.1 - y, 2)) / d);
//  }
//
//  SampleData protocol(const SampleID &id, const OptionalVector &control) override {
//
//    double d = 1.0 / 16.0;
//    bool smooth = false;
//    bool twoPoint = false;
//    double endTime = 0.7;
//
//    Config::Get("smooth", smooth);
//    Config::Get("twoPoint", twoPoint);
//    Config::Get<double>("EndTime", endTime);
//
//    auto [stateSample, hybridSolution] = subHybridProtocol(id, control);
//
//    const auto transportAdjointProblem =
//        TransportPDEProblemBuilder()
//            .WithFlux([&hybridSolution](const MeshIndex &level, const Cell &c,
//                                        const Point &x) -> VectorField {
//              return hybridSolution.EvaluateCellFlux(c);
//            })
//            .WithFaceNormalFlux([&hybridSolution](const MeshIndex &level, const Cell &c, int face,
//                                                  const VectorField &N, const Point &x) {
//              return hybridSolution.EvaluateNormalFlux(c, face);
//            })
//            .WithName([&id]() { return "OCSquare500.Adjoint" + id.IdString(); })
//            .WithHasExactSolution(false)
//            .WithSolution([&stateSample, d, smooth, twoPoint](double t, const Cell &C,
//                                                              const Point &x) -> double {
//              auto stateValue = evaluate::value(x, stateSample->vectors[0], C);
//              if (twoPoint and (distTwoPoints(x[0], x[1], d) > 1)) return stateValue;
//              if (twoPoint) return stateValue - (1 + cos(Pi * distTwoPoints(x[0], x[1], d)));
//              if (distX(x[0], d) > 1) return stateValue;
//              if (distY(x[1], d) > 1) return stateValue;
//              if (smooth)
//                return stateValue - (1 + cos(Pi * distX(x[0], d))) * (1 + cos(Pi * distY(x[1], d)));
//              else return stateValue - 1.0;
//            })
//            .WithTimeDomain({0.0, endTime})
//            .WithCalculateFwdProblem(false)
//            .WithMeshes(meshes)
//            .BuildShared(); // clang-format on
//
//    const auto adjointSolution = transportPDESolver.Run(transportAdjointProblem, id.GetMeshIndex());
//
//    return {stateSample, adjointSolution.vector};
//  }
//
//  std::pair<OptionalSampleSolution, HybridSolution>
//  subHybridProtocol(const SampleID &id, const OptionalVector &control) {
//
//    double d = 1.0 / 16.0;
//    bool smooth = false;
//    bool twoPoint = false;
//    double endTime = 0.7;
//
//    Config::Get("smooth", smooth);
//    Config::Get("twoPoint", twoPoint);
//    Config::Get<double>("EndTime", endTime);
//
//    const auto mixedProblem =
//        EllipticPDEProblemBuilder::Laplace2D()
//            .WithPermeability([](const Point &x, const cell &c) -> Tensor { return One; })
//            .WithName([]() { return "MixedLaplaceSquare500"; })
//            .WithHasExactSolution([]() { return false; })
//            .WithMeshes(meshes)
//            .BuildShared();
//
//    auto hybridSolution = hybridPDESolver.Run(mixedProblem);
//
//    const auto transportStateProblem =
//        TransportPDEProblemBuilder()
//            .WithFlux([&hybridSolution](const MeshIndex &level, const Cell &c,
//                                        const Point &x) -> VectorField {
//              return hybridSolution.EvaluateCellFlux(c);
//            })
//            .WithFaceNormalFlux([&hybridSolution](const MeshIndex &level, const Cell &c, int face,
//                                                  const VectorField &N, const Point &x) {
//              return hybridSolution.EvaluateNormalFlux(c, face);
//            })
//            .WithName([&id]() { return "OCSquare500.State" + id.IdString(); })
//            .WithHasExactSolution(true)
//            .WithSolution(
//                [&control, d, smooth, twoPoint](double t, const Cell &C, const Point &x) -> double {
//                  if (t == 0)
//                    if (control == std::nullopt) return 0.0;
//                    else { return evaluate::value(x, control.value(), C); }
//                  else {
//                    if (twoPoint and (distTwoPoints(x[0], x[1], d) > 1)) return 0.0;
//                    if (twoPoint) return (1 + cos(Pi * distTwoPoints(x[0], x[1], d)));
//                    if (distX(x[0], d) > 1) return 0.0;
//                    if (distY(x[1], d) > 1) return 0.0;
//                    if (smooth)
//                      return (1 + cos(Pi * distX(x[0], d))) * (1 + cos(Pi * distY(x[1], d)));
//                    else return 1.0;
//              }
//            })
//            .WithTimeDomain({0.0, endTime})
//            .WithCalculateFwdProblem(true)
//            .WithMeshes(meshes)
//            .BuildShared();
//
//    auto stateSolution = transportPDESolver.Run(transportStateProblem, id.GetMeshIndex());
//
//    SampleData stateSample(stateSolution, id);
//
//    stateSample.solution.values.at("L2") = std::pow(stateSolution.values["Error"], 2) / 2;
//
//    return {stateSample, hybridSolution};
//  }
//
//  SampleData subProtocol(const SampleID &id, const OptionalVector &control) override {
//    return {subHybridProtocol(id, control).first, std::nullopt};
//  }
//
//  std::string Name() const override { return "OCRiemann2DSquare500"; }
//};
//
//class OCStochasticTransport : public MultiXFEM {
//public:
//  double targetCost = 0.0;
//
//  explicit OCStochasticTransport(const MultiXFEMConfig &conf) :
//      MultiXFEM("UnitSquare", std::move(conf)), hybridPDESolver(PDESolverConfig()),
//      transportPDESolver(PDESolverConfig().WithDegree(1)) {
//    Config::Get("targetCost", targetCost);
//  }
//protected:
//  HybridPDESolver hybridPDESolver;
//
//  TransportPDESolver transportPDESolver;
//
//  CirculantEmbedding2D circEmb;
//
//  static double distX(double x, double d) { return abs(0.5 - x) / (3 * d); };
//
//  static double distY(double y, double d) { return 2 * abs(1.5 * d - y) / d; }
//
//  static double distTwoPoints(double x, double y, double d) {
//    return min(sqrt(pow(0.2 - x, 2) + pow(0.1 - y, 2)) / d,
//               sqrt(pow(0.9 - x, 2) + pow(0.1 - y, 2)) / d);
//  }
//
//  SampleData protocol(const SampleID &id, const OptionalVector &control) override {
//
//    double d = 1.0 / 16.0;
//    bool smooth = false;
//    bool twoPoint = false;
//    double endTime = 0.7;
//
//    Config::Get("smooth", smooth);
//    Config::Get("twoPoint", twoPoint);
//    Config::Get<double>("EndTime", endTime);
//
//    auto [stateSample, hybridSolution] = subHybridProtocol(id, control);
//
//    const auto transportAdjointProblem =
//        TransportPDEProblemBuilder()
//            .WithFlux([&hybridSolution](const MeshIndex &level, const Cell &c,
//                                        const Point &x) -> VectorField {
//              return hybridSolution.EvaluateCellFlux(c);
//            })
//            .WithFaceNormalFlux([&hybridSolution](const MeshIndex &level, const Cell &c, int face,
//                                                  const VectorField &N, const Point &x) {
//              return hybridSolution.EvaluateNormalFlux(c, face);
//            })
//            .WithName([&id]() { return "OCStochasticTransport.Adjoint" + id.IdString(); })
//            .WithHasExactSolution(false)
//            .WithSolution([&stateSample, d, smooth, twoPoint](double t, const Cell &C,
//                                                              const Point &x) -> double {
//              auto stateValue = evaluate::value(x, stateSample->vectors[0], C);
//              if (twoPoint and (distTwoPoints(x[0], x[1], d) > 1)) return stateValue;
//              if (twoPoint) return stateValue - (1 + cos(Pi * distTwoPoints(x[0], x[1], d)));
//              if (distX(x[0], d) > 1) return stateValue;
//              if (distY(x[1], d) > 1) return stateValue;
//              if (smooth)
//                return stateValue - (1 + cos(Pi * distX(x[0], d))) * (1 + cos(Pi * distY(x[1], d)));
//              else return stateValue - 1.0;
//            })
//            .WithTimeDomain({0.0, endTime})
//            .WithCalculateFwdProblem(false)
//            .WithMeshes(meshes)
//            .BuildShared(); // clang-format on
//
//    const auto adjointSolution = transportPDESolver.Run(transportAdjointProblem, id.GetMeshIndex());
//
//    return {stateSample, adjointSolution.vector};
//  }
//
//  std::pair<OptionalSampleSolution, HybridSolution>
//  subHybridProtocol(const SampleID &id, const OptionalVector &control) {
//    double d = 1.0 / 16.0;
//    bool smooth = false;
//    bool twoPoint = false;
//    double endTime = 0.7;
//
//    Config::Get("smooth", smooth);
//    Config::Get("twoPoint", twoPoint);
//    Config::Get<double>("EndTime", endTime);
//
//    auto y = circEmb.DrawSample(id, meshes);
//
//    const auto mixedProblem =
//        EllipticPDEProblemBuilder::Laplace2D()
//            .WithPermeability([&y](const Point &x, const cell &c) -> Tensor {
//              return y[floor(x[0] * y.rows())][floor(x[1] * y.cols())] * One;
//                                  })
//                                  .WithName([]() { return "MixedStochasticLaplace"; })
//                                  .WithHasExactSolution([]() { return false; })
//            .WithMeshes(meshes)
//                                  .BuildShared();
//
//    auto hybridSolution = hybridPDESolver.Run(mixedProblem);
//
//    const auto transportStateProblem =
//        TransportPDEProblemBuilder()
//            .WithFlux([&hybridSolution](const MeshIndex &level, const Cell &c,
//                                        const Point &x) -> VectorField {
//              return hybridSolution.EvaluateCellFlux(c);
//            })
//            .WithFaceNormalFlux([&hybridSolution](const MeshIndex &level, const Cell &c, int face,
//                                                  const VectorField &N, const Point &x) {
//              return hybridSolution.EvaluateNormalFlux(c, face);
//            })
//            .WithName([&id]() { return "OCStochasticTransport.State" + id.IdString(); })
//            .WithHasExactSolution(true)
//            .WithSolution(
//                [&control, d, smooth, twoPoint](double t, const Cell &C, const Point &x) -> double {
//                  if (t == 0)
//                    if (control == std::nullopt) return 0.0;
//                    else { return evaluate::value(x, control.value(), C); }
//                  else {
//                    if (twoPoint and (distTwoPoints(x[0], x[1], d) > 1)) return 0.0;
//                    if (twoPoint) return (1 + cos(Pi * distTwoPoints(x[0], x[1], d)));
//                    if (distX(x[0], d) > 1) return 0.0;
//                    if (distY(x[1], d) > 1) return 0.0;
//                    if (smooth)
//                      return (1 + cos(Pi * distX(x[0], d))) * (1 + cos(Pi * distY(x[1], d)));
//                    else return 1.0;
//              }
//            })
//            .WithTimeDomain({0.0, endTime})
//            .WithCalculateFwdProblem(true)
//            .WithMeshes(meshes)
//            .BuildShared();
//
//    auto stateSolution = transportPDESolver.Run(transportStateProblem, id.GetMeshIndex());
//
//    SampleData stateSample(stateSolution, id);
//
//    stateSample.solution.values.at("L2") = std::pow(stateSolution.values["Error"], 2) / 2;
//
//    return {stateSample, hybridSolution};
//  }
//
//  SampleData subProtocol(const SampleID &id, const OptionalVector &control) override {
//    return {subHybridProtocol(id, control).first, std::nullopt};
//  }
//
//  std::string Name() const override { return "OCStochasticTransport"; }
//};


MultiXFEM *CreateMXFEM(const MultiXFEMConfig &conf);

std::unique_ptr<MultiXFEM> CreateUniqueMXFEM(const MultiXFEMConfig &conf);

std::shared_ptr<MultiXFEM> CreateSharedMXFEM(const MultiXFEMConfig &conf);


#endif // MPP_OPTIMALCONTROLSAMPLING_H

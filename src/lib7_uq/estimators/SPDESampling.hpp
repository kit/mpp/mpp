#ifndef MPP_SPDESAMPLING_H
#define MPP_SPDESAMPLING_H

#include <format>
#include "DGDiscretization.hpp"
#include "EllipticPDESolver.hpp"
#include "LagrangeDiscretization.hpp"
#include "MultiXFEM.hpp"
#include "Random.hpp"
#include "ScalarElement.hpp" // Todo remove again
#include "Transfers.hpp"

template<int SDim>
class SPDESampling : public MultiXFEM {
public:
  SPDESampling(std::string domainName, MultiXFEMConfig conf) :
      MultiXFEM(std::move(domainName), std::move(conf)), //
      pdeSolver(PDESolverConfig()                        //
                    .WithPreconditioner("Multigrid")     //
                    .WithLinearSolver("CG")              //
                    .WithModel("Lagrange")               //
                    .WithDegree(1)) {
    init();
  }

  SPDESampling(std::shared_ptr<Meshes> meshes, MultiXFEMConfig conf) :
      MultiXFEM(meshes, std::move(conf)),            //
      pdeSolver(PDESolverConfig()                    //
                    .WithPreconditioner("Multigrid") //
                    .WithLinearSolver("CG")          //
                    .WithModel("Lagrange")           //
                    .WithDegree(1)) {
    init();
  }

  SampleData DrawSample(const SampleID &id);

protected:
  void init();

  Matrix whiteNoiseFactor(const Vector &xi);

  void setDirichlet(Vector &xi, std::shared_ptr<EllipticPDEProblem> problem);

  SampleData protocol(const SampleID &id, const OptionalVector &opt) override;

  EllipticPDESolver pdeSolver;

  double varScaling{};

  double kappa{};

  double nu{};

  int zeta{};

  double ell = 0.3;

  bool plotting = false;

  bool logMemory = false;

  // changing state to capture coarse sample
  std::shared_ptr<Vector> samplePtr = nullptr;
};

class SPDESamplingOnInterval : public SPDESampling<1> {
public:
  explicit SPDESamplingOnInterval(const MultiXFEMConfig &conf) : SPDESampling("Interval", conf) {}

  SPDESamplingOnInterval(std::shared_ptr<Meshes> meshes, const MultiXFEMConfig &conf) :
      SPDESampling(meshes, conf) {
    if (meshes->Name() != "Interval") {
      throw std::runtime_error("SPDESamplingOnInterval: Meshes must be of type Interval");
    }
  }

  std::string Name() const override { return "SPDESamplingOnInterval"; };
};

class SPDESamplingOnSquare : public SPDESampling<2> {
public:
  explicit SPDESamplingOnSquare(const MultiXFEMConfig &conf) : SPDESampling("Square", conf) {}

  SPDESamplingOnSquare(std::shared_ptr<Meshes> meshes, const MultiXFEMConfig &conf) :
      SPDESampling(meshes, conf) {
    if (meshes->Name() != "Square") {
      throw std::runtime_error("SPDESamplingOnSquare: Meshes must be of type Square");
    }
  }

  std::string Name() const override { return "SPDESamplingOnSquare"; };
};

class SPDESamplingOnHexahedron : public SPDESampling<3> {
public:
  explicit SPDESamplingOnHexahedron(const MultiXFEMConfig &conf) :
      SPDESampling("Hexahedron", conf) {}

  SPDESamplingOnHexahedron(std::shared_ptr<Meshes> meshes, const MultiXFEMConfig &conf) :
      SPDESampling(meshes, conf) {
    if (meshes->Name() != "Hexahedron") {
      throw std::runtime_error("SPDESamplingOnHexahedron: Meshes must be of type Hexahedron");
    }
  }

  std::string Name() const override { return "SPDESamplingOnHexahedron"; };
};

template<int SDim>
void SPDESampling<SDim>::init() {
  Config::Get("SPDELogMemory", logMemory);
  Config::Get("SPDEPlotting", plotting);
  Config::Get("ell", ell);

  // for zeta = 1
  if constexpr (SDim == 1)
    nu = 1.5; // zeta=1 // 3.5->zeta=2 // 5.5->zeta=3 // 7.5->zeta=4 // 9.5->zeta=5
  if constexpr (SDim == 2)
    nu = 1; // zeta=1 // 3->zeta=2   // 5->zeta=3   // 7->zeta=4   // 9->zeta=5
  if constexpr (SDim == 3)
    nu = 0.5; // zeta=1 // 2.5->zeta=2 // 4.5->zeta=3 // 6.5->zeta=4 // 8.5->zeta=5

  Config::Get("nu", nu);

  // Todo double check this formula
  double cnu = std::pow(4.0 * std::numbers::pi_v<double>, SDim / 2.0);
  cnu *= std::tgamma(nu + SDim / 2.0) / std::tgamma(nu);

  double exponent = nu / 2.0 + SDim / 4.0;
  if (std::fabs(exponent - std::round(exponent)) > 1e-3) {
    Exit("Fractional exponents not allowed")
  } else {
    zeta = std::round(exponent);
  }
  kappa = std::sqrt(2.0 * nu) / ell;
  varScaling = std::sqrt(cnu) * std::pow(kappa, nu);
}

template<int SDim>
SampleData SPDESampling<SDim>::protocol(const SampleID &id, const OptionalVector &opt) {
  auto disc = std::make_shared<const LagrangeDiscretization>(*meshes, 1);
  Vector sample(0.0, disc, id.GetMeshIndex());
  Matrix wnf = whiteNoiseFactor(sample);
  Vector rhs(0.0, sample);

  if (logMemory) MemoryLogger::LogMemory();

  // clang-format off
  const double ks = std::pow(kappa, 2);
  for (int i = 0; i < (int)pow(2, SDim); ++i) {
    Vector xi(disc, id.GetMeshIndex(), DIST_TYPE::NORMAL);
    for (int exponentCount = 0; exponentCount < zeta; ++exponentCount) {
      const auto problem = EllipticPDEProblemBuilder().WithMeshes(meshes)
        .WithLoad([&rhs](const Point &x, const cell &c) -> double {
          return ScalarElement(rhs, *c).Value(x, rhs);
        })
        .WithDirichletBoundary([i](const Point &x) {
          if constexpr (SDim == 1) {
            if (i == 0) return (x[0] == 0.0 || x[0] == 1.0);
          } else if constexpr (SDim == 2) {
            if (i == 0) return (x[0] == 0.0 || x[0] == 1.0) || (x[1] == 0.0 || x[1] == 1.0);
            if (i == 1) return (x[0] == 0.0 || x[0] == 1.0);
            if (i == 2) return (x[1] == 0.0 || x[1] == 1.0);
          } else if constexpr (SDim == 3) {
            if (i == 0) return (x[0] == 0.0 || x[0] == 1.0) || (x[1] == 0.0 || x[1] == 1.0) || (x[2] == 0.0 || x[2] == 1.0);
            if (i == 1) return (x[0] == 0.0 || x[0] == 1.0) || (x[1] == 0.0 || x[1] == 1.0);
            if (i == 2) return (x[0] == 0.0 || x[0] == 1.0) || (x[2] == 0.0 || x[2] == 1.0);
            if (i == 3) return (x[1] == 0.0 || x[1] == 1.0) || (x[2] == 0.0 || x[2] == 1.0);
            if (i == 4) return (x[0] == 0.0 || x[0] == 1.0);
            if (i == 5) return (x[1] == 0.0 || x[1] == 1.0);
            if (i == 6) return (x[2] == 0.0 || x[2] == 1.0);
          }
          return false;
        })
        .WithNeumannBoundary([i](const Point &x) -> bool {
          if constexpr (SDim == 1) {
            if (i == 1) return (x[0] == 0.0 || x[0] == 1.0);
          } else if constexpr (SDim == 2) {
            if (i == 1) return (x[1] == 0.0 || x[1] == 1.0);
            if (i == 2) return (x[0] == 0.0 || x[0] == 1.0);
            if (i == 3) return (x[0] == 0.0 || x[0] == 1.0) || (x[1] == 0.0 || x[1] == 1.0);
          } else if constexpr (SDim == 3) {
            if (i == 1) return (x[2] == 0.0 || x[2] == 1.0);
            if (i == 2) return (x[1] == 0.0 || x[1] == 1.0);
            if (i == 3) return (x[0] == 0.0 || x[0] == 1.0);
            if (i == 4) return (x[1] == 0.0 || x[1] == 1.0) || (x[2] == 0.0 || x[2] == 1.0);
            if (i == 5) return (x[0] == 0.0 || x[0] == 1.0) || (x[2] == 0.0 || x[2] == 1.0);
            if (i == 6) return (x[0] == 0.0 || x[0] == 1.0) || (x[1] == 0.0 || x[1] == 1.0);
            if (i == 7) return (x[0] == 0.0 || x[0] == 1.0) || (x[1] == 0.0 || x[1] == 1.0) || (x[2] == 0.0 || x[2] == 1.0);
          }
          return false;
        })
        .WithSolution([](const Point &x) -> double { return 0.0; })
        .WithFlux([](const Point &x) -> VectorField { return zero; })
        .WithReactionTerm([&ks](const Point &x) -> double { return ks; })
        .WithName([&id, i]() { return std::format("SPDE{}.{}", id.IdString(), i); })
        .BuildShared(); // clang-format on

      if (exponentCount == 0) {
        setDirichlet(xi, problem);
        rhs = wnf * xi;
        rhs *= varScaling;
      } else {
        setDirichlet(rhs, problem);
      }

      if (verbose > 1) {
        mout << "xi:";
        pout << xi << std::endl;
        mout << "whiteNoiseFactor:";
        pout << wnf << std::endl;
        mout << "rhs:";
        pout << rhs << std::endl;
      }

      auto tempSolution = pdeSolver.Run(problem, id.GetMeshIndex());
      rhs = tempSolution.vector;

      if (logMemory) MemoryLogger::LogMemory();
    }
    sample.MakeAdditive();
    rhs.MakeAdditive();
    sample += rhs;
  }

  sample *= std::pow(2.0, -SDim / 2.0);
  sample.Accumulate();
  if (plotting) mpp::PlotData("GRF", "GRF" + id.IdString(), sample);

  return SampleData("grf", id, sample);
}

template<int SDim>
void SPDESampling<SDim>::setDirichlet(Vector &xi, std::shared_ptr<EllipticPDEProblem> problem) {
  xi.ClearDirichletFlags();
  if (problem->OnDirichlet()) {
    for (cell c = xi.cells(); c != xi.cells_end(); ++c) {
      RowBndValues u_c(xi, *c);
      if (!u_c.onBnd()) continue;
      for (int face = 0; face < c.Faces(); ++face) {
        for (const Point &np : xi.GetNodalPointsOnFace(*c, face)) {
          if (problem->OnDirichlet()(np)) {
            xi(np, 0) = problem->Solution(np);
            xi.D(np, 0) = true;
          }
        }
      }
    }
  }
}

template<int SDim>
Matrix SPDESampling<SDim>::whiteNoiseFactor(const Vector &sample) {
  Matrix wnf(sample);
  for (cell c = wnf.cells(); c != wnf.cells_end(); ++c) {
    const ScalarElement elem = ScalarElement(wnf, *c);
    RowEntries A_c(wnf, elem);
    for (int q = 0; q < elem.nQ(); ++q) {
      double w = elem.QWeight(q);
      for (unsigned int i = 0; i < elem.size(); ++i) {
        double trial = elem.Value(q, i);
        for (unsigned int j = 0; j < elem.size(); ++j) {
          double test = elem.Value(q, j);
          A_c(i, i) += w * trial * test; // mass lumping
        }
      }
    }
  }
  for (double &entry : wnf.GetData()) {
    if (entry != 0.0) entry = 1.0 / std::sqrt(entry);
  }
  return wnf;
}

template<int SDim>
SampleData SPDESampling<SDim>::DrawSample(const SampleID &id) {
  if (id.coarse) {
    return SampleData("grf", id, *samplePtr);
  }
  samplePtr = std::make_shared<Vector>(protocol(id, std::nullopt).template Get<Vector>("grf"));
  return SampleData("grf", id, *samplePtr);
}


#endif // MPP_SPDESAMPLING_H

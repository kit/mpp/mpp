#ifndef MULTILEVELESTIMATOR_HPP
#define MULTILEVELESTIMATOR_HPP

#include "ContinuationData.hpp"
#include "EstimatorMap.hpp"

class MultiLevelEstimator : public Estimator {
private:
  int plotting = 1;

  int verbose = 1;

  int attemptCtr = 0;

  double epsilon = 0.0;

  EstimatorMap estMap;

  ContinuationData contData;

  MLEstimatorConfig mlEstmConf;

  bool levelLoop(const OptionalVector &input);

  bool expectedToBeFeasible();

  bool tryUpdateEpsilonOrSampleNumbers();

  bool appendLevelIfNeeded();

  bool updateSamplesIfNeeded();

  bool timeBudgetIsExhausted();

  bool targetEpsilonReached();

  void updateData(std::map<int, std::pair<double, double>> timeMap);
public:
  explicit MultiLevelEstimator(const MLEstimatorConfig &mlEstmConf) :
      epsilon(mlEstmConf.epsilon), mlEstmConf(mlEstmConf), estMap(mlEstmConf) {
    Config::Get("MLEstimatorPlotting", plotting);
    Config::Get("MLEstimatorVerbose", verbose);
  }

  ~MultiLevelEstimator() override { estMap.clear(); }

  const SampleData WithdrawEstimate() override;

  void UpdateEpsilon(double _epsilon) override { epsilon = _epsilon; };

  void MLE(const OptionalVector &input);

  void AMLE(const OptionalVector &input);

  void BMLE(const OptionalVector &input);

  Estimator &Method(const OptionalVector &input) override;

  void UpdateSampleAmount(std::vector<int> levels, std::vector<int> requestedSamples) override;

  double Cost() const { return estMap.TimeCost(); }

  const EstimatorMap &GetEstimatorMap() const { return estMap; }

  static std::string Name() { return "MultiLevelEstimator"; }

  void PrintInfo() const;

  void PrintExponent() const;

  void PrintMultilevel() const;
};

#endif // MULTILEVELESTIMATOR_HPP

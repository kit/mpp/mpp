#ifndef MPP_OPTIMIZATIONDATA_H
#define MPP_OPTIMIZATIONDATA_H

#include <vector>
#include "ContinuationData.hpp"
#include "EstimatorMap.hpp"
#include "WelfordAggregate.hpp"

struct OptimizationData {

  EstimatorType estimatorType;

  // E(j(z)) := E(0.5 * |u-d|_L2^2) + 0.5 * lambda * |z|_L2^2)
  std::vector<double> targetFunctionalL2Norms{};

  std::vector<double> gradientL2Norms{}, descentL2Norms{}, controlL2Norms{};

  std::vector<double> L2ErrorForward{};

  std::vector<double> stepSizes{};

  std::vector<double> timeCosts{}, memoryCost{};

  std::vector<double> rmse{}, mse{}, sam{}, num{};

  std::vector<double> alpha{}, beta{}, gammaCT{}, gammaMem{};

  OptimizationData(const SGDConfig &conf) : estimatorType(conf.estimatorType) {};

  void PushBack(const SampleData &data, const Vector &control, const Vector &descent,
                double stepSize) {
    gradientL2Norms.push_back(norms::L2(data.Get<Vector>("E(backward)")));
    targetFunctionalL2Norms.push_back(data.Get<double>("E(j)"));
    L2ErrorForward.push_back(data.qoIs.at("E(L2Error(forward))"));
    timeCosts.push_back(data.qoIs.at("TotalTimeCostSum"));
    memoryCost.push_back(data.qoIs.at("TotalMemoCostSum"));
    controlL2Norms.push_back(norms::L2(control));
    descentL2Norms.push_back(norms::L2(descent));
    stepSizes.push_back(stepSize);

    if (estimatorType == EstimatorType::MULTILEVEL) {
      rmse.push_back(data.qoIs.at("rmse"));
      mse.push_back(data.qoIs.at("mse"));
      sam.push_back(data.qoIs.at("sam"));
      num.push_back(data.qoIs.at("num"));

      alpha.push_back(data.qoIs.at("alpha"));
      beta.push_back(data.qoIs.at("beta"));
      gammaCT.push_back(data.qoIs.at("gammaCT"));
      gammaMem.push_back(data.qoIs.at("gammaMem"));
    }
  }

  bool IsEmpty() const { return gradientL2Norms.empty(); }

  int Size() const { return gradientL2Norms.size(); }

  void PrintInfo() const;
};


#endif // MPP_OPTIMIZATIONDATA_H

#ifndef MPP_OPTIMIZER_H
#define MPP_OPTIMIZER_H

#include "ConfigStructsUQ.hpp"
#include "OptimizationData.hpp"
#include "Sample.hpp"

class Optimizer {
protected:
  SGDConfig sgdConf;

  OptimizationData optData;

  virtual double stepSize() const = 0;

  friend class StochasticGradientDescent;

  Vector admissibleMapping(const Vector &control) const;

  Vector prolongateOnNewEstimate(const Vector &newEstimate, const Vector &control) const;
public:
  Optimizer(const SGDConfig &sgdConf) : sgdConf(sgdConf), optData(sgdConf) {}

  virtual Vector InitStep(const SampleData &data) {
    Vector descent(data.Get<Vector>("E(backward)")); // this is the first gradient
    descent *= -stepSize();                          // this is the first descent
    Vector newControl = admissibleMapping(descent);  // this is the first admissible control
    optData.PushBack(data, newControl, descent, stepSize());
    return newControl;
  }

  virtual Vector Step(const Vector &control, const SampleData &data) {
    Vector descent(data.Get<Vector>("E(backward)"));               // this is the adjoint
    Vector newControl = prolongateOnNewEstimate(descent, control); // old projected control
    descent += sgdConf.targetCost * newControl;                    // this is the gradient
    descent *= stepSize();                                         // this is the descent
    newControl -= descent;                                         // this is the new control
    newControl = admissibleMapping(newControl);                    // new admissible control
    optData.PushBack(data, newControl, descent, stepSize());
    return newControl;
  }
};

class ConstantStepSizeOptimizer : public Optimizer {
protected:
  double stepSize() const override { return sgdConf.defaultStepSize; }
public:
  ConstantStepSizeOptimizer(const SGDConfig &sgdConf) : Optimizer(sgdConf) {}
};

class DecreasingStepSizeOptimizer : public Optimizer {
protected:
  double stepSize() const override { return sgdConf.defaultStepSize / sqrt(optData.Size() + 1); }
public:
  DecreasingStepSizeOptimizer(const SGDConfig &sgdConf) : Optimizer(sgdConf) {}
};

class AdamOptimizer : public Optimizer {
protected:
  std::unique_ptr<Vector> firstMoment;

  std::unique_ptr<Vector> secRawMoment;

  double stepSize() const override {
    return sgdConf.defaultStepSize * sqrt((1 - std::pow(sgdConf.betaAdam2, optData.Size() + 1)))
           / (1 - std::pow(sgdConf.betaAdam, optData.Size() + 1));
  }
public:
  AdamOptimizer(const SGDConfig &sgdConf) : Optimizer(sgdConf) {}

  Vector InitStep(const SampleData &data) override {
    if (firstMoment != nullptr || secRawMoment != nullptr)
      Exit("Moments should be nullptr in InitStep");

    const Vector &backEstm = data.Get<Vector>("E(backward)");
    firstMoment = std::make_unique<Vector>(0.0, backEstm);
    secRawMoment = std::make_unique<Vector>(0.0, backEstm);
    Vector epsVector(sgdConf.epsilonAdam, backEstm);

    // ADAM step
    (*firstMoment) += (1 - sgdConf.betaAdam) * backEstm;
    (*secRawMoment) += (1 - sgdConf.betaAdam2) * HadamardProductVector(backEstm, backEstm);
    Vector descent = ComponentDivide((*firstMoment), ComponentSqrt((*secRawMoment)) + epsVector);
    Vector newControl(descent);
    newControl *= -stepSize();
    newControl = admissibleMapping(newControl);
    optData.PushBack(data, newControl, descent, stepSize());
    return newControl;
  }

  Vector Step(const Vector &control, const SampleData &data) override {
    Vector backEstm(data.Get<Vector>("E(backward)"));               // this is the adjoint
    Vector newControl = prolongateOnNewEstimate(backEstm, control); // old projected control
    Vector epsVector(sgdConf.epsilonAdam, backEstm);

    if (backEstm.Level() != control.Level()) {
      firstMoment = std::make_unique<Vector>(prolongateOnNewEstimate(backEstm, *firstMoment));
      secRawMoment = std::make_unique<Vector>(prolongateOnNewEstimate(backEstm, *secRawMoment));
    }

    (*firstMoment) *= sgdConf.betaAdam;
    (*secRawMoment) *= sgdConf.betaAdam2;
    (*firstMoment) += (1 - sgdConf.betaAdam) * backEstm;
    (*secRawMoment) += (1 - sgdConf.betaAdam2) * HadamardProductVector(backEstm, backEstm);
    Vector descent = ComponentDivide((*firstMoment), ComponentSqrt((*secRawMoment)) + epsVector);
    newControl -= stepSize() * descent;
    newControl = admissibleMapping(newControl);
    optData.PushBack(data, newControl, descent, stepSize());
    return newControl;
  }
};

class CustomStepSizeOptimizer : public Optimizer {
  // Todo: Schrittweite direkt übergeben oder aus dem
  //  Kontext des Models auslesen (Geiersbach)
  //  if (breakpointCriteria == "gradientNorm") {
  //      if (targetCost > 10e-5) {
  //        double targetCost; // used to be a class member, only used here & in MXFEM
  //        double theta; // used to be a class member, only used here
  //        double nu; // used to be a class member, only used here
  //        return theta / (optStep + 1.0 + nu);
  //        vout(4) << "stepsize (µ conv): " << stepSize << endl;
  //    }
  //  }
};


#endif // MPP_OPTIMIZER_H

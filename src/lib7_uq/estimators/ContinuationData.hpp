#ifndef MPP_CONTINUATIONDATA_H
#define MPP_CONTINUATIONDATA_H

#include "EstimatorMap.hpp"

struct ContinuationData;

struct ContinuationExponents {
  double deltaCT = 0.0;
  double deltaMem = 0.0;

  void ComputeDelta(const ContinuationData &contData);

  friend std::ostream &operator<<(std::ostream &s, const ContinuationExponents &exponents) { //
    return s << "deltaCT=" << std::setprecision(3) << exponents.deltaCT                      //
             << " deltaMem=" << std::setprecision(3) << exponents.deltaMem;                  //
  }
};

struct ContinuationData {
  std::vector<double> epsilons{};
  std::vector<double> timeCosts{};
  std::vector<double> memoCosts{};
  std::vector<double> relTimeCosts{};
  std::vector<double> relMemoCosts{};

  std::vector<std::vector<int>> commSplits{};
  std::vector<std::vector<int>> samplesPerLevel{};
//  std::map<std::string, std::vector<double>> values{}; // todo
  std::map<std::string, std::vector<double>> num{};
  std::map<std::string, std::vector<double>> sam{};
  std::map<std::string, std::vector<double>> mse{};
  std::map<std::string, std::vector<double>> rmse{};

  void PushBack(const EstimatorMap &estMap, double epsilon) {
    timeCosts.push_back(estMap.TimeCost());
    memoCosts.push_back(MemoryLogger::TotalMemoryUsage());
    relTimeCosts.push_back(timeCosts.back() / estMap.GetMLEstimatorConfig().timeBudget);
    relMemoCosts.push_back(memoCosts.back() / estMap.GetMLEstimatorConfig().memoBudget);
    samplesPerLevel.push_back(computeDeltaOfSamplesPerLevel(estMap));
    commSplits.push_back(estMap.GetCommSplits());
    epsilons.push_back(epsilon);

    for (const auto &key : estMap.GetErrors().GetErrorKeys()) {
      num[key].push_back(estMap.GetErrors().num.at(key));
      sam[key].push_back(estMap.GetErrors().sam.at(key));
      mse[key].push_back(estMap.GetErrors().mse.at(key));
      rmse[key].push_back(estMap.GetErrors().rmse.at(key));
    }
  }

  bool IsEmpty() const { return timeCosts.empty(); }

  size_t Size() const { return timeCosts.size(); }

  void PrintInfo() const;

private:
  std::vector<int> computeDeltaOfSamplesPerLevel(const EstimatorMap &estMap);

  void computeDelta();
};


#endif // MPP_CONTINUATIONDATA_H

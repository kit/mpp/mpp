#include "ConfigStructsUQ.hpp"

MultiXFEMConfig MultiXFEMConfig::WithPDESolverConfigs(const std::vector<PDESolverConfig> &conf) {
  pdeSolverConfigs = conf;
  return *this;
}

MultiXFEMConfig MultiXFEMConfig::WithCommSplit(int communicationSplit) {
  commSplit = communicationSplit;
  return *this;
}

MultiXFEMConfig MultiXFEMConfig::WithCoarseLevel(int level) {
  cLevel = level;
  return *this;
}

MultiXFEMConfig MultiXFEMConfig::WithFineLevel(int level) {
  fLevel = level;
  return *this;
}

MultiXFEMConfig MultiXFEMConfig::WithProblem(std::string problem) {
  name = std::move(problem);
  return *this;
}

SLEstimatorConfig &SLEstimatorConfig::WithInitLevel(int level) {
  for (int i = 0; i < mxFEMConfig.pdeSolverConfigs.size(); i++) {
    mxFEMConfig.pdeSolverConfigs[i] = mxFEMConfig.pdeSolverConfigs[i].WithLevel(level);
  }
  initLevel = level;
  return *this;
}

SLEstimatorConfig &SLEstimatorConfig::WithEpsilon(double targetEpsilon) {
  epsilon = targetEpsilon;
  return *this;
}

SLEstimatorConfig &SLEstimatorConfig::WithInitSamples(int samples) {
  initSamples = samples;
  return *this;
}

SLEstimatorConfig &SLEstimatorConfig::WithOnlyFine(bool onlyComputeOnFine) {
  onlyFine = onlyComputeOnFine;
  return *this;
}

SLEstimatorConfig &SLEstimatorConfig::WithParallel(bool parallelEstimator) {
  parallel = parallelEstimator;
  return *this;
}

SLEstimatorConfig &SLEstimatorConfig::WithMultiSampleFEMConfig(const MultiXFEMConfig &conf) {
  mxFEMConfig = conf;
  return *this;
}

SLEstimatorConfig &SLEstimatorConfig::WithDelayTotalUpdate(bool delayTotUpdate) {
  delayTotalUpdate = delayTotUpdate;
  return *this;
}

SLEstimatorConfig &SLEstimatorConfig::WithDelayParallelUpdate(bool delayParaUpdate) {
  delayParallelUpdate = delayParaUpdate;
  return *this;
}

SLEstimatorConfig &SLEstimatorConfig::WithMaxSamplesByMem(int maxSamples) {
  maxSamplesByMem = maxSamples;
  return *this;
}

SLEstimatorConfig &SLEstimatorConfig::WithObjective(const std::string &obj) {
  objective = obj;
  return *this;
}

std::ostream &operator<<(std::ostream &s, const SLEstimatorConfig &conf) {
  return s << DOUT(conf.initLevel) << DOUT(conf.initSamples) << DOUT(conf.epsilon)
           << DOUT(conf.parallel) << DOUT(conf.onlyFine) << DOUT(conf.delayTotalUpdate)
           << DOUT(conf.delayParallelUpdate);
}

double MLEstimatorConfig::convertBudgetStringToDouble(const std::string &str) {
  size_t i = 0;
  while (i < str.size() && (std::isdigit(str[i]) || str[i] == '.'))
    ++i;

  std::string numericPart = str.substr(0, i);
  std::string unitPart = str.substr(i);
  double number = std::stod(numericPart);

  if (unitPart == "TB" || unitPart == "tb") {
    number *= 1048576.0;
  } else if (unitPart == "GB" || unitPart == "gb") {
    number *= 1024.0;
  } else if (unitPart == "MB" || unitPart == "mb") {
  } else if (unitPart == "KB" || unitPart == "kb") {
    number /= 1024.0;
  }

  return number;
}

MLEstimatorConfig &MLEstimatorConfig::WithSLEstimatorConfig(const SLEstimatorConfig &conf) {
  slEstmConf = conf;
  return *this;
}

MLEstimatorConfig &MLEstimatorConfig::WithInitSamples(const std::vector<int> &samples) {
  initSamples = samples;
  return *this;
}

MLEstimatorConfig &MLEstimatorConfig::WithInitLevel(const std::vector<int> &levels) {
  initLevels = levels;
  return *this;
}

MLEstimatorConfig &MLEstimatorConfig::WithTimeBudget(double timeBudgetAsDouble) {
  timeBudget = timeBudgetAsDouble;
  return *this;
}

MLEstimatorConfig &MLEstimatorConfig::WithMemoryBudget(double memoryBudgetAsDouble) {
  memoBudget = memoryBudgetAsDouble;
  return *this;
}

MLEstimatorConfig &MLEstimatorConfig::WithTheta(double biasVarianceTradeOff) {
  theta = biasVarianceTradeOff;
  return *this;
}

MLEstimatorConfig &MLEstimatorConfig::WithEpsilon(double targetEpsilon) {
  epsilon = targetEpsilon;
  return *this;
}

MLEstimatorConfig &MLEstimatorConfig::WithEta(double errorReduction) {
  eta = errorReduction;
  return *this;
}

MLEstimatorConfig &MLEstimatorConfig::WithObjective(const std::string &obj) {
  objective = obj;
  dataKey = objectiveStr2DataStr();
  return *this;
}

std::string MLEstimatorConfig::objectiveStr2DataStr() {
  std::string returnStr = objective;
  if (auto pos = returnStr.find("E("); pos != std::string::npos) {
    returnStr.replace(pos, 2, "\u2206");
    returnStr.pop_back();
  }
  return returnStr;
}

void MLEstimatorConfig::PrintInfo() const {
  mout.PrintInfo("MLEstimatorConfig", 1,                                 //
                 InfoEntry("Eta", eta),                                  //
                 InfoEntry("Processes", PPM->Size(0)),                   //
                 InfoEntry("Theta", theta),                              //
                 InfoEntry("Epsilon", epsilon),                          //
                 InfoEntry("Quantity", quantity),                        //
                 InfoEntry("Objective", objective),                      //
                 InfoEntry("DataString", dataKey),                       //
                 InfoEntry("TimeBudget", timeBudget),                    //
                 InfoEntry("MemoBudget", memoBudget),                    //
                 InfoEntry("InitLevels", initLevels),                    //
                 InfoEntry("InitSamples", initSamples),                  //
                 InfoEntry("CPUTimeBudget", timeBudget * PPM->Size(0))); //
}

std::ostream &operator<<(std::ostream &s, const MLEstimatorConfig &conf) {
  return s << DOUT(conf.eta) << DOUT(conf.theta) << DOUT(conf.epsilon) << DOUT(conf.timeBudget)
           << DOUT(conf.memoBudget) << DOUT(conf.quantity) << DOUT(conf.objective)
           << "conf.initLevels:" << vec2str(conf.initLevels)
           << " conf.initSamples:" << vec2str(conf.initSamples);
}

SGDConfig SGDConfig::WithMLEstimatorConfig(const MLEstimatorConfig &conf) {
  mlEstmConf = conf;
  return *this;
}

SGDConfig SGDConfig::WithBetaAdam(double beta) {
  betaAdam = beta;
  return *this;
}

SGDConfig SGDConfig::WithBetaAdam2(double beta) {
  betaAdam2 = beta;
  return *this;
}

SGDConfig SGDConfig::SGDConfig::WithDefaultStepsize(double defaultValue) {
  defaultStepSize = defaultValue;
  return *this;
}

SGDConfig SGDConfig::WithEpsilonAdam(double epsilon) {
  mout << "Warning! This should never be overwritten" << endl;
  return *this;
}

SGDConfig SGDConfig::WithGradientEstimatorType(EstimatorType estimatorType) {
  estimatorType = estimatorType;
  return *this;
}

SGDConfig SGDConfig::WithMaxGradientSteps(int gradientSteps) {
  maxGradientSteps = gradientSteps;
  return *this;
}

SGDConfig SGDConfig::WithOptimizerType(OptimizerType optType) {
  optimizerType = optType;
  return *this;
}

SGDConfig SGDConfig::WithAveragingFraction(double fraction) {
  polyakAveragingFraction = fraction;
  return *this;
}

SGDConfig SGDConfig::WithAdmissibleBounds(double lowerBound, double upperBound) {
  WithAdmissibleLowerBound(lowerBound);
  WithAdmissibleUpperBound(upperBound);
  return *this;
}

SGDConfig SGDConfig::WithAdmissibleLowerBound(double lowerBound) {
  admissibleLowerBound = lowerBound;
  return *this;
}

SGDConfig SGDConfig::WithAdmissibleUpperBound(double upperBound) {
  admissibleUpperBound = upperBound;
  return *this;
}

SGDConfig SGDConfig::WithEpsilonTarget(double epsilon) {
  epsilonTarget = epsilon;
  return *this;
}

SGDConfig SGDConfig::WithTargetCost(double cost) {
  targetCost = cost;
  return *this;
}

SGDConfig SGDConfig::WithBatchSize(int batchSize) {
  // Todo check estimator type?
  mlEstmConf.slEstmConf.initSamples = batchSize;
  return *this;
}

SGDConfig SGDConfig::WithBatchSize(std::map<int, int> batchSize) {
  for (const auto &[level, size] : batchSize) {
    mlEstmConf.initLevels.push_back(level);
    mlEstmConf.initSamples.push_back(size);
  }
  return *this;
}

SGDConfig SGDConfig::WithEstimatorType(EstimatorType estmType) {
  estimatorType = estmType;
  return *this;
}

#ifndef MPP_SPARSEGRIDSAMPLING_H
#define MPP_SPARSEGRIDSAMPLING_H

#include "KLExpansionGenerator.hpp"

// Todo place new implementation for sparse grid problems
//  here


// Todo: Rewrite Problemclass:

class StochasticSparseGridLaplace2D : public MultiXFEM {
public:
  explicit StochasticSparseGridLaplace2D(const MultiXFEMConfig &conf) :
      MultiXFEM("Square", conf), pdeSolver(conf.pdeSolverConfigs[0]) {}
protected:
  EllipticPDESolver pdeSolver;
  KLExpansionGenerator KLGenerator;

  SampleData protocol(const SampleID &id, const OptionalVector &opt) override {
    auto y = KLGenerator.DrawSample(id, meshes);
    auto w = KLGenerator.GetSampleWeight();

    auto pdeProblem = EllipticPDEProblemBuilder()
                          .WithMeshes(meshes)
                          .WithPermeability([&y](const Point &x, const cell &c) -> Tensor {
                            return exp(y[floor(x[0] * sqrt(y.size())) * floor(sqrt(y.size()) - 1)
                                         + floor(x[1] * sqrt(y.size()))])
                                   * One;
                          })
                          .WithSolution([](const Point &x) -> double { return -x[1]; })
                          .WithFlux([](const Point &x) -> VectorField { return {0.0, -1.0}; })
                          .WithLoad([](const Point &x, const cell &c) -> double { return 0.0; })
                          .WithName([&]() { return Name() + id.IdString(); })
                          .BuildShared();

    auto ellipticSolution = pdeSolver.Run(pdeProblem, id.GetMeshIndex());
    SampleData sampleData(Name(), id, ellipticSolution);
    sampleData.weight = w;
    return sampleData;
  }

  std::string Name() const override { return "StochasticSparseGridLaplace2D"; };
};

// class SparseGridLaplace2D : public IStochasticEllipticProblem {
// private:
//   double factor = exp(-1.0 / 8.0);
//
//   double a = -sqrt(12) / 2.0;
//
//   double b = sqrt(12) / 2.0;
//
//   double a_min = 0.01;
//
//   SparseGridGenerator sparseGridGen;
//
//   void drawSample(const SampleID &id) override { sparseGridGen.DrawSample(id); }
// public:
//   SparseGridLaplace2D() :
//       IProblem("Square"), sparseGridGen(SparseGridGenerator(GridDomain({{a, a, a, a}, {b, b, b,
//       b}}))) {}
//
//   void InitGenerator(int init) override { sparseGridGen.InitGenerator(meshes, init); }
//
//   // Todo why is weight not working
//   //  double SumOfWeights() override {
//   //    return sparseGridGen.SumOfWeights();
//   //  }
//   //
//   //  double SampleWeight() const override {
//   //    return sparseGridGen.SampleWeight(currentID);
//   //  }
//
//   int NumOfSamples() const override { return sparseGridGen.GetNumPoints(); }
//
//   double Load(const Point &x, const cell &c) const override { return 0.0; }
//
//   double Solution(const Point &x) const override { return -x[1]; }
//
//   VectorField Flux(const Point &x) const override { return {0.0, -1.0}; }
//
//   Tensor Permeability(const Point &x) const override {
//     return (a_min
//             + exp(factor
//                   * (cos(2 * M_PI * x[0]) * this->sparseGridGen.EvalSample()[0]
//                      + sin(2 * M_PI * x[0]) * this->sparseGridGen.EvalSample()[1]
//                      + cos(2 * M_PI * x[1]) * this->sparseGridGen.EvalSample()[2]
//                      + sin(2 * M_PI * x[1]) * this->sparseGridGen.EvalSample()[3])))
//            * One;
//   }
//
//   std::string Name() const override { return "SparseGridLaplace2D"; }
// };
//
// class StochasticSGRiemann1D : public IStochasticTransportProblem {
//   SparseGridGenerator sparseGridGen;
//
//   double thickness = 1.0 / 8.0;
//
//   double centerStart = 1.5 * thickness;
// public:
//   StochasticSGRiemann1D() :
//       IProblem("Interval"), sparseGridGen(SparseGridGenerator(GridDomain({{0}, {1}}))) {}
//
//   void drawSample(const SampleID &id) override { sparseGridGen.DrawSample(id); }
//
//   double center(double t) const { return centerStart - t; }
//
//   void InitGenerator(int init) override { sparseGridGen.InitGenerator(meshes, init); }
//
//   int NumOfSamples() const override { return sparseGridGen.GetNumPoints(); }
//
//   double Solution(double t, const Point &x) const override {
//     if (abs(center(t) - x[0]) < (thickness * this->sparseGridGen.EvalSample()[0]) / 2.0)
//       return 1.0 / (thickness * this->sparseGridGen.EvalSample()[0]);
//     else return 0.0;
//   }
//
//   static VectorField TransportFlux(const Point &) { return {1.0, 0.0}; }
//
//   VectorField CellFlux(const cell &c, const Point &x) const override { return TransportFlux(x); }
//
//   double FaceNormalFlux(const cell &c, int face, const VectorField &N,
//                         const Point &x) const override {
//     return TransportFlux(x) * N;
//   }
// };
//
// class StochasticSGGaussHat1D : public IStochasticTransportProblem {
// private:
//   SparseGridGenerator sparseGridGen;
//
//   double sigma2;
//
//   double factor;
// public:
//   explicit StochasticSGGaussHat1D(double sigma = 0.05) :
//       IProblem("Line"), sigma2(sigma * sigma),
//       sparseGridGen(SparseGridGenerator(GridDomain({{0}, {1}}))) {
//     T = 0.5;
//     factor = 1.0 / sqrt((2.0 * std::numbers::pi * sigma2));
//   }
//
//   void InitGenerator(int init) override { sparseGridGen.InitGenerator(meshes, init); }
//
//   void drawSample(const SampleID &id) override { sparseGridGen.DrawSample(id); }
//
//   int NumOfSamples() const override { return sparseGridGen.GetNumPoints(); }
//
//   static Point mu(double t) { return {0.25, 0.0}; }
//
//   double Solution(double t, const Point &x) const override {
//     VectorField diff = (x - mu(t) * this->sparseGridGen.EvalSample()[0]);
//     double exponent = -diff * diff / (2 * sigma2);
//     return factor * exp(exponent);
//   }
//
//   static VectorField TransportFlux(const Point &x) { return {1.0, 0.0}; }
//
//   VectorField CellFlux(const cell &c, const Point &x) const override { return TransportFlux(x); }
//
//   double FaceNormalFlux(const cell &c, int face, const VectorField &N,
//                         const Point &x) const override {
//     return TransportFlux(x) * N;
//   }
// };
//
// class StochasticSGline1D : public IStochasticTransportProblem {
// private:
//   SparseGridGenerator sparseGridGen;
// public:
//   StochasticSGline1D() :
//       IProblem("Line"), sparseGridGen(SparseGridGenerator(GridDomain({{0}, {1}}))) {
//     T = 0.5;
//   }
//
//   void InitGenerator(int init) override { sparseGridGen.InitGenerator(meshes, init); }
//
//   void drawSample(const SampleID &id) override { sparseGridGen.DrawSample(id); }
//
//   int NumOfSamples() const override { return sparseGridGen.GetNumPoints(); }
//
//   double Solution(double t, const Point &x) const override { return x[0] + x[1]; }
//
//   static VectorField TransportFlux(const Point &x) { return {1.0, 0.0}; }
//
//   VectorField CellFlux(const cell &c, const Point &x) const override { return TransportFlux(x); }
//
//   double FaceNormalFlux(const cell &c, int face, const VectorField &N,
//                         const Point &x) const override {
//     return TransportFlux(x) * N;
//   }
// };
//
// class StochasticSGGaussHat2D : public IStochasticTransportProblem {
// private:
//   SparseGridGenerator sparseGridGen;
//
//   double factor;
//
//   Tensor sigma;
// public:
//   StochasticSGGaussHat2D() :
//       IProblem("UnitSquare"), sigma(0.005, 0.0, 0.0, 0.005),
//       sparseGridGen(SparseGridGenerator(GridDomain({{0, 0}, {0.1, 0.1}}))) {
//     factor = 1.0 / sqrt(pow(2.0 * std::numbers::pi, 2) * sigma.det());
//     T = 2.0;
//   }
//
//   void InitGenerator(int init) override { sparseGridGen.InitGenerator(meshes, init); }
//
//   void drawSample(const SampleID &id) override { sparseGridGen.DrawSample(id); }
//
//   int NumOfSamples() const override { return sparseGridGen.GetNumPoints(); }
//
//   static Point mu(double t) { return {0.25 + t, 0.25 + t}; }
//
//   double Solution(double t, const Point &x) const override {
//     VectorField diff =
//         (x - mu(t)
//          + Point(this->sparseGridGen.EvalSample()[0], this->sparseGridGen.EvalSample()[1]));
//     VectorField temp = Invert(sigma) * diff;
//     double exponent = -0.5 * diff * temp;
//     return factor * exp(exponent);
//   }
//
//   static VectorField TransportFlux(const Point &x) {
//     using std::numbers::pi;
//     return {cos(pi * x[0]) / 2, sin(pi * x[1]) / 2};
//   }
//
//   VectorField CellFlux(const cell &c, const Point &x) const override { return TransportFlux(x); }
//
//   double FaceNormalFlux(const cell &c, int face, const VectorField &N,
//                         const Point &x) const override {
//     return TransportFlux(x) * N;
//   }
// };
//
// class StochasticSGDWave2D : public IStochasticTransportProblem {
// private:
//   SparseGridGenerator sparseGridGen;
// public:
//   StochasticSGDWave2D() :
//       IProblem("UnitSquare"), sparseGridGen(SparseGridGenerator(GridDomain({{0, 0}, {1, 1}}))) {}
//
//   void InitGenerator(int init) override { sparseGridGen.InitGenerator(meshes, init); }
//
//   void drawSample(const SampleID &id) override { sparseGridGen.DrawSample(id); }
//
//   int NumOfSamples() const override { return sparseGridGen.GetNumPoints(); }
//
//   double Solution(double t, const Point &x) const override {
//     if (x[0] == 0) return 1 + this->sparseGridGen.EvalSample()[0];
//     else if (x[0] == 1) return 1 + this->sparseGridGen.EvalSample()[1];
//     else return 0;
//   }
//
//   static VectorField TransportFlux(const Point &x) {
//     if (x[0] < 0.5) return {1.0, 0.0};
//     else return {-1.0, 0.0};
//   }
//
//   VectorField CellFlux(const cell &c, const Point &x) const override { return TransportFlux(x); }
//
//   double FaceNormalFlux(const cell &c, int face, const VectorField &N,
//                         const Point &x) const override {
//     return TransportFlux(x) * N;
//   }
// };
//
// class StochasticSGCWave2D : public IStochasticTransportProblem {
// private:
//   SparseGridGenerator sparseGridGen;
// public:
//   StochasticSGCWave2D() :
//       IProblem("UnitSquare"), sparseGridGen(SparseGridGenerator(GridDomain({{0, 0}, {1, 1}}))) {}
//
//   void InitGenerator(int init) override { sparseGridGen.InitGenerator(meshes, init); }
//
//   void drawSample(const SampleID &id) override { sparseGridGen.DrawSample(id); }
//
//   int NumOfSamples() const override { return sparseGridGen.GetNumPoints(); }
//
//   double Solution(double t, const Point &x) const override {
//     return exp(-x[0]) * this->sparseGridGen.EvalSample()[0]
//            + exp(-x[1]) * this->sparseGridGen.EvalSample()[1];
//   }
//
//   static VectorField TransportFlux(const Point &x) { return {x[0] * 0.5, x[1] * 0.5}; }
//
//   VectorField CellFlux(const cell &c, const Point &x) const override { return TransportFlux(x); }
//
//   double FaceNormalFlux(const cell &c, int face, const VectorField &N,
//                         const Point &x) const override {
//     return TransportFlux(x) * N;
//   }
// };

#endif // MPP_SPARSEGRIDSAMPLING_H

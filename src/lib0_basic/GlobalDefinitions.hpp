#ifndef GLOBALDEFINITIONS
#define GLOBALDEFINITIONS

#include <numbers>
#include <string>

template<typename T>
[[deprecated("Use std::min instead")]]
inline const T &min(const T &a, const T &b) noexcept {
  return std::min(a, b);
};

template<typename T>
[[deprecated("Use std::max instead")]]
inline const T &max(const T &a, const T &b) noexcept {
  return std::max(a, b);
};

template<typename T>
  requires requires(T t) { std::to_string(t); }
[[deprecated("Use std::to_string instead")]]
inline std::string to_string(T t) noexcept {
  return std::to_string(t);
};

using Scalar [[deprecated("Use double instead")]] = double;

[[deprecated("Use -1 instead")]]
inline constexpr double iUnit(-1);

[[deprecated("Use std::numbers::pi instead")]]
inline constexpr double Pi = std::numbers::pi;


#endif // GLOBALDEFINITIONS

#include "Assertion.hpp"

#include <algorithm>
#include <format>
#include <iostream>
#include <string_view>

#include "Config.hpp"
#include "Parallel.hpp"

std::vector<std::string> Assertion::previousWarnings = {};

void Assertion::assertion(bool assertion, const std::string_view message,
                          const std::string_view file, size_t line) {
  if (assertion) return;
  if (ParallelProgrammingModel::IsInitialized()) {
    std::cerr << std::format("\n\033[1;35mAssert: {}\033[0m\n"
                             "        on proc {}\n"
                             "        in {}:{}\n"
                             "        on line {}\n\n",
                             message, PPM->Proc(0), file, line, line);
  } else {
    std::cerr << std::format("\n\033[1;35mAssert: {}\033[0m\n"
                             "        in {}:{}\n"
                             "        on line {}\n\n",
                             message, file, line, line);
  }
  std::cerr << std::flush;
  assert(0);
}

void Assertion::assertion(bool assertion, const std::string_view message, int n,
                          const std::string_view file, size_t line) {
  if (assertion) return;
  if (ParallelProgrammingModel::IsInitialized()) {
    std::cerr << std::format("\n\033[1;35mAssert: {}{}\033[0m\n"
                             "        on proc {}\n"
                             "        in {}:{}\n"
                             "        on line {}\n\n",
                             message, n, PPM->Proc(0), file, line, line);
  } else {
    std::cerr << std::format("\n\033[1;35mAssert: {}{}\033[0m\n"
                             "        in {}:{}\n"
                             "        on line {}\n\n",
                             message, n, file, line, line);
  }
  std::cerr << std::flush;
  assert(0);
}

void Assertion::exit(const std::string_view message, const std::string_view file, size_t line) {
  if (ParallelProgrammingModel::IsInitialized()) {
    std::cerr << std::format("\n\033[1;31mError: {}\033[0m\n"
                             "        on proc {}\n"
                             "        in {}:{}\n"
                             "        on line {}\n\n",
                             message, PPM->Proc(0), file, line, line);
  } else {
    std::cerr << std::format("\n\033[1;31mError: {}\033[0m\n"
                             "        in {}:{}\n"
                             "        on line {}\n\n",
                             message, file, line, line);
  }
  std::cerr << std::flush;
}

void Assertion::warning(const std::string_view message, const std::string_view file, size_t line) {
  if (!PPM->Master(0)) return;
  if (std::find(previousWarnings.begin(), previousWarnings.end(), file) != previousWarnings.end())
    return;
  previousWarnings.emplace_back(file);

  std::cout << std::format("\n\033[1;33mWarning: {}\033[0m\n"
                           "        in {}:{}\n"
                           "        on line {}\n\n",
                           message, file, line, line);
  std::cout << std::flush;
  mout.AddWarningMsg(std::format("Warning: {}\n"
                                 "        in {}:{}\n"
                                 "        on line {}\n\n",
                                 message, file, line, line));
}

void Assertion::warningOnProc(int proc, const std::string_view message, const std::string_view file,
                              size_t line) {
  if (PPM->Proc(0) != proc) return;

  if (std::find(previousWarnings.begin(), previousWarnings.end(), message)
      != previousWarnings.end())
    return;
  previousWarnings.emplace_back(message);

  if (ParallelProgrammingModel::IsInitialized()) {
    std::cerr << std::format("\n\033[1;33mWarning: {}\033[0m\n"
                             "        on proc {}\n"
                             "        in {}:{}\n"
                             "        on line {}\n\n",
                             message, proc, file, line, line)
              << std::flush;
    mout.AddWarningMsg(std::format("Warning: {}\n"
                                   "        on proc {}\n"
                                   "        in {}:{}\n"
                                   "        on line {}\n\n",
                                   message, proc, file, line, line));
  } else {
    std::cerr << std::format("\n\033[1;33mWarning: {}\033[0m\n"
                             "        in {}:{}\n"
                             "        on line {}\n\n",
                             message, file, line, line)
              << std::flush;
    mout.AddWarningMsg(std::format("Warning: {}\n"
                                   "        in {}:{}\n"
                                   "        on line {}\n\n",
                                   message, file, line, line));
  }
}

void Assertion::warningOnProc(const std::string_view message, const std::string_view file,
                              size_t line) {
  if (std::find(previousWarnings.begin(), previousWarnings.end(), message)
      != previousWarnings.end())
    return;
  previousWarnings.emplace_back(message);

  if (ParallelProgrammingModel::IsInitialized()) {
    std::cerr << std::format("\n\033[1;33mWarning: {}\033[0m\n"
                             "        on proc {}\n"
                             "        in {}:{}\n"
                             "        on line {}\n\n",
                             message, PPM->Proc(0), file, line, line)
              << std::flush;
    mout.AddWarningMsg(std::format("Warning: {}\n"
                                   "        on proc {}\n"
                                   "        in {}:{}\n"
                                   "        on line {}\n\n",
                                   message, PPM->Proc(0), file, line, line));
  } else {
    std::cerr << std::format("\n\033[1;33mWarning: {}\033[0m\n"
                             "        in {}:{}\n"
                             "        on line {}\n\n",
                             message, file, line, line)
              << std::flush;
    mout.AddWarningMsg(std::format("Warning: {}\n"
                                   "        in {}:{}\n"
                                   "        on line {}\n\n",
                                   message, file, line, line));
  }
}

void Assertion::synchronizeErrors(bool failed, const std::string_view message,
                                  const std::string_view file, size_t line) {
  std::string msg = "";
  if (failed) {
    exit(message, file, line);
    if (ParallelProgrammingModel::IsInitialized()) {
      msg = std::format("Error: {}\n"
                        "        on proc {}\n"
                        "        in {}:{}\n"
                        "        on line {}\n\n",
                        message, PPM->Proc(0), file, line, line);
    } else {
      msg = std::format("Error: {}\n"
                        "        in {}:{}\n"
                        "        on line {}\n\n",
                        message, file, line, line);
    }
  }
  failed = PPM->Or(failed, 0);
  if (failed) {
    ExchangeBuffer exBuffer(0);
    exBuffer.Send(0) << msg;
    exBuffer.Communicate();
    if (PPM->Proc(0) == 0) {
      for (short p = 0; p < PPM->Size(0); ++p) {
        std::string msg_p;
        exBuffer.Receive(p) >> msg_p;
        mout.AddErrorMsg(msg_p);
      }
    }
    std::exit(1);
  }
}

void Assertion::Error(const std::string_view message, const std::string_view file, size_t line) {
  if (!PPM->Master(0)) return;
  exit(message, file, line);

  if (ParallelProgrammingModel::IsInitialized()) {
    mout.AddErrorMsg(std::format("Error: {}\n"
                                 "        on all procs\n"
                                 "        in {}:{}\n"
                                 "        on line {}\n\n",
                                 message, file, line, line));
  } else {
    mout.AddErrorMsg(std::format("Error: {}\n"
                                 "        in {}:{}\n"
                                 "        on line {}\n\n",
                                 message, file, line, line));
  }
}

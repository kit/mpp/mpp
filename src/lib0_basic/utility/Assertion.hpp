#ifndef _DEBUG_H_
#define _DEBUG_H_

#include <cassert>
#include <exception>
#include <format>
#include <string>
#include <string_view>
#include <typeinfo>
#include <vector>
#include "mpi.h"

template<class C>
inline const char *Type(const C &c) {
  return typeid(c).name();
}

template<class C>
inline const char *Type(const C *c) {
  return typeid(*c).name();
}

class Assertion {
  static std::vector<std::string> previousWarnings;
public:
  Assertion() = default;

  static void assertion(bool assertion, const std::string_view message, const std::string_view file,
                        size_t line);

  static void assertion(bool assertion, const std::string_view message, int n,
                        const std::string_view file, size_t line);

  static void exit(const std::string_view message, const std::string_view file, size_t line);

  static void warning(const std::string_view message, const std::string_view file, size_t line);

  static void warningOnProc(int proc, const std::string_view message, const std::string_view file,
                            size_t line);

  static void warningOnProc(const std::string_view message, const std::string_view file,
                            size_t line);

  static void synchronizeErrors(bool failed, const std::string_view message = {},
                                const std::string_view file = {}, size_t line = 0);

  static void Error(const std::string_view message, const std::string_view file, size_t line);
};

struct MppException : public std::exception {
  std::string error;
  const char *file;
  int line;
  std::string errorMsg{};

  MppException(std::string error, const char *file, int line) :
      error(error), file(file), line(line) {
    errorMsg = std::format("{}\n"
                           "        in {}:{}\n"
                           "        on line {}\n\n",
                           error, file, line, line);
  }

  const char *what() const throw() { return errorMsg.c_str(); }
};

struct NotImplementedException : public std::exception {};

/// Use this for errors definitely occurring on all procs
#define ERROR(s)                                                                                   \
  {                                                                                                \
    Assertion::Error(s, __FILE__, __LINE__);                                                       \
    std::exit(1);                                                                                  \
  }

#define THROW(s) throw MppException(s, __FILE__, __LINE__);

#define TRY try

#define CATCH(s)                                                                                   \
  catch (const MppException &exception) {                                                          \
    Assertion::synchronizeErrors(true, exception.error, exception.file, exception.line);           \
  }                                                                                                \
  catch (...) {                                                                                    \
    Assertion::synchronizeErrors(true, s, __FILE__, __LINE__);                                     \
  }                                                                                                \
  Assertion::synchronizeErrors(false);

#define Exit(s)                                                                                    \
  {                                                                                                \
    Assertion::exit(s, __FILE__, __LINE__);                                                        \
    exit(1);                                                                                       \
  }

#define Warning(s)                                                                                 \
  { Assertion::warning(s, __FILE__, __LINE__); }

#define VerboseWarning(s, i)                                                                       \
  if (verbose >= i) Warning(s)

#define WarningOnProc(s)                                                                           \
  { Assertion::warningOnProc(s, __FILE__, __LINE__); }

#define WarningOnMaster(s)                                                                         \
  { Assertion::warningOnProc(0, s, __FILE__, __LINE__); }

#define LOCATION "file = " << __FILE__ << " line = " << __LINE__

#define Assert(s) Assertion::assertion(s, __STRING(s), __FILE__, __LINE__)

#define CheckMem(a)                                                                                \
  if (!(a)) Assertion::assertion(false, "out of memory", __FILE__, __LINE__);

#endif // of #ifndef _DEBUG_H_

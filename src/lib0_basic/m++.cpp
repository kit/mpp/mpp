#include "m++.hpp"

#include <cstdlib>

#include "Assertion.hpp"
#include "Config.hpp"
#include "Parallel.hpp"

// TODO REMOVE THIS. This is not code which should be called at the start of the program
#include "lib4_fem/plot/VtuPlot.hpp"

namespace Mpp {

void initialize(int *argc, char **argv) {
  static bool initialized = false;
  if (!initialized) {
    initialized = true;
    ParallelProgrammingModel::Initialize(*argc, argv);
    Config::Initialize(argc, argv);

    // Todo clean this stuff up, rather use filesystem; commented code below found in dead main
    bool clear = false;
    Config::Get("ClearData", clear);
    if (clear && PPM->Master()) { int cleared = system("exec rm -rf data/vt*/*"); }
    //  bool clear = false;
    //  Config::Get("ClearData", clear);
    //  if (clear && PPM->Master()) {
    //    // The artifacts are stored in the following structure:
    //    // data/
    //    // └── vtu/
    //    //     └── foo.vtu
    //    // └── vtk/
    //    //     └── bar.vtk
    //    // We remove everything within the directories in data/, but keep the
    //    // directory structure itself intact. This is necessary since the plotting
    //    // code does not verify the existence of data/vtu for example, and silently
    //    // fails without it.
    //    for (const auto &entry :
    //        std::filesystem::directory_iterator(Config::GetDataPath())) {
    //      if (entry.is_directory()) {
    //        for (const auto &entry_to_remove :
    //            std::filesystem::directory_iterator(entry)) {
    //          std::filesystem::remove_all(entry_to_remove.path());
    //        }
    //      }
    //    }
    //  }

    // TODO REMOVE THIS. This is not code which should be called at the start of the program
    Config::Get("ParallelPlotting", globalParallelPlotting);
    Config::Get("Compression", globalCompression);
  } else {
    THROW("Mpp already initialized")
  }
}

} // namespace Mpp

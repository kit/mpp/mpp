#ifndef _MPLUSPLUS_HPP_
#define _MPLUSPLUS_HPP_

#include "Assertion.hpp"
#include "Config.hpp"

namespace Mpp {

void initialize(int *argc, char **argv);

} // namespace Mpp

#endif // of #ifndef _MPLUSPLUS_HPP_

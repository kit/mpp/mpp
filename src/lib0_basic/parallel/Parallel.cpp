#include "Parallel.hpp"
#include "ExchangeBuffer.hpp"
#include "MemoryLogger.hpp"

#include <cmath>
#include <format>
#include <iostream>
#include <memory>


#ifdef BUILD_IA

#include "IACInterval.hpp"

#endif

std::unique_ptr<ParallelProgrammingModel> ParallelProgrammingModel::ppm = nullptr;

ParallelProgrammingModel::ParallelProgrammingModel(int *argc, char ***argv) {
  MPI_Init(argc, argv);
#ifdef BUILD_IA
  MPI_Op_create((MPI_User_function *)mpi_sum_iainterval, 1, &MPI_SUM_IAINTERVAL);
  MPI_Op_create((MPI_User_function *)mpi_sum_iacinterval, 1, &MPI_SUM_IACINTERVAL);
  MPI_Op_create((MPI_User_function *)mpi_hull_iainterval, 1, &MPI_HULL_IAINTERVAL);
  MPI_Op_create((MPI_User_function *)mpi_intersect_iainterval, 1, &MPI_INTERSECT_IAINTERVAL);
#endif
  // Register the world communicator
  std::shared_ptr<WorldCommunicator> worldComm = std::make_shared<WorldCommunicator>();
  splittedComms.push_back(worldComm);
}

ParallelProgrammingModel::~ParallelProgrammingModel() {
  if (MasterLogging::isInitialized()) mout.CommunicateWarnings();
#ifdef BUILD_IA
  MPI_Op_free(&MPI_SUM_IAINTERVAL);
  MPI_Op_free(&MPI_SUM_IACINTERVAL);
  MPI_Op_free(&MPI_HULL_IAINTERVAL);
  MPI_Op_free(&MPI_INTERSECT_IAINTERVAL);
#endif
  splittedComms.clear();
  dualComms.clear();
  MPI_Finalize();
}

void ParallelProgrammingModel::Initialize(int argc, char **argv) {
  if (IsInitialized()) THROW("PPM already initialized!")
  ppm = std::make_unique<ParallelProgrammingModel>(&argc, &argv);
  PPM->FillCommunicators();
}

bool ParallelProgrammingModel::IsInitialized() { return ppm != nullptr; }

void ParallelProgrammingModel::FillCommunicators() {
  // Initialize splittedComms
  int s = 0;
  while (PPM->Size(s) != 1) {
    splittedComms.push_back(splittedComms.back()->SplitLeftRight(s));
    s++;
  }
  // Initialize dualComms
  for (int d = 0; d < MaxCommSplit(); d++) {
    dualComms.push_back(SetUpDualComms(d));
  }
}

ParallelProgrammingModel *ParallelProgrammingModel::Instance() {
  if (!ppm) THROW("PPM not initialized!")
  return ppm.get();
}

void ParallelProgrammingModel::Close() { ppm = nullptr; }

void ParallelProgrammingModel::Abort() { MPI_Abort(MPI_COMM_NULL, 1); }

CommunicatorPtr ParallelProgrammingModel::getComm(int commSplit, int distLevel) const {
  if (distLevel == -1) { // access to splittedComms
    if (commSplit == -1 || commSplit > MaxCommSplit()) return splittedComms.back();
    return splittedComms.at(commSplit);
  } else { // access to dualComms
    if (distLevel < 0 || distLevel >= MaxCommSplit()) { Exit("distLevel out of bounds") }
    return dualComms.at(distLevel);
  }
}

Communicator *ParallelProgrammingModel::Copy(int commSplit) const {
  if (commSplit == -1) return splittedComms.back()->Copy();
  if (commSplit > MaxCommSplit()) {
    THROW(std::format("Communicator split {} does not exist", commSplit));
  }
  return splittedComms.at(commSplit)->Copy();
}

int ParallelProgrammingModel::Proc(int commSplit, int distLevel) const {
  return getComm(commSplit, distLevel)->Proc();
}

int ParallelProgrammingModel::Size(int commSplit, int distLevel) const {
  return getComm(commSplit, distLevel)->Size();
}

bool ParallelProgrammingModel::Master(int commSplit, int distLevel) const {
  return getComm(commSplit, distLevel)->Master();
}

int ParallelProgrammingModel::Color(int commSplit, int distLevel) const {
  return getComm(commSplit, distLevel)->Color();
}

bool ParallelProgrammingModel::IsParallel(int commSplit, int distLevel) const {
  return Size(commSplit, distLevel) > 1;
}

void ParallelProgrammingModel::PrintInfo(int commSplit, int distLevel) const {
  return getComm(commSplit, distLevel)->PrintInfo();
}

void ParallelProgrammingModel::Communicate(ExchangeBuffer &exBuffer, int commSplit, int distLevel) {
  getComm(commSplit, distLevel)->Communicate(exBuffer);
}

bool ParallelProgrammingModel::And(bool b, int commSplit, int distLevel) const {
  return getComm(commSplit, distLevel)->And(b);
}

bool ParallelProgrammingModel::Or(bool b, int commSplit, int distLevel) const {
  return getComm(commSplit, distLevel)->Or(b);
}

int ParallelProgrammingModel::CommsSize() const { return splittedComms.size(); }

int ParallelProgrammingModel::MaxCommSplit() const {
  if (CommsSize() != 0) return CommsSize() - 1;
  else return 0;
}

void ParallelProgrammingModel::Barrier(int commSplit, int distLevel) {
  getComm(commSplit, distLevel)->Barrier();
}

#ifdef BUILD_IA
void ParallelProgrammingModel::Hull(IAInterval *a, size_t n, int commSplit) {
  getComm(commSplit, -1)->Hull(a, n);
}

void ParallelProgrammingModel::Intersect(IAInterval *a, size_t n, int commSplit) {
  getComm(commSplit, -1)->Intersect(a, n);
}

IAInterval ParallelProgrammingModel::Hull(IAInterval a) {
  Hull(&a, 1);
  return a;
}

IAInterval ParallelProgrammingModel::Intersect(IAInterval a) {
  Intersect(&a, 1);
  return a;
}

#endif


// Credits to: https://gist.github.com/thirdwing/da4621eb163a886a03c5
#include <fstream>
#include <unistd.h>

void ParallelProgrammingModel::Synchronize(const char *c) {
  std::cout.flush();
  PPM->Barrier(0);
  if (c == 0) return;
  std::cout << getComm(-1, -1)->Proc() << "|";
  PPM->Barrier(0);
  std::cout.flush();
  if (PPM->Master(0)) std::cout << c << endl;
  std::cout.flush();
  PPM->Barrier(0);
  std::cout.flush();
}

/*
 * =========================
 * Deprecated
 * =========================
 */

bool ParallelProgrammingModel::Boolean(bool b) { return getComm(0, -1)->Or(b); }

void ParallelProgrammingModel::BroadcastDouble(double d) {
  Assert(Master(0));
  BCastOnCommSplit(d, 0);
}

double ParallelProgrammingModel::TotalMemoryUsage(bool perPro) {
  return MemoryLogger::TotalMemoryUsage(perPro);
}

double ParallelProgrammingModel::BroadcastDouble() {
  Assert(!Master(0));
  double a;
  BCastOnCommSplit(a, 0);
  return a;
}

void ParallelProgrammingModel::BroadcastInt(int i) {
  Assert(Master(0));
  BCastOnCommSplit(i, 0);
}

int ParallelProgrammingModel::BroadcastInt() {
  Assert(!Master(0));
  int i;
  BCastOnCommSplit(i, 0);
  return i;
}

int ParallelProgrammingModel::NumberOfCommunicators(int commSplit) {
  return int(pow(2, commSplit));
}

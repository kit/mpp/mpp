#ifndef _PARALLEL_H_
#define _PARALLEL_H_

#include <memory>
#include "Assertion.hpp"
#include "Communicator.hpp"

class ParallelProgrammingModel {
private:
  static std::unique_ptr<ParallelProgrammingModel> ppm;

  friend class TestPPM;

  Communicators splittedComms;
  /*
   * Example for worldSize=4
   * s=0:   [0 1 2 3 4 5 6 7] (world)
   * color:  0
   *
   * s=1:   [0 1 2 3 | 4 5 6 7]
   * color:  0         1
   *
   * s=2:   [0 1 | 2 3 | 4 5 | 6 7]
   * color:  0     1     2    3
   *
   * s=3:   [0 | 1 | 2 | 3 | 4 | 5 | 6 | 7]
   * color:  0   1   2   3   4   5   6   7
   */

  Communicators dualComms;
  /*
   * Example for worldSize=8
   * d=1:   [0 2 | 1 3 | 4 6 | 5 7]
   * color:  0     1     4     5
   *
   * d=2:   [0 4 | 1 5 |2 6 | 3 7]
   * color:  0     1    2     3
   *
   */

  CommunicatorPtr getComm(int commSplit, int distLevel) const;
public:
  ParallelProgrammingModel(int *argc, char ***argv);

  static void Initialize(int argc, char **argv);

  static ParallelProgrammingModel *Instance();

  void FillCommunicators();

  static bool IsInitialized();

  ~ParallelProgrammingModel();

  static void Close();

  static void Abort();

  // Always the same return size
  static int NumberOfCommunicators(int commSplit = 0);

  int Proc(int commSplit = 0, int distLevel = -1) const;

  int Size(int commSplit = 0, int distLevel = -1) const;

  bool Master(int commSplit = 0, int distLevel = -1) const;

  int Color(int commSplit = 0, int distLevel = -1) const;

  bool IsParallel(int commSplit, int distLevel = -1) const;

  void PrintInfo(int commSplit = 0, int distLevel = -1) const;

  // TODO: Remove size issue such that the following holds (NOT true yet!!!):
  //   Note that size is only the size of the array (NOT multiplied with sizeof(T))!
  template<typename T>
  void Broadcast(T *data, size_t size, int commSplit = 0, int distLevel = -1, int q = 0) {
    size /= sizeof(T);
    getComm(commSplit, distLevel)->Broadcast(data, size, q);
  }

  template<typename T>
  void BCastOnCommSplit(T &t, int commSplit, int distLevel = -1) {
    getComm(commSplit, distLevel)->Broadcast(t);
  }

  template<typename T>
  void BCastOnCommSplit(const T &t, int commSplit, int distLevel = -1) {
    getComm(commSplit, distLevel)->Broadcast(t);
  }

  template<typename T>
  void Send(T *data, size_t size, int destination, int commSplit = 0, int distLevel = -1) const {
    getComm(commSplit, distLevel)->Send(data, size, destination);
  };

  Communicator *Copy(int commSplit = 0) const;

  template<typename T>
  void Broadcast(const T &t) {
    getComm(0, -1)->Broadcast(t);
  }

  template<typename T>
  void Receive(T *data, size_t size, int source, int commSplit = 0, int distLevel = -1) const {
    getComm(commSplit, distLevel)->Receive(data, size, source);
  };

  // Tested in Exchange Buffer
  void Communicate(ExchangeBuffer &exBuffer, int commSplit = 0, int distLevel = -1);

  template<typename T>
  void Allgather(T *sendData, size_t sizeSend, T *receiveData, int commSplit = 0) const {
    getComm(commSplit, -1)->Allgather(sendData, sizeSend, receiveData);
  };

  template<typename T>
  void Allgatherv(T *sendData, size_t sizeSend, T *receiveData, int *sizeReceive, int *displs,
                  int commSplit) const {
    getComm(commSplit, -1)->Allgatherv(sendData, sizeSend, receiveData, sizeReceive, displs);
  };

  template<typename T>
  void Sum(T *a, size_t n, int commSplit = 0, int distLevel = -1) const {
    getComm(commSplit, distLevel)->Sum(a, n);
  };

  template<typename T>
  T SumOnCommSplit(T a, int commSplit) const {
    Sum(&a, 1, commSplit);
    return a;
  }

  template<typename T>
  T SumOn(T a, int commSplit, int distLevel) const {
    Sum(&a, 1, commSplit, distLevel);
    return a;
  }

  template<typename T>
  void Sum(std::vector<T> &v) const {
    Sum(v.data(), std::size(v));
  }

  template<typename T>
  void SumOnCommSplit(std::vector<T> &v, int commSplit) const {
    size_t n = std::size(v);
    T *a = new T[n];
    for (int i = 0; i < n; ++i)
      a[i] = v[i];
    Sum(a, n, commSplit);
    for (int i = 0; i < n; ++i)
      v[i] = a[i];
    delete[] a;
  }

  template<typename T>
  T SumAcrossComm(T a, int commSplit) const {
    return (SumOnCommSplit(a, 0) / Size(commSplit));
  }

  template<typename T>
  T Min(T a, int commSplit = 0, int distLevel = -1) const {
    return getComm(commSplit, distLevel)->Min(a);
  }

  template<typename T>
  T Max(T a, int commSplit = 0, int distLevel = -1) const {
    return getComm(commSplit, distLevel)->Max(a);
  }

  template<typename T>
  T ExclusiveScanSum(T a, int commSplit = 0, int distLevel = -1) const {
    return getComm(commSplit, distLevel)->ExclusiveScanSum(a);
  }

  bool And(bool b, int commSplit = 0, int distLevel = -1) const;

  bool Or(bool b, int commSplit = 0, int distLevel = -1) const;

#ifdef BUILD_IA

  void Hull(IAInterval *a, size_t n, int commSplit = 0);

  void Intersect(IAInterval *a, size_t n, int commSplit = 0);

  IAInterval Hull(IAInterval a);

  IAInterval Intersect(IAInterval a);

#endif

  bool SplitCommunicatorsLeftRight();

  bool SplitCommunicatorsModulo();

  int CommsSize() const;

  int MaxCommSplit() const;

  void Barrier(int commSplit = 0, int distLevel = -1);

  void Synchronize(const char *c = 0);

  double TotalMemoryUsage(bool perPro = false);

  void BroadcastDouble(double d);

  double BroadcastDouble();

  void BroadcastInt(int i);

  int BroadcastInt();

  /*
   * Functions below should not be used. Instead the capitalized version of the function with
   * CommSplit should be used
   */

  template<typename T>
  void Broadcast(T &t) {
    getComm(0, -1)->Broadcast(t);
  }

  template<typename T>
  T Sum(T a) const {
    Sum(&a, 1);
    return a;
  }

  bool Boolean(bool b);

  [[deprecated("Use Proc instead!")]]
  int proc() const {
    return Proc();
  }

  [[deprecated("Use Size instead!")]]
  int size() const {
    return Size();
  };

  [[deprecated("Use Master instead!")]]
  bool master() const {
    return Master();
  };
};

#define PPM (ParallelProgrammingModel::Instance())

#endif // of #ifndef _PARALLEL_H_

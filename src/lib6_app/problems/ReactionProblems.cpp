#include "ReactionProblems.hpp"


double StartConcentration(const Point &x) {
  double thickness = 1.0 / 8.0;
  double centerStart = 1 - 1.5 * thickness;
  if ((abs(centerStart - x[1]) < thickness / 2) &&
    (abs(0.5 - x[0]) < 3 * thickness))
    return 1;
  else return 0.0;
}

ReactionPDEProblemBuilder ReactionPDEProblemBuilder::ExponentialReaction2D() {
  double diffusion = 0.01;
  double reaction = 5.0;

  Config::Get("Diffusion", diffusion);
  Config::Get("Reaction", reaction);

  return ReactionPDEProblemBuilder()
      .WithName([]() { return "ExponentialReaction2D"; })
      .WithMeshName("UnitSquare")
      .WithLoad([](const Point &x) { return 0.0; })
      .WithFlux([](const Point &x) { return zero; })
      .WithDiffusion([diffusion](const Point &x) { return diffusion * One; })
      .WithConvection([](const Point &x) -> VectorField { return {0.0, -1.0}; })
      .WithReaction([reaction](const Point &, double c) { return reaction * c; })
      .WithConcentration([](double t, const Point &x) { return StartConcentration(x); })
      .WithDerivativeReaction([reaction](const Point &x, double c) { return reaction; });
}

ReactionPDEProblemBuilder ReactionPDEProblemBuilder::ExponentialReactionSquare500() {
  return ExponentialReaction2D().WithMeshName("Square500").WithName([]() {
    return "ExponentialReactionSquare500";
  });
}

ReactionPDEProblemBuilder ReactionPDEProblemBuilder::LogisticReaction2D() {
  double r0 = 30.0;
  double r1 = 10.0;
  double diffusion = 0.01;

  Config::Get("Reaction0", r0);
  Config::Get("Reaction1", r1);
  Config::Get("Diffusion", diffusion);

  return ReactionPDEProblemBuilder()
      .WithName([]() { return "LogisticReaction2D"; })
      .WithMeshName("UnitSquare")
      .WithLoad([](const Point &x) { return 0.0; })
      .WithFlux([](const Point &x) { return zero; })
      .WithDiffusion([diffusion](const Point &x) { return diffusion * One; })
      .WithConvection([](const Point &x) -> VectorField { return {0.0, -1.0}; })
      .WithReaction([r0, r1](const Point &, double c) { return r0 * c - r1 * c * c; })
      .WithConcentration([](double t, const Point &x) { return StartConcentration(x); })
      .WithDerivativeReaction([r0, r1](const Point &x, double c) { return r0 - 2 * r1 * c; });
}

ReactionPDEProblemBuilder ReactionPDEProblemBuilder::LogisticReactionSquare500() {
  return LogisticReaction2D()
      .WithName([]() { return "LogisticReactionSquare500"; })
      .WithMeshName("Square500");
}

ReactionPDEProblemBuilder ReactionPDEProblemBuilder::ConvectionDiffusionCosHat2D() {
  using std::numbers::pi;
  double amplitude = 1.0;
  double frequency = 8.0;
  double diffusion = 0.01;

  Config::Get("Diffusion", diffusion);

  const auto CircleVectorField = [](const Point &x) -> VectorField {
    return 2 * pi * VectorField(0.5 - x[1], x[0] - 0.5, 0.0);
  };

  return ReactionPDEProblemBuilder()
      .WithName([]() { return "ConvectionDiffusionCosHat2D"; })
      .WithMeshName("UnitSquare")
      .WithLoad([](const Point &x) { return 0.0; })
      .WithFlux([](const Point &x) { return zero; })
      .WithDiffusion([diffusion](const Point &x) { return Zero; })
      .WithConvection(
          [CircleVectorField](const Point &x) -> VectorField { return CircleVectorField(x); })
      .WithReaction([](const Point &, double c) { return 0.0; })
      .WithConcentration([amplitude, frequency](double t, const Point &x) {
        Point midPoint = 0.25 * (Point(cos(2.0 * pi * t), sin(2.0 * pi * t))) + Point(0.5, 0.5);
        double r = dist(midPoint, x);
        if (r < (pi / 2.0) / frequency) return amplitude * pow(cos(frequency * r), 2.0);
        return 0.0;
      })
      .WithDerivativeReaction([](const Point &x, double c) { return 0.0; })
      .WithFaceConvection([CircleVectorField](const Cell &c, int face, const VectorField &N,
                                              const Point &x) { return CircleVectorField(x) * N; });
}

ReactionPDEProblemBuilder
ReactionPDEProblemBuilder::PollutionExponentialReaction2D(const HybridSolution solution,
                                                          std::shared_ptr<Meshes> meshes) {
  double convection = 1.0;
  Config::Get("Convection", convection);

  return ExponentialReactionSquare500()
      .WithName([]() { return "HybridExponentialReaction2D"; })
      .WithMeshes(meshes)
      .WithCellConvection([convection, solution](const Cell &c, const Point &x) {
        return convection * solution.EvaluateCellFlux(c);
      })
      .WithFaceConvection(
          [convection, solution](const Cell &c, int face, const VectorField &N, const Point &x) {
            return convection * solution.EvaluateNormalFlux(c, face);
          });
}

ReactionPDEProblemBuilder
ReactionPDEProblemBuilder::PollutionExponentialReactionSquare500(const HybridSolution solution,
                                                                 std::shared_ptr<Meshes> meshes) {
  double convection = 1.0;
  Config::Get("Convection", convection);

  return ExponentialReactionSquare500()
      .WithName([]() { return "PollutionExponentialReactionSquare500"; })
      .WithMeshes(meshes)
      .WithCellConvection([convection, solution](const Cell &c, const Point &x) {
        return convection * solution.EvaluateCellFlux(c);
      })
      .WithFaceConvection(
          [convection, solution](const Cell &c, int face, const VectorField &N, const Point &x) {
            return convection * solution.EvaluateNormalFlux(c, face);
          });
}

ReactionPDEProblemBuilder
ReactionPDEProblemBuilder::PollutionLogisticReaction2D(const HybridSolution solution,
                                                       std::shared_ptr<Meshes> meshes) {
  double convection = 1.0;
  Config::Get("Convection", convection);

  return LogisticReaction2D()
      .WithName([]() { return "HybridLogisticReaction2D"; })
      .WithMeshes(meshes)
      .WithCellConvection([convection, solution](const Cell &c, const Point &x) {
        return convection * solution.EvaluateCellFlux(c);
      })
      .WithFaceConvection(
          [convection, solution](const Cell &c, int face, const VectorField &N, const Point &x) {
            return convection * solution.EvaluateNormalFlux(c, face);
          });
}

ReactionPDEProblemBuilder
ReactionPDEProblemBuilder::PollutionLogisticReactionSquare500(const HybridSolution solution,
                                                              std::shared_ptr<Meshes> meshes) {
  double convection = 1.0;
  Config::Get("Convection", convection);

  return LogisticReaction2D()
      .WithName([]() { return "PollutionLogisticReactionSquare500"; })
      .WithMeshes(meshes)
      .WithCellConvection([convection, solution](const Cell &c, const Point &x) {
        return convection * solution.EvaluateCellFlux(c);
      })
      .WithFaceConvection(
          [convection, solution](const Cell &c, int face, const VectorField &N, const Point &x) {
            return convection * solution.EvaluateNormalFlux(c, face);
          });
}

std::unique_ptr<IReactionProblem> CreateReactionProblem(const std::string &problemName) {
  if (problemName == "ConvectionDiffusionCosHat2D")
    return ReactionPDEProblemBuilder::ConvectionDiffusionCosHat2D().Build();

  if (problemName == "ExponentialReaction2D")
    return ReactionPDEProblemBuilder::ExponentialReaction2D().Build();
  if (problemName == "ExponentialReactionSquare500")
    return ReactionPDEProblemBuilder::ExponentialReactionSquare500().Build();

  if (problemName == "LogisticReaction2D")
    return ReactionPDEProblemBuilder::LogisticReaction2D().Build();
  if (problemName == "LogisticReactionSquare500")
    return ReactionPDEProblemBuilder::LogisticReactionSquare500().Build();

  if (problemName == "PollutionExponentialReaction2D") {
    const auto problem = CreateEllipticProblemShared("Laplace2D");
    const auto solution = HybridPDESolver(PDESolverConfig("Hybrid", "Laplace2D")).Run(problem);

    return ReactionPDEProblemBuilder::PollutionExponentialReaction2D(solution,
                                                                     problem->IntoMeshes())
        .Build();
  }

  if (problemName == "PollutionExponentialReactionSquare500") {
    const auto problem = CreateEllipticProblemShared("LaplaceSquare500");
    const auto solution =
        HybridPDESolver(PDESolverConfig("Hybrid", "LaplaceSquare500")).Run(problem);

    return ReactionPDEProblemBuilder::PollutionExponentialReactionSquare500(solution,
                                                                            problem->IntoMeshes())
        .Build();
  }

  if (problemName == "PollutionLogisticReaction2D") {
    const auto problem = CreateEllipticProblemShared("Laplace2D");
    const auto solution = HybridPDESolver(PDESolverConfig("Hybrid", "Laplace2D")).Run(problem);

    return ReactionPDEProblemBuilder::PollutionLogisticReaction2D(solution, problem->IntoMeshes())
        .Build();
  }

  if (problemName == "PollutionLogisticReactionSquare500") {
    const auto problem = CreateEllipticProblemShared("LaplaceSquare500");
    const auto solution =
        HybridPDESolver(PDESolverConfig("Hybrid", "LaplaceSquare500")).Run(problem);

    return ReactionPDEProblemBuilder::PollutionLogisticReactionSquare500(solution,
                                                                         problem->IntoMeshes())
        .Build();
  }

  Exit(problemName + " not found")
}

std::unique_ptr<IReactionProblem>
CreateReactionProblemUnique(const std::string &problemName) {
  return CreateReactionProblem(problemName);
}

std::shared_ptr<IReactionProblem>
CreateReactionProblemShared(const std::string &problemName) {
  return std::shared_ptr<IReactionProblem>(
    CreateReactionProblem(problemName)
  );
}

void CreateProblemShared(const std::string &problemName, std::shared_ptr<IReactionProblem> &ptr) {
  ptr = std::move(CreateReactionProblemShared(problemName));
}
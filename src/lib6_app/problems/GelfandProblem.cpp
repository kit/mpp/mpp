#include "GelfandProblem.hpp"

#include <format>

GelfandProblem *CreateGelfandProblem(const std::string &problemName) {
  if (problemName == "Gelfand") return new GelfandProblem();
  else Exit(std::format("{} not found for SpaceDim={}", problemName, SpaceDimension));
}

std::shared_ptr<GelfandProblem> CreateGelfandProblemShared(const std::string &problemName) {
  return std::shared_ptr<GelfandProblem>(CreateGelfandProblem(problemName));
}

void CreateProblemShared(const std::string &problemName, std::shared_ptr<GelfandProblem> &ptr) {
  ptr = std::move(CreateGelfandProblemShared(problemName));
}
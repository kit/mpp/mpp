#include "Config.hpp"

struct TimeDependentProblem {
    double cfl = 0.5;
    double startTime = 0.0;
    double endTime = 1.0;
    double deltaTime = 0.0;

    TimeDependentProblem() {
        Config::Get("CFL", cfl);
        Config::Get("t0", startTime);
        Config::Get("T", endTime);
    }

    [[nodiscard]] double GetCFL() const noexcept { return cfl; }
    [[nodiscard]] double GetStepSize(int level) const noexcept { return cfl * pow(2, -level); }
    [[nodiscard]] double GetStartTime() const noexcept { return startTime; }
    [[nodiscard]] double GetEndTime() const noexcept { return endTime; }
};
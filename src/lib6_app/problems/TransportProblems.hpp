#ifndef _DGPROBLEM_H_
#define _DGPROBLEM_H_

#include <optional>
#include "AcousticProblems.hpp"
#include "Algebra.hpp"
#include "IProblem.hpp"
#include "MixedPDESolver.hpp"

class ITransportProblem : virtual public IProblem, public TimeDependentProblem {
public:
  ITransportProblem() {}

  virtual bool RHS() const { return false; };

  virtual double Solution(double t, const Cell &c, const Point &x) const {
    THROW("Solution for Problem " + Name() + " not implemented.")
  }

  virtual double Inflow(double t, const Cell &c, const Point &x) const {
    THROW("Inflow for Problem " + Name() + " not implemented.")
  }

  virtual VectorField Flux(const MeshIndex &level, const Cell &c, const Point &x) const {
    return zero;
  }

  virtual double FaceNormalFlux(const MeshIndex &level, const Cell &c, int f, const VectorField &N,
                                const Point &x) const {
    return Flux(level, c, x) * N;
  }

  virtual double divFluxVector(const MeshIndex &level, const Cell &c, const Point &x) const {
    return 0;
  }

  double DivFlux(const MeshIndex &level, const Cell &c, const Point &x, double &u,
                 VectorField &Du) const {
    return u * divFluxVector(level, c, x) + Flux(level, c, x) * Du;
  }

  bool HasInflow(const MeshIndex &level, const Cell &c, const Point &x,
                 VectorField N) const {
    return Flux(level, c, x) * N < 0;
  }

  virtual double InflowData(const MeshIndex &level, const Cell &c, double time, const Point &x,
                            VectorField &N) const {
    return 0.0;
  }

  bool HasOutflow(const MeshIndex &level, const Cell &c, const Point &x, VectorField N) const {
    return Flux(level, c, x) * N >= 0;
  }

  virtual bool InternalInflow() const { return false; }

  virtual bool FwdProblem() const { return true; };
};

class TransportPDEProblem : public ITransportProblem {
private:
  friend class TransportPDEProblemBuilder;

  using FluxFunction =
      std::function<VectorField(const MeshIndex &level, const Cell &c, const Point &x)>;
  using SolutionFunction = std::function<double(double t, const Cell &C, const Point &x)>;
  using FaceNormalFluxFunction = std::function<double(const MeshIndex &level, const Cell &c, int f,
                                                      const VectorField &N, const Point &x)>;
  using NameFunction = std::function<std::string()>;
  using InflowFunction = std::function<double(double t, const Cell &c, const Point &x)>;

  bool rhs = false;
  bool hasExactSolution = false;
  bool internalInflow = false;
  bool fwd = true;

  FluxFunction fluxFunction = nullptr;

  SolutionFunction solutionFunction = nullptr;

  FaceNormalFluxFunction faceNormalFluxFunction = nullptr;

  NameFunction nameFunction = nullptr;

  InflowFunction inflowFunction = nullptr;
public:
  explicit TransportPDEProblem(std::shared_ptr<Meshes> meshes) : IProblem(std::move(meshes)) {}

  explicit TransportPDEProblem(std::string meshName) : IProblem(std::move(meshName)) {}

  bool RHS() const override { return rhs; }

  bool InternalInflow() const override { return internalInflow; }

  bool FwdProblem() const override { return fwd; }

  bool HasExactSolution() const override { return hasExactSolution; }

  double Solution(double t, const Cell &c, const Point &x) const override {
    if (solutionFunction) return solutionFunction(t, c, x);
    return 0.;
  }

  double Inflow(double t, const Cell &c, const Point &x) const override {
    if (inflowFunction) return inflowFunction(t, c, x);
    return 0.0;
  }

  VectorField Flux(const MeshIndex &level, const Cell &c, const Point &x) const override {
    if (fluxFunction) return fluxFunction(level, c, x);
    return zero;
  }

  double FaceNormalFlux(const MeshIndex &level, const Cell &c, int f, const VectorField &N,
                        const Point &x) const override {
    if (faceNormalFluxFunction) return faceNormalFluxFunction(level, c, f, N, x);
    return fluxFunction(level, c, x) * N;
  }

  std::string Name() const override {
    if (nameFunction) return nameFunction();
    return "TransportPDEProblem";
  }
};

class TransportPDEProblemBuilder {
private:
  std::string meshName = "";
  std::shared_ptr<Meshes> meshes;

  TransportPDEProblem::FluxFunction fluxFunction = nullptr;
  TransportPDEProblem::SolutionFunction solutionFunction = nullptr;
  TransportPDEProblem::FaceNormalFluxFunction faceNormalFluxFunction = nullptr;
  TransportPDEProblem::NameFunction nameFunction = nullptr;
  TransportPDEProblem::InflowFunction inflowFunction = nullptr;
  std::optional<std::pair<double, double>> startAndEndTime = {};
  bool hasExactSolution = false;
  bool internalInflow = false;
  bool rhs = false;
  bool fwd = true;
public:
  explicit TransportPDEProblemBuilder() {}

  [[nodiscard]]
  TransportPDEProblemBuilder &WithRHS(const bool _rhs) {
    this->rhs = _rhs;
    return *this;
  }

  [[nodiscard]]
  TransportPDEProblemBuilder &WithInternalInflow(const bool _internalInflow) {
    this->internalInflow = _internalInflow;
    return *this;
  }

  [[nodiscard]]
  TransportPDEProblemBuilder &WithInflow(TransportPDEProblem::InflowFunction func) {
    inflowFunction = std::move(func);
    return *this;
  }

  [[nodiscard]]
  TransportPDEProblemBuilder &WithHasExactSolution(const bool _hasExactSolution) {
    this->hasExactSolution = _hasExactSolution;
    return *this;
  }

  [[nodiscard]]
  TransportPDEProblemBuilder &WithCalculateFwdProblem(const bool fwd) {
    this->fwd = fwd;
    return *this;
  }

  [[nodiscard]]
  TransportPDEProblemBuilder &
  WithSolution(TransportPDEProblem::SolutionFunction func) {
    solutionFunction = std::move(func);
    return *this;
  }

  [[nodiscard]]
  TransportPDEProblemBuilder &
  WithFlux(TransportPDEProblem::FluxFunction func) {
    fluxFunction = std::move(func);
    return *this;
  }

  [[nodiscard]]
  TransportPDEProblemBuilder &
  WithFaceNormalFlux(TransportPDEProblem::FaceNormalFluxFunction func) {
    faceNormalFluxFunction = std::move(func);
    return *this;
  }

  [[nodiscard]]
  TransportPDEProblemBuilder &
  WithName(TransportPDEProblem::NameFunction func) {
    nameFunction = std::move(func);
    return *this;
  }

  [[nodiscard]]
  TransportPDEProblemBuilder &WithMeshName(std::string &&name) {
    meshName = name;
    return *this;
  }

  [[nodiscard]]
  TransportPDEProblemBuilder &WithTimeDomain(std::pair<double, double> &&timeDomain) {
    startAndEndTime = timeDomain;
    return *this;
  }

  /*
   * Set a problem mesh
   *
   * Calling this overrides any mesh name that was set before,
   * causing it to be ignored.
   */
  [[nodiscard]]
  TransportPDEProblemBuilder &WithMeshes(std::shared_ptr<Meshes> sharedMeshes) {
    meshName = sharedMeshes->Name();
    meshes = sharedMeshes;
    return *this;
  }

  [[nodiscard]]
  std::unique_ptr<TransportPDEProblem> Build() {
    std::unique_ptr<TransportPDEProblem> problem;
    if (meshes) {
      problem = std::make_unique<TransportPDEProblem>(meshes);
    } else {
      problem = std::make_unique<TransportPDEProblem>(meshName);
    }

    problem->rhs = rhs;
    problem->hasExactSolution = hasExactSolution;
    problem->internalInflow = internalInflow;
    problem->solutionFunction = std::move(solutionFunction);
    problem->faceNormalFluxFunction = std::move(faceNormalFluxFunction);
    problem->fluxFunction = std::move(fluxFunction);
    problem->inflowFunction = std::move(inflowFunction);
    problem->nameFunction = std::move(nameFunction);
    if(startAndEndTime.has_value()) {
      problem->startTime = startAndEndTime.value().first;
      problem->endTime = startAndEndTime.value().second;
    }

    return problem;
  }

  [[nodiscard]]
  std::shared_ptr<TransportPDEProblem> BuildShared() {
    return std::shared_ptr<TransportPDEProblem>(Build());
  }

  static TransportPDEProblemBuilder RiemannTransport1D();
  static TransportPDEProblemBuilder InflowTest();
  static TransportPDEProblemBuilder Inflow();
  static TransportPDEProblemBuilder Hat();
  static TransportPDEProblemBuilder FatHat();
  static TransportPDEProblemBuilder TravelingWave();
  static TransportPDEProblemBuilder SphericalWave2D();
  static TransportPDEProblemBuilder CircleWave2D();
  static TransportPDEProblemBuilder SineWave2D();
  static TransportPDEProblemBuilder CirclePacman();
  static TransportPDEProblemBuilder Riemann1D();
  static TransportPDEProblemBuilder Riemann2D();
  static TransportPDEProblemBuilder GaussHat2D();
  static TransportPDEProblemBuilder CosHat1D();
  static TransportPDEProblemBuilder CosHat2D();
  static TransportPDEProblemBuilder Inflow2D();
  static TransportPDEProblemBuilder Pollution(MultiLevelHybridSolution solutions,
                                              std::shared_ptr<Meshes> meshes);
  static TransportPDEProblemBuilder PollutionSquare2(MultiLevelHybridSolution solutions,
                                                     std::shared_ptr<Meshes> meshes);
  static TransportPDEProblemBuilder PollutionSquare500(MultiLevelHybridSolution solutions,
                                                       std::shared_ptr<Meshes> meshes);
  static TransportPDEProblemBuilder PollutionSquare501(MultiLevelHybridSolution solutions,
                                                       std::shared_ptr<Meshes> meshes);
  static TransportPDEProblemBuilder
  PollutionSquare500ST(MultiLevelHybridSolution solutions,
                       const std::unordered_map<int, std::shared_ptr<Distribution>> &distributions);
};

ITransportProblem *CreateTransportProblem(const std::string &problemName);

std::unique_ptr<ITransportProblem>
CreateTransportProblemUnique(const std::string &problemName);

std::shared_ptr<ITransportProblem>
CreateTransportProblemShared(const std::string &problemName);

void CreateProblemShared(const std::string &problemName, std::shared_ptr<ITransportProblem> &ptr);

#endif

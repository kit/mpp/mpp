#include "TransportProblems.hpp"

#include <numbers>

std::vector<double> CreateTimesteps(double dt, double T) {
  const uint64_t steps = ceil(T / dt);
  std::vector<double> timesteps(steps + 1);
  for (size_t i = 0; i <= steps; i++) {
    timesteps[i] = (double)i * dt;
  }
  return timesteps;
}

std::vector<double> CreateTimestepsFromConfig() {
  double T = 1;
  double dt = 0.1;
  Config::Get("T", T);
  Config::Get("dt", dt);
  return CreateTimesteps(dt, T);
}

TransportPDEProblemBuilder TransportPDEProblemBuilder::RiemannTransport1D() {
  const double c = 1.0;

  auto amplitude = [](double r) -> double {
    if (abs(r) > 0.06251) return 0.0;
    return 1.0;
  };

  return TransportPDEProblemBuilder()
      .WithMeshName("Interval")
      .WithName([]() { return "RiemannTransport1D"; })
    .WithSolution([amplitude, c](double t, const Cell &C, const Point &x) -> double {
      return amplitude(c * t - x[0] + 0.5);
    })
      .WithFlux([](const MeshIndex &level, const Cell &c, const Point &x) -> VectorField {
        return {1.0, 0.0};
      })
      .WithFaceNormalFlux([](const MeshIndex &level, const Cell &c, int f, const VectorField &N,
                             const Point &x) -> double { return VectorField(1.0, 0.0) * N; });
}

TransportPDEProblemBuilder TransportPDEProblemBuilder::InflowTest() {
  return TransportPDEProblemBuilder()
      .WithMeshName("Square-10x10")
      .WithName([]() { return "InflowTest"; })
      .WithSolution([](double t, const Cell &C, const Point &x) -> double {
        if (x[0] == 0.0 && x[1] >= 1.0) return 1.0;
      return 0.0;
    });
}

TransportPDEProblemBuilder TransportPDEProblemBuilder::Inflow() {
  return TransportPDEProblemBuilder()
      .WithMeshName("Square-10x10")
      .WithName([]() { return "Inflow"; })
      .WithSolution([](double t, const Cell &C, const Point &x) -> double {
        if (x[1] == 10) return 10.0;
      return 0.0;
    });
}

TransportPDEProblemBuilder TransportPDEProblemBuilder::Hat() {
  return TransportPDEProblemBuilder()
      .WithMeshName("Square-10x10")
      .WithName([]() { return "Hat"; })
      .WithSolution([](double t, const Cell &C, const Point &x) -> double {
        Point midPoint = Point(5, 5);
      double rr;
      rr = pow(0.5, 3) - norm(midPoint - x);
      if (norm(midPoint - x) < 0.05)
        return rr * 40;
      return 0.0;
    });
}

TransportPDEProblemBuilder TransportPDEProblemBuilder::FatHat() {
  return TransportPDEProblemBuilder()
      .WithMeshName("Square-10x10")
      .WithName([]() { return "FatHat"; })
      .WithSolution([](double t, const Cell &C, const Point &x) -> double {
        if (x[0] >= 1.0 && x[0] <= 4.0 && x[1] >= 1.0 && x[1] <= 4.0) return 1.0;
      else return 0.0;
    });
}

TransportPDEProblemBuilder TransportPDEProblemBuilder::TravelingWave() {
  VectorField Q(0.0, -15.0, 0.0);
  VectorField N(0.0, -1.0, 0.0);
  double c = N * Q;

  auto amplitude = [](double r) -> double {
    if (abs(r) < 0.625) return 1.0;
    else return 0.0;
  };

  return TransportPDEProblemBuilder()
      .WithMeshName("UnitSquare")
      .WithName([]() { return "TravelingWave"; })
    .WithRHS(true)
      .WithSolution([amplitude, N, c](double t, const Cell &C, const Point &x) -> double {
        if (x[0] >= 5 && x[0] <= 15) {
        Point Pmid = Point(0.0, 21.0, 0.0);
        Point xd = x - Pmid;
        return amplitude(c * t - N[0] * xd[0] - N[1] * xd[1] - N[2] * xd[2]);
      }
      return 0.0;
    })
      .WithFlux(
          [Q](const MeshIndex &level, const Cell &c, const Point &x) -> VectorField { return Q; });
}

TransportPDEProblemBuilder TransportPDEProblemBuilder::SphericalWave2D() {
  const double aa = 1.0;
  const double cc = 0.5;

  return TransportPDEProblemBuilder()
      .WithMeshName("UnitSquare")
      .WithName([]() { return "SphericalWave2D"; })
    .WithHasExactSolution(true)
      .WithSolution([aa, cc](double t, const Cell &C, const Point &x) -> double {
        Point midPoint = Point(t, t, 0.0);
      double r = dist(midPoint, x);
      if (r < 1 / cc) return aa * pow(cos(cc * std::numbers::pi * r) + 1.0, 2.0);
      return 0.0;
    })
      .WithFlux([](const MeshIndex &level, const Cell &c, const Point &x) -> VectorField {
        return {5.0, 5.0, 0.0};
      });
}

TransportPDEProblemBuilder TransportPDEProblemBuilder::CircleWave2D() {
  using std::numbers::pi;
  const double aa = 1.0;
  const double cc = 0.5;

  return TransportPDEProblemBuilder()
      .WithMeshName("Square-10x10")
      .WithName([]() { return "CircleWave2D"; })
      .WithHasExactSolution(true)
      .WithSolution([aa, cc](double t, const Cell &C, const Point &x) -> double {
        Point midPoint = 5.0 * Point(cos(2. * pi * t), sin(2. * pi * t));
        double r = dist(midPoint, x);
        if (r < 1 / cc) return aa * pow(cos(cc * pi * r) + 1.0, 2.0);
        return 0.0;
      })
      .WithFlux([](const MeshIndex &level, const Cell &c, const Point &x) -> VectorField {
        return 2 * pi * VectorField(-x[1], x[0], 0.0);
      });
}

TransportPDEProblemBuilder TransportPDEProblemBuilder::SineWave2D() {
  return TransportPDEProblemBuilder()
      .WithMeshName("UnitSquare")
      .WithName([]() { return "SineWave2D"; })
      .WithHasExactSolution(true)
      .WithSolution([](double t, const Cell &C, const Point &x) -> double {
        return sin((x[0] + x[1] - 2.0 * t) * 1. / 5. * std::numbers::pi);
      })
      .WithFlux([](const MeshIndex &level, const Cell &c, const Point &x) -> VectorField {
        return {1., 1., 0};
      });
}

TransportPDEProblemBuilder TransportPDEProblemBuilder::CirclePacman() {
  using std::numbers::pi;
  double aa = 1.0;
  double cc = 0.5;
  double alpha = pi / 4;

  return TransportPDEProblemBuilder()
      .WithMeshName("Square-10x10")
      .WithName([]() { return "CirclePacman"; })
      .WithSolution([aa, cc, alpha](double t, const Cell &C, const Point &x) -> double {
        Point Pmid = 5.0 * Point(cos(2. * pi * t), sin(2. * pi * t));
        Point P = x - Pmid;
        double r = dist(Pmid, x);

        if (r < 1 / cc) {
          double MidAngle = atan2(Pmid[1], Pmid[0]);
          double angle = atan2(P[1], P[0]) - MidAngle + 8 * pi - pi / 2;
          angle = fmod(angle, 2 * pi);
          if (abs(angle) < alpha || abs(angle) > 2 * pi - alpha) return 0.0;
          return aa * pow(cos(cc * pi * r) + 1.0, 2.0);
        }
        return 0.0;
      })
      .WithFlux([](const MeshIndex &level, const Cell &c, const Point &x) -> VectorField {
        return 2 * pi * VectorField(-x[1], x[0], 0.0);
      });
}

TransportPDEProblemBuilder TransportPDEProblemBuilder::Riemann1D() {
  const double thickness = 1.0 / 8.0;
  const double centerStart = 1.5 * thickness;

  auto center = [centerStart](double t) -> double {
    return centerStart + t;
  };

  return TransportPDEProblemBuilder()
      .WithMeshName("Interval")
      .WithHasExactSolution(true)
    .WithName([]() { return "Riemann1D"; })
      .WithSolution([thickness, center](double t, const Cell &C, const Point &x) -> double {
        if (abs(center(t) - x[0]) < thickness / 2.0)
        return 1.0 / thickness;
      else
        return 0.0;
    })
      .WithFlux([](const MeshIndex &level, const Cell &c, const Point &x) -> VectorField {
        return {1.0, 0.0, 0.0};
      });
}

TransportPDEProblemBuilder TransportPDEProblemBuilder::Riemann2D() {
  const double thickness = 1.0 / 8.0;
  const double centerStart = 1 - 1.5 * thickness;

  auto center = [centerStart](double t) -> double {
    return centerStart - t;
  };

  return TransportPDEProblemBuilder()
      .WithMeshName("UnitSquare")
      .WithName([]() { return "Riemann2D"; })
    .WithHasExactSolution(true)
      .WithSolution([thickness, center](double t, const Cell &C, const Point &x) -> double {
        if (abs(center(t) - x[1]) < thickness / 2.0)
        return 1.0 / thickness;
      else
        return 0.0;
    })
      .WithFlux([](const MeshIndex &level, const Cell &c, const Point &x) -> VectorField {
        return {0.0, -1.0, 0.0};
      });
}

TransportPDEProblemBuilder TransportPDEProblemBuilder::Inflow2D() {
  const double thickness = 1.0 / 8.0;
  const double centerStart = 1 - 1.5 * thickness;

  auto center = [centerStart](double t) -> double { return centerStart - t; };

  return TransportPDEProblemBuilder()
      .WithMeshName("UnitSquare")
      .WithName([]() { return "Inflow2D"; })
      .WithRHS([]() { return true; })
      .WithSolution(
          [thickness, center](double t, const Cell &C, const Point &x) -> double { return 0.0; })
      .WithFlux([](const MeshIndex &level, const Cell &c, const Point &x) -> VectorField {
        return {0.0, -1.0, 0.0};
      })
      .WithInternalInflow([]() { return true; })
      .WithInflow([](double t, const Cell &c, const Point &x) {
        if ((sqrt(pow(x[0] - 0.5, 2) + pow(x[1] - 0.9, 2)) < 0.05))
          return 1.0; // Todo: check for dependency on timestep.
        else return 0.0;
      });
}

TransportPDEProblemBuilder TransportPDEProblemBuilder::GaussHat2D() {
  const Tensor sigma(0.005, 0.0, 0.0, 0.005);
  const double factor = 1.0 / sqrt(pow(2.0 * std::numbers::pi, 2) * sigma.det());

  auto mu = [](double t) -> Point { return {0.25 + t, 0.25 + t}; };

  return TransportPDEProblemBuilder()
      .WithMeshName("UnitSquare")
      .WithName([]() { return "GaussHat2D"; })
      .WithSolution([sigma, factor, mu](double t, const Cell &C, const Point &x) -> double {
        VectorField diff = (x - mu(t));
      VectorField temp = Invert(sigma) * diff;
      double exponent = -0.5 * diff * temp;
      return factor * exp(exponent);
    })
      .WithFlux([](const MeshIndex &level, const Cell &c, const Point &x) -> VectorField {
        return {0.5, 0.5};
      });
}

class LinearTransport : public ITransportProblem {
public:
  VectorField q = {1.0, 1.0, 0.0};
  VectorField a = {4.0/3.0, 2.0/3.0, 0.0};

  LinearTransport() : IProblem("UnitSquare") {}

  std::string Name() const override {
    return "LinearTransport";
  }
  VectorField Flux(const MeshIndex &level, const Cell &c, const Point &x) const override {
    return q;
  }
  double divFluxVector(const MeshIndex &level, const Cell &c, const Point &x) const override {
    return 0;
  }
  double Solution(double t, const Cell &c, const Point &x) const override {
    return (x - t * q) * a;
  }

  double InflowData(const MeshIndex &level, const Cell &c, double time, const Point &x,
                    VectorField &N) const override {
    return Solution(time, c, x) * Flux(level, c, x) * N;
  }
  bool RHS() const { return true; };

  bool HasExactSolution() const override { return true; }
};

class LinearTransportST : public ITransportProblem {
public:
  VectorField q = {1.0, 1.0, 0.0};
  VectorField a = {4.0 / 3.0, 2.0 / 3.0, 0.0};

  LinearTransportST() :
      IProblem(MeshesCreator("UnitSquare").WithTimesteps(CreateTimesteps(0.1, 1))) {}

  std::string Name() const override { return "LinearTransport"; }

  VectorField Flux(const MeshIndex &level, const Cell &c, const Point &x) const override {
    return q;
  }

  double divFluxVector(const MeshIndex &level, const Cell &c, const Point &x) const override {
    return 0;
  }

  double Solution(double t, const Cell &c, const Point &x) const override {
    return (x - t * q) * a;
  }

  double InflowData(const MeshIndex &level, const Cell &c, double time, const Point &x,
                    VectorField &N) const override {
    return Solution(time, c, x) * Flux(level, c, x) * N;
  }

  bool RHS() const { return true; };

  bool HasExactSolution() const override { return true; }
};

TransportPDEProblemBuilder TransportPDEProblemBuilder::Pollution(MultiLevelHybridSolution solutions,
                                                                 std::shared_ptr<Meshes> meshes) {
  const double d = 1.0 / 16.0;

  return TransportPDEProblemBuilder()
      .WithMeshes(meshes)
      .WithName([]() { return "Pollution"; })
      .WithSolution([d](double t, const Cell &C, const Point &x) -> double {
        if ((abs(1 - x[1] - 1.5 * d) < d / 2) && (abs(0.5 - x[0]) < 3 * d)) return 1.0;
        else return 0.0;
      })
      .WithFlux([solutions](const MeshIndex &level, const Cell &c, const Point &x) -> VectorField {
        return solutions.EvaluateCellFlux(level, c);
      })
      .WithFaceNormalFlux(
          [solutions](const MeshIndex &level, const Cell &c, int face, const VectorField &N,
                      const Point &x) { return solutions.EvaluateNormalFlux(level, c, face); });
}

TransportPDEProblemBuilder
TransportPDEProblemBuilder::PollutionSquare500(MultiLevelHybridSolution solutions,
                                               std::shared_ptr<Meshes> meshes) {
  using std::numbers::pi;
  // Todo: One could get the meshes out of the solutions,
  //  but this requires that the mixed and the transport problems share the ST mesh
  //  and not just the distribution.
  bool smooth = false;
  Config::Get("smooth", smooth);

  double d = 1.0 / 16.0;
  auto dist_x = [d](double x) -> double { return abs(0.5 - x) / (3 * d); };
  auto dist_y = [d](double t, double y) -> double { return 2 * abs(1 - y - t - 1.5 * d) / d; };

  return TransportPDEProblemBuilder()
      .WithMeshes(meshes)
      .WithSolution([dist_x, dist_y, smooth](double t, const Cell &C, const Point &x) -> double {
        if (dist_x(x[0]) > 1) return 0.0;
        if (dist_y(t, x[1]) > 1) return 0.0;
        if (smooth) return (1 + cos(pi * dist_x(x[0]))) * (1 + cos(pi * dist_y(t, x[1])));
        return 1.0 / 0.0234375;
      })
      .WithFlux([solutions](const MeshIndex &level, const Cell &c, const Point &x) {
        return solutions.EvaluateCellFlux(level, c);
      })
      .WithFaceNormalFlux(
          [solutions](const MeshIndex &level, const Cell &c, int face, const VectorField &N,
                      const Point &x) { return solutions.EvaluateNormalFlux(level, c, face); })
      .WithName([]() { return "PollutionSquare500"; });
}

TransportPDEProblemBuilder TransportPDEProblemBuilder::PollutionSquare500ST(
    MultiLevelHybridSolution solutions,
    const std::unordered_map<int, std::shared_ptr<Distribution>> &distributions) {
  MeshesCreator meshesCreator("Square500");
  if (!distributions.empty()) {
    auto dist = distributions.at(0);
    auto stDistribution = std::make_shared<Distribution>("LikeSpace", dist);
    meshesCreator.WithDistribution(stDistribution);
  }

  meshesCreator.WithTimesteps(CreateTimestepsFromConfig());

  return TransportPDEProblemBuilder::PollutionSquare500(solutions, meshesCreator.CreateShared())
      .WithName([]() { return "PollutionSquare500ST"; });
}

TransportPDEProblemBuilder
TransportPDEProblemBuilder::PollutionSquare501(MultiLevelHybridSolution solutions,
                                               std::shared_ptr<Meshes> meshes) {
  return TransportPDEProblemBuilder::PollutionSquare500(solutions, meshes).WithName([]() {
    return "PollutionSquare501";
  });
}

TransportPDEProblemBuilder
TransportPDEProblemBuilder::PollutionSquare2(MultiLevelHybridSolution solutions,
                                             std::shared_ptr<Meshes> meshes) {

  return TransportPDEProblemBuilder::PollutionSquare500(solutions, meshes).WithName([]() {
    return "PollutionSquare2";
  });
}

class GaussHat1D : public ITransportProblem {
private:
  double sigma2;
  double factor;

public:
  GaussHat1D(double sigma = 0.05) : IProblem("Interval"), sigma2(sigma * sigma) {
    endTime = 0.5;
    factor = 1.0 / sqrt(2.0 * std::numbers::pi * sigma2);
  }

  static Point mu(double t) {
    return {0.25 + t, 0.0};
  }

  double Solution(double t, const Cell &c, const Point &x) const override {
    VectorField diff = (x - mu(t));
    double exponent = -diff * diff / (2 * sigma2);
    return factor * exp(exponent);
  }

  static VectorField TransportFlux(const Point &x) {
    return {1.0, 0.0};
  }

  VectorField Flux(const MeshIndex &level, const Cell &c, const Point &x) const override {
    return VectorField{1.0, 0.0};
  }

  std::string Name() const override { return "GaussHat1D"; }
};

TransportPDEProblemBuilder TransportPDEProblemBuilder::CosHat1D() {
  const double amplitude = 1.0;
  const double frequency = 8.0;

  return TransportPDEProblemBuilder()
      .WithMeshName("Interval")
      .WithName([]() { return "CosHat1D"; })
      .WithSolution([amplitude, frequency](double t, const Cell &C, const Point &x) -> double {
        Point midPoint = Point(0.25 + t, 0.0);
      double r = dist(midPoint, x);
      if (r < (std::numbers::pi / 2.0) / frequency) return amplitude * pow(cos(frequency * r), 2.0);
      return 0.0;
    })
      .WithFlux([](const MeshIndex &level, const Cell &c, const Point &x) -> VectorField {
        return {1.0, 0.0};
      });
}

TransportPDEProblemBuilder TransportPDEProblemBuilder::CosHat2D() {
  using std::numbers::pi;
  const double amplitude = 1.0;
  const double frequency = 8.0;

  return TransportPDEProblemBuilder()
      .WithMeshName("UnitSquare")
      .WithName([]() { return "CosHat2D"; })
      .WithSolution([amplitude, frequency](double t, const Cell &C, const Point &x) -> double {
        Point midPoint = 0.25 * (Point(cos(2.0 * pi * t), sin(2.0 * pi * t))) + Point(0.5, 0.5);
        double r = dist(midPoint, x);
        if (r < (pi / 2.0) / frequency) return amplitude * pow(cos(frequency * r), 2.0);
        return 0.0;
      })
      .WithFlux([](const MeshIndex &level, const Cell &c, const Point &x) -> VectorField {
        return 2 * pi * VectorField(0.5 - x[1], x[0] - 0.5, 0.0);
      });
}

ITransportProblem *CreateTransportProblem(const std::string &problemName) {
  // TODO how should flux be implemented for the commented out classes?
  if (problemName == "Riemann1D") return TransportPDEProblemBuilder::Riemann1D().Build().release();
  else if (problemName == "Riemann2D" || problemName == "RiemannUnitSquare")
    return TransportPDEProblemBuilder::Riemann2D().Build().release();
  else if (problemName == "Inflow2D")
    return TransportPDEProblemBuilder::Inflow2D().Build().release();
  else if (problemName == "CircleWave2D")
    return TransportPDEProblemBuilder::CircleWave2D().Build().release();
  else if (problemName == "Inflow") return TransportPDEProblemBuilder::Inflow().Build().release();
  else if (problemName == "SphericalWave2D")
    return TransportPDEProblemBuilder::SphericalWave2D().Build().release();
  else if (problemName == "SineWave2D")
    return TransportPDEProblemBuilder::SineWave2D().Build().release();
  else if (problemName == "FatHat") return TransportPDEProblemBuilder::FatHat().Build().release();
  else if (problemName == "TravelingWave")
    return TransportPDEProblemBuilder::TravelingWave().Build().release();
  else if (problemName == "InflowTest")
    return TransportPDEProblemBuilder::InflowTest().Build().release();
  else if (problemName == "Hat") return TransportPDEProblemBuilder::Hat().Build().release();
  if (problemName == "CosHat2D") return TransportPDEProblemBuilder::CosHat2D().Build().release();
  else if (problemName == "CirclePacman")
    return TransportPDEProblemBuilder::CirclePacman().Build().release();
  else if (problemName == "RiemannTransport1D")
    return TransportPDEProblemBuilder::RiemannTransport1D().Build().release();
  else if (problemName == "GaussHat2D")
    return TransportPDEProblemBuilder::GaussHat2D().Build().release();
  else if (problemName == "GaussHat1D")
    return new GaussHat1D();
  else if (problemName == "CosHat1D")
    return TransportPDEProblemBuilder::CosHat1D().Build().release();
  else if (problemName == "Pollution") {
    const auto problem = CreateEllipticProblemShared("Laplace2D");
    const auto solution = MixedPDESolver().Run(problem);
    return TransportPDEProblemBuilder::Pollution(solution, problem->IntoMeshes()).Build().release();
  } else if (problemName == "LinearTransport") {
    return new LinearTransport();
  } else if (problemName == "LinearTransportST") {
    return new LinearTransportST();
  } else if (problemName == "PollutionSquare500") {
    //    Exit("Dont use this, Build your problem right where you need it!")
    const auto problem = CreateEllipticProblemShared("LaplaceSquare500");
    const auto solution = MixedPDESolver().Run(problem);
    return TransportPDEProblemBuilder::PollutionSquare500(solution, problem->IntoMeshes())
        .Build()
        .release();
  } else if (problemName == "PollutionSquare500ST") {
    Exit("Dont use this, Build your problem right where you need it!")
  } else if (problemName == "PollutionSquare2") {
    const auto problem = CreateEllipticProblemShared("LaplaceSquare2");
    const auto solution = MixedPDESolver().Run(problem);
    return TransportPDEProblemBuilder::PollutionSquare500(solution, problem->IntoMeshes())
        .Build()
        .release();
  } else if (problemName == "PollutionSquare501") {
    const auto problem = CreateEllipticProblemShared("LaplaceSquare501");
    const auto solution = MixedPDESolver().Run(problem);
    return TransportPDEProblemBuilder::PollutionSquare500(solution, problem->IntoMeshes())
        .Build()
        .release();
  } else Exit(problemName + " not found")
}

std::unique_ptr<ITransportProblem>
CreateTransportProblemUnique(const std::string &problemName) {
  return std::unique_ptr<ITransportProblem>(CreateTransportProblem(problemName));
}

std::shared_ptr<ITransportProblem>
CreateTransportProblemShared(const std::string &problemName) {
  return std::shared_ptr<ITransportProblem>(CreateTransportProblem(problemName));
}

void CreateProblemShared(const std::string &problemName, std::shared_ptr<ITransportProblem> &ptr) {
  ptr = std::move(CreateTransportProblemShared(problemName));
}
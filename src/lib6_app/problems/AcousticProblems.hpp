#ifndef MPP_ACOUSTIC_PROBLEMS_H
#define MPP_ACOUSTIC_PROBLEMS_H

#include <format>
#include <optional>
#include "Component.hpp"
#include "IProblem.hpp"
#include "RVector.hpp"
#include "SeismogramData.hpp"
#include "TimeDependentProblem.hpp"

double Ricker(double tau, double duration);

double CosHat(double dist, double width);

double GaussHat(double dist, double width);

// TODO maybe use RVector instead of DampingVector
class DampingVector : public std::valarray<double> {
public:
  DampingVector(const size_t size)
      : std::valarray<double>(0.0, std::max((size_t)1, size)) {}
};

inline double absNorm(const DampingVector &dampingVector) {
  return std::accumulate(std::begin(dampingVector), std::end(dampingVector),
                         0.0, [](double in1, double in2) {
                           return in1 + std::abs(in2);
                         });
}

inline double maxNorm(const DampingVector &dampingVector) {
  return *std::max_element(std::begin(dampingVector), std::end(dampingVector),
                           [](double in1, double in2) {
                             return std::abs(in1) < std::abs(in2);
                           });
}

class AcousticProblem : public IProblem, public TimeDependentProblem {
protected:
  int dim;
  int numL = 0;

  double rho = 1.0;
  double kappa = 1.0;
  double tau = 0.1 / M_PI;
  double tau_p = 0.1;
  std::vector<COMPONENT> components;

  static std::vector<COMPONENT> createComponents(int dim, int nL) {
    std::vector<COMPONENT> components{COMPONENT::V_X};
    if (dim > 1)
      components.push_back(COMPONENT::V_Y);
    if (dim > 2)
      components.push_back(COMPONENT::V_Z);
    components.push_back(COMPONENT::P0);
    if (nL > 0)
      components.push_back(COMPONENT::P1);
    if (nL > 1)
      components.push_back(COMPONENT::P2);
    if (nL > 2)
      components.push_back(COMPONENT::P3);
    if (nL > 3)
      components.push_back(COMPONENT::P4);
    if (nL > 4)
      components.push_back(COMPONENT::P5);
    if (nL > 5) {
      Exit("implement more damping variables.")
    }
    return components;
  }

public:
  AcousticProblem(MeshesVariant meshes, const std::string &defaultMesh, int numL)
      : IProblem(useMeshes(std::move(meshes), defaultMesh)), dim(GetMeshes().dim()), numL(numL),
        components(createComponents(dim, numL)) {
  }

  virtual int BndID(const Point &z) const { return 2; }

  int SpaceDim() const { return dim; }

  int NumDamping() const { return numL; }

  bool Damping() const { return numL > 0; }

  virtual double ut(double t, const Point &x, const Cell &c, int i) const {
    Exit("Not implemented: ut(double t, const Point &x, const Cell &c, int i)");
  }

  virtual double ut(double t, const Point &x, const Cell &c,
                    COMPONENT comp) const {
    return 0.0;
  }

  virtual VectorField ut_Velocity(double t, const Point &x,
                                  const Cell &c) const {
    if (dim == 2) {
      return VectorField{ut(t, x, c, COMPONENT::V_X),
                         ut(t, x, c, COMPONENT::V_Y)};
    } else if (dim == 1) {
      return VectorField{ut(t, x, c, COMPONENT::V_X), 0};
    } else if (dim == 3) {
      return VectorField{ut(t, x, c, COMPONENT::V_X),
                         ut(t, x, c, COMPONENT::V_Y),
                         ut(t, x, c, COMPONENT::V_Z)};
    }
    Exit(std::format("ut_Velocity not implemented for dim={}", dim))
  }

  virtual double ut_Pressure(double t, const Point &x, const Cell &c) const {
    return ut(t, x, c, COMPONENT::P0);  // ut_Pressure = sum_{i=0}^numL P_i?
  }

  virtual DampingVector ut_DampingPressure(double t, const Point &x,
                                           const Cell &c) const {
    DampingVector DP(numL);
    for (int i = 0; i < numL; i++) {
      COMPONENT comp = GetDampingComponent(i);
      DP[i] = ut(t, x, c, comp);
    }
    return DP;
  }

  virtual double ut_dual(const Point &x, COMPONENT comp, const Point &a,
                         const Point &b) const {
    if (x[0] < a[0] || x[1] < a[1] || x[0] > b[0] || x[1] > b[1])
      return 0.0;
    Point ba = b - a;
    if (comp != COMPONENT::P0)
      return 0;
    else
      return 1.0 / (ba[0] * ba[1]);
  }

  double Dirichlet(double t, const Cell &c, const Point &z, int i) const {
    return ut(t, z, c, dim + i);
  }

  double Neumann(double t, const Cell &c, const Point &z,
                 const VectorField &N) const {
    VectorField V = zero;
    for (int i = 0; i < dim; ++i) {
      V[i] = ut(t, z, c, i);
    }
    return V * N;
  }

  virtual bool HasRHS() const { return false; }

  virtual bool hasPointSource(Point &x) const { return false; }

  virtual double F_PointSource(double t, const Point &x) const {
    Exit("Not implemented: F_PointSource(const Point &x)")
  }
  virtual double ForceP_i(double t, const Cell &c, const Point &x,
                          int i) const {
    return 0.0;
  }

  virtual VectorField ForceV(double t, const Cell &c, const Point &x) const {
    return zero;
  }

  virtual void Force(VectorField &V_rhs, RVector &Force_, double t,
                     const Cell &c, const Point &z) const {
    V_rhs = ForceV(t, c, z);
    for (int n = 0; n < Force_.size(); ++n) {
      Force_[n] = ForceP_i(t, c, z, n);
    }
  }

  virtual double Kappa(const Cell &c, const Point &x) const final {
      double kappa = 0.0;
      for (int i = 0; i < 1 + numL; ++i) {
        kappa += Kappa_i(c, x, i);
      }
      return kappa;
  }

  virtual double Kappa_i(const Cell &c, const Point &x, int i) const {
    if (i == 0) return kappa;
    Exit("not implemented here!")
  }

  virtual double Tau_i(const Cell &c, const Point &x, int i) const {
    return tau;
  }

  virtual double Tau_p(const Cell &, const Point &) const {
    Exit("Tau_p not implemented here!");
  }

  virtual double alpha(const Cell &, const Point &) const {
    Exit("alpha not implemented here!");
  }
  virtual double V_p(const Cell &, const Point &) const {
    Exit("V_p not implemented here!");
  }

  virtual double Rho(const Cell &c, const Point &x) const { return rho; }

  virtual bool InRegionOfInterest(const Point &x) const { return true; }

  virtual double u0(const Cell &c, const Point &x, int i) const {
    return ut(0, x, c, i);
  }

  virtual bool hasInitialData() const { return false; }

  virtual double Mdtut(double t, const Point &x, const Cell &c,
                       COMPONENT comp) const {
    Exit("not implemented for " + Name()) return 0.0;
  }

  virtual double Mdtut_Pressure(double t, const Point &x, const Cell &c) const {
    return Mdtut(t, x, c, COMPONENT::P0);
  }

  virtual VectorField Mdtut_Velocity(double t, const Point &x,
                                     const Cell &c) const {
    if (dim == 2) {
      return {Mdtut(t, x, c, COMPONENT::V_X), Mdtut(t, x, c, COMPONENT::V_Y)};
    } else if (dim == 1) {
      return {Mdtut(t, x, c, COMPONENT::V_X), 0};
    } else if (dim == 3) {
      return {Mdtut(t, x, c, COMPONENT::V_X), Mdtut(t, x, c, COMPONENT::V_Y),
              Mdtut(t, x, c, COMPONENT::V_Z)};
    }
    Exit(std::format("Mdtut_Velocity not implemented for dim={}", dim))
  }

  virtual double Dut(double t, const Point &x, const Cell &c,
                     COMPONENT comp) const {
    return 0.0;
  }

  virtual double Aut(double t, const Point &x, const Cell &c,
                     COMPONENT comp) const {
    Exit("not implemented for " + Name()) return 0.0;
  }

  virtual double Aut_Pressure(double t, const Point &x, const Cell &c) const {
    return Aut(t, x, c, COMPONENT::P0);
  }

  virtual VectorField Aut_Velocity(double t, const Point &x,
                                   const Cell &c) const {
    if (dim == 2) {
      return {Aut(t, x, c, COMPONENT::V_X), Aut(t, x, c, COMPONENT::V_Y)};
    } else if (dim == 1) {
      return {Aut(t, x, c, COMPONENT::V_X), 0};
    } else if (dim == 3) {
      return {Aut(t, x, c, COMPONENT::V_X), Aut(t, x, c, COMPONENT::V_Y),
              Aut(t, x, c, COMPONENT::V_Z)};
    }
    Exit(std::format("Aut_Velocity not implemented for dim={}", dim))
  }

  virtual double F(double t, const Cell &c, const Point &x, COMPONENT comp) const {
    if (HasExactSolution()) {
      return Mdtut(t, x, c, comp) + Aut(t, x, c, comp) + Dut(t, x, c, comp);
    }
    return 0.0;
  }

  virtual DampingVector F_DampingPressure(double t, const Cell &c,
                                          const Point &x) const {
    DampingVector DV(numL);
    for (int i = 0; i < numL; i++) {
      DV[i] = F(t, c, x, GetDampingComponent(i));
    }
    return DV;
  }

  virtual double F_Pressure(double t, const Cell &c, const Point &x) const {
    return F(t, c, x, COMPONENT::P0);
  }

  virtual VectorField F_Velocity(double t, const Cell &c,
                                 const Point &x) const {
    if (dim == 2) {
      return {F(t, c, x, COMPONENT::V_X), F(t, c, x, COMPONENT::V_Y)};
    } else if (dim == 1) {
      return {F(t, c, x, COMPONENT::V_X), 0};
    } else if (dim == 3) {
      return {F(t, c, x, COMPONENT::V_X), F(t, c, x, COMPONENT::V_Y),
              F(t, c, x, COMPONENT::V_Z)};
    }
    Exit(std::format("F_Velocity not implemented for dim={}", dim))
  }

  virtual double F_Pkt(const double &t, const Cell &c, const int &i) const {
    return 0.0;
  }

  int Dim() const { return dim + 1 + numL; }

  int nL() const { return numL; }

  std::vector<COMPONENT> GetComponents() const { return components; };

  virtual std::shared_ptr<SeismogramData> GetSourceData() const {
    return nullptr;
  }
};


class AcousticPDEProblem: public AcousticProblem {
private:
  friend class AcousticPDEProblemBuilder;

  // TODO: Use better names for these
  using UtFunction = std::function<double(double t, const Point &x, const Cell &c, int i)>;
  using ForceVFunction = std::function<VectorField(double t, const Cell &c, const Point &x)>;
  using ForcePiFunction = std::function<double(double t, const Cell &c, const Point &x, int i)>;
  using RegionOfInterestFunction = std::function<bool(const Point& x)>;
  using BndIdFunction = std::function<int(const Point &z)>;
  using KappaIFunction = std::function<double(const Cell &c, const Point &z, int i)>;
  using RhoFunction = std::function<double(const Cell &c, const Point &z)>;
  using TauIFunction = std::function<double(const Cell &c, const Point &x, int i)>;
  using TauPFunction = std::function<double(const Cell &c, const Point &x)>;
  using FFunction = std::function<double(double t, const Cell &c, const Point &x, COMPONENT comp)>;
  using MdTutFunction = std::function<double(double t, const Point &x, const Cell &c, COMPONENT comp)>;
  using AutFunction = std::function<double(double t, const Point &x, const Cell &c, COMPONENT comp)>;

  UtFunction utFunction;
  ForceVFunction forceVFunction;
  ForcePiFunction forcePiFunction;
  RegionOfInterestFunction regionOfInterestFunction;
  BndIdFunction bndIdFunction;
  KappaIFunction kappaIFunction;
  RhoFunction rhoFunction;
  TauIFunction tauIFunction;
  TauPFunction tauPFunction;
  FFunction fFunction;
  MdTutFunction mdTutFunction;
  AutFunction autFunction;

  std::string name;
  bool hasExactSolution;
  bool hasRightHandSide;
  bool hasInitialDataInner;
public:
  explicit AcousticPDEProblem(MeshesVariant meshes, const std::string &defaultMesh, int numL):
    AcousticProblem(meshes, defaultMesh, numL) {}

  [[nodiscard]]
  bool HasExactSolution() const override { return hasExactSolution; }

  [[nodiscard]]
  bool hasInitialData() const override { return hasInitialDataInner; }

  double Tau_i(const Cell &c, const Point &x, int i) const override {
    if(tauIFunction) {
      return tauIFunction(c, x, i);
    }

    return tau;
  }

  double Tau_p(const Cell& c, const Point& x) const override {
    if(tauPFunction) {
      return tauPFunction(c, x);
    }

    Exit("Tau_p not implemented here!");
  }

  double F(double t, const Cell &c, const Point &x, COMPONENT comp) const override {
    if(fFunction) {
      return fFunction(t, c, x, comp);
    }

    if (HasExactSolution()) {
      return Mdtut(t, x, c, comp) + Aut(t, x, c, comp) + Dut(t, x, c, comp);
    }
    return 0.0;
  }

  double Mdtut(double t, const Point &x, const Cell &c,
                       COMPONENT comp) const override {
    if (mdTutFunction) {
      return mdTutFunction(t, x, c, comp);
    }

    Exit("not implemented for " + Name());
  }

  double Aut(double t, const Point &x, const Cell &c,
                     COMPONENT comp) const override {
    if (autFunction) {
      return autFunction(t, x, c, comp);
    }

    Exit("not implemented for " + Name());
  }

  [[nodiscard]]
  double ut(double t, const Point &x, const Cell &c, int i) const override {
    if(utFunction) {
      return utFunction(t, x, c, i);
    }

    return 0.0;
  }

  [[nodiscard]]
  VectorField ForceV(double t, const Cell &c, const Point &x) const override {
    if(forceVFunction) {
      return forceVFunction(t, c, x);
    }

    return zero;
  }

  [[nodiscard]]
  double ForceP_i(double t, const Cell &c, const Point &x, int i) const override {
    if(forcePiFunction) {
      return forcePiFunction(t, c, x, i);
    }

    return 0.0;
  }

  [[nodiscard]]
  bool HasRHS() const override {
    return hasRightHandSide;
  }

  [[nodiscard]]
  bool InRegionOfInterest(const Point& x) const override {
    if (regionOfInterestFunction) {
      return regionOfInterestFunction(x);
    }

    return true;
  }

  [[nodiscard]]
  int BndID(const Point& x) const override {
    if (bndIdFunction) {
      return bndIdFunction(x);
    }
    return 2;
  }

  [[nodiscard]]
  double Kappa_i(const Cell &c, const Point &z, int i) const override {
    if(kappaIFunction) {
      return kappaIFunction(c, z, i);
    }

    if (i == 0) return kappa;
    Exit("not implemented here!")
  }

  [[nodiscard]]
  double Rho(const Cell &c, const Point &z) const override {
    if(rhoFunction) {
      return rhoFunction(c, z);
    }
    return rho;
  }

  [[nodiscard]]
  std::string Name() const override {
    return name;
  }
};

class AcousticPDEProblemBuilder {
private:
  MeshesVariant meshes;
  std::string defaultMesh;

  AcousticPDEProblem::UtFunction utFunction;
  AcousticPDEProblem::ForceVFunction forceVFunction;
  AcousticPDEProblem::ForcePiFunction forcePiFunction;
  AcousticPDEProblem::RegionOfInterestFunction regionOfInterest = [](const Point& x) { return true; };
  AcousticPDEProblem::BndIdFunction bndIdFunction;
  AcousticPDEProblem::KappaIFunction kappaIFunction;
  AcousticPDEProblem::RhoFunction rhoFunction;
  AcousticPDEProblem::TauIFunction tauIFunction;
  AcousticPDEProblem::TauPFunction tauPFunction;
  AcousticPDEProblem::FFunction fFunction;
  AcousticPDEProblem::MdTutFunction mdTutFunction;
  AcousticPDEProblem::AutFunction autFunction;

  std::string name = "AcousticPDEProblem";
  bool hasExactSolution = false;
  bool hasRightHandSide = false;
  bool hasInitialData = false;
  int numL = 0;

  std::optional<double> startTime = {};
  std::optional<double> endTime = {};
  std::optional<double> cfl = {};

public:
  [[nodiscard]] explicit AcousticPDEProblemBuilder() {}

  [[nodiscard]] AcousticPDEProblemBuilder&
  WithMeshes(const MeshesVariant meshes) {
    this->meshes = meshes;
    return *this;
  }

  [[nodiscard]] AcousticPDEProblemBuilder&
  WithMeshName(std::string&& name) {
    this->defaultMesh = name;
    return *this;
  }

  [[nodiscard]] AcousticPDEProblemBuilder&
  WithStartTime(double startTime) {
    this->startTime = startTime;
    return *this;
  }

  [[nodiscard]] AcousticPDEProblemBuilder&
  WithEndTime(double endTime) {
    this->endTime = endTime;
    return *this;
  }

  [[nodiscard]] AcousticPDEProblemBuilder&
  WithCfl(double cfl) {
    this->cfl = cfl;
    return *this;
  }

  [[nodiscard]] AcousticPDEProblemBuilder&
  WithNumL(int numL) {
    this->numL = numL;
    return *this;
  }

  [[nodiscard]] AcousticPDEProblemBuilder&
  WithRightHandSide(bool hasRightHandSide) {
    this->hasRightHandSide = hasRightHandSide;
    return *this;
  }

  [[nodiscard]] AcousticPDEProblemBuilder&
  WithInitialData(bool hasInitialData) {
    this->hasInitialData = hasInitialData;
    return *this;
  }

  /*
  * Set a problem mesh
  *
  * Calling this overrides any mesh name that was set before,
  * causing it to be ignored.
  */
  [[nodiscard]] AcousticPDEProblemBuilder&
  WithMeshes(std::shared_ptr<Meshes> sharedMeshes) {
    meshes = sharedMeshes;
    return *this;
  }

  [[nodiscard]] AcousticPDEProblemBuilder&
  WithHasExactSolution(bool hasExactSolution) {
    this->hasExactSolution = hasExactSolution;
    return *this;
  }

  [[nodiscard]] AcousticPDEProblemBuilder&
  WithName(std::string&& name) {
    this->name = name;
    return *this;
  }

  [[nodiscard]] AcousticPDEProblemBuilder&
  WithUtFunction(AcousticPDEProblem::UtFunction utFunction) {
    this->utFunction = utFunction;
    return *this;
  }

  [[nodiscard]] AcousticPDEProblemBuilder&
  WithForceVFunction(AcousticPDEProblem::ForceVFunction forceVFunction) {
    this->forceVFunction = forceVFunction;
    return *this;
  }

  [[nodiscard]] AcousticPDEProblemBuilder&
  WithForcePiFunction(AcousticPDEProblem::ForcePiFunction forcePiFunction) {
    this->forcePiFunction = forcePiFunction;
    return *this;
  }

  [[nodiscard]] AcousticPDEProblemBuilder&
  WithRegionOfInterest(AcousticPDEProblem::RegionOfInterestFunction regionOfInterest) {
    this->regionOfInterest = regionOfInterest;
    return *this;
  }

  [[nodiscard]] AcousticPDEProblemBuilder&
  WithBndIdFunction(AcousticPDEProblem::BndIdFunction bndIdFunction) {
    this->bndIdFunction = bndIdFunction;
    return *this;
  }

  [[nodiscard]] AcousticPDEProblemBuilder&
  WithKappaIFunction(AcousticPDEProblem::KappaIFunction kappaIFunction) {
    this->kappaIFunction = kappaIFunction;
    return *this;
  }

  [[nodiscard]] AcousticPDEProblemBuilder&
  WithRhoFunction(AcousticPDEProblem::RhoFunction rhoFunction) {
    this->rhoFunction = rhoFunction;
    return *this;
  }

  [[nodiscard]] AcousticPDEProblemBuilder&
  WithTauIFunction(AcousticPDEProblem::TauIFunction tauIFunction) {
    this->tauIFunction = tauIFunction;
    return *this;
  }

  [[nodiscard]] AcousticPDEProblemBuilder&
  WithTauPFunction(AcousticPDEProblem::TauPFunction tauPFunction) {
    this->tauPFunction = tauPFunction;
    return *this;
  }

  [[nodiscard]] AcousticPDEProblemBuilder&
  WithFFunction(AcousticPDEProblem::FFunction fFunction) {
    this->fFunction = fFunction;
    return *this;
  }

  [[nodiscard]] AcousticPDEProblemBuilder&
  WithMdTutFunction(AcousticPDEProblem::MdTutFunction mdTutFunction) {
    this->mdTutFunction = mdTutFunction;
    return *this;
  }

  [[nodiscard]] AcousticPDEProblemBuilder&
  WithAutFunction(AcousticPDEProblem::AutFunction autFunction) {
    this->autFunction = autFunction;
    return *this;
  }

  [[nodiscard]] std::unique_ptr<AcousticPDEProblem> Build() {
    std::unique_ptr<AcousticPDEProblem> problem = std::make_unique<AcousticPDEProblem>(meshes, defaultMesh, numL);

    problem->hasExactSolution = std::move(hasExactSolution);
    problem->hasRightHandSide = hasRightHandSide;
    problem->hasInitialDataInner = hasInitialData;
    problem->name = std::move(name);
    problem->utFunction = std::move(utFunction);
    problem->forceVFunction = std::move(forceVFunction);
    problem->forcePiFunction = std::move(forcePiFunction);
    problem->regionOfInterestFunction = std::move(regionOfInterest);
    problem->bndIdFunction = std::move(bndIdFunction);
    problem->kappaIFunction = std::move(kappaIFunction);
    problem->rhoFunction = std::move(rhoFunction);
    problem->tauIFunction = std::move(tauIFunction);
    problem->tauPFunction = std::move(tauPFunction);
    problem->fFunction = std::move(fFunction);
    problem->mdTutFunction = std::move(mdTutFunction);
    problem->autFunction = std::move(autFunction);

    if(startTime.has_value())
      problem->startTime = startTime.value();
    if(endTime.has_value())
      problem->endTime = endTime.value();
    if(cfl.has_value())
      problem->cfl = cfl.value();
    return problem;
  }

  [[nodiscard]] std::shared_ptr<AcousticPDEProblem> BuildShared() {
    return std::shared_ptr<AcousticPDEProblem>(Build());
  }

  [[nodiscard]] static AcousticPDEProblemBuilder Linear(const MeshesVariant meshes = DefaultMesh{});
  [[nodiscard]] static AcousticPDEProblemBuilder Quadratic(const MeshesVariant meshes = DefaultMesh{});
  [[nodiscard]] static AcousticPDEProblemBuilder CRC(const MeshesVariant meshes = DefaultMesh{});
  [[nodiscard]] static AcousticPDEProblemBuilder RiemannWave2D(const MeshesVariant meshes = DefaultMesh{});
  [[nodiscard]] static AcousticPDEProblemBuilder GaussHatAndRicker2D(const MeshesVariant meshes = DefaultMesh{});
  [[nodiscard]] static AcousticPDEProblemBuilder AcousticWaveProcTest(const MeshesVariant meshes = DefaultMesh{});
};

AcousticProblem *CreateAcousticProblem(const std::string &name);

std::shared_ptr<AcousticProblem> CreateAcousticProblemShared(const std::string &name,
                                                             MeshesVariant meshes = DefaultMesh{});

void CreateProblemShared(const std::string &problemName, std::shared_ptr<AcousticProblem> &ptr);

struct FlexibleAcousticProblemData {
public:
  std::shared_ptr<Meshes> meshes;
  std::shared_ptr<AcousticProblem> problem;
  std::function<double(const Point &)> kappa = nullptr;
  std::function<double(const Point &)> rho = nullptr;
  std::shared_ptr<SeismogramData> sourceData;
  std::function<double(double t, const Cell &c, const Point &)> FPressure =
      nullptr;
  std::string name;
};

class FlexibleAcousticProblem : public AcousticProblem {
public:
  const FlexibleAcousticProblemData &builder;
  FlexibleAcousticProblem(FlexibleAcousticProblemData &builderParam)
      : AcousticProblem(builderParam.meshes, "NO_DEFAULT_MESH", builderParam.problem->nL()),
        builder(builderParam) {}

  double Kappa_i(const Cell &c, const Point &x, int i) const override {
    double kappa = builder.kappa ? builder.kappa(x) : 1.0;
    if (i == 0) {
      return kappa;
    }
    return Tau_p(c, x) * kappa;
  }

  double Rho(const Cell &c, const Point &x) const override {
    return builder.rho ? builder.rho(c()) : 1.0;
  }

  double F(double t, const Cell &c, const Point &x, COMPONENT comp) const override {
    if (comp == COMPONENT::P0 && builder.FPressure) {
      return builder.FPressure(t, c, x);
    }
    return 0.0;
  }

  bool HasRHS() const override { return builder.FPressure != nullptr; }

  std::shared_ptr<SeismogramData> GetSourceData() const override {
    return builder.sourceData ? builder.sourceData : nullptr;
  }

  std::string Name() const override {
    return builder.name.empty() ? "FlexibleAcousticProblem without name"
                                : builder.name;
  }
};

#endif  // MPP_ACOUSTIC_PROBLEMS_H

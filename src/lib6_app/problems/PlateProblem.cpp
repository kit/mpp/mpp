#include "PlateProblem.hpp"

#include <format>

PlateProblem *CreatePlateProblem(const std::string &problemName) {
  if (problemName == "Plate") return new PlateProblem();
  else Exit(std::format("{} not found for SpaceDim={}", problemName, SpaceDimension))
}

std::shared_ptr<PlateProblem> CreatePlateProblemShared(const std::string &problemName) {
  return std::shared_ptr<PlateProblem>(CreatePlateProblem(problemName));
}

void CreateProblemShared(const std::string &problemName, std::shared_ptr<PlateProblem> &ptr) {
  ptr = std::move(CreatePlateProblemShared(problemName));
}
#include "LagrangeEllipticAssemble.hpp"
#include "HybridEllipticAssemble.hpp"
#include "MixedEllipticAssemble.hpp"
#include "DGEllipticAssemble.hpp"
#include "EGEllipticAssemble.hpp"
#include "PDESolver.hpp"

std::unique_ptr<IEllipticAssemble> CreateEllipticAssemble(const IEllipticProblem &problem, const PDESolverConfig &conf) {
  if (conf.modelName.find("Lagrange") != std::string::npos) {

    return std::make_unique<LagrangeEllipticAssemble>(problem, conf.degree);
  }

  if (conf.modelName.find("Mixed") != std::string::npos) {
    return std::make_unique<MixedEllipticAssemble>(problem);
  }

  if (conf.modelName.find("Hybrid") != std::string::npos) {
    return std::make_unique<HybridEllipticAssemble>(problem);
  }

  if (conf.modelName.find("DG") != std::string::npos) {
    return std::make_unique<DGEllipticAssemble>(problem, conf.degree);
  }

  if (conf.modelName.find("EG") != std::string::npos) {
    return std::make_unique<EGEllipticAssemble>(problem, conf.degree);
  }

  Exit(conf.modelName + " not found")
}

std::unique_ptr<IEllipticAssemble>
CreateEllipticAssembleUnique(const IEllipticProblem &problem, const PDESolverConfig &conf) {
  return CreateEllipticAssemble(problem, conf);
}

std::shared_ptr<IEllipticAssemble> CreateEllipticAssembleShared(
    const IEllipticProblem &problem, const PDESolverConfig &conf) {
  return std::shared_ptr<IEllipticAssemble>(CreateEllipticAssemble(problem, conf));
}
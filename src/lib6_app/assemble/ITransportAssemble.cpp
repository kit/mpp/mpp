#include "DGTransportAssemble.hpp"
#include "PDESolver.hpp"

std::unique_ptr<ITransportAssemble> CreateTransportAssemble(const ITransportProblem &problem,
                                                            const PDESolverConfig &conf) {
  return std::make_unique<DGTransportAssemble>(problem, conf.degree);
}

std::shared_ptr<ITransportAssemble> CreateTransportAssembleShared(const ITransportProblem &problem,
                                                                  const PDESolverConfig &conf) {
  return std::shared_ptr<ITransportAssemble>(CreateTransportAssemble(problem, conf));
}

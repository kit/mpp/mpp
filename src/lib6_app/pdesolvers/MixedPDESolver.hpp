#ifndef HYBRIDMAIN_HPP
#define HYBRIDMAIN_HPP

#include "Algebra.hpp"
#include "EllipticProblems.hpp"
#include "HybridEllipticAssemble.hpp"
#include "MixedEllipticAssemble.hpp"
#include "PDESolver.hpp"

struct HybridSolution : Solution {
  explicit HybridSolution(std::shared_ptr<const IDiscretization> disc) :
      Solution(disc), flux(disc) {}

  explicit HybridSolution(std::shared_ptr<const IDiscretization> disc, MeshIndex meshIndex) :
      Solution(disc, meshIndex), flux(disc, meshIndex) {}

  explicit HybridSolution(const Vector &vector, const Vector &flux) :
      Solution(vector), flux(flux) {}

  Vector flux;

  VectorField EvaluateCellFlux(const Cell &c) const;

  double EvaluateNormalFlux(const Cell &c, int face) const;
};

struct MultiLevelHybridSolution : public Solution {
private:
  int baseSpaceLevel;
  std::vector<Vector> solutions{};
public:
  explicit MultiLevelHybridSolution(std::shared_ptr<const IDiscretization> disc,
                                    MeshIndex meshIndex) : Solution(disc, meshIndex) {
    const Meshes &meshes = disc->GetMeshes();
    const int start = meshes.PLevel().space;
    const int end = meshes.FineLevel().space + 1;

    solutions.reserve(end - start);
    for (MeshIndex level = meshes.PLevel(); level.space < end; level = level.NextInSpace()) {
      solutions.push_back(Vector(0.0, disc, level.WithCommSplit(meshIndex.commSplit)));
    }

    this->baseSpaceLevel = start;
  }

  [[nodiscard]]
  const Vector &OnLevel(const MeshIndex &level) const {
    return solutions.at(level.space - baseSpaceLevel);
  }

  [[nodiscard]]
  Vector &OnLevel(const MeshIndex &level) {
    return solutions.at(level.space - baseSpaceLevel);
  }

  [[nodiscard]]
  VectorField EvaluateCellFlux(const MeshIndex &level, const Cell &c) const;

  [[nodiscard]]
  double EvaluateNormalFlux(const MeshIndex &level, const Cell &c, int face) const;
};

class HybridPDESolver : public GenericPDESolver<IEllipticProblem, HybridSolution> {
private:
  int verbose = 1;

  std::shared_ptr<HybridEllipticAssemble> assemble;
protected:
  void run(HybridSolution &solution) const override;

  void plotVtu(const HybridSolution &solution) const override;

  void createAssemble(const IEllipticProblem &problem) override;
public:
  explicit HybridPDESolver(const PDESolverConfig &conf);

  void computeValues(HybridSolution &solution) const override { /* intentionally left blank */ }

  std::shared_ptr<const IDiscretization> GetSharedDisc() const override {
    return assemble->GetSharedDisc();
  }
};

class MixedPDESolver : public GenericPDESolver<IEllipticProblem, MultiLevelHybridSolution> {
private:
  std::shared_ptr<MixedEllipticAssemble> assemble;
protected:
  void run(MultiLevelHybridSolution &solution) const override;

  void plotVtu(const MultiLevelHybridSolution &solution) const override;

  void createAssemble(const IEllipticProblem &problem) override;
public:
  explicit MixedPDESolver(const PDESolverConfig &conf) : GenericPDESolver(conf) {}

  explicit MixedPDESolver() : MixedPDESolver(PDESolverConfig()) {}

  void computeValues(MultiLevelHybridSolution &solution) const override;

  std::shared_ptr<const IDiscretization> GetSharedDisc() const override {
    return assemble->GetSharedDisc();
  }

  std::string Name() const override { return "MixedPDESolver"; };
};

#endif // HYBRIDMAIN_HPP

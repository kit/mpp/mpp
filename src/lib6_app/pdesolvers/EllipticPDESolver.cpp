#include "EllipticPDESolver.hpp"
#include "HybridEllipticAssemble.hpp"
#include "LagrangeEllipticAssemble.hpp"
#include "MixedEllipticAssemble.hpp"

EllipticPDESolver::EllipticPDESolver(const PDESolverConfig &conf) :
    newton(CreateNewton(conf.linearSolver, conf.preconditioner)), GenericPDESolver(conf) {}

void EllipticPDESolver::run(EllipticSolution &solution) const {
  solution.vector.SetAccumulateFlag(true);
  newton->operator()(*assemble, solution.vector);
  solution.converged = newton->converged();
  assemble->SetFlux(solution.vector, solution.flux);
}

void EllipticPDESolver::computeValues(EllipticSolution &solution) const {
  const Vector &u = solution.vector;
  auto &values = solution.values;

  values["L2"] = assemble->L2(u);
  values["H1"] = assemble->H1(u);

  if (assemble->GetProblem().HasExactSolution()) {
    values["L2Error"] = assemble->L2Error(u);
    values["MaxError"] = assemble->MaxError(u);
    values["FaceError"] = assemble->FaceError(u);
    values["EnergyError"] = assemble->EnergyError(u);
    values["L2CellAvgError"] = assemble->L2CellAvgError(u);
  }

  if (conf.normsOnly) return;

  values["Energy"] = assemble->Energy(u);
  values["DoFCount"] = u.size();
  values["LinearSteps"] = newton->GetLinearSolver().GetIteration().Steps();
  FluxPair fluxPair = assemble->InflowOutflow(u);
  FluxPair preFluxPair = assemble->PrescribedInflowOutflow(u);
  values["Inflow"] = fluxPair.first;
  values["Outflow"] = fluxPair.second;
  values["PreInflow"] = preFluxPair.first;
  values["PreOutflow"] = preFluxPair.second;
  values["Goal"] = assemble->GoalFunctional(u);
  values["FluxLoss"] = fluxPair.first + fluxPair.second;
  // FluxError checks neumann bc, not dependent of HasExactSolution
  values["FluxError"] = assemble->FluxError(u);

  if (conf.dualPrimal) values["DualPrimal"] = assemble->DualPrimalError(u);

  if (assemble->GetProblem().Name() == "Rock") {
    FluxPair outflowLeftRight = assemble->OutflowLeftRight(u);
    values["OutflowLeft"] = outflowLeftRight.first;
    values["OutflowRight"] = outflowLeftRight.second;
  }
}

void EllipticPDESolver::plotVtu(const EllipticSolution &solution) const {
#ifdef BUILD_UQ
  if (typeid(*assemble) == typeid(HybridEllipticAssemble)) {
    Exit("Hybrid and Mixed Assemble not supported in UQ");
  }
  if(typeid(*assemble) == typeid(MixedEllipticAssemble)) {
    Exit("Hybrid and Mixed Assemble not supported in UQ");
  }

  const std::string filename = assemble->GetProblem().Name();
  const auto &meshes = assemble->GetProblem().GetMeshes();

  VtuPlot plot(filename);
  auto kappaDisc = std::make_shared<const LagrangeDiscretization>(meshes, 0, 1);
  Vector kappa(0.0, kappaDisc, solution.vector.Level());
  assemble->GetProblem().Permeability(kappa);
  mpp::PlotData("U", std::format("U.{}", filename), solution.vector);
  mpp::PlotData("Flux", std::format("Flux.{}", filename), solution.flux);
  mpp::PlotData("Kappa", std::format("Kappa.{}", filename), kappa);

#else
  const auto &meshes = assemble->GetProblem().GetMeshes();
  const auto fluxDisc = std::make_shared<const LagrangeDiscretization>(meshes, 0, SpaceDimension);
  std::shared_ptr<IDiscretization> pressureDisc;
  // Todo: As soon as ELlipticPDESolver is not used with a mixed assemble this should be removed
  if (typeid(*assemble) == typeid(HybridEllipticAssemble)
      || typeid(*assemble) == typeid(MixedEllipticAssemble)) {
    pressureDisc = std::make_shared<LagrangeDiscretization>(meshes, 0, 1);
  } else {
    pressureDisc = std::make_shared<LagrangeDiscretization>(meshes, 1, 1);
  }
  Vector p(0.0, pressureDisc);
  assemble->SetPressure(solution.vector, p);

  mpp::plot("u") << p << mpp::endp;
  mpp::plot("flux") << solution.flux << mpp::endp;

  auto kappaDisc = std::make_shared<const LagrangeDiscretization>(meshes, 0, 1);
  Vector kappa(0.0, kappaDisc, solution.vector.Level());
  assemble->GetProblem().Permeability(kappa);
  mpp::plot("kappa") << kappa << mpp::endp;

  if (assemble->GetProblem().HasExactSolution()) {
    Vector u_ex(0.0, solution.vector);
    assemble->SetExactSolution(u_ex);
    Vector error = solution.vector - u_ex;

    if (typeid(*assemble) == typeid(HybridEllipticAssemble)) {
      Vector u_ex_plottable(0.0, pressureDisc);
      Vector u_error_plottable(0.0, pressureDisc);
      Vector exactFlux(0.0, fluxDisc);
      auto hybridAssemble = std::dynamic_pointer_cast<HybridEllipticAssemble>(assemble);
      hybridAssemble->SetPressureFlux(u_ex, u_ex_plottable, exactFlux);
      mpp::plot("u_ex") << u_ex_plottable << mpp::endp;
      hybridAssemble->SetPressureFlux(error, u_error_plottable, exactFlux);
      mpp::plot("u_error") << u_error_plottable << mpp::endp;
    } else {
      mpp::plot("u_ex") << u_ex << mpp::endp;
      mpp::plot("u_error") << error << mpp::endp;
    }
  }
#endif
}

void EllipticPDESolver::createAssemble(const IEllipticProblem &problem) {
  assemble = CreateEllipticAssemble(problem, conf);
}

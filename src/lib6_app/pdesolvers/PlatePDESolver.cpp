#include "PlatePDESolver.hpp"
#include <format>

PlatePDESolver::PlatePDESolver(const PDESolverConfig &conf) :
    newton(CreateNewton(conf.linearSolver, conf.preconditioner)), GenericPDESolver(conf) {}

void PlatePDESolver::run(Solution &solution) const {
  solution.vector.SetAccumulateFlag(true);
  newton->operator()(*assemble, solution.vector);
  solution.converged = newton->converged();
  int N = 10;
  Config::Get("eigenvalues", N);
  Vectors U(N, assemble->GetSharedDisc());
  for (int i = 0; i < N; ++i)
    assemble->Initialize(U[i]);
  Matrix A(solution.vector);
  Matrix B(solution.vector, false);
  assemble->Matrices(A, B);
  IEigenSolver *esolver = EigenSolverCreator("LOBPCG").Create();
  Eigenvalues lambda;
  (*esolver)(U, lambda, A, B);
  delete esolver;
  for (int i = 0; i < N; ++i)
    mout << "eigenvalue[" << i << "] = " << lambda[i] << endl;
  for (int i = 0; i < N; ++i) {
    std::string filename = std::format("C.{}", i);
    mpp::plot(filename) << U[i] << mpp::endp;
  }
}

void PlatePDESolver::computeValues(Solution &solution) const {
  Vector &u = solution.vector;
  auto &values = solution.values;

  values["DoFCount"] = u.size();
  values["LinearSteps"] = newton->GetLinearSolver().GetIteration().Steps();
  values["Energy"] = assemble->Energy(u);
  values["max(u)"] = u.Max();
}

void PlatePDESolver::plotVtu(const Solution &solution) const {
  mpp::plot("u") << solution.vector << mpp::endp;
}

void PlatePDESolver::createAssemble(const PlateProblem &problem) {
  assemble = CreatePlateAssembleUnique(problem, conf);
}
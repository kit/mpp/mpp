#ifndef PDESOLVER_HPP
#define PDESOLVER_HPP

#include "IDiscretization.hpp"
#include "IProblem.hpp"
#include "MemoryLogger.hpp"
#include "SeismogramData.hpp"
#include "Vector.hpp"

#include <typeinfo>
#include <utility>

#if BUILD_UQ
// Todo remove
struct PriorSamplerConfig {
  std::string generator = "Normal";
  double mean = 0;
  double variance = 1;

  PriorSamplerConfig();
  PriorSamplerConfig WithGenerator(std::string generator);
  PriorSamplerConfig WithMean(double mean);
  PriorSamplerConfig WithVariance(double stdv);
};

// Todo remove
struct ProposalConfig {
  std::string proposal_type = "RandomWalk";
  double step_length = 0.2;
  PriorSamplerConfig prior_sampler = PriorSamplerConfig();
  PriorSamplerConfig initial_sampler = PriorSamplerConfig().WithVariance(-1);
  std::string field_name = "Simple2DKL";
  bool adaptive = false;

  ProposalConfig();

  ProposalConfig WithStepLength(double step_length);
  ProposalConfig WithProposalType(std::string type);
  ProposalConfig WithPriorConfig(PriorSamplerConfig conf);
  ProposalConfig WithInitialSampler(PriorSamplerConfig conf);
  ProposalConfig WithRandomField(std::string name);
  ProposalConfig WithAdaptive();
};
#endif

// Todo: This one should be leaner
struct PDESolverConfig {
  double T{};

  double dt{};

  // Todo remove level here -> Only use MeshIndex
  int level{};

  // Todo remove pLevel here
  //  -> pLevel can only be set in ProblemDefinition or fitting to PPM->Size
  int pLevel{};

  int degree{};

  int rkorder{};

  // Todo minLevel = equalize to pLevel
  int minLevel{};

  int timeDegree{};

  std::string modelName{};

  std::string problemName{};

  std::string preconditioner = "SuperLU";

  bool dualPrimal = false;

  bool normsOnly = false;

  std::string distribution = "none";

  std::string linearSolver = "GMRES";

  bool usePrevious = false;

  PDESolverConfig(int level, int degree, std::string modelName, std::string problemName) :
      level(level), pLevel(level - 1), degree(degree), modelName(std::move(modelName)),
      problemName(std::move(problemName)) {}

  PDESolverConfig(const std::string &modelName, const std::string &problemName) :
      PDESolverConfig(0, 0, modelName, problemName) {}

  PDESolverConfig() {
    if (!Config::IsInitialized()) return;

    Config::Get("Level", level);
    Config::Get("level", level);
    pLevel = std::max(level - 1, 0);
    Config::Get("plevel", pLevel);

    Config::Get("degree", degree);

    Config::Get("Model", modelName);
    Config::Get("MinLevel", minLevel);
    Config::Get("Problem", problemName);

    Config::Get("Preconditioner", preconditioner);
    Config::Get("time_degree", timeDegree);
    Config::Get("Distribution", distribution);

    Config::Get("T", T);
    Config::Get("dt", dt);

    Config::Get("rkorder", rkorder);

    Config::Get("DualPrimal", dualPrimal);
    Config::Get("NormsOnly", normsOnly);
    Config::Get("LinearSolver", linearSolver);

    Config::Get("UsePrevious", usePrevious);
  }

  PDESolverConfig WithLevel(int _level) {
    pLevel = (preconditioner == "Multigrid") ? minLevel : _level - 1;
    this->level = _level;
    return *this;
  }

  PDESolverConfig WithLinearSolver(std::string _linearSolver) {
    this->linearSolver = std::move(_linearSolver);
    return *this;
  }

  PDESolverConfig WithPreconditioner(std::string _preconditioner) {
    this->preconditioner = _preconditioner;
    return *this;
  }

  PDESolverConfig WithPLevel(int _plevel) {
    this->pLevel = _plevel;
    return *this;
  }

  PDESolverConfig WithDegree(const DegreePair &deg) {
    this->degree = deg.space;
    this->timeDegree = deg.time;
    return *this;
  }

  PDESolverConfig WithDegree(int _degree) {
    this->degree = _degree;
    return *this;
  }

  PDESolverConfig WithDualPrimal(bool dualPrimal) {
    this->dualPrimal = dualPrimal;
    return *this;
  }

  PDESolverConfig WithMinLevel(int minimumLevel) {
    minLevel = minimumLevel;
    return *this;
  }

  PDESolverConfig WithModel(std::string model) {
    modelName = std::move(model);
    return *this;
  }

  PDESolverConfig WithProblem(std::string problem) {
    problemName = std::move(problem);
    return *this;
  }

  PDESolverConfig WithNormsOnly(bool onlyNorms) {
    normsOnly = std::move(onlyNorms);
    return *this;
  }


  PDESolverConfig WithRkOrder(int rkorder) {
    this->rkorder = rkorder;
    return *this;
  }

#if BUILD_UQ
  // Todo remove again
  ProposalConfig proposal_config = ProposalConfig();

  std::vector<std::pair<std::string, std::vector<double>>> observations = {{"Point", {0.5, 0.5}}};

  PDESolverConfig WithObservations(std::vector<std::pair<std::string, std::vector<double>>> obs) {
    this->observations = obs;
    return *this;
  }

  PDESolverConfig WithProposalConfig(ProposalConfig conf) {
    this->proposal_config = conf;
    return *this;
  }
#endif
};

typedef std::map<std::string, double> ValueMap;
typedef std::map<std::string, std::vector<double>> MultiValueMap;

struct Solution {
  explicit Solution(std::shared_ptr<const IDiscretization> disc) :
      vector(Vector(0.0, std::move(disc))) {}

  explicit Solution(std::shared_ptr<const IDiscretization> disc, MeshIndex meshIndex) :
      vector(Vector(0.0, std::move(disc), meshIndex)) {}

  explicit Solution(const Vector &vector) : vector(Vector(vector)) {}

  double computingTime{};

  double totalTime{};

  double initTime{};

  std::string name{};

  ValueMap values{};

  // TODO: find a better way to store time-dependent data or maybe this is fine?
  MultiValueMap multiValues{};

  bool converged{};

  Vector vector;

  std::shared_ptr<SeismogramData> seismograms;

  int verbose = 0;

  void PrintInfo() {
    // TODO this needs to be different in each SolutionClass, setting specific verbosity for each
    // entry
    std::vector<PrintInfoEntry<double>> entries;
    for (auto &[key, value] : values) {
      entries.emplace_back(key, value, verbose = 1);
    }
    mout.PrintInfo("Solution", verbose, entries);

    if (multiValues.empty()) return;

    // TODO: Discuss what to do with solver-specific values?
    // TODO: for now, disable multiValues (aka time-dependent values and others like
    // solver-specific)
    return;
    // TODO: Here we want to print for each timesteps every value.
    // datastructure is needed in order to guarantee all values are initialized.
    // map<std::string,vector<double>> may not hold enough doubles for each key-string
    // std::vector<map<std::string,double>> may not hold all key-strings for each timestep.

    for (int n = 0; n < multiValues.begin()->second.size(); ++n) {
      std::vector<PrintIterEntry<double>> timestep_entries;
      for (auto &[key, valueList] : multiValues) {
        timestep_entries.emplace_back(key, valueList[n], 11, verbose = 1);
      }
      mout.PrintIteration(verbose, timestep_entries);
    }
  }
};

template<typename ProblemType, typename SolutionType>
class GenericPDESolver {
public:
  using ProblemClass = ProblemType;
  using SolutionClass = SolutionType;
protected:
  int verbose = 1;

  int plotting = 0;

  PDESolverConfig conf;

  virtual void run(SolutionType &solution) const = 0;

  virtual void plotVtu(const SolutionType &solution) const {};

  virtual void computeValues(SolutionType &solution) const = 0;

  virtual void createAssemble(const ProblemType &problem) = 0;

  virtual void computeAdjointValues(SolutionType &solution) const {
    THROW("implement computeAdjointValues in " + Name())
  }

  virtual void runAdjoint(SolutionType &solution) const {
    THROW("implement runAdjoint in " + Name())
  }

  virtual void createAdjointAssemble(std::shared_ptr<ProblemType> problem) {
    THROW("implement createAdjointAssemble in " + Name())
  };
public:
  explicit GenericPDESolver(PDESolverConfig conf) : conf(std::move(conf)) {
    Config::Get("PDESolverVerbose", verbose);
    Config::Get("VtuPlot", plotting);
  }

  // Todo remove this one, or discuss other usage.
  //    Maybe Problem should be solved on all Meshes passed with Problem,
  //    and then gives back a Solution Collection?
  //
  SolutionType Run(std::shared_ptr<ProblemType> problem) {
    MeshIndex level;
    if (problem->IsMeshInitialized()) {
      level = problem->GetMeshes().FineLevel();
    } else {
      level = {conf.level, conf.level, 0, 0};
    }
    return Run(problem, level);
  }

  // Todo, only use this one
  // Todo, make problem const&
  SolutionType Run(std::shared_ptr<ProblemType> problem, MeshIndex meshIndex) {
    mout.StartBlock(Name());
    vout(1) << "Solve Problem " << problem->Name() << endl;

    double start = MPI_Wtime();
    createAssemble(*problem);
    SolutionType solution(GetSharedDisc(), meshIndex);
    solution.initTime = (MPI_Wtime() - start);

    if (verbose > 1) problem->GetMeshes().PrintInfo(meshIndex);

    double startSolve = MPI_Wtime();
    run(solution);
    solution.computingTime = (MPI_Wtime() - startSolve);

    computeValues(solution);
    if (plotting) plotVtu(solution);

    solution.totalTime = (MPI_Wtime() - start);

    mout.EndBlock(verbose == 0);
    return solution;
  }

  SolutionType RunAdjoint(std::shared_ptr<ProblemType> problem, MeshIndex meshIndex) {
    mout.StartBlock(Name());
    vout(1) << "Solve AdjointProblem " << problem->Name() << endl;

    double start = MPI_Wtime();
    createAdjointAssemble(std::move(problem));
    SolutionType solution(GetSharedDisc(), meshIndex);
    solution.initTime = (MPI_Wtime() - start);

    runAdjoint(solution);
    if (plotting) plotVtu(solution);
    computeAdjointValues(solution);
    solution.totalTime = (MPI_Wtime() - start);
    mout.EndBlock(verbose == 0);
    return solution;
  }

  virtual std::string Name() const { return "PDESolver"; };

  virtual ~GenericPDESolver() = default;

  const PDESolverConfig &GetConfig() const { return conf; }

  virtual std::shared_ptr<const IDiscretization> GetSharedDisc() const = 0;
};

template<typename Problem>
using PDESolver = GenericPDESolver<Problem, Solution>;

#endif // PDESOLVER_HPP

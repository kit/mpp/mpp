#include "ReactionPDESolver.hpp"
#include "GMRES.hpp"
#include "Newton.hpp"
#include "TimeIntegrator.hpp"

void ReactionPDESolver::run(Solution &solution) const {
  //  assemble->SetTimeSeries(solution.vector);

  if (typeid(*assemble) == typeid(PGReactionAssemble)) {
    auto timeIntegrator =
        TimeIntegratorCreator(IMPLICIT_EULER)
            .WithNonLinearSolver(new Newton(std::make_unique<GMRES>(GetPC("SuperLU"))))
            .CreateNonLinearTimeIntegratorUnique();
    solution.converged = timeIntegrator->Method(*assemble, solution.vector);
  }

  if (typeid(*assemble) == typeid(DGReactionAssemble)) {
    auto timeIntegrator =
        TimeIntegratorCreator(IMPLICIT_EULER)
            .WithNonLinearSolver(new Newton(std::make_unique<GMRES>(GetPC("PointBlockJacobi"))))
            .CreateNonLinearTimeIntegratorUnique();
    solution.converged = timeIntegrator->Method(*assemble, solution.vector);
  }
}

void ReactionPDESolver::computeValues(Solution &solution) const {
  solution.values["Energy"] = assemble->Energy(solution.vector);
  solution.values["Mass"] = assemble->Mass(solution.vector);

  RatePair fluxPair = assemble->InflowOutflow(solution.vector);
  solution.values["Inflow"] = fluxPair.first;
  solution.values["Outflow"] = fluxPair.second;


  if (!assemble->GetProblem().HasExactSolution()) return;
}

void ReactionPDESolver::createAssemble(const IReactionProblem &problem) {
  if (conf.modelName == "PGReaction")
    assemble = std::make_unique<PGReactionAssemble>(problem, conf.degree);

  if (conf.modelName == "DGReaction")
    assemble = std::make_unique<DGReactionAssemble>(problem, conf.degree);
}

#include "AcousticPDESolver.hpp"
#include "AcousticProblems.hpp"
#include "LinearSolver.hpp"

AcousticPDESolver::AcousticPDESolver(const PDESolverConfig &conf) : GenericPDESolver(conf) {
  timeIntegrator = TimeIntegratorCreator().
      WithLinearSolver(GetLinearSolver(GetPC())).
      WithLinearSolver(GetLinearSolver(GetPC())).
      WithLinearSolver(GetLinearSolver(GetPC())).
      WithLinearSolver(GetLinearSolver(GetPC())).
      WithRkOrder(conf.rkorder).
      CreateUnique();
}

void AcousticPDESolver::run(Solution &solution) const {
  assemble->SetTimeSeries(solution.vector);
  solution.converged = timeIntegrator->Method(assemble.get(), solution.vector);
}

void AcousticPDESolver::computeValues(Solution &solution) const {

  // TODO: which energy should be used? norms.energy or assemble->Energy

  auto norms = assemble->ComputeNorms(solution.vector);

  solution.values["Energy"] = assemble->Energy(solution.vector);
  solution.values["L1"] = norms.l1;
  solution.values["L2"] = norms.l2;
  solution.values["MinP"] = assemble->MinMaxPressure(solution.vector).first;
  solution.values["MaxP"] = assemble->MinMaxPressure(solution.vector).second;

  Vector tmp(solution.vector);
  tmp.MakeAdditive();
  solution.values["NormCoefficients"] = tmp.norm();

  if (!assemble->GetProblem().HasExactSolution()) return;

  auto errors = assemble->ComputeErrors(solution.vector);

  solution.values["L1Error"] = errors.l1;
  solution.values["L2Error"] = errors.l2;
  solution.values["MaxError"] = errors.inf;
}

void AcousticPDESolver::createAssemble(const AcousticProblem &problem) {
  assemble = CreateAcousticAssembleUnique(problem, conf);
}
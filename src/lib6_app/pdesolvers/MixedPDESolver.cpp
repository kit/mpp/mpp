#include "MixedPDESolver.hpp"
#include "HybridEllipticAssemble.hpp"
#include "LagrangeDiscretization.hpp"
#include "MeshesCreator.hpp"
#include "MixedEllipticAssemble.hpp"
#include "Newton.hpp"
#include "PDESolver.hpp"

template<typename Assemble>
void plot(std::shared_ptr<Assemble> assemble, const HybridSolution &solution) {
  const auto &meshes = assemble->GetProblem().GetMeshes();
  const auto fluxDisc = std::make_shared<const LagrangeDiscretization>(meshes, 0, SpaceDimension);
  const auto uDisc = std::make_shared<const LagrangeDiscretization>(meshes, 0, 1);
  Vector flux(0.0, fluxDisc);
  Vector p(0.0, uDisc);
  assemble->SetPressureFlux(solution.vector, p, flux);

  mpp::plot("u") << p << mpp::endp;
  mpp::plot("flux") << flux << mpp::endp;
}

std::shared_ptr<IEllipticProblem> setupProblem(const std::string &problemName) {
  auto problem = CreateEllipticProblemShared(problemName);
  return problem;
}

HybridPDESolver::HybridPDESolver(const PDESolverConfig &conf) : GenericPDESolver(conf) {
  Config::Get("HybridVerbose", verbose);
}

void HybridPDESolver::run(HybridSolution &solution) const {
  NewtonMethod(*assemble, solution.vector, true);
  assemble->SetNormalFlux(solution.vector, solution.flux);
}

void HybridPDESolver::plotVtu(const HybridSolution &solution) const {
  const auto &meshes = assemble->GetProblem().GetMeshes();
  const auto fluxDisc = std::make_shared<const LagrangeDiscretization>(meshes, 0, SpaceDimension);
  const auto uDisc = std::make_shared<const LagrangeDiscretization>(meshes, 0, 1);
  Vector flux(0.0, fluxDisc);
  Vector p(0.0, uDisc);
  assemble->SetPressureFlux(solution.vector, p, flux);
  mpp::plot("u") << p << mpp::endp;
  mpp::plot("flux") << flux << mpp::endp;
}

void HybridPDESolver::createAssemble(const IEllipticProblem &problem) {
  assemble = std::make_shared<HybridEllipticAssemble>(problem);
}

VectorField EvaluateCellFluxInner(const Vector &flux, const Cell &c) {
  if constexpr (DebugLevel > 0) {
    if (flux.find_cell(c()) == flux.cells_end()) {
      mout << PPM->Proc() << " cell " << c() << "not found" << endl;
      THROW("HybridSolution::EvaluateCellFlux: cell not found.")
    }
  }

  RTLagrangeElementT<> elem(flux, c);

  VectorField F = zero;
  double area = 0;
  for (int q = 0; q < elem.nQ(); ++q) {
    double w = elem.QWeight(q);
    area += w;
    F += w * elem.VelocityField(q, flux);
  }
  F *= (1 / area);
  return F;
}

VectorField HybridSolution::EvaluateCellFlux(const Cell &c) const {
  return EvaluateCellFluxInner(flux, c);
}

VectorField MultiLevelHybridSolution::EvaluateCellFlux(const MeshIndex &level,
                                                       const Cell &c) const {
  return EvaluateCellFluxInner(this->OnLevel(level), c.SpaceCell());
}

double EvaluateNormalFluxInner(const Vector &flux, const Cell &c, int i) {
  if constexpr (DebugLevel > 0) {
    if (flux.find_cell(c()) == flux.cells_end()) {
      mout << PPM->Proc() << " cell " << c() << "not found" << endl;
      THROW("HybridSolution::EvaluateNormalFlux: cell not found.")
    }
  }

  RTLagrangeElementT<> elem(flux, c);
  RTFaceElementT<> faceElem(flux, c, i);
  return flux(elem[i], 0) * elem.Sign(i) / faceElem.Area();
}

double HybridSolution::EvaluateNormalFlux(const Cell &c, int i) const {
  return EvaluateNormalFluxInner(flux, c, i);
}

double MultiLevelHybridSolution::EvaluateNormalFlux(const MeshIndex &level, const Cell &c,
                                                    int i) const {
  return EvaluateNormalFluxInner(this->OnLevel(level), c.SpaceCell(), i);
}

void MixedPDESolver::run(MultiLevelHybridSolution &solution) const {
  const Meshes &meshes = assemble->GetSharedDisc()->GetMeshes();
  for (MeshIndex index = meshes.PLevel(); index != meshes.FineLevel().NextInSpace();
       index = index.NextInSpace()) {
    auto &solutionOnLevel = solution.OnLevel(index);
    NewtonMethod(*assemble, solutionOnLevel, true);
  }
}

void MixedPDESolver::plotVtu(const MultiLevelHybridSolution &solutions) const {
  const auto &solution = solutions.OnLevel(assemble->GetSharedDisc()->GetMeshes().FineLevel());
  const Meshes &meshes = assemble->GetSharedDisc()->GetMeshes();
  auto fluxDisc = std::make_shared<const LagrangeDiscretization>(meshes, 0, 3);
  auto uDisc = std::make_shared<const LagrangeDiscretization>(meshes, 0, 1);
  Vector flux(0.0, fluxDisc);
  Vector kappa(0.0, uDisc);
  Vector p(0.0, uDisc);
  assemble->SetPressureFlux(solution, p, flux);
  assemble->GetProblem().Permeability(kappa);
#ifdef BUILD_UQ
  std::string filename = assemble->GetProblem().Name();
  VtuPlot plot(filename);
  plot.AddData("Kappa", kappa);
  plot.AddData("Flux", flux);
  plot.AddData("Pressure", p);
  plot.PlotFile();
#else
  mpp::plot("Flux") << flux << mpp::endp;
  mpp::plot("P") << p << mpp::endp;
#endif
}

void MixedPDESolver::createAssemble(const IEllipticProblem &problem) {
  assemble = std::make_shared<MixedEllipticAssemble>(problem);
}

void MixedPDESolver::computeValues(MultiLevelHybridSolution &solution) const {
  const Vector &u = solution.vector;
  auto &values = solution.values;
  values["L2"] = assemble->L2(u);
  values["FluxError"] = assemble->FluxError(u);
}

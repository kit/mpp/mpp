#include "TransportPDESolver.hpp"
#include "GMRES.hpp"

TransportPDESolver::TransportPDESolver(const PDESolverConfig &conf) :
    timeIntegrator(TimeIntegratorCreator().
        WithLinearSolver(new GMRES(GetPC("PointBlockJacobi"))).
        WithLinearSolver(new GMRES(GetPC("PointBlockJacobi"))).
        WithLinearSolver(new GMRES(GetPC("PointBlockJacobi"))).
        WithLinearSolver(new GMRES(GetPC("PointBlockJacobi"))).
        WithConfigTypeEntry().WithRkOrder(conf.rkorder).
        CreateUnique()),
    GenericPDESolver(conf) {}


void TransportPDESolver::run(Solution &solution) const {
  assemble->SetTimeSeries(solution.vector);
  solution.converged = timeIntegrator->Method(assemble.get(), solution.vector);
}

void TransportPDESolver::computeValues(Solution &solution) const {
  // These values will only be computed for the last timestep.
  // TODO: Get values from TimeIntegrator!
  Vector &u = solution.vector;
  auto &values = solution.values;
  RatePair fluxPair = assemble->InflowOutflow();
  values["Energy"] = assemble->Energy(u);
  values["Mass"] = assemble->Mass(u);
  values["L1"] = norms::L1(u);
  values["Inflow"] = fluxPair.first;
  values["Outflow"] = fluxPair.second;
  values["L2"] = assemble->L2(u);
  values["Flux"] = assemble->MaxFlux(u);
  if (!assemble->GetProblem().HasExactSolution()) return;
  values["Error"] = assemble->Error(u);
}

void TransportPDESolver::createAssemble(const ITransportProblem &problem) {
  assemble = CreateTransportAssemble(problem, conf);
}


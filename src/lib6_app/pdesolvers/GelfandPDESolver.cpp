#include "GelfandPDESolver.hpp"

GelfandPDESolver::GelfandPDESolver(const PDESolverConfig &conf) :
    newton(CreateNewton(conf.linearSolver, conf.preconditioner)), GenericPDESolver(conf) {
      newton->PrintInfo();
    }

void GelfandPDESolver::run(Solution &solution) const {
  solution.vector.SetAccumulateFlag(true);
  assemble->Initialize(solution.vector);
  newton->operator()(*assemble, solution.vector);
  solution.converged = newton->converged();
}

void GelfandPDESolver::computeValues(Solution &solution) const {
  Vector &u = solution.vector;
  auto &values = solution.values;

  values["DoFCount"] = u.size();
  values["LinearSteps"] = newton->GetLinearSolver().GetIteration().Steps();
  values["Energy"] = assemble->Energy(u);
}

void GelfandPDESolver::plotVtu(const Solution &solution) const {
  mpp::plot("u") << solution.vector << mpp::endp;
}

void GelfandPDESolver::createAssemble(const GelfandProblem &problem) {
  assemble = CreateGelfandAssembleUnique(problem, conf);
}
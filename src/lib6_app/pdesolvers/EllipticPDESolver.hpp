#ifndef ELLIPTICPDESOLVER_HPP
#define ELLIPTICPDESOLVER_HPP

#include "EllipticProblems.hpp"
#include "IEllipticAssemble.hpp"
#include "LagrangeDiscretization.hpp"
#include "Newton.hpp"
#include "PDESolver.hpp"

struct EllipticSolution : Solution {
  explicit EllipticSolution(std::shared_ptr<const IDiscretization> disc) :
      Solution(disc), flux(disc) {}

  explicit EllipticSolution(std::shared_ptr<const IDiscretization> disc, MeshIndex meshIndex) :
      Solution(disc, meshIndex),
      flux(std::make_shared<LagrangeDiscretization>(disc->GetMeshes(), 0, SpaceDimension),
           meshIndex) {}

  explicit EllipticSolution(const Vector &vector, const Vector &flux) :
      Solution(vector), flux(flux) {}

  Vector flux;
};

class EllipticPDESolver : public GenericPDESolver<IEllipticProblem, EllipticSolution> {
protected:
  std::shared_ptr<Newton> newton;

  std::shared_ptr<IEllipticAssemble> assemble;

  void run(EllipticSolution &solution) const override;

  void plotVtu(const EllipticSolution &solution) const override;

  void computeValues(EllipticSolution &solution) const override;

  void createAssemble(const IEllipticProblem &problem) override;
public:
  explicit EllipticPDESolver(const PDESolverConfig &conf);

  std::string Name() const override { return "EllipticPDESolver"; }

  std::shared_ptr<const IDiscretization> GetSharedDisc() const override {
    return assemble->GetSharedDisc();
  }
};

#endif //ELLIPTICPDESOLVER_HPP

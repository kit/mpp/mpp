#ifndef TRANSPORTPDESOLVER_HPP
#define TRANSPORTPDESOLVER_HPP

#include "TransportProblems.hpp"
#include "ITransportAssemble.hpp"
#include "PDESolver.hpp"
#include "TimeIntegrator.hpp"

class TransportPDESolver : public PDESolver<ITransportProblem> {
private:
  std::shared_ptr<ITransportAssemble> assemble;

  std::unique_ptr<TimeIntegrator> timeIntegrator;

protected:
  void run(Solution &solution) const override;

  void computeValues(Solution &solution) const override;

  void createAssemble(const ITransportProblem &problem) override;
public:
  explicit TransportPDESolver(const PDESolverConfig &conf);

  TransportPDESolver() : TransportPDESolver(PDESolverConfig{}) {}

  std::string Name() const override { return "TransportPDESolver"; }

  std::shared_ptr<const IDiscretization> GetSharedDisc() const override {
    return assemble->GetSharedDisc();
  }
};

#endif //TRANSPORTPDESOLVER_HPP

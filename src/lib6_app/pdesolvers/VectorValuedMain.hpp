#ifndef TUTORIAL_VECTORVALUEDMAIN_HPP
#define TUTORIAL_VECTORVALUEDMAIN_HPP

#include <utility>

#include "IVectorValuedAssemble.hpp"
#include "EllipticProblems.hpp"
#include "Newton.hpp"
#include "PDESolver.hpp"

class VectorValuedMain : public PDESolver<IVectorValuedProblem> {
private:
  std::shared_ptr<Newton> newton; // Todo replace with linear solver

  std::shared_ptr<IVectorValuedAssemble> assemble;
protected:
  void run(Solution &solution) const override;

  void computeValues(Solution &solution) const override;

  void createAssemble(const IVectorValuedProblem &problem) override;
public:
  explicit VectorValuedMain(const PDESolverConfig &conf) : GenericPDESolver(conf),
  newton(CreateNewton(conf.linearSolver, conf.preconditioner))  {

  }

  double EvaluateQuantity(const Vector &u, const std::string &quantity) const;

  std::shared_ptr<const IDiscretization> GetSharedDisc() const override {
    return assemble->GetSharedDisc();
  }
};


#endif //TUTORIAL_VECTORVALUEDMAIN_HPP

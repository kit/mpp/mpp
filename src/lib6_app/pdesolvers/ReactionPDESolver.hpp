#ifndef REACTIONPDESOLVER_HPP
#define REACTIONPDESOLVER_HPP

#include "ReactionProblems.hpp"
#include "DGReactionAssemble.hpp"
#include "PGReactionAssemble.hpp"

#include "PDESolver.hpp"
#include "Newton.hpp"

class ReactionPDESolver : public PDESolver<IReactionProblem> {
private:
  std::shared_ptr<IReactionAssemble> assemble;
protected:
  void run(Solution &solution) const override;

  void computeValues(Solution &solution) const override;

  void createAssemble(const IReactionProblem &problem) override;
public:
  explicit ReactionPDESolver(const PDESolverConfig &conf) : GenericPDESolver(conf) {
  }

  std::string Name() const override { return "ParabolicPDESolver"; }

  std::shared_ptr<const IDiscretization> GetSharedDisc() const override {
    return assemble->GetSharedDisc();
  }
};

#endif //REACTIONPDESOLVER_HPP

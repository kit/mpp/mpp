#ifndef __CALCULATION_INFO__
#define __CALCULATION_INFO__

#include "IterableSystem.hpp"

struct ElementWeightCacheable : public AllocationHolder {
  std::vector<std::tuple<size_t, Point, double>> quadraturePointWeight;
};

inline void initQuadraturePoints(ElementWeightCacheable &holder, size_t quadratureSize,
                                 std::invocable<size_t> auto pointFunction,
                                 std::invocable<size_t> auto weightFunction, bool sorted = false) {
  holder.quadraturePointWeight.resize(quadratureSize);
  for (size_t quadratureIndex : std::views::iota((size_t)0, quadratureSize)) {
    auto &quad = holder.quadraturePointWeight[quadratureIndex];
    std::get<0>(quad) = quadratureIndex;
    std::get<1>(quad) = pointFunction(quadratureIndex);
    std::get<2>(quad) = weightFunction(quadratureIndex);
  }
  if (sorted) {
    std::ranges::sort(holder.quadraturePointWeight, [](auto &first, auto &second) {
      return std::get<1>(first) < std::get<1>(second);
    });
  }
}

inline std::invocable<size_t> auto elementPoints(const SpaceTimeQuadrature &stQuad,
                                                 const Cell &cell) {
  const auto &qPoints = stQuad.QPoints();
  return [&](size_t quadIndex) { return cell.LocalToGlobal(qPoints[quadIndex]); };
}

inline std::invocable<size_t> auto elementWeight(const SpaceTimeQuadrature &stQuad,
                                                 const Cell &cell) {
  const auto determinant = cell.GetTransformation().Det();
  const auto &weights = stQuad.Weights();
  return [&, determinant](size_t quadIndex) { return weights[quadIndex] * determinant; };
}

inline std::invocable<size_t> auto face1Points(const std::vector<Point> &facePoints,
                                               const Quadrature &timeQuad, const Cell &cell) {
  const auto quadratureSize = facePoints.size();
  return [&, quadratureSize](size_t quadIndex) {
    const size_t qSpaceI = quadIndex % quadratureSize;
    const size_t qTimeI = quadIndex / quadratureSize;
    return cell.LocalToGlobal(facePoints[qSpaceI].CopyWithT(timeQuad.QPoint(qTimeI)[0]));
  };
}

inline std::invocable<size_t> auto face1Weight(const std::vector<Point> &facePoints,
                                               const Quadrature &spaceQuad, const size_t faceID,
                                               const Quadrature &timeQuad, const Cell &cell) {
  const auto &spaceCell = cell.SpaceCell();
  const Transformation &transform = spaceCell.GetTransformation(facePoints[0]);
  VectorField quadratureNormal = transform * spaceCell.LocalFaceNormal(faceID);
  const double timeDet = cell.max() - cell.min();
  const auto weightModifier =
      spaceCell.LocalFaceArea(faceID) * norm(quadratureNormal) * timeDet * transform.Det();
  const auto quadratureSize = facePoints.size();
  return [&, weightModifier, quadratureSize](size_t quadIndex) {
    const size_t qSpaceI = quadIndex % quadratureSize;
    const size_t qTimeI = quadIndex / quadratureSize;
    return spaceQuad.Weight(qSpaceI) * timeQuad.Weight(qTimeI) * weightModifier;
  };
}

inline std::invocable<size_t> auto face2Points(const size_t faceID, const VectorMatrixBase &matrix,
                                               const Quadrature &spaceQuad, const Cell &cell) {
  static const Quadrature &timeQuad = GetQuadrature("Qint1");
  const auto &face = matrix.find_face(cell.Face(faceID));
  const auto quadratureSize = spaceQuad.size();
  const auto time = face().t();
  const auto &spaceCell = cell.SpaceCell();
  return [&, quadratureSize, time](size_t quadIndex) {
    const size_t qSpaceI = quadIndex % quadratureSize;
    const size_t qTimeI = quadIndex / quadratureSize;
    return spaceCell.LocalToGlobal(spaceQuad.QPoint(qSpaceI)).CopyWithT(time);
  };
}

inline std::invocable<size_t> auto face2Weight(const std::vector<Point> &facePoints,
                                               const Quadrature &spaceQuad, const size_t faceID,
                                               const Cell &cell) {
  static const Quadrature &timeQuad = GetQuadrature("Qint1");
  const auto quadratureSize = spaceQuad.size();
  const auto &spaceCell = cell.SpaceCell();
  const double weightModifier = spaceCell.GetTransformation(spaceQuad.QPoint(0)).Det();
  return [&, weightModifier, quadratureSize](size_t quadIndex) {
    const size_t qSpaceI = quadIndex % quadratureSize;
    const size_t qTimeI = quadIndex / quadratureSize;
    return spaceQuad.Weight(qSpaceI) * weightModifier;
  };
}

struct CalculationInfo : public ElementWeightCacheable {
  const Point &point;
  Matrix &matrix;
  const AcousticProblem &problem;
  const double rho;
  Vector &vector;
  const Cell &cellIterator;
  std::vector<double> kappaInverse;
  std::vector<double> kappaTauInverse;
  STDiscretization &discretization;
  std::function<double(Point)> weightFunction;
  DGRowEntries entries;

  CalculationInfo(const Point &point, Matrix &matrix, const AcousticProblem &problem,
                  Vector &vector, const Cell &cellIterator,
                  std::function<double(Point)> weightFunction, STDiscretization &discretization,
                  DGRowEntries &rowEntries, const AllocationHelper &helper,
                  const SpaceTimeQuadrature &quadrature) :
      ElementWeightCacheable{helper, rowEntries.a, rowEntries.nf, problem.nL()}, point(point),
      matrix(matrix), problem(problem), rho(problem.Rho(cellIterator, point)), vector(vector),
      cellIterator(cellIterator), kappaInverse(2 + problem.nL()), kappaTauInverse(2 + problem.nL()),
      discretization(discretization), weightFunction(weightFunction), entries(rowEntries) {
    initQuadraturePoints(*this, quadrature.size(), elementPoints(quadrature, cellIterator),
                         elementWeight(quadrature, cellIterator));
    kappaInverse[0] = 0.0;
    kappaTauInverse[0] = kappaTauInverse[1] = 0.0;
    kappaInverse[1] = 1.0 / problem.Kappa_i(cellIterator, point, 0);
    for (int j = 2; j < 2 + problem.nL(); j++) {
      kappaInverse[j] = 1.0 / problem.Kappa_i(cellIterator, point, j - 1);
      kappaTauInverse[j] = kappaInverse[j] / problem.Tau_i(cellIterator, point, j - 1);
    }
  }
};

inline void initFluxCache(FluxCache &fluxCache, const ElementWeightCacheable &quadrature,
                          const AllocationHolder &holder, const VectorField &quadratureNormal) {
  const auto quadratureSize = quadrature.helper.generatedWith.quadratur;
  const auto shapeSize = holder.helper.generatedWith.shape;
  fluxCache.helper.generatedWith = {.quadratur = quadratureSize, .shape = shapeSize};
  fluxCache.outputPointer = quadrature.outputPointer;
  const IterableOf<size_t> shapeSizeSpaceDimIterator{0, shapeSize * holder.helper.spaceDimension};
  fluxCache.helper.fluxCache.resize(shapeSizeSpaceDimIterator.end() * quadratureSize);
  fluxCache.helper.dimension.resize(fluxCache.helper.fluxCache.size());
  fluxCache.rowSize = quadrature.rowSize;
  for (const auto &[index, point, weight] : quadrature.quadraturePointWeight) {
    // double VFlux_c_Vi = VFlux_c(V_i, Nq);
    const auto quadOffset = index * shapeSizeSpaceDimIterator.end();
    for (const auto [i, velocity, dimension] :
         velocityIterableWithDim(index, holder, shapeSizeSpaceDimIterator)) {
      fluxCache.helper.fluxCache[i + quadOffset] = quadratureNormal[dimension] * velocity;
      fluxCache.helper.dimension[i + quadOffset] = dimension;
    }
  }
  fluxCache.helper.flux = {fluxCache.helper.fluxCache.data(), holder.helper.spaceDimension,
                           holder.helper.spaceDimension, 0};
}

[[nodiscard]]
inline size_t FindNeighbourFaceID(const Point &facePoint, const Cell &cell) {
  for (size_t foundFaceID = 0; foundFaceID < cell.Faces(); ++foundFaceID) {
    if (facePoint == cell.Face(foundFaceID)) return foundFaceID;
  }
  ERROR("Mesh::FindNeighbourFaceID Face not found!");
}

struct CalculateFaceInfo {
  bool isOnBoundary;
  Point facePoint;
  int boundaryID;
  const Cell &faceCellIterator;
  Point pointForFaceCell;
  int neighbourFaceID;
  double alpha[4];
  double z_K;

  CalculateFaceInfo(CalculationInfo &info, const size_t faceID, const Mesh &mesh,
                    const double z_K) :
      isOnBoundary(info.matrix.OnBoundary(info.cellIterator, faceID)),
      facePoint(info.cellIterator.Face(faceID)),
      boundaryID(isOnBoundary ? info.problem.BndID(facePoint) : -1),
      faceCellIterator(isOnBoundary ? info.cellIterator :
                                      info.matrix.FindNeighbourCell(info.cellIterator, faceID)),
      pointForFaceCell(faceCellIterator()),
      neighbourFaceID(FindNeighbourFaceID(facePoint, faceCellIterator)), z_K(z_K) {
    const double z_Kf = sqrt(info.problem.Rho(faceCellIterator, pointForFaceCell)
                             * info.problem.Kappa(faceCellIterator, pointForFaceCell));
    const auto alpha1 = 1.0 / (z_K + z_Kf);
    alpha[0] = alpha1;
    alpha[1] = z_Kf * z_K * alpha1;
    alpha[2] = z_Kf * alpha1;
    alpha[3] = z_K * alpha1;
  }
};

#endif
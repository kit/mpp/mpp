#include "CalculationInfo.hpp"
#include "IterableSystem.hpp"
#include "STMFViscoAcousticElement.hpp"

inline auto generateBasic(const GenerationInputInfo &generationInfo, const size_t dampingCount,
                          const size_t spaceDimension, std::span<GenerateInfo> inputs) {
  AllocationHelper helper;
  helper.spaceDimension = spaceDimension;
  const size_t allocationSize = generationInfo.quadratur * generationInfo.shape;

  const uint8_t amount = (uint8_t)std::max(dampingCount, spaceDimension);
  inputs[0].multiple = amount;

  helper.allocate(generationInfo, inputs);

  helper.dimension.resize(amount * allocationSize);
  helper.dampingDimension.resize(helper.dimension.size());
  auto dimensionIterator = helper.dimension.begin();
  for (size_t q = 0; q < generationInfo.quadratur; q++) {
    for (uint8_t dim = 0; dim < (uint8_t)amount; dim++) {
      const auto startIterator = dimensionIterator;
      dimensionIterator += generationInfo.shape;
      std::fill(startIterator, dimensionIterator, dim);
    }
  }
  std::ranges::transform(helper.dimension, helper.dampingDimension.begin(),
                         [](auto in) { return in + 2; });

  helper.damping = {.data = helper.data.data(),
                    .perQuadOffset = amount,
                    .perQuadSize = dampingCount,
                    .perQuadOutputOffset = 1 + spaceDimension};
  helper.pressure = helper.damping;
  helper.pressure.perQuadSize = 1;
  helper.pressure.perQuadOutputOffset = spaceDimension;
  helper.velocity = helper.damping;
  helper.velocity.perQuadSize = spaceDimension;
  helper.velocity.perQuadOutputOffset = 0;
  return std::make_pair(std::move(helper), helper.data.data() + allocationSize * amount);
}

inline AllocationHelper generateFaceHelper(const STDiscretization &discretization, Matrix &M,
                                           const Cell &currentCell, size_t faceID,
                                           const size_t dampingCount) {
  const auto degree = M.GetDoF().GetDegree(currentCell());
  const auto &spaceQuadrature = discretization.GetCellFaceQuad(faceID, degree.space);
  const auto &timeQuadrature = discretization.GetTimeFaceQuad(faceID, degree.time);
  const auto &stShape = GetSpaceTimeShape(M, currentCell);
  const auto &timeShape = stShape.GetTimeShape();
  const auto &spaceShape = stShape.GetSpaceShape();
  const GenerationInputInfo generationInput{spaceQuadrature.size() * timeQuadrature.size(),
                                            stShape.size()};
  const auto &qPointsTime = timeQuadrature.QPoints();
  GenerateInfo generationInfo{[&, faceID](const GenerationInputInfo &in) {
    const size_t ti = in.shape / spaceShape.size();
    const size_t si = in.shape % spaceShape.size();
    const size_t qti = in.quadratur / spaceQuadrature.size();
    const size_t qsi = in.quadratur % spaceQuadrature.size();
    return timeShape(qPointsTime[qti], ti) * spaceShape(faceID, qsi, si);
  }};
  std::span generationInfos(&generationInfo, &generationInfo + 1);
  return generateBasic(generationInput, dampingCount, currentCell.dim(), generationInfos).first;
}

template<bool upOrDown>
inline AllocationHelper generateFaceEdge(const STDiscretization &discretization, Matrix &M,
                                         const Cell &previousTimeCell, const Cell &currentCell,
                                         const size_t dampingCount) {
  const auto timePoint = upOrDown ? Point(1.0) : Point(0.0);
  const auto degree = M.GetDoF().GetDegree(currentCell());
  const auto &spaceQuadrature = discretization.GetCellQuad(degree.space);
  const auto &stShape = GetSpaceTimeShape(M, currentCell);
  const auto &timeShape = stShape.GetTimeShape();
  const auto &spaceShape = stShape.GetSpaceShape();

  const GenerationInputInfo generationInput{(size_t)spaceQuadrature.size(), (size_t)stShape.size()};
  std::span qPointsSpace = spaceQuadrature.QPoints();
  std::vector<Point> points;
  if constexpr (upOrDown) {
    const auto time = M.find_face(previousTimeCell.Face(previousTimeCell.Faces() - 1))().t();
    points.resize(qPointsSpace.size());
    auto start = points.begin();
    for (const auto &p : qPointsSpace) {
      *start = previousTimeCell.GlobalToLocal(previousTimeCell.LocalToGlobal(p).CopyWithT(time));
      start++;
    }
    qPointsSpace = points;
  }
  const size_t shapeSize = spaceShape.size();
  const size_t quadratureSize = spaceQuadrature.size();
  GenerateInfo generationInfo{[&, shapeSize, quadratureSize](const GenerationInputInfo &in) {
    const size_t ti = in.shape / shapeSize;
    const size_t si = in.shape % shapeSize;
    const size_t qsi = in.quadratur % quadratureSize;
    const auto spacePoint = qPointsSpace[qsi];
    return timeShape(timePoint, ti) * spaceShape(spacePoint, si);
  }};
  std::span generationInfos(&generationInfo, &generationInfo + 1);
  return generateBasic(generationInput, dampingCount, currentCell.dim(), generationInfos).first;
}

inline AllocationHelper generateGenerationInfos(Matrix &M, const Cell &currentCell,
                                                const size_t dampingCount) {
  const auto &stQuadrature = GetSpaceTimeQuadrature(M, currentCell);
  const auto &stShape = GetSpaceTimeShape(M, currentCell);
  const GenerationInputInfo generationInfo{stQuadrature.size(), stShape.size()};

  const size_t allocationSize = generationInfo.quadratur * generationInfo.shape;
  std::vector<VectorField> gradientsCache(allocationSize);
  const Transformation &transform = currentCell.GetTransformation();

  const size_t shapeSize = (size_t)stShape.size();

  for (size_t q = 0; q < generationInfo.quadratur; q++) {
    const auto quadPoint = stQuadrature.QPoint(q);
    for (size_t i = 0; i < generationInfo.shape; i++) {
      VectorField STGrad = stShape.LocalSpaceTimeGradient(q, i);
      gradientsCache[q * shapeSize + i] = (transform * STGrad);
    }
  }

  std::array<GenerateInfo, 4> generationInfos;
  generationInfos[0].function = [&](const GenerationInputInfo &in) {
    return stShape(in.quadratur, in.shape);
  };
  const size_t spaceDimension = currentCell.dim();
  generationInfos[1].function = [&, spaceDimension](const GenerationInputInfo &in) {
    const auto value = in.dim % spaceDimension;
    return gradientsCache[in.quadratur * shapeSize + in.shape][value];
  };
  generationInfos[1].multiple = spaceDimension;
  generationInfos[2].function = [&](const GenerationInputInfo &in) {
    return gradientsCache[in.quadratur * shapeSize + in.shape].t();
  };
  generationInfos[2].multiple = std::max(dampingCount, spaceDimension);
  generationInfos[3].function = [&, dampingCount](const GenerationInputInfo &in) {
    const auto value = in.dim / dampingCount;
    return gradientsCache[in.quadratur * shapeSize + in.shape][value];
  };
  generationInfos[3].multiple = dampingCount * spaceDimension;

  auto [helper, gradiantData] =
      generateBasic(generationInfo, dampingCount, spaceDimension, generationInfos);

  helper.velocityDiv = {.data = gradiantData,
                        .perQuadOffset = generationInfos[1].multiple,
                        .perQuadSize = spaceDimension,
                        .perQuadOutputOffset = 0};
  helper.velocityDt = helper.velocityDiv;
  helper.velocityDt.perQuadOffset = generationInfos[2].multiple;
  helper.velocityDt.data += helper.velocityDiv.perQuadOffset * shapeSize * generationInfo.quadratur;
  helper.pressureDt = helper.velocityDt;
  helper.pressureDt.perQuadSize = 1;
  helper.pressureDt.perQuadOutputOffset = spaceDimension;
  helper.pressureGrad = helper.velocityDiv;
  helper.pressureGrad.perQuadOutputOffset = spaceDimension;
  helper.dampingDt = helper.velocityDt;
  helper.dampingDt.perQuadSize = dampingCount;
  helper.dampingDt.perQuadOutputOffset = spaceDimension + 1;
  helper.dampingGrad.perQuadOffset = dampingCount * spaceDimension;
  helper.dampingGrad.data =
      helper.velocityDt.data + shapeSize * generationInfos[2].multiple * generationInfo.quadratur;
  helper.dampingGrad.perQuadSize = dampingCount * spaceDimension;
  helper.dampingGrad.perQuadOutputOffset = spaceDimension + 1;
  return helper;
}

inline double pressureFluxCoefficient(bool boundary, int boundaryId) {
  // Dirichlet and Robin have negatives
  if (boundary && (boundaryId == 1 || boundaryId == 3)) return -1;
  return 1; // Neumann or none
}

inline double velocityFluxCoefficient(bool boundary, int boundaryId) {
  // Neumann and Robin have negatives
  if (boundary && (boundaryId == 2 || boundaryId == 3)) return -1;
  return 1; // Dirichlet or none
}

inline size_t findCorrespondingQuadrature(const Point &point, const std::vector<Point> &points) {
  const auto quadrature = std::ranges::find(points, point);
#ifdef DEBUG // This should not happen in production
  if (quadrature == points.end()) {
    Exit("Could not find corresponding quadrature point in neighbour cell!");
  }
#endif
  return quadrature - points.begin();
}

constexpr size_t VELOCITY_ID = 0;
constexpr size_t PRESSURE_ID = 1;

inline auto generateFaceFirst(const CalculationInfo &info, size_t faceID,
                              /*out*/ AllocationHelper &helper) {
  const auto &cellUsed = info.cellIterator;

  auto &disc = info.discretization;
  const auto &faceQPoints = disc.FaceQPoints();
  helper = generateFaceHelper(disc, info.matrix, cellUsed, faceID, info.damping);
  const auto &degree = info.matrix.GetDoF().GetDegree(cellUsed);

  ElementWeightCacheable cachable{helper, info.entries.a, info.entries.nf, info.problem.nL()};
  const size_t quadratureSize = helper.generatedWith.quadratur;
  const auto &spaceQuad = disc.GetCellFaceQuad(faceID, degree.space);
  const auto &timeQuad = disc.GetTimeFaceQuad(faceID, degree.time);
  initQuadraturePoints(cachable, quadratureSize,
                       face1Points(faceQPoints[faceID], timeQuad, cellUsed),
                       face1Weight(faceQPoints[faceID], spaceQuad, faceID, timeQuad, cellUsed),
                       true);
  return cachable;
}

inline size_t findFaceInternal(const Point &oldFace, const Cell &cellUsed) {
  for (size_t f1 = 0; f1 < cellUsed.Faces(); ++f1) {
    if (oldFace == cellUsed.Face(f1)) return f1;
  }
  Exit("Face id not found this should not happen in production!");
  return 0;
}

inline std::vector<size_t> generateQuadratureDictionary(const CalculationInfo &info,
                                                        const size_t oldFace, const Cell &cellUsed,
                                                        const ElementWeightCacheable &other) {
  // TODO IOTA opt

  const auto faceID = findFaceInternal(info.cellIterator.Face(oldFace), cellUsed);
  auto &disc = info.discretization;
  const auto &faceQPoints = disc.FaceQPoints();
  AllocationHelper helper;
  const auto &degree = info.matrix.GetDoF().GetDegree(cellUsed);

  ElementWeightCacheable cachable{helper, info.entries.a, info.entries.nf, info.problem.nL()};
  const auto &spaceQuad = disc.GetCellFaceQuad(faceID, degree.space);
  const auto &timeQuad = disc.GetTimeFaceQuad(faceID, degree.time);
  const size_t quadratureSize = spaceQuad.size() * timeQuad.size();
  initQuadraturePoints(cachable, quadratureSize,
                       face1Points(faceQPoints[faceID], timeQuad, cellUsed),
                       face1Weight(faceQPoints[faceID], spaceQuad, faceID, timeQuad, cellUsed),
                       true);
  std::vector<size_t> quadratureLookup(other.quadraturePointWeight.size());
  auto lookup = cachable.quadraturePointWeight.begin();
#ifdef DEBUG
  if (cachable.quadraturePointWeight.size() != other.quadraturePointWeight.size()) {
    Exit("Error, quadrature have different sizes this should not happen!")
  }
#endif // DEBUG
  for (const auto &[index, _u1, _u] : other.quadraturePointWeight) {
    const auto &[indexOther, _u2, _u3] = *lookup;
    quadratureLookup[index] = indexOther;
    lookup++;
  }
  return quadratureLookup;
}

inline auto outIteratorFrom(const RowIterator &startIterator, const size_t damping,
                            const AllocationHelper &helper) {
  const auto shape = helper.generatedWith.shape;
  const auto dim = helper.spaceDimension * shape;
  IterableOf<RowIterator> velocityIterator{startIterator, startIterator + dim};
  IterableOf<RowIterator> pressureIterator{startIterator + dim, startIterator + shape + dim};
  IterableOf<RowIterator> dampingIterator{startIterator + shape + dim,
                                          startIterator + damping * shape + shape + dim};
  return std::make_tuple(velocityIterator, pressureIterator, dampingIterator);
}

inline auto CreateCalculationInfo(Matrix &M, Vector &RHS, const AcousticProblem &prob,
                                  const Point &point, const Cell &cell,
                                  const std::function<double(Point)> &weight_func,
                                  AllocationHelper &out) {
  out = generateGenerationInfos(M, cell, prob.nL());

  const auto &spaceTimeQuadrature = GetSpaceTimeQuadrature(M, cell);

  DGRowEntries rowEntries(M, cell, cell);
  return CalculationInfo(point, M, prob, RHS, cell, weight_func, (STDiscretization &)M.GetDisc(),
                         rowEntries, out, spaceTimeQuadrature);
}

struct FaceCalculationInfo {
  AllocationHelper faceCurrentHelper;
  AllocationHelper neighbourHelper;
  FluxCache faceCurrentFlux;
  FluxCache neighbourFlux;
  FluxCache *usedFlux;
};

// Bullshit passing conventions ... wtf should I do ... probably better to use a
// macro hear ... way to much to pass Should have fucking used pointers instead
inline auto CreateFaceCalculationInfos(CalculationInfo &info, const size_t faceID,
                                       FaceCalculationInfo &allocations) {
  auto &matrix = info.matrix;
  const auto &mesh = matrix.GetMesh();
  auto &problem = info.problem;
  const auto &point = info.point;
  const auto &cellIterator = info.cellIterator;
  const double z_K = sqrt(problem.Rho(cellIterator, point) * problem.Kappa(cellIterator, point));
  const auto &spaceCell = cellIterator.SpaceCell();
  const Transformation &transform = spaceCell.GetTransformation();

  CalculateFaceInfo faceInfo{info, faceID, mesh, z_K};

  allocations.faceCurrentHelper.spaceDimension = info.helper.spaceDimension;
  allocations.neighbourHelper.spaceDimension = info.helper.spaceDimension;
  auto faceCurrentCellData = generateFaceFirst(info, faceID, allocations.faceCurrentHelper);

  VectorField qNormal = transform * spaceCell.LocalFaceNormal(faceID);
  qNormal /= norm(qNormal);
  initFluxCache(allocations.faceCurrentFlux, faceCurrentCellData, faceCurrentCellData, qNormal);

  auto outputTuple =
      outIteratorFrom({faceCurrentCellData.outputPointer, faceCurrentCellData.rowSize},
                      info.damping, info.helper);
  allocations.usedFlux = &allocations.faceCurrentFlux;
  if (!faceInfo.isOnBoundary) {
    DGRowEntries rowEntries(matrix, cellIterator, faceInfo.faceCellIterator, false);
    outputTuple = outIteratorFrom({rowEntries.a, (size_t)rowEntries.nf}, info.damping, info.helper);
    allocations.neighbourHelper =
        generateFaceHelper(info.discretization, matrix, faceInfo.faceCellIterator,
                           faceInfo.neighbourFaceID, info.damping);
    AllocationHolder interm{allocations.neighbourHelper};
    initFluxCache(allocations.neighbourFlux, faceCurrentCellData, interm, qNormal);
    allocations.usedFlux = &allocations.neighbourFlux;
  }
  AllocationHolder neighbourAllocHolder{.helper =
                                            (faceInfo.isOnBoundary ? faceCurrentCellData.helper :
                                                                     allocations.neighbourHelper),
                                        .damping = info.damping};
  return std::make_tuple(faceInfo, faceCurrentCellData, neighbourAllocHolder, outputTuple, qNormal);
}

inline auto CreateFaceSecondaryCalculate(CalculationInfo &info, const Cell &c_prev,
                                         AllocationHelper &helper) {
  helper = generateFaceEdge<false>(info.discretization, info.matrix, c_prev, info.cellIterator,
                                   info.damping);
  ElementWeightCacheable cacheable{helper, info.entries.a, info.entries.nf, info.problem.nL()};
  const size_t firstFaceID = info.cellIterator.Faces() - 2;
  const size_t spaceDegree = info.matrix.GetDoF().GetDegree(info.cellIterator).space;
  const auto &facePoints = info.discretization.FaceQPoints();
  const auto &firstspaceQuad = info.discretization.GetCellQuad(spaceDegree);
  initQuadraturePoints(cacheable, firstspaceQuad.size(),
                       face2Points(firstFaceID, info.matrix, firstspaceQuad, info.cellIterator),
                       face2Weight(facePoints[firstFaceID], firstspaceQuad, firstFaceID,
                                   info.cellIterator));
  return cacheable;
}

inline auto CreateFaceEdgeCalculation(CalculationInfo &info, const Cell &c_prev,
                                      AllocationHelper &helper) {
  auto &matrix = info.matrix;
  const auto &cellIterator = info.cellIterator;
  const auto &problem = info.problem;
  const auto &discretization = info.discretization;

  DGRowEntries M_c_prev(matrix, cellIterator, c_prev, false);
  const auto iterator =
      outIteratorFrom({M_c_prev.a, (size_t)M_c_prev.nf}, info.damping, info.helper);
  helper = generateFaceEdge<true>(info.discretization, matrix, c_prev, cellIterator, info.damping);
  return std::make_pair(AllocationHolder{helper, M_c_prev.a, (size_t)M_c_prev.nf, info.damping},
                        iterator);
}

inline auto vectorIterator(Vector &vector, const Point &cellPoint, const size_t shapeSize,
                           const size_t damping, const size_t dim) {
  auto velocityOutput = fromVector(vector, cellPoint, shapeSize, 0, dim);
  auto pressureOutput = fromVector(vector, cellPoint, shapeSize, dim);
  auto dampingOutput = fromVector(vector, cellPoint, shapeSize, dim + 1, damping);
  return std::make_tuple(velocityOutput, pressureOutput, dampingOutput);
}

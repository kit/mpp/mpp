#ifndef __ITERABLE__SYSTEMS
#define __ITERABLE__SYSTEMS

#include <array>
#include <concepts>
#include <cstddef>
#include <cstdint>
#include <functional>
#include <ranges>
#include <span>
#include <tuple>
#include <vector>

#include <AcousticProblems.hpp>
#include <MatrixAccess.hpp>
#include <SpaceTimeDiscretization.hpp>
#include <SpaceTimeQuadrature.hpp>
#include <Vector.hpp>
#include <VectorMatrixBase.hpp>

struct GenerationInputInfo {
  size_t quadratur = SIZE_MAX;
  size_t shape = SIZE_MAX;
  size_t dim = SIZE_MAX;

  size_t allocationSize() const { return quadratur * shape; }
};

using GeneratorFunction = std::function<double(const GenerationInputInfo &)>;

struct GenerateInfo {
  GeneratorFunction function;
  size_t multiple = 1;
};

struct AllocationInfo {
  const double *data = nullptr;
  size_t perQuadOffset = SIZE_MAX;
  size_t perQuadSize = SIZE_MAX;
  size_t perQuadOutputOffset = SIZE_MAX;

  inline auto begin() const { return data; }

  inline auto end() const { return data + perQuadSize; }
};

struct AllocationHelper {
  GenerationInputInfo generatedWith{};
  size_t spaceDimension = SIZE_MAX;
  std::vector<double> data;
  std::vector<uint8_t> dimension;
  std::vector<uint8_t> dampingDimension;
  AllocationInfo pressure;
  AllocationInfo damping;
  AllocationInfo velocity;
  /* Opt */
  AllocationInfo velocityDiv;
  AllocationInfo velocityDt;
  AllocationInfo pressureDt;
  AllocationInfo pressureGrad;
  AllocationInfo dampingDt;
  AllocationInfo dampingGrad;

  using Iterator = std::vector<double>::iterator;

  inline Iterator allocateSingle(const GenerateInfo &input, const GenerationInputInfo &info,
                                 Iterator iterator) {
    for (GenerationInputInfo next{0, 0, 0}; next.quadratur < info.quadratur; next.quadratur++) {
      for (next.dim = 0; next.dim < input.multiple; next.dim++) {
        for (next.shape = 0; next.shape < info.shape; next.shape++) {
          (*iterator++) = input.function(next);
        }
      }
    }
    return iterator;
  }

  inline void allocate(const GenerationInputInfo &info, const std::span<GenerateInfo> inputs) {
    generatedWith = info;
    const size_t allocationPerInstance = info.allocationSize();
    const auto sum = std::accumulate(inputs.begin(), inputs.end(), (size_t)0,
                                     [](auto akku, auto &info) { return akku + info.multiple; });
    const auto sizeOfAll = allocationPerInstance * sum;
    data.resize(sizeOfAll);
    auto beginData = data.begin();
    for (const auto &generation : inputs) {
      beginData = allocateSingle(generation, info, beginData);
    }
  }
};

struct AllocationHolder {
  const AllocationHelper &helper;
  double *outputPointer = nullptr;
  size_t rowSize = SIZE_MAX;
  size_t damping = SIZE_MAX;
};

struct OutputIterator {
  double *entries = nullptr;

  inline auto &operator*() const { return *entries; };
};

inline bool operator!=(const OutputIterator output1, const OutputIterator output2) {
  return output1.entries != output2.entries;
}

inline auto operator++(OutputIterator &output) {
  output.entries++;
  return output;
}

template<TupleLike Tuple>
using OutputIteratorFrom = std::tuple_element_t<0, Tuple>;

struct BlockIterator {
  double *rowIterator = nullptr;
  size_t rowSize = SIZE_MAX;

  inline auto operator+(const size_t addition) const {
    return BlockIterator{rowIterator + addition * rowSize, rowSize};
  };

  inline auto &operator*() const { return *rowIterator; }
};

struct RowIterator : public BlockIterator {};

template<class Iter>
struct IterableOf {
  std::remove_cv_t<Iter> beginInternal;
  std::remove_cv_t<Iter> endInternal;

  std::remove_cv_t<Iter> begin() const { return beginInternal; }

  std::remove_cv_t<Iter> end() const { return endInternal; }
};

struct VectorIterator {
  double *rowIterator;

  VectorIterator(Vector &vector, const Point &point, size_t offset = 0) :
      rowIterator(vector(point) + offset) {}

  VectorIterator(double *rowIterator) : rowIterator(rowIterator) {}

  inline auto &operator*() const { return *rowIterator; };

  inline auto operator+(const size_t addition) const {
    return VectorIterator(rowIterator + addition);
  };
};

inline auto operator++(VectorIterator &output) {
  output.rowIterator++;
  return output;
}

inline bool operator!=(const VectorIterator output1, const VectorIterator output2) {
  return output1.rowIterator != output2.rowIterator;
}

inline IterableOf<VectorIterator> fromVector(Vector &vector, const Point &cellPoint,
                                             size_t shapeSize, size_t offset = 0,
                                             size_t endOffset = 1) {
  VectorIterator startIter(vector, cellPoint, shapeSize * offset);
  return {startIter, startIter + shapeSize * endOffset};
}

template<class Holder>
inline IterableOf<RowIterator> rowFrom(const BlockIterator &quadrature, const Holder &info,
                                       size_t value = 1, size_t offset = 0) {
  const auto shapeDimension = info.helper.generatedWith.shape * value;
  RowIterator startIter{quadrature.rowIterator
                            + info.helper.generatedWith.shape * info.rowSize * offset,
                        info.rowSize};
  return {startIter, startIter + shapeDimension};
}

template<class Holder>
inline IterableOf<OutputIterator> outputFrom(const BlockIterator &quadrature, const Holder &info,
                                             size_t value = 1, size_t offset = 0) {
  const auto shapeDimension = info.helper.generatedWith.shape * value;
  OutputIterator startIter{quadrature.rowIterator + info.helper.generatedWith.shape * offset};
  return {startIter, OutputIterator{startIter.entries + shapeDimension}};
}

inline bool operator!=(const BlockIterator output1, const BlockIterator output2) {
  return output1.rowIterator != output2.rowIterator;
}

inline auto operator++(std::derived_from<BlockIterator> auto &output) {
  output.rowIterator += output.rowSize;
  return output;
}

template <typename Tuple>
struct CalculateIterator {
  Tuple types;

  inline const auto operator*() const {
    return std::apply(
        [](const auto size, const auto &...inputs) {
          return std::make_tuple(size, (*inputs)...);
        },
        types);
  }
};

inline auto &operator++(TupleLike auto &tuple) {
  std::apply([&](auto &...inputs) { (++inputs, ...); }, tuple);
  return tuple;
}

template <typename Tuple>
inline auto &operator++(CalculateIterator<Tuple> &iterator) {
  return ++iterator.types;
}

template <typename Tuple>
inline bool operator!=(const CalculateIterator<Tuple> &iterator,
                       const OutputIteratorFrom<Tuple> end) {
  return std::get<0>(iterator.types) != end;
}

template <typename Tuple>
struct CalculateIterable {
  CalculateIterator<Tuple> beginTypes;

  using OutputIteratorType = OutputIteratorFrom<Tuple>;
  const OutputIteratorType endSize;

  CalculateIterable(const Tuple &beginTypes, const OutputIteratorType endSize) :
      beginTypes(CalculateIterator<Tuple>{beginTypes}), endSize(endSize) {}

  inline const auto &begin() const { return beginTypes; }

  inline const auto end() const { return endSize; }
};

template<class... ExternalTypes>
using IteratorType = std::remove_cvref_t<std::tuple_element_t<0, std::tuple<ExternalTypes...>>>;

template <class... ExternalTypes>
inline auto make_iterable(const size_t startOffset, const ExternalTypes &...types) {
  const auto [end, valueTuple] = std::invoke(
      [&](const auto &output, const auto &...inputs) {
        return std::make_pair(std::end(output),
                              std::make_tuple(std::begin(output),
                                              (std::begin(inputs) + startOffset)...));
      },
      types...);
  return CalculateIterable(valueTuple, end);
}

struct FluxCache {
  struct _impl {
    GenerationInputInfo generatedWith{};
    std::vector<double> fluxCache;
    std::vector<uint8_t> dimension;
    AllocationInfo flux;
  } helper;

  size_t rowSize = SIZE_MAX;
  double *outputPointer = nullptr;
};

template<class Iterator>
inline auto __implFrom(const size_t index, const size_t shape, const AllocationInfo &info,
                       const IterableOf<Iterator> &iterator, const size_t size,
                       const size_t offset = 0) {
  const auto fullDimension = shape * info.perQuadOffset;
  const auto fullSize = size * shape;
  const std::span toUseSpan(info.begin() + offset, info.begin() + offset + fullSize);
  return make_iterable(index * fullDimension, iterator, toUseSpan);
}

template<class Iterator>
inline auto __implWithDimFrom(const size_t index, const size_t shape, const AllocationInfo &info,
                              const IterableOf<Iterator> &iterator,
                              const std::vector<uint8_t> &dimension, const size_t size,
                              const size_t offset = 0) {
  const auto fullDimension = shape * info.perQuadOffset;
  const auto fullSize = size * shape;
  const std::span toUseSpan(info.begin() + offset, info.begin() + offset + fullSize);
  const std::span toUseSpanDimension(dimension.begin() + offset,
                                     dimension.begin() + offset + fullSize);
  return make_iterable(index * fullDimension, iterator, toUseSpan, toUseSpanDimension);
}

template<class Iterator>
inline auto __implWithDimFrom(const size_t index, const size_t shape, const AllocationInfo &info,
                              const IterableOf<Iterator> &iterator,
                              const std::vector<uint8_t> &dimension) {
  return __implWithDimFrom(index, shape, info, iterator, dimension, info.perQuadSize);
}

template<class Iterator>
inline auto __implFrom(const size_t index, const size_t shape, const AllocationInfo &info,
                       const IterableOf<Iterator> &iterator) {
  return __implFrom(index, shape, info, iterator, info.perQuadSize);
}

template<class Iterator>
inline auto __implFrom(const size_t index, const AllocationHelper &shape,
                       const AllocationInfo &info, const IterableOf<Iterator> &iterator) {
  return __implFrom(index, shape.generatedWith.shape, info, iterator);
}

#define DEFAULT_OVERLOAD(name, value)                                                              \
  template<class CalculationInfo>                                                                  \
  inline auto name(const size_t startOf, const CalculationInfo &info,                              \
                   const RowIterator &rowIterator) {                                               \
    const auto &allocation = info.helper.value;                                                    \
    return name(startOf, info,                                                                     \
                outputFrom(rowIterator, info, allocation.perQuadSize,                              \
                           allocation.perQuadOutputOffset));                                       \
  }                                                                                                \
  template<class CalculationInfo>                                                                  \
  inline auto name(const size_t startOf, const CalculationInfo &info) {                            \
    const auto &allocation = info.helper.value;                                                    \
    RowIterator rowIterator{info.outputPointer, 0};                                                \
    return name(startOf, info,                                                                     \
                rowFrom(rowIterator, info, allocation.perQuadSize,                                 \
                        allocation.perQuadOutputOffset));                                          \
  }

#define DEFAULT_INDEX(name, value)                                                                 \
  template<class CalculationInfo>                                                                  \
  inline auto name(const size_t startOf, const CalculationInfo &info,                              \
                   const RowIterator &rowIterator, size_t dimension) {                             \
    const auto &allocation = info.helper.value;                                                    \
    const auto shape = info.helper.generatedWith.shape;                                            \
    const auto offset = (dimension + allocation.perQuadOutputOffset) * shape;                      \
    OutputIterator start = {rowIterator.rowIterator + offset};                                     \
    IterableOf<OutputIterator> iterable = {start, {start.entries + shape}};                        \
    return __implFrom(startOf, shape, allocation, iterable, 1u, dimension * shape);                \
  }

template<class Iterator>
inline auto dampingFrom(const size_t index, const AllocationHolder &info,
                        const IterableOf<Iterator> &iterator) {
  return __implFrom(index, info.helper, info.helper.damping, iterator);
}
DEFAULT_OVERLOAD(dampingFrom, damping)
DEFAULT_INDEX(dampingFrom, damping);

template<class Iterator>
inline auto derivativeDampingIterable(const size_t startOf, const AllocationHolder &info,
                                      const IterableOf<Iterator> &iterator) {
  return __implFrom(startOf, info.helper.generatedWith.shape, info.helper.dampingDt, iterator);
}
DEFAULT_OVERLOAD(derivativeDampingIterable, dampingDt)
DEFAULT_INDEX(derivativeDampingIterable, dampingDt);

template<class Iterator>
inline auto dampingWithDim(const size_t index, const AllocationHolder &info,
                           const IterableOf<Iterator> &iterator) {
  return __implWithDimFrom(index, info.helper.generatedWith.shape, info.helper.damping, iterator,
                           info.helper.dampingDimension);
}
DEFAULT_OVERLOAD(dampingWithDim, damping)

template<class Iterator>
inline auto pressureIterable(const size_t startOf, const AllocationHolder &info,
                             const IterableOf<Iterator> &iterator) {
  return __implFrom(startOf, info.helper, info.helper.pressure, iterator);
}
DEFAULT_OVERLOAD(pressureIterable, pressure)

template<class Iterator>
inline auto fluxIterable(const size_t startOf, const FluxCache &info,
                         const IterableOf<Iterator> &iterator) {
  return __implFrom(startOf, info.helper.generatedWith.shape, info.helper.flux, iterator);
}
DEFAULT_OVERLOAD(fluxIterable, flux)
DEFAULT_INDEX(fluxIterable, flux);

template<class Iterator>
inline auto fluxIterableWithDim(const size_t index, const FluxCache &info,
                                const IterableOf<Iterator> &iterator) {
  return __implWithDimFrom(index, info.helper.generatedWith.shape, info.helper.flux, iterator,
                           info.helper.dimension);
}
DEFAULT_OVERLOAD(fluxIterableWithDim, flux)

template<class Iterator>
inline auto derivativePressureIterable(const size_t startOf, const AllocationHolder &info,
                                       const IterableOf<Iterator> &iterator) {
  return __implFrom(startOf, info.helper, info.helper.pressureDt, iterator);
}
DEFAULT_OVERLOAD(derivativePressureIterable, pressureDt)

template<class Iterator>
inline auto derivativeVelocityIterable(const size_t startOf, const AllocationHolder &info,
                                       const IterableOf<Iterator> &iterator) {
  return __implFrom(startOf, info.helper, info.helper.velocityDt, iterator);
}
DEFAULT_OVERLOAD(derivativeVelocityIterable, velocityDt)
DEFAULT_INDEX(derivativeVelocityIterable, velocityDt)

inline auto gradientDampingIterable(const size_t startOf, const AllocationHolder &info,
                                    const BlockIterator &oldIterator, const size_t dimension,
                                    const size_t dampingSize) {
  const auto shape = info.helper.generatedWith.shape;
  const auto iterator =
      outputFrom(oldIterator, info, 1, dampingSize + info.helper.spaceDimension + 1);
  auto damping = info.helper.dampingGrad;
  damping.data += dimension * shape * info.damping + dampingSize * shape;
  return __implFrom(startOf, info.helper, damping, iterator);
}

template<class Iterator>
inline auto gradientPressureIterable(const size_t startOf, const AllocationHolder &info,
                                     const IterableOf<Iterator> &iterator) {
  return __implFrom(startOf, info.helper, info.helper.pressureGrad, iterator);
}
DEFAULT_OVERLOAD(gradientPressureIterable, pressureGrad)

inline auto gradientPressureIterable(const size_t startOf, const AllocationHolder &info,
                                     const RowIterator &rowIterator, size_t dimension) {
  const auto &allocation = info.helper.pressureGrad;
  const auto shape = info.helper.generatedWith.shape;
  const auto offset = allocation.perQuadOutputOffset * shape;
  OutputIterator start = {rowIterator.rowIterator + offset};
  IterableOf<OutputIterator> iterable = {start, {start.entries + shape}};
  return __implFrom(startOf, shape, allocation, iterable, 1u, dimension * shape);
}

template<class Iterator, class CalculationInfo>
inline auto divergenceVelocityIterable(const size_t startOf,
                                       const CalculationInfo &info,
                                       const IterableOf<Iterator> &iterator) {
  return __implFrom(startOf, info.helper, info.helper.velocityDiv, iterator);
}
DEFAULT_OVERLOAD(divergenceVelocityIterable, velocityDiv)
DEFAULT_INDEX(divergenceVelocityIterable, velocityDiv);

template<class Iterator>
inline auto velocityIterable(const size_t startOf, const AllocationHolder &info,
                             const IterableOf<Iterator> &iterator) {
  return __implFrom(startOf, info.helper, info.helper.velocity, iterator);
}
DEFAULT_OVERLOAD(velocityIterable, velocity)
DEFAULT_INDEX(velocityIterable, velocity);

template<class Iterator>
inline auto velocityIterableWithDim(const size_t startOf, const AllocationHolder &info,
                                    const IterableOf<Iterator> &iterator) {
  return __implWithDimFrom(startOf, info.helper.generatedWith.shape, info.helper.velocity, iterator,
                           info.helper.dimension);
}
DEFAULT_OVERLOAD(velocityIterableWithDim, velocity)

#endif

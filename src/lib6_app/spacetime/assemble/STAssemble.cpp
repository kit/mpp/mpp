#include "STPGViscoAcousticAssemble.hpp"
#include "STPGViscoElasticAssemble.hpp"
#include "STDGElasticity.hpp"
#include "STDGViscoAcousticAssemble.hpp"
#include "STDGTransportAssemble.hpp"
#include "STGLViscoAcousticAssemble.hpp"
#include "STDGMatrixFreeViscoAcousticAssemble.hpp"

double STAssemble::MhalfLuMinusF(const Vector &U, const std::string &filename) const {
  Vector v(0.0, U);
  for(cell c = U.cells(); c != U.cells_end(); c++){
    v(c(),0) = MminushalfLuMinusF(c, U);
  }
  return v.norm();
}

void STAssemble::PlotErrorEstimator(const Vector &Eta, std::string filename) {
  VtuPlot plot(filename, {.parallelPlotting=false,
                      .plotBackendCreator=std::make_unique<DefaultSpaceTimeBackend>});
  plot.AddData("Eta", Eta, 0);
  plot.PlotFile();
}


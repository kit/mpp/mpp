#ifndef STPATHMANAGER_HPP
#define STPATHMANAGER_HPP

#include <vector>
#include "Mesh.hpp"
#include "STAssemble.hpp"

using Path = std::vector<MeshIndex>;

class PathStrategy {
public:
  virtual Path createPath(MeshIndex from, MeshIndex to) const = 0;
  virtual std::string name() const = 0;

  virtual ~PathStrategy() {};
};

class AdaptiveToZeroDirectPathStrategy : public PathStrategy {
  Path createPath(MeshIndex from, MeshIndex to) const override {
    Path path{from};
    MeshIndex current = from;
    while (current.adaptivityLevel > 0) {
      current = current.CoarserInAdaptivity();
      path.push_back(current);
    }
    while (current.space > to.space && current.time > to.time) {
      current = current.CoarserInBoth();
      path.push_back(current);
    }
    return path;
  }
  std::string name() const override{
    return "AdaptiveToZeroDirectPathStrategy";
  }
};


class DirectPathStrategy : public PathStrategy {
  Path createPath(MeshIndex from, MeshIndex to) const override {
    Path path{from};
    MeshIndex current = from;
    while (current.space > to.space && current.time > to.time) {
      current = current.CoarserInBoth();
      path.push_back(current);
    }
    return path;
  }
  std::string name() const override{
    return "SpaceThenTimePathStrategy";
  }
};

class SpaceThenTimePathStrategy : public PathStrategy {
  Path createPath(MeshIndex from, MeshIndex to) const override {
    Path path{from};
    MeshIndex current = from;
    while (current.space > to.space || current.time > to.time) {
      if (current.space > to.space){
        current = current.CoarserInSpace();
      } else {
        current = current.CoarserInTime();
      }
      path.push_back(current);
    }
    return path;
  }
  std::string name() const override{
    return "SpaceThenTimePathStrategy";
  }
};

class TimeThenSpacePathStrategy : public PathStrategy {
  Path createPath(MeshIndex from, MeshIndex to) const override {
    Path path{from};
    MeshIndex current = from;
    while (current.space > to.space || current.time > to.time) {
      if (current.time > to.time){
        current = current.CoarserInTime();
      } else {
        current = current.CoarserInSpace();
      }
      path.push_back(current);
    }
    return path;
  }
  std::string name() const override{
    return "TimeThenSpacePathStrategy";
  }
};

class OnlyTimePathStrategy : public PathStrategy {
  Path createPath(MeshIndex from, MeshIndex to) const override {
    Path path{from};
    MeshIndex current = from;
    while (current.time > to.time) {
      current = current.CoarserInTime();
      path.push_back(current);
    }
    return path;
  }
  std::string name() const override{
    return "OnlyTimePathStrategy";
  }
};

class OnlySpacePathStrategy : public PathStrategy {
  Path createPath(MeshIndex from, MeshIndex to) const override {
    Path path{from};
    MeshIndex current = from;
    while (current.space > to.space) {
      current = current.CoarserInSpace();
      path.push_back(current);
    }
    return path;
  }
  std::string name() const override{
    return "OnlySpacePathStrategy";
  }
};

Path makePathSpaceThenTime(MeshIndex levels);

Path makePathTimeThenSpace(MeshIndex levels);

Path makeTimePath(MeshIndex levels);

Path makeTimePath(MeshIndex levels, MeshIndex first);

Path makeSpacePath(MeshIndex levels);

Path makeSpacePath(MeshIndex levels, MeshIndex first);

std::unique_ptr<Preconditioner> createPreconditioner(const std::string &pathChoice,
                                                     STAssemble &assemble);

#endif //STPATHMANAGER_HPP

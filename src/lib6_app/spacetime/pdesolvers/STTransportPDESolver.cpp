#include "STTransportPDESolver.hpp"
#include <format>
#include "GMRES.hpp"

#include "STDGDGTransportElement.hpp"
#include "StringUtil.hpp"

#include "VtuPlot.hpp"

void STTransportPDESolver::createAssemble(const ITransportProblem &problem) {
  auto degree = DegreePair((short)conf.degree, (short)conf.timeDegree);

  if (assemble) {
    assemble = std::make_unique<STDGTransportAssemble>(*assemble, problem);
  } else {
    assemble = std::make_unique<STDGTransportAssemble>(degree, problem);
  }
  assemble->PrintInfo();

  solver = GetLinearSolverUnique("GMRES", GetPC("PointBlockGaussSeidel"), "Linear");
}

bool STTransportPDESolver::AdaptiveStep(Vector &u, int step) {
  int degree = 0;
  Config::Get("degree", degree);
  if (step > degree) return true;

  mout.StartBlock("Adaptivity");
  double theta = 0;
  Config::Get("theta", theta);
  if (theta == -1) return true;
  double theta_min = 0;
  double theta_factor = 1.0;
  std::string refine_by("abs_value");
  Config::Get("theta_factor", theta_factor);
  Config::Get("theta_min", theta_min);
  Config::Get("refine_by", refine_by);

  Vector Eta(u);
  theta *= pow(theta_factor, Eta.Level().adaptivityLevel);

  assemble->AdaptPolynomialDegrees(Eta, theta, theta_min, refine_by);
  assemble->AdaptQuadrature(Eta.Level().NextInAdaptivity(), -1, true);
  mout.EndBlock();

  Vector u_ref(0.0, u.GetSharedDisc(), u.Level().NextInAdaptivity());
  u_ref.Clear();
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    STDGDGTransportElement elem(u, c);
    std::vector<Point> z = u_ref.GetDoF().GetNodalPoints(*c);
    for (int i = 0; i < z.size(); ++i)
      u_ref(c(), i) = elem.Density(z[i], u);
  }
  u_ref.Accumulate();

  Vector RHS_ref(0.0, u_ref);
  RHS_ref.PrintInfo();
  Matrix M_ref(RHS_ref);
  assemble->System(M_ref, RHS_ref);
  solver->operator()(M_ref, true);
  RHS_ref -= M_ref * u_ref;
  u_ref += (*solver) * RHS_ref;


  STTransportSolution sol(u_ref);
  plotVtu(sol);
  computeValues(sol);
  sol.PrintInfo();


  AdaptiveStep(u_ref, step + 1);
  return true;
}

void STTransportPDESolver::run(STTransportSolution &solution) const {
  mout.StartBlock("STTransportPDESolver");
  Vector RHS(0.0, solution.vector);
  RHS.PrintInfo();
  M = std::make_unique<Matrix>(RHS);
  M->PrintInfo();
  assemble->System(*M, RHS);
  solver->operator()(*M, true);
  if (assemble->GetProblem().HasExactSolution()) {
    bool set_exact_solution = false;
    Config::Get("set_exact_solution", set_exact_solution);
    if (set_exact_solution) assemble->get_exact_solution(solution.vector);
  }
  RHS -= (*M) * solution.vector;
  solution.vector += (*solver) * RHS;
  mout.EndBlock();
}

void STTransportPDESolver::computeValues(STTransportSolution &solution) const {
  std::map<std::string, std::vector<double>> &timeValues = solution.timeDependentValues;
  Vector &u = solution.vector;
  double T = u.GetMesh().GetEndTime();
  double dt = 0;
  int level = u.Level().space;

  Config::Get("dt", dt);
  dt *= pow(2, -level);
  size_t N = u.GetMesh().GetTimesteps().size();
  // std::vector<double> Energy(N);
  std::vector<double> Mass(N);
  std::vector<double> inflow(N);
  std::vector<double> outflow(N);
  std::vector<double> outflow_bnd(N);

  // assemble->Energy(u, Energy);
  assemble->Mass(u, T, Mass, inflow, outflow, outflow_bnd);

  double Isum = 0;
  double Osum = 0;
  for (int n = 0; n < N; ++n) {
    Isum += inflow[n];
    Osum += outflow[n];
    timeValues["n"].push_back(n);
    timeValues["t"].push_back(u.GetMesh().GetTimesteps()[n]);
    // values["Energy"].push_back(Energy[n]);
    timeValues["Mass"].push_back(Mass[n]);
    timeValues["Inflow"].push_back(inflow[n] / dt);
    timeValues["Outflow"].push_back(outflow[n] / dt);
    timeValues["IF_sum"].push_back(Isum);
    timeValues["OF_sum"].push_back(Osum);
  }
  std::map<std::string, double> &values = solution.values;
  values["norm"] = u.norm();
  values["L2_Norm"] = assemble->L2Norm(u);

  solution.iterations.push_back(solver->GetIteration());
  values["SpaceLevel"] = u.Level().space;
  values["TimeLevel"] = u.Level().time;
  values["AdaptiveLevel"] = u.Level().adaptivityLevel;
  auto count_deg = u.GetMatrixGraph().GetDegreeCount();
  for (const auto &[deg, deg_count] : count_deg) {
    values["CellCountOfDeg" + deg.toString()] = (double)deg_count;
  }
  values["DoFs"] = u.pSize();
  values["MatrixAllocationSize"] = PPM->Sum(u.Size());
  values["Cells"] = u.GetMesh().CellCountGeometry();


  if (assemble->GetProblem().HasExactSolution()) {
    values["L1_Error"] = assemble->L1Error(u);
    values["L2_Error"] = assemble->L2Error(u);
    values["GN_Error"] = assemble->GNError(u);
    values["LInf_Error"] = assemble->LInfError(u);
  }
}

void STTransportPDESolver::plotSolution(const Vector &U) const {
  auto vtuDisc =
      std::make_shared<STDiscretizationT_DGDG<>>(U.GetDisc().GetMeshes(), DegreePair{0, 0}, 1);
  Vector U_vtu(0.0, vtuDisc, U.Level());
  // TODO move to code to assemble!
  for (cell c = U.cells(); c != U.cells_end(); ++c) {
    STDGDGTransportElement elem(U, c);
    U_vtu(elem.r(), 0) = elem.Density(c(), U);
  }

  std::string name =
      std::format("NumericalSolutionST_l{}_a{}", U.Level().space, U.Level().adaptivityLevel);

  VtuPlot plot(name, {.parallelPlotting = false,
                      .plotBackendCreator = std::make_unique<DefaultSpaceTimeBackend>});
  plot.AddData("Density", U_vtu, 0);
  plot.PlotFile();

  bool vtuPlotTimeSeries = false;
  Config::Get("VtuPlotTimeSeries", vtuPlotTimeSeries);
  if (vtuPlotTimeSeries) {
    VtuPlot plot2(name, {.parallelPlotting = false,
                         .plotBackendCreator = std::make_unique<SpaceTimeBackend>});
    plot2.AddData("Density", U_vtu, 0);
    plot2.PlotFile();
  }
}

void STTransportPDESolver::plotDegrees(const Vector &U) const {
  auto degreeDisc = std::make_shared<STDiscretizationT_DGDG<>>(U.GetMeshes(), DegreePair{0, 0}, 2);
  Vector degrees(0.0, degreeDisc, U.Level());
  for (row r = degrees.rows(); r != degrees.rows_end(); ++r) {
    DegreePair degPair = U.GetDoF().GetDegree(r());
    degrees(r)[0] = degPair.time;
    degrees(r)[1] = degPair.space;
  }
  std::string name = std::format("Degrees_{}", count);

  VtuPlot plot{name,
               {.parallelPlotting = false,
                .plotBackendCreator = std::make_unique<DefaultSpaceTimeBackend>}};
  plot.AddData("TimeDegree", degrees, 0);
  plot.AddData("SpaceDegree", degrees, 1);
  plot.PlotFile();

  bool vtuPlotTimeSeries = false;
  Config::Get("VtuPlotTimeSeries", vtuPlotTimeSeries);
  if (vtuPlotTimeSeries) {
    VtuPlot plot2(name, {.parallelPlotting = false,
                         .plotBackendCreator = std::make_unique<SpaceTimeBackend>});
    plot2.AddData("TimeDegree", degrees, 0);
    plot2.AddData("SpaceDegree", degrees, 1);
    plot2.PlotFile();
  }
}

void STTransportPDESolver::plotVtu(const STTransportSolution &solution) const {
  bool vtuPlot = false;
  Config::Get("VtuPlot", vtuPlot);
  if (!vtuPlot) return;

  const Vector &U = solution.vector;

  plotDegrees(U);
  plotSolution(U);

  count++;
}

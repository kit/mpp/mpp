#ifndef SPACETIME_STACOUSTICMAIN_HPP
#define SPACETIME_STACOUSTICMAIN_HPP

#include <format>
#include "AcousticProblems.hpp"
#include "AdaptiveConfig.hpp"
#include "PDESolver.hpp"
#include "STCallbackStrategy.hpp"
#include "STDGMatrixFreeViscoAcousticAssemble.hpp"
#include "STDGViscoAcousticAssemble.hpp"
#include "STGLViscoAcousticAssemble.hpp"
#include "STPGViscoAcousticAssemble.hpp"
#include "STPathManager.hpp"

struct STAcousticSolution {
  explicit STAcousticSolution(std::shared_ptr<const IDiscretization> disc) :
      vector(Vector(0.0, std::move(disc))) {}

  explicit STAcousticSolution(std::shared_ptr<const IDiscretization> disc, MeshIndex meshIndex) :
      vector(Vector(0.0, std::move(disc), meshIndex)) {}

  explicit STAcousticSolution(const Vector &vector) : vector(Vector(vector)) {}

  double computingTime{};

  double totalTime{};

  double initTime{};

  ValueMap values{};

  MultiValueMap timeDependentValues{};

  std::vector<Iteration> iterations;

  Vector vector;

  std::shared_ptr<SeismogramData> seismograms;

  int verbose = 0;

  void PrintInfo() {
    std::vector<PrintInfoEntry<double>> entries;
    for (auto &[key, value] : values) {
      entries.emplace_back(key, value, verbose = 1);
    }
    mout.PrintInfo("Solution", verbose, entries);

    if (timeDependentValues.empty()) return;

    for (int n = 0; n < timeDependentValues.begin()->second.size(); ++n) {
      std::vector<PrintIterEntry<double>> timestep_entries;
      for (auto &[key, valueList] : timeDependentValues) {
        timestep_entries.emplace_back(key, valueList[n], 11, verbose = 1);
      }
      mout.PrintIteration(verbose, timestep_entries);
    }
  }
};

template<class AcousticAssemble>
class STAcousticPDESolverT : public GenericPDESolver<AcousticProblem, STAcousticSolution> {
protected:
  std::unique_ptr<AcousticAssemble> assemble = nullptr;

  std::unique_ptr<LinearSolver> solver = nullptr;

  std::unique_ptr<ObservationSpecification> specification = nullptr;

  bool measure = false;

  mutable int count = 0;

  mutable std::unique_ptr<Matrix> M = nullptr;

  // Todo: discuss if this as a state is really needed.
  // UQOC has same problem; i.e. control has to be passed too
  // Proposal: Rather give Run method possibility to take
  // Problem and old solution
  mutable std::unique_ptr<Vector> oldSolution = nullptr;
public:
  using GenericPDESolver::Run;

  explicit STAcousticPDESolverT(PDESolverConfig _mc) : GenericPDESolver(_mc) {}

  std::shared_ptr<const IDiscretization> GetSharedDisc() const override {
    return assemble->GetSharedDisc();
  }

  void createAssemble(const AcousticProblem &problem) override;

  void createAdjointAssemble(std::shared_ptr<AcousticProblem> problem) override {
    solver->GetPreconditioner().transpose();
    assemble->SetProblem(problem);
  };

  STAcousticPDESolverT() : STAcousticPDESolverT(PDESolverConfig{}) {}

  std::string Name() const override { return "STAcoustic"; }

  void plotVtu(const STAcousticSolution &solution) const override;

  void computeValues(STAcousticSolution &solution) const override;

  void plotParams(const Vector &U) const { assemble->PlotParameters(U, std::to_string(count)); }

  void plotDegrees(const Vector &U) const {
    auto degreeDisc =
        std::make_shared<STDiscretizationT_DGDG<>>(U.GetMeshes(), DegreePair{0, 0}, 2);
    Vector degrees(0.0, degreeDisc, U.Level());
    for (row r = degrees.rows(); r != degrees.rows_end(); ++r) {
      degrees(r)[0] = U.GetDoF().get_time_deg(r());
      degrees(r)[1] = U.GetDoF().get_space_deg(r());
    }
    VtuPlot plot{std::format("Degrees{}", count)};
    plot.AddData("TimeDegree", degrees, 0);
    plot.AddData("SpaceDegree", degrees, 1);
    plot.PlotFile();
  }

  void plotSolution(const Vector &U) const {
    assemble->PlotSingleSolution(U, std::format("NumericalSolution{}", count));

    if (assemble->GetProblem().HasExactSolution()) {
      Vector exactSolution(0.0, U);
      assemble->get_exact_solution(exactSolution);
      assemble->PlotSingleSolution(exactSolution, std::format("ExactSolution{}", count));
    }
  }

  void ClearOldSolution() { oldSolution = nullptr; }

  void run(STAcousticSolution &solution) const override;

  void runAdjoint(STAcousticSolution &solution) const override {
    Vector &u = solution.vector;
    mout.StartBlock("STAcousticPDESolver");
    Vector RHS(0.0, u.GetSharedDisc(), u.Level());
    if (count == 0) RHS.PrintInfo();

    if (!M) {
      M = std::make_unique<Matrix>(RHS);
      assemble->System(*M, RHS);
      RHS.Clear();
    }
    assemble->RHS(RHS);

    assemble->MakeAdjointMatrix(*M);

    solver->operator()(*M, true);
    RHS -= (*M) * u;
    u += (*solver) * RHS;

    mout.EndBlock();
  }

  void EstimateErrorAndApplyAdaptivity(const Vector &U, AdaptiveConfig conf) {
    mout.StartBlock("Adaptivity");
    auto errorEstimator = CreateErrorEstimator(conf.data.errorEstimator, *assemble, *solver);
    Vector Eta = errorEstimator->EstimateError(*M, U, U.Level().adaptivityLevel);
    mout << "U.Level in EstimateErrorAndApplyAdaptivity" << U.Level() << endl;
    mout << DOUT(Eta.Level()) << endl;
    double theta = conf.data.theta;
    double theta_min = conf.data.theta_min;
    double theta_factor = conf.data.theta_factor;
    theta *= pow(theta_factor, Eta.Level().adaptivityLevel);
    assemble->AdaptPolynomialDegrees(Eta, theta, theta_min, conf.data.refine_by);
    assemble->AdaptQuadrature(Eta.Level().NextInAdaptivity(), -1, false);
    mout.EndBlock();
  }

  Vector CalculateMaterialUpdate(const Vector &material, const Vector &forwardSolution,
                                 const Vector &backwardSolution) {
    return assemble->CalculateMaterialUpdate(material, forwardSolution, backwardSolution);
  }
};

using STAcousticPDESolver = STAcousticPDESolverT<STDGViscoAcousticAssemble>;
using STAcousticPDESolverGL = STAcousticPDESolverT<STGLViscoAcousticAssemble>;
using STAcousticPDESolverMF = STAcousticPDESolverT<STDGMatrixFreeViscoAcousticAssemble>;
using STAcousticPDESolverPG = STAcousticPDESolverT<STPGViscoAcousticAssemble>;


#endif // SPACETIME_STACOUSTICMAIN_HPP

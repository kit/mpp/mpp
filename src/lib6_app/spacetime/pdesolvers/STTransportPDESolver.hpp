#ifndef STTRANSPORTMAIN_HPP
#define STTRANSPORTMAIN_HPP

#include "AdaptiveConfig.hpp"
#include "ErrorEstimator.hpp"
#include "LinearSolver.hpp"
#include "MeshesCreator.hpp"
#include "PDESolver.hpp"
#include "STDGTransportAssemble.hpp"
#include "TransportProblems.hpp"

struct STTransportSolution {
  explicit STTransportSolution(std::shared_ptr<const IDiscretization> disc) :
      vector(Vector(0.0, std::move(disc))) {}

  explicit STTransportSolution(std::shared_ptr<const IDiscretization> disc, MeshIndex meshIndex) :
      vector(Vector(0.0, std::move(disc), meshIndex)) {}

  explicit STTransportSolution(const Vector &vector) : vector(Vector(vector)) {}

  double computingTime{};

  double totalTime{};

  double initTime{};

  ValueMap values{};

  MultiValueMap timeDependentValues{};

  std::vector<Iteration> iterations;

  Vector vector;

  int verbose = 1;

  void PrintInfo() {
    mout.StartBlock("STTransportSolution");
    std::vector<PrintInfoEntry<double>> entries;
    for (auto &[key, value] : values) {
      entries.emplace_back(key, value, verbose = 1);
    }
    mout.PrintInfo("Solution", verbose, entries);

    if (timeDependentValues.empty()) return;

    std::vector<std::string> keys{"n", "t", "Mass", "Inflow", "IF_sum", "Outflow", "OF_sum"};

    for (auto &[key, _] : timeDependentValues) {
      if (auto found = std::find(keys.begin(), keys.end(), key); found == keys.end()) {
        keys.push_back(key);
      }
    }

    for (int n = 0; n < timeDependentValues.begin()->second.size(); ++n) {
      std::vector<PrintIterEntry<double>> timestep_entries;
      for (auto &key : keys) {
        auto valueList = timeDependentValues[key];
        timestep_entries.emplace_back(key, valueList[n], 14, verbose = 1);
      }
      mout.PrintIteration(verbose, timestep_entries);
    }
    mout.EndBlock(false);
  }
};

/*
void STTransportPDESolver::PrintValues(const Solution &solution) {
  if (verbose == 0) return;
  const std::map<std::string, std::vector<double>> &allValues = solution.multiValues;
  const Vector &u = solution.vector;
  if (assemble->GetProblem().HasExactSolution()) {
    for (int n = 0; n < allValues.begin()->second.size(); ++n) {
      double t = solution.vector.GetMesh().GetTimesteps()[n];
      std::vector<PrintIterEntry<double>> entries{
        {"n",      double(n),                5,  1},
        {"t",      t, 11, 1},
        {"Mass",   allValues.at("Mass")[n],    11, 1},
        {"IFR",    allValues.at("Inflow")[n],  11, 1},
        {"IF_sum", allValues.at("IF_sum")[n],  11, 1},
        {"OFR",    allValues.at("Outflow")[n], 11, 1},
        {"OF_sum", allValues.at("OF_sum")[n],  11, 1},
        {"Error",  assemble->L2SpaceNormAtTimeError(solution.vector,t),  11, 1}
      };
      mout.PrintIteration(verbose, entries);
    }
  } else {
    for (int n = 0; n < allValues.begin()->second.size(); ++n) {
      std::vector<PrintIterEntry<double>> entries{
        {"n",      double(n),                5,  1},
        {"t",      solution.vector.GetMesh().GetTimesteps()[n], 11, 1},
        {"Mass",   allValues.at("Mass")[n],    11, 1},
        {"IFR",    allValues.at("Inflow")[n],  11, 1},
        {"IF_sum", allValues.at("IF_sum")[n],  11, 1},
        {"OFR",    allValues.at("Outflow")[n], 11, 1},
        {"OF_sum", allValues.at("OF_sum")[n],  11, 1}
    };
    mout.PrintIteration(verbose, entries);
    }
  }
  auto values = solution.values.empty() ? ComputeValues(solution.vector) : solution.values;
  mout.PrintInfo("STTransport", verbose,
                 PrintInfoEntry("norm", values["norm"]),
                 PrintInfoEntry("L2_Norm", values["L2_Norm"]));
  if (assemble->GetProblem().HasExactSolution())
    mout.PrintInfo("STTransport", verbose,
                   PrintInfoEntry("L1_Error", values["L1_Error"]),
                   PrintInfoEntry("L2_Error", values["L2_Error"]),
                   PrintInfoEntry("GN_Error", values["GN_Error"]),
                   PrintInfoEntry("LInf_Error", values["LInf_Error"]));
  mout << "Problem " << assemble->GetProblem().Name() << endl;
}
*/


class STTransportPDESolver : public GenericPDESolver<ITransportProblem, STTransportSolution> {
private:
  std::unique_ptr<STDGTransportAssemble> assemble = nullptr;

  mutable std::unique_ptr<Matrix> M = nullptr;
  std::unique_ptr<LinearSolver> solver = nullptr;

  mutable int count = 0;
protected:
  void run(STTransportSolution &solution) const override;

  void createAssemble(const ITransportProblem &problem) override;

  void computeValues(STTransportSolution &solution) const override;
public:
  explicit STTransportPDESolver(const PDESolverConfig &conf) : GenericPDESolver(conf) {}

  STTransportPDESolver() : STTransportPDESolver(PDESolverConfig{}) {}

  std::shared_ptr<const IDiscretization> GetSharedDisc() const override {
    return assemble->GetSharedDisc();
  }

  std::string Name() const override { return "STTransport"; }

  void plotSolution(const Vector &U) const;

  void plotDegrees(const Vector &U) const;

  void plotVtu(const STTransportSolution &solution) const override;

  bool AdaptiveStep(Vector &u, int step);

  void EstimateErrorAndApplyAdaptivity(const Vector &U, AdaptiveConfig conf) {
    mout.StartBlock("Adaptivity");
    auto errorEstimator = CreateErrorEstimator(conf.data.errorEstimator, *assemble, *solver);
    Vector Eta = errorEstimator->EstimateError(*M, U, U.Level().adaptivityLevel);
    double theta = conf.data.theta;
    double theta_min = conf.data.theta_min;
    double theta_factor = conf.data.theta_factor;
    theta *= pow(theta_factor, Eta.Level().adaptivityLevel);
    assemble->AdaptPolynomialDegrees(Eta, theta, theta_min, conf.data.refine_by);
    assemble->AdaptQuadrature(Eta.Level().NextInAdaptivity(), -1, false);
    mout.EndBlock();
  }
};

#endif
#include "STAcousticPDESolver.hpp"
#include <format>

template class STAcousticPDESolverT<STDGViscoAcousticAssemble>;

template class STAcousticPDESolverT<STGLViscoAcousticAssemble>;

template class STAcousticPDESolverT<STDGMatrixFreeViscoAcousticAssemble>;

template class STAcousticPDESolverT<STPGViscoAcousticAssemble>;

template<class AcousticAssemble>
void STAcousticPDESolverT<AcousticAssemble>::createAssemble(const AcousticProblem &problem) {

  // TODO Remove this hack. For that the SetProblem must disappear!
  // Setters are bad practice anyway, To solve FWI problem use new problem builder strategy
  std::shared_ptr<AcousticProblem> problemPtr =
      std::shared_ptr<AcousticProblem>(const_cast<AcousticProblem *>(&problem),
                                       [](AcousticProblem *) {
                                         // Custom deleter does nothing to avoid double deletion
                                       });

  if (!assemble) {
    DegreePair degree(conf.degree, conf.timeDegree);
    assemble = std::make_unique<AcousticAssemble>(degree, problemPtr);
  } else {
    assemble->SetProblem(problemPtr);
  }
  std::string pathChoice = "none";
  Config::Get("PathChoice", pathChoice);
  solver = GetLinearSolverUnique(createPreconditioner(pathChoice, *assemble));

  Config::Get("doMeasure", measure);


  if (measure && !specification) {
    const Mesh &fineMesh = problemPtr->GetMeshes()[problemPtr->GetMeshes().FineLevel()];
    double dt = fineMesh.GetTimesteps()[1] / 4;
    specification = std::move(ObservationSpecificationBuilder()
                                  .WithReceiverCounts({20})
                                  .WithT(fineMesh.GetEndTime())
                                  .Withdt(dt)
                                  .WithWaypoints({Point(750, -350), Point(750, 1650)})
                                  .CreateUnique());
  }
}

template<class AcousticAssemble>
void STAcousticPDESolverT<AcousticAssemble>::run(STAcousticSolution &solution) const {
  Vector &u = solution.vector;
  if (oldSolution && conf.usePrevious) { u = *oldSolution; }

  mout.StartBlock("STAcousticPDESolver");
  mout << "Run" << endl;
  Vector RHS(0.0, u.GetSharedDisc(), u.Level());
  RHS.PrintInfo();
  RHS.GetMatrixGraph().MatrixMemoryInfo().PrintInfo();

  M = std::make_unique<Matrix>(RHS);
  assemble->System(*M, RHS);
  solver->operator()(*M, true);
  RHS -= (*M) * u;
  u += (*solver) * RHS;
  if (conf.usePrevious) { oldSolution = std::make_unique<Vector>(u); }
  mout.EndBlock();
}

template<class AcousticAssemble>
void STAcousticPDESolverT<AcousticAssemble>::plotVtu(const STAcousticSolution &solution) const {
  bool vtuPlot = false;
  Config::Get("VtuPlot", vtuPlot);
  if (!vtuPlot) return;
  const Vector &u = solution.vector;
  globalPlotBackendCreator = std::make_unique<DefaultSpaceTimeBackend>;
  plotParams(u);
  plotDegrees(u);
  plotSolution(u);
  vtuPlot = false;
  Config::Get("VtuPlotTimeSeries", vtuPlot);
  if (vtuPlot) {
    globalPlotBackendCreator = std::make_unique<SpaceTimeBackend>;
    plotDegrees(u);
    plotSolution(u);
  }
  count++;
}

template<class AcousticAssemble>
void STAcousticPDESolverT<AcousticAssemble>::computeValues(STAcousticSolution &solution) const {
  Vector &U = solution.vector;
  mout.StartBlock("Measure");
  if (measure && specification) {
    try {
      SeismogramData data = assemble->measure(*specification, U);
      solution.seismograms = std::make_shared<SeismogramData>(data);
      data.WriteToFile(std::format("data/seismo/seismogram{}", count));
    } catch (NotImplementedException &e) { mout << "Measuring not implemented."; }
  }

  mout.EndBlock(true);

  std::map<std::string, double> &values = solution.values;

  mout.StartBlock("ComputeNorms");

  std::vector<std::string> excludedResults;
  Config::Get("ExcludedResults", excludedResults);

  auto includeResult = [&excludedResults](const std::string &result) {
    return std::find(excludedResults.begin(), excludedResults.end(), result)
           == excludedResults.end();
  };

  double l2norm = 0;
  if (includeResult("L2Norm")) {
    l2norm = assemble->L2Norm(U);
    values["L2_Norm"] = l2norm;
  }
  solution.iterations.push_back(solver->GetIteration());

  values["SpaceLevel"] = U.Level().space;
  values["TimeLevel"] = U.Level().time;
  values["AdaptiveLevel"] = U.Level().adaptivityLevel;

  auto count_deg = U.GetMatrixGraph().GetDegreeCount();
  if (includeResult("CellCountOfDeg")) {
    for (const auto &[deg, deg_count] : count_deg) {
      values["CellCountOfDeg" + deg.toString()] = (double)deg_count;
    }
  }

  values["DoFs"] = U.pSize();
  values["MatrixAllocationSize"] = PPM->Sum(U.Size());

  values["Cells"] = U.GetMesh().CellCountGeometry();

  if (assemble->GetProblem().HasExactSolution()) {
    if (includeResult("L2Error")) {
      double l2error = assemble->L2Error(U);
      values["L2_Error"] = l2error;
      double l1error = assemble->L1Error(U);
      values["L1_Error"] = l1error;
      if (assemble->GetProblem().nL() == 0) {
        double dgerror = assemble->DGError(U);
        values["DG_Error"] = dgerror;
      }
      if (l2norm > 0) { values["L2_Error_relative"] = l2error / l2norm; }
    }
  }

  if (!includeResult("all")) {
    mout.EndBlock();
    return;
  }

  assemble->AdaptQuadrature(U.Level(), 6, true);

  if (includeResult("DGNorm")) {
    double dgnorm = assemble->DGNorm(U, *M);
    values["DGNorm"] = dgnorm;
  }
  if (includeResult("Conf") && assemble->HasConformingLagrangeInterpolation()) {
    Vector U_conf(0.0, U);
    double T = U.GetMesh().GetEndTime();
    assemble->ConformingLagrangeInterpolation(U, U_conf);

    values["||Lu_h - f||_Q"] = assemble->MhalfLuMinusF(U, "Residual_u_h");
    values["||Lu_conf - f||_Q"] = assemble->MhalfLuMinusF(U_conf, "Residual_u_conf");

    if (assemble->GetProblem().HasExactSolution()) {
      values["||Lu_exact -f||_Q"] = assemble->MhalfLu_exactMinusF(U.Level());
      values["||u_conf - u  ||_Q"] = assemble->L2Error(U_conf);
      values["||u_conf(T)-u(T)||_Omega"] = assemble->L2SpaceNormAtTimeError(U_conf, T);
      values["||u_conf(0)-u_h(0)||_Omega"] = assemble->L2SpaceNormAtTime(U_conf - U, 0);
      values["||u_conf(T)-u_h(T)||_Omega"] = assemble->L2SpaceNormAtTime(U_conf - U, T);
    }
  }

  if (includeResult("GoalFunctional")) {
    std::string goal_functional = "none";

    Config::Get("GoalFunctional", goal_functional);
    if (goal_functional != "none") {
      Point roi_min = Origin;
      Point roi_max = Origin;
      Config::Get("roi_min", roi_min);
      Config::Get("roi_max", roi_max);
      if (goal_functional == "linear") {
        values["GoalFunctional"] = assemble->Goal_Functional(U, roi_min, roi_max);
      } else if (goal_functional == "quadratic") {
        values["GoalFunctional"] = assemble->Energy(U, roi_min, roi_max);
      }
    }
  }

  double l1norm = 0;
  if (includeResult("L1Norm")) {
    l1norm = assemble->L1Norm(U);
    if (l1norm >= 0) { values["L1_Norm"] = l1norm; }
  }

  if (!assemble->GetProblem().HasExactSolution()) {
    assemble->AdaptQuadrature(U.Level(), -1, true);
    mout.EndBlock();
    return;
  }

  if (includeResult("L1Error")) {
    double l1error = values["L1_Error"];
    if (l1norm > 0) { values["L1_Error_relative"] = l1error / l1norm; }
  }
  double linferror = assemble->LInfError(U);
  values["LInf_Error"] = linferror;

  if (includeResult("DGError")) {
    double dgerror = assemble->DGError(U);
    if (dgerror > 0) { values["DG_Error"] = dgerror; }
  }

  Vector exactSol_int(0.0, U);
  Vector exactSol_proj(0.0, U);
  assemble->get_exact_solution(exactSol_int);
  assemble->get_projected_exact_solution(exactSol_proj);

  if (includeResult("exact")) {
    values["||Lu_int - f||_Q"] = assemble->MhalfLuMinusF(exactSol_int, "Residual_u_int");
    values["||Lu_proj - f||_Q"] = assemble->MhalfLuMinusF(exactSol_proj, "Residual_u_proj");

    if (includeResult("DG_Int_Error")) { values["DG_Int_Error"] = assemble->DGError(exactSol_int); }

    if (includeResult("||u_int-u_h||_DG")) {
      values["||u_int-u_h||_DG"] = assemble->DGNorm(exactSol_int - U, *M);
    }

    if (includeResult("||u_proj-u_h||_DG")) {
      values["||u_proj-u_h||_DG"] = assemble->DGNorm(exactSol_proj - U, *M);
    }
  }

  Vector error = U - exactSol_int;
  Vector error_proj = U - exactSol_proj;


  if (includeResult("L2SpaceAtT")) {
    bool includeL2SpaceAtT_int = includeResult("L2SpaceAtT_int");
    for (double t : U.GetMeshes().coarse().GetTimesteps()) {
      std::stringstream s;
      s << "L2SpaceAtT" << std::to_string(t) /*.substr(0,4)*/ << "_error";
      double value = assemble->L2SpaceNormAtTimeError(U, t);
      if (value != -1) { values[s.str()] = value; }
      if (includeL2SpaceAtT_int) {
        value = assemble->L2SpaceNormAtTimeError(exactSol_int, t);
        if (value != -1) { values[s.str() + "_int"] = value; }
      }
    }
  }

  if (includeResult("projError")) {
    values["L1_proj_Error"] = assemble->L1Error(exactSol_proj);
    values["L2_proj_Error"] = assemble->L2Error(exactSol_proj);
    values["LInf_proj_Error"] = assemble->LInfError(exactSol_proj);
  }
  if (includeResult("intError")) {
    values["L1_int_Error"] = assemble->L1Error(exactSol_int);
    values["L2_int_Error"] = assemble->L2Error(exactSol_int);
    values["LInf_int_Error"] = assemble->LInfError(exactSol_int);
  }
  if (includeResult("EE")) {
    ResidualErrorEstimator ee_res(*assemble, *solver);
    Vector Eta_res = ee_res.EstimateError(*M, U, -1);
    values["eta_(res,h)"] = norm(Eta_res);

    if (includeResult("Conf") && assemble->HasConformingLagrangeInterpolation()) {
      L2ReliableResidualErrorEstimator ee_l2(*assemble, *solver);
      DGErrorEstimator ee_dg(*assemble, *solver);
      Vector Eta_l2 = ee_l2.EstimateError(*M, U, -1);
      values["eta_(L2,h)"] = norm(Eta_l2);
      Vector Eta_dg = ee_dg.EstimateError(*M, U, -1, Eta_res, Eta_l2);
      values["eta_(dg,h)"] = norm(Eta_dg);
    }
  }

  assemble->AdaptQuadrature(U.Level(), -1, true);
  mout.EndBlock(verbose == 0);
}

#include "STMFViscoAcousticElement.hpp"

STMFViscoAcousticElement::STMFViscoAcousticElement(const VectorMatrixBase &g, const cell &tc,
                                                   int nL, bool max_quad_order) :
    shape(GetSpaceTimeShape(g, *tc)), quad(GetSpaceTimeQuadrature(g, *tc)), TC(tc),
    r(g.find_row(tc())), dimension(tc.dim()), gradients(quad.size(), shape.size()),
    dampingComponentCount(nL),
    space_quad(max_quad_order ? GetSpaceTimeQuadratureHighOrder(g).GetSpaceQuad() :
                                quad.GetSpaceQuad()),
    component_to_index(tc.SpaceCell().dim() + 1 + nL, std::vector<size_t>{}) {
  qWeight.resize(nQ());
  qPoint.resize(nQ());
  velocityST.resize(nQ() * j_dimension());
  dtVelocityST.resize(nQ() * j_dimension());
  divVelocityST.resize(nQ() * j_dimension());

  pressureST.resize(nQ() * j_dimension());
  dtPressureST.resize(nQ() * j_dimension());
  gradPressureST.resize(nQ() * j_dimension());

  for (size_t i = 0; i < i_dimension(); i++) {
    component_to_index[GetComponent(i)].push_back(i);
  }

  const Transformation &T = TC.GetTransformation();

  det_time = T.DetTime();
  det_space = T.DetSpace();
  for (int q = 0; q < quad.size(); q++) {
    qWeight[q] = T.Det() * quad.Weight(q);
    qPoint[q] = TC.LocalToGlobal(quad.QPoint(q));
    for (int i = 0; i < shape.size(); i++) {
      VectorField STGrad = shape.LocalSpaceTimeGradient(q, i);
      gradients[q][i] = T * STGrad;
    }
  }

  const size_t allDimensions = (1 + dimension + dampingComponentCount);
  for (size_t quadIndex = 0; quadIndex < quad.size(); quadIndex++) {
    const size_t offset = quadIndex * allDimensions * shape.size();
    for (size_t dim = 0; dim < allDimensions; dim++) {
      const auto dimOffset = offset + dim * shape.size();
      for (size_t shapeIndex = 0; shapeIndex < shape.size(); shapeIndex++) {
        const auto shapeOffset = dimOffset + shapeIndex;
        if (dim < dimension) {
          velocityST[shapeOffset][dim] = shape(quadIndex, shapeIndex);
          dtVelocityST[shapeOffset][dim] = gradients[quadIndex][shapeIndex].t();
          divVelocityST[shapeOffset] = gradients[quadIndex][shapeIndex][dim];
        } else {
          pressureST[shapeOffset] = shape(quadIndex, shapeIndex);
          dtPressureST[shapeOffset] = gradients[quadIndex][shapeIndex].t();
          gradPressureST[shapeOffset] = gradients[quadIndex][shapeIndex].sliceTime();
        }
      }
    }
  }
}

int STMFViscoAcousticElement::nQ() const { return quad.size(); }

int STMFViscoAcousticElement::nSpaceQ() const { return space_quad.size(); }

int STMFViscoAcousticElement::nTimeQ() const { return quad.GetTimeQuad().size(); }

double STMFViscoAcousticElement::QWeight(int q) const { return qWeight[q]; }

double STMFViscoAcousticElement::SpaceQWeight(int qq) {
  if (space_qWeight.empty()) {
    space_qWeight.resize(space_quad.size());
    for (int q = 0; q < space_qWeight.size(); q++) {
      space_qWeight[q] = det_space * space_quad.Weight(q);
    }
  }
  return space_qWeight[qq];
}

Point STMFViscoAcousticElement::SpaceQPoint(int q) const { return space_quad.QPoint(q); }

double STMFViscoAcousticElement::TimeQWeight(int qq) {
  if (time_qWeight.empty()) {
    time_qWeight.resize(quad.GetTimeQuad().size());
    for (int q = 0; q < time_qWeight.size(); q++) {
      time_qWeight[q] = det_time * quad.GetTimeQuad().Weight(q);
    }
  }
  return time_qWeight[qq];
}

const Point &STMFViscoAcousticElement::QPoint(int q) const { return qPoint[q]; }

int STMFViscoAcousticElement::i_dimension() const {
  return shape.size() * (1 + dimension + dampingComponentCount);
}

double STMFViscoAcousticElement::Area() const {
  double a = 0;
  for (int q = 0; q < nQ(); ++q)
    a += qWeight[q];
  return a;
}

int STMFViscoAcousticElement::j_dimension() const {
  return shape.size() * (1 + dimension + dampingComponentCount);
}

int STMFViscoAcousticElement::variable(int j) const {
  const size_t shapeSize = shape.size();
  const size_t tmp = j % (shapeSize * (dimension + 1 + dampingComponentCount));
  if (tmp < dimension * shapeSize) return 0;
  for (int nL = 0; nL <= dampingComponentCount; nL++)
    if (tmp - shapeSize * dimension < (nL + 1) * shapeSize) return 1 + nL;
  Exit("BLUPP");
}

int STMFViscoAcousticElement::GetComponent(int j) const {
  return (j % (shape.size() * (dimension + 1 + dampingComponentCount))) / shape.size();
}

double STMFViscoAcousticElement::EvaluateComponentGlobal(const Point &p, const Vector &u,
                                                         int component) const {
  Point localPoint = TC.GlobalToLocal(p);
  return EvaluateComponentLocal(localPoint, u, component);
}

double STMFViscoAcousticElement::EvaluateComponentLocal(const Point &p, const Vector &u,
                                                        int component) const {
  double value = 0.0;
  for (size_t i : GetIndicesForComponent(component)) {
    size_t shapeIndex = i % shape.size();
    value += u(r, i) * shape(p, shapeIndex);
  }
  return value;
}

double STMFViscoAcousticElement::EvaluateComponent(int nq, const Vector &u, int component) const {
  THROW("check order of Dofs")
  double value = 0.0;

  for (int i : GetIndicesForComponent(component)) {
    int ti = i / (shape.GetSpaceShape().size() * GetComponentCount());
    int si = i % (shape.GetSpaceShape().size() * GetComponentCount());
    int ssj = si % shape.GetSpaceShape().size();
    int shape_iddx = ti * shape.GetSpaceShape().size() + ssj;
    value += u(r, i) * shape(nq, shape_iddx);
  }

  return value;
}

VectorField STMFViscoAcousticElement::Velocity(int nq, int j) const {
  return velocityST[nq * j_dimension() + j];
}

VectorField STMFViscoAcousticElement::Velocity(int nq, const Vector &U) const {
  VectorField V = zero;
  for (int j = 0; j < j_dimension(); ++j) {
    V += U(r, j) * Velocity(nq, j);
  }
  return V;
}

VectorField STMFViscoAcousticElement::VelocityLocal(const Point &localPoint, int j) const {
  int var = variable(j);
  if (var != 0) return zero;
  const size_t shapeIndex = j % shape.size();
  const size_t vComponent = j / shape.size();
  VectorField V = zero;
  V[vComponent] = shape(localPoint, shapeIndex);
  return V;
}

VectorField STMFViscoAcousticElement::VelocityLocal(const Point &localPoint,
                                                    const Vector &u) const {
  VectorField V = zero;
  for (int j = 0; j < j_dimension(); ++j)
    V += u(r, j) * VelocityLocal(localPoint, j);
  return V;
}

VectorField STMFViscoAcousticElement::VelocityGlobal(const Point &globalPoint,
                                                     const Vector &u) const {
  Point localPoint = TC.GlobalToLocal(globalPoint);
  return VelocityLocal(localPoint, u);
}

VectorField STMFViscoAcousticElement::DtVelocity(int nq, int j) const {
  return dtVelocityST[nq * j_dimension() + j];
}

VectorField STMFViscoAcousticElement::DtVelocity(int nq, const Vector &u) const {
  VectorField V = zero;
  for (int j = 0; j < j_dimension(); ++j)
    V += u(r, j) * DtVelocity(nq, j);
  return V;
}

double STMFViscoAcousticElement::DivVelocity(int nq, int j) const {
  return divVelocityST[nq * j_dimension() + j];
}

double STMFViscoAcousticElement::DivVelocity(int nq, const Vector &u) const {
  double divV = 0.0;
  for (int j = 0; j < j_dimension(); ++j)
    divV += u(r, j) * DivVelocity(nq, j);
  return divV;
}

double STMFViscoAcousticElement::Pressure(int nq, int j) const {
  return pressureST[nq * j_dimension() + j];
}

double STMFViscoAcousticElement::Pressure(int nq, const Vector &U) const {
  double P = 0.0;
  for (int j : component_to_index[TC.SpaceCell().dim()]) {
    P += U(r, j) * Pressure(nq, j);
  }
  return P;
}

DampingVector STMFViscoAcousticElement::DampingPressure(int nq, const Vector &U) const {
  DampingVector DP(dampingComponentCount);
  const auto spaceDimension = TC.SpaceCell().dim();
  for (int i = 0; i < dampingComponentCount; ++i) {
    for (int j : component_to_index[spaceDimension + 1 + i]) {
      DP[i] += U(r, j) * Pressure(nq, j);
    }
  }
  return DP;
}

double STMFViscoAcousticElement::PressureLocal(const Point &localPoint, int j) const {
  int var = variable(j);
  if (var == 0) return 0.0;
  const size_t shapeIndex = j % shape.size();
  return shape(localPoint, shapeIndex);
}

double STMFViscoAcousticElement::PressureLocal(const Point &localPoint, const Vector &u) const {
  double P = 0;
  for (int j = 0; j < j_dimension(); ++j)
    P += u(r, j) * PressureLocal(localPoint, j);
  return P;
}

double STMFViscoAcousticElement::PressureGlobal(const Point &globalPoint, int i) const {
  Point localPoint = TC.GlobalToLocal(globalPoint);
  return PressureLocal(localPoint, i);
}

double STMFViscoAcousticElement::PressureGlobal(const Point &globalPoint, const Vector &u) const {
  Point localPoint = TC.GlobalToLocal(globalPoint);
  return PressureLocal(localPoint, u);
}

double STMFViscoAcousticElement::DtPressure(int nq, int j) const {
  return dtPressureST[nq * j_dimension() + j];
}

double STMFViscoAcousticElement::DtPressure(int nq, const Vector &u) const {
  double dtP = 0;
  for (int j = 0; j < j_dimension(); ++j)
    dtP += u(r, j) * DtPressure(nq, j);
  return dtP;
}

VectorField STMFViscoAcousticElement::GradPressure(int nq, int j) const {
  return gradPressureST[nq * j_dimension() + j];
}

VectorField STMFViscoAcousticElement::GradPressure(int nq, const Vector &u) const {
  VectorField gradP = zero;
  for (int j = 0; j < j_dimension(); ++j)
    gradP += u(r, j) * GradPressure(nq, j);
  return gradP;
}
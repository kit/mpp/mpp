#ifndef MPP_CONVERGENCESTUDY_HPP
#define MPP_CONVERGENCESTUDY_HPP

#include "PDESolver.hpp"
#include "Matrix.hpp"
#include "Transfers.hpp"
#include "Plotting.hpp"

#include "SingleExperiment.hpp"

#include <ranges>

std::map<std::string, std::vector<double>>
convertResults(std::vector<std::map<std::string, double>> results);

template<class SolutionType>
std::vector<std::map<std::string, double>>
getInternalValues(const std::vector<SolutionType> &solutions) {
  std::vector<std::map<std::string, double>> results;
  std::ranges::transform(solutions, std::back_inserter(results),
                         [](auto sol) { return sol.values; });
  return results;
}

template<class PDESolverType>
class ConvergenceStudy {
private:
  PDESolverType pdeSolver;
  using ProblemClass = typename PDESolverType::ProblemClass;
  using SolutionClass = typename PDESolverType::SolutionClass;

  std::vector<MeshIndex> getLevels() {
    const auto &conf = pdeSolver.GetConfig();
    std::vector<MeshIndex> levels;
    std::ranges::transform(std::ranges::iota_view(conf.pLevel, conf.level + 1),
                           std::back_inserter(levels),
                           [](int level) { return MeshIndex{.space = level, .time = level}; });
    return levels;
  }
public:
  int verbose = 0;

  explicit ConvergenceStudy(PDESolverConfig conf) : pdeSolver(conf) {
    Config::Get("ResultsVerbose", verbose);
  }

  auto Method(std::shared_ptr<ProblemClass> problem) {
    std::vector<SolutionClass> solutions;
    for (MeshIndex level : getLevels()) {
      mout << "Convergence on Level " << level.str() << endl;
      solutions.push_back(pdeSolver.Run(problem, level));
      solutions.back().PrintInfo();
      Plotting::Instance().Clear();
    }
    auto results = getInternalValues<SolutionClass>(solutions);
    mout.PrintEntries("Convergence Results", verbose, convertResults(results));
    return solutions;
  }
};

template<class PDESolver>
auto RunConvergenceExperiment(PDESolverConfig conf) {
  ConvergenceStudy<PDESolver> confStudy(conf);
  std::shared_ptr<typename PDESolver::ProblemClass> problem;
  CreateProblemShared(conf.problemName, problem);
  return confStudy.Method(problem);
}

std::vector<std::map<std::basic_string<char>, double>>
RunConvergenceExperimentForValues(PDESolverConfig conf);

#endif

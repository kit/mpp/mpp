#include "ConvergenceStudy.hpp"

#if USE_SPACETIME
#include "STAcousticPDESolver.hpp"
#include "STTransportPDESolver.hpp"
#endif


std::map<std::string, std::vector<double>>
convertResults(std::vector<std::map<std::string, double>> results) {
  std::map<std::string, std::vector<double>> res;
  for (size_t level = 0; level < results.size(); level++) {
    for (auto &[name, value] : results[level]) {
      res.try_emplace(name, results.size());
      res[name][level] = value;
    }
  }
  return res;
}

std::vector<std::map<std::string, double>> RunConvergenceExperimentForValues(PDESolverConfig conf) {
  if (conf.modelName.find("Elliptic") != std::string::npos) {
    THROW("implement CreateConvergenceStudy(PDESolverConfig conf) ")
  }

  if (conf.modelName.find("DGTransport") != std::string::npos) {
    THROW("implement CreateConvergenceStudy(PDESolverConfig conf) ")
  }

  if (conf.modelName.find("Reaction") != std::string::npos) {
    THROW("implement CreateConvergenceStudy(PDESolverConfig conf) ")
  }
#if USE_SPACETIME
  if (conf.modelName == "STAcoustic") {
    auto solutions = RunConvergenceExperiment<STAcousticPDESolver>(conf);
    return getInternalValues(solutions);
  }
  if (conf.modelName == "STAcousticGL") {
    auto solutions = RunConvergenceExperiment<STAcousticPDESolverGL>(conf);
    return getInternalValues(solutions);
  }
  if (conf.modelName == "STAcousticMF") {
    auto solutions = RunConvergenceExperiment<STAcousticPDESolverMF>(conf);
    return getInternalValues(solutions);
  }
  if (conf.modelName == "STAcousticPG") {
    auto solutions = RunConvergenceExperiment<STAcousticPDESolverPG>(conf);
    return getInternalValues(solutions);
  }
  if (conf.modelName == "STTransport") {
    auto solutions = RunConvergenceExperiment<STTransportPDESolver>(conf);
    return getInternalValues(solutions);
  }
#endif
  if (conf.modelName.find("VectorValued") != std::string::npos) {
    THROW("implement CreateConvergenceStudy(PDESolverConfig conf) ")
  }

  if (conf.modelName.find("Acoustic") != std::string::npos) {
    THROW("implement CreateConvergenceStudy(PDESolverConfig conf) ")
  }

  Exit(conf.modelName + " not found")
}

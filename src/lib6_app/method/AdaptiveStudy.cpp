#include "AdaptiveStudy.hpp"

#if USE_SPACETIME

#include "STAcousticPDESolver.hpp"
#include "STTransportPDESolver.hpp"

#endif

std::vector<std::map<std::basic_string<char>, double>>
RunAdaptiveExperimentForValues(PDESolverConfig conf, AdaptiveConfig aconf) {
  if (conf.modelName.find("Elliptic") != std::string::npos) {
    THROW("implement CreateConvergenceStudy(PDESolverConfig conf) ")
  }

  if (conf.modelName.find("DGTransport") != std::string::npos) {
    THROW("implement CreateConvergenceStudy(PDESolverConfig conf) ")
  }

  if (conf.modelName.find("Reaction") != std::string::npos) {
    THROW("implement CreateConvergenceStudy(PDESolverConfig conf) ")
  }
#if USE_SPACETIME
  if (conf.modelName == "STAcoustic" || conf.modelName == "STAcousticPDESolver") {
    auto solutions = RunAdaptiveExperiment<STAcousticPDESolver>(conf, aconf);
    return getInternalValues(solutions);
  }
  if (conf.modelName == "STAcousticGL") {
    auto solutions = RunAdaptiveExperiment<STAcousticPDESolver>(conf, aconf);
    return getInternalValues(solutions);
  }
  if (conf.modelName == "STAcousticMF") {
    auto solutions = RunAdaptiveExperiment<STAcousticPDESolver>(conf, aconf);
    return getInternalValues(solutions);
  }
  if (conf.modelName == "STAcousticPG") {
    auto solutions = RunAdaptiveExperiment<STAcousticPDESolverPG>(conf, aconf);
    return getInternalValues(solutions);
  }
  if (conf.modelName == "STTransport") {
    auto solutions = RunAdaptiveExperiment<STTransportPDESolver>(conf, aconf);
    return getInternalValues(solutions);
  }
#endif
  if (conf.modelName.find("VectorValued") != std::string::npos) {
    THROW("implement CreateConvergenceStudy(PDESolverConfig conf) ")
  }
  if (conf.modelName.find("Acoustic") != std::string::npos) {
    THROW("implement CreateConvergenceStudy(PDESolverConfig conf) ")
  }
  Exit(conf.modelName + " not found")
}

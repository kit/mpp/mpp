#ifndef MPP_SINGLEEXPERIMENT_HPP
#define MPP_SINGLEEXPERIMENT_HPP

#include "PDESolver.hpp"

template<class PDESolver>
auto RunSingleExperiment(PDESolverConfig conf) {
  PDESolver pdeSolver(conf);
  std::shared_ptr<typename PDESolver::ProblemClass> problem;
  CreateProblemShared(conf.problemName, problem);
  auto solution = pdeSolver.Run(problem);
  solution.PrintInfo();
  return solution;
}

std::map<std::string, double> RunSingleExperimentForValues(const PDESolverConfig &conf);

#endif // MPP_SINGLEEXPERIMENT_HPP

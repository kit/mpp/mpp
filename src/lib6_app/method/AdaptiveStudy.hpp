#ifndef MPP_ADAPTIVESTUDY_HPP
#define MPP_ADAPTIVESTUDY_HPP

#include "ConvergenceStudy.hpp"
#include "AdaptiveConfig.hpp"

template<class PDESolverType>
class AdaptiveStudy {
private:
  using ProblemClass = typename PDESolverType::ProblemClass;
  using SolutionClass = typename PDESolverType::SolutionClass;

  std::vector<MeshIndex> getLevelsForAdaptivity(MeshIndex level) const {
    std::vector<MeshIndex> levels{level};
    MeshIndex current = level;
    bool redistribute = false;
    Config::Get("redistribute", redistribute);
    do {
      current = current.NextInAdaptivity();
      if (redistribute) current = current.NextInReDistribution();
      levels.push_back(current);
    } while (current.adaptivityLevel != aconf.data.refinement_steps);
    return levels;
  }
public:
  const AdaptiveConfig aconf;
  PDESolverType pdeSolver;

  int verbose = 1;

  explicit AdaptiveStudy(PDESolverConfig conf, AdaptiveConfig aconf) :
      pdeSolver(std::move(conf)), aconf(std::move(aconf)) {
    Config::Get("ResultsVerbose", verbose);
  }

  auto Method(std::shared_ptr<ProblemClass> problem) {
    std::vector<SolutionClass> solutions;
    auto levels = getLevelsForAdaptivity(problem->GetMeshes().FineLevel());
    for (MeshIndex level : levels) {
      mout << "Adaptive Step on Level " << level.str() << endl;
      auto solution = pdeSolver.Run(problem, level);
      solution.PrintInfo();
      solutions.push_back(solution);
      if (level != levels.back()) {
        this->pdeSolver.EstimateErrorAndApplyAdaptivity(solution.vector, this->aconf);
      }
      Plotting::Instance().Clear();
    }
    auto results = getInternalValues(solutions);
    mout.PrintEntries("Convergence Results", verbose, convertResults(results));
    return solutions;
  }
};

template<class PDESolver>
auto RunAdaptiveExperiment(PDESolverConfig conf, AdaptiveConfig aconf) {
  AdaptiveStudy<PDESolver> confStudy(conf, aconf);
  std::shared_ptr<typename PDESolver::ProblemClass> problem;
  CreateProblemShared(conf.problemName, problem);
  return confStudy.Method(problem);
}

std::vector<std::map<std::string, double>> RunAdaptiveExperimentForValues(PDESolverConfig conf,
                                                                          AdaptiveConfig aconf);

#endif

#include "SingleExperiment.hpp"
#include "AcousticPDESolver.hpp"
#include "EllipticPDESolver.hpp"
#include "GelfandPDESolver.hpp"
#include "PlatePDESolver.hpp"
#include "ReactionPDESolver.hpp"
#include "TransportPDESolver.hpp"
#include "VectorValuedMain.hpp"

#ifdef USE_SPACETIME

#include "AcousticProblems.hpp"
#include "STAcousticPDESolver.hpp"
#include "STTransportPDESolver.hpp"

#endif


std::map<std::string, double> RunSingleExperimentForValues(const PDESolverConfig &conf) {
  if (conf.modelName.find("Elliptic") != std::string::npos) {
    return RunSingleExperiment<EllipticPDESolver>(conf).values;
  }
  if (conf.modelName.find("DGTransport") != std::string::npos) {
    return RunSingleExperiment<TransportPDESolver>(conf).values;
  }
  if (conf.modelName.find("Reaction") != std::string::npos) {
    return RunSingleExperiment<ReactionPDESolver>(conf).values;
  }
  if (conf.modelName.find("Gelfand") != std::string::npos) {
    return RunSingleExperiment<GelfandPDESolver>(conf).values;
  }
  if (conf.modelName.find("Plate") != std::string::npos) {
    return RunSingleExperiment<PlatePDESolver>(conf).values;
  }
// Todo use these two also for elliptic problems and cleanup plotting hacks
//  if (conf.modelName.find("Hybrid") != std::string::npos) {
//    return RunSingleExperiment<HybridPDESolver>(conf).values;
//  }
//  if (conf.modelName.find("Mixed") != std::string::npos) {
//    return RunSingleExperiment<MixedMain>(conf).values;
//  }
#ifdef USE_SPACETIME
  if (conf.modelName == "STAcousticPDESolver" || conf.modelName == "STAcoustic") {
    return RunSingleExperiment<STAcousticPDESolver>(conf).values;
  }
  if (conf.modelName == "STAcousticGL") {
    return RunSingleExperiment<STAcousticPDESolverGL>(conf).values;
  }
  if (conf.modelName == "STAcousticMF") {
    return RunSingleExperiment<STAcousticPDESolverMF>(conf).values;
  }
  if (conf.modelName == "STAcousticPG") {
    return RunSingleExperiment<STAcousticPDESolverPG>(conf).values;
  }
  if (conf.modelName == "STTransport") {
    return RunSingleExperiment<STTransportPDESolver>(conf).values;
  }
#endif
  if (conf.modelName.find("VectorValued") != std::string::npos) {
    return RunSingleExperiment<VectorValuedMain>(conf).values;
  }
  if (conf.modelName.find("Acoustic") != std::string::npos) {
    return RunSingleExperiment<AcousticPDESolver>(conf).values;
  }

  Exit(conf.modelName + " not found")
}

#include "VectorAccess.hpp"

#include <algorithm>
#include <cstddef>
#include <iterator>

#include "ICell.hpp"
#include "IMatrixGraph.hpp"
#include "MixedDoF.hpp"
#include "RowIterators.hpp"
#include "Vector.hpp"

RowValues::RowValues(Vector &u, const rows &R) : a(R.size()) {
  std::transform(std::cbegin(R), std::cend(R), std::begin(a),
                 [&u](const auto &value) { return u(value); });
}

RowValues::RowValues(Vector &u, const Cell &c) : RowValues(u, rows(u.GetMatrixGraph(), c)) {}

RowValuesVector::RowValuesVector(Vectors &u, const rows &R) {
  for (const auto &value : u) {
    rowvalues.emplace_back(*value, R);
  }
}

RowValuesVector::RowValuesVector(Vectors &u, const Cell &c) :
    RowValuesVector(u, rows(u.GetMatrixGraph(), c)) {}

RowBndValues::RowBndValues(Vector &u, const Cell &c) : BF(u.GetMesh(), c), a(0), b(0) {
  rows R(u.GetMatrixGraph(), c);
  a.resize(R.size());
  b.resize(R.size());
  for (size_t i = 0; i < R.size(); ++i) {
    a[i] = u(R[i]);
    b[i] = u.D(R[i]);
  }
}

MixedRowValues::MixedRowValues(Vector &u, const Cell &c) :
    MixedRowValues(u, c, rows(u.GetMatrixGraph(), c)) {}

MixedRowValues::MixedRowValues(Vector &u, const Cell &c, const rows &R) : a(u.NumberOfDoFs()) {
  const MixedDoF &mixedDoF = MixedDoF::Cast(u.GetDoF());
  for (int n = 0; n < u.NumberOfDoFs(); ++n) {
    const std::vector<SPData> &data_n = mixedDoF.StoragePointData(n, c);
    a[n].resize(data_n.size());
    std::transform(std::cbegin(data_n), std::cend(data_n), std::begin(a[n]),
                   [&](const auto &value) { return &u(R[value.index], value.shift); });
  }
}

MixedRowValuesVector::MixedRowValuesVector(Vectors &u, const Cell &c, const rows &R) {
  for (auto &value : u) {
    rowvalues.emplace_back(*value, c, R);
  }
}

MixedRowValuesVector::MixedRowValuesVector(Vectors &u, const Cell &c) :
    MixedRowValuesVector(u, c, rows(u.GetMatrixGraph(), c)) {}

MixedRowBndValues::MixedRowBndValues(Vector &u, const Cell &c, const rows &R) :
    BF(u.GetMesh(), c), a(u.NumberOfDoFs()), b(u.NumberOfDoFs()) {
  const MixedDoF &mixedDoF = MixedDoF::Cast(u.GetDoF());
  for (int n = 0; n < u.NumberOfDoFs(); ++n) {
    const std::vector<SPData> &data_n = mixedDoF.StoragePointData(n, c);
    a[n].resize(data_n.size());
    b[n].resize(data_n.size());

    for (size_t i = 0; i < a[n].size(); ++i) {
      a[n][i] = &u(R[data_n[i].index], data_n[i].shift);
      b[n][i] = &u.D(R[data_n[i].index], data_n[i].shift);
    }
  }
}

MixedRowBndValues::MixedRowBndValues(Vector &u, const Cell &c) :
    MixedRowBndValues(u, c, rows(u.GetMatrixGraph(), c)) {}

DGSizedRowBndValues::DGSizedRowBndValues(Vector &u, const Cell &c, int elemN) :
    BF(u.GetMesh(), c), elemSize(elemN) {
  row r = u.find_row(c());
  a = u(r);
  b = u.D(r);
}

DGRowValues::DGRowValues(Vector &u, const row &r, int elemN) : a(u(r)), elemSize(elemN) {}

DGRowValues::DGRowValues(Vector &u, const Cell &c, int elemN) :
    DGRowValues(u, u.find_row(c()), elemN) {}

// RowFaceValues::RowFaceValues(int face, Vector &u, const Cell &c, const rows &R, int n) {
//   const std::vector<NPData> &data_face_n = u.NodalPointFaceData(n, c, face);
//   a.resize(data_face_n.size());
//   for (int i = 0; i < a.size(); ++i)
//     a[i] = u(R[data_face_n[i].index]) + data_face_n[i].shift;
// }
//
// RowBndFaceValues::RowBndFaceValues(int face, Vector &u, const Cell &c, const rows &R, int n) {
//   const std::vector<NPData> &data_face_n = u.NodalPointFaceData(n, c, face);
//   a.resize(data_face_n.size());
//   b.resize(data_face_n.size());
//   for (int i = 0; i < a.size(); ++i) {
//     a[i] = u(R[data_face_n[i].index]) + data_face_n[i].shift;
//     b[i] = u.D(R[data_face_n[i].index]) + data_face_n[i].shift;
//   }
// }

PartRowBndValues::PartRowBndValues(Vector &u, const rows &R, const Cell &c,
                                   const std::ptrdiff_t mpIndex) :
    BF(u.GetMesh(), c), a(R.size()), b(R.size()) {
  for (size_t i = 0; i < R.size(); ++i) {
    int q = 0;
    if (R[i].NumberOfDofs() > 1) q = mpIndex;
    a[i] = &u(R[i], q);
    b[i] = &u.D(R[i], q);
  }
}

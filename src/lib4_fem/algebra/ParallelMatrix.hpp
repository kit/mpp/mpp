#ifndef PARALLELMATRIX_HPP
#define PARALLELMATRIX_HPP

#include <cstddef>
#include <memory>
#include <vector>

#include "Communicator.hpp"
#include "Sparse.hpp"

#include "Lapdef.hpp"

struct pardec {
  int size;
  int proc;
  int shift;
};

class ParallelMatrix {
protected:
  Communicator &C; // nur Referenz
  mutable std::vector<double> a;
  std::vector<int> IPIV; // perhaps std::vector<int*> for different IPIVs
  int local_col;         // sum = col
  int local_row;
  int row;
  int col;
  //     std::vector <double*> local_dec;
  std::vector<pardec> dec; // decomposition

  std::unique_ptr<Communicator> C_loc; // falls weniger kommuniziert werden soll...

  std::vector<double> other_column; // for Communication
  std::vector<double> other_row;
  std::vector<int> other_IPIV;
  int info;

  void Create_Distribution(int MINSIZE, int maxP);

  void Create() { a = std::vector<double>(local_row * local_col); }
public:
  ParallelMatrix(int r, int c, Communicator &PC, int matrixsize = 0, int maxP = 0) :
      C(PC), IPIV(0), local_col(0), local_row(0), row(r), col(c), dec(0), C_loc(nullptr),
      other_column(0), other_row(0), other_IPIV(0) {
    if (row == 0 || col == 0) return;
    Define_ParallelDistribution(matrixsize, maxP);
  }

  ParallelMatrix(int r, const ParallelMatrix &PM, int matrixsize = 0, int maxP = 0);

  ParallelMatrix(Communicator &PC) :
      C(PC), IPIV(0), local_col(0), local_row(0), row(0), col(0), dec(0), C_loc(nullptr),
      other_column(0), other_row(0), other_IPIV(0) {}

  inline void Define_ParallelDistribution(int MINSIZE = 0, int maxP = 0) {
    Create_Distribution(MINSIZE, maxP);
    local_row = row;
    Create();
  }

  inline int rows() const noexcept { return row; }

  inline int cols() const noexcept { return col; }

  inline int cols_loc() const noexcept { return local_col; }

  void makeLU(int i);

  void Solve(int i, double *rhs, int nrhs = 1, int size_rhs = 1);

  void Solve_own(int);

  void MMM_own(int); // Matrix-Matrix-Multiplication

  void MMM_left(double *, int, pardec);

  void MMM_right(double *, pardec);

  void MMM_schur(double *, double *, int, pardec);

  void MV_rhs(int, double *, int, int);

  void MV_rhs_left(int, int, double *, int, int);

  void MV_rhs_right(int, double *, int, int);

  void MV_rhs_Z(int, double *, int, int);

  void Solve_right(double *, int, pardec);

  inline const std::vector<pardec> *get_dec() noexcept { return &dec; }

  inline pardec &get_dec(int i) noexcept { return dec[i]; }

  inline int get_dec_size() const noexcept { return dec.size(); }

  inline int *get_other_IPIV() noexcept { return other_IPIV.data(); }

  inline double *get_other_column() noexcept { return other_column.data(); }

  inline double *get_other_row() noexcept { return other_row.data(); }

  inline Communicator &get_PC() noexcept { return C; }

  inline Communicator &get_PC() const noexcept { return C; }

  inline Communicator *get_PC_loc() const noexcept { return C_loc.get(); }

  void Create_other_column(int i, bool withIPIV = true);

  void Communicate_Column_global(int i, bool withIPIV = true);

  void Communicate_Column_intern(int i, Communicator *C_locloc, bool withIPIV = true);

  inline void Create_other_row(int num_rows) {
    size_t sz = cols() * (num_rows);
    other_row.resize(sz);
  }

  void Communicate_row(int r_begin, int r_end, Communicator *PC = nullptr);

  void make_LU_multiply_local(double *left, int r_begin, int r_end);

  void makeLU();

  inline void Copy(const ParallelMatrix &CP) { a = CP.a; }

  void Clear() {
    a.clear();
    IPIV.clear();
    other_column.clear();
    other_row.clear();
    other_IPIV.clear();
  }

  inline double *ref() noexcept { return a.data(); }

  void set(int i, int j, double val);

  void add(int i, int j, double val);

  double *operator()(int i, int j);

  inline double *loc(int i, int j) {
    if (a.empty()) return nullptr;
    return &a[j * row + i];
  }

  inline void loc_add(int i, int j, double val) {
    if (a.empty()) return;
    a[j * row + i] += val;
  }

  double *operator()(int i, int j) const;

  inline double *loc(int i, int j) const {
    if (!a.empty()) return nullptr;
    return &a[j * row + i];
  }
};

#endif // PARALLELMATRIX_HPP

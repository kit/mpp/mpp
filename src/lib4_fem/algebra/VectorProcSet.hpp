#ifndef VECTORPROCSET_HPP
#define VECTORPROCSET_HPP

#include <algorithm>
#include <cstddef>
#include <iterator>
#include <vector>

#include "ProcSet.hpp"

class Vector;

class vector_ps {
  ProcSet ps;
  int local_size = 0;
  int global_size = 0;
  std::vector<int> invIND;
  int position = -1;
public:
  vector_ps() = default;

  vector_ps(int n) : local_size(n), invIND(n, -1) {}

  void create(int n) { invIND.resize(n); }

  void set_local_size(int n) noexcept { local_size = n; }

  void set_global_size(int n) noexcept { global_size = n; }

  std::size_t size() const noexcept { return invIND.size(); }

  int operator[](int n) const { return invIND[n]; }

  ProcSet &getps() noexcept { return ps; }

  const ProcSet &getps() const noexcept { return ps; }

  int const get_local_size() const noexcept { return local_size; }

  int const get_global_size() const noexcept { return global_size; }

  int const get_position() const noexcept { return position; }

  void set_position(int pos) noexcept { position = pos; }

  void Add(int k) {
    invIND[local_size] = k;
    local_size++;
  }

  void set_real_local_size() { invIND.resize(local_size); }
};

class VectorProcSet {
  int own_proc{};
  int total_proc{};
  int num_sorted_elements{};

  std::vector<vector_ps> vecPS;
public:
  VectorProcSet() = default;

  VectorProcSet(const Vector &A);

  const ProcSet &GetPS(int i) const { return vecPS[i].getps(); }

  int const GetGlobalSize(int i) const { return vecPS[i].get_global_size(); }

  int const GetLocalSize(int i) const { return vecPS[i].get_local_size(); }

  int const Get_Position(int i) const { return vecPS[i].get_position(); }

  void SetGlobalSize(int i, int s) { vecPS[i].set_global_size(s); }

  void SetLocalSize(int i, int s) { vecPS[i].set_local_size(s); }

  void add_to_position(int index) { setPosition(index, num_sorted_elements); }

  void clear_position() {
    for (auto &ps : vecPS)
      ps.set_position(-1);
    num_sorted_elements = 0;
  }

  int get_position(int index) const { return vecPS[index].get_position(); }

  int number_of_sorted_elements() const noexcept { return num_sorted_elements; }

  std::size_t size() const noexcept { return vecPS.size(); }

  void sort_global();

  void combine_procs();

  vector_ps *getvps(int i) { return &vecPS[i]; }

  const vector_ps *getvps(int i) const { return &vecPS[i]; }

  int get_own_proc() const noexcept { return own_proc; }

  int get_total_proc() const noexcept { return total_proc; }
private:
  void add(const ProcSet &Q, int local = -1, int global = -1) {
    ProcSet P(Q);
    P.Sort();
    if (findequalProcSet(P) == -1) {
      std::size_t m = vecPS.size();

      vecPS.resize(m + 1);
      vecPS[m].getps().Add(P);
      if (local >= 0) vecPS[m].set_local_size(local);
      if (global >= 0) vecPS[m].set_global_size(global);

      vecPS[m].set_position(m);
      ++num_sorted_elements;
    }
  }

  void add2(const ProcSet &Q, int &m, int local = -1, int global = -1);

  int findequalProcSet(const ProcSet &P) {
    for (int i = 0; i < static_cast<int>(vecPS.size()); ++i)
      if (vecPS[i].getps().equalProcSet(P)) return i;
    return -1;
  }

  void setPosition(int index, int pos) {
    if (vecPS[index].get_position() == -1) ++num_sorted_elements;
    vecPS[index].set_position(pos);
  }

  int minPS(const ProcSet &P1) {
    auto minIt = std::min_element(std::cbegin(P1), std::cend(P1));
    if (minIt == std::cend(P1)) { return -1; }
    return *minIt;
  }
};

#endif // VECTORPROCSET_HPP

#include "MatrixAccess.hpp"

#include <algorithm>
#include <cstddef>
#include <iterator>
#include <vector>

#include "Matrix.hpp"

RowEntries::RowEntries(Matrix &A, const rows &R) :
    a(R.size(), std::vector<double *>(R.size())), n(R.size()) {
  std::transform(std::cbegin(R), std::cend(R), std::begin(n),
                 [](const auto &value) { return value.NumberOfDofs(); });
  auto rowIt = std::begin(a);
  for (const auto &rowIdx : R) {
    auto colIt = std::begin(*rowIt);
    for (const auto &colIdx : R) {
      *colIt = A(rowIdx, colIdx);
      ++colIt;
    }
    ++rowIt;
  }
}

RowEntries::RowEntries(Matrix &A, const Cell &c) : RowEntries(A, rows(A.GetMatrixGraph(), c)) {}

MixedRowEntries::MixedRowEntries(Matrix &A, const Cell &c, const rows &R) :
    a(A.NumberOfDoFs(), std::vector<std::vector<std::vector<double *>>>(A.NumberOfDoFs())),
    length(A.NumberOfDoFs()) {

  const MixedDoF &mixedDoF = MixedDoF::Cast(A.GetDoF());
  for (int n = 0; n < A.NumberOfDoFs(); ++n) {
    const std::vector<SPData> &data_n = mixedDoF.StoragePointData(n, c);
    std::size_t nps_n = data_n.size();
    length[n].resize(nps_n);
    std::transform(std::cbegin(data_n), std::cend(data_n), std::begin(length[n]),
                   [&R](const auto &value) { return R[value.index].NumberOfDofs(); });
    for (int m = 0; m < A.NumberOfDoFs(); ++m) {
      const std::vector<SPData> &data_m = mixedDoF.StoragePointData(m, c);
      std::size_t nps_m = data_m.size();
      a[n][m].resize(nps_n);
      for (int i = 0; i < nps_n; ++i) {
        a[n][m][i].resize(nps_m);
        for (int j = 0; j < nps_m; ++j) {
          a[n][m][i][j] = A(R[data_n[i].index], R[data_m[j].index])
                          + data_n[i].shift * R[data_m[j].index].NumberOfDofs() + data_m[j].shift;
        }
      }
    }
  }
}

MixedRowEntries::MixedRowEntries(Matrix &A, const Cell &c) :
    MixedRowEntries(A, c, rows(A.GetMatrixGraph(), c)) {}

MixedRowEntries::MixedRowEntries(Matrix &A, const Cell &c, const Cell &cf) :
    a(A.NumberOfDoFs(), std::vector<std::vector<std::vector<double *>>>(A.NumberOfDoFs())),
    length(A.NumberOfDoFs()) {
  rows R(A.GetMatrixGraph(), c);
  rows R2(A.GetMatrixGraph(), cf);

  const MixedDoF &mixedDoF = MixedDoF::Cast(A.GetDoF());
  for (int n = 0; n < A.NumberOfDoFs(); ++n) {
    const std::vector<SPData> &data_n = mixedDoF.StoragePointData(n, c);
    std::size_t nps_n = data_n.size();
    length[n].resize(nps_n);
    std::transform(std::cbegin(data_n), std::cend(data_n), std::begin(length[n]),
                   [&R](const auto &value) { return R[value.index].NumberOfDofs(); });
    for (int m = 0; m < A.NumberOfDoFs(); ++m) {
      const std::vector<SPData> &data_m = mixedDoF.StoragePointData(m, cf);
      std::size_t nps_m = data_m.size();
      a[n][m].resize(nps_n);
      for (int i = 0; i < nps_n; ++i) {
        a[n][m][i].resize(nps_m);
        for (int j = 0; j < nps_m; ++j) {
          a[n][m][i][j] = A(R[data_n[i].index], R2[data_m[j].index])
                          + data_n[i].shift * R2[data_m[j].index].NumberOfDofs() + data_m[j].shift;
        }
      }
    }
  }
}

void DGRowEntries::initialize(Matrix &A, const row &r, const row &rf) {
  n = r.NumberOfDofs();
  nf = rf.NumberOfDofs();
  a = A(r, rf);
}

DGRowEntries::DGRowEntries(Matrix &A, const Cell &c, const Cell &cf, bool prev) :
    prev(prev), time_deg(prev ? A.GetDoF().get_time_deg(cf) : 0) {
  initialize(A, A.find_row(c()), A.find_row(cf()));
}

DGRowEntries::DGRowEntries(Matrix &A, const Cell &c) : DGRowEntries(A, A.find_row(c())) {}

DGSizedRowEntries::DGSizedRowEntries(Matrix &A, const row &R, int elemN, const row &Rf, int elemNf,
                                     bool prev) :
    a(A(R, Rf)), n(R.NumberOfDofs()), nf(Rf.NumberOfDofs()), elemsize(elemN),
    elemsize_nghbr(elemNf), prev(prev), time_deg(prev ? A.GetDoF().get_time_deg(Rf()) : 0) {}

DGSizedRowEntries::DGSizedRowEntries(Matrix &A, const Cell &c, int elemN, const Cell &cf,
                                     int elemNf, bool prev) :
    DGSizedRowEntries(A, A.find_row(c()), elemN, A.find_row(cf()), elemNf, prev) {}

DGSizedRowEntries::DGSizedRowEntries(Matrix &A, const Cell &c, int elemN) :
    DGSizedRowEntries(A, A.find_row(c()), elemN, A.find_row(c()), elemN) {}

double &DGSizedRowEntries::operator()(int i, int j, int k, int l) {
  int k_1 = k * elemsize + i;
  int l_1 = l * elemsize_nghbr + j;
  /*if (k_1 * nf + l_1 >= n * nf) {
    mout << OUT (n) << OUT (nf) << OUT (k_1) << OUT (l_1) << endl;
    Exit ("too large")
  }
  if (prev) return a[k_1 * nf + l_1 + (time_deg - 1) * (nf / time_deg)];*/
  return a[k_1 * nf + l_1];
}

PartRowEntries::PartRowEntries(Matrix &A, const rows &R, int p) :
    a(R.size(), std::vector<double *>(R.size())), n(R.size()), q(R.size()) {
  std::transform(std::cbegin(R), std::cend(R), std::begin(n),
                 [](const auto &value) { return value.NumberOfDofs(); });
  std::transform(std::cbegin(n), std::cend(n), std::begin(q), [p](const auto &value) {
    if (value > 1) { return p; }
    return 0;
  });
  for (unsigned int i = 0; i < R.size(); ++i) {
    for (unsigned int j = 0; j < R.size(); ++j) {
      a[i][j] = A(R[i], R[j]) + q[i] * n[j] + q[j];
    }
  }
}

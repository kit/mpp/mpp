#include "ParallelMatrix.hpp"

#include <algorithm>
#include <cstdlib>
#include <iterator>
#include <memory>
#include <vector>


#undef TRUE
#undef FALSE

#ifdef DCOMPLEX
#define Create_Dense_Matrix zCreate_Dense_Matrix
#define Create_CompCol_Matrix zCreate_CompCol_Matrix
#define Print_CompCol_Matrix zPrint_CompCol_Matrix
#define Print_SuperNode_Matrix zPrint_SuperNode_Matrix
#define gstrs zgstrs
#define gstrf zgstrf
#define SLU_DT SLU_Z

typedef complex<double> doublecomplex;

#include "../lib/superlu/include/slu_zdefs.hpp"
#else
#define Create_CompCol_Matrix dCreate_CompCol_Matrix
#define Create_Dense_Matrix dCreate_Dense_Matrix
#define Print_CompCol_Matrix dPrint_CompCol_Matrix
#define Print_SuperNode_Matrix dPrint_SuperNode_Matrix
#define gstrs dgstrs
#define gstrf dgstrf
#define SLU_DT SLU_D

#include "slu_ddefs.h"

#endif

const char trans = 'N';
const double one = 1;
const double minone = -1;

ParallelMatrix::ParallelMatrix(int r, const ParallelMatrix &PM, int matrixsize, int maxP) :
    C(PM.C), IPIV(0), local_col(PM.local_col), local_row(PM.local_row), row(r), col(PM.col), dec(0),
    C_loc(nullptr), other_column(0), other_row(0), other_IPIV(0) {
  if (row == 0 || col == 0) { return; }
  if (PM.row == 0) {
    Define_ParallelDistribution(matrixsize, maxP);
    return;
  }
  dec = PM.dec;
  int color = 0;
  if (local_col > 0) color = 1;

  //     if (dec.size() < C.size())
  C_loc = std::make_unique<Communicator>(C, color);
  ;
  //     if (!C_loc) C_loc = &C;
  //     else
  if (color == 0) { C_loc = nullptr; }

  local_row = row;
  Create();
}

void ParallelMatrix::Create_Distribution(int MINSIZE, int maxP) {
  int P = C.Size();
  if (maxP > 0 && maxP < P) P = maxP;
  int N = col;
  if (N < P) P = N;
  int p = C.Proc();
  if (MINSIZE < 0) MINSIZE = N;
  if (N < MINSIZE) MINSIZE = N;

  int local_size = (N / P);
  int num_dec = P;
  if (local_size < MINSIZE) {
    local_size = MINSIZE;
    num_dec = (N / local_size);
    local_size = (N / num_dec);
  }

  int extra = N - num_dec * local_size;

  dec.resize(num_dec);
  local_col = 0;
  int color = 0;
  int SHIFT = 0;
  for (int i = 0; i < extra; ++i) {
    dec[i].size = local_size + 1;
    dec[i].proc = i;
    if (p == i) {
      local_col = local_size + 1;
      color = 1;
    }
    dec[i].shift = SHIFT;
    SHIFT += dec[i].size;
  }

  for (int i = extra; i < num_dec; ++i) {
    dec[i].size = local_size;
    dec[i].proc = i;
    if (p == i) {
      local_col = local_size;
      color = 1;
    }
    dec[i].shift = SHIFT;
    SHIFT += dec[i].size;
  }

  if (static_cast<int>(dec.size()) < C.Size()) C_loc = std::make_unique<Communicator>(C, color);
}

void ParallelMatrix::makeLU(int i) {
  if (C.Proc() == i) {
    if (!IPIV.empty()) { THROW("Error in ParallelMatrix::makeLU") }
    int n = dec[i].size;
    IPIV.resize(n);
    GETRF(&n, &n, &a[dec[i].shift], &col, IPIV.data(), &info);
    if (info != 0) { THROW("Error in GETRF (LU)") }
  }
}

void ParallelMatrix::Solve(int i, double *rhs, int nrhs, int size_rhs) {
  if (C.Proc() == i) {
    int n = dec[i].size;
    GETRS(&trans, &n, &nrhs, &a[dec[i].shift], &col, IPIV.data(), rhs + dec[i].shift, &size_rhs,
          &info);
  }
}

void ParallelMatrix::Solve_own(int i) {
  if (other_column.empty()) return;
  pardec dec = get_dec(i);
  GETRS(&trans, &dec.size, &local_col, &other_column[dec.shift], &row, other_IPIV.data(),
        &a[dec.shift], &row, &info);
}

void ParallelMatrix::MMM_own(int i) {
  if (other_column.empty()) return;
  pardec dec = get_dec(i);
  int n = row - dec.shift - dec.size;
  GEMM(&trans, &trans, &n, &local_col, &dec.size, &minone, &other_column[dec.shift + dec.size],
       &row, &a[dec.shift], &row, &one, &a[dec.shift + dec.size], &row);
}

void ParallelMatrix::MMM_right(double *A_other_column, pardec pardec) {
  int n = row - pardec.shift - pardec.size;
  GEMM(&trans, &trans, &n, &local_col, &pardec.size, &minone,
       A_other_column + pardec.shift + pardec.size, &row, &a[pardec.shift], &row, &one,
       &a[pardec.shift + pardec.size], &row);
}

void ParallelMatrix::MMM_left(double *in, int Arows, pardec pardec) {
  if (other_column.empty()) return;
  GEMM(&trans, &trans, &row, &local_col, &pardec.size, &minone, other_column.data(), &row, in,
       &Arows, &one, a.data(), &row);
  //     LIB_GEMM(&trans, &trans, &leftrows, &local_col, &dec.size, &minone, leftcolumn, &leftrows,
  //              a+dec.shift, &row, &one, left, &leftrows); //GEMM LEFT
}

void ParallelMatrix::MMM_schur(double *left, double *right, int right_row, pardec pardec) {
  GEMM(&trans, &trans, &row, &local_col, &pardec.size, &minone, left, &row, right + pardec.shift,
       &right_row, &one, a.data(), &row); // SCHUR
}

void ParallelMatrix::Solve_right(double *rightref, int local_col_right, pardec pardec) {
  GETRS(&trans, &pardec.size, &local_col_right, &other_column[pardec.shift], &row,
        other_IPIV.data(), rightref + pardec.shift, &row, &info); // Solve RIGHT
}

void ParallelMatrix::MV_rhs(int i, double *rhs, int nrhs, int size_rhs) { // call with A->
  pardec dec = get_dec(i);
  int n = row - dec.shift - dec.size;
  GEMM(&trans, &trans, &n, &nrhs, &dec.size, &minone, &a[dec.shift + dec.size], &row,
       rhs + dec.shift, &size_rhs, &one, rhs + dec.shift + dec.size, &size_rhs);
}

void ParallelMatrix::MV_rhs_left(int i, int Arow, double *rhs, int nrhs,
                                 int size_rhs) { // call with Left->
  pardec dec = get_dec(i);
  GEMM(&trans, &trans, &row, &nrhs, &dec.size, &minone, a.data(), &row, rhs + dec.shift, &size_rhs,
       &one, rhs + Arow, &size_rhs);
}

void ParallelMatrix::MV_rhs_right(int p, double *rhs, int nrhs, int size_rhs) {
  if (p < get_dec_size()) {
    pardec dec = get_dec(p);
    GEMM(&trans, &trans, &row, &nrhs, &local_col, &minone, a.data(), &row, rhs + row + dec.shift,
         &size_rhs, &one, rhs, &size_rhs);
  }
}

void ParallelMatrix::MV_rhs_Z(int i, double *rhs, int nrhs, int size_rhs) {
  pardec dec = get_dec(i);
  GEMM(&trans, &trans, &dec.shift, &nrhs, &local_col, &minone, a.data(), &row, rhs + dec.shift,
       &size_rhs, &one, rhs, &size_rhs);
}

void ParallelMatrix::Create_other_column(int i, bool withIPIV) {
  size_t sz = dec[i].size * row;
  other_column.resize(sz);
  if (withIPIV) other_IPIV.resize(dec[i].size);
  else other_IPIV.clear();
}

void ParallelMatrix::Communicate_Column_global(int i, bool withIPIV) {
  Create_other_column(i, withIPIV);
  size_t sz = dec[i].size * row;
  if (C.Proc() == i) {
    std::copy_n(std::cbegin(a), sz, std::begin(other_column));
    if (withIPIV) std::copy_n(std::cbegin(IPIV), sz, std::begin(other_IPIV));
  }
  C.Broadcast(other_column.data(), sz, i);
  if (withIPIV) C.Broadcast(other_IPIV.data(), dec[i].size, i);
}

void ParallelMatrix::Communicate_Column_intern(int i, Communicator *C_locloc, bool withIPIV) {
  if (!C_locloc) {
    Communicate_Column_global(i, withIPIV);
    return;
  }
  if (C.Proc() != C_locloc->Proc()) return;
  Create_other_column(i, withIPIV);
  size_t sz = dec[i].size * row;
  if (C_locloc->Proc() == i) {
    std::copy_n(std::cbegin(a), sz, std::begin(other_column));
    if (withIPIV) std::copy_n(std::cbegin(IPIV), dec[i].size, std::begin(other_IPIV));
  }
  C_locloc->Broadcast(other_column.data(), sz, i);
  if (withIPIV) C_locloc->Broadcast(other_IPIV.data(), dec[i].size, i);
}

void ParallelMatrix::Communicate_row(int r_begin, int r_end, Communicator *PC) {
  int num_rows = r_end - r_begin;
  Create_other_row(num_rows);
  if (!PC) return;
  if (C.Proc() >= PC->Size()) return;

  if (PC->Size() == 1) {
    for (int i = 0; i < local_col; ++i) {
      std::copy_n(std::next(std::cbegin(a), r_begin + i * rows()), num_rows,
                  std::next(std::begin(other_row), i * num_rows));
    }
    return;
  }

  std::vector<double> senddata(local_col * num_rows);
  size_t sendsize = 0;
  std::vector<int> receivesize(PC->Size(), 0);
  std::vector<int> displs(PC->Size(), 0);
  auto sizeIt = std::begin(receivesize);
  auto shiftIt = std::begin(displs);
  for (const auto &value : dec) {
    *sizeIt = num_rows * value.size;
    *shiftIt = num_rows * value.shift;
    ++sizeIt;
    ++shiftIt;
  }
  for (int i = 0; i < local_col; ++i) {
    std::copy_n(std::next(std::cbegin(a), r_begin + i * rows()), num_rows,
                std::next(std::begin(other_row), i * num_rows));
  }
  PC->Allgatherv(senddata.data(), sendsize, other_row.data(), &receivesize[0], &displs[0]);
}

void ParallelMatrix::make_LU_multiply_local(double *left, int r_begin, int r_end) {
  if (a.empty()) return;
  int r = row - r_end;
  int k = r_end - r_begin;
  int c = local_col;
  const char dotrans = 'T';
  double *B = &a[r_begin];
  double *C = &a[r_end];
  GEMM(&dotrans, &trans, &r, &c, &k, &minone, left, &k, B, &row, &one, C, &row);
}

void ParallelMatrix::makeLU() {
  if (a.empty()) return;
  for (int i = 0; i < static_cast<int>(dec.size()); ++i) {
    makeLU(i);
    if (C_loc->Proc() == C.Proc()) Communicate_Column_intern(i, C_loc.get());
    if (C.Proc() > i) {
      GETRS(&trans, &dec[i].size, &local_col, &other_column[dec[i].shift], &row, other_IPIV.data(),
            &a[dec[i].shift], &row, &info);
      int n = row - dec[i].shift - dec[i].size;
      GEMM(&trans, &trans, &n, &local_col, &dec[i].size, &minone,
           &other_column[dec[i].shift + dec[i].size], &row, &a[dec[i].shift], &row, &one,
           &a[dec[i].shift + dec[i].size], &row);
    }
  }
}

void ParallelMatrix::set(int i, int j, double val) {
  if (a.empty()) return;
  int p = C.Proc();
  if (p >= static_cast<int>(dec.size())) return;
  if ((j < dec[p].shift) || (j >= dec[p].shift + local_col)) return;
  a[(j - dec[p].shift) * row + i] = val;
}

void ParallelMatrix::add(int i, int j, double val) {
  if (a.empty()) return;
  int p = C.Proc();
  if (p >= static_cast<int>(dec.size())) return;
  if ((j < dec[p].shift) || (j >= dec[p].shift + local_col)) return;
  a[(j - dec[p].shift) * row + i] += val;
}

double *ParallelMatrix::operator()(int i, int j) {
  if (a.empty()) return nullptr;
  int p = C.Proc();
  if (p >= static_cast<int>(dec.size())) return nullptr;
  if ((j < dec[p].shift) || (j >= dec[p].shift + local_col)) return nullptr;
  return &a[(j - dec[p].shift) * row + i];
}

double *ParallelMatrix::operator()(int i, int j) const {
  if (a.empty()) return nullptr;
  int p = C.Proc();
  if (p >= static_cast<int>(dec.size())) return nullptr;
  if ((j < dec[p].shift) || (j >= dec[p].shift + local_col)) return nullptr;
  return &a[(j - dec[p].shift) * row + i];
}

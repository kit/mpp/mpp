#ifndef _ALGEBRA_H_
#define _ALGEBRA_H_

#include "Matrix.hpp"
#include "MatrixAccess.hpp"
#include "Operator.hpp"
#include "Sparse.hpp"
#include "Vector.hpp"
#include "VectorAccess.hpp"

#endif // of #ifndef _ALGEBRA_H_

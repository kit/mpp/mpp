#include "VectorMatrixBase.hpp"

#include <string>

template<>
const IDiscretization &VectorMatrixBase::GetDiscT<double, SpaceDimension, TimeDimension>() const {
  return *disc;
}

#ifdef BUILD_IA

template<>
const IAIDiscretization &
VectorMatrixBase::GetDiscT<IAInterval, SpaceDimension, TimeDimension>() const {
  if (iadisc) return *iadisc;
  THROW("IADiscretization not set!")
}

#endif

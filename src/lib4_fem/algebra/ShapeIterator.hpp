#ifndef SHAPEITERATOR_HPP
#define SHAPEITERATOR_HPP

#include <utility>

template<typename DATA>
struct ShapeIterator {
protected:
  DATA data;
public:
  ShapeIterator(DATA &&data) : data(std::move(data)) {}

  inline ShapeIterator &operator++() {
    data.Incr();
    return *this;
  }

  inline bool operator==(const ShapeIterator &other) const noexcept {
    return data.id == other.data.id;
  }

  inline bool operator!=(const ShapeIterator &other) const noexcept {
    return data.id != other.data.id;
  }

  inline DATA &operator*() noexcept { return data; }
};

struct ShapeId {
  int id;

  ShapeId(int id) : id(id) {}
};

template<typename T>
struct IteratorProvider {
private:
  T &t;
  int n;
public:
  IteratorProvider(T &t, int n) : t(t), n(n) {}

  inline auto begin() { return t.begin(n); }

  inline auto end() { return t.end(n); }
};

#endif // SHAPEITERATOR_HPP

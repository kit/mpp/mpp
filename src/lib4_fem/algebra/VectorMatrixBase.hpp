#ifndef VECTORMATRIXBASE_HPP
#define VECTORMATRIXBASE_HPP

#include <sys/types.h>
#include <cstddef>
#include <cstdint>
#include <memory>
#include <string>
#include <utility>
#include <vector>

#include "IDiscretization.hpp"
#include "MixedDoF.hpp"

class VectorMatrixBase {
protected:
  std::shared_ptr<const IDiscretization> disc;

#ifdef BUILD_IA
  std::shared_ptr<const IAIDiscretization> iadisc;
#endif

  // reference to avoid multiple access to matrix graph map in discretization
  IMatrixGraph &graph;

  VectorMatrixBase(std::shared_ptr<const IDiscretization> disc, MeshIndex levels) :
      disc(std::move(disc)), graph((*this->disc)(levels)) {}

  VectorMatrixBase(const VectorMatrixBase &g) : disc(g.disc), graph(g.graph) {
#ifdef BUILD_IA
    iadisc = g.iadisc;
#endif
  }
public:
  inline const IDiscretization &GetDisc() const noexcept { return *disc; }

  inline std::shared_ptr<const IDiscretization> GetSharedDisc() const noexcept { return disc; }

  inline const Meshes &GetMeshes() const noexcept { return GetDisc().GetMeshes(); }

  template<typename T, int sDim, int tDim>
  const IDiscretizationT<T, sDim, tDim> &GetDiscT() const;

#ifdef BUILD_IA

  inline const IAIDiscretization &GetIADisc() const noexcept { return *iadisc; }

  inline std::shared_ptr<const IAIDiscretization> GetSharedIADisc() const noexcept {
    return iadisc;
  }

#endif

  inline const ProcSets &GetProcSets() const { return graph.GetProcSets(); }

  inline const IMatrixGraph &GetMatrixGraph() const noexcept { return graph; }

  [[nodiscard]]
  inline const Cells &MeshCells() const noexcept {
    return graph.MeshCells();
  }

  inline int CommSplit() const noexcept { return graph.CommSplit(); }

  inline MeshIndex Level() const noexcept { return graph.GetLevel(); }

  inline int SpaceLevel() const noexcept { return Level().space; }

  inline int TimeLevel() const noexcept { return Level().time; }

  inline void PrintInfo() const { graph.PrintInfo(); }

  inline row rows() const noexcept { return graph.rows(); }

  inline row rows_end() const noexcept { return graph.rows_end(); }

  inline row find_row(const Point &z) const { return graph.find_row(z); }

  inline size_t pMatrixSize() const { return graph.pMatrixSize(); }

  inline ssize_t Id(const Point &z) const { return graph.GetRows().RowId(z); }

  inline ssize_t Idx(const Point &z) const { return graph.GetRows().RowIdChecked(z); }

  inline size_t size() const noexcept { return graph.size(); }

  inline size_t Size() const noexcept { return graph.Size(); }

  inline size_t pSize() const { return graph.pSize(); }

  inline size_t Index(const size_t i) const { return graph.Index(i); }

  inline ssize_t Column(const size_t i) const { return graph.Column(i); }

  inline size_t Diag(const size_t i) const { return graph.Diag(i); }

  inline ssize_t Entry(const size_t i) const { return graph.Entry(i); }

  inline bool SingleEntry(const size_t i) const { return graph.SingleEntry(i); }

  inline uint16_t Dof(const size_t i) const { return graph.Dof(i); }

  inline ssize_t GetEntry(const row &r0, const row &r1) const {
    return graph.GetRows().Index(r0, r1);
  }

  inline ssize_t GetEntryX(const row &r0, const row &r1) const {
    return graph.GetRows().IndexChecked(r0, r1);
  }

  inline ssize_t GetDoubleEntryX(const row &r0, const row &r1) const {
    return graph.GetRows().DoubleIndexChecked(r0, r1);
  }

  inline size_t nR() const noexcept { return graph.GetRows().size(); }

  template<typename S>
  friend inline LogTextStream<S> &operator<<(LogTextStream<S> &s, const VectorMatrixBase &G) {
    return s << G.disc;
  }

  size_t rowsize() const noexcept { return graph.rowsize(); }

  inline const Mesh &GetMesh() const noexcept { return graph.GetMesh(); }

  inline cell cells() const { return graph.cells(); }

  inline cell cells_end() const { return graph.cells_end(); }

  inline cell find_cell(const Point &z) const { return graph.find_cell(z); }

  inline cell overlap() const { return graph.overlap(); }

  inline cell overlap_end() const { return graph.overlap_end(); }

  inline face faces() const { return graph.faces(); }

  inline face find_face(const Point &center) const { return graph.find_face(center); }

  inline vertex vertices() const { return graph.vertices(); }

  inline vertex vertices_end() const { return graph.vertices_end(); }

  inline vertex find_vertex(const Point &p) const { return graph.find_vertex(p); }

  inline edge edges() const { return graph.edges(); }

  inline edge edges_end() const { return graph.edges_end(); }

  inline edge find_edge(const Point &center) const { return graph.find_edge(center); }

  [[nodiscard]]
  const Cell &FindNeighbourCell(const Cell &cell, size_t faceID) const noexcept(DebugLevel == 0) {
    face ff = find_face(cell.Face(faceID));
    if constexpr (DebugLevel > 0) {
      if (ff == faces_end()) { THROW("Mesh::find_neighbour_cell, Face not found!") }
    }
    if (cell() != ff.Right()) return *find_cell_or_overlap_cell(ff.Right());
    return *find_cell_or_overlap_cell(ff.Left());
  }

  [[nodiscard]]
  const Cell &FindPreviousCell(const Cell &cell) const noexcept(false) {
    face f = find_face(cell.Face(cell.Faces() - 2));
    if (f == faces_end()) {
      return cell; // Is this sensiable edgecase handling? TODO
    }
    auto c_prev = find_cell_or_overlap_cell(f.Left());
    if (c_prev.Center() == cell.Center() && has_cell_or_overlap_cell(f.Right())) {
      c_prev = find_cell_or_overlap_cell(f.Right());
    }
    if (c_prev == cells_end() || c_prev == overlap_end()) { return cell; }

    return *c_prev;
  }

  inline cell find_neighbour_cell(const cell &c, int f) const {
    return graph.find_neighbour_cell(c, f);
  }

  inline cell find_neighbour_cell(const Cell &c, int f) const {
    return graph.find_neighbour_cell(c, f);
  }

  inline int find_neighbour_face_id(const Point &f_c, const cell &cf) const {
    return graph.find_neighbour_face_id(f_c, cf);
  }

  inline cell find_cell_or_overlap_cell(const Point &z) const {
    return graph.find_cell_or_overlap_cell(z);
  }

  inline Point neighbour_center(const Cell &c, int i) const { return graph.neighbour_center(c, i); }

  inline bool has_cell_or_overlap_cell(const Point &z) const {
    return graph.has_cell_or_overlap_cell(z);
  }

  inline bool OnBoundary(const Cell &c) const { return graph.OnBoundary(c); }

  inline bool has_previous_cell(const cell &c) const { return graph.has_previous_cell(c); }

  inline bool OnBoundary(const Cell &c, int f) const { return graph.OnBoundary(c, f); }

  inline cell find_previous_cell(const cell &c) const { return graph.find_previous_cell(c); }

  inline face faces_end() const { return graph.faces_end(); }

  inline cell find_overlap_cell(const Point &z) const { return graph.find_overlap_cell(z); }

  inline double t(const size_t k) const { return graph.t(k); }

  inline bool onBnd(const Point &x) const { return graph.onBnd(x); }

  inline int BndPart(const Point &x) const { return graph.BndPart(x); }

  inline bnd_face bnd_faces() const { return graph.bnd_faces(); }

  inline bnd_face bnd_faces_end() const { return graph.bnd_faces_end(); }

  inline bnd_face find_bnd_face(const Point &z) const { return graph.find_bnd_face(z); }

  inline identifyset identifysets() const { return graph.GetIdentifySets().identifysets(); }

  inline identifyset identifysets_end() const { return graph.GetIdentifySets().identifysets_end(); }

  inline identifyset find_identifyset(const Point &z) const { return graph.find_identifyset(z); }

  inline bool identify() const { return graph.identify(); }

  inline bool parallel() const { return graph.parallel(); }

  inline int dim() const { return graph.dim(); }

  inline procset procsets() const { return graph.procsets(); }

  inline procset procsets_end() const { return graph.procsets_end(); }

  inline procset find_procset(const Point &z) const { return graph.find_procset(z); }

  inline bool master(const Point &z) const { return graph.GetProcSets().master(z); }

  inline const SubVectorMask &Mask(const char *name) const { return graph.Mask(name); }

  inline MatrixGraphBuffers &Buffers() { return graph.Buffers(); }

  inline size_t OverlapCount() const { return graph.OverlapCount(); }

  inline size_t OverlapCountGeometry() const { return graph.OverlapCountGeometry(); }

  /// functions from IDoF to provide functions for Vector, Matrix, etc.
  ///===============================================================================================
  inline IDoF &GetDoF() const { return graph.GetDoF(); }

  // TODO: Remove
  inline const IDoF &GetShapeDoF() const { return graph.GetShapeDoF(); }

  inline std::string DoFsName() const { return GetDoF().Name(); }

  inline short NumberOfDoFs() const { return GetDoF().NumberOfDoFs(); }

  inline short NumberOfComponents() const { return GetDoF().NumberOfComponents(); }

  inline short NumberOfNodalPoints(const Cell &c) const { return GetDoF().NumberOfNodalPoints(c); }

  inline short NumberOfNodalPoints(int n, const Cell &c) const {
    return MixedDoF::Cast(GetDoF()).NumberOfNodalPoints(n, c);
  }

  inline short NumberOfNodalPoints(const int sDeg, const int tDeg) const {
    return GetDoF().NumberOfNodalPoints(sDeg, tDeg);
  }

  inline std::vector<Point> GetNodalPoints(const Cell &c) const {
    return GetDoF().GetNodalPoints(c);
  }

  inline std::vector<Point> GetNodalPoints(int n, const Cell &c) const {
    return MixedDoF::Cast(GetDoF()).GetNodalPoints(n, c);
  }

  inline std::vector<short> DoFSizesAtNodalPoints(const Cell &c) const {
    return GetDoF().DoFSizesAtNodalPoints(c);
  }

  inline std::vector<short> DoFSizesAtNodalPoints(int n, const Cell &c) const {
    return MixedDoF::Cast(GetDoF()).DoFSizesAtNodalPoints(n, c);
  }

  inline short NumberOfNodalPointsOnFace(const Cell &c, int faceId) const {
    return GetDoF().NumberOfNodalPointsOnFace(c, faceId);
  }

  inline short NumberOfNodalPointsOnFace(int n, const Cell &c, int faceId) const {
    return MixedDoF::Cast(GetDoF()).NumberOfNodalPointsOnFace(n, c, faceId);
  }

  inline short IdOfNodalPointOnFace(const Cell &c, int faceId, int k) const {
    return GetDoF().IdOfNodalPointOnFace(c, faceId, k);
  }

  inline short IdOfNodalPointOnFace(int n, const Cell &c, int faceId, int k) const {
    return MixedDoF::Cast(GetDoF()).IdOfNodalPointOnFace(n, c, faceId, k);
  }

  inline std::vector<Point> GetNodalPointsOnFace(const Cell &c, int faceId) const {
    return GetDoF().GetNodalPointsOnFace(c, faceId);
  }

  inline std::vector<Point> GetNodalPointsOnFace(int n, const Cell &c, int faceId) const {
    return MixedDoF::Cast(GetDoF()).GetNodalPointsOnFace(n, c, faceId);
  }

  inline short NumberOfNodalPointsOnEdge(const Cell &c, int edgeId) const {
    return GetDoF().NumberOfNodalPointsOnEdge(c, edgeId);
  }

  inline short NumberOfNodalPointsOnEdge(int n, const Cell &c, int edgeId) const {
    return MixedDoF::Cast(GetDoF()).NumberOfNodalPointsOnEdge(n, c, edgeId);
  }

  inline short IdOfNodalPointOnEdge(const Cell &c, int edgeId, int k) const {
    return GetDoF().IdOfNodalPointOnEdge(c, edgeId, k);
  }

  inline short IdOfNodalPointOnEdge(int n, const Cell &c, int edgeId, int k) const {
    return MixedDoF::Cast(GetDoF()).IdOfNodalPointOnEdge(n, c, edgeId, k);
  }

  inline std::vector<Point> GetNodalPointsOnEdge(const Cell &c, int edgeId) const {
    return GetDoF().GetNodalPointsOnEdge(c, edgeId);
  }

  inline std::vector<Point> GetNodalPointsOnEdge(int n, const Cell &c, int edgeId) const {
    return MixedDoF::Cast(GetDoF()).GetNodalPointsOnEdge(n, c, edgeId);
  }
};


#endif // of #ifndef VECTORMATRIXBASE_HPP

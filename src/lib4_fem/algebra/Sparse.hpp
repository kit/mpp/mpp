#ifndef _SPARSE_H_
#define _SPARSE_H_

#include "AlgebraFwd.hpp"

#include "BasicSparseSolver.hpp"
#include "Operator.hpp"
#include "SparseRMatrix.hpp"

// Needed?
#include <algorithm>
#include <cmath>
#include <iterator>
#include <map>

template<class D>
inline bool isZero(D z) {
  return abs(z) < nearToZero;
}

template<class D>
inline bool isnotZero(D z) {
  return abs(z) >= nearToZero;
}

class SparseMatrix : public SparseRMatrix, public Operator {
  int size_(const SparseMatrix &M, const std::vector<bool> &mask) const;

  int Size_(const SparseMatrix &M, const std::vector<bool> &mask) const;

  int Size_(const SparseMatrix &M, const std::vector<int> &rindex) const;

  int Size_(const SparseMatrix &M, const std::vector<int> &indexrow,
            const std::vector<int> &indexcol) const;

  inline int int_in_vector(int x, const std::vector<int> &y) const {
    if (x == -1) return -1;
    auto it = std::find(std::cbegin(y), std::cend(y), x);
    if (it != std::cend(y)) { return std::distance(std::cbegin(y), it); }
    return -1;
  }

  inline int _Size(const SparseMatrix &M, const std::vector<int> &Indices) const {
    int n = 0;
    for (int Indice : Indices) {
      for (int k = M.rowind(Indice); k < M.rowind(Indice + 1); ++k)
        if (int_in_vector(M.colptr(k), Indices) != -1) n++;
    }
    return n;
  }

  typedef std::map<int, double> SparseVecT;
  typedef SparseVecT::iterator SparVecIt;
  typedef SparseVecT::const_iterator conSparVecIt;
  typedef std::vector<SparseVecT> SparseMatT;
  typedef std::vector<int> DofDofId;
  DofDofId ParDofId, ParDofRes;

  void convert_sm(SparseMatT &) const;

  void print_sm(const SparseMatT &) const;

  void print_sm_dense(const SparseMatT &) const;

  void build_ident(const Matrix &);

  void apply_PC_mat(SparseMatT &, const SparseMatT &) const;
public:
  void convert_sm_back(const SparseMatT &);

  SparseMatrix(const Matrix &M);

  SparseMatrix(const SparseMatrix &);

  SparseMatrix(const SparseMatrix &M, const std::vector<bool> &mask);

  SparseMatrix(const SparseMatrix &M, const std::vector<int> &Indices);

  SparseMatrix(const SparseMatrix &M, const std::vector<int> &index,
               const std::vector<int> &rindex);

  SparseMatrix(const SparseMatrix &M, const std::vector<int> &indexrow,
               const std::vector<int> &indexcol, bool dummy);

  void multiply_plus(Vector &b, const Vector &u) const override;

  void pc_mat_convert(const Matrix &);

  void ShrinkIdentify(Vector &, const Vector &) const;

  void ExpandIdentify(Vector &) const;

  double *ref(int j) const {
    double *a;
    a = new double[rows()];
    for (int i = 0; i < rows(); ++i) {
      int e = find(i, j);
      if (e == -1) a[i] = 0.0;
      else a[i] = nzval(e);
    }
    return a;
  }

  void plusMatVec(Vector &b, const Vector &u) const;

  void minusMatVec(Vector &b, const Vector &u) const;

  void multiply_minus(Vector &b, const Vector &u) const override;
};

constAB<Operator, Vector> operator*(const SparseMatrix &S, const Vector &v);

constAB<Operator, Vectors> operator*(const SparseMatrix &S, const Vectors &v);

#endif // of #ifndef _SPARSE_H_

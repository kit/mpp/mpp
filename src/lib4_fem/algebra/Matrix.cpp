#include "Matrix.hpp"
#include <sys/types.h>

#include <algorithm>
#include <cmath>
#include <cstddef>
#include <cstdint>
#include <iterator>
#include <memory>
#include <utility>

#include "Operator.hpp"
#include "Sparse.hpp"
#include "Vector.hpp"

Matrix::Matrix(const Vector &U, bool correctionAddBCArgyris) :
    VectorMatrixBase(U), data(U.Size()), Vec(U), sm(nullptr), addBC(correctionAddBCArgyris) {}

Matrix::Matrix(const Matrix &A) :
    VectorMatrixBase(A), data(A.data), Vec(A.Vec), sm(nullptr),
    collectAfterProd(A.collectAfterProd) {}

Matrix::Matrix(Matrix &&A) :
    VectorMatrixBase(A), data(std::move(A.data)), Vec(A.Vec), sm(nullptr),
    collectAfterProd(A.collectAfterProd) {}

Matrix::~Matrix() = default;

void Matrix::CreateSparse() {
  sm = std::make_unique<SparseMatrix>(*this);
  sm->compress();
}

Matrix &Matrix::operator=(const Matrix &A) {
  data = A.data;
  return *this;
}

Matrix &Matrix::operator=(Matrix &&A) noexcept {
  data = std::move(A.data);
  return *this;
}

Matrix &Matrix::operator+=(const constAB<double, Matrix> &aA) {
  double s = aA.first();
  const auto &mtxData = aA.second().GetData();
  std::transform(std::cbegin(data), std::cend(data), std::cbegin(mtxData), std::begin(data),
                 [&s](const auto &first, const auto &second) { return first + s * second; });
  return *this;
}

void Matrix::multiply_plus(Vector &b, const Vector &u) const {
  plusMatVec(b, u);
  if (collectAfterProd) {
    b.Collect();
  } else {
    b.SetAccumulateFlag(false);
  }
}

void Matrix::multiply_minus(Vector &b, const Vector &u) const {
  minusMatVec(b, u);
  if (collectAfterProd) {
    b.Collect();
  } else {
    b.SetAccumulateFlag(false);
  }
}

void Matrix::multiply(Vector &b, const Vector &u) const {
  matVec(b, u);
  if (collectAfterProd) {
    b.Collect();
  } else {
    b.SetAccumulateFlag(false);
  }
}

void Matrix::multiply_transpose_plus(Vector &b, const Vector &u) const {
  plusMatTVec(b, u);
  if (collectAfterProd) {
    b.Collect();
  } else {
    b.SetAccumulateFlag(false);
  }
}

void Matrix::multiply_plus(Vectors &b, const Vectors &u) const {
  plusMatVecs(b, u);
  if (collectAfterProd) {
    b.Collect();
  } else {
    b.SetAccumulateFlags(false);
  }
}

void Matrix::multiply(Vectors &b, const Vectors &u) const {
  matVecs(b, u);
  if (collectAfterProd) {
    b.Collect();
  } else {
    b.SetAccumulateFlags(false);
  }
}

void Matrix::multiply_transpose_plus(Vectors &b, const Vectors &u) const {
  plusMatTVecs(b, u);
  if (collectAfterProd) {
    b.Collect();
  } else {
    b.SetAccumulateFlags(false);
  }
}

void Matrix::minusMatVec(Vector &b, const Vector &u) const {
  if (!u.GetAccumulateFlag()) { Warning("Vector not accumulated in Matrix Vector product"); }
  applications++;
  if (sm) {
    sm->minusMatVec(b, u);
    return;
  }
  auto a = std::cbegin(data);
  for (size_t i = 0; i < nR(); ++i) {
    size_t d = Diag(i);
    const uint16_t n = Dof(i);
    for (uint16_t k = 0; k < n; ++k)
      for (uint16_t l = 0; l < n; ++l, ++a)
        b(i, k) -= *a * u(i, l);
    for (++d; d < Diag(i + 1); ++d) {
      const ssize_t j = Column(d);
      const uint16_t m = Dof(j);
      for (uint16_t k = 0; k < n; ++k)
        for (uint16_t l = 0; l < m; ++l, ++a)
          b(i, k) -= *a * u(j, l);
      if (SingleEntry(d)) continue;
      for (uint16_t k = 0; k < m; ++k)
        for (uint16_t l = 0; l < n; ++l, ++a)
          b(j, k) -= *a * u(i, l);
    }
  }
}

void Matrix::plusMatVec(Vector &b, const Vector &u) const {
  if (!u.GetAccumulateFlag()) { Warning("Vector not accumulated in Matrix Vector product"); }
  applications++;
  if (sm) {
    sm->plusMatVec(b, u);
    return;
  }
  auto a = std::cbegin(data);
  for (size_t i = 0; i < nR(); ++i) {
    size_t d = Diag(i);
    const uint16_t n = Dof(i);
    for (uint16_t k = 0; k < n; ++k)
      for (uint16_t l = 0; l < n; ++l, ++a)
        b(i, k) += *a * u(i, l);
    for (++d; d < Diag(i + 1); ++d) {
      const ssize_t j = Column(d);
      const uint16_t m = Dof(j);
      for (uint16_t k = 0; k < n; ++k)
        for (uint16_t l = 0; l < m; ++l, ++a)
          b(i, k) += *a * u(j, l);
      if (SingleEntry(d)) continue;
      for (uint16_t k = 0; k < m; ++k)
        for (uint16_t l = 0; l < n; ++l, ++a)
          b(j, k) += *a * u(i, l);
    }
  }
}

void Matrix::matVec(Vector &u, const Vector &v) const {
  u = 0;
  plusMatVec(u, v);
}

void Matrix::plusMatTVec(Vector &b, const Vector &u) const {
  if (!u.GetAccumulateFlag()) { Warning("Vector not accumulated in Matrix Vector product!"); }
  auto a = std::cbegin(data);
  for (size_t i = 0; i < nR(); ++i) {
    size_t d = Diag(i);
    const uint16_t n = Dof(i);
    for (uint16_t k = 0; k < n; ++k) {
      for (uint16_t l = 0; l < n; ++l, ++a)
        b(i, l) += *a * u(i, k);
    }
    for (++d; d < Diag(i + 1); ++d) {
      const ssize_t j = Column(d);
      const uint16_t m = Dof(j);
      for (uint16_t k = 0; k < n; ++k) {
        for (uint16_t l = 0; l < m; ++l, ++a)
          b(j, l) += *a * u(i, k);
      }
      if (SingleEntry(d)) continue;
      for (uint16_t k = 0; k < m; ++k) {
        for (uint16_t l = 0; l < n; ++l, ++a)
          b(i, l) += *a * u(j, k);
      }
    }
  }
}

void Matrix::plusMatVecs(Vectors &b, const Vectors &u) const {
  if (std::any_of(std::cbegin(u), std::cend(u),
                  [](const auto &vec) { return !vec->GetAccumulateFlag(); })) {
    Warning("At least one Vector in Vectors not accumulated!");
  }
  if (sm) {
    for (size_t i = 0; i < b.size(); ++i) {
      sm->plusMatVec(b[i], u[i]);
    }
    return;
  }
  auto a = std::cbegin(data);
  for (size_t i = 0; i < nR(); ++i) {
    size_t d = Diag(i);
    const uint16_t n = Dof(i);
    for (uint16_t k = 0; k < n; ++k)
      for (uint16_t l = 0; l < n; ++l, ++a)
        for (size_t v = 0; v < b.size(); ++v)
          b(v, i, k) += *a * u(v, i, l);
    for (++d; d < Diag(i + 1); ++d) {
      const ssize_t j = Column(d);
      const uint16_t m = Dof(j);
      for (uint16_t k = 0; k < n; ++k)
        for (uint16_t l = 0; l < m; ++l, ++a)
          for (size_t v = 0; v < b.size(); ++v)
            b(v, i, k) += *a * u(v, j, l);
      if (SingleEntry(d)) continue;
      for (uint16_t k = 0; k < m; ++k)
        for (uint16_t l = 0; l < n; ++l, ++a)
          for (size_t v = 0; v < b.size(); ++v)
            b(v, j, k) += *a * u(v, i, l);
    }
  }
}

void Matrix::matVecs(Vectors &u, const Vectors &v) const {
  u = 0;
  plusMatVecs(u, v);
}

void Matrix::plusMatTVecs(Vectors &b, const Vectors &u) const {
  if (std::any_of(std::cbegin(u), std::cend(u),
                  [](const auto &vec) { return !vec->GetAccumulateFlag(); })) {
    Warning("At least one Vector in Vectors not accumulated!");
  }
  auto a = std::cbegin(data);
  for (size_t i = 0; i < nR(); ++i) {
    size_t d = Diag(i);
    uint16_t n = Dof(i);
    for (uint16_t k = 0; k < n; ++k)
      for (uint16_t l = 0; l < n; ++l, ++a)
        for (size_t v = 0; v < b.size(); ++v)
          b(v, i, l) += *a * u(v, i, k);
    for (++d; d < Diag(i + 1); ++d) {
      ssize_t j = Column(d);
      uint16_t m = Dof(j);
      for (uint16_t k = 0; k < n; ++k)
        for (uint16_t l = 0; l < m; ++l, ++a)
          for (size_t v = 0; v < b.size(); ++v)
            b(v, j, l) += *a * u(v, i, k);
      if (SingleEntry(d)) continue;
      for (uint16_t k = 0; k < m; ++k)
        for (uint16_t l = 0; l < n; ++l, ++a)
          for (size_t v = 0; v < b.size(); ++v)
            b(v, i, l) += *a * u(v, j, k);
    }
  }
}

void Matrix::copy(double *a, int *ind, int *col) const {
  if (nR() == 0) return;
  for (size_t i = 0; i < Index(nR()); ++i)
    ind[i] = 0;
  for (size_t i = 0; i < nR(); ++i)
    for (uint16_t k = 0; k < Dof(i); ++k) {
      ind[Index(i) + k] += Dof(i);
      for (size_t d = Diag(i) + 1; d < Diag(i + 1); ++d) {
        const ssize_t j = Column(d);
        const uint16_t nc = Dof(j);
        for (uint16_t l = 0; l < nc; ++l) {
          ++ind[Index(i) + k];
          ++ind[Index(j) + l];
        }
      }
    }
  for (size_t i = 1; i < Index(nR()); ++i)
    ind[i] += ind[i - 1];
  for (size_t i = Index(nR()); i > 0; --i)
    ind[i] = ind[i - 1];
  ind[0] = 0;
  for (size_t i = 0; i < nR(); ++i) {
    const uint16_t nc = Dof(i);
    for (uint16_t k = 0; k < nc; ++k) {
      const size_t d = Diag(i);
      a[ind[Index(i) + k]] = data[Entry(d) + k * nc + k];
      col[ind[Index(i) + k]++] = Index(i) + k;
      for (uint16_t l = 0; l < nc; ++l) {
        if (l == k) continue;
        a[ind[Index(i) + k]] = data[Entry(d) + k * nc + l];
        col[ind[Index(i) + k]++] = Index(i) + l;
      }
    }
  }
  for (size_t i = 0; i < nR(); ++i) {
    const uint16_t nr = Dof(i);
    for (uint16_t k = 0; k < nr; ++k) {
      size_t d = Diag(i);
      for (++d; d < Diag(i + 1); ++d) {
        const ssize_t j = Column(d);
        const uint16_t nc = Dof(j);
        for (uint16_t l = 0; l < nc; ++l) {
          a[ind[Index(i) + k]] = data[Entry(d) + k * nc + l];
          col[ind[Index(i) + k]++] = Index(j) + l;
          if (SingleEntry(d)) continue;
          a[ind[Index(j) + l]] = data[Entry(d) + nc * nr + l * nr + k];
          col[ind[Index(j) + l]++] = Index(i) + k;
        }
      }
    }
  }
  for (size_t i = Index(nR()); i > 0; --i)
    ind[i] = ind[i - 1];
  ind[0] = 0;
}

void Matrix::ClearDirichletValues() {
  const bool *Dirichlet = Vec.D();
  size_t ind = 0;
  for (size_t i = 0; i < nR(); ++i) {
    const uint16_t nr = Dof(i);
    for (uint16_t k = 0; k < nr; ++k) {
      size_t d = Diag(i);
      if (Dirichlet[ind++]) {
        ssize_t e = Entry(d);
        data[e + k * nr + k] = 1.0;
        for (uint16_t l = 0; l < nr; ++l)
          if (l != k) data[e + k * nr + l] = 0.0;
        for (++d; d < Diag(i + 1); ++d) {
          const ssize_t j = Column(d);
          const uint16_t nc = Dof(j);
          e = Entry(d);
          for (uint16_t l = 0; l < nc; ++l)
            data[e + k * nc + l] = 0.0;
          e += nr * nc;
          for (uint16_t l = 0; l < nc; ++l)
            if (Dirichlet[Index(j) + l]) data[e + l * nr + k] = 0.0;
        }
      } else {
        for (++d; d < Diag(i + 1); ++d) {
          const ssize_t j = Column(d);
          const uint16_t nc = Dof(j);
          const ssize_t e = Entry(d) + nr * nc;
          for (uint16_t l = 0; l < nc; ++l)
            if (Dirichlet[Index(j) + l]) data[e + l * nr + k] = 0.0;
        }
      }
    }
  }
  // linear combinations of DoFs at bnd (for Argyris element)
  if (!Vec.BC()) return;
  if (!addBC) return;
  const BCEquations &bc = Vec.GetBCEquations();
  ind = 0;
  for (size_t i = 0; i < nR(); ++i) {
    const uint16_t nr = Dof(i);
    for (uint16_t k = 0; k < nr; ++k, ind++) {
      size_t d = Diag(i);
      if (bc.Exists(ind)) {
        ssize_t e = Entry(d);
        data[e + k * nr + k] = 1.0;
        for (uint16_t l = 0; l < nr; ++l)
          if (l != k) {
            if (bc.Exists(ind, l)) {
              data[e + k * nr + l] = bc.Lambda(ind, l);
            } else {
              data[e + k * nr + l] = 0.0;
            }
          }
        for (++d; d < Diag(i + 1); ++d) {
          const ssize_t j = Column(d);
          const uint16_t nc = Dof(j);
          e = Entry(d);
          for (uint16_t l = 0; l < nc; ++l)
            data[e + k * nc + l] = 0.0;
          e += nr * nc;
          for (uint16_t l = 0; l < nc; ++l)
            if (bc.Exists(Index(j) + l)) data[e + l * nr + k] = 0.0;
        }
      } else {
        for (++d; d < Diag(i + 1); ++d) {
          const ssize_t j = Column(d);
          const uint16_t nc = Dof(j);
          const ssize_t e = Entry(d) + nr * nc;
          for (uint16_t l = 0; l < nc; ++l)
            if (bc.Exists(Index(j) + l)) data[e + l * nr + k] = 0.0;
        }
      }
    }
  }
}

void Matrix::EliminateDirichlet() {
  const bool *Dirichlet = Vec.D();
  if (identifysets() != identifysets_end())
    for (row r = rows(); r != rows_end(); ++r) {
      const uint16_t n = r.NumberOfDofs();
      for (entry e = r.entries(); e != r.entries_end(); ++e) {
        identifyset is = find_identifyset(e());
        if (is == identifysets_end()) continue;
        if (is.master()) continue;
        row r0 = find_row(is[0]);
        row r2 = find_row(e());
        double *A00 = (*this)(r0, r0);
        double *A22 = (*this)(r2, r2);
        double *A01 = (*this)(r0, r);
        double *A21 = (*this)(r2, r);
        const uint16_t m = r2.NumberOfDofs();
        for (uint16_t k = 0; k < n; ++k)
          for (uint16_t l = 0; l < m; ++l) {
            A01[k * m + l] += A21[k * m + l];
            A21[k * m + l] = 0;
            A00[k * m + l] += A22[k * m + l];
            A22[k * m + l] = 0;
          }
      }
    }
  for (identifyset is = identifysets(); is != identifysets_end(); ++is) {
    if (is.master()) continue;
    row r2 = find_row(is());
    row r0 = find_row(is[0]);
    double *A00 = (*this)(r0, r0);
    double *A22 = (*this)(r2, r2);
    double *A20 = (*this)(r2, r0);
    const uint16_t m = r2.NumberOfDofs();
    for (entry e = r2.entries(); e != r2.entries_end(); ++e) {
      row r = find_row(e());
      const uint16_t n = r.NumberOfDofs();
      double *A01 = (*this)(r0, r);
      double *A21 = (*this)(r2, r);
      for (uint16_t k = 0; k < n; ++k)
        for (uint16_t l = 0; l < m; ++l) {
          A01[k * m + l] += A21[k * m + l];
          A21[k * m + l] = 0;
          A00[k * m + l] += A22[k * m + l];
          A22[k * m + l] = 0;
        }
      continue;

      const ssize_t e00 = GetEntryX(r0, r0);
      const ssize_t e22 = GetEntryX(r2, r2);
      const ssize_t e10 = GetEntryX(r, r0);
      const ssize_t e12 = GetEntryX(r, r2);

      mout << "Algebra " << OUT(e10) << OUT(e22) << OUT(e00) << OUT(e12) << endl;
    }
    for (uint16_t k = 0; k < m; ++k) {
      A22[k + m * k] = 1;
      A20[k + m * k] = -1;
    }
  }
  size_t ind = 0;
  for (size_t i = 0; i < nR(); ++i) {
    const uint16_t nr = Dof(i);
    for (uint16_t k = 0; k < nr; ++k) {
      size_t d = Diag(i);
      if (Dirichlet[ind++]) {
        ssize_t e = Entry(d);
        data[e + k * nr + k] = 1.0;
        for (uint16_t l = 0; l < nr; ++l)
          if (l != k) {
            data[e + k * nr + l] = 0.0;
            data[e + l * nr + k] = 0.0;
          }
        for (++d; d < Diag(i + 1); ++d) {
          const ssize_t j = Column(d);
          const uint16_t nc = Dof(j);
          e = Entry(d);
          for (uint16_t l = 0; l < nc; ++l) {
            data[e + k * nc + l] = 0.0;
          }
          e += nr * nc;
          for (uint16_t l = 0; l < nc; ++l)
            if (Dirichlet[Index(j) + l]) {
              data[e + l * nr + k] = 0.0;
            } else data[e + l * nr + k] = 0.0;
        }
      } else {
        for (++d; d < Diag(i + 1); ++d) {
          const ssize_t j = Column(d);
          const uint16_t nc = Dof(j);
          ssize_t e = Entry(d);
          for (uint16_t l = 0; l < nc; ++l) {
            if (Dirichlet[Index(j) + l]) data[e + k * nc + l] = 0.0;
          }
          e += nr * nc;
          for (uint16_t l = 0; l < nc; ++l)
            if (Dirichlet[Index(j) + l]) data[e + l * nr + k] = 0.0;
        }
      }
    }
  }
}

void Matrix::Accumulate() {
  if (identify()) AccumulateIdentify();
  if (parallel()) AccumulateParallel();
}

void Matrix::CommunicateMatrix(ExchangeBuffer &exBuffer) {
  exBuffer.Communicate();
  for (short q = 0; q < PPM->Size(CommSplit()); ++q)
    while (exBuffer.Receive(q).size() < exBuffer.ReceiveSize(q)) {
      Point x;
      exBuffer.Receive(q) >> x;
      row r = find_row(x);
      if (r == rows_end()) THROW("no row in Communicate Matrix")
      const ssize_t id = r.Id();
      const ssize_t d = Entry(Diag(id));
      const uint32_t n = Dof(id);
      for (uint32_t j = 0; j < std::pow(n, 2); ++j) {
        double b;
        exBuffer.Receive(q) >> b;
        data[d + j] += b;
      }
      size_t s;
      exBuffer.Receive(q) >> s;
      for (size_t k = 0; k < s; ++k) {
        Point y;
        uint32_t m;
        exBuffer.Receive(q) >> y >> m;
        const ssize_t dd = r.EntryIndexChecked(y);
        if (dd == -1) {
          double tmp;
          for (uint32_t j = 0; j < m; ++j)
            exBuffer.Receive(q) >> tmp;
        } else {
          double tmp;
          for (uint32_t j = 0; j < m; ++j) {
            exBuffer.Receive(q) >> tmp;
            data[dd + j] += tmp;
          }
        }
      }
    }
  exBuffer.ClearBuffers();
}

void Matrix::AccumulateIdentify() {
  ExchangeBuffer &exBuffer = Buffers().AccumulateMatrixIdentifyBuffer();
  int q = PPM->Proc(CommSplit());
  for (identifyset is = identifysets(); is != identifysets_end(); ++is) {
    const Point &z = is();
    row r = find_row(z);
    const ssize_t id = r.Id();
    const ssize_t d = Entry(Diag(id));
    uint16_t n = Dof(id);
    for (size_t i = 0; i < is.size(); ++i) {
      Point y = is[i];
      exBuffer.Send(q) << y;
      for (uint32_t j = 0; j < n * n; ++j)
        exBuffer.Send(q) << data[d + j];
      exBuffer.Send(q) << r.NumberOfEntries();
      for (entry e = r.entries(); e != r.entries_end(); ++e) {
        exBuffer.Send(q) << e();
        uint32_t m = 2 * n * Dof(e.Id());
        exBuffer.Send(q) << m;
        ssize_t dd = e.EntryIndex();
        for (uint32_t j = 0; j < m; ++j)
          exBuffer.Send(q) << data[dd + j];
      }
    }
  }
  CommunicateMatrix(exBuffer);
}

void Matrix::AccumulateParallel() {
  ExchangeBuffer &exBuffer = Buffers().AccumulateMatrixBuffer();
  for (procset p = procsets(); p != procsets_end(); ++p) {
    const Point &x = p();
    row r = find_row(x);
    const ssize_t id = r.Id();
    const ssize_t d = Entry(Diag(id));
    uint16_t n = Dof(id);
    for (size_t i = 0; i < p.size(); ++i) {
      int q = p[i];
      if (q == PPM->Proc(CommSplit())) continue;
      exBuffer.Send(q) << x;
      for (uint32_t j = 0; j < n * n; ++j)
        exBuffer.Send(q) << data[d + j];
      exBuffer.Send(q) << r.NumberOfEntries();
      for (entry e = r.entries(); e != r.entries_end(); ++e) {
        exBuffer.Send(q) << e();
        const uint32_t m = 2 * n * Dof(e.Id());
        exBuffer.Send(q) << m;
        const ssize_t dd = e.EntryIndex();
        for (uint32_t j = 0; j < m; ++j)
          exBuffer.Send(q) << data[dd + j];
      }
    }
  }
  CommunicateMatrix(exBuffer);
}

void Matrix::Transpose() {
  for (size_t i = 0; i < nR(); ++i) {
    size_t d = Diag(i);
    const uint16_t n = Dof(i);
    const ssize_t e = Entry(d);

    std::vector<std::vector<double>> mem_diag(n);

    for (uint16_t k = 0; k < n; ++k) {
      mem_diag[k].resize(n);
      for (uint16_t l = 0; l < n; ++l) {
        mem_diag[k][l] = data[e + k * n + l];
        data[e + k * n + l] = 0.0;
      }
    }
    for (uint16_t k = 0; k < n; ++k) {
      for (uint16_t l = 0; l < n; ++l) {
        data[e + k * n + l] += mem_diag[l][k];
      }
    }
    for (d = Diag(i) + 1; d < Diag(i + 1); ++d) {
      const ssize_t e_col = Entry(d);
      const ssize_t j = Column(d);
      const uint16_t m = Dof(j);
      std::vector<std::vector<double>> mem_0(n);
      std::vector<std::vector<double>> mem_1(m);
      for (uint16_t k = 0; k < n; ++k) {
        mem_0[k].resize(m);
        for (uint16_t l = 0; l < m; ++l) {
          mem_0[k][l] = data[e_col + k * m + l];
          data[e_col + k * m + l] = 0.0;
        }
      }
      for (uint16_t l = 0; l < m; ++l) {
        mem_1[l].resize(n);
        for (uint16_t k = 0; k < n; ++k) {
          mem_1[l][k] = data[e_col + n * m + l * n + k];
          data[e_col + n * m + l * n + k] = 0.0;
        }
      }
      uint32_t cnt = 0;
      for (uint16_t l = 0; l < n; ++l) {
        for (uint16_t k = 0; k < m; ++k) {
          data[e_col + cnt] += mem_1[k][l];
          ++cnt;
        }
      }
      for (uint16_t l = 0; l < m; ++l) {
        for (uint16_t k = 0; k < n; ++k) {
          data[e_col + cnt] += mem_0[k][l];
          ++cnt;
        }
      }
    }
  }
}

void Matrix::Symmetric() {
  for (size_t i = 0; i < nR(); ++i) {
    const uint16_t n = Dof(i);
    const size_t k = Diag(i);
    const ssize_t e = Entry(k);
    for (uint16_t i0 = 0; i0 < n; ++i0)
      for (uint16_t i1 = 0; i1 < n; ++i1)
        data[e + i1 * n + i0] = data[e + i0 * n + i1];
    for (size_t k = Diag(i) + 1; k < Diag(i + 1); ++k) {
      const ssize_t e = Entry(k);
      const ssize_t j = Column(k);
      const uint16_t m = Dof(j);
      for (uint16_t i0 = 0; i0 < n; ++i0) {
        for (uint16_t i1 = 0; i1 < m; ++i1) {
          data[e + n * m + i1 * m + i0] = data[e + i0 * n + i1];
        }
      }
    }
  }
}

bool Matrix::IsSymmetric() {
  for (size_t i = 0; i < nR(); ++i) {
    const uint16_t n = Dof(i);
    const size_t k = Diag(i);
    const ssize_t e = Entry(k);
    for (uint16_t i0 = 0; i0 < n; ++i0)
      for (uint16_t i1 = 0; i1 < n; ++i1)
        if (!mpp_ba::isNear(data[e + i1 * n + i0], data[e + i0 * n + i1])) return false;
    for (size_t k = Diag(i) + 1; k < Diag(i + 1); ++k) {
      const ssize_t e = Entry(k);
      const ssize_t j = Column(k);
      const uint16_t m = Dof(j);
      for (uint16_t i0 = 0; i0 < n; ++i0) {
        for (uint16_t i1 = 0; i1 < m; ++i1)
          if (!mpp_ba::isNear(data[e + n * m + i1 * m + i0], data[e + i0 * n + i1])) return false;
      }
    }
  }
  return true;
}

void Matrix::print() const {
  auto a = std::cbegin(data);
  size_t zz = 0;
  for (size_t i = 0; i < nR(); ++i) {
    size_t d = Diag(i);
    const uint16_t n = Dof(i);
    mout << n * n << "      ";
    for (uint16_t k = 0; k < n; ++k) {
      for (uint16_t l = 0; l < n; ++l, ++a) {
        mout << " " << *a;
        zz++;
      }
    }
    mout << endl;
    for (++d; d < Diag(i + 1); ++d) {
      const ssize_t j = Column(d);
      const uint16_t m = Dof(j);
      mout << n * m << "      ";
      for (uint16_t k = 0; k < n; ++k) {
        for (uint16_t l = 0; l < m; ++l, ++a) {
          mout << " " << *a;
          zz++;
        }
      }
      mout << endl;
      if (SingleEntry(d)) continue;
      mout << m * n << "      ";
      for (uint16_t k = 0; k < m; ++k) {
        for (uint16_t l = 0; l < n; ++l, ++a) {
          mout << " " << *a;
          zz++;
        }
      }
      mout << endl;
    }
  }
  //    for (row r = rows(); r != rows_end(); ++r) {
  //      mout << r() << " : " << (r.NumberOfEntries() + 1) << endl;
  //      mout << "      ";
  //      const double *AA = this->operator()(r, r);
  //      for (int i = 0; i < r.n() * r.n(); ++i)
  //        mout << " " << AA[i];
  //      mout << endl;
  //      for (auto e = r.entries(); e != r.entries_end(); e++) {
  //        mout << "      ";
  //        row rr = find_row(e());
  //        const double *AA = this->operator()(r, rr);
  //        for (int i = 0; i < r.n() * rr.n(); ++i) {
  //          mout << " " << AA[i];
  //        }
  //        mout << endl;
  //      }
  //      mout << endl;
  //      continue;
  //
  //
  //      mout << r() << " :";
  //      procset p = find_procset(r());
  //      if (p != procsets_end()) mout << " p " << p << endl;
  //      int id = r.Id();
  //      int d = Entry(Diag(id));
  //      int n = Dof(id);
  //      for (int i = 0; i < n; ++i)
  //        mout << " " << A[d + i];
  //      mout << endl;
  //    }
}

std::ostream &operator<<(std::ostream &s, const Matrix &A) {
  SparseMatrix A_sparse(A);
  return s << A_sparse;
}

#ifndef VECTOR_HPP
#define VECTOR_HPP

#include <sys/types.h>
#include <algorithm>
#include <cmath>
#include <cstddef>
#include <cstdint>
#include <format>
#include <functional>
#include <iterator>
#include <memory>
#include <numeric>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

#include "AlgebraFwd.hpp"
#include "RVector.hpp"
#include "VectorMatrixBase.hpp"

void SetQuasiperiodic(const Point &);

const Point &GetQuasiperiodic();

using BCEquation = std::unordered_map<int, double>;

/*
 * Contains equations for BC where not only single DoFs are effected but entire equations have to be
 * considered for the correct BC. Note that the (system of) equations need(s) to be solved for
 * specific DoF, i.e., the i-th equation needs to be of the form
 *    \varphi_j(p) + \sum_{l\neq j} \lambda_l \varphi_j(p) = 0
 * where p denotes the nodal point under consideration. In particular, the coefficient of the i-th
 * shape function is supposed to be 1.0. Note that only conditions within DoFs of one single node
 * can be used (cf. equation above holds for fixed p, i.e., for fixed node)
 */
class BCEquations {
  // key (int) corresponds to row id; value contains (k, \lambda_k) as presented above
  std::unordered_map<size_t, BCEquation> equations{};
public:
  // j denotes the index of node with respect to entire vector size (will be mapped to numbering of
  // row in vector) k denotes the number of DoF at node i
  void AddCondition(const size_t i, const int l, const double lambda);

  inline bool Exists(const size_t i) const { return equations.contains(i); }

  inline bool Exists(const size_t i, const int l) const { return equations.at(i).contains(l); }

  inline double Lambda(const size_t i, const int l) const { return equations.at(i).at(l); }

  inline const BCEquation &Get(const size_t i) const { return equations.at(i); }
};

class DirichletFlags {
  bool *f;
  size_t n;
public:
  inline void ClearDirichletFlags() { std::fill_n(f, n, false); }

  DirichletFlags(const size_t N) : f(new bool[N]), n(N) { ClearDirichletFlags(); }

  ~DirichletFlags() { delete[] f; }

  inline const bool *D() const noexcept { return f; }

  inline bool D(size_t i) const { return f[i]; }

  inline bool *D() noexcept { return f; }
};

enum class DIST_TYPE;

class Vector : public VectorMatrixBase {
  bool accumulateFlag = false;
  BasicVector data;
  std::shared_ptr<DirichletFlags> dirichletFlags;
  std::shared_ptr<BCEquations> bcEquations;
public:
  inline double Max() const { return data.MaxAccumulated(CommSplit()); }

  inline double Min() const { return data.MinAccumulated(CommSplit()); }

  // not parallel for consistent Vectors
  inline double norm() const { return data.normAccumulated(CommSplit()); }

  // abs(sqrt((*this)*(*this))); }

  // Default value provides Vector on fine level
  Vector(std::shared_ptr<const IDiscretization> disc, int spaceLevel = -1, int timeLevel = -1) :
      Vector(std::move(disc), {spaceLevel, timeLevel}) {}

  Vector(std::shared_ptr<const IDiscretization> disc, MeshIndex levels) :
      VectorMatrixBase(std::move(disc), levels), data(VectorMatrixBase::size()),
      dirichletFlags(std::make_shared<DirichletFlags>(VectorMatrixBase::size())),
      bcEquations(nullptr) {}

  Vector(double b, std::shared_ptr<const IDiscretization> disc, int spaceLevel = -1,
         int timeLevel = -1) : Vector(b, std::move(disc), {spaceLevel, timeLevel}) {}

  Vector(double b, std::shared_ptr<const IDiscretization> disc, MeshIndex levels) :
      VectorMatrixBase(std::move(disc), levels), data(b, VectorMatrixBase::size()),
      dirichletFlags(std::make_shared<DirichletFlags>(VectorMatrixBase::size())),
      bcEquations(nullptr) {
    (*this).accumulateFlag = true;
  }

  Vector(const RVector &b, const Vector &other, MeshIndex levels) :
      Vector(b, other.GetSharedDisc(), levels) {}

  Vector(const RVector &b, std::shared_ptr<const IDiscretization> disc, MeshIndex levels);

#if BUILD_UQ

  Vector(std::shared_ptr<const IDiscretization> disc, MeshIndex levels, DIST_TYPE distType);

  Vector(std::shared_ptr<const IDiscretization> disc, MeshIndex levels, DIST_TYPE distType,
         std::vector<double> params);

#endif

  Vector(const Vector &u) :
      VectorMatrixBase(u), data(u.data), dirichletFlags(u.dirichletFlags),
      bcEquations(u.bcEquations) {
    (*this).accumulateFlag = u.GetAccumulateFlag();
  }

  Vector(Vector &&u) :
      VectorMatrixBase(u), data(std::move(u.data)), dirichletFlags(u.dirichletFlags),
      bcEquations(u.bcEquations) {
    (*this).accumulateFlag = u.GetAccumulateFlag();
  }

  Vector(double b, const Vector &u) :
      VectorMatrixBase(u), data(b, u.size()), dirichletFlags(u.dirichletFlags),
      bcEquations(u.bcEquations) {
    (*this).accumulateFlag = true;
  }

  Vector(const constAB<Operator, Vector> &Ov);

  inline size_t size() const noexcept { return data.size(); }

  inline auto begin() const noexcept { return std::cbegin(data); }

  inline auto begin() noexcept { return std::begin(data); }

  inline auto end() const noexcept { return std::cend(data); }

  inline auto end() noexcept { return std::end(data); }

  inline void PrintInfo() const { graph.PrintVectorMemoryInfo(); }

#ifdef BUILD_IA

  inline void SetIADisc(std::shared_ptr<const IAIDiscretization> disc) { iadisc = std::move(disc); }

#endif

  inline Vector &operator=(const Vector &u) {
#ifdef BUILD_IA
    iadisc = u.iadisc;
#endif
    (*this).accumulateFlag = u.GetAccumulateFlag();
    if (u.size() != size()) { THROW("Sizes of Vector do not fit"); }

    data = u.data;
    return *this;
  }

  inline Vector &operator=(double b) {
    (*this).accumulateFlag = true;
    data = b;
    return *this;
  }

  inline Vector &operator*=(double b) {
    data *= b;
    return *this;
  }

  inline Vector &operator*=(int b) {
    data *= b;
    return *this;
  }

  inline Vector &operator/=(double b) {
    data /= b;
    return *this;
  }

  inline Vector &operator+=(const Vector &u) {
    //  data += u.data;
    //  return *this;
    if (u.GetAccumulateFlag() != (*this).GetAccumulateFlag()) {
      Warning(std::format("Adding Vectors with different accumulate Flags: this({}) and u({})",
                          this->GetAccumulateFlag(), u.GetAccumulateFlag()));
    }
    if (u.size() != size()) { THROW("Sizes of Vector do not fit"); }
    data += u.data;
    return *this;
  }

  inline Vector &operator-=(const Vector &u) {
    if (u.GetAccumulateFlag() != (*this).GetAccumulateFlag()) {
      Warning("Subtracting accumulated and not accumulated Vector");
    }
    data -= u.data;
    return *this;
  }

  inline Vector &operator*=(const Vector &u) {
    if (u.GetAccumulateFlag() != (*this).GetAccumulateFlag()) {
      Warning("Multiplying accumulated and not accumulated Vector");
    }
    data *= u.data;
    return *this;
  }

  inline Vector &operator/=(const Vector &u) {
    if (u.GetAccumulateFlag() != (*this).GetAccumulateFlag()) {
      Warning("Dividing accumulated and not accumulated Vector");
    }
    data /= u.data;
    return *this;
  }

  void ConsistentAddition(const Vector &u);

  inline void Clear() {
    (*this).accumulateFlag = false;
    data = 0.0;
  }

  inline Vector &operator=(const constAB<double, Vector> &au) {
    (*this).accumulateFlag = au.second().GetAccumulateFlag();
    data.Multiply(au.first(), au.second().data);
    return *this;
  }

  inline Vector &operator+=(const constAB<double, Vector> &au) {
    if (au.second().GetAccumulateFlag() != (*this).GetAccumulateFlag()) {
      Warning("Adding accumulated and not accumulated Vector");
    }
    data.MultiplyPlus(au.first(), au.second().data);
    return *this;
  }

  inline Vector &operator-=(const constAB<double, Vector> &au) {
    if (au.second().GetAccumulateFlag() != (*this).GetAccumulateFlag()) {
      Warning("Subtracting accumulated and not accumulated Vector");
    }
    data.MultiplyMinus(au.first(), au.second().data);
    return *this;
  }

  inline Vector &operator=(const constAB<int, Vector> &au) {
    (*this).accumulateFlag = au.second().GetAccumulateFlag();
    data.Multiply(au.first(), au.second().data);
    return *this;
  }

  Vector &operator=(const constAB<Vector, Operator> &vO);

  Vector &operator+=(const constAB<Vector, Operator> &vO);

  Vector &operator-=(const constAB<Vector, Operator> &vO);

  Vector &operator=(const constAB<Operator, Vector> &Ov);

  Vector &operator+=(const constAB<Operator, Vector> &Ov);

  Vector &operator-=(const constAB<Operator, Vector> &Ov);

  inline const double &operator[](const size_t i) const { return data[i]; }

  inline double &operator[](const size_t i) { return data[i]; }

  inline const double *operator()() const noexcept { return data(); }

  inline double *operator()() noexcept { return data(); }

  inline const BasicVector &GetData() const noexcept { return data; }

  inline BasicVector &GetData() noexcept { return data; }

  inline const double *operator()(const size_t i) const { return &data[Index(i)]; }

  inline double *operator()(const size_t i) { return &data[Index(i)]; }

  inline const double *operator()(const row &r) const { return (*this)(r.Id()); }

  inline double *operator()(const row &r) { return (*this)(r.Id()); }

  inline const double *operator()(const Point &z) const { return (*this)(find_row(z)); }

  inline double *operator()(const Point &z) { return (*this)(find_row(z)); }

  inline double operator()(const size_t i, const std::ptrdiff_t k) const {
    return data[Index(i) + k];
  }

  inline double &operator()(const size_t i, const std::ptrdiff_t k) { return data[Index(i) + k]; }

  inline double operator()(const row &r, const std::ptrdiff_t k) const {
    return (*this)(r.Id(), k);
  }

  inline double &operator()(const row &r, const std::ptrdiff_t k) { return (*this)(r.Id(), k); }

  inline double operator()(const Point &z, const size_t k) const { return (*this)(find_row(z), k); }

  inline double &operator()(const Point &z, const size_t k) { return (*this)(find_row(z), k); }

  inline const DirichletFlags &GetDirichletFlags() const noexcept { return *dirichletFlags; }

  inline std::shared_ptr<DirichletFlags> GetDirichletFlagsPtr() const noexcept {
    return dirichletFlags;
  }

  inline void ClearDirichletFlags() { dirichletFlags->ClearDirichletFlags(); }

  inline const bool *D() const { return dirichletFlags->D(); }

  inline bool *D() { return dirichletFlags->D(); }

  inline const bool *D(const size_t i) const { return dirichletFlags->D() + Index(i); }

  inline bool *D(const size_t i) { return dirichletFlags->D() + Index(i); }

  inline const bool *D(const row &r) const { return D(r.Id()); }

  inline bool *D(const row &r) { return D(r.Id()); }

  inline const bool *D(const Point &z) const { return D(find_row(z)); }

  inline bool *D(const Point &z) { return D(find_row(z)); }

  inline bool D(const size_t i, const size_t k) const { return D(i)[k]; }

  inline bool &D(const size_t i, const size_t k) { return D(i)[k]; }

  inline bool D(const row &r, const size_t k) const { return D(r)[k]; }

  inline bool &D(const row &r, const size_t k) { return D(r)[k]; }

  inline bool D(const Point &z, const size_t k) const { return D(z)[k]; }

  inline bool &D(const Point &z, const size_t k) { return D(z)[k]; }

  inline const BCEquations &GetBCEquations() const noexcept { return *bcEquations; };

  inline bool BC() const { return bcEquations.operator bool(); }

  inline bool BC(const size_t i, const std::ptrdiff_t k) const {
    return bcEquations->Exists(Index(i) + k);
  }

  inline bool BC(const size_t i, const std::ptrdiff_t k, const size_t l) {
    return bcEquations->Exists(Index(i) + k, l);
  }

  inline double BCLambda(const size_t i, const std::ptrdiff_t k, const size_t l) const {
    return bcEquations->Lambda(Index(i) + k, l);
  }

  inline const BCEquation &BCGet(const size_t i, const std::ptrdiff_t k) const {
    return bcEquations->Get(Index(i) + k);
  }

  inline void BCadd(const size_t i, const std::ptrdiff_t k, const size_t l, const double lambda) {
    if (!BC()) { bcEquations = std::make_shared<BCEquations>(); }
    bcEquations->AddCondition(Index(i) + k, l, lambda);
  }

  inline bool BC(const row &r) const {
    bool a = BC(r, 0);
    for (uint16_t k = 1; k < r.NumberOfDofs(); ++k)
      a |= BC(r, k);
    return a;
  }

  inline bool BC(const row &r, const std::ptrdiff_t k) const { return BC(r.Id(), k); }

  inline bool BC(const row &r, const std::ptrdiff_t k, const size_t l) { return BC(r.Id(), k, l); }

  inline double BCLambda(const row &r, const std::ptrdiff_t k, const size_t l) const {
    return BCLambda(r.Id(), k, l);
  }

  inline const BCEquation &BCGet(const row &r, const std::ptrdiff_t k) const {
    return BCGet(r.Id(), k);
  }

  inline void BCadd(const row &r, const std::ptrdiff_t k, const size_t l, const double lambda) {
    BCadd(static_cast<size_t>(r.Id()), k, l, lambda);
  }

  inline bool BC(const Point &z) const { return BC(find_row(z)); }

  inline bool BC(const Point &z, const std::ptrdiff_t k) const { return BC(find_row(z), k); }

  inline bool BC(const Point &z, const std::ptrdiff_t k, const size_t l) {
    return BC(find_row(z), k, l);
  }

  inline double BCLambda(const Point &z, const std::ptrdiff_t k, const size_t l) const {
    return BCLambda(find_row(z), k, l);
  }

  inline const BCEquation &BCGet(const Point &z, const size_t k) const {
    return BCGet(find_row(z), k);
  }

  inline void BCadd(const Point &z, const std::ptrdiff_t k, const size_t l, const double lambda) {
    BCadd(find_row(z), k, l, lambda);
  }

  Saver &save(Saver &saver) const;

  Loader &load(Loader &loader);

  void print() const;

  friend double operator*(const Vector &u, const Vector &v);

  friend double ConsistentScalarProduct(const Vector &u, const Vector &v);

  inline double ConsistentNorm() const { return sqrt(ConsistentScalarProduct(*this, *this)); }

  void ClearDirichletValues() {
    std::transform(std::cbegin(data), std::cend(data), dirichletFlags->D(), std::begin(data),
                   [](const auto &value, const auto &flag) {
                     if (flag) { return double{}; }
                     return value;
                   });
    // linear combinations of DoFs at bnd
    if (!BC()) return;
    for (size_t i = 0; i < size(); ++i) {
      if (bcEquations->Exists(i)) { data[i] = double{}; }
    }
  }

  void DirichletConsistent();

  void Consistent2Additive();

  void Accumulate();

  inline void Collect() {
    accumulateFlag = false;
    if (identify()) CollectIdentify();
    if (parallel()) CollectParallel();
  }

  void MakeAdditive();

  void MakeConsistent();

  void Average();

  inline bool GetAccumulateFlag() const noexcept { return accumulateFlag; }

  inline void SetAccumulateFlag(bool flag) noexcept { accumulateFlag = flag; }

  inline bool IsAccumulated() {
    bool accumulated = true;
    if (identify()) accumulated = accumulated && IsAccumulatedIdentify();
    if (parallel()) accumulated = accumulated && IsAccumulatedParallel();
    return accumulated;
  }

  bool IsAccumulatedParallel();

  bool IsAccumulatedIdentify();

  bool IsAdditive();

  void CommunicateVector(ExchangeBuffer &exBuffer);

  template<typename S>
  friend LogTextStream<S> &operator<<(LogTextStream<S> &s, const Vector &u);
private:
  void AccumulateIdentify();

  void AccumulateParallel();

  void CollectIdentify();

  void CollectParallel();
};

inline Saver &operator<<(Saver &saver, const Vector &u) { return u.save(saver); }

inline Loader &operator>>(Loader &loader, Vector &u) { return u.load(loader); }

inline constAB<Vector, Operator> operator*(const Vector &v, const Operator &A) {
  return constAB<Vector, Operator>(v, A);
}

inline constAB<Operator, Vector> operator*(const Operator &A, const Vector &v) {
  return constAB<Operator, Vector>(A, v);
}

inline constAB<double, Vector> operator*(const double &a, const Vector &v) {
  return constAB<double, Vector>(a, v);
}

inline constAB<int, Vector> operator*(const int &a, const Vector &v) {
  return constAB<int, Vector>(a, v);
}

inline Vector operator+(const Vector &u, const Vector &v) {
  if (u.GetAccumulateFlag() != v.GetAccumulateFlag()) {
    Warning("Adding accumulated and not accumulated Vector");
  }
  Vector w = u;
  return w += v;
}

inline Vector operator-(const Vector &u, const Vector &v) {
  if (u.GetAccumulateFlag() != v.GetAccumulateFlag()) {
    Warning("Subtracting accumulated and not accumulated Vector");
  }
  Vector w = u;
  return w -= v;
}

inline Vector HadamardProductVector(const Vector &u, const Vector &v) {
  Vector product(0.0, u);
  std::transform(std::cbegin(u), std::cend(u), std::cbegin(v), std::begin(product),
                 std::multiplies<>());
  return product;
}

inline Vector ComponentSqrt(const Vector &u) {
  Vector root(0.0, u);
  std::transform(std::cbegin(u), std::cend(u), std::begin(root),
                 [](const auto &value) { return std::sqrt(value); });
  return root;
}

inline Vector ComponentDivide(const Vector &u, const Vector &v) {
  Vector division(0.0, u);
  std::transform(std::cbegin(u), std::cend(u), std::cbegin(v), std::begin(division),
                 std::divides<>());
  return division;
}

inline double norm(const Vector &u) { return u.norm(); }

inline double norm(const double &u) { return std::abs(u); }

inline double norm(double &u) { return std::abs(u); }

class Vectors : public VectorMatrixBase {
  bool sharesFlags;
protected:
  std::vector<std::unique_ptr<Vector>> V;
public:
  Vectors(const size_t n, std::shared_ptr<const IDiscretization> disc, int spaceLevel = -1,
          int timeLevel = -1, bool shared = true) :
      Vectors(n, disc, {spaceLevel, timeLevel}, shared) {}

  Vectors(const size_t n, double b, std::shared_ptr<const IDiscretization> disc,
          int spaceLevel = -1, int timeLevel = -1, bool shared = true) :
      Vectors(n, b, disc, {spaceLevel, timeLevel}, shared) {}

  Vectors(const size_t n, std::shared_ptr<const IDiscretization> disc, MeshIndex levels,
          bool shared = true) : VectorMatrixBase(disc, levels), V(n) {
    if (n < 1) return;
    V[0] = std::make_unique<Vector>(disc, levels);
    for (size_t i = 1; i < n; ++i) {
      if (shared) V[i] = std::make_unique<Vector>(*V[0]);
      else V[i] = std::make_unique<Vector>(disc, levels);
    }
  }

  Vectors(const size_t n, double b, std::shared_ptr<const IDiscretization> disc, MeshIndex levels,
          bool shared = true) : VectorMatrixBase(disc, levels), V(n), sharesFlags(shared) {
    if (n < 1) return;
    V[0] = std::make_unique<Vector>(b, disc, levels);
    for (size_t i = 1; i < n; ++i) {
      if (sharesFlags) V[i] = std::make_unique<Vector>(*V[0]);
      else V[i] = std::make_unique<Vector>(b, disc, levels);
    }
  }

  Vectors(const size_t n, const Vector &u, bool shared = true) :
      VectorMatrixBase(u), V(n), sharesFlags(shared) {
    for (size_t i = 0; i < n; ++i) {
      if (sharesFlags) V[i] = std::make_unique<Vector>(u);
      else {
        V[i] = std::make_unique<Vector>(disc, Level());
        *V[i] = u;
      }
    }
  }

  Vectors(const size_t n, const Vectors &u, bool shared = true) :
      VectorMatrixBase(u), V(n), sharesFlags(shared) {
    for (size_t i = 0; i < std::min(n, u.size()); ++i) {
      if (sharesFlags) V[i] = std::make_unique<Vector>(u[i]);
      else {
        V[i] = std::make_unique<Vector>(disc, Level());
        *V[i] = u[i];
      }
    }
    for (size_t i = std::min(n, u.size()); i < n; ++i) {
      if (sharesFlags) V[i] = std::make_unique<Vector>(0.0, u[0]);
      else { V[i] = std::make_unique<Vector>(0.0, disc, Level()); }
    }
  }

  Vectors(const Vectors &u) : VectorMatrixBase(u), V(u.size()), sharesFlags(u.sharesFlags) {
    for (size_t i = 0; i < V.size(); ++i) {
      if (sharesFlags) V[i] = std::make_unique<Vector>(*(u.V[i]));
      else {
        V[i] = std::make_unique<Vector>(disc, Level());
        *(V[i]) = *(u.V[i]);
      }
    }
  }

  Vectors(Vectors &&u) : VectorMatrixBase(u), V(std::move(u.V)), sharesFlags(u.sharesFlags) {}

  /// selects part of u (indices included)
  Vectors(const Vectors &u, size_t startIdx, const ssize_t endIdx = -1);

  Vectors(const constAB<Operator, Vectors> &Ov);

  inline size_t size() const noexcept { return V.size(); }

  constexpr auto begin() noexcept { return std::begin(V); }

  constexpr auto begin() const noexcept { return std::cbegin(V); }

  constexpr auto end() noexcept { return std::end(V); }

  constexpr auto end() const noexcept { return std::cend(V); }

  void resize(const size_t N);

  /// Data remains while resizing the object
  void resizeData(const size_t N) {
    if (N == V.size()) return;
    if (N < V.size()) {
      V.erase(V.begin() + N, V.end());
    } else {
      if (V.empty()) V.push_back(std::make_unique<Vector>(0.0, disc, Level()));
      for (size_t i = V.size(); i < N; ++i)
        V.push_back(std::make_unique<Vector>(0.0, *V[0]));
    }
  }

  /// Sets the data, the vectors object will not be resized (cf assigning operator)
  inline void set(const Vectors &u) {
    for (size_t i = 0; i < std::min(size(), u.size()); ++i)
      *(V[i]) = u[i];
    for (size_t i = std::min(size(), u.size()); i < size(); ++i)
      *(V[i]) = 0.0;
  }

  inline const Vector &operator[](const size_t i) const noexcept { return *(V[i]); }

  inline Vector &operator[](const size_t i) noexcept { return *(V[i]); }

  inline void erase(const std::ptrdiff_t i) { V.erase(V.begin() + i); }

  inline void reverse() { std::reverse(V.begin(), V.end()); }

  inline Vector &back() noexcept { return *V.back(); }

  inline const Vector &back() const noexcept { return *V.back(); }

#ifdef BUILD_IA

  inline void SetIADisc(std::shared_ptr<const IAIDiscretization> disc) {
    iadisc = disc;
    for (const auto &vec : V) {
      vec->SetIADisc(disc);
    }
  }

#endif

  inline Vectors &operator=(const Vector &u) {
#ifdef BUILD_IA
    iadisc = u.GetSharedIADisc();
#endif
    for (const auto &v : V)
      (*v) = u;
    return *this;
  }

  inline Vectors &operator=(const Vectors &u) {
#ifdef BUILD_IA
    iadisc = u.iadisc;
#endif
    const size_t n = V.size();
    for (size_t i = 0; i < n; ++i)
      (*V[i]) = *(u.V[i]);
    return *this;
  }

  inline Vectors &operator=(double b) {
    for (const auto &v : V)
      (*v) = b;
    return *this;
  }

  inline Vectors &operator*=(double b) {
    for (const auto &v : V)
      (*v) *= b;
    return *this;
  }

  inline Vectors &operator*=(int b) {
    for (const auto &v : V)
      (*v) *= static_cast<double>(b);
    return *this;
  }

  inline Vectors &operator/=(double b) {
    for (const auto &v : V)
      (*v) /= b;
    return *this;
  }

  inline Vectors &operator+=(const Vector &u) {
    for (const auto &v : V)
      (*v) += u;
    return *this;
  }

  inline Vectors &operator+=(const Vectors &u) {
    for (size_t i = 0; i < V.size(); ++i)
      (*V[i]) += *(u.V[i]);
    return *this;
  }

  inline Vectors &operator-=(const Vector &u) {
    for (const auto &v : V)
      (*v) -= u;
    return *this;
  }

  inline Vectors &operator-=(const Vectors &u) {
    for (size_t i = 0; i < V.size(); ++i)
      (*V[i]) -= *(u.V[i]);
    return *this;
  }

  inline Vectors &operator=(const constAB<double, Vectors> &au) {
    for (size_t i = 0; i < au.second().size(); ++i)
      *V[i] = constAB<double, Vector>(au.first(), au.second()[i]);
    return *this;
  }

  inline Vectors &operator+=(const constAB<double, Vectors> &au) {
    for (size_t i = 0; i < au.second().size(); ++i)
      *V[i] += constAB<double, Vector>(au.first(), au.second()[i]);
    return *this;
  }

  inline Vectors &operator-=(const constAB<double, Vectors> &au) {
    for (size_t i = 0; i < au.second().size(); ++i)
      *V[i] -= constAB<double, Vector>(au.first(), au.second()[i]);
    return *this;
  }

  inline Vectors &operator=(const constAB<int, Vectors> &au) {
    for (size_t i = 0; i < au.second().size(); ++i)
      *V[i] = constAB<int, Vector>(au.first(), au.second()[i]);
    return *this;
  }

  Vectors &operator=(const constAB<Vectors, Operator> &vO);

  Vectors &operator+=(const constAB<Vectors, Operator> &vO);

  Vectors &operator-=(const constAB<Vectors, Operator> &vO);

  Vectors &operator=(const constAB<Operator, Vectors> &Ov);

  Vectors &operator+=(const constAB<Operator, Vectors> &Ov);

  Vectors &operator-=(const constAB<Operator, Vectors> &Ov);

  inline const double *operator()(const size_t n, const size_t i) const { return (*V[n])(i); }

  inline double *operator()(const size_t n, const size_t i) { return (*V[n])(i); }

  inline const double *operator()(const size_t n, const row &r) const { return (*V[n])(r.Id()); }

  inline double *operator()(const size_t n, const row &r) { return (*V[n])(r.Id()); }

  inline double operator()(const size_t n, const size_t i, const std::ptrdiff_t k) const {
    return (*V[n])(i, k);
  }

  inline double &operator()(const size_t n, const size_t i, const std::ptrdiff_t k) {
    return (*V[n])(i, k);
  }

  inline double operator()(const size_t n, const row &r, const std::ptrdiff_t k) const {
    return (*V[n])(r, k);
  }

  inline double &operator()(const size_t n, const row &r, const std::ptrdiff_t k) {
    return (*V[n])(r, k);
  }

  inline std::vector<double> norm() const {
    std::vector<double> no(V.size());
    std::transform(std::cbegin(V), std::cend(V), std::begin(no),
                   [](const auto &v) { return v->norm(); });
    return no;
  }

  double normScalar() const;

  inline void ConsistentAddition(const Vectors &u) {
    auto vIt = std::begin(V);
    for (const auto &uV : u) {
      (*vIt)->ConsistentAddition(*uV);
      ++vIt;
    }
  }

  friend double ConsistentScalarProduct(const Vectors &u, const Vectors &v);

  inline double ConsistentScalarNorm() const {
    return std::sqrt(ConsistentScalarProduct(*this, *this));
  }

  inline bool GetSharesFlags() const noexcept { return sharesFlags; }

  inline void SetSharesFlags(bool sharesFlags) noexcept { Vectors::sharesFlags = sharesFlags; }

  Saver &save(Saver &saver) const;

  Loader &load(Loader &loader);

  inline void ClearDirichletValues() {
    for (auto &v : V) {
      v->ClearDirichletValues();
    }
  }

  inline void DirichletConsistent() {
    for (auto &v : V) {
      v->DirichletConsistent();
    }
  }

  inline void Accumulate() {
    for (auto &v : V) {
      v->Accumulate();
    }
  }

  inline void Collect() {
    for (auto &v : V) {
      v->Collect();
    }
  }

  inline void MakeAdditive() {
    for (auto &v : V) {
      v->MakeAdditive();
    }
  }

  inline void Average() {
    for (auto &v : V) {
      v->Average();
    }
  }

  inline void SetAccumulateFlags(bool flag) {
    for (auto &v : V) {
      v->SetAccumulateFlag(flag);
    }
  }

  inline void Clear() {
    for (auto &v : *this) {
      v->Clear();
    }
  }
};

inline Saver &operator<<(Saver &saver, const Vectors &u) { return u.save(saver); }

inline Loader &operator>>(Loader &loader, Vectors &u) { return u.load(loader); }

inline constAB<Vectors, Operator> operator*(const Vectors &v, const Operator &A) {
  return constAB<Vectors, Operator>(v, A);
}

inline constAB<Operator, Vectors> operator*(const Operator &A, const Vectors &v) {
  return constAB<Operator, Vectors>(A, v);
}

inline constAB<double, Vectors> operator*(const double &a, const Vectors &v) {
  return constAB<double, Vectors>(a, v);
}

inline constAB<int, Vectors> operator*(const int &a, const Vectors &v) {
  return constAB<int, Vectors>(a, v);
}

inline double operator*(const Vectors &u, const Vectors &v) {
  if (u.size() != v.size()) THROW("sizes dont match")
  return std::inner_product(std::cbegin(u), std::cend(u), std::cbegin(v), 0.0, std::plus<>(),
                            [](const auto &first, const auto &second) {
                              return (*first) * (*second);
                            });
}

inline Vectors operator+(const Vectors &u, const Vectors &v) {
  Vectors w = u;
  return w += v;
}

inline Vectors operator-(const Vectors &u, const Vectors &v) {
  Vectors w = u;
  return w -= v;
}

namespace norms {

template<typename ElementType>
double L2(const Vector &u) {
  double l2 = 0.0;
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    const auto elem = ElementType(u, *c);
    for (int q = 0; q < elem.nQ(); ++q) {
      double w = elem.QWeight(q);
      double U = elem.Value(q, u);
      l2 += w * U * U;
    }
  }
  return sqrt(PPM->SumOnCommSplit(l2, u.CommSplit()));
}

template<typename ElementType>
double H1(const Vector &u) {
  double h1 = 0.0;
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    const auto &elem = ElementType(u, *c);
    for (int q = 0; q < elem.nQ(); ++q) {
      double w = elem.QWeight(q);
      double U = elem.Value(q, u);
      VectorField DU = elem.Derivative(q, u);
      h1 += w * U * U + w * DU * DU;
    }
  }
  return sqrt(PPM->SumOnCommSplit(h1, u.CommSplit()));
}

template<typename ElementType>
double L1(const Vector &u) {
  double l1 = 0;
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    const auto &elem = ElementType(u, *c);
    for (int q = 0; q < elem.nQ(); ++q) {
      double w = elem.QWeight(q);
      double U = elem.Value(q, u);
      l1 += std::abs(w * U);
    }
  }
  return PPM->SumOnCommSplit(l1, u.CommSplit());
}

double L1(const Vector &u);

double L2(const Vector &u);

double H1(const Vector &u);

double Quantity(const Vector &u, std::string quantity = "");

} // namespace norms

/*
 *  Allows lazy-memory allocation of Vectors.
 */

class LazyVectors {

  std::vector<std::unique_ptr<Vector>> v;
  std::unique_ptr<Vector> u;
public:
  LazyVectors(const size_t size, const Vector &u) : v(size), u(new Vector(u)) {};

  inline Vector &operator[](const size_t i) {
    if (i < 0 && i >= v.size()) {
      Exit(std::format("Trying to access LazyVector with i={} but size was {}", i, v.size()))
    }
    if (!v[i]) { v[i] = std::make_unique<Vector>(*u); }
    return *v[i];
  }
};

template<typename S>
LogTextStream<S> &operator<<(LogTextStream<S> &s, const Vector &u) {
  std::vector<row> R(u.nR());

  for (row r = u.rows(); r != u.rows_end(); ++r)
    R[r.Id()] = r;

  for (size_t k = 0; k < R.size(); ++k) {
    identifyset identifySet = u.find_identifyset(R[k]());
    procset procSet = u.find_procset(R[k]());

    s << R[k]() << " ";
    if (identifySet == u.identifysets_end()) s << " ";
    else s << "I";

    for (uint16_t i = 0; i < R[k].NumberOfDofs(); ++i)
      s << " " << u.D(R[k], i);

    s << " :";
    for (uint16_t i = 0; i < R[k].NumberOfDofs(); ++i) {
      double a = u(R[k], i);
      if (abs(a) < 1e-10) s << " 0";
      else s << " " << a;
    }

    if (u.BC()) {
      s << " :";
      if (u.BC(R[k])) {
        for (uint16_t i = 0; i < R[k].NumberOfDofs(); ++i) {
          if (u.BC(k, i)) {
            s << " n_" << i;
            const BCEquation &eq_i = u.BCGet(R[k], i);
            for (auto part : eq_i) {
              if (part.second < 0.0) s << part.second << "*n_" << part.first;
              else s << "+" << part.second << "*n_" << part.first;
            }
          }
        }
      } else {
        s << " empty";
      }
    }

    if (procSet != u.procsets_end()) {
      s << " p "
        << "[ ";
      for (const auto &p : *procSet) {
        s << p << " ";
      }
      s << "]";
    }
    s << "\n";

    if (identifySet == u.identifysets_end()) continue;

    for (size_t i = 0; i < identifySet.size(); ++i) {
      ssize_t l = u.Idx(identifySet[i]);
      if (l == -1) continue;
      if ((i == 0) && (!identifySet.master())) s << R[l]() << " M";
      else s << R[l]() << " i";
      for (uint16_t i = 0; i < R[l].NumberOfDofs(); ++i)
        s << " " << u.D(R[l], i);
      s << " :";
      for (uint16_t i = 0; i < R[l].NumberOfDofs(); ++i) {
        double a = u(R[l], i);
        if (abs(a) < 1e-10) s << " 0";
        else s << " " << a;
      }
      s << "\n";
    }
  }

  return s;
}

namespace evaluate {
double value(const Point &globalPoint, const Vector &u, const Cell &C);
} // namespace evaluate

#endif // VECTOR_HPP

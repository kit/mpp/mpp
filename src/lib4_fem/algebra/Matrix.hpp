#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <cstddef>
#include <memory>

#include "AlgebraFwd.hpp"
#include "Operator.hpp"
#include "RVector.hpp"
#include "VectorMatrixBase.hpp"

class Matrix : public VectorMatrixBase, public Operator {
  BasicVector data;
  const Vector &Vec;
  std::unique_ptr<SparseMatrix> sm;
  bool collectAfterProd = true;
  bool addBC = true;
public:
  mutable int applications = 0;

  inline void setCollectAfterProd(bool collect) noexcept { collectAfterProd = collect; }

  inline double Max() const { return data.MaxAccumulated(CommSplit()); }

  inline double Min() const { return data.MinAccumulated(CommSplit()); }

  inline double norm() const { return data.normAccumulated(CommSplit()); }

  explicit Matrix(const Vector &U, bool correctionAddBCArgyris = true);

  Matrix(const Matrix &A);

  Matrix(Matrix &&A);

  virtual ~Matrix();

  inline void PrintInfo() const { graph.MatrixMemoryInfo().PrintInfo(); }

  inline const SparseMatrix *GetSparse() const noexcept { return &*sm; }

  void CreateSparse();

  Matrix &operator=(const Matrix &A);

  Matrix &operator=(Matrix &&A) noexcept;

  Matrix &operator=(double b) {
    data = b;
    return *this;
  }

  inline const Vector &GetVector() const noexcept { return Vec; }

  inline size_t size() const noexcept { return VectorMatrixBase::size(); }

  inline const BasicVector &GetData() const noexcept { return data; }

  inline BasicVector &GetData() noexcept { return data; }

  inline const double *operator()(const size_t d) const { return &data[Entry(d)]; }

  inline double *operator()(const size_t d) { return &data[Entry(d)]; }

  inline const double *operator()(const row &r0, const row &r1) const {
    return &data[GetEntry(r0, r1)];
  }

  inline double *operator()(const row &r0, const row &r1) { return &data[GetEntry(r0, r1)]; }

  Matrix &operator+=(const constAB<double, Matrix> &aA);

  inline Matrix &operator+=(const Matrix &A) { return *this += constAB<double, Matrix>(1.0, A); };

  Matrix &operator*=(const double &a) {
    data *= a;
    return *this;
  }

  void multiply(Vector &u, const Vector &v) const override;

  void multiply(Vectors &u, const Vectors &v) const override;

  void multiply_plus(Vector &, const Vector &) const override;

  void multiply_transpose_plus(Vector &, const Vector &) const override;

  void multiply_plus(Vectors &, const Vectors &) const override;

  void multiply_transpose_plus(Vectors &, const Vectors &) const override;

  void multiply_minus(Vector &b, const Vector &u) const override;

  void copy(double *, int *, int *) const;

  void Transpose();

  void Symmetric();

  bool IsSymmetric();

  void print() const;

  size_t rowsize() const { return VectorMatrixBase::rowsize(); }

  void ClearDirichletValues();

  void EliminateDirichlet();

  void Accumulate();
private:
  void CommunicateMatrix(ExchangeBuffer &exBuffer);

  void AccumulateIdentify();

  void AccumulateParallel();
protected:
  void plusMatTVec(Vector &b, const Vector &u) const;

  void plusMatTVecs(Vectors &b, const Vectors &u) const;

  void plusMatVec(Vector &b, const Vector &u) const;

  void matVecs(Vectors &u, const Vectors &v) const;

  void plusMatVecs(Vectors &b, const Vectors &u) const;

  void matVec(Vector &u, const Vector &v) const;

  void minusMatVec(Vector &b, const Vector &u) const;
};

std::ostream &operator<<(std::ostream &s, const Matrix &u);

inline constAB<Operator, Vector> operator*(const Matrix &A, const Vector &v) { return {A, v}; }

inline constAB<Operator, Vectors> operator*(const Matrix &A, const Vectors &v) { return {A, v}; }

inline constAB<double, Matrix> operator*(const double &a, const Matrix &A) { return {a, A}; }

#endif // MATRIX_HPP

#include "Vector.hpp"
#include <sys/types.h>

#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstddef>
#include <cstdint>
#include <format>
#include <functional>
#include <iterator>
#include <memory>
#include <numeric>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

#include "Operator.hpp"
#include "ProcSet.hpp"
#include "VectorAccess.hpp"

Point K = Origin;

void SetQuasiperiodic(const Point &k) { K = k; }

const Point &GetQuasiperiodic() { return K; }

void BCEquations::AddCondition(const size_t i, const int k, const double lambda) {
  auto entry = equations.find(i);
  if (entry != equations.end()) {
    entry->second[k] = lambda;
  } else {
    BCEquation cd;
    cd[k] = lambda;
    equations[i] = cd;
  }
}

Vector::Vector(const RVector &b, std::shared_ptr<const IDiscretization> disc, MeshIndex levels) :
    VectorMatrixBase(std::move(disc), levels),
    dirichletFlags(std::make_shared<DirichletFlags>(VectorMatrixBase::size())),
    bcEquations(nullptr) {
  if (b.size() != VectorMatrixBase::size()) {
    std::stringstream message;
    message << "Sizes of RVector (" << b.size() << ") does not fit discretization ("
            << VectorMatrixBase::size() << ")";
    THROW(message.str());
  }
  data = b;
  MakeConsistent();
  this->accumulateFlag = true;
}

Vector::Vector(const constAB<Operator, Vector> &Ov) :
    VectorMatrixBase(Ov.second()), data(Ov.second().data),
    dirichletFlags(Ov.second().dirichletFlags), bcEquations(Ov.second().bcEquations) {
  Ov.first().multiply(*this, Ov.second());
}

void Vector::ConsistentAddition(const Vector &u) {
  for (row r = u.rows(); r != u.rows_end(); ++r) {
    procset p = u.find_procset(r());
    if (p == u.procsets_end()) {
      for (uint16_t i = 0; i < r.NumberOfDofs(); ++i) {
        (*this)(r, i) += u(r, i);
      }
    } else {
      if (p.master() == PPM->Proc(CommSplit())) {
        for (uint16_t i = 0; i < r.NumberOfDofs(); ++i) {
          (*this)(r, i) += u(r, i);
        }
      }
    }
  }
}

double ConsistentScalarProduct(const Vector &u, const Vector &v) {
  double s = 0.0;
  for (row r = u.rows(); r != u.rows_end(); ++r) {
    procset p = u.find_procset(r());
    if (p == u.procsets_end()) {
      for (uint16_t i = 0; i < r.NumberOfDofs(); ++i) {
        s += u(r, i) * v(r, i);
      }
    } else {
      if (p.master() == PPM->Proc(u.CommSplit())) {
        for (uint16_t i = 0; i < r.NumberOfDofs(); ++i) {
          s += u(r, i) * v(r, i);
        }
      }
    }
  }
  return PPM->SumOnCommSplit(s, u.CommSplit());
}

Vector &Vector::operator=(const constAB<Vector, Operator> &vO) {
  vO.second().multiply_transpose(*this, vO.first());
  return *this;
}

Vector &Vector::operator+=(const constAB<Vector, Operator> &vO) {
  vO.second().multiply_transpose_plus(*this, vO.first());
  return *this;
}

Vector &Vector::operator-=(const constAB<Vector, Operator> &vO) {
  vO.second().multiply_transpose_minus(*this, vO.first());
  return *this;
}

Vector &Vector::operator=(const constAB<Operator, Vector> &Ov) {
  Ov.first().multiply(*this, Ov.second());
  return *this;
}

Vector &Vector::operator+=(const constAB<Operator, Vector> &Ov) {
  Ov.first().multiply_plus(*this, Ov.second());
  return *this;
}

Vector &Vector::operator-=(const constAB<Operator, Vector> &Ov) {
  Ov.first().multiply_minus(*this, Ov.second());
  return *this;
}

void Vector::Consistent2Additive() {
  for (procset p = (*this).procsets(); p != (*this).procsets_end(); ++p) {
    ssize_t i = (*this).Id(p());
    identifyset is = (*this).find_identifyset(p());
    if (is != (*this).identifysets_end()) continue;
    if (p.master() != PPM->Proc((*this).CommSplit())) {
      for (uint16_t k = 0; k < (*this).Dof(i); ++k)
        (*this)(i, k) = 0.0;
    }
  }
  Vector v((*this));
  (*this) = 0.0;
  std::for_each(cells(), cells_end(), [&](const auto &c) {
    ::rows R((*this).GetMatrixGraph(), c);
    RowValues u_c((*this), R);
    RowValues v_c(v, R);
    for (size_t i = 0; i < R.size(); ++i) {
      for (uint16_t k = 0; k < R[i].NumberOfDofs(); ++k)
        u_c(i, k) = v_c(i, k);
    }
  });
}

void Vector::DirichletConsistent() {
  ExchangeBuffer &exBuffer = Buffers().DirichletParallelBuffer();
  for (procset p = procsets(); p != procsets_end(); ++p) {
    ssize_t i = Id(p());
    for (const auto &q : *p) {
      if (q == PPM->Proc(CommSplit())) continue;
      exBuffer.Send(q) << p();
      for (uint16_t k = 0; k < Dof(i); ++k) {
        if (D(i, k)) exBuffer.Send(q) << (*this)(i, k);
        else exBuffer.Send(q) << double(infty);
      }
    }
  }
  exBuffer.Communicate();
  for (short q = 0; q < PPM->Size(CommSplit()); ++q) {
    while (exBuffer.Receive(q).size() < exBuffer.ReceiveSize(q)) {
      Point z;
      //      dpout(100) << "s " << exBuffer.Receive(q).size()
      //                 << " S " << exBuffer.Receive(q).Size()
      //                 << " q " << q
      //                 << endl;
      exBuffer.Receive(q) >> z;
      ssize_t i = Idx(z);
      if (i == -1) pout << OUT(z) << " not found \n";
      assert(i != -1);
      for (uint16_t k = 0; k < Dof(i); ++k) {
        double a;
        exBuffer.Receive(q) >> a;
        if (a != infty) {
          D(i, k) = true;
          (*this)(i, k) = a;
        }
      }
    }
  }
  exBuffer.ClearBuffers();
  for (identifyset is = identifysets(); is != identifysets_end(); ++is) {
    ssize_t j = Id(is());
    for (const auto &i : *is) {
      ssize_t l = Idx(i);
      if (l == -1) continue;
      for (uint16_t k = 0; k < Dof(l); ++k) {
        if (D(l, k)) {
          D(j, k) = true;
          (*this)(j, k) = (*this)(l, k);
        }
      }
    }
  }
}

void Vector::Accumulate() {
  if (accumulateFlag) {
    Warning("Accumulating already accumulated Vector!");
  } else {
    accumulateFlag = true;
  }
  if (identify()) AccumulateIdentify();
  if (parallel()) AccumulateParallel();
  if (!identify()) return;
  const Point &K = GetQuasiperiodic();
  if (K == Origin) return;
  for (identifyset is = identifysets(); is != identifysets_end(); ++is) {
    const Point &z = is();
    ssize_t j = Id(z);
    double zK = 0;
    for (int i = 0; i < z.SpaceDim(); ++i)
      if (z[i] == 1) zK += K[i];
    double s = exp((zK) * -1);
    for (uint16_t k = 0; k < Dof(j); ++k)
      (*this)(j, k) *= s;
  }
}

void Vector::MakeAdditive() {
  accumulateFlag = false;
  for (row r = rows(); r != rows_end(); ++r) {
    procset p = find_procset(r());
    if (p == procsets_end()) continue;
    if (p.master() == PPM->Proc(CommSplit())) continue;
    for (uint16_t k = 0; k < r.NumberOfDofs(); ++k)
      (*this)(r, k) = 0;
  }
  for (identifyset is = identifysets(); is != identifysets_end(); ++is) {
    row r = find_row(is());
    if (is.master()) continue;
    for (uint16_t k = 0; k < r.NumberOfDofs(); ++k)
      (*this)(r, k) = 0;
  }
}

void Vector::MakeConsistent() {
  ExchangeBuffer &exBuffer = Buffers().ConsistentMatrixBuffer();
  for (procset p = procsets(); p != procsets_end(); ++p) {
    ssize_t i = Id(p());
    if (p.master() != PPM->Proc(CommSplit())) continue;
    for (size_t j = 0; j < p.size(); ++j) {
      intproc q = p[j];
      if (q == PPM->Proc(CommSplit())) continue;
      exBuffer.Send(q) << p();
      for (uint16_t k = 0; k < Dof(i); ++k)
        exBuffer.Send(q) << (*this)(i, k);
    }
  }
  MakeAdditive();
  CommunicateVector(exBuffer);
}

void Vector::Average() {
  ExchangeBuffer &exBuffer = Buffers().AverageParallelBuffer();
  for (procset p = procsets(); p != procsets_end(); ++p) {
    ssize_t i = Id(p());
    if (p.master() != PPM->Proc(CommSplit())) continue;
    double s = 1.0 / double(p.size());
    for (uint16_t k = 0; k < Dof(i); ++k)
      (*this)(i, k) *= s;
    for (const auto &q : *p) {
      if (q == PPM->Proc(CommSplit())) continue;
      exBuffer.Send(q) << p();
      for (uint16_t k = 0; k < Dof(i); ++k)
        exBuffer.Send(q) << (*this)(i, k);
    }
  }
  CommunicateVector(exBuffer);
}

bool Vector::IsAccumulatedParallel() {
  ExchangeBuffer &exBuffer = Buffers().AccumulateParallelBuffer();
  for (procset p = procsets(); p != procsets_end(); ++p) {
    ssize_t i = Id(p());
    for (const auto &q : *p) {
      if (q == PPM->Proc(CommSplit())) continue;
      exBuffer.Send(q) << p();
      for (uint16_t k = 0; k < Dof(i); ++k)
        exBuffer.Send(q) << (*this)(i, k);
    }
  }
  exBuffer.Communicate();
  bool accumulated = true;
  for (short q = 0; q < PPM->Size(CommSplit()); ++q) {
    while (exBuffer.Receive(q).size() < exBuffer.ReceiveSize(q)) {
      Point z;
      exBuffer.Receive(q) >> z;
      ssize_t i = Idx(z);
      for (uint16_t k = 0; k < Dof(i); ++k) {
        double a;
        exBuffer.Receive(q) >> a;
        if (i != -1) accumulated &= ((*this)(i, k) == a);
      }
    }
  }
  exBuffer.ClearBuffers();

  return PPM->And(accumulated, CommSplit());
}

bool Vector::IsAccumulatedIdentify() {
  ExchangeBuffer &exBuffer = Buffers().AccumulateIdentifyBuffer();
  for (identifyset is = identifysets(); is != identifysets_end(); ++is) {
    const Point &z = is();
    ssize_t j = Id(z);
    for (const auto &y : *is) {
      exBuffer.Send(PPM->Proc(CommSplit())) << y;
      for (uint16_t k = 0; k < Dof(j); ++k)
        exBuffer.Send(PPM->Proc(CommSplit())) << (*this)(j, k);
    }
  }
  exBuffer.Communicate();
  bool accumulated = true;
  for (short q = 0; q < PPM->Size(CommSplit()); ++q) {
    while (exBuffer.Receive(q).size() < exBuffer.ReceiveSize(q)) {
      Point z;
      exBuffer.Receive(q) >> z;
      ssize_t i = Idx(z);
      for (uint16_t k = 0; k < Dof(i); ++k) {
        double a;
        exBuffer.Receive(q) >> a;
        if (i != -1) accumulated &= ((*this)(i, k) == a);
      }
    }
  }
  exBuffer.ClearBuffers();

  return PPM->And(accumulated, CommSplit());
}

bool Vector::IsAdditive() {
  bool additive = true;

  for (row r = rows(); r != rows_end(); ++r) {
    procset p = find_procset(r());
    if (p == procsets_end()) continue;
    if (p.master() == PPM->Proc(CommSplit())) continue;
    for (uint16_t k = 0; k < r.NumberOfDofs(); ++k)
      additive &= (*this)(r, k) == 0.0;
  }
  for (identifyset is = identifysets(); is != identifysets_end(); ++is) {
    row r = find_row(is());
    if (is.master()) continue;
    for (uint16_t k = 0; k < r.NumberOfDofs(); ++k)
      additive &= (*this)(r, k) == 0.0;
  }
  return PPM->And(additive, CommSplit());
}

void Vector::CommunicateVector(ExchangeBuffer &exBuffer) {
  exBuffer.Communicate();
  for (short q = 0; q < PPM->Size(CommSplit()); ++q) {
    while (exBuffer.Receive(q).size() < exBuffer.ReceiveSize(q)) {
      Point z;
      //      std::cout << PPM->Proc() << ": s " << exBuffer.Receive(q).size()
      //                << " S " << exBuffer.Receive(q).Size()
      //                << " q " << q
      //                << endl;
      exBuffer.Receive(q) >> z;
      ssize_t i = Idx(z);
      if constexpr (DebugLevel > 0) {
        if (exBuffer.Receive(q).size() + Dof(i) >= exBuffer.ReceiveSize(q)) {
          //          std::cout << PPM->Proc()
          //                    << ": s " << exBuffer.Receive(q).size()
          //                    << " S " << exBuffer.Receive(q).Size()
          //                    << " q " << q
          //                    << " i " << i
          //                    << " z " << z
          //                    << " "
          //                    << endl;
          Exit(std::format("{}: Out of bounds.", PPM->Proc(CommSplit())));
        }
      }
      for (uint16_t k = 0; k < Dof(i); ++k) {
        double a;
        exBuffer.Receive(q) >> a;
        //        std::cout << PPM->Proc() << ": " << z << " from " << q
        //                  << " recv " << a
        //                  << " with i = " << i
        //                  << " and k = " << k << endl;
        if (i != -1) (*this)(i, k) += a;
      }
    }
  }
  exBuffer.ClearBuffers();
}

void Vector::AccumulateIdentify() {
  ExchangeBuffer &exBuffer = Buffers().AccumulateIdentifyBuffer();
  for (identifyset is = identifysets(); is != identifysets_end(); ++is) {
    const Point &z = is();
    ssize_t j = Id(z);
    for (const auto &y : *is) {
      exBuffer.Send(PPM->Proc(CommSplit())) << y;
      for (uint16_t k = 0; k < Dof(j); ++k)
        exBuffer.Send(PPM->Proc(CommSplit())) << (*this)(j, k);
    }
  }
  CommunicateVector(exBuffer);
}

void Vector::AccumulateParallel() {
  ExchangeBuffer &exBuffer = Buffers().AccumulateParallelBuffer();
  for (procset p = procsets(); p != procsets_end(); ++p) {
    ssize_t i = Id(p());
    for (const auto &q : *p) {
      if (q == PPM->Proc(CommSplit())) continue;
      exBuffer.Send(q) << p();
      for (uint16_t k = 0; k < Dof(i); ++k)
        exBuffer.Send(q) << (*this)(i, k);
    }
  }
  CommunicateVector(exBuffer);
}

void Vector::CollectIdentify() {
  ExchangeBuffer &exBuffer = Buffers().CollectIdentifyBuffer();
  for (identifyset is = identifysets(); is != identifysets_end(); ++is) {
    if (is.master()) continue;
    const Point &z = is();
    ssize_t j = Id(z);
    exBuffer.Send(PPM->Proc(CommSplit())) << is[0];
    for (uint16_t k = 0; k < Dof(j); ++k) {
      exBuffer.Send(PPM->Proc(CommSplit())) << (*this)(j, k);
      (*this)(j, k) = 0;
    }
  }
  CommunicateVector(exBuffer);
}

void Vector::CollectParallel() {
  ExchangeBuffer &exBuffer = Buffers().CollectParallelBuffer();
  for (procset p = procsets(); p != procsets_end(); ++p) {
    ssize_t i = Id(p());
    intproc q = p.master();
    if (q == PPM->Proc(CommSplit())) continue;
    exBuffer.Send(q) << p();
    for (uint16_t k = 0; k < Dof(i); ++k) {
      exBuffer.Send(q) << (*this)(i, k);
      (*this)(i, k) = 0;
    }
  }
  CommunicateVector(exBuffer);
}

void Vector::print() const {
  std::vector<row> R(Size());
  for (row r = rows(); r != rows_end(); ++r)
    R[r.Id()] = r;
  for (const auto &row : R) {
    mout << row() << " :";
    for (uint16_t i = 0; i < row.NumberOfDofs(); ++i)
      mout << " " << D(row, i);
    mout << " :";
    for (uint16_t i = 0; i < row.NumberOfDofs(); ++i)
      mout << " " << (*this)(row, i);
    mout << endl;
  }
  return;

  for (row r = rows(); r != rows_end(); ++r) {
    mout << r() << " :";
    for (uint16_t i = 0; i < r.NumberOfDofs(); ++i)
      mout << " " << D(r, i);
    mout << " :";
    for (uint16_t i = 0; i < r.NumberOfDofs(); ++i)
      mout << " " << (*this)(r, i);
    mout << endl;
  }
}

Saver &Vector::save(Saver &saver) const {
  saver << size();
  for (const auto &value : data) {
    saver << value;
  }
  return saver;
}

Loader &Vector::load(Loader &loader) {
  size_t s;
  loader >> s;
  if (s != size()) THROW("Size does not fit")
  for (auto &value : data) {
    loader >> value;
  }
  return loader;
}

size_t computeLength(const size_t length, const ssize_t startIdx, ssize_t endIdx) {
  if (endIdx < 0 || endIdx >= length) endIdx = length - 1;
  return endIdx - startIdx + 1;
}

Vectors::Vectors(const Vectors &u, size_t startIdx, const ssize_t endIdx) :
    VectorMatrixBase(u), V(computeLength(u.size(), startIdx, endIdx)), sharesFlags(u.sharesFlags) {
  for (size_t i = 0; i < V.size(); ++i, ++startIdx) {
    if (sharesFlags) V[i] = std::make_unique<Vector>(*(u.V[startIdx]));
    else {
      V[i] = std::make_unique<Vector>(disc, Level());
      *(V[i]) = *(u.V[startIdx]);
    }
  }
}

Vectors::Vectors(const constAB<Operator, Vectors> &Ov) :
    VectorMatrixBase(Ov.second()), V(Ov.second().size()), sharesFlags(Ov.second().sharesFlags) {
  for (size_t i = 0; i < V.size(); ++i) {
    if (sharesFlags) V[i] = std::make_unique<Vector>(Ov.second()[i]);
    else {
      V[i] = std::make_unique<Vector>(disc, Level());
      *V[i] = Ov.second()[i];
    }
  }
  Ov.first().multiply(*this, Ov.second());
}

void Vectors::resize(const size_t N) {
  if (N == V.size()) {
    for (const auto &v : V)
      *v = 0.0;
  } else if (N < V.size()) {
    V.erase(V.begin() + N, V.end());
    for (const auto &v : V)
      *v = 0.0;
  } else {
    if (V.empty()) V.push_back(std::make_unique<Vector>(0.0, disc, Level()));
    else {
      for (const auto &v : V)
        *v = 0.0;
    }
    for (size_t i = V.size(); i < N; ++i) {
      if (sharesFlags) {
        V.push_back(std::make_unique<Vector>(0.0, *V[0]));
      } else {
        V.push_back(std::make_unique<Vector>(0.0, disc, Level()));
      }
    }
  }
}

Vectors &Vectors::operator=(const constAB<Vectors, Operator> &vO) {
  vO.second().multiply_transpose(*this, vO.first());
  return *this;
}

Vectors &Vectors::operator+=(const constAB<Vectors, Operator> &vO) {
  vO.second().multiply_transpose_plus(*this, vO.first());
  return *this;
}

Vectors &Vectors::operator-=(const constAB<Vectors, Operator> &vO) {
  vO.second().multiply_transpose_minus(*this, vO.first());
  return *this;
}

Vectors &Vectors::operator=(const constAB<Operator, Vectors> &Ov) {
  Ov.first().multiply(*this, Ov.second());
  return *this;
}

Vectors &Vectors::operator+=(const constAB<Operator, Vectors> &Ov) {
  Ov.first().multiply_plus(*this, Ov.second());
  return *this;
}

Vectors &Vectors::operator-=(const constAB<Operator, Vectors> &Ov) {
  Ov.first().multiply_minus(*this, Ov.second());
  return *this;
}

double Vectors::normScalar() const { return std::sqrt((*this) * (*this)); }

double ConsistentScalarProduct(const Vectors &u, const Vectors &v) {
  return std::inner_product(std::cbegin(u), std::cend(u), std::cbegin(v), 0.0, std::plus<>(),
                            [](const auto &first, const auto &second) {
                              return ConsistentScalarProduct(*first, *second);
                            });
}

Saver &Vectors::save(Saver &saver) const {
  saver << size();
  for (const auto &v : V) {
    saver << *v;
  }
  return saver;
}

Loader &Vectors::load(Loader &loader) {
  size_t s;
  loader >> s;
  resize(s);
  for (auto &v : V) {
    loader >> *v;
  }
  return loader;
}

double operator*(const Vector &u, const Vector &v) {
  // Todo: Is there a case where both Vectors are on different communicators?
  if (u.GetAccumulateFlag() && v.GetAccumulateFlag()) {
    Warning("Both vectors accumulated in scalar product!");
  }
  double value = u.data * v.data;
  value = PPM->SumOnCommSplit(value, u.CommSplit());
  return value;
}

#include "DGDiscretization.hpp"
#include "DGElement.hpp"
#include "LagrangeDiscretization.hpp"
#include "RTLagrangeDiscretization.hpp"
#include "RTLagrangeElement.hpp"
#include "ScalarElement.hpp"

double norms::L1(const Vector &u) {
  if (typeid(u.GetDisc()) == typeid(LagrangeDiscretization)) {
    return norms::L1<ScalarElement>(u);
  } else if (typeid(u.GetDisc()) == typeid(DGDiscretization)) {
    return norms::L1<DGElement>(u);
  } else {
    THROW("Unknown Discretization type")
  }
}

double norms::L2(const Vector &u) {
  if (typeid(u.GetDisc()) == typeid(LagrangeDiscretization)) {
    return norms::L2<ScalarElement>(u);
  } else if (typeid(u.GetDisc()) == typeid(RTLagrangeDiscretization)) {
    return norms::L2<RTLagrangeElement>(u);
  } else if (typeid(u.GetDisc()) == typeid(DGDiscretization)) {
    return norms::L2<DGElement>(u);
  } else {
    THROW("Unknown Discretization type")
  }
}

double norms::H1(const Vector &u) {
  if (typeid(u.GetDisc()) == typeid(LagrangeDiscretization)) {
    return norms::H1<ScalarElement>(u);
  } else if (typeid(u.GetDisc()) == typeid(DGDiscretization)) {
    return norms::H1<DGElement>(u);
  } else {
    THROW("Unknown Discretization type")
  }
}

double norms::Quantity(const Vector &u, std::string quantity) {
  if (quantity.empty()) { Config::Get("Quantity", quantity); }
  if (quantity == "L1") {
    return norms::L1(u);
  } else if (quantity == "L2") {
    return norms::L2(u);
  } else if (quantity == "H1") {
    return norms::H1<ScalarElement>(u);
  } else {
    Exit(std::format("Quantity \"{}\" not implemented", quantity));
  }
}

double evaluate::value(const Point &globalPoint, const Vector &u, const Cell &C) {
  if (typeid(u.GetDisc()) == typeid(LagrangeDiscretization)) {
    return ScalarElement(u, C).Value(C.GlobalToLocal(globalPoint), u);
  } else if (typeid(u.GetDisc()) == typeid(DGDiscretization)) {
    return DGElement(u, C).Value(C.GlobalToLocal(globalPoint), u);
    ;
  } else {
    THROW("Unknown Discretization type")
  }
}

#include "VectorProcSet.hpp"

#include <algorithm>
#include <cstddef>
#include <iterator>
#include <vector>

#include "Vector.hpp"

void VectorProcSet::add2(const ProcSet &Q, int &m, int local, int global) {
  ProcSet P(Q);
  P.Sort();
  auto it = vecPS.begin();
  it += m;

  while (it != vecPS.end() && (*it).getps().size() < P.size()) {
    ++it;
    ++m;
  }
  while (it != vecPS.end() && (*it).getps().size() == P.size()) {
    int oldm = m;
    unsigned int i = 0;
    while (i < P.size() && (*it).getps()[i] == P[i])
      ++i;
    if (i == P.size()) { return; }
    if (it != vecPS.end() && (*it).getps().size() == P.size() && (*it).getps()[i] < P[i]) {
      ++it;
      ++m;
    }
    if (oldm == m) break;
  }
  vector_ps &v = *vecPS.emplace(it);
  v.getps().Add(P);
  v.set_local_size(local);
  v.set_global_size(global);
  v.set_position(-1);
  ++num_sorted_elements;
}

VectorProcSet::VectorProcSet(const Vector &U) :
    own_proc(PPM->Proc(U.CommSplit())), total_proc(PPM->Size(U.CommSplit())) {
  int size = U.GetMatrixGraph().GetRows().SumOfDoFs();

  VectorProcSet::add(ProcSet(own_proc, 0));

  vecPS.resize(1);
  vecPS[0].create(size);
  vecPS[0].set_local_size(0);
  vecPS[0].set_global_size(0);

  std::vector<std::vector<row>> R(0);
  for (row r = U.rows(); r != U.rows_end(); ++r) {
    procset _ps = U.find_procset(r());
    if (_ps != U.procsets_end()) {
      int i = findequalProcSet(_ps->second);
      if (i != -1) {
        R[i - 1].push_back(r);
      } else {
        R.emplace_back(1, r);
        VectorProcSet::add(_ps->second);
      }
    } else {
      int idx = U.Index(r.Id());
      for (int k = 0; k < r.NumberOfDofs(); ++k)
        vecPS[0].Add(idx + k);
    }
  }

  vecPS[0].set_real_local_size();
  vecPS[0].set_global_size(vecPS[0].get_local_size());

  for (int b = 0; b < R.size(); ++b) {
    std::sort(R[b].begin(), R[b].end(), [](const row &r0, const row &r1) { return r0() < r1(); });

    int vecb = b + 1;
    vecPS[vecb].create(size);
    vecPS[vecb].set_local_size(0);
    for (row r : R[b]) {
      int idx = U.Index(r.Id());
      for (int k = 0; k < r.NumberOfDofs(); ++k)
        vecPS[vecb].Add(idx + k);
    }
    vecPS[vecb].set_real_local_size();
    vecPS[vecb].set_global_size(vecPS[vecb].get_local_size());
  }
  sort_global();
}

void VectorProcSet::sort_global() {
  for (auto &vps : vecPS)
    vps.getps().Sort();
  for (unsigned int i = 0; i < vecPS.size(); ++i) {
    for (unsigned int j = i + 1; j < vecPS.size(); ++j)
      if (vecPS[j].getps().size() < vecPS[i].getps().size()) { std::swap(vecPS[i], vecPS[j]); }
  }
  for (unsigned int i = 0; i < vecPS.size(); ++i) {
    for (unsigned int j = i + 1; j < vecPS.size(); ++j) {
      if (vecPS[i].getps().size() == vecPS[j].getps().size()) {
        ProcSet Phelp1;
        Phelp1.Add(vecPS[i].getps());
        ProcSet Phelp2;
        Phelp2.Add(vecPS[j].getps());
        int minP1 = minPS(Phelp1);
        int minP2 = minPS(Phelp2);
        while (minP1 == minP2) {
          minP1 = minPS(Phelp1);
          minP2 = minPS(Phelp2);
          Phelp1.erase(minP1);
          Phelp2.erase(minP2);
        }
        if (minP1 > minP2) { std::swap(vecPS[i], vecPS[j]); }
      }
    }
  }
  num_sorted_elements = vecPS.size();
}

void VectorProcSet::combine_procs() {
  // Bestimme jeweilige Groesse fuer die Kommunikation
  int own_sz = 0;
  int sendsize = 1;
  for (const auto &vps : vecPS)
    own_sz += vps.getps().size() + 2;
  std::vector<int> sending(own_sz);
  std::vector<int> all_sz(total_proc);
  PPM->Allgather(&own_sz, sendsize, all_sz.data(), vecPS[0].getps().CommSplit());

  size_t total_rcv = 0;
  std::vector<int> displs(total_proc);
  displs[0] = 0;
  for (int i = 0; i < total_proc; ++i) {
    total_rcv += all_sz[i];
    if (i < total_proc - 1) displs[i + 1] = displs[i] + all_sz[i];
  }

  std::vector<int> receiving(total_rcv);

  auto posIt = std::begin(sending);
  for (const auto &vps : vecPS) {
    *(posIt++) = static_cast<int>(vps.getps().size());
    for (short q : vps.getps())
      *(posIt++) = static_cast<int>(q);
    *(posIt++) = (vps.get_local_size());
  }
  PPM->Allgatherv(sending.data(), own_sz, receiving.data(), all_sz.data(), displs.data(),
                  vecPS[0].getps().CommSplit());

  int i = 0;
  auto rcvIt = std::cbegin(receiving);
  int tmps, tmpq, sz;
  while (i < static_cast<int>(total_rcv)) {
    ProcSet PS;
    PS.resize(0);
    tmps = *(rcvIt++);
    for (int s = 0; s < tmps; ++s) {
      tmpq = *(rcvIt++);
      PS.Add(tmpq);
    }
    sz = *(rcvIt++);
    i += tmps + 2;
    int m = 0;
    add2(PS, m, -1, sz);
  }
}

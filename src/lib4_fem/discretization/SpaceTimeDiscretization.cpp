#include "SpaceTimeDiscretization.hpp"

namespace {
CELLTYPE getMeshCellTypeOrNone(const Mesh &mesh) {
  if (mesh.CellCount() > 0) { return SpaceCellType(mesh.cells().Type()); }
  return CELLTYPE::NONE;
}
}

template<typename T, int sDim, int tDim>
STDiscretizationT<T, sDim, tDim>::STDiscretizationT(const Meshes &meshes, DegreePair degree,
                                                    int pDim, bool singleEntries,
                                                    bool blockDiagonal) :
    IDiscretizationT<>(meshes, "SpaceTimeDiscretization"),
    cell_type(getMeshCellTypeOrNone(meshes.fine())), defaultDegree(degree), pDim(pDim),
    singleEntries(singleEntries), blockDiagonal(blockDiagonal) {}

template<typename T, int sDim, int tDim>
void STDiscretizationT<T, sDim, tDim>::CreateAdaptivityLevel(
    const std::unordered_map<Point, DegreePair> &map, MeshIndex level) {
  if (IDiscretization::graphs.find(level) != IDiscretization::graphs.end()) {
    THROW("Adaptivitylevel already initialized: " + level.str())
  }
  IDiscretization::graphs[level] = std::move(createMatrixGraph(level, map));
}

// TODO: Consider moving the definitions into the header
template class STDiscretizationT<>;

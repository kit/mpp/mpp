#ifndef MPP_MAIN_HPP
#define MPP_MAIN_HPP

#if USE_SPACETIME
#include "FWISTExperiment.hpp"
FWISTExperiment FWIExperimentST() {
  auto fwiExperiment = FWISTExperiment(PDESolverConfig());
  fwiExperiment.Method();
  return fwiExperiment;
}
#endif

#if BUILD_UQ

#include "StochasticGradientDescent.hpp"
#include "MultiLevelEstimator.hpp"
#include "SingleLevelEstimator.hpp"
#include "SequentialMonteCarlo.hpp"
#include "Random.hpp"

SequentialMonteCarlo SMCExperiment() {
  Random::Initialize();
  auto estimator = SequentialMonteCarlo(SequentialMonteCarloConfig());
  estimator.Method();
  estimator.EstimatorResults();
  return estimator;
}

StochasticGradientDescent SGDExperiment() {
  mout.StartBlock("SGDExperiment");
  mout << "Start" << endl;

  Random::Initialize();

  auto sgd = StochasticGradientDescent(SGDConfig());
  sgd.Method();

  mout.EndBlock();
  mout << endl;

  sgd.PrintResults();
  MemoryLogger::PrintInfo();

  return sgd;
}

SingleLevelEstimator MCExperiment() {
  mout.StartBlock("MCExperiment");
  mout << "Start" << endl;

  Random::Initialize();

  auto estimator = SingleLevelEstimator(SLEstimatorConfig());
  estimator.Method(std::nullopt);

  mout.EndBlock();
  mout << endl;

  estimator.PrintInfo();
  MemoryLogger::PrintInfo();

  return estimator;
}

MultiLevelEstimator MLMCExperiment() {
  mout.StartBlock("MLMCExperiment");
  mout << "Start" << endl;

  Random::Initialize();

  auto estimator = MultiLevelEstimator(MLEstimatorConfig());
  estimator.Method(std::nullopt);

  mout.EndBlock();
  mout << endl;

  estimator.PrintInfo();
  MemoryLogger::PrintInfo();

  return estimator;
}

// Todo: Setup SC Experiment.

#endif

#endif //MPP_MAIN_HPP

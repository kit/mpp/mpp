#include <cstddef>
#include <memory>
#include <string>
#include <utility>

#include <CoarseGeometry.hpp>
#include <Distribution.hpp>
#include <Mesh.hpp>
#include <MeshSettings.hpp>
#include <Meshes.hpp>
#include <MeshesCreator.hpp>
#include <Parallel.hpp>
#include <gtest/gtest.h>
#include "lib2_mesh/mesh/MeshIndex.hpp"

#include "TestEnvironment.hpp"
#include "TestMeshesPrintInfo.hpp"

class TestMeshes : public Meshes {
public:
  explicit TestMeshes(MeshSettings &&settings) : Meshes(std::move(settings)) {}

  MeshesContainer::node_type extract(const MeshIndex &lp) { return meshes.extract(lp); }

  void insert(MeshesContainer::node_type &&node) { meshes.insert(std::move(node)); }
};

TEST(TestMeshes, TestSpaceTimeMeshes) {
#ifndef USE_SPACETIME
  GTEST_SKIP() << "SpaceTime is not enabled";
#endif

  static constexpr int scales = 2;
  static constexpr int coarseLevel = 3;
  static constexpr int distributeLevel = 2;
  static constexpr int fineLevel = 4;

  static constexpr MeshIndex coarseLp = MeshIndex{coarseLevel, -1};
  static constexpr MeshIndex distributedLp = MeshIndex{distributeLevel, distributeLevel};
  static constexpr MeshIndex fineLp = MeshIndex{fineLevel, fineLevel};
  static constexpr MeshIndex maxDefaultLp = MeshIndex{fineLevel, fineLevel + scales};

  MeshSettings settings;
  settings.coarseLevel = coarseLevel;
  settings.distributeLevel = distributeLevel;
  settings.fineLevel = fineLevel;
  settings.timeRefinement = scales;
  settings.coarseGeometry =
      CreateCoarseGeometryShared("SpaceTimeSquare", VertexDataList(settings.vertexData),
                                 CellDataList(settings.cellData));

  TestMeshes meshes(std::move(settings));

  // Test if all meshes are present
  EXPECT_EQ(meshes.Size(), (fineLevel - distributeLevel + 1) /* Space only meshes */
                               + (fineLevel - distributeLevel + 1)
                                     * (fineLevel - distributeLevel + 1 + scales) /* ST meshes */);
  EXPECT_TRUE(meshes.Contains(coarseLp));
  EXPECT_TRUE(meshes.Contains(MeshIndex{fineLevel, -1}));
  EXPECT_TRUE(meshes.Contains(distributedLp));
  EXPECT_TRUE(meshes.Contains(MeshIndex{fineLevel, distributeLevel}));
  EXPECT_TRUE(meshes.Contains(MeshIndex{distributeLevel, fineLevel}));
  EXPECT_TRUE(meshes.Contains(fineLp));
  EXPECT_TRUE(meshes.Contains(MeshIndex{distributeLevel, fineLevel + scales}));
  EXPECT_TRUE(meshes.Contains(maxDefaultLp));

  // Test lazy init for > fineLevel & erase
  const std::size_t defaultSize = meshes.Size();
  static constexpr MeshIndex beyondMaxDefaultLp = MeshIndex{fineLevel + 1, fineLevel + scales + 1};
  {
    const Mesh &newMesh = meshes[beyondMaxDefaultLp];
    EXPECT_EQ(newMesh.Level(), beyondMaxDefaultLp);
  }
  /* Added: {fineLevel + 1, fineLevel + scales},
            {fineLevel, fineLevel + scales + 1},
            {fineLevel + 1, fineLevel + scales + 1} */
  EXPECT_EQ(meshes.Size(), defaultSize + 3);
  EXPECT_EQ(meshes.Erase(beyondMaxDefaultLp), 1);
  EXPECT_EQ(meshes.Size(), defaultSize + 2);

  // Test if meshes are distributed
  EXPECT_EQ((meshes[MeshIndex{distributeLevel, -1}].ProcSetsCount()), 0);
  EXPECT_GT((meshes[distributedLp].ProcSetsCount()), 0);
  EXPECT_GT((meshes[fineLp].ProcSetsCount()), 0);
}

TEST(TestMeshes, TestSpaceMeshes) {
  static constexpr int coarseLevel = 3;
  static constexpr int distributeLevel = 2;
  static constexpr int fineLevel = 4;

  static constexpr MeshIndex coarseLp = MeshIndex{coarseLevel, -1};
  static constexpr MeshIndex distributeLp = MeshIndex{distributeLevel, -1};
  static constexpr MeshIndex fineLp = MeshIndex{fineLevel, -1};

  MeshSettings settings;
  settings.coarseLevel = coarseLevel;
  settings.distributeLevel = distributeLevel;
  settings.fineLevel = fineLevel;
  settings.coarseGeometry =
      CreateCoarseGeometryShared("Interval", VertexDataList(settings.vertexData),
                                 CellDataList(settings.cellData));

  TestMeshes meshes(std::move(settings));

  // Test if all meshes are present
  const std::size_t defaultSize = meshes.Size();
  EXPECT_EQ(defaultSize, fineLevel - distributeLevel + 1);
  EXPECT_TRUE(meshes.Contains(coarseLp));
  EXPECT_TRUE(meshes.Contains(distributeLp));
  EXPECT_TRUE(meshes.Contains(fineLp));

  // Test lazy init for > fineLevel & erase
  static constexpr MeshIndex beyondFineLp = MeshIndex{fineLevel + 1, -1, 0, 0};
  {
    const Mesh &newMesh = meshes[beyondFineLp];
    EXPECT_EQ(newMesh.Level(), beyondFineLp);
  }
  EXPECT_EQ(meshes.Size(), defaultSize + 1);
  EXPECT_EQ(meshes.Erase(beyondFineLp), 1);
  EXPECT_EQ(meshes.Size(), defaultSize);

  // Test init for new commSplits
  PPM->Barrier(0);
  static constexpr MeshIndex newCommSplitLevel = MeshIndex{coarseLevel, -1, 0, 1};
  const Mesh &newCommSplitMesh = meshes[newCommSplitLevel];
  EXPECT_EQ(newCommSplitMesh.Level(), newCommSplitLevel);
  EXPECT_EQ(meshes.Size(), 2 * defaultSize);
  EXPECT_NE(&newCommSplitMesh, &(meshes[MeshIndex{coarseLevel, -1, 0, 0}]));
  PPM->Barrier(0);

  // Test ignore adaptivityLevel
  const Mesh *const meshPtrWithoutAdaptivity = &meshes[MeshIndex{fineLevel, -1, 0, 0}];
  const Mesh *const meshPtrWithAdaptivity = &meshes[MeshIndex{fineLevel, -1, 1, 0}];
  EXPECT_EQ(meshPtrWithoutAdaptivity, meshPtrWithAdaptivity);

  // Test if meshes are distributed
  EXPECT_GT((meshes[distributeLp].ProcSetsCount()), 0);
  EXPECT_GT((meshes[fineLp].ProcSetsCount()), 0);

  // Test distribution of lazy created meshes
  auto coarseNode = meshes.extract(coarseLp);
  auto distributedNode = meshes.extract(distributeLp);
  meshes.Clear();
  meshes.insert(std::move(coarseNode));
  const auto &newDistributedMesh = meshes[MeshIndex{distributeLevel, distributeLevel}];
  EXPECT_EQ(distributedNode.mapped().Level(), newDistributedMesh.Level());
  EXPECT_EQ(distributedNode.mapped().CellCount(), newDistributedMesh.CellCount());
  EXPECT_EQ(distributedNode.mapped().VertexCount(), newDistributedMesh.VertexCount());
  EXPECT_EQ(distributedNode.mapped().FaceCount(), newDistributedMesh.FaceCount());
  EXPECT_EQ(distributedNode.mapped().EdgeCount(), newDistributedMesh.EdgeCount());
  EXPECT_EQ(distributedNode.mapped().BoundaryFaceCount(), newDistributedMesh.BoundaryFaceCount());
  EXPECT_EQ(distributedNode.mapped().ProcSetsCount(), newDistributedMesh.ProcSetsCount());
}

TEST(TestMeshes, TestSingleMeshMeshes) {
  static constexpr int level = 0;
  static constexpr MeshIndex lp = MeshIndex{level, -1};

  MeshSettings settings;
  settings.coarseLevel = level;
  settings.distributeLevel = level;
  settings.fineLevel = level;
  settings.coarseGeometry =
      CreateCoarseGeometryShared("Interval", VertexDataList(settings.vertexData),
                                 CellDataList(settings.cellData));

  TestMeshes meshes(std::move(settings));
  EXPECT_EQ(meshes.Size(), level + 1);
  EXPECT_TRUE(meshes.Contains(lp));
  EXPECT_GT((meshes[lp].ProcSetsCount()), 0);
}

TEST_P(TestMeshesPrintInfo, TestMeshesPrintInfo) { meshes->PrintInfo(); }

int main(int argc, char **argv) {
  return MppTest(MppTestBuilder(argc, argv)
                     .WithConfigEntry("DistributionVerbose", 1)
                     .WithConfigEntry("MeshesVerbose", 4)
                     .WithConfigEntry("MeshVerbose", 2)
                     .WithParallelListeners()
                     .WithScreenLogging()
                     .WithPPM())
      .RUN_ALL_MPP_TESTS();
}

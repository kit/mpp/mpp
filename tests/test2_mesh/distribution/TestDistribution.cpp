#include "MeshesCreator.hpp"
#include "TestEnvironment.hpp"

struct TestParams {
  std::string meshName;
  std::string distName;
  int plevel = 2;
  int level = 3;
};

std::ostream &operator<<(std::ostream &s, const TestParams &testParams) {
  return s << "meshName: " << testParams.meshName << " distName: " << testParams.distName
           << " plevel: " << testParams.plevel << " level: " << testParams.level << endl;
}

class TestDistribution : public TestWithParam<TestParams> {
protected:
  int verbose = 0;

  int maxCommSplit;

  int pLevel;

  int level;

  std::unique_ptr<Meshes> meshesPtr;

  const Meshes &meshes;

  explicit TestDistribution() :
      maxCommSplit(PPM->MaxCommSplit()), level(GetParam().level), pLevel(GetParam().plevel),
      meshesPtr(MeshesCreator(GetParam().meshName)
                    .WithDistribute(GetParam().distName)
                    .WithPLevel(pLevel)
                    .WithLevel(level)
                    .CreateUnique()),
      meshes(*meshesPtr) {
    Config::Get("TestDistributionVerbose", verbose);
  }

  void TearDown() override { PPM->Barrier(0); }
};

INSTANTIATE_TEST_SUITE_P(
    TestDistribution, TestDistribution,
    Values(TestParams{"Interval", "Stripes", 4, 5}, TestParams{"Interval", "Stripes_x", 4, 5},
           TestParams{"Interval", "Stripes_y", 4, 5}, TestParams{"Interval", "Stripes_z", 4, 5},
           TestParams{"Interval", "RCB", 4, 5}, TestParams{"Interval", "RCBx", 4, 5},
           TestParams{"Interval", "RCBy", 4, 5}, TestParams{"Interval", "RCBz", 4, 5},
           TestParams{"Interval", "RCB2D", 4, 5}, TestParams{"Interval", "RCB2Dx", 4, 5},
           TestParams{"Interval", "RCB2Dy", 4, 5}
#if (SpaceDimension >= 2)
           ,
           TestParams{"Triangle", "Stripes"}, TestParams{"Triangle", "Stripes_x"},
           TestParams{"Triangle", "Stripes_y"}, TestParams{"Triangle", "Stripes_z"},
           TestParams{"Triangle", "RCB"}, TestParams{"Triangle", "RCBx"},
           TestParams{"Triangle", "RCBy"}, TestParams{"Triangle", "RCBz"},
           TestParams{"Triangle", "RCB2D"}, TestParams{"Triangle", "RCB2Dx"},
           TestParams{"Triangle", "RCB2Dy"}, TestParams{"Square", "Stripes"},
           TestParams{"Square", "Stripes_x"}, TestParams{"Square", "Stripes_y"},
           TestParams{"Square", "Stripes_z"}, TestParams{"Square", "RCB"},
           TestParams{"Square", "RCBx"}, TestParams{"Square", "RCBy"}, TestParams{"Square", "RCBz"},
           TestParams{"Square", "RCB2D"}, TestParams{"Square", "RCB2Dx"},
           TestParams{"Square", "RCB2Dy"}, TestParams{"Square", "RCBG"},
           TestParams{"Square", "RCBGx"}, TestParams{"Square", "RCBGy"}
//           ,TestParams{"Square", "RCBCommConsistent"}
#endif
#if (SpaceDimension >= 3)
           ,
           TestParams{"Tetrahedron", "Stripes"}, TestParams{"Tetrahedron", "Stripes_x"},
           TestParams{"Tetrahedron", "Stripes_y"}, TestParams{"Tetrahedron", "Stripes_z"},
           TestParams{"Tetrahedron", "RCB"}, TestParams{"Tetrahedron", "RCBx"},
           TestParams{"Tetrahedron", "RCBy"}, TestParams{"Tetrahedron", "RCBz"},
           TestParams{"Tetrahedron", "RCB2D"}, TestParams{"Tetrahedron", "RCB2Dx"},
           TestParams{"Tetrahedron", "RCB2Dy"}, TestParams{"Hexahedron", "Stripes"},
           TestParams{"Hexahedron", "Stripes_x"}, TestParams{"Hexahedron", "Stripes_y"},
           TestParams{"Hexahedron", "Stripes_z"}, TestParams{"Hexahedron", "RCB"},
           TestParams{"Hexahedron", "RCBx"}, TestParams{"Hexahedron", "RCBy"},
           TestParams{"Hexahedron", "RCBz"}, TestParams{"Hexahedron", "RCB2D"},
           TestParams{"Hexahedron", "RCB2Dx"}, TestParams{"Hexahedron", "RCB2Dy"},
           TestParams{"Hexahedron", "RCBG"}, TestParams{"Hexahedron", "RCBGx"},
           TestParams{"Hexahedron", "RCBGy"}, TestParams{"Hexahedron", "RCBGz"}
#endif
#if (defined USE_SPACETIME && SpaceDimension >= 2)
//        , TODO: does this even make sense for Spacetime. Timelevel is always -1!
//        TestParams{"SpaceTimeSquare", "Stripes",3,4},
//        TestParams{"SpaceTimeSquare", "st-opt",3,4},
//        TestParams{"SpaceTimeSquare", "RCB",3,4},
//        TestParams{"SpaceTimeSquare", "deformed",3,4},
//        TestParams{"SpaceTimeSquare", "deformed_optimized",3,4},
//        TestParams{"SpaceTimeSquare", "optimized_doubleslit",3,4},
//        TestParams{"SpaceTimeSquare", "time_stripes",3,4},
//        TestParams{"SpaceTimeSquare", "x_stripes",3,4},
//        TestParams{"SpaceTimeSquare", "y_stripes",3,4}
#endif
           ));

TEST_P(TestDistribution, TestCellCount) {
  for (int commSplit = 0; commSplit <= maxCommSplit; commSplit++) {

    int cellCount = meshes[{pLevel, -1, 0, commSplit}].CellCount();
    int cellCountGeometry = meshes[{pLevel, -1, 0, commSplit}].CellCountGeometry();

    EXPECT_EQ(cellCount, ceil((double)cellCountGeometry / PPM->Size(commSplit)))
        << GetParam() << " Size(commSplit)=" << PPM->Size(commSplit) << " cellCount=" << cellCount
        << " cellCountGeometry=" << cellCountGeometry << endl;

    cellCount = meshes[{level, -1, 0, commSplit}].CellCount();
    cellCountGeometry = meshes[{level, -1, 0, commSplit}].CellCountGeometry();

    EXPECT_EQ(cellCount, cellCountGeometry / PPM->Size(commSplit))
        << GetParam() << " Size(commSplit)=" << PPM->Size(commSplit) << " cellCount=" << cellCount
        << " cellCountGeometry=" << cellCountGeometry << endl;
  }
}

TEST_P(TestDistribution, TestCommunicatorConsistency) {
  const Mesh &meshOnPLevelSplit0 = meshes[{pLevel, -1, 0, 0}];
  const Mesh &meshLevelSplit0 = meshes[{level, -1, 0, 0}];

  for (int s = 1; s <= PPM->MaxCommSplit(); s++) {

    const Mesh &meshOnPLevelSplitS = meshes[{pLevel, -1, 0, s}];
    const Mesh &meshLevelSplitS = meshes[{level, -1, 0, s}];

    mout << "----------------------------------"
         << " s=" << s << " "
         << "----------------------------------" << endl;

    PPM->Barrier(0);

    meshLevelSplit0.PrintInfo();

    // check level
    for (cell c = meshLevelSplit0.cells(); c != meshLevelSplit0.cells_end(); ++c) {
      PPM->Barrier(0);
      bool found = (meshLevelSplitS.find_cell(c()) != meshLevelSplitS.cells_end());
      EXPECT_TRUE(found) << "Proc " << PPM->Proc(0) << " cell " << c() << " not found in s=" << s;
      PPM->Barrier(0);
    }

    // check plevel
    for (cell c = meshOnPLevelSplit0.cells(); c != meshOnPLevelSplit0.cells_end(); ++c) {
      PPM->Barrier(0);
      bool found = (meshOnPLevelSplitS.find_cell(c()) != meshOnPLevelSplitS.cells_end());
      EXPECT_TRUE(found) << "Proc " << PPM->Proc(0) << " cell " << c() << " not found in s=" << s;
      PPM->Barrier(0);
    }
  }
}

int main(int argc, char **argv) {
  return MppTest(MppTestBuilder(argc, argv)
                     .WithConfigEntry("DistributionVerbose", 2)
                     .WithConfigEntry("MeshVerbose", 2)
                     .WithConfigEntry("MakeCommunicatorConsistent", true)
                     .WithParallelListeners()
                     .WithScreenLogging()
                     .WithPPM())
      .RUN_ALL_MPP_TESTS();
}
#include <gtest/gtest.h>
#include "SeismogramData.hpp"
#include "TestEnvironment.hpp"

namespace {

constexpr double Eps = 1e-10;

} // anonymous namespace

struct TestParam {
  double EndTime{};
  double TimeDelta{};
  std::vector<Point> SpaceRec;
  int TimeSlices;
};

class SeismogramTest : public TestWithParam<TestParam> {
protected:
  std::unique_ptr<ObservationSpecification> spec;
  std::unique_ptr<SeismogramParallelizationData> ParaDat;
  std::unique_ptr<SeismogramData> FwdSeis;
public:
  SeismogramTest() {
    auto dat = GetParam();
    spec = ObservationSpecificationBuilder()
               .Witht0(0)
               .WithT(dat.EndTime)
               .Withdt(dat.TimeDelta)
               .WithReceivers(dat.SpaceRec)
               .CreateUnique();
    ParaDat = std::make_unique<SeismogramParallelizationData>(*spec);
    for (int i = 0; i < spec->size(); ++i) {
      if (i % PPM->Size() == PPM->Proc()) { ParaDat->LocalReceivers.emplace_back((*spec)[i]); }
    }
    std::sort(ParaDat->LocalReceivers.begin(), ParaDat->LocalReceivers.end());
    ParaDat->LocalIndexToGlobalIndex = std::vector<int>(ParaDat->LocalReceivers.size());
    for (int i = 0; i < ParaDat->LocalReceivers.size(); ++i) {
      int j = 0;
      for (const auto &re : *spec) {
        if (norm((re - ParaDat->LocalReceivers[i])) < Eps) {
          ParaDat->CoordsToLocalIndex[re] = i;
          ParaDat->LocalIndexToGlobalIndex[i] = j;
        }
        ++j;
      }
    }
    for (int i = 0; i < ParaDat->CoordsToLocalIndex.size(); ++i) {
      ParaDat->LocalIndexToLocalRange.push_back(
          {(i * spec->GetNumberOfTimeSteps()), ((i + 1) * spec->GetNumberOfTimeSteps() - 1)});

      ParaDat->LocalIndexToGlobalRange.push_back(
          {(ParaDat->LocalIndexToGlobalIndex[i] * spec->GetNumberOfTimeSteps()),
           ((ParaDat->LocalIndexToGlobalIndex[i] + 1) * spec->GetNumberOfTimeSteps() - 1)});
      ParaDat->GlobalTimesOwned.push_back({0, spec->GetNumberOfTimeSteps() - 1});
    }
    FwdSeis = std::make_unique<SeismogramData>(*ParaDat);
  }
};

// TODO: Find test for ST.
TEST_P(SeismogramTest, ReadAndWriteTest) {
  auto &SeisData = (*this).FwdSeis->GetDataWrite();
  for (int i = 0; i < SeisData.size(); ++i) {
    SeisData[i] = i;
  }
  (*this).FwdSeis->WriteToFile("SeismogramTmp");
  auto FwdSeis2 = std::make_unique<SeismogramData>(*ParaDat);
  FwdSeis2->ReadFromFile("SeismogramTmp");
  *FwdSeis -= *FwdSeis2;
  EXPECT_NEAR(sqrt(normSqr(SeisData) / normSqr(FwdSeis2->GetData())), 0.0, 1e-50);
}

INSTANTIATE_TEST_SUITE_P(
    IOTest, SeismogramTest,
    Values(TestParam{1.0, 0.5, {{0.0, 0.0}, {0.0, 1.0}, {1.0, 1.0}, {1.0, 0.0}}, 1}));

int main(int argc, char **argv) {
  MppTest mppTest = MppTestBuilder(argc, argv).WithPPM().WithScreenLogging();
  return mppTest.RUN_ALL_MPP_TESTS();
}
#include "ConvergenceStudy.hpp"
#include "PrintUtil.hpp"
#include "STAcousticPDESolver.hpp"
#include "TestEnvironment.hpp"

#include <map>
#include <memory>

#include <string>

struct ConvergenceParamType {
  std::string problem;
  std::map<std::string, std::vector<double>> values;
};

std::ostream &operator<<(std::ostream &s, const ConvergenceParamType &param) {
  return s << "Problem:" << param.problem;
}

template<class PDESolver>
class TestHConvergence : public TestWithParam<ConvergenceParamType> {
protected:
  void SetUp() override {}

  void TearDown() override {}

  void run() {

    auto mainConfig = PDESolverConfig();

    mainConfig.level = 4;
    mainConfig.pLevel = 2;
    mainConfig.degree = 1;
    mainConfig.timeDegree = 1;
    mainConfig.problemName = GetParam().problem;
    mainConfig.preconditioner = "PointBlockGaussSeidel";
    ConvergenceStudy<PDESolver> study(mainConfig);
    auto problem = CreateAcousticProblemShared(GetParam().problem);
    auto solutions = study.Method(problem);
    auto results = getInternalValues(solutions);
    auto convertedResults = convertResults(results);
    for (auto &[name, values] : GetParam().values) {
      PrintValues(name, convertedResults[name], 100, 100);
      EXPECT_VECTOR_NEAR(convertedResults[name], GetParam().values.at(name), 1e-5);
    }
  }
};

using TestHConvergenceWithDG = TestHConvergence<STAcousticPDESolver>;
using TestHConvergenceWithMF = TestHConvergence<STAcousticPDESolverMF>;
using TestHConvergenceWithGL = TestHConvergence<STAcousticPDESolverGL>;

INSTANTIATE_TEST_SUITE_P(
    DG, TestHConvergenceWithDG,
    Values(ConvergenceParamType{.problem = "SinCos",
                                .values = {{"L2_Error", {0.082031, 0.018311, 0.004013}}}},
           ConvergenceParamType{.problem = "QuadraticST",
                                .values = {{"L2_Error", {0.005945, 0.001493, 0.000374}}}},
           ConvergenceParamType{.problem = "RiemannST",
                                .values = {{"L2_Error", {0.033486, 0.026174, 0.020456}}}}));

TEST_P(TestHConvergenceWithDG, runTest) { this->run(); }

INSTANTIATE_TEST_SUITE_P(
    GL, TestHConvergenceWithGL,
    Values(ConvergenceParamType{.problem = "SinCos",
                                .values = {{"L2_Error", {0.275175, 0.094073, 0.025390}}}},
           ConvergenceParamType{.problem = "QuadraticST",
                                .values = {{"L2_Error", {0.049072, 0.012883, 0.003288}}}},
           ConvergenceParamType{.problem = "RiemannST",
                                .values = {{"L2_Error", {0.043172, 0.035367, 0.028361}}}}));

TEST_P(TestHConvergenceWithGL, runTest) { run(); }

INSTANTIATE_TEST_SUITE_P(
    MF, TestHConvergenceWithMF,
    Values(ConvergenceParamType{.problem = "SinCos",
                                .values = {{"L2_Error", {0.082031, 0.018311, 0.004013}}}},
           ConvergenceParamType{.problem = "QuadraticST",
                                .values = {{"L2_Error", {0.005945, 0.001493, 0.000374}}}},
           ConvergenceParamType{.problem = "RiemannST",
                                .values = {{"L2_Error", {0.033486, 0.026174, 0.020456}}}}));

TEST_P(TestHConvergenceWithMF, runTest) { run(); }

int main(int argc, char **argv) {
  return MppTest(MppTestBuilder(argc, argv)
                     .WithConfigEntry("Distribution", "deformed_optimized")
                     .WithConfigEntry("AdaptQuadrature", 0)
                     .WithConfigEntry("DistributionVerbose", 0)
                     .WithConfigEntry("LinearReduction", 1e-6)
                     .WithConfigEntry("LinearEpsilon", 1e-8)
                     .WithConfigEntry("ConfigVerbose", 5)
                     .WithConfigEntry("LinearVerbose", 1)
                     .WithConfigEntry("level", 4)
                     .WithConfigEntry("plevel", 2)
                     .WithConfigEntry("LinearSteps", 200)
                     .WithConfigEntry("MeshesVerbose", 1)
                     .WithConfigEntry("ResultsVerbose", 0)
                     .WithConfigEntry("MeshVerbose", 0)
                     .WithConfigEntry("Overlap", "STCellsWithFaces")
                     .WithConfigEntry("ExcludedResults",
                                      "EE, DGNorm, DGError, DG_Int_Error, DGNorm_Conf, "
                                      "goal_functional, ||u_proj-u_h||_DG, Conf")
                     .WithScreenLogging()
                     .WithPPM())
      .RUN_ALL_MPP_TESTS();
}
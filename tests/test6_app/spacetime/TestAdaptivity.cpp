#include "AdaptiveStudy.hpp"
#include "SingleExperiment.hpp"
#include "TestEnvironment.hpp"

#include <map>

class TestAdaptivity : public Test {
protected:
  TestAdaptivity() = default;

  void SetUp() override {}

  void TearDown() override { Plotting::Instance().Clear(); }

  void run() {
    PDESolverConfig general;
    general.problemName = "AcousticWaveDampingAdaptiveDegree3";
    general.level = 0;
    general.modelName = "STAcoustic";
    general.pLevel = 0;
    std::map<int, PDESolverConfig> degToConfig;
    for (int degree = 1; degree <= 3; degree++) {
      degToConfig[degree] = general;
      degToConfig[degree].degree = degree;
      degToConfig[degree].timeDegree = degree;
    }
    AdaptiveConfig aconf = AdaptiveData{.refinement_steps = 2,
                                        .errorEstimator = "Residual",
                                        .theta = 0.5,
                                        .theta_min = 0,
                                        .refine_by = "my_percentage"};
    auto adaptiveResultsDeg1 = RunAdaptiveExperimentForValues(degToConfig.at(1), aconf);
    auto convertedAdaptiveResults1 = convertResults(adaptiveResultsDeg1);
    auto deg1L2error = convertedAdaptiveResults1["L2_Error"];
    auto uniformDeg2Values = RunSingleExperimentForValues(degToConfig.at(2));
    auto deg2L2error = uniformDeg2Values["L2_Error"];
    auto uniformDeg3Values = RunSingleExperimentForValues(degToConfig.at(3));
    auto deg3L2error = uniformDeg3Values["L2_Error"];

    mout.PrintInfo("Error", 1,
                   PrintInfoEntry{"L2 Error of deg 1 + 1 refinement", deg1L2error.at(1)},
                   PrintInfoEntry{"L2 Error of deg 2", deg2L2error},
                   PrintInfoEntry{"L2 Error of deg 1 + 2 refinement", deg1L2error.at(2)},
                   PrintInfoEntry{"L2 Error of deg 3", deg3L2error});
    EXPECT_NEAR(deg1L2error[1], deg2L2error, 1e-3)
        << "L2 Error of deg 1 + 1 refinement not near L2 Error of deg 2";
    EXPECT_NEAR(deg1L2error[2], deg3L2error, 1e-5)
        << "L2 Error of deg 1 + 2 refinement not near L2 Error of deg 3";
  }
};


#if SpaceDimension == 2
TEST_F(TestAdaptivity, Test2DAdaptivity) { this->run(); }

#endif

int main(int argc, char **argv) {
  return MppTest(MppTestBuilder(argc, argv)
                     .WithScreenLogging()
                     .WithoutDefaultConfig()
                     .WithConfigEntry("Overlap", "STCellsWithFaces")
                     .WithConfigEntry("Distribution", "deformed_optimized")
                     .WithConfigEntry("Verbose", 1)
                     .WithConfigEntry("MainVerbose", 0)
                     .WithConfigEntry("ConfigVerbose", -1)
                     .WithConfigEntry("AssembleVerbose", 1)
                     .WithConfigEntry("VtuPlot", 1)
                     .WithConfigEntry("MeshesVerbose", 3)
                     .WithConfigEntry("LinearVerbose", -1)
                     .WithConfigEntry("LinearPrintSteps", 1000)
                     .WithConfigEntry("MeshVerbose", 10)
                     .WithConfigEntry("ExcludedResults",
                                      "Conf, projError, exact, EE, DiscNorm, DGError, DGNorm, "
                                      "L2SpaceAtT, DGNorm_Conf, goal_functional, DegreeProc, "
                                      "||u_proj-u_h||_DG")
                     .WithConfigEntry("ResultsVerbose", 1)
                     .WithConfigEntry("LinearVerbose", 1)
                     .WithConfigEntry("AdaptivityVerbose", 1)
                     .WithConfigEntry("LinearEpsilon", "1e-20")
                     .WithConfigEntry("LinearReduction", "1e-16")
                     .WithConfigEntry("ResultsVerbose", -1)
                     .WithConfigEntry("doMeasure", 0)
                     .WithPPM())
      .RUN_ALL_MPP_TESTS();
}

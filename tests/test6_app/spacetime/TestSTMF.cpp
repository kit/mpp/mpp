#include <map>
#include <memory>
#include <ranges>

#include <TestableInterface.hpp>
#include <fenv.h>

#include "GMRES.hpp"
#include "STAcousticMain.hpp"
#include "STDGDGViscoAcousticFaceElement.hpp"
#include "STDGMatrixFreeViscoAcousticAssemble.hpp"
#include "SingleRunMethod.hpp"
#include "TestEnvironment.hpp"
/**
 * TestParam:
 * std::string : Model
 * int : degree
 * int : level
 * std::string : Dirichlet, "Dirichlet" or ""
 * std::string : mesh
 * int : numL
 */

using TestParam = std::tuple<int, int, std::string, std::string, int>;

std::ostream &operator<<(std::ostream &os, const TestParam &param) {
  return os << "Degree:    " << std::get<0>(param) << "\n"
            << "Meshlevel: " << std::get<1>(param) << "\n"
            << "Boundary:  " << std::get<2>(param) << "\n"
            << "Mesh:      " << std::get<3>(param) << "\n"
            << "numL:      " << std::get<4>(param) << "\n";
}

std::string CreateProblemName(const std::string &basename, int degree, int numL,
                              const std::string &dirichlet) {
  std::stringstream ss;
  ss << basename;
  ss << SpaceDimension << "D";
  ss << "Degree" << degree;
  if (numL > 0) { ss << "numL" << numL; }
  ss << dirichlet;
  return ss.str();
}

class TestPolynomialSTProblem : public TestWithParam<TestParam> {
protected:
  int degree;
  int level;
  std::string dirichlet;
  std::string mesh;
  int numL;
  std::string problemName;

  TestPolynomialSTProblem() :
      degree(std::get<0>(GetParam())), level(std::get<1>(GetParam())),
      dirichlet(std::get<2>(GetParam())), mesh(std::get<3>(GetParam())),
      numL(std::get<4>(GetParam())),
      problemName(CreateProblemName("Polynomial", degree, numL, dirichlet)) {
    mout << problemName << endl;
    mout << GetParam() << endl;
  }

  void SetUp() override {}

  void TearDown() override { Plotting::Instance().Clear(); }

  void testAllocationInfo(const AllocationInfo &info, const size_t offset, const size_t size,
                          const size_t outputOffset) {
    EXPECT_NE(info.data, nullptr);
    EXPECT_NE(info.perQuadOffset, SIZE_MAX);
    EXPECT_NE(info.perQuadSize, SIZE_MAX);
    EXPECT_NE(info.perQuadOutputOffset, SIZE_MAX);
    EXPECT_EQ(info.perQuadOffset, offset);
    EXPECT_EQ(info.perQuadSize, size);
    EXPECT_EQ(info.perQuadOutputOffset, outputOffset);
  }

  bool testAllocationHelperValid(const AllocationHelper &helper, const size_t damping,
                                 const bool hasOptional = false) {
    EXPECT_NE(helper.spaceDimension, SIZE_MAX);
    EXPECT_LE(helper.spaceDimension, SpaceDimension);
    EXPECT_GE(helper.spaceDimension, 1);
    const auto allocation = std::max(helper.spaceDimension, damping);
    testAllocationInfo(helper.pressure, allocation, 1, helper.spaceDimension);
    testAllocationInfo(helper.velocity, allocation, helper.spaceDimension, 0);
    testAllocationInfo(helper.damping, allocation, damping, helper.spaceDimension + 1);
    EXPECT_EQ(helper.dampingDimension.size(), helper.dimension.size());
    for (size_t i = 0; i < helper.dimension.size(); i++) {
      EXPECT_EQ(helper.dimension.at(i) + 2, helper.dampingDimension.at(i));
    }
    /* Opt */
    if (hasOptional) {
      const auto optAllocation2 = allocation;
      testAllocationInfo(helper.velocityDiv, helper.spaceDimension, helper.spaceDimension, 0);
      testAllocationInfo(helper.velocityDt, optAllocation2, helper.spaceDimension, 0);
      testAllocationInfo(helper.pressureDt, optAllocation2, 1, helper.spaceDimension);
      testAllocationInfo(helper.pressureGrad, helper.spaceDimension, helper.spaceDimension,
                         helper.spaceDimension);
      testAllocationInfo(helper.dampingDt, optAllocation2, damping, helper.spaceDimension + 1);
      testAllocationInfo(helper.dampingGrad, damping * helper.spaceDimension,
                         damping * helper.spaceDimension, helper.spaceDimension + 1);
      return true;
    }
    return false;
  }

  bool testAllocationHolderValid(const AllocationHolder &holder, const size_t damping,
                                 const bool hasOptional = false, const bool testOut = true) {
    EXPECT_NE(holder.damping, SIZE_MAX);
    EXPECT_EQ(holder.damping, damping);
    if (testOut) {
      EXPECT_NE(holder.outputPointer, nullptr);
      EXPECT_NE(holder.rowSize, SIZE_MAX);
    }
    return testAllocationHelperValid(holder.helper, holder.damping, hasOptional);
  }

  auto testCalculationInfo(Matrix &matrix, Vector &RHS, cell iter,
                           const ViscoAcousticProblem &problem, AllocationHelper &helper) {
    auto point = iter->first;
    auto &cell = *iter->second;
    static constexpr auto constOne_func = [](const Point &QP) { return 1.0; };
    auto info = CreateCalculationInfo(matrix, RHS, problem, point, cell, constOne_func, helper);
    EXPECT_EQ(testAllocationHolderValid(info, (size_t)problem.nL(), true), true);
    const auto with = info.helper.generatedWith;
    STDGDGViscoAcousticElement elem(matrix, iter, info.damping);

    STMFViscoAcousticElement element(matrix, iter, problem.nL());
    EXPECT_EQ(with.quadratur, info.quadraturePointWeight.size());
    EXPECT_EQ(element.qWeight.size(), info.quadraturePointWeight.size());
    EXPECT_EQ(element.qPoint.size(), info.quadraturePointWeight.size());
    EXPECT_EQ(element.nQ(), info.quadraturePointWeight.size());
    for (auto &[index, point, weight] : info.quadraturePointWeight) {
      EXPECT_EQ(element.qPoint[index], point);
      EXPECT_EQ(element.qWeight[index], weight);
      size_t pos = 0;
      const auto offset = element.dampingComponentCount + 1 + element.dimension;
      const auto quadOff = offset * with.shape * index;
      const auto dampingOffset = (element.dimension + 1) * with.shape;
      const auto pressureOffset = element.dimension * with.shape;
      EXPECT_EQ(pos, 0);
      for (auto &[next, velocity, dimension] : velocityIterableWithDim(index, info)) {
        EXPECT_LT(dimension, element.dimension) << "Pos[" << pos << "]";
        const auto intoElement = pos + quadOff;
        EXPECT_DOUBLE_EQ(element.velocityST[intoElement][dimension], velocity)
            << "Pos[" << pos << "]"
            << " in Element[" << intoElement << "]"
            << " with Dim[" << (size_t)dimension << "]";
        pos++;
      }
      EXPECT_EQ(pos, element.dimension * element.shape.size());
      pos = 0;
      EXPECT_EQ(pos, 0);
      for (auto &[next, damping, component] : dampingWithDim(index, info)) {
        EXPECT_GE(component - 2, 0) << "Pos[" << pos << "]";
        EXPECT_LT(component - 2, element.dampingComponentCount) << "Pos[" << pos << "]";
        const auto intoElement = pos + quadOff + dampingOffset;
        EXPECT_DOUBLE_EQ(element.pressureST[intoElement], damping)
            << "Pos[" << pos << "]"
            << " in Element[" << intoElement << "]"
            << " with Component[" << (size_t)component << "]";
        pos++;
      }
      EXPECT_EQ(pos, element.dampingComponentCount * element.shape.size());
      pos = 0;
      EXPECT_EQ(pos, 0);
      for (auto &[next, damping] : dampingFrom(index, info)) {
        const auto intoElement = pos + quadOff + dampingOffset;
        EXPECT_DOUBLE_EQ(element.pressureST[intoElement], damping)
            << "Pos[" << pos << "]"
            << " in Element[" << intoElement << "]"
            << " without Component";
        pos++;
      }
      EXPECT_EQ(pos, element.dampingComponentCount * element.shape.size());
      pos = 0;
      EXPECT_EQ(pos, 0);
      for (auto &[next, velocity] : velocityIterable(index, info)) {
        const auto intoElement = pos + quadOff;
        const auto dim = (pos / with.shape) % element.dimension;
        const auto value = element.velocityST[intoElement][dim];
        EXPECT_DOUBLE_EQ(value, velocity) << "Pos[" << pos << "]"
                                          << " in Element[" << intoElement << "]"
                                          << " without Dimension";
        pos++;
      }
      EXPECT_EQ(pos, element.dimension * element.shape.size());
      pos = 0;
      EXPECT_EQ(pos, 0);
      for (auto &[next, pressure] : pressureIterable(index, info)) {
        const auto intoElement = pos + quadOff + pressureOffset;
        EXPECT_DOUBLE_EQ(element.pressureST[intoElement], pressure)
            << "Pos[" << pos << "]"
            << " in Element[" << intoElement << "]";
        pos++;
      }
      EXPECT_EQ(pos, element.shape.size());
      pos = 0;
      EXPECT_EQ(pos, 0);
      for (auto &[next, divVelocity] : divergenceVelocityIterable(index, info)) {
        const auto intoElement = pos + quadOff;
        EXPECT_DOUBLE_EQ(element.divVelocityST[intoElement], divVelocity)
            << "Pos[" << pos << "]"
            << " in Element[" << intoElement << "]";
        pos++;
      }
      EXPECT_EQ(pos, element.shape.size() * element.dimension);
      pos = 0;
      EXPECT_EQ(pos, 0);
      for (auto &[next, gradPressure] : gradientPressureIterable(index, info)) {
        const auto intoElement = pos % with.shape + quadOff + pressureOffset;
        const auto dim = pos / with.shape;
        const auto &value = element.gradPressureST[intoElement];
        EXPECT_DOUBLE_EQ(value[dim], gradPressure) << "Pos[" << pos << "]"
                                                   << " in Element[" << intoElement << "]"
                                                   << " with Dim[" << dim << "]";
        pos++;
      }
      EXPECT_EQ(pos, element.shape.size() * element.dimension);
      pos = 0;
      EXPECT_EQ(pos, 0);
      for (auto &[next, dtVelocity] : derivativeVelocityIterable(index, info)) {
        const auto intoElement = pos + quadOff;
        const auto dim = (pos / with.shape) % element.dimension;
        const auto &value = element.dtVelocityST[intoElement];
        EXPECT_DOUBLE_EQ(value[dim], dtVelocity) << "Pos[" << pos << "]"
                                                 << " in Element[" << intoElement << "]"
                                                 << " with Dim[" << dim << "]";
        pos++;
      }
      EXPECT_EQ(pos, element.shape.size() * element.dimension);
      pos = 0;
      EXPECT_EQ(pos, 0);
      for (auto &[next, dtVelocity] : derivativeVelocityIterable(index, info)) {
        const auto intoElement = pos + quadOff;
        const auto dim = (pos / with.shape) % element.dimension;
        const auto &value = element.dtVelocityST[intoElement];
        EXPECT_DOUBLE_EQ(value[dim], dtVelocity) << "Pos[" << pos << "]"
                                                 << " in Element[" << intoElement << "]"
                                                 << " with Dim[" << dim << "]";
        pos++;
      }
      EXPECT_EQ(pos, element.shape.size() * element.dimension);
      pos = 0;
      EXPECT_EQ(pos, 0);
      for (auto &[next, dtPressure] : derivativePressureIterable(index, info)) {
        const auto intoElement = pos + quadOff + pressureOffset;
        EXPECT_DOUBLE_EQ(element.dtPressureST[intoElement], dtPressure)
            << "Pos[" << pos << "]"
            << " in Element[" << intoElement << "]";
        pos++;
      }
      EXPECT_EQ(pos, element.shape.size());
      pos = 0;
      EXPECT_EQ(pos, 0);
      for (auto &[next, dtDamping] : derivativeDampingIterable(index, info)) {
        const auto intoElement = pos + quadOff + dampingOffset;
        EXPECT_DOUBLE_EQ(element.dtPressureST[intoElement], dtDamping)
            << "Pos[" << pos << "]"
            << " in Element[" << intoElement << "]";
        pos++;
      }
      EXPECT_EQ(pos, element.shape.size() * element.dampingComponentCount);
      pos = 0;
      // Test multiple
      DGRowEntries &rowEntries = info.entries;
      size_t outerPos = 0;
      for (auto &[next, velocity, dim] : velocityIterableWithDim(index, info)) {
        for (size_t i = 0; i < info.damping; i++) {
          pos = 0;
          for (auto &[out, gradientDamping] : gradientDampingIterable(index, info, next, dim, i)) {
            const auto intoElement = pos + quadOff + dampingOffset;
            const auto &value = element.gradPressureST[intoElement];
            EXPECT_DOUBLE_EQ(gradientDamping, value[dim]);
            const auto ptr = &rowEntries(outerPos, pos + i * with.shape + dampingOffset);
            EXPECT_EQ(ptr, out.entries);
            pos++;
          }
          EXPECT_EQ(pos, element.shape.size());
        }
        outerPos++;
      }
      EXPECT_EQ(outerPos, element.shape.size() * element.dimension);
      pos = 0;
      EXPECT_EQ(pos, 0);
      for (auto &[next, velocity, dimension] : velocityIterableWithDim(index, info)) {
        const auto intoElement = pos + quadOff;
        const auto value = element.velocityST[intoElement][dimension];
        EXPECT_DOUBLE_EQ(value, velocity) << "Pos[" << pos << "]"
                                          << " in Element[" << intoElement << "]"
                                          << " with Dim[" << dimension << "]";
        size_t posX = 0;
        for (auto &[out, velocity2] : velocityIterable(index, info, next, dimension)) {
          const auto offset = dimension * element.shape.size();
          const auto intoElement = posX + quadOff + offset;
          const auto value = element.velocityST[intoElement][dimension];
          EXPECT_DOUBLE_EQ(value, velocity2) << "Pos[" << pos << "]"
                                             << " in Element[" << intoElement << "]"
                                             << " with Dim[" << dimension << "]";
          const auto ptr = &rowEntries(pos, posX + dimension * offset);
          EXPECT_EQ(ptr, out.entries);
          posX++;
        }
        EXPECT_EQ(posX, element.shape.size());
        posX = 0;
        for (auto &[out, gradientPressure] :
             gradientPressureIterable(index, info, next, dimension)) {
          const auto intoElement = posX + quadOff + pressureOffset;
          const auto value = element.gradPressureST[intoElement][dimension];
          EXPECT_NEAR(value, gradientPressure, tolerance);
          const auto ptr = &rowEntries(pos, posX + pressureOffset);
          EXPECT_EQ(out.entries, ptr);
          posX++;
        }
        EXPECT_EQ(posX, element.shape.size());
        pos++;
      }
      EXPECT_EQ(pos, element.dimension * element.shape.size());

      pos = 0;
      EXPECT_EQ(pos, 0);
      for (auto &[next, pressure] : pressureIterable(index, info)) {
        const auto offset = element.dimension * element.shape.size();
        const auto intoElement = pos + quadOff + offset;
        const auto value = element.pressureST[intoElement];
        EXPECT_DOUBLE_EQ(value, pressure) << "Pos[" << pos << "]"
                                          << " in Element[" << intoElement << "]";
        size_t posX = 0;
        for (auto &[out, pressure2] : pressureIterable(index, info, next)) {
          const auto intoElement = posX + quadOff + offset;
          const auto value = element.pressureST[intoElement];
          EXPECT_DOUBLE_EQ(value, pressure2) << "Pos[" << pos << "]"
                                             << " in Element[" << intoElement << "]";
          const auto ptr = &rowEntries(pos + offset, posX + offset);
          EXPECT_EQ(ptr, out.entries);
          posX++;
        }
        EXPECT_EQ(posX, element.shape.size());
        posX = 0;
        for (auto [out, derivativePressure] : derivativePressureIterable(index, info, next)) {
          const auto ptr = &rowEntries(pos + offset, posX + offset);
          EXPECT_EQ(ptr, out.entries);
          posX++;
        }
        EXPECT_EQ(posX, element.shape.size());
        posX = 0;
        for (auto [out, divergentVelocity] : divergenceVelocityIterable(index, info, next)) {
          const auto ptr = &rowEntries(pos + offset, posX);
          EXPECT_EQ(ptr, out.entries);
          posX++;
        }
        EXPECT_EQ(posX, element.shape.size() * element.dimension);
        pos++;
      }
      EXPECT_EQ(pos, element.shape.size());

      auto [velocityOut, pressureOut, dampingOut] =
          vectorIterator(RHS, iter->first, info.helper.generatedWith.shape, info.damping,
                         info.helper.spaceDimension);

      const auto vectorPtr = RHS(iter->first);
      pos = 0;
      EXPECT_EQ(pos, 0);
      for (auto &[next, pressure] : pressureIterable(index, info, pressureOut)) {
        const auto offset = element.dimension * element.shape.size();
        EXPECT_EQ(vectorPtr + pos + offset, next.rowIterator);
        pos++;
      }
      EXPECT_EQ(pos, element.shape.size());
    }
    return info;
  }

  void testFluxCache(const size_t index, const FluxCache &cache,
                     std::function<double(const VectorField &)> calculateOld,
                     const SpaceTimeViscoAcousticDGTFaceElement &element) {
    const size_t perElement = element.dim + element.numL + 1;
    const size_t quadOffset = index * perElement * element.shape.size();
    const size_t shape = element.shape.size();
    const size_t spaceShape = element.shape.GetSpaceShape().size();

    size_t pos = 0;

    EXPECT_EQ(cache.helper.dimension.size(), cache.helper.fluxCache.size());
    EXPECT_EQ(cache.helper.dimension.size(), shape * element.dim * element.nQ());
    EXPECT_EQ(cache.helper.flux.perQuadOffset, element.dim);
    EXPECT_EQ(cache.helper.flux.perQuadSize, element.dim);
    EXPECT_EQ(cache.helper.flux.perQuadOutputOffset, 0);
    EXPECT_EQ(cache.helper.generatedWith.quadratur, element.nQ());
    EXPECT_EQ(cache.helper.generatedWith.shape, element.shape.size());

    for (auto &[out, flux, dimension] : fluxIterableWithDim(index, cache)) {
      const size_t indexIn = pos % spaceShape;
      const size_t intoElement = indexIn + quadOffset + dimension * spaceShape
                                 + ((pos % shape) / spaceShape) * perElement * spaceShape;
      const auto &velocity = element.velocityST[intoElement];
      const double fluxValue = calculateOld(velocity);
      EXPECT_DOUBLE_EQ(fluxValue, flux) << "Pos[" << pos << "]"
                                        << " in Element[" << intoElement << "]"
                                        << " with Dim[" << (size_t)dimension << "]";
      pos++;
    }
    EXPECT_EQ(pos, element.shape.size() * element.dim);
    pos = 0;

    for (auto &[out, flux] : fluxIterable(index, cache)) {
      const size_t dimension = pos / shape;
      const size_t indexIn = pos % spaceShape;
      const size_t intoElement = indexIn + quadOffset + dimension * spaceShape
                                 + ((pos % shape) / spaceShape) * perElement * spaceShape;
      const auto &velocity = element.velocityST[intoElement];
      const double fluxValue = calculateOld(velocity);
      EXPECT_DOUBLE_EQ(fluxValue, flux) << "Pos[" << pos << "]"
                                        << " in Element[" << intoElement << "]"
                                        << " with Dim[" << (size_t)dimension << "]";
      pos++;
    }
    EXPECT_EQ(pos, element.shape.size() * element.dim);
  }

  void testFaceSingle(const size_t index, const auto &element, AllocationHolder &holder) {
    const size_t perElement = element.dim + element.numL + 1;
    const size_t quadOffset = index * perElement * element.shape.size();
    const size_t shape = element.shape.size();
    const size_t spaceShape = element.shape.GetSpaceShape().size();
    size_t pos = 0;
    for (auto &[out, velocity, dimension] : velocityIterableWithDim(index, holder)) {
      const size_t indexIn = pos % spaceShape;
      const size_t intoElement = indexIn + quadOffset + dimension * spaceShape
                                 + ((pos % shape) / spaceShape) * perElement * spaceShape;
      const auto &velocityOther = element.velocityST[intoElement];
      EXPECT_DOUBLE_EQ(velocity, velocityOther[dimension])
          << "Pos[" << pos << "]"
          << " in Element[" << intoElement << "]"
          << " with Dim[" << (size_t)dimension << "]";
      pos++;
    }
    EXPECT_EQ(pos, shape * element.dim);
    pos = 0;
    for (auto &[out, velocity] : velocityIterable(index, holder)) {
      const size_t dimension = pos / shape;
      const size_t indexIn = pos % spaceShape;
      const size_t intoElement = indexIn + quadOffset + dimension * spaceShape
                                 + ((pos % shape) / spaceShape) * perElement * spaceShape;
      const auto &velocityOther = element.velocityST[intoElement];
      EXPECT_DOUBLE_EQ(velocity, velocityOther[dimension])
          << "Pos[" << pos << "]"
          << " in Element[" << intoElement << "]"
          << " with Dim[" << (size_t)dimension << "]";
      pos++;
    }
    EXPECT_EQ(pos, shape * element.dim);
    pos = 0;
    for (auto &[out, pressure] : pressureIterable(index, holder)) {
      const size_t dimension = element.dim;
      const size_t indexIn = pos % spaceShape;
      const size_t intoElement = indexIn + quadOffset + dimension * spaceShape
                                 + ((pos % shape) / spaceShape) * perElement * spaceShape;
      const auto &pressureOther = element.pressureST[intoElement];
      EXPECT_DOUBLE_EQ(pressure, pressureOther) << "Pos[" << pos << "]"
                                                << " in Element[" << intoElement << "]"
                                                << " with Dim[" << (size_t)dimension << "]";
      pos++;
    }
    EXPECT_EQ(pos, shape);
    pos = 0;
    for (auto &[out, damping, dimension] : dampingWithDim(index, holder)) {
      const size_t indexIn = pos % spaceShape;
      const size_t intoElement = indexIn + quadOffset + (dimension - 1 + element.dim) * spaceShape
                                 + ((pos % shape) / spaceShape) * perElement * spaceShape;
      const auto &dampingOther = element.pressureST[intoElement];
      EXPECT_DOUBLE_EQ(damping, dampingOther) << "Pos[" << pos << "]"
                                              << " in Element[" << intoElement << "]"
                                              << " with Dim[" << (size_t)dimension << "]";
      pos++;
    }
    EXPECT_EQ(pos, shape * element.numL);
    pos = 0;
    for (auto &[out, damping] : dampingFrom(index, holder)) {
      const size_t dimension = (pos / shape) + element.dim + 1;
      const size_t indexIn = pos % spaceShape;
      const size_t intoElement = indexIn + quadOffset + dimension * spaceShape
                                 + ((pos % shape) / spaceShape) * perElement * spaceShape;
      const auto &dampingOther = element.pressureST[intoElement];
      EXPECT_DOUBLE_EQ(damping, dampingOther) << "Pos[" << pos << "]"
                                              << " in Element[" << intoElement << "]"
                                              << " with Dim[" << (size_t)dimension << "]";
      pos++;
    }
    EXPECT_EQ(pos, shape * element.numL);
  }

  void testFaceSingleIterator(const size_t index, const auto &element, AllocationHolder &holder,
                              auto &iteratorPack) {
    const size_t perElement = element.dim + element.numL + 1;
    const size_t quadOffset = index * perElement * element.shape.size();
    const size_t shape = element.shape.size();
    const size_t spaceShape = element.shape.GetSpaceShape().size();
    size_t pos = 0;
    auto [velocityOut, pressureOut, dampingOut] = iteratorPack;
    for (auto &[out, velocity, dimension] : velocityIterableWithDim(index, holder, velocityOut)) {
      const size_t indexIn = pos % spaceShape;
      const size_t intoElement = indexIn + quadOffset + dimension * spaceShape
                                 + ((pos % shape) / spaceShape) * perElement * spaceShape;
      const auto &velocityOther = element.velocityST[intoElement];
      EXPECT_DOUBLE_EQ(velocity, velocityOther[dimension])
          << "Pos[" << pos << "]"
          << " in Element[" << intoElement << "]"
          << " with Dim[" << (size_t)dimension << "]";
      pos++;
    }
    EXPECT_EQ(pos, shape * element.dim);
    pos = 0;
    for (auto &[out, velocity] : velocityIterable(index, holder, velocityOut)) {
      const size_t dimension = pos / shape;
      const size_t indexIn = pos % spaceShape;
      const size_t intoElement = indexIn + quadOffset + dimension * spaceShape
                                 + ((pos % shape) / spaceShape) * perElement * spaceShape;
      const auto &velocityOther = element.velocityST[intoElement];
      EXPECT_DOUBLE_EQ(velocity, velocityOther[dimension])
          << "Pos[" << pos << "]"
          << " in Element[" << intoElement << "]"
          << " with Dim[" << (size_t)dimension << "]";
      pos++;
    }
    EXPECT_EQ(pos, shape * element.dim);
    pos = 0;
    for (auto &[out, pressure] : pressureIterable(index, holder, pressureOut)) {
      const size_t dimension = element.dim;
      const size_t indexIn = pos % spaceShape;
      const size_t intoElement = indexIn + quadOffset + dimension * spaceShape
                                 + ((pos % shape) / spaceShape) * perElement * spaceShape;
      const auto &pressureOther = element.pressureST[intoElement];
      EXPECT_DOUBLE_EQ(pressure, pressureOther) << "Pos[" << pos << "]"
                                                << " in Element[" << intoElement << "]"
                                                << " with Dim[" << (size_t)dimension << "]";
      pos++;
    }
    EXPECT_EQ(pos, shape);
    pos = 0;
    for (auto &[out, damping, dimension] : dampingWithDim(index, holder, dampingOut)) {
      const size_t indexIn = pos % spaceShape;
      const size_t intoElement = indexIn + quadOffset + (dimension - 1 + element.dim) * spaceShape
                                 + ((pos % shape) / spaceShape) * perElement * spaceShape;
      const auto &dampingOther = element.pressureST[intoElement];
      EXPECT_DOUBLE_EQ(damping, dampingOther) << "Pos[" << pos << "]"
                                              << " in Element[" << intoElement << "]"
                                              << " with Dim[" << (size_t)dimension << "]";
      pos++;
    }
    EXPECT_EQ(pos, shape * element.numL);
    pos = 0;
    for (auto &[out, damping] : dampingFrom(index, holder, dampingOut)) {
      const size_t dimension = (pos / shape) + element.dim + 1;
      const size_t indexIn = pos % spaceShape;
      const size_t intoElement = indexIn + quadOffset + dimension * spaceShape
                                 + ((pos % shape) / spaceShape) * perElement * spaceShape;
      const auto &dampingOther = element.pressureST[intoElement];
      EXPECT_DOUBLE_EQ(damping, dampingOther) << "Pos[" << pos << "]"
                                              << " in Element[" << intoElement << "]"
                                              << " with Dim[" << (size_t)dimension << "]";
      pos++;
    }
    EXPECT_EQ(pos, shape * element.numL);
  }

  void testFaceCalculationInfo(CalculationInfo &info, Matrix &matrix, Vector &RHS, cell iter,
                               const ViscoAcousticProblem &problem) {
    STDiscretization &disc = (STDiscretization &)matrix.GetDisc();
    for (size_t i = 0; i < iter->second->Faces() - 2; i++) {
      FaceCalculationInfo calculations;
      auto [faceInfo, faceCurrentCellData, neighbourAllocHolder, outputTuple, qNormal] =
          CreateFaceCalculationInfos(info, i, calculations);

      bool bnd = matrix.OnBoundary(*iter, i);
      int bnd_id = bnd ? problem.BndID(iter.Face(i)) : -1;
      cell cf = bnd ? iter : matrix.find_neighbour_cell(iter, i);
      EXPECT_EQ(cf(), faceInfo.faceCellIterator());

      DGRowEntries M_cf(matrix, info.cellIterator, *cf, false);
      auto [vel, pres, damp] = outputTuple;
      EXPECT_EQ(vel.beginInternal.rowIterator, M_cf.a);
      EXPECT_EQ(vel.beginInternal.rowSize, M_cf.nf);

      SpaceTimeViscoAcousticDGTFaceElement felem(disc, matrix, iter, i, problem.nL());
      int f1 = matrix.find_neighbour_face_id(iter.Face(i), cf);
      SpaceTimeViscoAcousticDGTFaceElement felem_1(disc, matrix, cf, f1, problem.nL());

      testAllocationHolderValid(faceCurrentCellData, felem.numL);
      testAllocationHolderValid(neighbourAllocHolder, felem.numL, false, false);

      const auto quadratureDictionary =
          generateQuadratureDictionary(info, i, *cf->second, faceCurrentCellData);

      for (auto [index, point, weight] : faceCurrentCellData.quadraturePointWeight) {
        EXPECT_EQ(felem.QPoint(index), point);
        EXPECT_DOUBLE_EQ(felem.QWeight(index), weight);
        VectorField Nq = felem.QNormal(index);
        EXPECT_EQ(Nq, qNormal);

        const auto otherQuadrature = quadratureDictionary.at(index);
        int q1 = find_q_id(felem.QPoint(index), felem_1);
        EXPECT_EQ(q1, otherQuadrature);

        testFluxCache(
            index, calculations.faceCurrentFlux,
            [&](auto &velocity) { return VFlux_c(velocity, Nq); }, felem);

        testFluxCache(
            index, *calculations.usedFlux,
            [&](auto &velocity) {
              return VFlux_cf(velocity, Nq, bnd, bnd_id) * velocityFluxCoefficient(bnd, bnd_id);
            },
            felem_1);

        testFaceSingle(index, felem, faceCurrentCellData);
        testFaceSingle(index, felem_1, neighbourAllocHolder);
        testFaceSingleIterator(index, felem_1, neighbourAllocHolder, outputTuple);
      }
    }
  }

  void testSecondFaceCalculation(CalculationInfo &info, cell iter) {
    const auto &c_prev = info.matrix.GetMesh().FindPreviousCell(info.cellIterator);

    cell prevOld = info.matrix.find_previous_cell(iter);
    EXPECT_EQ((*prevOld->second)(), c_prev());
    SpaceTimeViscoAcousticDGTFaceElement felem(info.discretization, info.matrix, iter,
                                               info.cellIterator.Faces() - 2, info.problem.nL(),
                                               "dg");
    SpaceTimeViscoAcousticDGTFaceElement felem_1(info.discretization, info.matrix, prevOld,
                                                 c_prev.Faces() - 1, info.problem.nL(), "dg", iter);
    AllocationHelper helper;
    auto cacheable = CreateFaceSecondaryCalculate(info, c_prev, helper);

    AllocationHelper helper2;
    auto [holder, iterator] = CreateFaceEdgeCalculation(info, c_prev, helper2);

    for (auto &[index, point, weight] : cacheable.quadraturePointWeight) {
      EXPECT_EQ(felem.QPoint(index), point);
      EXPECT_DOUBLE_EQ(felem.QWeight(index), weight);

      testFaceSingle(index, felem, cacheable);
      testFaceSingle(index, felem_1, holder);
      testFaceSingleIterator(index, felem_1, holder, iterator);
    }
  }

  void testSolvingAndExactSolution() {
    MainConfig mainConf;
    mainConf.mesh = mesh;
    mainConf.level = level;
    mainConf.pLevel = 0;
    mainConf.model = "STAcousticMF";
    mainConf.problem = problemName;
    mainConf.degree = degree;
    mainConf.timeDegree = degree;
    auto stmain = (STAcousticMainMF *)GetSTAcousticMain(mainConf);
    SingleRunMethod main(stmain);

    STDGMatrixFreeViscoAcousticAssemble &mf = *stmain->assemble;

    Vector u(0.0, stmain->GetDisc());
    Vector RHS(0.0, u.GetDisc(), u.Level());
    Matrix matrix(RHS);
    const auto &mesh = matrix.GetMesh();
    const auto &problem = mf.GetProblem();

    for (auto iter = mesh.cells(); iter != mesh.cells_end(); iter++) {
      AllocationHelper helper;
      auto info = testCalculationInfo(matrix, RHS, iter, problem, helper);

      testFaceCalculationInfo(info, matrix, RHS, iter, problem);

      testSecondFaceCalculation(info, iter);
    }

    stmain->assemble->System(matrix, RHS);

    mainConf.model = "STAcoustic";
    auto stmain2 = (STAcousticMain *)GetSTAcousticMain(mainConf);

    Vector u2(0.0, stmain2->GetDisc());
    Vector RHS2(0.0, u2.GetDisc(), u2.Level());
    Matrix matrix2(RHS2);
    stmain2->assemble->System(matrix2, RHS2);
    SparseMatrix sm(matrix);
    SparseMatrix sm2(matrix2);
    const auto &stshape = GetSpaceTimeShape(matrix, *(matrix.GetMesh().cells()->second));
    const size_t spaceShape = stshape.GetSpaceShape().size();
    const size_t perElement = problem.Dim() * spaceShape;
    const size_t shape = stshape.size();
    const size_t dimension = problem.Dim() - problem.nL() - 1;

    for (auto iter = mesh.cells(); iter != mesh.cells_end(); iter++) {
      for (size_t i = 0; i < perElement; i++) {
        const size_t cDimi = i / shape;
        const size_t inSectioni = i % spaceShape;
        const size_t sectioni = (i % shape) / spaceShape;
        const size_t intoElementi = cDimi * spaceShape + sectioni * perElement + inSectioni;
        EXPECT_NEAR(RHS(iter->first, i), RHS2(iter->first, intoElementi), tolerance);
      }
    }
    const auto fullDim = problem.Dim() * shape;

    EXPECT_EQ(sm.rows(), sm2.rows());
    EXPECT_EQ(sm.cols(), sm2.cols());

    EXPECT_NE(sm.ref(0), sm2.ref(0));

    for (size_t i = 0; i < sm.rows(); i++) {
      const size_t inBlock = i % (fullDim);
      const size_t block = i / (fullDim);
      const size_t cDimi = inBlock / shape;
      const size_t inSectioni = inBlock % spaceShape;
      const size_t sectioni = (inBlock % shape) / spaceShape;
      const size_t intoElementi =
          cDimi * spaceShape + sectioni * perElement + inSectioni + block * fullDim;

      for (size_t j = 0; j < sm.cols(); j++) {
        const size_t inBlockJ = j % (fullDim);
        const size_t blockJ = j / (fullDim);
        const size_t cDimj = inBlockJ / shape;
        const size_t inSectionj = inBlockJ % spaceShape;
        const size_t sectionj = (inBlockJ % shape) / spaceShape;
        const size_t intoElementj =
            cDimj * spaceShape + sectionj * perElement + inSectionj + blockJ * fullDim;

        const auto value = sm(i, j);
        const auto old = sm2(intoElementi, intoElementj);
        if (std::abs(value - old) >= tolerance) {
          std::cout << i << " " << j << ": " << value << ", prev: " << old << std::endl;
          continue;
        }
        ASSERT_NEAR(value, old, tolerance) << "Pos[" << i << ", " << j << "]"
                                           << "OldPos[" << intoElementi << ", " << intoElementj
                                           << "]" << DOUT(matrix) << DOUT(matrix2);
      }
    }
  }
};

#if SpaceDimension == 1
INSTANTIATE_TEST_SUITE_P(TestPolynomialSTProblem, TestPolynomialSTProblem,
                         testing::Combine(testing::Range(1, 6),            // Degree
                                          Values(3),                       // Mesh Level
                                          Values("", "Dirichlet"),         // Boundary Type
                                          Values("SpaceTimeUnitInterval"), // Mesh
                                          Values(0)                        // NumL
                                          ));
#elif SpaceDimension == 2

INSTANTIATE_TEST_SUITE_P(TestPolynomialSTProblemWithDampingOnSquaresMinimal,
                         TestPolynomialSTProblem,
                         testing::Combine(testing::Range(0, 3), // Degree
                                          testing::Range(1, 3), // Mesh Level
                                          Values("",
                                                 "Dirichlet"),       // Boundary Type
                                          Values("SpaceTimeSquare"), // Mesh
                                          Values(0, 1, 2, 3)         // NumL

                                          ));

INSTANTIATE_TEST_SUITE_P(TestPolynomialSTProblemWithDampingOnSquares, TestPolynomialSTProblem,
                         testing::Combine(testing::Range(0, 3), // Degree
                                          testing::Range(1, 3), // Mesh Level
                                          Values("",
                                                 "Dirichlet"),       // Boundary Type
                                          Values("SpaceTimeSquare"), // Mesh
                                          Values(0, 1, 2, 3)         // NumL
                                          ));

INSTANTIATE_TEST_SUITE_P(TestPolynomialSTProblemWithDampingOnTriangles, TestPolynomialSTProblem,
                         testing::Combine(testing::Range(0, 3),               // Degree
                                          testing::Range(1, 3),               // Mesh Level
                                          Values("", "Dirichlet"),            // Boundary Type
                                          Values("SpaceTimeSquareTriangles"), // Mesh
                                          Values(0, 1, 2, 3)                  // NumL

                                          ));
#elif SpaceDimension == 3
INSTANTIATE_TEST_SUITE_P(TestPolynomialSTProblem, TestPolynomialSTProblem,
                         testing::Combine(testing::Range(0, 3), // Degree
                                          Values(0),            // Mesh Level
                                          Values("",
                                                 "Dirichlet"),         // Boundary Type
                                          Values("SpaceTimeUnitCube"), // Mesh
                                          Values(0, 1, 2, 3)           // NumL
                                          ));
#endif

TEST_P(TestPolynomialSTProblem, TestExactSolution) { this->testSolvingAndExactSolution(); }

int main(int argc, char **argv) {
  feenableexcept(FE_INVALID | FE_OVERFLOW);
  return MppTest(MppTestBuilder(argc, argv)
                     .WithScreenLogging()
                     .WithoutDefaultConfig()
                     .WithConfigEntry("Overlap", "STCellsWithCorners")
                     .WithConfigEntry("Distribution", "deformed_optimized")
                     .WithConfigEntry("Verbose", -1)
                     .WithConfigEntry("MainVerbose", 0)
                     .WithConfigEntry("ConfigVerbose", -1)
                     .WithConfigEntry("AssembleVerbose", 1)
                     .WithConfigEntry("MeshesVerbose", -1)
                     .WithConfigEntry("LinearVerbose", -1)
                     .WithConfigEntry("LinearPrintSteps", 1000)
                     .WithConfigEntry("MeshVerbose", -1)
                     .WithConfigEntry("ExcludedResults",
                                      "Conf, projError, exact, EE, DiscNorm, "
                                      "DGError, DGNorm, L2SpaceAtT, DGNorm_Conf, "
                                      "goal_functional, ||u_proj-u_h||_DG")
                     .WithConfigEntry("ResultsVerbose", 1)
                     .WithConfigEntry("LinearVerbose", 1)
                     .WithConfigEntry("LinearEpsilon", "1e-20")
                     .WithConfigEntry("LinearReduction", "1e-16")
                     .WithConfigEntry("ResultsVerbose", -1)
                     .WithPPM())
      .RUN_ALL_MPP_TESTS();
}

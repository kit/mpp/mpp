#include "AcousticPDESolver.hpp"
#include "TestEnvironment.hpp"

inline constexpr double TEST_TOLERANCE = 1e-6;

/**
 * @brief Verify that the solution did converge and contains the expected values.
 *        The solution may contain additional values not contained in the reference.
 *
 * @param solution The solution returned by the pdesolver
 * @param reference The expected solution values
 */
void verifySolution(const Solution solution, const ValueMap reference) {
  EXPECT_TRUE(solution.converged);

  for (const auto &[key, referenceValue] : reference) {
    const auto solutionValue = solution.values.find(key);
    ASSERT_NE(solutionValue, solution.values.end()) << "Solution does not contain reference value: " << key;

    EXPECT_NEAR(referenceValue, solutionValue->second, TEST_TOLERANCE);
  }
}

/**
 * @brief Test suite parameterized on the CommSplit
 */
class TestAcousticPDESolverNew: public ::testing::TestWithParam<int> {
protected:
  int commSplit = 0;

  TestAcousticPDESolverNew() {
    const ConfigMap configMap{{"TimeIntegratorVerbose", "1"}, {"PDESolverPlotting", "1"},
                              {"PDESolverVerbose", "2"},      {"AssembleVerbose", "1"},
                              {"MeshesVerbose", "1"},         {"NewtonVerbose", "1"},
                              {"LinearVerbose", "0"},         {"ConfigVerbose", "1"},
                              {"MeshVerbose", "2"},           {"MainVerbose", "0"},
                              {"Model", "DGAcoustic"}};

    Config::Initialize(configMap);
  }

  void TearDown() override {
    PPM->Barrier(0);
    Plotting::Instance().Clear();
    Config::Close();
  }
};

TEST_P(TestAcousticPDESolverNew, TestLinearProblem) {
    const auto rkorder = 2;
    const auto endTime = 0.0;
    const auto degree = 1;
    const auto commSplit = GetParam();
    const auto level = 4;

    const auto config = PDESolverConfig().WithRkOrder(2).WithDegree(degree);
    const auto problem = AcousticPDEProblemBuilder::Linear().WithEndTime(endTime).BuildShared();
    auto solver = AcousticPDESolver(config);
    const auto solution = solver.Run(problem, MeshIndex{level, -1, 0, commSplit});

    const auto expectedValues = ValueMap{
        { "Energy", 1.0 },
        { "L1", 1.1248088 },
        { "L2", 1.0 },
        { "MaxP", 0.0 },
        { "MinP", -0.96875 },
        { "NormCoefficients", 32.031234756 },
        { "L2Error", 0. }
    };
    verifySolution(solution, expectedValues);
}

TEST_P(TestAcousticPDESolverNew, TestQuadraticProblem) {
    const auto rkorder = 2;
    const auto endTime = 0.0;
    const auto degree = 2;
    const auto commSplit = GetParam();
    const auto level = 4;

    const auto config = PDESolverConfig().WithRkOrder(2).WithDegree(degree);
    const auto problem = AcousticPDEProblemBuilder::Quadratic().WithEndTime(endTime).BuildShared();
    auto solver = AcousticPDESolver(config);
    const auto solution = solver.Run(problem, MeshIndex{level, -1, 0, commSplit});

    const auto expectedValues = ValueMap{
        { "Energy", 0.5577733 },
        { "L1", 0.763762 },
        { "L2", 0.5577733 },
        { "MaxP", 0.93847656 },
        { "MinP", 0.0 },
        { "NormCoefficients", 26.810442083014955 },
        { "L2Error", 0. }
    };
    verifySolution(solution, expectedValues);
}

TEST_P(TestAcousticPDESolverNew, TestCrcProblem) {
    const auto rkorder = 2;
    const auto endTime = 0.0;
    const auto degree = 1;
    const auto commSplit = GetParam();
    const auto level = 2;

    const auto config = PDESolverConfig().WithRkOrder(2).WithDegree(degree);
    const auto problem = AcousticPDEProblemBuilder::CRC().WithEndTime(endTime).BuildShared();
    auto solver = AcousticPDESolver(config);
    const auto solution = solver.Run(problem, MeshIndex{level, -1, 0, commSplit});

    const auto expectedValues = ValueMap{
        { "Energy", 1.1756163 },
        { "L1", 0.0 },
        { "L2", 0.0 },
        { "MaxP", 0.0 },
        { "MinP", 0.0 },
        { "NormCoefficients", 29.079148 }
    };
    verifySolution(solution, expectedValues);
}

TEST_P(TestAcousticPDESolverNew, TestRiemannWave2DProblem) {
    const auto rkorder = 2;
    const auto endTime = 0.0;
    const auto degree = 2;
    const auto commSplit = GetParam();
    const auto level = 4;

    const auto config = PDESolverConfig().WithRkOrder(2).WithDegree(degree);
    const auto problem = AcousticPDEProblemBuilder::RiemannWave2D().WithEndTime(endTime).BuildShared();
    auto solver = AcousticPDESolver(config);
    const auto solution = solver.Run(problem, MeshIndex{level, -1, 0, commSplit});

    const auto expectedValues = ValueMap{
        { "Energy", 1.4100027 },
        { "L1", 1.410340 },
        { "L2", 1.4100027 },
        { "MaxP", 1.0 },
        { "MinP", -1.0 },
        { "NormCoefficients", 67.882250 },
        { "L2Error", 0.1204999 }
    };
    verifySolution(solution, expectedValues);
}

TEST_P(TestAcousticPDESolverNew, TestGaussHatAndRicker2DProblem) {
    const auto rkorder = 2;
    const auto endTime = 0.0;
    const auto degree = 2;
    const auto commSplit = GetParam();
    const auto level = 4;

    const auto config = PDESolverConfig().WithRkOrder(2).WithDegree(degree);
    const auto problem = AcousticPDEProblemBuilder::GaussHatAndRicker2D().WithEndTime(endTime).BuildShared();
    auto solver = AcousticPDESolver(config);
    const auto solution = solver.Run(problem, MeshIndex{level, -1, 0, commSplit});

    const auto expectedValues = ValueMap{
        { "Energy", 0.0 },
        { "L1", 0.0 },
        { "L2", 0.0 },
        { "MaxP", 0.0 },
        { "MinP", 0.0 },
        { "NormCoefficients", 0.0 }
    };
    verifySolution(solution, expectedValues);
}

TEST_P(TestAcousticPDESolverNew, TestAcousticWaveProcTest) {
    const auto rkorder = 2;
    const auto endTime = 0.0;
    const auto degree = 2;
    const auto commSplit = GetParam();
    const auto level = 4;

    const auto config = PDESolverConfig().WithRkOrder(2).WithDegree(degree);
    const auto problem = AcousticPDEProblemBuilder::AcousticWaveProcTest().WithEndTime(endTime).BuildShared();
    auto solver = AcousticPDESolver(config);
    const auto solution = solver.Run(problem, MeshIndex{level, -1, 0, commSplit});

    const auto expectedValues = ValueMap{
        { "Energy", 0.0 },
        { "L1", 0.0 },
        { "L2", 0.0 },
        { "MaxP", 0.0 },
        { "MinP", 0.0 },
        { "NormCoefficients", 0.0 }
    };
    verifySolution(solution, expectedValues);
}

INSTANTIATE_TEST_SUITE_P(AcousticPDESolverTests, TestAcousticPDESolverNew, ::testing::Values(0, 1));

int main(int argc, char **argv) {
  return MppTest(MppTestBuilder(argc, argv)
                     .WithoutDefaultConfig()
                     .WithConfPath(std::string(ProjectMppDir) + "/conf/")
                     .WithGeoPath(std::string(ProjectMppDir) + "/conf/geo/")
                     .WithScreenLogging()
                     .WithFileLogging()
                     .WithPPM())
      .RUN_ALL_MPP_TESTS();
}
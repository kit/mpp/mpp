#ifndef TESTTRANSPORTMAIN_HPP
#define TESTTRANSPORTMAIN_HPP

#include <utility>

#include <PDESolver.hpp>
#include <TransportPDESolver.hpp>

#include "TestEnvironment.hpp"

class TestTransportPDESolver : public TestWithParam<std::pair<ConfigMap, ValueMap>> {
protected:
  std::unique_ptr<TransportPDESolver> pdeSolver;

  double TEST_TOLERANCE = 1e-6;

  ConfigMap configMap{{"PDESolverPlotting", "1"}, {"PDESolverVerbose", "2"},
                      {"AssembleVerbose", "1"},   {"MeshesVerbose", "1"},
                      {"NewtonVerbose", "1"},     {"LinearVerbose", "0"},
                      {"ConfigVerbose", "1"},     {"MeshVerbose", "2"},
                      {"MainVerbose", "0"}};

  ValueMap refValues{};

  PDESolverConfig conf;

  int commSplit;

  TestTransportPDESolver(ConfigMap additionalMap1, ValueMap valueMap1 = {}, int commSplit = 0) :
      commSplit(commSplit) {
    additionalMap1.merge(configMap);
    ConfigMap additionalMap2 = GetParam().first;
    additionalMap2.merge(additionalMap1);
    Config::Initialize(additionalMap2);
    if (valueMap1.empty()) refValues = GetParam().second;
    else refValues = valueMap1;
    conf = PDESolverConfig();
    pdeSolver = std::make_unique<TransportPDESolver>(PDESolverConfig());
  }

  void TestRun() {
    auto problem = CreateTransportProblemShared(PDESolverConfig().problemName);
    auto solution = pdeSolver->Run(problem, MeshIndex{conf.level, -1, 0, commSplit});

    EXPECT_TRUE(solution.converged);
    solution.PrintInfo();
    for (const auto &[key, referenceValue] : refValues) {
      const auto solutionValue = solution.values.find(key);
      ASSERT_NE(solutionValue, solution.values.end())
          << "Solution does not contain reference value: " << key;

      EXPECT_NEAR(referenceValue, solutionValue->second, TEST_TOLERANCE);
    }
  }

  void TearDown() override {
    PPM->Barrier(0);
    Plotting::Instance().Clear();
    Config::Close();
  }
};

class TestProblems : public TestTransportPDESolver {
public:
  TestProblems() :
      TestTransportPDESolver({ConfigMap{{"Model", "DGTransport"}, {"rkorder", "-2"}}}) {}
};

class TestProblemsCommSplit : public TestTransportPDESolver {
public:
  TestProblemsCommSplit() :
      TestTransportPDESolver({ConfigMap{{"Model", "DGTransport"}, {"rkorder", "-2"}}}, {}, 1) {}
};

#endif // TESTTRANSPORTMAIN_HPP

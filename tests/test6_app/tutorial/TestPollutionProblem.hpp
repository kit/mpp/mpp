#ifndef TESTREACTIONMAIN_HPP
#define TESTREACTIONMAIN_HPP

#include "TestEnvironment.hpp"

#include "STTransportPDESolver.hpp"
#include "TransportPDESolver.hpp"

#include "SingleExperiment.hpp"

template<typename PDESolverType>
class TestPollutionProblem : public TestWithParam<std::pair<ConfigMap, ValueMap>> {
protected:
  double TEST_TOLERANCE = 1e-5;
  ConfigMap configMap{{"PDESolverPlotting", "1"}, {"PDESolverVerbose", "2"},
                      {"AssembleVerbose", "1"},   {"MeshesVerbose", "1"},
                      {"NewtonVerbose", "1"},     {"LinearVerbose", "0"},
                      {"ConfigVerbose", "1"},     {"MeshVerbose", "2"},
                      {"MainVerbose", "0"}};
  ValueMap refValues{};

  // Todo constructor can be cleaned up. AddtionalMap not needed
  TestPollutionProblem() {
    ConfigMap confMap = GetParam().first;
    confMap.merge(configMap);
    Config::Initialize(confMap);
    refValues = GetParam().second;
  }

  void TearDown() override {
    PPM->Barrier(0);
    Plotting::Instance().Clear();
    Config::Close();
  }
};

using TestSpaceTimePollutionProblem = TestPollutionProblem<STTransportPDESolver>;
using TestTimeSteppingPollutionProblem = TestPollutionProblem<TransportPDESolver>;

#endif // TESTREACTIONMAIN_HPP

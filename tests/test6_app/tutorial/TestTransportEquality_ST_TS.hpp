#ifndef MPP_TESTTRANSPORTEQUALITY_ST_TS_HPP
#define MPP_TESTTRANSPORTEQUALITY_ST_TS_HPP

#include "STTransportPDESolver.hpp"
#include "TestEnvironment.hpp"
#include "TransportPDESolver.hpp"

class TestTransportEquality_ST_TS : public TestWithParam<std::pair<ConfigMap, ValueMap>> {
  const double REFERENCE_MASS = 0.2155919236;

  const double TEST_TOLERANCE = 1e-10;

  PDESolverConfig conf{};

  ValueMap refValues{};
public:

  TestTransportEquality_ST_TS() {
    conf.degree = 2;   // Todo If they are constant, why not set them as ConfigEntry in TestBuilder
    conf.rkorder = -1; // Todo ...
    conf.timeDegree = 0;            // Todo ...
    conf.modelName = "DGTransport"; // Todo needed?
    Plotting::Instance().Clear();
  }

  void Run() {
    // Create and solve Mixed Problem
    const auto mixedProblem = CreateEllipticProblemShared("LaplaceSquare500");
    const auto mixedSolution = MixedPDESolver().Run(mixedProblem);

    // Create and solve the transport problem with time stepping
    const auto tsProblem =
        TransportPDEProblemBuilder::PollutionSquare500(mixedSolution, mixedProblem->IntoMeshes())
            .BuildShared();
    const auto solutionTS = TransportPDESolver(conf).Run(tsProblem);
    const double last_mass_ts = solutionTS.values.at("Mass");
    mout << "TS mass: " << last_mass_ts << endl;
    EXPECT_NEAR(last_mass_ts, REFERENCE_MASS, TEST_TOLERANCE) << "Mass not equal.";

    Plotting::Instance().Clear();

    // Create and solve the transport problem with space-time
    conf.modelName = "STTransport";
    const auto &dist = mixedProblem->GetMeshes().Settings().distributions;
    const auto stProblem =
        TransportPDEProblemBuilder::PollutionSquare500ST(mixedSolution, dist).BuildShared();
    auto solutionST = STTransportPDESolver(conf).Run(stProblem);
    const std::vector<double> massST = solutionST.timeDependentValues.at("Mass");
    const double last_mass_st = massST.back();
    mout << "ST mass: " << last_mass_st << endl;
    EXPECT_NEAR(last_mass_st, REFERENCE_MASS, TEST_TOLERANCE) << "Mass not equal.";
  }
};

TEST_P(TestTransportEquality_ST_TS, TestEquality) { Run(); }

INSTANTIATE_TEST_SUITE_P(TestTransportEquality, TestTransportEquality_ST_TS,
                         Values(std::pair{ConfigMap{}, ValueMap{{"Mass", 1.0},
                                                                {"Energy", 3.9999258206882993}}}));


#endif

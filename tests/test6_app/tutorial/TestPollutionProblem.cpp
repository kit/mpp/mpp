#include "TestPollutionProblem.hpp"


INSTANTIATE_TEST_SUITE_P(TestPollutionProblem, TestSpaceTimePollutionProblem,
                         Values(std::pair{ConfigMap{{"Problem", "PollutionSquare500ST"},
                                                    {"level", "1"},
                                                    {"plevel", "0"},
                                                    {"degree", "1"},
                                                    {"degree_time", "0"},
                                                    {"T", "1.0"},
                                                    {"dt", "0.1"},
                                                    {"ClearDistribution", "0"},
                                                    {"Distribution", "RCB"},
                                                    {"ConfigVerbose", "100"},
                                                    {"LinearVerbose", "1"},
                                                    {"PDESolverVerbose", "1"}
                                                    },
                                          ValueMap{{"L2_Norm", 2.25618}}}));

INSTANTIATE_TEST_SUITE_P(TestPollutionProblem, TestTimeSteppingPollutionProblem,
                         Values(std::pair{ConfigMap{{"Problem", "PollutionSquare500"},
                                                    {"level", "1"},
                                                    {"plevel", "0"},
                                                    {"degree", "1"},
                                                    {"Model", "DGTransport"},
                                                    {"T", "1.0"},
                                                    {"dt", "0.05"},
                                                    {"ClearDistribution", "0"},
                                                    {"Distribution", "RCB"},
                                                    {"rkorder", "-1"}},
                                          ValueMap{{}}}));

TEST_P(TestSpaceTimePollutionProblem, TestRun) {
  const auto mixedProblem = CreateEllipticProblemShared("LaplaceSquare500");
  const auto mixedSolution = MixedPDESolver().Run(mixedProblem);
  const auto &distributions = mixedProblem->GetMeshes().Settings().distributions;
  const auto transportProblem =
      TransportPDEProblemBuilder::PollutionSquare500ST(mixedSolution, distributions).BuildShared();
  auto transportSolution = STTransportPDESolver().Run(transportProblem);
  transportSolution.PrintInfo();

  for (auto &refValue : refValues) {
    EXPECT_NEAR(refValue.second, transportSolution.values[refValue.first], TEST_TOLERANCE);
  }
  EXPECT_NO_THROW(transportSolution.vector.GetMatrixGraph().GetProcSets().CheckConsistency());
  EXPECT_NO_THROW(transportSolution.vector.GetMesh().GetProcSets().CheckConsistency());
}

TEST_P(TestTimeSteppingPollutionProblem, TestRun) {
  const auto mixedProblem = CreateEllipticProblemShared("LaplaceSquare500");
  const auto mixedSolution = MixedPDESolver().Run(mixedProblem);
  const auto transportProblem =
      TransportPDEProblemBuilder::PollutionSquare500(mixedSolution, mixedProblem->IntoMeshes())
          .BuildShared();
  auto transportSolution = TransportPDESolver().Run(transportProblem);
  transportSolution.PrintInfo();

  for (auto &refValue : refValues) {
    EXPECT_NEAR(refValue.second, transportSolution.values[refValue.first], TEST_TOLERANCE);
  }
  EXPECT_NO_THROW(transportSolution.vector.GetMatrixGraph().GetProcSets().CheckConsistency());
  EXPECT_NO_THROW(transportSolution.vector.GetMesh().GetProcSets().CheckConsistency());
}

int main(int argc, char **argv) {
  return MppTest(MppTestBuilder(argc, argv)
                     .WithConfPath(std::string(ProjectMppDir) + "/conf/")
                     .WithGeoPath(std::string(ProjectMppDir) + "/conf/geo/")
                     .WithoutDefaultConfig()
                     .WithScreenLogging()
                     .WithFileLogging()
                     .WithPPM())
      .RUN_ALL_MPP_TESTS();
}

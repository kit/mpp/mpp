#include "Cell.hpp"
#include "DGDiscretization.hpp"
#include "ExchangeBuffer.hpp"
#include "IDiscretization.hpp"
#include "IDoF.hpp"
#include "IMatrixGraph.hpp"
#include "LagrangeDoF.hpp"
#include "MatrixGraph.hpp"
#include "Mesh.hpp"
#include "MeshIndex.hpp"
#include "Meshes.hpp"
#include "MeshesCreator.hpp"
#include "MixedEGDiscretization.hpp"
#include "Point.hpp"
#include "ProcSet.hpp"
#include "RTDoF.hpp"
#include "TestEnvironment.hpp"

#include <algorithm>
#include <iterator>
#include <memory>
#include <numeric>
#include <ostream>
#include <string>
#include <tuple>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>

#include <gtest/gtest.h>

struct TestParam {
  std::string distributeName;
  std::string dofName;
  int degree;
  int numberOfDofsAtNodalPoint;

  friend void PrintTo(const TestParam &param, std::ostream *os) {
    *os << "DistName: " << param.distributeName << " dofName: " << param.dofName
        << " degree: " << param.degree << " numberOfDofsAtNodalPoint"
        << param.numberOfDofsAtNodalPoint;
  }
};

class MatrixGraphTest : public TestWithParam<TestParam> {
protected:
  int pLevel = 2;
  int level = 2;
  std::unique_ptr<Meshes> meshes;
  const Mesh &mesh;
  std::unique_ptr<MatrixGraph> graph;

  int numberOfRows = -1;
  int numberOfDofsAtNodalPoint = -1;
  int vectorSize = -1;
  int matrixSize = -1;

  MatrixGraphTest(std::string meshName) :
      meshes(MeshesCreator(meshName)
                 .WithDistribute(GetParam().distributeName)
                 .WithPLevel(pLevel)
                 .WithLevel(level)
                 .CreateUnique()),
      mesh(meshes->fine()) {
    computeNumberOfRows(meshName);
    numberOfDofsAtNodalPoint = GetParam().numberOfDofsAtNodalPoint;
    vectorSize = numberOfRows * numberOfDofsAtNodalPoint;
    std::unique_ptr<IDoF> _dof = nullptr;
    if (GetParam().dofName == "LagrangeDoF") {
      _dof = std::make_unique<LagrangeDoF>(GetParam().degree, GetParam().numberOfDofsAtNodalPoint);
    } else if (GetParam().dofName == "RTDoF") {
      _dof = std::make_unique<RTDoF>(GetParam().degree, GetParam().numberOfDofsAtNodalPoint);
    } else {
      Exit("DoF not implemented in MatrixGraphTest")
    }
    graph = std::unique_ptr<MatrixGraph>(new MatrixGraph(mesh, std::move(_dof)));
    computeMatrixSize();
  }
private:
  bool entryExists(const std::vector<Point> &v, const Point &p) const {
    return std::find(v.begin(), v.end(), p) != v.end();
  }

  void computeNumberOfRows(const std::string &meshName) {
    if (GetParam().dofName == "LagrangeDoF") {
      switch (GetParam().degree) {
      case 0:
        numberOfRows = mesh.CellCount();
        break;
      case 1:
        numberOfRows = mesh.VertexCount();
        break;
      case 2:
        numberOfRows = 0;
        switch (mesh.dim()) {
        case 1:
          numberOfRows = mesh.EdgeCount() + mesh.CellCount();
          break;
        case 3:
        case 2:
          if (meshName == "Hexahedron") numberOfRows += mesh.CellCount() + mesh.FaceCount();
          if (meshName == "Square") numberOfRows += mesh.CellCount();
          numberOfRows += mesh.VertexCount() + mesh.EdgeCount();
          break;
        default:
          Exit("Celltype not implemented")
        }
        break;
      default:
        Exit("Degree not implemented")
      }
    } else if (GetParam().dofName == "RTDoF") {
      if (GetParam().degree != 0) Exit("Degree not implemented") numberOfRows = mesh.FaceCount();
    } else {
      Exit("DoF not implemented in MatrixGraphTest")
    }
  }

  void computeMatrixSize() {
    matrixSize = 0;
    addCenters();
    addVertices();
    if (mesh.dim() > 1) addFaces();
    if (mesh.dim() > 2) addEdges();
    matrixSize *= numberOfDofsAtNodalPoint * numberOfDofsAtNodalPoint;
  }

  void addCenters() {
    for (cell cll = mesh.cells(); cll != mesh.cells_end(); ++cll) {
      std::vector<Point> nodalPointsOnCell = graph->GetDoF().GetNodalPoints(*cll);
      if (entryExists(nodalPointsOnCell, cll->first)) matrixSize += nodalPointsOnCell.size();
    }
  }

  bool addNodalPoints(std::vector<Point> &nodalPoints, const cell &cll,
                      const Point &consideredPoint) {
    std::vector<Point> nP = graph->GetDoF().GetNodalPoints(*cll);
    if (!entryExists(nP, consideredPoint)) // consideredPoint itself is no nodal point
      return false;
    for (int k = 0; k < nP.size(); ++k) {
      if (!entryExists(nodalPoints, nP[k])) nodalPoints.push_back(nP[k]);
    }
    return true;
  }

  void addVertices() {
    for (auto vtx = graph->vertices(); vtx != graph->vertices_end(); ++vtx) {
      // find all nodal points of cells containing vtx
      std::vector<Point> nodalPoints{};
      for (cell cll = mesh.cells(); cll != mesh.cells_end(); ++cll) {
        for (int i = 0; i < cll->second->Corners(); ++i) {
          if (vtx->first == cll->second->Corner(i)) {
            if (!addNodalPoints(nodalPoints, cll, vtx->first))
              return; // vtx itself is no nodal point
            break;
          }
        }
      }
      matrixSize += nodalPoints.size();
    }
  }

  void addFaces() {
    for (auto fce = graph->faces(); fce != graph->faces_end(); ++fce) {
      // find all nodal points of cells containing fce
      std::vector<Point> nodalPoints{};
      if (fce->second.Left() != Infty) {
        cell c = graph->find_cell(fce->second.Left());
        if (c != mesh.cells_end())
          if (!addNodalPoints(nodalPoints, c, fce->first)) return;
      }
      if (fce->second.Right() != Infty) {
        cell c = graph->find_cell(fce->second.Right());
        if (c != mesh.cells_end())
          if (!addNodalPoints(nodalPoints, c, fce->first)) return;
      }
      matrixSize += nodalPoints.size();
    }
  }

  void addEdges() {
    for (auto edg = graph->edges(); edg != graph->edges_end(); ++edg) {
      // find all nodal points of cells containing edg
      std::vector<Point> nodalPoints{};
      for (cell cll = mesh.cells(); cll != mesh.cells_end(); ++cll) {
        for (int i = 0; i < cll->second->Edges(); ++i) {
          if (edg->first == cll->second->Edge(i)) {
            if (!addNodalPoints(nodalPoints, cll, edg->first))
              return; // edg itself is no nodal point
            break;
          }
        }
      }
      matrixSize += nodalPoints.size();
    }
  }
};

#define MATRIXGRAPH_TESTS_LAGRANGE(MeshName, DistributeName)                                       \
  class MatrixGraph##DistributeName##MeshName##TestLagrange : public MatrixGraphTest {             \
  public:                                                                                          \
    MatrixGraph##DistributeName##MeshName##TestLagrange() : MatrixGraphTest(#MeshName) {}          \
  };                                                                                               \
                                                                                                   \
  TEST_P(MatrixGraph##DistributeName##MeshName##TestLagrange, NumberOfRowsTest) {                  \
    EXPECT_EQ(graph->rowsize(), numberOfRows);                                                     \
  }                                                                                                \
  TEST_P(MatrixGraph##DistributeName##MeshName##TestLagrange, NumberOfNodalPointsTest) {           \
    for (int i = 0; i < numberOfRows; ++i)                                                         \
      EXPECT_EQ(graph->Dof(i), numberOfDofsAtNodalPoint);                                          \
  }                                                                                                \
                                                                                                   \
  TEST_P(MatrixGraph##DistributeName##MeshName##TestLagrange, VectorSizeTest) {                    \
    EXPECT_EQ(graph->size(), vectorSize);                                                          \
  }                                                                                                \
                                                                                                   \
  TEST_P(MatrixGraph##DistributeName##MeshName##TestLagrange, VectorIndexTest) {                   \
    for (int i = 0; i < numberOfRows; ++i)                                                         \
      EXPECT_EQ(graph->Index(i), i *numberOfDofsAtNodalPoint);                                     \
  }                                                                                                \
                                                                                                   \
  TEST_P(MatrixGraph##DistributeName##MeshName##TestLagrange, MatrixSizeTest) {                    \
    EXPECT_EQ(graph->Size(), matrixSize);                                                          \
  }                                                                                                \
                                                                                                   \
  INSTANTIATE_TEST_SUITE_P(MatrixGraphTest, MatrixGraph##DistributeName##MeshName##TestLagrange,   \
                           Values(TestParam{#DistributeName, "LagrangeDoF", 0, 1},                 \
                                  TestParam{#DistributeName, "LagrangeDoF", 0, 2},                 \
                                  TestParam{#DistributeName, "LagrangeDoF", 1, 1},                 \
                                  TestParam{#DistributeName, "LagrangeDoF", 1, 2},                 \
                                  TestParam{#DistributeName, "LagrangeDoF", 2, 1},                 \
                                  TestParam{#DistributeName, "LagrangeDoF", 2, 2}));

#define MATRIXGRAPH_TESTS_RT(MeshName, DistributeName)                                             \
  class MatrixGraph##DistributeName##MeshName##TestRT : public MatrixGraphTest {                   \
  public:                                                                                          \
    MatrixGraph##DistributeName##MeshName##TestRT() : MatrixGraphTest(#MeshName) {}                \
  };                                                                                               \
                                                                                                   \
  TEST_P(MatrixGraph##DistributeName##MeshName##TestRT, NumberOfRowsTest) {                        \
    EXPECT_EQ(graph->rowsize(), numberOfRows);                                                     \
  }                                                                                                \
  TEST_P(MatrixGraph##DistributeName##MeshName##TestRT, NumberOfNodalPointsTest) {                 \
    for (int i = 0; i < numberOfRows; ++i)                                                         \
      EXPECT_EQ(graph->Dof(i), numberOfDofsAtNodalPoint);                                          \
  }                                                                                                \
                                                                                                   \
  TEST_P(MatrixGraph##DistributeName##MeshName##TestRT, VectorSizeTest) {                          \
    EXPECT_EQ(graph->size(), vectorSize);                                                          \
  }                                                                                                \
                                                                                                   \
  TEST_P(MatrixGraph##DistributeName##MeshName##TestRT, VectorIndexTest) {                         \
    for (int i = 0; i < numberOfRows; ++i)                                                         \
      EXPECT_EQ(graph->Index(i), i *numberOfDofsAtNodalPoint);                                     \
  }                                                                                                \
                                                                                                   \
  TEST_P(MatrixGraph##DistributeName##MeshName##TestRT, MatrixSizeTest) {                          \
    EXPECT_EQ(graph->Size(), matrixSize);                                                          \
  }                                                                                                \
                                                                                                   \
  INSTANTIATE_TEST_SUITE_P(MatrixGraphTest, MatrixGraph##DistributeName##MeshName##TestRT,         \
                           Values(TestParam{#DistributeName, "RTDoF", 0, 1},                       \
                                  TestParam{#DistributeName, "RTDoF", 0, 2}));
#if SpaceDimension >= 1
MATRIXGRAPH_TESTS_LAGRANGE(Interval, Stripes)
MATRIXGRAPH_TESTS_LAGRANGE(Interval, RCB)
#endif
#if SpaceDimension >= 2
MATRIXGRAPH_TESTS_LAGRANGE(Triangle, Stripes)
MATRIXGRAPH_TESTS_LAGRANGE(Triangle, RCB)

MATRIXGRAPH_TESTS_RT(Triangle, Stripes)
MATRIXGRAPH_TESTS_RT(Triangle, RCB)

MATRIXGRAPH_TESTS_LAGRANGE(Square, Stripes)
MATRIXGRAPH_TESTS_LAGRANGE(Square, RCB)
#endif
#if SpaceDimension >= 3
MATRIXGRAPH_TESTS_LAGRANGE(Tetrahedron, Stripes)
MATRIXGRAPH_TESTS_LAGRANGE(Tetrahedron, RCB)

// TODO: Fix Tetrahedron RT-Tests.

// MATRIXGRAPH_TESTS_RT(Tetrahedron, Stripes)
// MATRIXGRAPH_TESTS_RT(Tetrahedron, RCB)

MATRIXGRAPH_TESTS_LAGRANGE(Hexahedron, Stripes)
MATRIXGRAPH_TESTS_LAGRANGE(Hexahedron, RCB)
#endif

namespace {

inline void insertNoDuplicates(const std::vector<intproc> &from, std::vector<intproc> &to) {
  std::remove_copy_if(std::cbegin(from), std::cend(from), std::back_inserter(to),
                      [&to](const intproc proc) {
                        // Skip duplicates
                        return std::find(std::cbegin(to), std::cend(to), proc) != std::cend(to);
                      });
}

void insertMeshProcSet(const Mesh &mesh, const Point &point, std::vector<intproc> &procsContainer,
                       const size_t minConnections = 1) {
  const auto ps = mesh.GetProcSets().find_procset(point);
  if (ps != mesh.GetProcSets().procsets_end() && ps.size() > minConnections) {
    insertNoDuplicates(ps->second, procsContainer);
  }
}

std::vector<intproc> collectFaceProcs(const Mesh &mesh, const Cell &cell) {
  std::vector<intproc> procs = {PPM->Proc()};

  for (size_t i = 0; i < cell.Faces(); ++i) {
    auto face = cell.Face(i);
    insertMeshProcSet(mesh, face, procs);
  }

  if (procs.size() > 1) { return procs; }
  return {};
}

std::vector<intproc> collectCornerProcs(const Mesh &mesh, const Cell &cell) {
  std::vector<intproc> procs = {PPM->Proc()};

  for (size_t i = 0; i < cell.Corners(); ++i) {
    const auto &corner = cell.Corner(i);
    insertMeshProcSet(mesh, corner, procs, 2);
  }

  if (procs.size() > 1) { return procs; }
  return {};
}

void distributeCellPsToStoragePoints(
    const IMatrixGraph &graph, const Mesh &mesh, const std::vector<Cell *> &cells,
    const std::vector<std::vector<intproc>> &cellProcSets,
    std::unordered_map<Point, std::vector<intproc>> &procSetsContainer) {
  const auto &dof = graph.GetDoF();
  for (size_t i = 0; i < cells.size(); ++i) {
    const auto &cell = *cells[i];
    const auto &cellPs = cellProcSets[i];
    const auto &storagePoints = dof.GetStoragePoints(cell);
    for (const auto &storagePoint : storagePoints) {
      auto &pointProcSet = procSetsContainer[storagePoint];
      insertMeshProcSet(mesh, storagePoint, pointProcSet);
      insertNoDuplicates(cellPs, pointProcSet);
    }
  }
}

std::vector<Cell *>
communicateCellsWithProcSets(const IMatrixGraph &graph, const std::vector<Cell *> &cells,
                             const std::vector<std::vector<intproc>> &cellProcSets,
                             std::unordered_map<Point, std::vector<intproc>> &procSetsContainer) {
  ExchangeBuffer exBuffer;
  const auto &dof = graph.GetDoF();
  for (size_t i = 0; i < cells.size(); ++i) {
    const auto &cell = *cells[i];
    const auto &procs = cellProcSets[i];

    std::vector<std::pair<Point, std::vector<intproc>>> cellSpProcs;
    for (const auto &storagePoint : dof.GetStoragePoints(cell)) {
      const auto spPsIt = procSetsContainer.find(storagePoint);
      if (spPsIt != std::cend(procSetsContainer)) {
        cellSpProcs.emplace_back(spPsIt->first, spPsIt->second);
      }
    }

    for (const auto proc : procs) {
      if (proc == PPM->Proc()) { continue; }

      exBuffer.Send(proc) << cell;
      exBuffer.Send(proc) << cellSpProcs;
    }
  }

  exBuffer.Communicate();

  std::vector<Cell *> resultCells;
  for (intproc proc = 0; proc < PPM->Size(); ++proc) {
    while (exBuffer.Receive(proc).size() < exBuffer.Receive(proc).Size()) {
      Cell *cell = nullptr;
      std::vector<std::pair<Point, std::vector<intproc>>> cellSpProcs;
      exBuffer.Receive(proc) >> cell;
      exBuffer.Receive(proc) >> cellSpProcs;

      resultCells.push_back(cell);
      for (const auto &[point, procs] : cellSpProcs) {
        insertNoDuplicates(procs, procSetsContainer[point]);
      }
    }
  }

  return resultCells;
}

void communicatePointProcs(std::unordered_map<Point, std::vector<intproc>> &procSetsContainer) {
  ExchangeBuffer exBuffer;
  for (const std::pair<Point, std::vector<intproc>> &pointProcsPair : procSetsContainer) {
    for (const auto proc : pointProcsPair.second) {
      if (proc == PPM->Proc()) { continue; }
      exBuffer.Send(proc) << pointProcsPair;
    }
  }

  exBuffer.Communicate();

  for (intproc proc = 0; proc < PPM->Size(); ++proc) {
    while (exBuffer.Receive(proc).size() < exBuffer.Receive(proc).Size()) {
      std::pair<Point, std::vector<intproc>> pointProcsPair;
      exBuffer.Receive(proc) >> pointProcsPair;
      insertNoDuplicates(pointProcsPair.second, procSetsContainer[pointProcsPair.first]);
    }
  }
}

std::pair<std::vector<Cell *>, std::vector<std::vector<intproc>>>
findOverlapNeighbourCellsAndProcSets(const Mesh &mesh) {
  std::vector<Cell *> cells;
  std::vector<std::vector<intproc>> procSets;

  for (cell cell = mesh.cells(); cell != mesh.cells_end(); ++cell) {
    std::vector<intproc> neighbourProcs = collectFaceProcs(mesh, *cell);
    if (neighbourProcs.size() > 1) {
      cells.emplace_back(cell->second);
      procSets.emplace_back(std::move(neighbourProcs));
    }
  }

  return std::make_pair(std::move(cells), std::move(procSets));
}

std::pair<std::vector<Cell *>, std::vector<std::vector<intproc>>>
findCrossPointCellsAndProcSets(const Mesh &mesh) {
  std::vector<Cell *> cells;
  std::vector<std::vector<intproc>> procSets;

  for (cell cell = mesh.cells(); cell != mesh.cells_end(); ++cell) {
    auto cornerProcs = collectCornerProcs(mesh, *cell);
    // Collect cell and other procs only if there is more than 1 corner neighbour
    if (cornerProcs.size() > 1) {
      const auto &neighbourProcs = collectFaceProcs(mesh, *cell);
      insertNoDuplicates(neighbourProcs, cornerProcs);

      cells.emplace_back(cell->second);
      procSets.emplace_back(std::move(cornerProcs));
    }
  }

  return std::make_pair(std::move(cells), std::move(procSets));
}

void registerOrConnectAdjacentCells(
    std::unordered_map<Point, const Cell *> &faceToCellMap,
    std::vector<std::pair<const Cell *, const Cell *>> &cellConnectivity, const Cell &cell) {
  for (size_t i = 0; i < cell.Faces(); ++i) {
    const auto &faceCenter = cell.Face(i);
    const auto faceToCellCenterIt = faceToCellMap.find(faceCenter);
    if (faceToCellCenterIt != std::end(faceToCellMap)) {
      // We found 2 adjacent cells. Insert the ordered mapping
      if (faceToCellCenterIt->second->Center() > cell.Center()) {
        cellConnectivity.emplace_back(faceToCellCenterIt->second, &cell);
      } else {
        cellConnectivity.emplace_back(&cell, faceToCellCenterIt->second);
      }
    } else {
      faceToCellMap[faceCenter] = &cell;
    }
  }
}

void insertCellInternalConnections(
    const IDoF &dof, const Cell &cell,
    std::unordered_map<Point, std::unordered_set<Point>> &rowEntries) {
  const auto &storagePoints = dof.GetStoragePoints(cell);
  for (size_t i = 1; i < storagePoints.size(); ++i) {
    for (size_t j = 0; j < i; ++j) {
      if (storagePoints[i] > storagePoints[j]) {
        rowEntries[storagePoints[i]].emplace(storagePoints[j]);
      } else {
        rowEntries[storagePoints[j]].emplace(storagePoints[i]);
      }
    }
  }
}

} // namespace

class DerivedMatrixGraphTest :
    public testing::TestWithParam<std::tuple<std::string, std::string, int>> {
public:
protected:
  constexpr static int level = 2;
  constexpr static int plevel = 2;
  const int degree = std::get<2>(GetParam());

  const std::unique_ptr<const Meshes> meshes = MeshesCreator(std::get<0>(GetParam()))
                                                   .WithDistribute(std::get<1>(GetParam()))
                                                   .WithPLevel(plevel)
                                                   .WithLevel(level)
                                                   .CreateUnique();
  const Mesh &mesh = (*meshes)[MeshIndex{level}];

  void checkRowEntries(const IMatrixGraph &graph, const std::vector<Cell *> &olapCells) {
    // Find adjacent cells which must be represented by the graph
    std::unordered_map<Point, const Cell *> faceToCellMap;
    std::vector<std::pair<const Cell *, const Cell *>> cellConnectivity;
    const auto &dof = graph.GetDoF();
    std::unordered_map<Point, std::unordered_set<Point>> expectedRowEntries;
    for (cell cell = mesh.cells(); cell != mesh.cells_end(); ++cell) {
      registerOrConnectAdjacentCells(faceToCellMap, cellConnectivity, *cell);
      insertCellInternalConnections(dof, *cell, expectedRowEntries);
    }
    for (const auto olapCell : olapCells) {
      registerOrConnectAdjacentCells(faceToCellMap, cellConnectivity, *olapCell);
      insertCellInternalConnections(dof, *olapCell, expectedRowEntries);
    }

    // Check adjacent cell connectivity, including their storage points
    const auto &rows = graph.GetRows();
    for (const auto &[firstCell, secondCell] : cellConnectivity) {
      const auto &firstStoragePoints = dof.GetStoragePoints(*firstCell);
      const auto &secondStoragePoints = dof.GetStoragePoints(*secondCell);
      for (const auto &firstStoragePoint : firstStoragePoints) {
        for (const auto &secondStoragePoint : secondStoragePoints) {
          if (firstStoragePoint == secondStoragePoint) { continue; }
          Point first = firstStoragePoint;
          Point second = secondStoragePoint;
          if (firstStoragePoint < secondStoragePoint) {
            first = secondStoragePoint;
            second = firstStoragePoint;
          }
          expectedRowEntries[first].emplace(second);

          const auto &row = rows.at(first);
          const auto entryIt = row.FindEntry(second);
          // Check if all cells are properly connected
          EXPECT_TRUE(entryIt != row.EntriesEnd())
              << "Entry " << second << " in row " << first << " doesn't exist";
        }
      }
    }

    size_t numberOfEntries = 0;
    for (const auto &[_, connectedTargets] : expectedRowEntries) {
      numberOfEntries += connectedTargets.size();
    }

    // Ensure that we didn't miss an entry
    EXPECT_EQ(rows.NumberOfEntriesTotal(), rows.size() + numberOfEntries);
  }

  static void checkProcSets(const IMatrixGraph &graph,
                            const std::unordered_map<Point, std::vector<intproc>> &pointProcs) {
    for (const auto &[point, procs] : pointProcs) {
      const auto graphPs = graph.find_procset(point);
      EXPECT_TRUE(graphPs != graph.procsets_end()) << "ProcSet for " << point << " doesn't exist";

      EXPECT_TRUE(std::is_permutation(std::cbegin(*graphPs), std::cend(*graphPs),
                                      std::cbegin(procs), std::end(procs)))
          << "ProcSets for " << point << " don't contain the same elements";
    }
    EXPECT_EQ(graph.GetProcSets().size(), pointProcs.size());
  }

  void checkRows(const IMatrixGraph &graph, const std::vector<Cell *> &olapCells,
                 const std::vector<Cell *> &remoteCpCells = {}) {
    const auto &dof = graph.GetDoF();
    std::unordered_set<Point> expectedRows;

    for (cell cell = mesh.cells(); cell != mesh.cells_end(); ++cell) {
      const auto &storagePoints = dof.GetStoragePoints(*cell);
      for (const auto &storagePoint : storagePoints) {
        expectedRows.emplace(storagePoint);
        EXPECT_TRUE(graph.find_row(storagePoint) != graph.rows_end())
            << "Row for " << storagePoint << " doesn't exist";
      }
    }
    for (const auto olapCell : olapCells) {
      const auto &storagePoints = dof.GetStoragePoints(*olapCell);
      for (const auto &storagePoint : storagePoints) {
        expectedRows.emplace(storagePoint);
        EXPECT_TRUE(graph.find_row(storagePoint) != graph.rows_end())
            << "Row for " << storagePoint << " doesn't exist (overlap cell)";
      }
    }
    for (const auto cpCell : remoteCpCells) {
      const auto &storagePoints = dof.GetStoragePoints(*cpCell);
      for (const auto &storagePoint : storagePoints) {
        expectedRows.emplace(storagePoint);
        const auto row = graph.find_row(cpCell->Center());
        EXPECT_TRUE(row != graph.rows_end())
            << "Row for " << storagePoint << " doesn't exist (cross point cell)";

        const auto &allocSizes = dof.AllocationSizesAtStoragePoints(*cpCell);
        size_t expectedNumberOfDofs = std::reduce(std::cbegin(allocSizes), std::cend(allocSizes));
        EXPECT_EQ(row.NumberOfDofs(), expectedNumberOfDofs);
      }
    }

    // Must contain normal cells, overlap cells and cross point cells
    EXPECT_EQ(graph.GetRows().size(), expectedRows.size());
  }

  static void checkOverlapCells(const IMatrixGraph &graph, const std::vector<Cell *> &cells) {
    for (const auto cell : cells) {
      EXPECT_TRUE(graph.find_overlap_cell(cell->Center()) != graph.overlap_end())
          << "Overlap cell " << cell->Center() << " doesn't exist";
    }
    EXPECT_EQ(graph.OverlapCount(), cells.size());
  }
private:
};

TEST_P(DerivedMatrixGraphTest, TestDGMatrixGraph) {
  const auto disc = std::make_unique<DGDiscretization>(*meshes, degree, SpaceDimension);
  const IMatrixGraph &graph = (*disc)(MeshIndex{level});

  const auto &[olapNbCells, olapNbProcSets] = findOverlapNeighbourCellsAndProcSets(mesh);
  const auto &[localCpCells, localCpProcSets] = findCrossPointCellsAndProcSets(mesh);

  std::unordered_map<Point, std::vector<intproc>> storagePointProcSets;
  // Fill cell center procsets
  distributeCellPsToStoragePoints(graph, mesh, olapNbCells, olapNbProcSets, storagePointProcSets);
  distributeCellPsToStoragePoints(graph, mesh, localCpCells, localCpProcSets, storagePointProcSets);
  // Share overlap and cross point cells
  const auto &olapCells =
      communicateCellsWithProcSets(graph, olapNbCells, olapNbProcSets, storagePointProcSets);
  const auto &remoteCpCells =
      communicateCellsWithProcSets(graph, localCpCells, localCpProcSets, storagePointProcSets);

  // Check procsets of overlap (neighbour) and cross point cells
  checkProcSets(graph, storagePointProcSets);

  // Check if all overlap cells are present
  checkOverlapCells(graph, olapCells);

  // Check if all rows are present
  checkRows(graph, olapCells, remoteCpCells);

  // Check if all row entries exist
  checkRowEntries(graph, olapCells);

  for (const auto cell : olapCells) {
    delete cell;
  }
  for (const auto cell : remoteCpCells) {
    delete cell;
  }
}

TEST_P(DerivedMatrixGraphTest, TestEGMatrixGraph) {
  const auto disc = std::make_unique<MixedEGDiscretization>(*meshes, degree);
  const IMatrixGraph &graph = (*disc)(MeshIndex{level});

  const auto &[olapNbCells, olapNbProcSets] = findOverlapNeighbourCellsAndProcSets(mesh);

  std::unordered_map<Point, std::vector<intproc>> storagePointProcSets;
  // Add cell procset to every storage point and synchronize the them with every process. This is
  // required as some procsets are shared between multiple cells (faces, corners, etc.)
  distributeCellPsToStoragePoints(graph, mesh, olapNbCells, olapNbProcSets, storagePointProcSets);
  communicatePointProcs(storagePointProcSets);

  // Share overlap cells and all storage point procsets
  const auto &olapCells =
      communicateCellsWithProcSets(graph, olapNbCells, olapNbProcSets, storagePointProcSets);

  // Check procsets of overlap (neighbour) cells and their storage points (depending on the depth)
  checkProcSets(graph, storagePointProcSets);

  // Check if all overlap cells are present
  checkOverlapCells(graph, olapCells);

  // Check if all rows are present
  checkRows(graph, olapCells);

  // Check if all row entries exist
  checkRowEntries(graph, olapCells);

  for (const auto cell : olapCells) {
    delete cell;
  }
}

INSTANTIATE_TEST_SUITE_P(TestMatrixGraph, DerivedMatrixGraphTest,
                         Combine(Values("Interval"
#if SpaceDimension >= 2
                                        ,
                                        "Triangle", "Square", "Square2Triangles"
#endif
#if SpaceDimension >= 3
                                        ,
                                        "Tetrahedron", "Hexahedron"
#endif
                                        ),
                                 Values("Stripes", "Stripes_x", "Stripes_y", "Stripes_z", "RCB",
                                        "RCBx", "RCBy", "RCBz", "RCB2D", "RCB2Dx", "RCB2Dy", "RCBG",
                                        "RCBGx", "RCBGy", "RCBGz"),
                                 Range(0, 3)));

int main(int argc, char **argv) {
  MppTest mppTest = MppTestBuilder(argc, argv).WithPPM().WithParallelListeners();
  return mppTest.RUN_ALL_MPP_TESTS();
}

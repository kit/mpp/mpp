#include "TestVector.hpp"

TEST_F(TestVector, TestDoubleAccumulationOverlap) {
  vector = 1.0;
  vector.Accumulate();
  testVectorOverlap(vector, 1.0);
}

TEST_F(TestVector, TestDoubleAccumulationNonOverlap) {
  vector = 1.0;
  vector.Accumulate();
  testVectorNonOverlap(vector, 1.0);
}

TEST_F(TestVector, TestAddingWithoutOverlap) {
  vector = 1.0;
  vector.MakeAdditive();
  Vector vector2(vector);
  vector2 = 4.0;
  testAdditionWithoutOverlap(vector, vector2);
}

TEST_F(TestVector, TestNormWithoutOverlap) {
  vector = 1.0;
  testNormWithoutOverlap(vector);
}

TEST_F(TestVector, TestHadamard) {
  Vector product(0.0, vector);
  product = HadamardProductVector(vector, product);
  for (row r = product.rows(); r != product.rows_end(); ++r) {
    EXPECT_EQ(product(r, 0), 0.0);
  }
  product = HadamardProductVector(vector, vector);
  for (row r = product.rows(); r != product.rows_end(); ++r) {
    EXPECT_EQ(product(r, 0), 1.0);
  }
}

TEST_F(TestVector, TestSqrtComponents) {
  Vector root(4.0, vector);
  root = ComponentSqrt(root);
  for (row r = root.rows(); r != root.rows_end(); ++r) {
    EXPECT_EQ(root(r, 0), 2.0);
  }
  root = ComponentSqrt(vector);
  for (row r = root.rows(); r != root.rows_end(); ++r) {
    EXPECT_EQ(root(r, 0), 1.0);
  }
}

TEST_F(TestVector, TestComponentDivide) {
  Vector division(0.5, vector);
  division = ComponentDivide(vector, division);
  for (row r = division.rows(); r != division.rows_end(); ++r) {
    EXPECT_EQ(division(r, 0), 2.0);
  }
  division = ComponentDivide(vector, vector);
  for (row r = division.rows(); r != division.rows_end(); ++r) {
    EXPECT_EQ(division(r, 0), 1.0);
  }
}

TEST_F(TestVector, TestConstructorWithRVector) {
  auto disc = std::make_shared<const LagrangeDiscretization>(*meshes, 1, 1);
  RVector rVector(PPM->Proc(0), meshes->fine().VertexCount());
  Vector v(rVector, disc, meshes->FineLevel());
  pout << rVector << std::endl;
  pout << v << std::endl;
  for (row r = v.rows(); r != v.rows_end(); ++r) {
    procset ps = v.find_procset(r());
    if (ps != v.procsets_end()) {
      EXPECT_EQ(v(r, 0), ps.master()) << "Point:" << r() << std::endl;
    } else {
      EXPECT_EQ(v(r, 0), PPM->Proc(0));
    }
  }
}

#if BUILD_UQ

#include "Random.hpp"

TEST_F(TestVector, TestConstructorWithRandomRVectorLagrange) {
  auto disc = std::make_shared<const LagrangeDiscretization>(*meshes, 1, 1);
  Vector v(disc, meshes->FineLevel(), DIST_TYPE::NORMAL);
  v.MakeAdditive();
  pout << v << endl;
  double sum = 0;
  for (row r = v.rows(); r != v.rows_end(); ++r) {
    sum += v(r, 0);
  }
  // Not 100% accurate, but should be close
  pout << "Local average: " << sum / (double)meshes->fine().VertexCount() << endl;
  double average = PPM->SumOnCommSplit(sum, 0) / (double)v.Size();
  EXPECT_NEAR(average, 0, 2 * std::pow(v.Size(), -0.5))
      << "Average: " << average << " Size: " << v.Size() << std::endl;
}

TEST_F(TestVector, TestConstructorWithRandomRVectorDG) {
  auto disc = std::make_shared<const DGDiscretization>(*meshes, 1, 0);
  Vector v(disc, meshes->FineLevel(), DIST_TYPE::NORMAL);
  pout << v << endl;
}

#endif

int main(int argc, char **argv) {
  return MppTest(MppTestBuilder(argc, argv)
                     .WithConfigEntry("DistributionVerbose", 0)
                     .WithConfigEntry("MeshesVerbose", 1)
#if BUILD_UQ
                     .WithRandomInitialized()
#endif
                     .WithConfigEntry("MeshVerbose", 2)
                     .WithParallelListeners()
                     .WithScreenLogging()
                     .WithPPM())
      .RUN_ALL_MPP_TESTS();
}
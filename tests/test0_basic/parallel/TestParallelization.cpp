#include "parallel/Parallel.hpp"

#include "TestEnvironment.hpp"

struct Params {
  int commSplit = 0;

  int distLevel = 0;
};

std::ostream &operator<<(std::ostream &s, const Params &testParams) {
  return s << "CommSplit: " << testParams.commSplit << "   DistLevel: " << testParams.distLevel
           << endl;
}

class TestPPM : public TestWithParam<Params> {
public:
  TestPPM() :
      commSplit(GetParam().commSplit == -1 ? PPM->MaxCommSplit() : GetParam().commSplit),
      distLevel(GetParam().distLevel >= PPM->MaxCommSplit() ? -1 : GetParam().distLevel) {
    PPM->Barrier(0);
  }

  void TearDown() override { PPM->Barrier(0); }
protected:
  int commSplit = 0;

  int distLevel = 0;
};

// clang-format off
INSTANTIATE_TEST_SUITE_P(TestPPM, TestPPM, Values(
   // First tests for split comms
   Params{1, -1}, Params{2, -1}, Params{3, -1}, Params{4, -1}, Params{-1, -1},
    // Then tests for dual comms
   Params{0, 0}, Params{0, 1}, Params{0, 2}, Params{0, 3}, Params{0, 4}
));
// clang-format on

TEST_P(TestPPM, TestIsInitialized) { EXPECT_TRUE(PPM->IsInitialized()); }

TEST_P(TestPPM, TestBroadcastDouble) {
  double a = 1.0;
  double b;
  if (PPM->Master(commSplit, distLevel)) PPM->Broadcast(&a, sizeof(double), commSplit, distLevel);
  if (!PPM->Master(commSplit, distLevel)) {
    PPM->Broadcast(&b, sizeof(double), commSplit, distLevel);
    EXPECT_DOUBLE_EQ(a, b);
  }
}

TEST_P(TestPPM, TestBroadcastDouble2) {
  double a;
  if (PPM->Master(commSplit, distLevel)) PPM->BCastOnCommSplit(1.0, commSplit, distLevel);
  if (!PPM->Master(commSplit, distLevel)) {
    PPM->BCastOnCommSplit(a, commSplit, distLevel);
    EXPECT_DOUBLE_EQ(a, 1.0);
  }
}

TEST_P(TestPPM, TestBroadcastDouble3) {
  double a;
  if (PPM->Master(0)) PPM->BroadcastDouble(1.0);
  if (!PPM->Master(0)) {
    a = PPM->BroadcastDouble();
    EXPECT_DOUBLE_EQ(a, 1.0);
  }
}

TEST_P(TestPPM, TestBroadcastInt) {
  int a = 1;
  int b;
  if (PPM->Master(commSplit, distLevel)) PPM->Broadcast(&a, sizeof(int), commSplit, distLevel);
  if (!PPM->Master(commSplit, distLevel)) {
    PPM->Broadcast(&b, sizeof(double), commSplit, distLevel);
    EXPECT_EQ(a, b);
  }
}

TEST_P(TestPPM, TestBroadcastInt2) {
  int a;
  if (PPM->Master(commSplit, distLevel)) PPM->BCastOnCommSplit(1, commSplit, distLevel);
  if (!PPM->Master(commSplit, distLevel)) {
    PPM->BCastOnCommSplit(a, commSplit, distLevel);
    EXPECT_EQ(a, 1);
  }
}

TEST_P(TestPPM, TestBroadcastInt3) {
  int a;
  if (PPM->Master(0)) PPM->BroadcastInt(1);
  if (!PPM->Master(0)) {
    a = PPM->BroadcastInt();
    EXPECT_EQ(a, 1);
  }
}

TEST_P(TestPPM, TestProc) {
  if (PPM->Master(commSplit, distLevel)) EXPECT_EQ(PPM->Proc(commSplit, distLevel), 0);
  if (!PPM->Master(commSplit, distLevel)) EXPECT_NE(PPM->Proc(commSplit, distLevel), 0);
}

TEST_P(TestPPM, TestSize) {
  if (distLevel == -1) {
    int testSize;
    MPI_Comm_size(MPI_COMM_WORLD, &testSize);
    if (testSize != 1 && testSize >= pow(2, commSplit)) {
      EXPECT_EQ(PPM->Size(commSplit, distLevel), testSize / pow(2, commSplit));
    } else {
      EXPECT_EQ(PPM->Size(commSplit), 1);
    }
  } else {
    PPM->PrintInfo(commSplit, distLevel);
    EXPECT_EQ(PPM->Size(commSplit, distLevel), 2);
  }
}

TEST_P(TestPPM, TestColor) {
  if (distLevel == -1) { // This calls splitted comms
    if (commSplit == 0) {
      // Always world communicator, all have the same color
      EXPECT_EQ(PPM->Color(commSplit, distLevel), 0);
    } else if (commSplit >= PPM->MaxCommSplit()) {
      // Completely separated communicators, color is equal to process id
      EXPECT_EQ(PPM->Color(commSplit, distLevel), PPM->Proc(0));
    } else {
      EXPECT_EQ(PPM->Proc(commSplit, distLevel)
                    + PPM->Color(commSplit, distLevel) * PPM->Size(commSplit, distLevel),
                PPM->Proc(0, distLevel))
          << "global=" << PPM->Proc(0) << " local=" << PPM->Proc(commSplit, distLevel)
          << " color=" << PPM->Color(commSplit, distLevel);
    }
  } else { // This calls dual comms
    if (PPM->Proc(commSplit, distLevel) == 0) {
      EXPECT_EQ(PPM->Color(commSplit, distLevel), PPM->Proc(0));
    } else if (PPM->Proc(commSplit, distLevel) == 1) {
      int partnerRank = PPM->Proc(0) ^ (1 << distLevel);
      EXPECT_EQ(PPM->Color(commSplit, distLevel), partnerRank);
    } else {
      Exit("Size of communicator is not 2");
    }
  }
}

TEST_P(TestPPM, TestIntegerSum) {
  int a = 1;
  PPM->Sum(&a, 1, commSplit, distLevel);
  EXPECT_EQ(a, PPM->Size(commSplit, distLevel));
}

TEST_P(TestPPM, TestIntegerSum2) {
  int a = 1;
  EXPECT_EQ(PPM->SumOn(a, commSplit, distLevel), PPM->Size(commSplit, distLevel));
}

TEST_P(TestPPM, TestDoubleSum) {
  double a = 1.0;
  PPM->Sum(&a, 1, commSplit, distLevel);
  EXPECT_DOUBLE_EQ(a, (double)PPM->Size(commSplit, distLevel));
}

TEST_P(TestPPM, TestDoubleSum2) {
  double a = 1.0;
  EXPECT_DOUBLE_EQ(PPM->SumOn(a, commSplit, distLevel), (double)PPM->Size(commSplit, distLevel));
}

TEST_P(TestPPM, TestSizeTypeSum) {
  size_t a = sizeof(int);
  PPM->Sum(&a, 1, commSplit, distLevel);
  EXPECT_EQ(a, (size_t)sizeof(int) * PPM->Size(commSplit, distLevel));
}

TEST_P(TestPPM, TestSizeTypeSum2) {
  size_t a = sizeof(int);
  EXPECT_EQ(PPM->SumOn(a, commSplit, distLevel), (size_t)a * PPM->Size(commSplit, distLevel));
}

TEST_P(TestPPM, TestComplexSum) {
  std::complex<double> a{1.0, 1.0};
  std::complex<double> b{std::real(a) * PPM->Size(commSplit, distLevel),
                         std::imag(a) * PPM->Size(commSplit, distLevel)};
  PPM->Sum(&a, 1, commSplit, distLevel);
  EXPECT_EQ(a, b);
}

TEST_P(TestPPM, TestIntegerMinMax) {
  int a;
  if (PPM->Master(commSplit, distLevel)) a = 1;
  if (!PPM->Master(commSplit, distLevel)) a = 0;

  if (PPM->Size(commSplit, distLevel) != 1) EXPECT_EQ(PPM->Min(a, commSplit, distLevel), 0);
  else EXPECT_EQ(PPM->Min(a, commSplit, distLevel), 1);

  if (PPM->Master(commSplit, distLevel)) a = 0;
  if (!PPM->Master(commSplit, distLevel)) a = 1;

  EXPECT_EQ(PPM->Min(a, commSplit, distLevel), 0);
}

TEST_P(TestPPM, TestDoubleMin) {
  double a;
  if (PPM->Master(commSplit, distLevel)) a = 1.0;
  if (!PPM->Master(commSplit, distLevel)) a = 0.0;

  if (PPM->Size(commSplit, distLevel) != 1) EXPECT_EQ(PPM->Min(a, commSplit, distLevel), 0.0);
  else EXPECT_EQ(PPM->Min(a, commSplit, distLevel), 1.0);

  if (PPM->Master(commSplit, distLevel)) a = 0.0;
  if (!PPM->Master(commSplit, distLevel)) a = 1.0;

  EXPECT_EQ(PPM->Min(a, commSplit, distLevel), 0.0);
}

TEST_P(TestPPM, TestUnsignedIntegerMin) {
  long unsigned int a;
  if (PPM->Master(commSplit, distLevel)) a = 1;
  if (!PPM->Master(commSplit, distLevel)) a = 0;

  if (PPM->Size(commSplit, distLevel) != 1) EXPECT_EQ(PPM->Min(a, commSplit, distLevel), 0);
  else EXPECT_EQ(PPM->Min(a, commSplit, distLevel), 1);

  if (PPM->Master(commSplit, distLevel)) a = 0;
  if (!PPM->Master(commSplit, distLevel)) a = 1;

  EXPECT_EQ(PPM->Min(a, commSplit, distLevel), 0);
}

TEST_P(TestPPM, TestIntegerMax) {
  int a;
  if (PPM->Master(commSplit, distLevel)) a = 1;
  if (!PPM->Master(commSplit, distLevel)) a = 0;

  EXPECT_EQ(PPM->Max(a, commSplit, distLevel), 1);

  if (PPM->Master(commSplit, distLevel)) a = 0;
  if (!PPM->Master(commSplit, distLevel)) a = 1;

  if (PPM->Size(commSplit, distLevel) != 1) EXPECT_EQ(PPM->Max(a, commSplit, distLevel), 1);
  else EXPECT_EQ(PPM->Max(a, commSplit, distLevel), 0);
}

TEST_P(TestPPM, TestDoubleMax) {
  double a;
  if (PPM->Master(commSplit, distLevel)) a = 1.0;
  if (!PPM->Master(commSplit, distLevel)) a = 0.0;

  EXPECT_DOUBLE_EQ(PPM->Max(a, commSplit, distLevel), 1.0);

  if (PPM->Master(commSplit, distLevel)) a = 0.0;
  if (!PPM->Master(commSplit, distLevel)) a = 1.0;

  if (PPM->Size(commSplit) != 1) EXPECT_EQ(PPM->Max(a, commSplit, distLevel), 1.0);
  else EXPECT_EQ(PPM->Max(a, commSplit, distLevel), 0.0);
}

TEST_P(TestPPM, TestUnsignedIntMax) {
  long unsigned int a;
  if (PPM->Master(commSplit, distLevel)) a = 1;
  if (!PPM->Master(commSplit, distLevel)) a = 0;

  EXPECT_EQ(PPM->Max(a, commSplit, distLevel), 1);

  if (PPM->Master(commSplit, distLevel)) a = 0;
  if (!PPM->Master(commSplit, distLevel)) a = 1;

  if (PPM->Size(commSplit, distLevel) != 1) EXPECT_EQ(PPM->Max(a, commSplit, distLevel), 1);
  else EXPECT_EQ(PPM->Max(a, commSplit, distLevel), 0);
}

TEST_P(TestPPM, TestAnd) {
  bool b;
  if (PPM->Master(commSplit, distLevel)) b = true;
  if (!PPM->Master(commSplit, distLevel)) b = false;

  if (PPM->Size(commSplit, distLevel) != 1) EXPECT_EQ(PPM->And(b, 0), false);
  else EXPECT_EQ(PPM->And(b, 0), true);

  if (PPM->Master(commSplit, distLevel)) b = false;
  if (!PPM->Master(commSplit, distLevel)) b = true;

  EXPECT_EQ(PPM->And(b, 0), false);
}

TEST_P(TestPPM, TestOr) {
  bool b;
  if (PPM->Master(commSplit, distLevel)) b = true;
  if (!PPM->Master(commSplit, distLevel)) b = false;

  EXPECT_EQ(PPM->Or(b), true);

  if (PPM->Master(commSplit, distLevel)) b = false;
  if (!PPM->Master(commSplit, distLevel)) b = true;

  if (PPM->Size(commSplit) != 1) EXPECT_EQ(PPM->Or(b), true);
  else EXPECT_EQ(PPM->Or(b), false);
}

int main(int argc, char **argv) {
  return MppTest(MppTestBuilder(argc, argv).WithParallelListeners().WithScreenLogging().WithPPM())
      .RUN_ALL_MPP_TESTS();
}
#include "SPDESampling.hpp"
#include "SingleLevelEstimator.hpp"
#include "TestEnvironment.hpp"

class TestSPDESampling : public Test {
protected:
  double TOL_MEAN = 4.0;

  double TOL_SVAR = 64.0;

  double TOL_SKEW = 128.0;

  double TOL_KURT = 256.0;
public:
  TestSPDESampling() {}
};

TEST_F(TestSPDESampling, TestSPDESamplingOnInterval) {
  SPDESamplingOnInterval spdeSampling((MultiXFEMConfig()));
  spdeSampling.RunProtocol(SampleID(5, 0, false));
}

TEST_F(TestSPDESampling, TestDrawSampleOnInterval) {
  SPDESamplingOnInterval spdeSampling((MultiXFEMConfig()));
  spdeSampling.RunProtocol(SampleID(5, 0, false));
}

#if (SpaceDimension >= 2)
TEST_F(TestSPDESampling, TestSPDESamplingOnSquare) {
  SPDESamplingOnSquare spdeSampling((MultiXFEMConfig()));
  spdeSampling.RunProtocol(SampleID(4, 0, false));
}

TEST_F(TestSPDESampling, TestDrawSampleOnSquare) {
  const auto id = SampleID(5, 0, false);
  SPDESamplingOnSquare spdeSampling((MultiXFEMConfig()));
  auto sample = spdeSampling.RunProtocol(id);
  auto disc = std::make_shared<const DGDiscretization>(sample.Get<Vector>("grf").GetMeshes(), 0);
  Vector cellCenteredLogNormal(0.0, disc, id.GetMeshIndex());
  for (cell c = sample.Get<Vector>("grf").cells(); c != sample.Get<Vector>("grf").cells_end(); ++c) {
    cellCenteredLogNormal(c(), 0) =
        std::exp(ScalarElement(sample.Get<Vector>("grf"), *c).Value(c(), sample.Get<Vector>("grf")));
  }
  mpp::PlotData("LogNormal", "LogNormal" + id.IdString(), cellCenteredLogNormal);
}
#endif

#if (SpaceDimension >= 3)
TEST_F(TestSPDESampling, TestSPDESamplingOnHexahedron) {
  SPDESamplingOnHexahedron spdeSampling((MultiXFEMConfig()));
  spdeSampling.RunProtocol(SampleID(3, 0, false));
}
#endif

#if AGGREGATE_FOR_SOLUTION
TEST_F(TestSPDESampling, TestStatisticsOnSquare) {
  SingleLevelEstimator estimator(
      SLEstimatorConfig(5, 4096, 0.0, true)
          .WithMultiSampleFEMConfig(MultiXFEMConfig("SPDESamplingOnInterval")));

  estimator.Method(std::nullopt);

  const auto aggregate = estimator.GetAggregate();
  const auto meanField = aggregate.rdF.at("grf").GetTotal().mean;
  const auto sVarField = aggregate.rdF.at("grf").GetTotal().sVar;
  const auto skewField = aggregate.rdF.at("grf").GetTotal().skew;
  const auto kurtField = aggregate.rdF.at("grf").GetTotal().kurt;
  const auto sampleNum = aggregate.ctr.M;

  for (row r = meanField.rows(); r != meanField.rows_end(); r++) {
    EXPECT_NEAR(0.0, meanField(r, 0), TOL_MEAN * sqrt(1.0 / sampleNum)) << "Point: " << r();
    EXPECT_NEAR(1.0, sVarField(r, 0), TOL_SVAR * sqrt(1.0 / sampleNum)) << "Point: " << r();
    //    EXPECT_NEAR(0.0, skewField(r, 0), TOL_SKEW * sqrt(1.0 / sampleNum)) << "Point: " << r();
    //    EXPECT_NEAR(3.0, kurtField(r, 0), TOL_KURT * sqrt(1.0 / sampleNum)) << "Point: " << r();
  }
}
#endif

class TestEllipticProblemsWithLinearLagrange : public Test {
protected:
  PDESolverConfig conf;
public:
  TestEllipticProblemsWithLinearLagrange() {
    conf = PDESolverConfig().WithModel("Lagrange").WithLevel(5).WithPLevel(0).WithDegree(1);
  }
};

int main(int argc, char **argv) {
  return MppTest(MppTestBuilder(argc, argv)
                     .WithConfigEntry("Quantity", "L2")
                     .WithConfigEntry("VtuPlot", 0)
                     .WithConfigEntry("ParallelPlotting", 0)
                     .WithConfigEntry("SPDEPlotting", 1)
                     .WithConfigEntry("SLEstimatorPlotting", 0)
                     .WithConfigEntry("SLEstimatorVerbose", 1)
                     .WithConfigEntry("MSFEMVerbose", 0)
                     .WithConfigEntry("MeshesVerbose", 0)
                     .WithConfigEntry("MeshVerbose", 0)
                     .WithConfigEntry("PDESolverVerbose", 0)
                     .WithConfigEntry("NewtonVerbose", 0)
                     .WithConfigEntry("LinearVerbose", 0)
                     .WithConfigEntry("level", 3)
                     .WithConfigEntry("plevel", 1)
                     //                     .WithConfigEntry("ell", 0.3)
                     //                     .WithConfigEntry("nu", 1)
                     .WithScreenLogging()
                     .WithRandomInitialized()
                     .WithPPM())
      .RUN_ALL_MPP_TESTS();
}
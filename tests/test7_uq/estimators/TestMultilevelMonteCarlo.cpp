#include <regex>
#include "MultiLevelEstimator.hpp"
#include "Random.hpp"
#include "TestEnvironment.hpp"

class TestMultilevelMonteCarlo : public TestWithParam<MLEstimatorConfig> {
protected:
  PDESolverConfig pdeSolverConfig;

  MultiXFEMConfig mxFEMConfig;

  SLEstimatorConfig slEstmConfig;

  MLEstimatorConfig mlEstmConfig;

  MultiLevelEstimator mlmc;

  TestMultilevelMonteCarlo(const PDESolverConfig &pdeSolverConf, const MultiXFEMConfig &mxFEMConf,
                           const SLEstimatorConfig &slEstmConf) :
      pdeSolverConfig(pdeSolverConf), mxFEMConfig(mxFEMConf), slEstmConfig(slEstmConf),
      mlEstmConfig(MLEstimatorConfig(GetParam())),

      mlmc(MultiLevelEstimator(
          mlEstmConfig.WithSLEstimatorConfig(slEstmConfig.WithMultiSampleFEMConfig(
              mxFEMConfig.WithPDESolverConfigs({pdeSolverConfig}))))) {

    mout << mxFEMConfig << std::endl;
    mout << slEstmConfig << std::endl;
    mout << mlEstmConfig << std::endl;
    mout << std::endl;

    mlmc.Method(std::nullopt);
    mout << endl;

    mlmc.PrintInfo();
  }

  void CheckTotalErrors() const {
    if (mlEstmConfig.epsilon == 0.0) return;
    const double &epsilon = mlEstmConfig.epsilon;
    const auto &dataKey = mlEstmConfig.dataKey;
    EXPECT_LE(mlmc.GetEstimatorMap().GetErrors().GetRMSE(dataKey), epsilon);
    EXPECT_LE(mlmc.GetEstimatorMap().GetErrors().GetDISC(dataKey), epsilon);
    EXPECT_LE(mlmc.GetEstimatorMap().GetErrors().GetMSE(dataKey), std::pow(epsilon, 2));
    EXPECT_LE(mlmc.GetEstimatorMap().GetErrors().GetNUM(dataKey), std::pow(epsilon, 2));
    EXPECT_LE(mlmc.GetEstimatorMap().GetErrors().GetSAM(dataKey), std::pow(epsilon, 2));
  }

  void CheckTotalCost() const {
    if (mlEstmConfig.timeBudget == 0.0) return;
    // Time budget test is not sharp to prevent random pipeline failures
    EXPECT_LE(mlmc.Cost(), mlEstmConfig.timeBudget * 2);
  }

  void CheckExponents() const {
    EXPECT_GE(mlmc.GetEstimatorMap().GetExponents().GetObjAlpha(mlEstmConfig.dataKey), 0.0);
    EXPECT_GE(mlmc.GetEstimatorMap().GetExponents().GetObjBeta(mlEstmConfig.dataKey), 0.0);
//    EXPECT_GE(mlmc.GetEstimatorMap().GetExponents().GetMemoGamma(mlEstmConfig.dataKey), 0.0);
    EXPECT_GE(mlmc.GetEstimatorMap().GetExponents().GetTimeGamma(mlEstmConfig.dataKey), 0.0);

    // Check further exponents

    //    for (const auto &key: mlmc.GetEstimatorMap().GetDelQoIKeys()) {
    //      EXPECT_TRUE(mlmc.GetEstimatorMap().GetExponents().alpha.at(key) > 0) << "key=" << key;
    //      EXPECT_TRUE(mlmc.GetEstimatorMap().GetExponents().beta.at(key) > 0) << "key=" << key;
    //    }
    //    if constexpr (AGGREGATE_FOR_SOLUTION) {
    //      for (const auto &key: mlmc.GetEstimatorMap().GetRdFKeys()) {
    //        const auto &alphaOut = mlmc.GetEstimatorMap().GetExponents().alphaOuter.at(key);
    //        const auto &alphaIn = mlmc.GetEstimatorMap().GetExponents().alphaInner.at(key);
    //        const auto &betaBochner = mlmc.GetEstimatorMap().GetExponents().betaBochner.at(key);
    //        const auto &betaIn = mlmc.GetEstimatorMap().GetExponents().betaInner.at(key);
    //        const auto &betaOuter = mlmc.GetEstimatorMap().GetExponents().betaOuter.at(key);
    //
    //        // Todo consider Lemma
    //        EXPECT_TRUE(alphaOut > 0) << "key=" << key;
    //        EXPECT_TRUE(alphaIn > 0) << "key=" << key;
    //        EXPECT_TRUE(betaBochner > 0) << "key=" << key;
    //        EXPECT_TRUE(betaIn > 0) << "key=" << key;
    //        EXPECT_TRUE(betaOuter > 0) << "key=" << key;
    //      }
    //    }
  }

  template<typename T>
  bool isDecreasing(const std::vector<T> &vec) {
    for (size_t i = 1; i < vec.size(); ++i) {
      if (vec[i - 1] < vec[i]) return false;
    }
    return true;
  }

  template<typename T>
  bool isIncreasing(const std::vector<T> &vec) {
    for (size_t i = 1; i < vec.size(); ++i) {
      if (vec[i - 1] > vec[i]) return false;
    }
    return true;
  }

  bool vectorLE(const std::vector<double> vec1, const std::vector<double> vec2) {
    for (size_t i = 0; i < vec1.size(); ++i) {
      if (vec1[i] > vec2[i]) return false;
    }
    return true;
  }

  std::string moveDelPosition(const std::string &input) {
    std::regex pattern(R"(([a-zA-Z0-9_]+)\(∆([^)]+)\))");
    std::smatch match;

    if (std::regex_search(input, match, pattern)) {
      std::string identifier1 = match[1].str();
      std::string identifier2 = match[2].str();

      return "\u2206" + identifier1 + "(" + identifier2 + ")";
    }

    return input;
  }

  void CheckMultilevelData() {
    EXPECT_TRUE(isDecreasing(mlmc.GetEstimatorMap().GetOverallMaxCommSplits()));
    EXPECT_TRUE(isDecreasing(mlmc.GetEstimatorMap().GetMVector()));
    //    EXPECT_TRUE(isIncreasing(mlmc.GetEstimatorMap().GetTotalMemoCost())); TODO: Fix
    //    EXPECT_TRUE(isIncreasing(mlmc.GetEstimatorMap().GetMemoCostPerSample())); TODO: Fix
    EXPECT_TRUE(isIncreasing(mlmc.GetEstimatorMap().GetTimeCostPerSample()));

    for (const auto &key : mlmc.GetEstimatorMap().GetInKeys()) {
      const auto &meanQoIVector = mlmc.GetEstimatorMap().GetMeanQoI(key);
      const auto &sVarQoIVector = mlmc.GetEstimatorMap().GetSVarQoI(key);
      const auto &skewQoIVector = mlmc.GetEstimatorMap().GetSkewQoI(key);
      const auto &kurtQoIVector = mlmc.GetEstimatorMap().GetKurtQoI(key);
      EXPECT_EQ(meanQoIVector, mlmc.GetEstimatorMap().GetMeanInnerNormRdF(key));
      EXPECT_EQ(sVarQoIVector, mlmc.GetEstimatorMap().GetSVarInnerNormRdF(key));
      EXPECT_EQ(skewQoIVector, mlmc.GetEstimatorMap().GetSkewInnerNormRdF(key));
      EXPECT_EQ(kurtQoIVector, mlmc.GetEstimatorMap().GetKurtInnerNormRdF(key));
    }
    for (const auto &key : mlmc.GetEstimatorMap().GetDelInKeys()) {
      const auto &meanInnerNorm = mlmc.GetEstimatorMap().GetMeanInnerNormRdF(key);
      // Due to reverse triangle inequality
      EXPECT_TRUE(vectorLE(mlmc.GetEstimatorMap().GetMeanQoI(moveDelPosition(key)), meanInnerNorm));
      // Due to triangle inequality
      EXPECT_TRUE(vectorLE(mlmc.GetEstimatorMap().GetMeanOuterNormRdF(key), meanInnerNorm));
    }
  }

  void TearDown() override {
    MemoryLogger::Tare();
    PPM->Barrier(0);
  }
};

class TestMLMCCirculantEmbedding : public TestMultilevelMonteCarlo {
public:
  TestMLMCCirculantEmbedding() :
      TestMultilevelMonteCarlo(PDESolverConfig()
                                   .WithModel("LagrangeElliptic")
                                   .WithDegree(1)
                                   .WithNormsOnly(true),
                               MultiXFEMConfig("StochasticLaplace2D"), SLEstimatorConfig()) {}
};

INSTANTIATE_TEST_SUITE_P(TestMLMCCirculantEmbedding, TestMLMCCirculantEmbedding,

                         Values(MLEstimatorConfig()
                                    .WithObjective("E(L2(forward))")
                                    .WithInitSamples({128, 32, 8, 2})
                                    .WithInitLevel({3, 4, 5, 6})
                                    .WithEpsilon(0.001),

                                MLEstimatorConfig()
                                    .WithObjective("L2(E(forward))")
                                    .WithInitSamples({128, 32, 8, 2})
                                    .WithInitLevel({3, 4, 5, 6})
                                    .WithEpsilon(0.005),

                                MLEstimatorConfig()
                                    .WithObjective("E(L2(forward))")
                                    .WithInitSamples({128, 32, 8, 2})
                                    .WithInitLevel({3, 4, 5, 6})
                                    .WithMemoryBudget(6000)
                                    .WithTimeBudget(30),

                                MLEstimatorConfig()
                                    .WithObjective("L2(E(forward))")
                                    .WithInitSamples({128, 32, 8, 2})
                                    .WithInitLevel({3, 4, 5, 6})
                                    .WithMemoryBudget(6000)
                                    .WithTimeBudget(30)));

TEST_P(TestMLMCCirculantEmbedding, TestMLMCCirculantEmbedding) {
  CheckMultilevelData();
  CheckTotalErrors();
  CheckTotalCost();
  CheckExponents();
}

class TestMultilevelMonteCarloSPDE : public TestMultilevelMonteCarlo {
public:
  TestMultilevelMonteCarloSPDE() :
      TestMultilevelMonteCarlo(PDESolverConfig().WithModel("LagrangeElliptic").WithDegree(1),
                               MultiXFEMConfig("EllipticDarcyLogNormalSPDE2D"),
                               SLEstimatorConfig()) {}
};

INSTANTIATE_TEST_SUITE_P(TestMultilevelMonteCarloSPDE, TestMultilevelMonteCarloSPDE,

                         Values(MLEstimatorConfig()
                                    .WithObjective("E(L2(forward))")
                                    .WithInitSamples({128, 32, 8, 2})
                                    .WithInitLevel({3, 4, 5, 6})
                                    .WithMemoryBudget(6000)
                                    .WithTimeBudget(30),

                                MLEstimatorConfig()
                                    .WithObjective("L2(E(forward))")
                                    .WithInitSamples({128, 32, 8, 2})
                                    .WithInitLevel({3, 4, 5, 6})
                                    .WithMemoryBudget(6000)
                                    .WithTimeBudget(30)));

TEST_P(TestMultilevelMonteCarloSPDE, TestMultilevelMonteCarloSPDE) {
  CheckMultilevelData();
  CheckTotalErrors();
  CheckTotalCost();
  CheckExponents();
}

int main(int argc, char **argv) {
  return MppTest(MppTestBuilder(argc, argv)
                     .WithConfigEntry("GeneratorVerbose", 0)
                     .WithConfigEntry("PDESolverVerbose", 0)
                     .WithConfigEntry("AggregateVerbose", 0)
                     .WithConfigEntry("NewtonVerbose", 0)
                     .WithConfigEntry("LinearVerbose", 0)
                     .WithConfigEntry("ConfigVerbose", 0)
                     .WithConfigEntry("MeshVerbose", 0)
                     .WithConfigEntry("MainVerbose", 0)
                     .WithConfigEntry("NormsOnly", true)
                     .WithConfigEntry("MakeCommunicatorConsistent", 1)
                     .WithConfigEntry("Quantity", "L2")
                     .WithConfigEntry("MLEstimatorVerbose", 1)
                     .WithConfigEntry("SLEstimatorVerbose", 1)
                     .WithRandomInitialized()
                     .WithScreenLogging()
                     .WithPPM())
      .RUN_ALL_MPP_TESTS();
}

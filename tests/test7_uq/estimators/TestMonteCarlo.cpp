#include "SingleLevelEstimator.hpp"
#include "TestEnvironment.hpp"

struct Params {
  std::string problemName;

  std::string quantity;

  std::string model;

  double epsilon;

  int samples;

  int level = 3;

  bool onlyFine = false;
};

std::ostream &operator<<(std::ostream &s, const Params &testParams) {
  return s << "Problem Name: " << testParams.problemName << endl
           << "   Model: " << testParams.model << endl
           << "   Quantity: " << testParams.quantity << endl;
}

class TestMonteCarlo : public TestWithParam<Params> {
protected:
  int samples;

  double epsilon;

  PDESolverConfig pdeSolverConf;

  MultiXFEMConfig msFEMConf;

  SingleLevelEstimator mcSerial;

  SingleLevelEstimator mcParallel;

  double Tolerance(const std::string &key) const {
    return 2 * std::max(mcSerial.GetAggregate().errors.GetRMSE(key),
                        mcSerial.GetAggregate().errors.GetRMSE(key));
  }

  TestMonteCarlo() :
      samples(GetParam().samples), epsilon(GetParam().epsilon),
      pdeSolverConf(PDESolverConfig().WithDegree(1).WithModel(GetParam().model)),
      msFEMConf(MultiXFEMConfig(GetParam().problemName).WithPDESolverConfigs({pdeSolverConf})),
      mcSerial(SingleLevelEstimator(
          SLEstimatorConfig()                                                 //
              .WithObjective("\u2206L2(forward)")                             //
              .WithMultiSampleFEMConfig(msFEMConf)                            //
              .WithOnlyFine(GetParam().onlyFine)                              //
              .WithInitLevel(GetParam().level)                                //
              .WithInitSamples(samples)                                       //
              .WithEpsilon(epsilon)                                           //
              .WithParallel(false))),                                         //
      mcParallel(SingleLevelEstimator(                                        //
          SLEstimatorConfig()                                                 //
              .WithObjective("\u2206L2(forward)")                             //
              .WithMultiSampleFEMConfig(msFEMConf)                            //
              .WithOnlyFine(GetParam().onlyFine)                              //
              .WithInitLevel(GetParam().level)                                //
              .WithInitSamples(samples)                                       //
              .WithEpsilon(epsilon)                                           //
              .WithParallel(true))) {                                         //

    mout << GetParam() << endl;

    mcSerial.Method(std::nullopt);
    mout << endl;
    mcSerial.PrintInfo();

    mcParallel.Method(std::nullopt);
    mout << endl;
    mcParallel.PrintInfo();
  }

  void TestTotalErrors() const {
//    if (epsilon == 0.0) return;
//    for (const auto &[key, err]: mcSerial.GetAggregate().errors.discQoI) {
//      EXPECT_LE(err, epsilon);
//      EXPECT_LE(mcSerial.GetAggregate().errors.discQoI.at(key), epsilon);
//      EXPECT_LE(mcSerial.GetAggregate().errors.discQoI.at(key), epsilon);
//    }
  }

  void CompareData() const {
    for (const auto &key: mcParallel.GetAggregate().GetAllQoIKeys()) {
      EXPECT_NEAR(mcParallel.GetAggregate().qoI.at(key).GetMean(),
                  mcSerial.GetAggregate().qoI.at(key).GetMean(), Tolerance(key));
      EXPECT_NEAR(mcParallel.GetAggregate().qoI.at(key).GetSVar(),
                  mcSerial.GetAggregate().qoI.at(key).GetSVar(), Tolerance(key));
    }
  }

  void TearDown() override { PPM->Barrier(0); }
};

INSTANTIATE_TEST_SUITE_P(
    TestMonteCarlo, TestMonteCarlo,
    Values(Params{"StochasticLaplace2D", "L2", "LagrangeElliptic", 0.0, 100},
           Params{"StochasticLaplace2D", "L2", "LagrangeElliptic", 0.05, 40},
           Params{"StochasticLaplace2DTest", "L2", "LagrangeElliptic", 0.0, 100},
           Params{"StochasticLaplace2DTest", "L2", "LagrangeElliptic", 0.05, 40}));

TEST_P(TestMonteCarlo, TestAndCompareEstimators) {
  TestTotalErrors();
  CompareData();
}

int main(int argc, char **argv) {
  return MppTest(MppTestBuilder(argc, argv)
                     .WithConfigEntry("SLEstimatorVerbose", 1)
                     .WithConfigEntry("ParallelPlotting", 1)
                     .WithConfigEntry("PDESolverVerbose", 0)
                     .WithConfigEntry("GeneratorVerbose", 0)
                     .WithConfigEntry("AggregateVerbose", 2)
                     .WithConfigEntry("Quantity", "L2")
                     .WithConfigEntry("NewtonVerbose", 0)
                     .WithConfigEntry("LinearVerbose", 0)
                     .WithConfigEntry("ConfigVerbose", 0)
                     .WithConfigEntry("NormsOnly", true)
                     .WithConfigEntry("MeshVerbose", 0)
                     .WithConfigEntry("MainVerbose", 0)
                     .WithConfigEntry("MLMCVerbose", 0)
                     .WithConfigEntry("VtuPlot", 0)
                     .WithScreenLogging()
                     .WithRandomInitialized()
                     .WithPPM())
      .RUN_ALL_MPP_TESTS();
}
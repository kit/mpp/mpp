#include "EstimatorMap.hpp"
#include "TestEnvironment.hpp"

class TestEstimatorMap : public Test {
protected:
  MLEstimatorConfig mlConf;

  std::map<int, WelfordAggregate> aggregateMap;

  EstimatorMap estimatorMap;

  // clang-format off
  TestEstimatorMap(MLEstimatorConfig mlEstmConf = MLEstimatorConfig().WithObjective("E(test)")) :
      mlConf(mlEstmConf),
      aggregateMap(
          {{3,
            WelfordAggregate(
              SampleCounter{1600, 0, 0},
              CostInSeconds(WelfordData<double>(400, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1600, 0.0)),
              QuantityOfInterest(WelfordData<double>(2.0e-02, 64.0e-02, 64.0e-02, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1600, 0.0)),
              640000, 400, 400, mlConf.dataKey)},
           {4,
            WelfordAggregate(
              SampleCounter{400, 0, 0},
              CostInSeconds(WelfordData<double>(800, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 400, 0.0)),
              QuantityOfInterest(WelfordData<double>(1.0e-02, 16.0e-02, 16.0e-02, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 400, 0.0)),
              320000, 800, 800, mlConf.dataKey)},
           {5,
            WelfordAggregate(
              SampleCounter(100, 0, 0),
              CostInSeconds(WelfordData<double>(1600, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 100, 0.0)),
              QuantityOfInterest(WelfordData<double>(0.5e-02, 4.0e-02, 4.0e-02, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 100, 0.0)),
              160000, 1600, 1600, mlConf.dataKey)},
           {6,
            WelfordAggregate(
              SampleCounter{25, 0, 0},
              CostInSeconds(WelfordData<double>(3200, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 25, 0.0)),
              QuantityOfInterest(WelfordData<double>(0.25e-02, 1.0e-02, 1.0e-02, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 25, 0.0)),
              80000, 3200, 3200, mlConf.dataKey)}}), // clang-format on

      estimatorMap(EstimatorMap(aggregateMap, mlEstmConf)) {};

  void TearDown() override { PPM->Barrier(0); }
};

TEST_F(TestEstimatorMap, TestExponentsAndErrors) {
  estimatorMap.UpdateExponentsAndErrors();
  for (const auto &[key, _] : estimatorMap.GetExponents().alpha) {
    EXPECT_FLOAT_EQ(estimatorMap.GetExponents().alpha.at(key), 1.0);
    EXPECT_FLOAT_EQ(estimatorMap.GetExponents().beta.at(key), 2.0);
    EXPECT_FLOAT_EQ(estimatorMap.GetErrors().num.at(key), std::pow(0.25e-2, 2));
    EXPECT_FLOAT_EQ(estimatorMap.GetErrors().sam.at(key), 1.6e-3);
  }
  EXPECT_FLOAT_EQ(estimatorMap.GetExponents().gammaCT, 1.0);
  EXPECT_FLOAT_EQ(estimatorMap.GetExponents().gammaMem, 1.0);
}

TEST_F(TestEstimatorMap, TestMonteCarloMapUpdateSampleCounter) {
  estimatorMap.UpdateSampleCounterOnMap(0.01, 0.5);
  for (auto &[level, estimator] : estimatorMap) {
    EXPECT_TRUE(estimator.SamplesToCompute() != 0);
    EXPECT_EQ(estimator.GetAggregate().ctr.M, aggregateMap.find(level)->second.ctr.M);
    EXPECT_EQ(estimator.MaxSamplesByMem(), std::numeric_limits<int>::max());
  }
}

class TestEstimatorMapEnoughBudget : public TestEstimatorMap {
protected:
  TestEstimatorMapEnoughBudget() :
      TestEstimatorMap(MLEstimatorConfig()
                           .WithTimeBudget(1000000000)
                           .WithMemoryBudget(6400)
                           .WithObjective("\u2206test")) {};
};

TEST_F(TestEstimatorMapEnoughBudget, TestMonteCarloMapAppendLevel) {
  const int newLevel = 7;
  estimatorMap.UpdateExponentsAndErrors();
  EXPECT_TRUE(estimatorMap.UpdateNewLevel(0.1, 0.5, newLevel));
  EXPECT_FLOAT_EQ(estimatorMap.find(newLevel)->second.MaxSamplesByMem(), 1);
  EXPECT_FLOAT_EQ(estimatorMap.find(newLevel)->second.SamplesToCompute(), 6); // floor(25 / 4)
}

class TestEstimatorMapNotEnoughTimeBudget : public TestEstimatorMap {
protected:
  TestEstimatorMapNotEnoughTimeBudget() :
      TestEstimatorMap(MLEstimatorConfig().WithTimeBudget(1).WithObjective("\u2206test")) {};
};

TEST_F(TestEstimatorMapNotEnoughTimeBudget, TestMonteCarloMapAppendLevel) {
  int newLevel = 7;
  estimatorMap.UpdateExponentsAndErrors();
  EXPECT_FALSE(estimatorMap.UpdateNewLevel(0.1, 0.5, newLevel));
}

class TestEstimatorMapNotEnoughMemoryBudget : public TestEstimatorMap {
protected:
  TestEstimatorMapNotEnoughMemoryBudget() :
      TestEstimatorMap(MLEstimatorConfig()
                           .WithTimeBudget(1000000000)
                           .WithMemoryBudget(1)
                           .WithObjective("\u2206test")) {};
};

TEST_F(TestEstimatorMapNotEnoughMemoryBudget, TestMonteCarloMapAppendLevel) {
  int newLevel = 7;
  estimatorMap.UpdateExponentsAndErrors();
  EXPECT_FALSE(estimatorMap.UpdateNewLevel(0.1, 0.5, newLevel));
}

int main(int argc, char **argv) {
  return MppTest(MppTestBuilder(argc, argv).WithScreenLogging().WithPPM()).RUN_ALL_MPP_TESTS();
}
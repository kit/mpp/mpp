#include "TestEnvironment.hpp"

#include <unistd.h>
#include "LagrangeDiscretization.hpp"
#include "MeshesCreator.hpp"
#include "MultiXFEM.hpp"
#include "PDESolver.hpp"
#include "SingleLevelEstimator.hpp"


const double TOL_MEAN = 4.0;

const double T_SVAR = 8.0;

const double T_SKEW = 32.0;

const double T_KURT = 128.0;

const double T_FIELD = 256.0;

class TestSLEstimator : public TestWithParam<int> {
protected:
  SingleLevelEstimator estimator;

  explicit TestSLEstimator(SLEstimatorConfig slEstmConfig) :
      estimator(SingleLevelEstimator(slEstmConfig.WithInitSamples(GetParam()))) {}

  void TestSVarExample1() {
    pout << "Before 1. Run: " << estimator.GetAggregate();
    estimator.Method(std::nullopt);
    pout << "After 1. Run: " << estimator.GetAggregate();

    double referenceMean = 0.0;
    double referenceSVar = 0.0;
    for (int i = 0; i < GetParam(); i++) {
      if (i % 2) referenceSVar += pow(1.0 - referenceMean, 2);
      else referenceSVar += pow(-1.0 - referenceMean, 2);
    }
    referenceSVar = referenceSVar / (GetParam() - 1);

    EXPECT_EQ(estimator.GetAggregate().qoI.at("ModuloIndexEvaluationMXFEM").GetSVar(),
              referenceSVar);
    EXPECT_EQ(estimator.GetAggregate().qoI.at("ModuloIndexEvaluationMXFEM").GetMean(),
              referenceMean);
  }

  int ReferenceIndex(int start, int samples) {
    if (samples >= PPM->Size(0)) return start + (estimator.SamplesToComputeOnComm()) * PPM->Proc(0);
    else return start + PPM->Color(estimator.CommSplit());
  }

  void TestIndices(int start, int samples) {
    int referenceIndex = ReferenceIndex(start, samples);
    pout << "Before 1. Run: " << DOUT(estimator.Index()) << endl;
    EXPECT_EQ(estimator.Index(), referenceIndex);
    estimator.Method(std::nullopt);
    pout << "After 1. Run: " << DOUT(estimator.Index()) << endl;
  }

  void TearDown() override { PPM->Barrier(0); }
};

class TestSLEstmProcEvaluation : public TestSLEstimator {
public:
  TestSLEstmProcEvaluation() :
      TestSLEstimator(SLEstimatorConfig().WithInitLevel(2).WithMultiSampleFEMConfig(
          MultiXFEMConfig("ProcEvaluationMXFEM")
              .WithPDESolverConfigs({PDESolverConfig().WithDegree(1)}))) {}
};

TEST_P(TestSLEstmProcEvaluation, TestInstanciation) {
  switch (GetParam()) {
  case 4:
    switch (PPM->Size(0)) {
    case 8:
      pout << DOUT(estimator.GetAggregate().ctr) << DOUT(estimator.CommSplit()) << endl;
      EXPECT_EQ(estimator.SamplesToComputeOnComm(), 1);
      EXPECT_EQ(estimator.CommSplit(), 2);
      break;
    case 4:
      pout << DOUT(estimator.GetAggregate().ctr) << DOUT(estimator.CommSplit()) << endl;
      EXPECT_EQ(estimator.SamplesToComputeOnComm(), 1);
      EXPECT_EQ(estimator.CommSplit(), 2);
      break;
    case 2:
      pout << DOUT(estimator.GetAggregate().ctr) << DOUT(estimator.CommSplit()) << endl;
      EXPECT_EQ(estimator.SamplesToComputeOnComm(), 2);
      EXPECT_EQ(estimator.CommSplit(), 1);
      break;
    case 1:
      pout << DOUT(estimator.GetAggregate().ctr) << DOUT(estimator.CommSplit()) << endl;
      EXPECT_EQ(estimator.SamplesToComputeOnComm(), 4);
      EXPECT_EQ(estimator.CommSplit(), 0);
      break;
    default:
      break;
    }
    break;
  case 8:
    switch (PPM->Size(0)) {
    case 8:
      pout << DOUT(estimator.GetAggregate().ctr) << DOUT(estimator.CommSplit()) << endl;
      EXPECT_EQ(estimator.SamplesToComputeOnComm(), 1);
      EXPECT_EQ(estimator.CommSplit(), 3);
      break;
    case 4:
      pout << DOUT(estimator.GetAggregate().ctr) << DOUT(estimator.CommSplit()) << endl;
      EXPECT_EQ(estimator.SamplesToComputeOnComm(), 2);
      EXPECT_EQ(estimator.CommSplit(), 2);
      break;
    case 2:
      pout << DOUT(estimator.GetAggregate().ctr) << DOUT(estimator.CommSplit()) << endl;
      EXPECT_EQ(estimator.SamplesToComputeOnComm(), 4);
      EXPECT_EQ(estimator.CommSplit(), 1);
      break;
    case 1:
      pout << DOUT(estimator.GetAggregate().ctr) << DOUT(estimator.CommSplit()) << endl;
      EXPECT_EQ(estimator.SamplesToComputeOnComm(), 8);
      EXPECT_EQ(estimator.CommSplit(), 0);
      break;
    default:
      break;
    }
    break;
  case 16:
    switch (PPM->Size(0)) {
    case 8:
      pout << DOUT(estimator.GetAggregate().ctr) << DOUT(estimator.CommSplit()) << endl;
      EXPECT_EQ(estimator.SamplesToComputeOnComm(), 2);
      EXPECT_EQ(estimator.CommSplit(), 3);
      break;
    case 4:
      pout << DOUT(estimator.GetAggregate().ctr) << DOUT(estimator.CommSplit()) << endl;
      EXPECT_EQ(estimator.SamplesToComputeOnComm(), 4);
      EXPECT_EQ(estimator.CommSplit(), 2);
      break;
    case 2:
      pout << DOUT(estimator.GetAggregate().ctr) << DOUT(estimator.CommSplit()) << endl;
      EXPECT_EQ(estimator.SamplesToComputeOnComm(), 8);
      EXPECT_EQ(estimator.CommSplit(), 1);
      break;
    case 1:
      pout << DOUT(estimator.GetAggregate().ctr) << DOUT(estimator.CommSplit()) << endl;
      EXPECT_EQ(estimator.SamplesToComputeOnComm(), 16);
      EXPECT_EQ(estimator.CommSplit(), 0);
      break;
    default:
      break;
    }
    break;
  case 1000:
    switch (PPM->Size(0)) {
    case 8:
      pout << DOUT(estimator.GetAggregate().ctr) << DOUT(estimator.CommSplit()) << endl;
      EXPECT_EQ(estimator.SamplesToComputeOnComm(), 125);
      EXPECT_EQ(estimator.CommSplit(), 3);
      break;
    case 4:
      pout << DOUT(estimator.GetAggregate().ctr) << DOUT(estimator.CommSplit()) << endl;
      EXPECT_EQ(estimator.SamplesToComputeOnComm(), 250);
      EXPECT_EQ(estimator.CommSplit(), 2);
      break;
    case 2:
      pout << DOUT(estimator.GetAggregate().ctr) << DOUT(estimator.CommSplit()) << endl;
      EXPECT_EQ(estimator.SamplesToComputeOnComm(), 500);
      EXPECT_EQ(estimator.CommSplit(), 1);
      break;
    case 1:
      pout << DOUT(estimator.GetAggregate().ctr) << DOUT(estimator.CommSplit()) << endl;
      EXPECT_EQ(estimator.SamplesToComputeOnComm(), 1000);
      EXPECT_EQ(estimator.CommSplit(), 0);
      break;
    default:
      break;
    }
    break;
  }
}

TEST_P(TestSLEstmProcEvaluation, TestMean) {
  if (PPM->Size(0) == 8 && GetParam() == 4) { return; } // The test is not meaningful for this case?
  if (PPM->Size(0) == 16 && GetParam() == 4) { return; } // ...
  if (PPM->Size(0) == 16 && GetParam() == 8) { return; } // ...

  estimator.Method(std::nullopt);
  pout << "After 1. Run: " << estimator.GetAggregate().qoI.at("ProcEvaluationMXFEM").GetTotal();

  double sumGauss = (pow((PPM->Size(0) - 1), 2) + (PPM->Size(0) - 1)) / 2.0;
  double referenceMean = sumGauss / PPM->Size(0);

  EXPECT_EQ(estimator.GetAggregate().qoI.at("ProcEvaluationMXFEM").GetMean(), referenceMean);

  int additionalSamples = PPM->Size(0);
  estimator.UpdateSampleAmount(additionalSamples);

  pout << "Before 2. Run: " << estimator.GetAggregate().qoI.at("ProcEvaluationMXFEM").GetMean();
  estimator.Method(std::nullopt);


  pout << "After 2. Run: " << estimator.GetAggregate().qoI.at("ProcEvaluationMXFEM").GetMean();

  EXPECT_EQ(estimator.GetAggregate().qoI.at("ProcEvaluationMXFEM").GetMean(), referenceMean);
}

class TestSLEstmZeroEvaluation : public TestSLEstimator {
public:
  TestSLEstmZeroEvaluation() :
      TestSLEstimator(SLEstimatorConfig().WithInitLevel(3).WithMultiSampleFEMConfig(
          MultiXFEMConfig("ZeroEvaluationMXFEM")
              .WithPDESolverConfigs({PDESolverConfig().WithDegree(1)}))) {}
};

TEST_P(TestSLEstmZeroEvaluation, TestIndex) {
  int totalSamples = GetParam();
  pout << "Start with " << GetParam() << " samples" << endl;
  TestIndices(0, GetParam());
  EXPECT_EQ(totalSamples, estimator.GetAggregate().ctr.M);

  int additionalSamples = 4;
  totalSamples += additionalSamples;
  estimator.UpdateSampleAmount(additionalSamples);
  pout << "Add " << additionalSamples << " more samples" << endl;
  TestIndices(estimator.GetAggregate().ctr.M, additionalSamples);
  EXPECT_EQ(totalSamples, estimator.GetAggregate().ctr.M);

  additionalSamples = 8;
  totalSamples += additionalSamples;
  estimator.UpdateSampleAmount(additionalSamples);
  pout << "Add " << additionalSamples << " more samples" << endl;
  TestIndices(estimator.GetAggregate().ctr.M, additionalSamples);
  EXPECT_EQ(totalSamples, estimator.GetAggregate().ctr.M);

  additionalSamples = 16;
  totalSamples += additionalSamples;
  estimator.UpdateSampleAmount(additionalSamples);
  pout << "Add " << additionalSamples << " more samples" << endl;
  TestIndices(estimator.GetAggregate().ctr.M, additionalSamples);
  EXPECT_EQ(totalSamples, estimator.GetAggregate().ctr.M);
  mout << endl;
}

TEST_P(TestSLEstmZeroEvaluation, TestSampleCounter) {
  pout << "Before: " << estimator.GetAggregate().ctr;
  estimator.Method(std::nullopt);
  pout << "After: " << estimator.GetAggregate().ctr;
  EXPECT_EQ(estimator.GetAggregate().ctr.M, GetParam());
  mout << endl;
}

class TestSLEstmModuloIndexEvaluation : public TestSLEstimator {
public:
  TestSLEstmModuloIndexEvaluation() :
      TestSLEstimator(SLEstimatorConfig().WithInitLevel(2).WithMultiSampleFEMConfig(
          MultiXFEMConfig("ModuloIndexEvaluationMXFEM")
              .WithPDESolverConfigs({PDESolverConfig().WithDegree(1)}))) {}
};

TEST_P(TestSLEstmModuloIndexEvaluation, TestSVar) {
  // clang-format off
  switch (GetParam()) {
    case 4:
      switch (PPM->Size(0)) {
        case 2:
          TestSVarExample1();
          break;
        case 1:
          TestSVarExample1();
          break;
        default:
          break;
      }
      break;
    case 8:
      switch (PPM->Size(0)) {
        case 4:
          TestSVarExample1();
          break;
        case 2:
          TestSVarExample1();
          break;
        default:
          break;
      }
      break;
    case 16:
      switch (PPM->Size(0)) {
        case 8:
          TestSVarExample1();
          break;
        case 4:
          TestSVarExample1();
          break;
        default:
          break;
      }
      break;
    default:
      break;
  }
  // clang-format on
}

class TestSLEstmNormalRVEvaluation : public TestSLEstimator {
public:
  TestSLEstmNormalRVEvaluation() :
      TestSLEstimator(SLEstimatorConfig()
                          .WithDelayTotalUpdate(true)
                          .WithDelayParallelUpdate(true)
                          .WithInitLevel(2)
                          .WithMultiSampleFEMConfig(
                              MultiXFEMConfig("NormalLagrangeMXFEM")
                                  .WithPDESolverConfigs({PDESolverConfig().WithDegree(1)}))) {}
};

TEST_P(TestSLEstmNormalRVEvaluation, TestUpdate) {
  const auto &a = estimator.GetAggregate();
  for (auto &samples : {128, 256, 512, 1024, 2048, 4096, 8192}) {
    pout << estimator.GetAggregate() << endl;
    MemoryLogger::LogMemory();
    estimator.UpdateSampleAmount(samples);
    estimator.Method(std::nullopt);
    pout << endl << estimator.GetAggregate() << endl;

    EXPECT_NEAR(0.0, a.qoI.at("Q").GetComm().mean, TOL_MEAN * sqrt(1.0 / a.ctr.Mcomm));
    EXPECT_NEAR(1.0, a.qoI.at("Q").GetComm().sVar, T_SVAR * sqrt(1.0 / a.ctr.Mcomm));
    EXPECT_NEAR(0.0, a.qoI.at("Q").GetComm().skew, T_SKEW * sqrt(1.0 / a.ctr.Mcomm));
    EXPECT_NEAR(3.0, a.qoI.at("Q").GetComm().kurt, T_KURT * sqrt(1.0 / a.ctr.Mcomm));
    EXPECT_DOUBLE_EQ(a.qoI.at("Q").GetComm().boch, a.qoI.at("Q").GetComm().sVar);
    EXPECT_DOUBLE_EQ(a.qoI.at("Q").GetComm().boch, a.qoI.at("\u2206Q").GetComm().boch);
    EXPECT_DOUBLE_EQ(a.qoI.at("Q").GetComm().mean, a.qoI.at("\u2206Q").GetComm().mean);
    EXPECT_DOUBLE_EQ(a.qoI.at("Q").GetComm().sVar, a.qoI.at("\u2206Q").GetComm().sVar);
    EXPECT_DOUBLE_EQ(a.qoI.at("Q").GetComm().skew, a.qoI.at("\u2206Q").GetComm().skew);
    EXPECT_DOUBLE_EQ(a.qoI.at("Q").GetComm().kurt, a.qoI.at("\u2206Q").GetComm().kurt);
#ifdef AGGREGATE_FOR_SOLUTION
    const auto &vectorComm = a.rdF.at("u").GetComm().mean;
    const auto &delVectorComm = a.rdF.at("\u2206u").GetComm().mean;
    for (row r = vectorComm.rows(); r != vectorComm.rows_end(); r++) {
      EXPECT_NEAR(-r()[1], a.rdF.at("u").GetComm().mean(r, 0), TOL_MEAN * sqrt(1.0 / a.ctr.Mcomm));
      EXPECT_NEAR(1.0, a.rdF.at("u").GetComm().sVar(r, 0), T_SVAR * sqrt(1.0 / a.ctr.Mcomm));
      EXPECT_NEAR(0.0, a.rdF.at("u").GetComm().skew(r, 0), T_SKEW * sqrt(1.0 / a.ctr.Mcomm));
      EXPECT_NEAR(3.0, a.rdF.at("u").GetComm().kurt(r, 0), T_KURT * sqrt(1.0 / a.ctr.Mcomm));
      EXPECT_NEAR(-r()[1], delVectorComm(r, 0), TOL_MEAN * sqrt(1.0 / a.ctr.Mcomm));
      EXPECT_NEAR(1.0, a.rdF.at("\u2206u").GetComm().sVar(r, 0), T_SVAR * sqrt(1.0 / a.ctr.Mcomm));
      EXPECT_NEAR(0.0, a.rdF.at("\u2206u").GetComm().skew(r, 0), T_SKEW * sqrt(1.0 / a.ctr.Mcomm));
      EXPECT_NEAR(3.0, a.rdF.at("\u2206u").GetComm().kurt(r, 0), T_KURT * sqrt(1.0 / a.ctr.Mcomm));
    }
#endif
    mout << "CALLING UpdateParallel()" << endl << endl;
    estimator.UpdateParallel();
    pout << estimator.GetAggregate() << endl;
    EXPECT_NEAR(0.0, a.qoI.at("Q").GetPara().mean, TOL_MEAN * sqrt(1.0 / a.ctr.Mpara));
    EXPECT_NEAR(1.0, a.qoI.at("Q").GetPara().sVar, T_SVAR * sqrt(1.0 / a.ctr.Mpara));
    EXPECT_NEAR(0.0, a.qoI.at("Q").GetPara().skew, T_SKEW * sqrt(1.0 / a.ctr.Mpara));
    EXPECT_NEAR(3.0, a.qoI.at("Q").GetPara().kurt, T_KURT * sqrt(1.0 / a.ctr.Mpara));
    EXPECT_DOUBLE_EQ(a.qoI.at("Q").GetPara().boch, a.qoI.at("Q").GetPara().sVar);
    EXPECT_DOUBLE_EQ(a.qoI.at("Q").GetPara().boch, a.qoI.at("\u2206Q").GetPara().boch);
    EXPECT_DOUBLE_EQ(a.qoI.at("Q").GetPara().mean, a.qoI.at("\u2206Q").GetPara().mean);
    EXPECT_DOUBLE_EQ(a.qoI.at("Q").GetPara().sVar, a.qoI.at("\u2206Q").GetPara().sVar);
    EXPECT_DOUBLE_EQ(a.qoI.at("Q").GetPara().skew, a.qoI.at("\u2206Q").GetPara().skew);
    EXPECT_DOUBLE_EQ(a.qoI.at("Q").GetPara().kurt, a.qoI.at("\u2206Q").GetPara().kurt);
#ifdef AGGREGATE_FOR_SOLUTION
    const auto &vectorPara = a.rdF.at("u").GetPara().mean;
    const auto &delVectorPara = a.rdF.at("\u2206u").GetPara().mean;
    for (row r = vectorPara.rows(); r != vectorPara.rows_end(); r++) {
      EXPECT_NEAR(-r()[1], a.rdF.at("u").GetPara().mean(r, 0), TOL_MEAN * sqrt(1.0 / a.ctr.Mpara));
      EXPECT_NEAR(1.0, a.rdF.at("u").GetPara().sVar(r, 0), T_SVAR * sqrt(1.0 / a.ctr.Mpara));
      EXPECT_NEAR(0.0, a.rdF.at("u").GetPara().skew(r, 0), T_SKEW * sqrt(1.0 / a.ctr.Mpara));
      EXPECT_NEAR(3.0, a.rdF.at("u").GetPara().kurt(r, 0), T_FIELD * sqrt(1.0 / a.ctr.Mpara));
      EXPECT_NEAR(-r()[1], delVectorPara(r, 0), TOL_MEAN * sqrt(1.0 / a.ctr.Mpara));
      EXPECT_NEAR(1.0, a.rdF.at("\u2206u").GetPara().sVar(r, 0), T_SVAR * sqrt(1.0 / a.ctr.Mpara));
      EXPECT_NEAR(0.0, a.rdF.at("\u2206u").GetPara().skew(r, 0), T_SKEW * sqrt(1.0 / a.ctr.Mpara));
      EXPECT_NEAR(3.0, a.rdF.at("\u2206u").GetPara().kurt(r, 0), T_FIELD * sqrt(1.0 / a.ctr.Mpara));
    }
#endif
    mout << "CALLING UpdateTotal()" << endl << endl;
    estimator.UpdateTotal();
    pout << estimator.GetAggregate() << endl;
    EXPECT_NEAR(0.0, a.qoI.at("Q").GetTotal().mean, TOL_MEAN * sqrt(1.0 / a.ctr.M));
    EXPECT_NEAR(1.0, a.qoI.at("Q").GetTotal().sVar, T_SVAR * sqrt(1.0 / a.ctr.M));
    EXPECT_NEAR(0.0, a.qoI.at("Q").GetTotal().skew, T_SKEW * sqrt(1.0 / a.ctr.M));
    EXPECT_NEAR(3.0, a.qoI.at("Q").GetTotal().kurt, T_KURT * sqrt(1.0 / a.ctr.M));
    EXPECT_DOUBLE_EQ(a.qoI.at("Q").GetTotal().boch, a.qoI.at("Q").GetTotal().sVar);
    EXPECT_DOUBLE_EQ(a.qoI.at("Q").GetTotal().boch, a.qoI.at("\u2206Q").GetTotal().boch);
    EXPECT_DOUBLE_EQ(a.qoI.at("Q").GetTotal().mean, a.qoI.at("\u2206Q").GetTotal().mean);
    EXPECT_DOUBLE_EQ(a.qoI.at("Q").GetTotal().sVar, a.qoI.at("\u2206Q").GetTotal().sVar);
    EXPECT_DOUBLE_EQ(a.qoI.at("Q").GetTotal().skew, a.qoI.at("\u2206Q").GetTotal().skew);
    EXPECT_DOUBLE_EQ(a.qoI.at("Q").GetTotal().kurt, a.qoI.at("\u2206Q").GetTotal().kurt);
#ifdef AGGREGATE_FOR_SOLUTION
    const auto &vectorTotal = a.rdF.at("u").GetTotal().mean;
    const auto &delVectorTotal = a.rdF.at("\u2206u").GetTotal().mean;
    for (row r = vectorTotal.rows(); r != vectorTotal.rows_end(); r++) {
      EXPECT_NEAR(-r()[1], a.rdF.at("u").GetTotal().mean(r, 0), TOL_MEAN * sqrt(1.0 / a.ctr.M));
      EXPECT_NEAR(1.0, a.rdF.at("u").GetTotal().sVar(r, 0), T_SVAR * sqrt(1.0 / a.ctr.M));
      EXPECT_NEAR(0.0, a.rdF.at("u").GetTotal().skew(r, 0), T_SKEW * sqrt(1.0 / a.ctr.M));
      EXPECT_NEAR(3.0, a.rdF.at("u").GetTotal().kurt(r, 0), T_FIELD * sqrt(1.0 / a.ctr.M));
      EXPECT_NEAR(-r()[1], delVectorTotal(r, 0), TOL_MEAN * sqrt(1.0 / a.ctr.M));
      EXPECT_NEAR(1.0, a.rdF.at("\u2206u").GetTotal().sVar(r, 0), T_SVAR * sqrt(1.0 / a.ctr.M));
      EXPECT_NEAR(0.0, a.rdF.at("\u2206u").GetTotal().skew(r, 0), T_SKEW * sqrt(1.0 / a.ctr.M));
      EXPECT_NEAR(3.0, a.rdF.at("\u2206u").GetTotal().kurt(r, 0), T_FIELD * sqrt(1.0 / a.ctr.M));
    }
#endif
  }
}

INSTANTIATE_TEST_SUITE_P(TestSLEstimator, TestSLEstmProcEvaluation, Values(4, 8, 16, 32));

INSTANTIATE_TEST_SUITE_P(TestSLEstimator, TestSLEstmModuloIndexEvaluation, Values(4, 8, 16, 32));

INSTANTIATE_TEST_SUITE_P(TestSLEstimator, TestSLEstmZeroEvaluation, Values(4, 8, 16, 32));

INSTANTIATE_TEST_SUITE_P(TestSLEstimator, TestSLEstmNormalRVEvaluation, Values(0));

class TestSLEstimatorNormalDG : public Test {
protected:
  SingleLevelEstimator estimator;
public:
  TestSLEstimatorNormalDG() :
      estimator(SingleLevelEstimator(
          SLEstimatorConfig()
              .WithInitSamples(8)
              .WithDelayTotalUpdate(true)
              .WithDelayParallelUpdate(true)
              .WithInitLevel(6)
              .WithMultiSampleFEMConfig(
                  MultiXFEMConfig("NormalDGMXFEM")
                      .WithPDESolverConfigs({PDESolverConfig().WithDegree(1)})))) {}
};

TEST_F(TestSLEstimatorNormalDG, TestUpdate) {
  auto &a = estimator.GetAggregate();
  estimator.Method(std::nullopt);
  pout << endl << estimator.GetAggregate() << endl;

  EXPECT_NEAR(0.0, a.qoI.at("Q").GetComm().mean, TOL_MEAN * sqrt(1.0 / a.ctr.Mcomm));
  EXPECT_NEAR(1.0, a.qoI.at("Q").GetComm().sVar, T_SVAR * sqrt(1.0 / a.ctr.Mcomm));
  EXPECT_NEAR(0.0, a.qoI.at("Q").GetComm().skew, T_SKEW * sqrt(1.0 / a.ctr.Mcomm));
  EXPECT_NEAR(3.0, a.qoI.at("Q").GetComm().kurt, T_KURT * sqrt(1.0 / a.ctr.Mcomm));
  EXPECT_DOUBLE_EQ(a.qoI.at("Q").GetComm().boch, a.qoI.at("Q").GetComm().sVar);
  EXPECT_DOUBLE_EQ(a.qoI.at("Q").GetComm().boch, a.qoI.at("\u2206Q").GetComm().boch);
  EXPECT_DOUBLE_EQ(a.qoI.at("Q").GetComm().mean, a.qoI.at("\u2206Q").GetComm().mean);
  EXPECT_DOUBLE_EQ(a.qoI.at("Q").GetComm().sVar, a.qoI.at("\u2206Q").GetComm().sVar);
  EXPECT_DOUBLE_EQ(a.qoI.at("Q").GetComm().skew, a.qoI.at("\u2206Q").GetComm().skew);
  EXPECT_DOUBLE_EQ(a.qoI.at("Q").GetComm().kurt, a.qoI.at("\u2206Q").GetComm().kurt);
#ifdef AGGREGATE_FOR_SOLUTION
  const auto &vectorComm = a.rdF.at("u").GetComm().mean;
  const auto &delVectorComm = a.rdF.at("\u2206u").GetComm().mean;
  for (row r = vectorComm.rows(); r != vectorComm.rows_end(); r++) {
    EXPECT_NEAR(-r()[1], vectorComm(r, 0), TOL_MEAN * sqrt(1.0 / a.ctr.Mcomm));
    EXPECT_NEAR(1.0, a.rdF.at("u").GetComm().sVar(r, 0), T_SVAR * sqrt(1.0 / a.ctr.Mcomm));
    EXPECT_NEAR(0.0, a.rdF.at("u").GetComm().skew(r, 0), T_SKEW * sqrt(1.0 / a.ctr.Mcomm));
    EXPECT_NEAR(3.0, a.rdF.at("u").GetComm().kurt(r, 0), T_KURT * sqrt(1.0 / a.ctr.Mcomm));
    EXPECT_NEAR(-r()[1], delVectorComm(r, 0), TOL_MEAN * sqrt(1.0 / a.ctr.Mcomm));
    EXPECT_NEAR(1.0, a.rdF.at("\u2206u").GetComm().sVar(r, 0), T_SVAR * sqrt(1.0 / a.ctr.Mcomm));
    EXPECT_NEAR(0.0, a.rdF.at("\u2206u").GetComm().skew(r, 0), T_SKEW * sqrt(1.0 / a.ctr.Mcomm));
    EXPECT_NEAR(3.0, a.rdF.at("\u2206u").GetComm().kurt(r, 0), T_KURT * sqrt(1.0 / a.ctr.Mcomm));
  }
#endif
  mout << "CALLING UpdateParallel()" << endl << endl;
  estimator.UpdateParallel();
  pout << estimator.GetAggregate() << endl;
  EXPECT_NEAR(0.0, a.qoI.at("Q").GetPara().mean, TOL_MEAN * sqrt(1.0 / a.ctr.Mpara));
  EXPECT_NEAR(1.0, a.qoI.at("Q").GetPara().sVar, T_SVAR * sqrt(1.0 / a.ctr.Mpara));
  EXPECT_NEAR(0.0, a.qoI.at("Q").GetPara().skew, T_SKEW * sqrt(1.0 / a.ctr.Mpara));
  EXPECT_NEAR(3.0, a.qoI.at("Q").GetPara().kurt, T_KURT * sqrt(1.0 / a.ctr.Mpara));
  EXPECT_DOUBLE_EQ(a.qoI.at("Q").GetPara().boch, a.qoI.at("Q").GetPara().sVar);
  EXPECT_DOUBLE_EQ(a.qoI.at("Q").GetPara().boch, a.qoI.at("\u2206Q").GetPara().boch);
  EXPECT_DOUBLE_EQ(a.qoI.at("Q").GetPara().mean, a.qoI.at("\u2206Q").GetPara().mean);
  EXPECT_DOUBLE_EQ(a.qoI.at("Q").GetPara().sVar, a.qoI.at("\u2206Q").GetPara().sVar);
  EXPECT_DOUBLE_EQ(a.qoI.at("Q").GetPara().skew, a.qoI.at("\u2206Q").GetPara().skew);
  EXPECT_DOUBLE_EQ(a.qoI.at("Q").GetPara().kurt, a.qoI.at("\u2206Q").GetPara().kurt);
#ifdef AGGREGATE_FOR_SOLUTION
  const auto &vectorPara = a.rdF.at("u").GetPara().mean;
  const auto &delVectorPara = a.rdF.at("\u2206u").GetPara().mean;
  for (row r = vectorPara.rows(); r != vectorPara.rows_end(); r++) {
    EXPECT_NEAR(-r()[1], vectorPara(r, 0), TOL_MEAN * sqrt(1.0 / a.ctr.Mpara));
    EXPECT_NEAR(1.0, a.rdF.at("u").GetPara().sVar(r, 0), T_SVAR * sqrt(1.0 / a.ctr.Mpara));
    EXPECT_NEAR(0.0, a.rdF.at("u").GetPara().skew(r, 0), T_SKEW * sqrt(1.0 / a.ctr.Mpara));
    EXPECT_NEAR(3.0, a.rdF.at("u").GetPara().kurt(r, 0), T_KURT * sqrt(1.0 / a.ctr.Mpara));
    EXPECT_NEAR(-r()[1], delVectorPara(r, 0), TOL_MEAN * sqrt(1.0 / a.ctr.Mpara));
    EXPECT_NEAR(1.0, a.rdF.at("\u2206u").GetPara().sVar(r, 0), T_SVAR * sqrt(1.0 / a.ctr.Mpara));
    EXPECT_NEAR(0.0, a.rdF.at("\u2206u").GetPara().skew(r, 0), T_SKEW * sqrt(1.0 / a.ctr.Mpara));
    EXPECT_NEAR(3.0, a.rdF.at("\u2206u").GetPara().kurt(r, 0), T_KURT * sqrt(1.0 / a.ctr.Mpara));
  }
#endif
  mout << "CALLING UpdateTotal()" << endl << endl;
  estimator.UpdateTotal();
  pout << estimator.GetAggregate() << endl;
  EXPECT_NEAR(0.0, a.qoI.at("Q").GetTotal().mean, TOL_MEAN * sqrt(1.0 / a.ctr.M));
  EXPECT_NEAR(1.0, a.qoI.at("Q").GetTotal().sVar, T_SVAR * sqrt(1.0 / a.ctr.M));
  EXPECT_NEAR(0.0, a.qoI.at("Q").GetTotal().skew, T_SKEW * sqrt(1.0 / a.ctr.M));
  EXPECT_NEAR(3.0, a.qoI.at("Q").GetTotal().kurt, T_KURT * sqrt(1.0 / a.ctr.M));
  EXPECT_DOUBLE_EQ(a.qoI.at("Q").GetTotal().boch, a.qoI.at("Q").GetTotal().sVar);
  EXPECT_DOUBLE_EQ(a.qoI.at("Q").GetTotal().boch, a.qoI.at("\u2206Q").GetTotal().boch);
  EXPECT_DOUBLE_EQ(a.qoI.at("Q").GetTotal().mean, a.qoI.at("\u2206Q").GetTotal().mean);
  EXPECT_DOUBLE_EQ(a.qoI.at("Q").GetTotal().sVar, a.qoI.at("\u2206Q").GetTotal().sVar);
  EXPECT_DOUBLE_EQ(a.qoI.at("Q").GetTotal().skew, a.qoI.at("\u2206Q").GetTotal().skew);
  EXPECT_DOUBLE_EQ(a.qoI.at("Q").GetTotal().kurt, a.qoI.at("\u2206Q").GetTotal().kurt);
#ifdef AGGREGATE_FOR_SOLUTION
  const auto &vectorTotal = a.rdF.at("u").GetTotal().mean;
  const auto &delVectorTotal = a.rdF.at("\u2206u").GetTotal().mean;
  for (row r = vectorTotal.rows(); r != vectorTotal.rows_end(); r++) {
    EXPECT_NEAR(-r()[1], a.rdF.at("u").GetTotal().mean(r, 0), TOL_MEAN * sqrt(1.0 / a.ctr.M));
    EXPECT_NEAR(1.0, a.rdF.at("u").GetTotal().sVar(r, 0), T_SVAR * sqrt(1.0 / a.ctr.M));
    EXPECT_NEAR(0.0, a.rdF.at("u").GetTotal().skew(r, 0), T_SKEW * sqrt(1.0 / a.ctr.M));
    EXPECT_NEAR(3.0, a.rdF.at("u").GetTotal().kurt(r, 0), T_KURT * sqrt(1.0 / a.ctr.M));
    EXPECT_NEAR(-r()[1], delVectorTotal(r, 0), TOL_MEAN * sqrt(1.0 / a.ctr.M));
    EXPECT_NEAR(1.0, a.rdF.at("\u2206u").GetTotal().sVar(r, 0), T_SVAR * sqrt(1.0 / a.ctr.M));
    EXPECT_NEAR(0.0, a.rdF.at("\u2206u").GetTotal().skew(r, 0), T_SKEW * sqrt(1.0 / a.ctr.M));
    EXPECT_NEAR(3.0, a.rdF.at("\u2206u").GetTotal().kurt(r, 0), T_KURT * sqrt(1.0 / a.ctr.M));
  }
#endif
}

int main(int argc, char **argv) {
  return MppTest(MppTestBuilder(argc, argv)
                     .WithParallelListeners()
                     .WithConfigEntry("Distribution", "Stripes_y")
                     .WithConfigEntry("SampleCounterVerbose", 1)
                     .WithConfigEntry("SLEstimatorVerbose", 1)
                     .WithConfigEntry("SLEstimatorPlotting", 3)
                     .WithConfigEntry("ParallelPlotting", false)
                     .WithConfigEntry("Quantity", "L2")
                     .WithScreenLogging()
                     .WithRandomInitialized()
                     .WithPPM())
      .RUN_ALL_MPP_TESTS();
}
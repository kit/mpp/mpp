#include "Main.hpp"
#include "TestEnvironment.hpp"

const double DETERMINISTIC_TOL = 1e-5; // that's good enough, we're still only on level 4

const double STOCHASTIC_TOL = 1e-2; // with noise, we can expect even less

/*
 * Todos:
 *  - Extend covered test cases
 *  - maybe use combine
 *  - test seems very flaky
 *
 *
 */


// clang-format off
class TestSGD : public Test {
protected:
  const MultiXFEMConfig mxFEMConfig;

  const SLEstimatorConfig slEstimatorConfig;

  const MLEstimatorConfig mlEstimatorConfig;

  const SGDConfig sgdConfig;
public:
  TestSGD(const std::string &problemName) :
      mxFEMConfig(MultiXFEMConfig()
        .WithProblem(problemName)),
      slEstimatorConfig(SLEstimatorConfig()
        .WithMultiSampleFEMConfig(mxFEMConfig)
        .WithOnlyFine(false)
        .WithParallel(true)
        .WithInitLevel(4)
        .WithObjective("L2(E(backward))")),
      mlEstimatorConfig(MLEstimatorConfig()
        .WithSLEstimatorConfig(slEstimatorConfig)
        .WithObjective("L2(E(backward))")) {
  }
};

class TestSGDOCLaplace2D : public TestSGD {
public:
  TestSGDOCLaplace2D() : TestSGD("OCLaplace2D") {}
};

TEST_F(TestSGDOCLaplace2D, TestWithConstantStepsize) {
  for (int batchSize = 1; batchSize <= 4; batchSize++) {
    mout.StartBlock("Batched Experiment");
    mout << std::format("batchSize={}", batchSize) << std::endl;
    SGDConfig sgdConfig = SGDConfig()
        .WithOptimizerType(OptimizerType::CONSTANT)
        .WithMLEstimatorConfig(mlEstimatorConfig)
        .WithAdmissibleBounds(-1000, 1000)
        .WithBatchSize(batchSize)
        .WithDefaultStepsize(200)
        .WithMaxGradientSteps(26)
        .WithAveragingFraction(0)
        .WithEpsilonTarget(0.0)
        .WithTargetCost(0.0)
        .WithBetaAdam2(0.0)
        .WithBetaAdam(0.0);

    auto batchedSgd = BatchedSGD(sgdConfig);
    batchedSgd.Method();
    const auto &optData = batchedSgd.GetOptData();

    EXPECT_NEAR(optData.targetFunctionalL2Norms.front(), 0.12500, DETERMINISTIC_TOL);
    EXPECT_NEAR(optData.targetFunctionalL2Norms.back(), 0.02553, DETERMINISTIC_TOL);

    EXPECT_NEAR(optData.gradientL2Norms.front(), 0.00625, DETERMINISTIC_TOL);
    EXPECT_NEAR(optData.gradientL2Norms.back(), 0.00282, DETERMINISTIC_TOL);

    EXPECT_EQ(optData.stepSizes.back(), optData.stepSizes.front());
    EXPECT_EQ(optData.stepSizes.back(), 200);
    mout.EndBlock();
  }
}

TEST_F(TestSGDOCLaplace2D, TestADAM) {
  for (int batchSize = 1; batchSize <= 4; batchSize++) {
    mout.StartBlock(std::format("Batched Experiment", batchSize));
    mout << std::format("batchSize={}", batchSize) << std::endl;
    SGDConfig sgdConfig = SGDConfig()
      .WithGradientEstimatorType(EstimatorType::BATCHED)
      .WithOptimizerType(OptimizerType::ADAM)
      .WithMLEstimatorConfig(mlEstimatorConfig)
      .WithBetaAdam(0.9)
      .WithBetaAdam2(0.99)
      .WithAdmissibleBounds(-1000, 1000)
      .WithDefaultStepsize(1.0)
      .WithBatchSize(1)
      .WithMaxGradientSteps(25)
      .WithAveragingFraction(0)
      .WithEpsilonTarget(0.0)
      .WithTargetCost(0.0);

    auto batchedSgd = BatchedSGD(sgdConfig);

    batchedSgd.Method();

    const auto &optData = batchedSgd.GetOptData();

    EXPECT_NEAR(optData.targetFunctionalL2Norms.front(), 0.12500, DETERMINISTIC_TOL);
    EXPECT_NEAR(optData.targetFunctionalL2Norms.back(), 0.03874, DETERMINISTIC_TOL);

    EXPECT_NEAR(optData.gradientL2Norms.front(), 0.00625, DETERMINISTIC_TOL);
    EXPECT_NEAR(optData.controlL2Norms.front(), 0.83331, DETERMINISTIC_TOL);
  mout.EndBlock();
  }
}

TEST_F(TestSGDOCLaplace2D, TestWithMultilevelEstimator) {
  mout.StartBlock("Multilevel Experiment");
  SGDConfig sgdConfig = SGDConfig()
      .WithOptimizerType(OptimizerType::CONSTANT)
      .WithMLEstimatorConfig(mlEstimatorConfig)
      .WithBatchSize(std::map<int,int> {{2, 64}, {3, 16}, {4, 4}})
      .WithAdmissibleBounds(-1000, 1000)
      .WithDefaultStepsize(200)
      .WithMaxGradientSteps(26)
      .WithAveragingFraction(0)
      .WithEpsilonTarget(0.0)
      .WithTargetCost(0.0)
      .WithBetaAdam2(0.0)
      .WithBetaAdam(0.0);

  auto batchedSgd = MultilevelSGD(sgdConfig);
  batchedSgd.Method();
  const auto &optData = batchedSgd.GetOptData();

  EXPECT_NEAR(optData.targetFunctionalL2Norms.front(), 0.12500, DETERMINISTIC_TOL);
  EXPECT_NEAR(optData.targetFunctionalL2Norms.back(), 0.02553, DETERMINISTIC_TOL);

  EXPECT_NEAR(optData.gradientL2Norms.front(), 0.00625, DETERMINISTIC_TOL);
  EXPECT_NEAR(optData.gradientL2Norms.back(), 0.00282, DETERMINISTIC_TOL);

  EXPECT_EQ(optData.stepSizes.back(), optData.stepSizes.front());
  EXPECT_EQ(optData.stepSizes.back(), 200);
  mout.EndBlock();
}

class TestSGDOCStochasticLaplace2D : public TestSGD {
public:
  TestSGDOCStochasticLaplace2D() : TestSGD("OCStochasticLaplace2D") {}
};

TEST_F(TestSGDOCStochasticLaplace2D, TestWithDecreasingStepsize) {
    mout.StartBlock("SGD Experiment");
    SGDConfig sgdConfig = SGDConfig()
      .WithGradientEstimatorType(EstimatorType::BATCHED)
      .WithOptimizerType(OptimizerType::DECREASING) // Different to the prev. test & Stoch. problem
      .WithMLEstimatorConfig(mlEstimatorConfig)
      .WithAdmissibleBounds(-1000, 1000)
      .WithDefaultStepsize(200)
      .WithBatchSize(1)
      .WithMaxGradientSteps(10)
      .WithAveragingFraction(0)
      .WithEpsilonTarget(0.0)
      .WithBetaAdam(0.0)
      .WithBetaAdam2(0.0)
      .WithTargetCost(0.0);

    auto batchedSgd = BatchedSGD(sgdConfig);
    batchedSgd.Method();

    const auto &optData = batchedSgd.GetOptData();

    EXPECT_NEAR(optData.targetFunctionalL2Norms.front(), 0.12500, STOCHASTIC_TOL);
    //EXPECT_NEAR(optData.targetFunctionalL2Norms.back(), 0.096121235833829519, STOCHASTIC_TOL);

    EXPECT_NEAR(optData.gradientL2Norms.front(), 0.00312, STOCHASTIC_TOL);
    EXPECT_NEAR(optData.gradientL2Norms.back(), 0.00571, STOCHASTIC_TOL);

    EXPECT_NEAR(optData.stepSizes[3], optData.stepSizes.front() / 2, STOCHASTIC_TOL);
    EXPECT_EQ(optData.stepSizes.front(), 200);
    mout.EndBlock();
}

TEST_F(TestSGDOCStochasticLaplace2D, TestWithMultilevelEstimator) {

    mout.StartBlock("Multilevel SGD Experiment");
    SGDConfig sgdConfig = SGDConfig()
      .WithOptimizerType(OptimizerType::DECREASING) // Different to the prev. test & Stoch. problem
      .WithMLEstimatorConfig(mlEstimatorConfig)
      .WithAdmissibleBounds(-1000, 1000)
      .WithDefaultStepsize(200)
      .WithMaxGradientSteps(10)
      .WithBatchSize(std::map<int,int> {{2, 128}, {3, 32},{4, 8}})
      .WithAveragingFraction(0)
      .WithEpsilonTarget(0.0)
      .WithBetaAdam(0.0)
      .WithBetaAdam2(0.0)
      .WithTargetCost(0.0);

    auto sgd = MultilevelSGD(sgdConfig);
    sgd.Method();

    const auto &optData = sgd.GetOptData();

    optData.PrintInfo();

    EXPECT_NEAR(optData.targetFunctionalL2Norms.front(), 0.12500, STOCHASTIC_TOL);
    EXPECT_NEAR(optData.targetFunctionalL2Norms.back(), 0.0857, STOCHASTIC_TOL);

    EXPECT_NEAR(optData.gradientL2Norms.front(), 0.009, STOCHASTIC_TOL);
    EXPECT_NEAR(optData.gradientL2Norms.back(), 0.004847, STOCHASTIC_TOL);

    EXPECT_EQ(optData.stepSizes[3], optData.stepSizes.front() / 2);
    EXPECT_EQ(optData.stepSizes.front(), 200);
    mout.EndBlock();
}


// clang-format on

int main(int argc, char **argv) {
  return MppTest(MppTestBuilder(argc, argv)
                     .WithConfigEntry("VtuPlot", 0)
                     .WithConfigEntry("NormsOnly", 1)
                     .WithConfigEntry("SGDVerbose", 2)
                     .WithConfigEntry("MLMCVerbose", 0)
                     .WithConfigEntry("MainVerbose", 0)
                     .WithConfigEntry("MeshVerbose", 0)
                     .WithConfigEntry("SGDPlotting", 0)
                     .WithConfigEntry("MXFEMVerbose", 0)
                     .WithConfigEntry("ParallelPlotting", 0)
                     .WithConfigEntry("MeshesVerbose", 0)
                     .WithConfigEntry("ConfigVerbose", 0)
                     .WithConfigEntry("LinearVerbose", 0)
                     .WithConfigEntry("NewtonVerbose", 0)
                     .WithConfigEntry("AssembleVerbose", 0)
                     .WithConfigEntry("PDESolverVerbose", 0)
                     .WithConfigEntry("GeneratorVerbose", 0)
                     .WithConfigEntry("SLEstimatorVerbose", 1)
                     .WithConfigEntry("MakeCommunicatorConsistent", 1)
                     .WithRandomInitialized()
                     .WithScreenLogging()
                     .WithPPM())
      .RUN_ALL_MPP_TESTS();
}

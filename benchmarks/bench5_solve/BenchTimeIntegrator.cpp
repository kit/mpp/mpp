#include <memory>

#include "BenchMpp.hpp"
#include "IAcousticAssemble.hpp"
#include "TimeIntegrator.hpp"

#include <benchmark/benchmark.h>

#include "AcousticProblems.hpp"
#include "PDESolver.hpp"
#include "TestEnvironment.hpp"

static void benchmark_timeintegrator(std::unique_ptr<AcousticProblem> problem,
                                     benchmark::State &state) {
  const auto integrator = TimeIntegratorCreator()
                              .WithLinearSolver(GetLinearSolver(GetPC()))
                              .WithLinearSolver(GetLinearSolver(GetPC()))
                              .WithLinearSolver(GetLinearSolver(GetPC()))
                              .WithLinearSolver(GetLinearSolver(GetPC()))
                              .WithRkOrder(2)
                              .CreateUnique();
  const auto level = 4;
  const auto config = PDESolverConfig().WithModel("DG");

  const auto assemble = CreateAcousticAssembleUnique(*problem, config);
  for (auto _ : state) {
    // Setup
    Solution solution(assemble->GetSharedDisc(), MeshIndex{level, -1, 0, 0});
    assemble->SetTimeSeries(solution.vector);

    // Code to be benchmarked
    BENCH_START_TIMING();
    integrator->Method(assemble.get(), solution.vector);
    BENCH_END_TIMING(state);
  }
}

static void benchmark_gausshat_and_ricker_2d(benchmark::State &state) {
  benchmark_timeintegrator(AcousticPDEProblemBuilder::GaussHatAndRicker2D().Build(), state);
}

MPI_BENCHMARK(benchmark_gausshat_and_ricker_2d);

static void benchmark_quadratic(benchmark::State &state) {
  benchmark_timeintegrator(AcousticPDEProblemBuilder::Quadratic().Build(), state);
}

MPI_BENCHMARK(benchmark_quadratic);

static void benchmark_riemannwave_2d(benchmark::State &state) {
  benchmark_timeintegrator(AcousticPDEProblemBuilder::RiemannWave2D().Build(), state);
}

MPI_BENCHMARK(benchmark_riemannwave_2d);

static void benchmark_crc(benchmark::State &state) {
  benchmark_timeintegrator(AcousticPDEProblemBuilder::CRC().WithEndTime(0.0).Build(), state);
}

MPI_BENCHMARK(benchmark_crc);

int main(int argc, char **argv) {
  MppTest mppTest = MppTestBuilder(argc, argv).WithPPM();
  mout.setEnabledLoggers(0);
  return mppTest.RUN_ALL_MPP_BENCHMARKS(argc, argv);
}

#include <functional>
#include <memory>
#include <utility>

#include <benchmark/benchmark.h>

#include "BenchMpp.hpp"
#include "DGDiscretization.hpp"
#include "IDiscretization.hpp"
#include "LagrangeDiscretization.hpp"
#include "Meshes.hpp"
#include "MeshesCreator.hpp"
#include "TestEnvironment.hpp"
#include "Vector.hpp"

namespace {

std::unique_ptr<Meshes> meshes;

constexpr int spaceLevel = 9;

} // anonymous namespace

class VectorBench : public MppMpiBench {
public:
  using DiscCreator = std::function<std::shared_ptr<IDiscretization>(const Meshes &)>;
protected:
  DiscCreator discCreator;
public:
  VectorBench(DiscCreator &&discCreator) : discCreator(std::move(discCreator)) {}

  inline void Create(benchmark::State &state) {
    auto disc = discCreator(*meshes);

    for (auto _ : state) {
      BENCH_START_TIMING();
      benchmark::DoNotOptimize(Vector{disc, spaceLevel});
      BENCH_END_TIMING(state);
    }
  }

  inline void AssignScalar(benchmark::State &state) {
    Vector vector{2, discCreator(*meshes), spaceLevel};
    for (auto _ : state) {
      BENCH_START_TIMING();
      benchmark::DoNotOptimize(vector = 1);
      BENCH_END_TIMING(state);
    }
  }

  inline void AssignVector(benchmark::State &state) {
    Vector firstVector{2, discCreator(*meshes), spaceLevel};
    Vector secondVector{1, firstVector};
    for (auto _ : state) {
      BENCH_START_TIMING();
      benchmark::DoNotOptimize(firstVector = secondVector);
      BENCH_END_TIMING(state);
    }
  }

  inline void MultiplyScalar(benchmark::State &state) {
    Vector vector{2, discCreator(*meshes), spaceLevel};
    for (auto _ : state) {
      BENCH_START_TIMING();
      benchmark::DoNotOptimize(vector *= 1);
      BENCH_END_TIMING(state);
    }
  }

  inline void MultiplyVector(benchmark::State &state) {
    Vector firstVector{2, discCreator(*meshes), spaceLevel};
    Vector secondVector{1, firstVector};
    for (auto _ : state) {
      BENCH_START_TIMING();
      benchmark::DoNotOptimize(firstVector *= secondVector);
      BENCH_END_TIMING(state);
    }
  }

  inline void AddVector(benchmark::State &state) {
    Vector firstVector{2, discCreator(*meshes), spaceLevel};
    Vector secondVector{1, firstVector};
    for (auto _ : state) {
      BENCH_START_TIMING();
      benchmark::DoNotOptimize(firstVector += secondVector);
      BENCH_END_TIMING(state);
    }
  }
};

class LagrangeVectorBench : public VectorBench {
public:
  LagrangeVectorBench() :
      VectorBench([](const Meshes &meshes) {
        return std::make_shared<LagrangeDiscretization>(meshes, 1);
      }) {}
};

class DGVectorBench : public VectorBench {
public:
  DGVectorBench() :
      VectorBench(
          [](const Meshes &meshes) { return std::make_shared<DGDiscretization>(meshes, 1); }) {}
};

BENCHMARK_F(LagrangeVectorBench, Create)(benchmark::State &state) { this->Create(state); }

BENCHMARK_F(LagrangeVectorBench, AssignScalar)(benchmark::State &state) {
  this->AssignScalar(state);
}

BENCHMARK_F(LagrangeVectorBench, AssignVector)(benchmark::State &state) {
  this->AssignVector(state);
}

BENCHMARK_F(LagrangeVectorBench, MultiplyScalar)(benchmark::State &state) {
  this->MultiplyScalar(state);
}

BENCHMARK_F(LagrangeVectorBench, MultiplyVector)(benchmark::State &state) {
  this->MultiplyVector(state);
}

BENCHMARK_F(LagrangeVectorBench, AddVector)(benchmark::State &state) { this->AddVector(state); }

BENCHMARK_F(DGVectorBench, Create)(benchmark::State &state) { this->Create(state); }

BENCHMARK_F(DGVectorBench, AssignScalar)(benchmark::State &state) { this->AssignScalar(state); }

BENCHMARK_F(DGVectorBench, AssignVector)(benchmark::State &state) { this->AssignVector(state); }

BENCHMARK_F(DGVectorBench, MultiplyScalar)(benchmark::State &state) { this->MultiplyScalar(state); }

BENCHMARK_F(DGVectorBench, MultiplyVector)(benchmark::State &state) { this->MultiplyVector(state); }

BENCHMARK_F(DGVectorBench, AddVector)(benchmark::State &state) { this->AddVector(state); }

int main(int argc, char **argv) {
  MppTest mppTest = MppTestBuilder(argc, argv).WithPPM();

  meshes = MeshesCreator().WithMeshName("Square").WithLevel(spaceLevel).CreateUnique();

  return mppTest.RUN_ALL_MPP_BENCHMARKS(argc, argv);
}

#pragma once

#include <chrono>

#include <Parallel.hpp>
#include <benchmark/benchmark.h>

/**
 * @brief Helper class to enforce fixed iterations for MPI benchmarks. The same number of iterations
 * has to be enforced if communication is used within the benchmark code.
 *
 * BENCHMARK_F(FixtureClassName, BenchmarkName)(benchmark::State &state) {
 *    for (auto _ : state) {
 *        // benchmark code goes here
 *    }
 * }
 */
class MppMpiBench : public benchmark::Fixture {
public:
  explicit MppMpiBench() { UseManualTime(); }

  /**
   * @brief Setup code which runs before each repetition (This is not the iteration count!).
   *
   * @param state
   */
  void SetUp(benchmark::State &state) override {};

  /**
   * @brief Tear down code which runs after each repetition.
   *
   * @param state
   */
  void TearDown(benchmark::State &state) override {};
};

/**
 * @brief Start the timing. Use with MPI_BENCHMARK or MppMpiBench.
 */
#define BENCH_START_TIMING() const auto _start = std::chrono::high_resolution_clock::now();

/**
 * @brief Stops the timing and sets the duration of the iteration across procs to the same value.
 * This is neccessary for googlebench to determine the same iteration count across procs. Use with
 * MPI_BENCHMARK or MppMpiBench.
 *
 * The counter "real_time_on_proc" contains the actual real time on the current proc, whereas
 * "real_time" contains the max real time used accross procs. Do not use "->Unit(...)" as only the
 * default time unit is respected.
 */
#define BENCH_END_TIMING(state)                                                                    \
  const auto _end = std::chrono::high_resolution_clock::now();                                     \
  state.PauseTiming();                                                                             \
  auto _duration = std::chrono::duration<double>(_end - _start).count();                           \
  auto &_real_time2 = state.counters["real_time_on_proc"];                                         \
  _real_time2.value +=                                                                             \
      _duration * benchmark::GetTimeUnitMultiplier(benchmark::GetDefaultTimeUnit());               \
  _real_time2.flags = benchmark::Counter::Flags::kAvgIterations;                                   \
  _duration = PPM->Max(_duration);                                                                 \
  state.SetIterationTime(_duration);                                                               \
  state.ResumeTiming();

/**
 * @brief Shorthand macro which enables the use of the manually set iteration duration for iteration
 * count determination.
 */
#define MPI_BENCHMARK(...) BENCHMARK(__VA_ARGS__)->UseManualTime()

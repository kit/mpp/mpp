#include <memory>
#include <string>
#include <utility>

#include <benchmark/benchmark.h>

#include "BenchMpp.hpp"
#include "MeshIndex.hpp"
#include "Meshes.hpp"
#include "MeshesCreator.hpp"
#include "TestEnvironment.hpp"

namespace {

constexpr int startLevel = 3;
constexpr int distributeLevel = startLevel - 1;
constexpr int coarseLevel = startLevel - 1;

} // anonymous namespace

class MeshBench : public MppMpiBench {
public:
  MeshBench(std::string &&meshName) : meshName(std::move(meshName)) {}

  void SetUp(benchmark::State &state) override {
    meshes = MeshesCreator{}
                 .WithMeshName(meshName)
                 .WithPLevel(distributeLevel)
                 .WithCLevel(coarseLevel)
                 .WithLevel(state.range(0) - 1)
                 .CreateUnique();
    (*meshes)[MeshIndex{static_cast<int>(state.range(0) - 1)}];
    state.SetComplexityN(state.range(0));
  }

  void TearDown(benchmark::State &state) override { meshes = nullptr; }

  inline void CreateMesh(benchmark::State &state) {
    const MeshIndex index{static_cast<int>(state.range(0))};
    for (auto _ : state) {
      BENCH_START_TIMING();
      (*meshes)[index];
      BENCH_END_TIMING(state);

      meshes->Erase(index);
    }
  }
protected:
  std::unique_ptr<Meshes> meshes = nullptr;
  const std::string meshName;
};

class IntervalMeshBench : public MeshBench {
public:
  IntervalMeshBench() : MeshBench("Interval") {}
};

class Square2TrianglesMeshBench : public MeshBench {
public:
  Square2TrianglesMeshBench() : MeshBench("Square2Triangles") {}
};

class SquareMeshBench : public MeshBench {
public:
  SquareMeshBench() : MeshBench("Square") {}
};

BENCHMARK_DEFINE_F(IntervalMeshBench, Create)(benchmark::State &state) { CreateMesh(state); };

BENCHMARK_DEFINE_F(Square2TrianglesMeshBench, Create)(benchmark::State &state) {
  CreateMesh(state);
};

BENCHMARK_DEFINE_F(SquareMeshBench, Create)(benchmark::State &state) { CreateMesh(state); };

BENCHMARK_REGISTER_F(IntervalMeshBench, Create)->DenseRange(startLevel, 20, 1)->Complexity();
BENCHMARK_REGISTER_F(Square2TrianglesMeshBench, Create)->DenseRange(startLevel, 9, 1)->Complexity();
BENCHMARK_REGISTER_F(SquareMeshBench, Create)->DenseRange(startLevel, 11, 1)->Complexity();

int main(int argc, char **argv) {
  MppTest mppTest = MppTestBuilder(argc, argv).WithPPM();

  return mppTest.RUN_ALL_MPP_BENCHMARKS(argc, argv);
}

#!/usr/bin/python3
import argparse
import bisect
import json
import warnings
from collections import defaultdict
from datetime import datetime
from pathlib import Path

import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.backends.backend_pdf import PdfPages


class BenchGroup:
    def __init__(self, run_path: Path, after: datetime, before: datetime):
        # Find dates in [after, before)
        paths = run_path.glob('[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]-[0-9][0-9]-[0-9][0-9]-[0-9][0-9]/')
        dates = sorted([datetime.strptime(d.name, '%Y-%m-%d-%H-%M-%S') for d in paths])
        start = bisect.bisect_left(dates, after)
        end = bisect.bisect_left(dates, before)
        self._dates = dates[start:end]

        self._group_name = ""
        self._num_procs = -1
        self._data = defaultdict()

        for date_idx, date in enumerate(self._dates):
            # Find proc JSONs
            run_on_procs = sorted((run_path / date.strftime('%Y-%m-%d-%H-%M-%S')).glob('[0-9][0-9][0-9][0-9].json'))

            if self._num_procs < 0:
                # Initialize procs and np array layout
                self._num_procs = len(run_on_procs)
                # data[benchmark_name][date][proc]
                self._data = defaultdict(lambda: np.full((len(self._dates), self._num_procs), np.nan))
            elif self._num_procs != len(run_on_procs):
                raise RuntimeError(
                    f"Previous benchmarks were run with {self._num_procs} procs but {date.strftime('%Y-%m-%d-%H-%M-%S')} ran with {len(run_on_procs)} procs.")

            # Fill data from process JSONs
            for proc, filepath in enumerate(run_on_procs):
                with open(filepath, "r") as infile:
                    data = json.load(infile)
                for benchmark in data["benchmarks"]:
                    benchmark_name, run_type = benchmark["name"].rsplit('/', 1)
                    if run_type != "manual_time":
                        # Skip infos like big O for plots
                        continue
                    self._data[benchmark_name][date_idx][proc] = benchmark["real_time_on_proc"]
                    benchmark_group_name = data["context"]["executable"].rsplit("/", 1)[-1]
                    if self._group_name == "":
                        self._group_name = benchmark_group_name
                    elif self._group_name != benchmark_group_name:
                        raise RuntimeError(
                            f"Previous executable is named {self._group_name} but {date.strftime('%Y-%m-%d-%H-%M-%S')} is {benchmark_group_name}")

    def group_name(self):
        """Returns the executable name of the benchmark"""
        return self._group_name

    def benchmark_names(self):
        """Returns the names of the benchmark cases"""
        return self._data.keys()

    def dates(self):
        """Returns the date of each benchmark run"""
        return self._dates

    def min(self, benchmark_name: str):
        """Returns the min duration across procs or NaN for all dates"""
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", category=RuntimeWarning)
            if benchmark_name in self._data:
                return np.nanmin(self._data[benchmark_name], axis=1)
            return np.full(len(self._dates), np.nan)

    def max(self, benchmark_name: str):
        """Returns the max duration across procs or NaN for all dates"""
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", category=RuntimeWarning)
            if benchmark_name in self._data:
                return np.nanmax(self._data[benchmark_name], axis=1)
            return np.full(len(self._dates), np.nan)

    def mean(self, benchmark_name: str):
        """Returns the mean duration across procs or NaN for all dates"""
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", category=RuntimeWarning)
            if benchmark_name in self._data:
                return np.nanmean(self._data[benchmark_name], axis=1)
            return np.full(len(self._dates), np.nan)

    def procs(self):
        """Returns the number of procs the benchmark has been run on"""
        return self._num_procs


def run_group(group_path: Path, output_path: Path, suffix: str, after: datetime, before: datetime, ref: str):
    group = BenchGroup(group_path, after, before)
    dates = group.dates()
    date_indices = np.arange(len(dates))
    benchmarks = list(group.benchmark_names())
    if len(benchmarks) == 0:
        warnings.warn(
            f"No benchmark runs found for {group_path} between {after} and {before}, skipping")

    # Specify how many plots are allowed per pdf page
    max_rows = 3
    max_cols = 4

    output_path.mkdir(mode=0o755, parents=True, exist_ok=True)
    with PdfPages(f"{output_path}/{group.group_name()}{suffix}.pdf") as pdf:
        offset = 0
        while offset < len(benchmarks):
            fig, ax = plt.subplots(nrows=max_rows, ncols=max_cols, sharex=True, figsize=(16, 9),
                                   gridspec_kw={'hspace': 0.3, 'wspace': 0.3})

            # Set main title
            fig.suptitle(f"{group.group_name()} on {group.procs()} proc(s)", fontsize=18, fontweight="bold",
                         url=f"https://gitlab.kit.edu/kit/mpp/mpp-data/-/tree/{ref}/{group.group_name()}")

            # Numpy is being silly and returns subplots as a nrows x ncols array
            ax = ax.flatten()

            benchmark_slice = benchmarks[offset:offset + max_rows * max_cols]

            for subplot, benchmark_name in zip(ax, benchmark_slice):
                subplot.set_title(benchmark_name, fontsize=11)
                # Add plot data
                subplot.plot(date_indices, group.max(
                    benchmark_name), marker='x', label='Max of procs')
                subplot.plot(date_indices, group.mean(
                    benchmark_name), marker='x', label='Mean of procs')
                subplot.plot(date_indices, group.min(
                    benchmark_name), marker='x', label='Min of procs')
                # Scale y axis individually
                subplot.set_ylim(auto=True)
                # Only show ticks on data points
                subplot.set_xticks(date_indices)
                subplot.xaxis.set_major_formatter(mdates.DateFormatter("%Y-%m-%d %H:%M:%S"))
                subplot.xaxis.set_tick_params(labelbottom=False)
                subplot.xaxis.set_ticklabels(dates)
                # Enable grid
                subplot.grid()

            # Enable x-axis tick labels for the bottom facing subplots
            for i in range(max(len(benchmark_slice) - max_cols, 0), len(benchmark_slice)):
                ax[i].xaxis.set_tick_params(labelbottom=True)
                # We got no space, rotate date labels
                plt.setp(ax[i].xaxis.get_majorticklabels(),
                         rotation=-60, ha="left", fontsize=10)

            # Set common axis labels
            fig.text(0.5, 0.02, "Date & Time of Run (YYYY-MM-DD HH:mm:ss)",
                     ha="center", fontsize=14)
            fig.text(0.02, 0.5, "Duration in ns", va="center", rotation="vertical", fontsize=14)

            # Add common legend
            handles, labels = ax[0].get_legend_handles_labels()
            fig.legend(handles=handles, labels=labels, ncol=2)

            # Remove unused frames
            for i in range(len(benchmark_slice), len(ax)):
                ax[i].set_visible(False)
            # Add more space for date time labels
            horizontal_margin = 0.08
            fig.subplots_adjust(
                bottom=0.2, left=horizontal_margin, right=(1.0 - horizontal_margin))
            # Save pdf page
            pdf.savefig()
            plt.close()
            offset += max_rows * max_cols


def run_many(data_path: Path, output_path: Path, suffix: str, after: datetime, before: datetime, ref: str):
    for f in data_path.iterdir():
        if f.is_dir():
            run_group(f, output_path, suffix, after, before, ref)


parser = argparse.ArgumentParser()

mode = parser.add_mutually_exclusive_group(required=True)
mode.add_argument('-g', '--groups', type=Path, nargs='+', metavar='path/to/group/',
                  help='visualize specified benchmark groups')
mode.add_argument('-m', '--many', type=Path, metavar='path/to/group_parent/', help='visualize all available groups')

parser.add_argument('-o', '--out', type=Path, required=True, metavar='path/to/out/',
                    help='set the output path for the generated plots')
parser.add_argument('-s', '--suffix', default="", type=str, metavar='suffix',
                    help='add a suffix to the generated plot names, i.e. BenchMeshBENCH<suffix>.pdf')
parser.add_argument('-a', '--after', default=datetime.min, type=datetime.fromisoformat, metavar='YYYY-MM-DD:HH:mm:ss',
                    help='only plot runs after the set date, i.e. 2025-05-01')
parser.add_argument('-b', '--before', default=datetime.max, type=datetime.fromisoformat, metavar='YYYY-MM-DD:HH:mm:ss',
                    help='only plot runs before the set date, i.e. 2025-05-02:13:10:10')
parser.add_argument('--data-commit-ref', default="benchmarks", type=str, help=argparse.SUPPRESS)

args = parser.parse_args()

if args.groups:
    for group in args.groups:
        run_group(group, args.out, args.suffix, args.after, args.before, args.data_commit_ref)

if args.many:
    run_many(args.many, args.out, args.suffix, args.after, args.before, args.data_commit_ref)

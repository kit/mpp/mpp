#include <benchmark/benchmark.h>

#include "BenchMpp.hpp"
#include "EllipticPDESolver.hpp"
#include "EllipticProblems.hpp"
#include "IEllipticAssemble.hpp"
#include "Logging.hpp"
#include "MeshIndex.hpp"
#include "PDESolver.hpp"
#include "TestEnvironment.hpp"

namespace {
constexpr int minLevel = 1;
constexpr int maxLevel = 4;
constexpr int minDegree = 1;
constexpr int maxDegree = 4;
static int minTime = 0;
} // anonymous namespace

class BenchPolynomials : public MppMpiBench {
protected:
  std::string problemName;
public:
  BenchPolynomials(std::string &&problemName) : problemName(problemName) {
    int minTime = 0;
    Config::Get("minTime", minTime);
    MinTime(minTime);
  }

  void SetUp(benchmark::State &state) override {
    state.SetComplexityN(state.range(0));
    state.SetComplexityN(state.range(1));
  }

  inline void run(benchmark::State &state) {
    const auto level = state.range(0);
    const auto degree = state.range(1);

    const auto config = PDESolverConfig().WithDegree(degree).WithModel("Lagrange");
    const auto problem = CreateEllipticProblemShared(problemName);
    for (auto _ : state) {
      BENCH_START_TIMING();
      auto solver = EllipticPDESolver(config);
      benchmark::DoNotOptimize(solver.Run(problem, MeshIndex{static_cast<int>(level), -1, 0, 0}));
      BENCH_END_TIMING(state);
    }
  }
};


class P4Test1D : public BenchPolynomials {
public:
  P4Test1D() : BenchPolynomials("P4Test1D") {}
};

#if SpaceDimension >= 2
class P4Test2D : public BenchPolynomials {
public:
  P4Test2D() : BenchPolynomials("P4Test2D") {}
};
#endif

#if SpaceDimension >= 3
class P4Test3D : public BenchPolynomials {
public:
  P4Test3D() : BenchPolynomials("P4Test3D") {}
};
#endif


BENCHMARK_DEFINE_F(P4Test1D, Run)(benchmark::State &state) { this->run(state); }

#if SpaceDimension >= 2
BENCHMARK_DEFINE_F(P4Test2D, Run)(benchmark::State &state) { this->run(state); }
#endif

#if SpaceDimension >= 3
BENCHMARK_DEFINE_F(P4Test3D, Run)(benchmark::State &state) { this->run(state); }
#endif

BENCHMARK_REGISTER_F(P4Test1D, Run)->ArgsProduct({
  benchmark::CreateDenseRange(minLevel, maxLevel, 1),
  benchmark::CreateDenseRange(minDegree, maxDegree, 1),
})->Complexity();

#if SpaceDimension >= 2
BENCHMARK_REGISTER_F(P4Test2D, Run)->ArgsProduct({
  benchmark::CreateDenseRange(minLevel, maxLevel, 1),
  benchmark::CreateDenseRange(minDegree, maxDegree, 1),
})->Complexity();
#endif

#if SpaceDimension >= 3
BENCHMARK_REGISTER_F(P4Test3D, Run)->ArgsProduct({
  benchmark::CreateDenseRange(minLevel, maxLevel, 1),
  benchmark::CreateDenseRange(minDegree, maxDegree, 1),
})->Complexity();

#endif

int main(int argc, char **argv) {
  MppTest mppTest = MppTestBuilder(argc, argv).WithPPM();
  mout.setEnabledLoggers(0);
  return mppTest.RUN_ALL_MPP_BENCHMARKS(argc, argv);
}

#include <string>
#include <utility>

#include <benchmark/benchmark.h>

#include "BenchMpp.hpp"
#include "EllipticPDESolver.hpp"
#include "EllipticProblems.hpp"
#include "IEllipticAssemble.hpp"
#include "Logging.hpp"
#include "MeshIndex.hpp"
#include "PDESolver.hpp"
#include "TestEnvironment.hpp"

namespace {

constexpr int minTime = 60;

} // anonymous namespace

class BenchDivergent : public MppMpiBench {
protected:
  std::string model;
public:
  BenchDivergent(std::string &&model) : model(std::move(model)) {
    MinTime(minTime);
  }

  inline void run(benchmark::State &state) {
    int level = 4;
    int degree = 4;
    Config::Get("level", level);
    Config::Get("degree", degree);

    const auto config = PDESolverConfig().WithDegree(degree).WithModel(model);
    const auto problem = EllipticPDEProblemBuilder::Divergent().BuildShared();
    for (auto _ : state) {
      BENCH_START_TIMING();
      auto solver = EllipticPDESolver(config);
      benchmark::DoNotOptimize(solver.Run(problem, MeshIndex{level, -1, 0, 0}));
      BENCH_END_TIMING(state);
    }
  }
};

class MixedModel : public BenchDivergent {
public:
  MixedModel() : BenchDivergent("Mixed") {}
};

class HybridModel : public BenchDivergent {
public:
  HybridModel() : BenchDivergent("Hybrid") {}
};

class LagrangeModel : public BenchDivergent {
public:
  LagrangeModel() : BenchDivergent("Lagrange") {}
};

BENCHMARK_F(MixedModel, Run)(benchmark::State &state) { this->run(state); }

BENCHMARK_F(HybridModel, Run)(benchmark::State &state) { this->run(state); }

BENCHMARK_F(LagrangeModel, Run)(benchmark::State &state) { this->run(state); }

int main(int argc, char **argv) {
  MppTest mppTest = MppTestBuilder(argc, argv).WithPPM();
  mout.setEnabledLoggers(0);
  return mppTest.RUN_ALL_MPP_BENCHMARKS(argc, argv);
}

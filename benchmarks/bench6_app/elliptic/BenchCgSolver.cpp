#include <string>
#include <utility>

#include <benchmark/benchmark.h>

#include "BenchMpp.hpp"
#include "EllipticPDESolver.hpp"
#include "EllipticProblems.hpp"
#include "IEllipticAssemble.hpp"
#include "Logging.hpp"
#include "MeshIndex.hpp"
#include "PDESolver.hpp"
#include "TestEnvironment.hpp"

namespace {

constexpr int minTime = 60;

} // anonymous namespace

class CgBench : public MppMpiBench {
protected:
  std::string preconditioner;
public:
  CgBench(std::string &&preconditioner) : preconditioner(std::move(preconditioner)) {
    MinTime(minTime);
  }

  inline void run(benchmark::State &state) {
    int level = 4;
    int degree = 4;
    Config::Get("level", level);
    Config::Get("degree", degree);

    const auto config = PDESolverConfig()
                            .WithDegree(degree)
                            .WithPreconditioner(preconditioner)
                            .WithLinearSolver("CG")
                            .WithModel("Lagrange");

    const auto problem = EllipticPDEProblemBuilder::Laplace2D().BuildShared();
    for (auto _ : state) {
      BENCH_START_TIMING();
      auto solver = EllipticPDESolver(config);
      benchmark::DoNotOptimize(solver.Run(problem, MeshIndex{level, -1, 0, 0}));
      BENCH_END_TIMING(state);
    }
  }
};

class BenchCgWithJacobi : public CgBench {
public:
  BenchCgWithJacobi() : CgBench("Jacobi") {}
};

class BenchCgWithSSOR : public CgBench {
public:
  BenchCgWithSSOR() : CgBench("SSOR") {}
};

class BenchCgWithSuperLU : public CgBench {
public:
  BenchCgWithSuperLU() : CgBench("SuperLU") {}
};

class BenchCgWithPs : public CgBench {
public:
  BenchCgWithPs() : CgBench("PS") {}
};

class BenchCgWithMultigrid : public CgBench {
public:
  BenchCgWithMultigrid() : CgBench("Multigrid") {}
};

BENCHMARK_F(BenchCgWithJacobi, Run)(benchmark::State &state) { this->run(state); }

BENCHMARK_F(BenchCgWithSSOR, Run)(benchmark::State &state) { this->run(state); }

BENCHMARK_F(BenchCgWithSuperLU, Run)(benchmark::State &state) { this->run(state); }

BENCHMARK_F(BenchCgWithMultigrid, Run)(benchmark::State &state) { this->run(state); }

int main(int argc, char **argv) {
  MppTest mppTest = MppTestBuilder(argc, argv).WithPPM();
  mout.setEnabledLoggers(0);
  return mppTest.RUN_ALL_MPP_BENCHMARKS(argc, argv);
}

#include <string>
#include <utility>

#include <benchmark/benchmark.h>

#include "BenchMpp.hpp"
#include "EllipticPDESolver.hpp"
#include "EllipticProblems.hpp"
#include "IEllipticAssemble.hpp"
#include "Logging.hpp"
#include "MeshIndex.hpp"
#include "PDESolver.hpp"
#include "TestEnvironment.hpp"

namespace {

constexpr int minTime = 60;

} // anonymous namespace

class GmResBench : public MppMpiBench {
protected:
  std::string preconditioner;
public:
  GmResBench(std::string &&preconditioner) : preconditioner(std::move(preconditioner)) {
    MinTime(minTime);
  }

  inline void run(benchmark::State &state) {
    int level = 4;
    int degree = 4;
    Config::Get("level", level);
    Config::Get("degree", degree);

    const auto config = PDESolverConfig()
                            .WithDegree(degree)
                            .WithPreconditioner(preconditioner)
                            .WithLinearSolver("GMRES")
                            .WithModel("Lagrange");

    const auto problem = EllipticPDEProblemBuilder::LaplaceSquare500().BuildShared();
    for (auto _ : state) {
      BENCH_START_TIMING();
      auto solver = EllipticPDESolver(config);
      benchmark::DoNotOptimize(solver.Run(problem, MeshIndex{level, -1, 0, 0}));
      BENCH_END_TIMING(state);
    }
  }
};

class BenchGmResWithJacobi : public GmResBench {
public:
  BenchGmResWithJacobi() : GmResBench("Jacobi") {}
};

class BenchGmResWithSSOR : public GmResBench {
public:
  BenchGmResWithSSOR() : GmResBench("SSOR") {}
};

class BenchGmResWithSuperLU : public GmResBench {
public:
  BenchGmResWithSuperLU() : GmResBench("SuperLU") {}
};

class BenchGmResWithPS : public GmResBench {
public:
  BenchGmResWithPS() : GmResBench("PS") {}
};

class BenchGmResWithMultigrid : public GmResBench {
public:
  BenchGmResWithMultigrid() : GmResBench("Multigrid") {}
};

BENCHMARK_F(BenchGmResWithJacobi, Run)(benchmark::State &state) { this->run(state); }

BENCHMARK_F(BenchGmResWithSSOR, Run)(benchmark::State &state) { this->run(state); }

BENCHMARK_F(BenchGmResWithSuperLU, Run)(benchmark::State &state) { this->run(state); }

BENCHMARK_F(BenchGmResWithMultigrid, Run)(benchmark::State &state) { this->run(state); }

int main(int argc, char **argv) {
  MppTest mppTest = MppTestBuilder(argc, argv).WithPPM();
  mout.setEnabledLoggers(0);
  return mppTest.RUN_ALL_MPP_BENCHMARKS(argc, argv);
}

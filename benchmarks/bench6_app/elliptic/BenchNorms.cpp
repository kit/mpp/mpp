#include <benchmark/benchmark.h>

#include "BenchMpp.hpp"
#include "EllipticProblems.hpp"
#include "IEllipticAssemble.hpp"
#include "Logging.hpp"
#include "PDESolver.hpp"
#include "TestEnvironment.hpp"
#include "Vector.hpp"

class BenchNorm : public MppMpiBench {
protected:
public:
  BenchNorm() = default;

  void Compute(benchmark::State &state) {
    const auto problem = EllipticPDEProblemBuilder::Laplace2D().Build();
    const auto assemble = CreateEllipticAssemble(*problem, PDESolverConfig().WithModel("DG"));
    const auto disc = assemble->GetSharedDisc();
    const auto v = Vector{disc, 2};

    run(v, *assemble, state);
  }

  virtual void run(Vector vector, IEllipticAssemble &assemble, benchmark::State &state) = 0;
};

class BenchEnergy : public BenchNorm {
public:
  void run(Vector vector, IEllipticAssemble &assemble, benchmark::State &state) override {
    for (auto _ : state) {
      BENCH_START_TIMING();
      benchmark::DoNotOptimize(assemble.Energy(vector));
      BENCH_END_TIMING(state);
    }
  }
};



class BenchEnergyError : public BenchNorm {
public:
  void run(Vector vector, IEllipticAssemble &assemble, benchmark::State &state) override {
    for (auto _ : state) {
      BENCH_START_TIMING();
      benchmark::DoNotOptimize(assemble.EnergyError(vector));
      BENCH_END_TIMING(state);
    }
  }
};

class BenchL2Error : public BenchNorm {
public:
  void run(Vector vector, IEllipticAssemble &assemble, benchmark::State &state) override {
    for (auto _ : state) {
      BENCH_START_TIMING();
      benchmark::DoNotOptimize(assemble.L2Error(vector));
      BENCH_END_TIMING(state);
    }
  }
};

class BenchL2CellAvgError : public BenchNorm {
public:
  void run(Vector vector, IEllipticAssemble &assemble, benchmark::State &state) override {
    for (auto _ : state) {
      BENCH_START_TIMING();
      benchmark::DoNotOptimize(assemble.L2CellAvgError(vector));
      BENCH_END_TIMING(state);
    }
  }
};

class BenchMaxError : public BenchNorm {
public:
  void run(Vector vector, IEllipticAssemble &assemble, benchmark::State &state) override {
    for (auto _ : state) {
      BENCH_START_TIMING();
      benchmark::DoNotOptimize(assemble.MaxError(vector));
      BENCH_END_TIMING(state);
    }
  }
};

class BenchFaceError : public BenchNorm {
public:
  void run(Vector vector, IEllipticAssemble &assemble, benchmark::State &state) override {
    for (auto _ : state) {
      BENCH_START_TIMING();
      benchmark::DoNotOptimize(assemble.FaceError(vector));
      BENCH_END_TIMING(state);
    }
  }
};

class BenchFluxError : public BenchNorm {
public:
  void run(Vector vector, IEllipticAssemble &assemble, benchmark::State &state) override {
    for (auto _ : state) {
      BENCH_START_TIMING();
      benchmark::DoNotOptimize(assemble.FluxError(vector));
      BENCH_END_TIMING(state);
    }
  }
};

BENCHMARK_F(BenchEnergy, Compute)(benchmark::State &state) { this->Compute(state); }

BENCHMARK_F(BenchEnergyError, Compute)(benchmark::State &state) { this->Compute(state); }

BENCHMARK_F(BenchL2Error, Compute)(benchmark::State &state) { this->Compute(state); }

BENCHMARK_F(BenchL2CellAvgError, Compute)(benchmark::State &state) { this->Compute(state); }

BENCHMARK_F(BenchMaxError, Compute)(benchmark::State &state) { this->Compute(state); }

BENCHMARK_F(BenchFaceError, Compute)(benchmark::State &state) { this->Compute(state); }

BENCHMARK_F(BenchFluxError, Compute)(benchmark::State &state) { this->Compute(state); }

#include "LagrangeDiscretization.hpp"
#include "RTLagrangeDiscretization.hpp"
#include "DGDiscretization.hpp"

class BenchNormNamespace : public MppMpiBench {
protected:
public:
  BenchNormNamespace() = default;

  void WithLagrangeDiscretization(benchmark::State &state) {
    const auto meshes = MeshesCreator("UnitSquare").CreateShared();
    const auto disc = std::make_shared<const LagrangeDiscretization>(*meshes, 2);
    const auto v = Vector{disc, 2};
    run(v, state);
  }

  void WithDGDiscretization(benchmark::State &state) {
    const auto meshes = MeshesCreator("UnitSquare").CreateShared();
    const auto disc = std::make_shared<const DGDiscretization>(*meshes, 2);
    const auto v = Vector{disc, 2};
    run(v, state);
  }

  void WithRTLagrangeDiscretization(benchmark::State &state) {
    const auto meshes = MeshesCreator("UnitSquare").CreateShared();
    const auto disc = std::make_shared<const RTLagrangeDiscretization>(*meshes, 2, 2);
    const auto v = Vector{disc, 2};
    run(v, state);
  }

  virtual void run(Vector vector, benchmark::State &state) = 0;
};

class BenchH1 : public BenchNormNamespace {
public:
  void run(Vector vector, benchmark::State &state) override {
    for (auto _ : state) {
      BENCH_START_TIMING();
      benchmark::DoNotOptimize(norms::H1(vector));
      BENCH_END_TIMING(state);
    }
  }
};

class BenchL1 : public BenchNormNamespace {
public:
  void run(Vector vector, benchmark::State &state) override {
    for (auto _ : state) {
      BENCH_START_TIMING();
      benchmark::DoNotOptimize(norms::L1(vector));
      BENCH_END_TIMING(state);
    }
  }
};


class BenchL2 : public BenchNormNamespace {
public:
  void run(Vector vector, benchmark::State &state) override {
    for (auto _ : state) {
      BENCH_START_TIMING();
      benchmark::DoNotOptimize(norms::L2(vector));
      BENCH_END_TIMING(state);
    }
  }
};

BENCHMARK_F(BenchH1, WithLagrangeDiscretization)(benchmark::State &state) { this->WithLagrangeDiscretization(state); }

BENCHMARK_F(BenchH1, WithDGDiscretization)(benchmark::State &state) { this->WithDGDiscretization(state); }

BENCHMARK_F(BenchL1, WithLagrangeDiscretization)(benchmark::State &state) { this->WithLagrangeDiscretization(state); }

BENCHMARK_F(BenchL1, WithDGDiscretization)(benchmark::State &state) { this->WithDGDiscretization(state); }

BENCHMARK_F(BenchL2, WithLagrangeDiscretization)(benchmark::State &state) { this->WithLagrangeDiscretization(state); }

BENCHMARK_F(BenchL2, WithDGDiscretization)(benchmark::State &state) { this->WithDGDiscretization(state); }

int main(int argc, char **argv) {
  MppTest mppTest = MppTestBuilder(argc, argv).WithPPM();
  mout.setEnabledLoggers(0);
  return mppTest.RUN_ALL_MPP_BENCHMARKS(argc, argv);
}

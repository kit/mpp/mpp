from os.path import abspath, join, basename, dirname, realpath
from matplotlib.ticker import FormatStrFormatter
from scipy.optimize import curve_fit
from os import listdir
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import numpy as np
import matplotlib
import argparse

# from sympy.printing.pretty.pretty_symbology import line_width

font = {'family': 'DejaVu Sans',
        'weight': 'normal',
        'size': 10}

plotType = {
    'level': 'level of discretization',
    'estimator': 'estimation by',
    'targetCost': 'cost parameter',
    'Objective': 'MLMC objective',
    'degree': 'Degree of FE'
}

estimatorType = {
    'batched': 'MC',
    'multilevel': 'MLMC'
}

singleParameterLabel = np.array(['targetCost', 'Objective', 'degree', 'ell', 'nu', 'OptimizerType'])

errors = np.array(["rmse", "mse", "num", "alpha", "beta", "gammaCT", "gammaMem"])

matplotlib.rc('font', **font)


def plot_label(run_data, comparison='estimator'):
    if comparison == 'estimator':
        label = str(estimatorType[run_data['Config Info']['EstimatorType']] + ': ' + sample_getter(run_data))
        return label
    elif comparison == 'level':
        if run_data['Config Info']['EstimatorType'] == 'batched':
            label = str(run_data['Config Info']['Level'])
        elif run_data['Config Info']['EstimatorType'] == 'multilevel':
            label = str(run_data['Config Info']['initLevels'])
        else:
            label = ""
        return label
    elif comparison in singleParameterLabel:
        label = str(run_data['Config Info'][comparison])
        return label
    else:
        print('unknown comparison type')
        return


def sample_getter(run_data):
    if run_data['Config Info']['EstimatorType'] == 'batched':
        return run_data['Config Info']['Samples']
    if run_data['Config Info']['EstimatorType'] == 'multilevel':
        return run_data['Config Info']['initSamples']


def plot_results_iteration(mpp, axs, row_index, type='estimator', log=False):
    plot_opt_data(mpp, axs[row_index][0], type, plotType[type], opt_data='E(j)', log_plot=log)
    plot_opt_data(mpp, axs[row_index][1], type, plotType[type], opt_data='E(L2(gradient))', log_plot=log)
    plot_opt_data(mpp, axs[row_index][2], type, plotType[type], opt_data='L2(z_k)', log_plot=log)


def plot_results_time(mpp, axs, row_index, type='estimator', log=False):
    plot_opt_data(mpp, axs[row_index][0], type, plotType[type], opt_data='E(j)', log_plot=log, x_axis='time')
    plot_opt_data(mpp, axs[row_index][1], type, plotType[type], opt_data='E(L2(gradient))', log_plot=log, x_axis='time')
    plot_opt_data(mpp, axs[row_index][2], type, plotType[type], opt_data='L2(z_k)', log_plot=log, x_axis='time')


def plot_errors(mpp, axs, row_index, type='estimator', log=False):
    plot_opt_data(mpp, axs[row_index][0], type, plotType[type], opt_data='rmse', log_plot=log)
    plot_opt_data(mpp, axs[row_index][1], type, plotType[type], opt_data='alpha', log_plot=log)
    plot_opt_data(mpp, axs[row_index][2], type, plotType[type], opt_data='beta', log_plot=log)


def plot_results(mpp, name, type='estimator', dpi=50, save_plot=True, show_plot=True, start=0, max_start=20,
                 min_end=80):
    axs = setup_figure(3, 3)

    log = True

    plot_results_iteration(mpp, axs, 0, type, False)
    plot_results_time(mpp, axs, 1, type, log)
    plot_errors(mpp, axs, 2, type, False)  # Todo: acutally only plot errors and exponents in different plot
    # Todo: Add more plots

    if save_plot:
        path = abspath(join(mpp.dm.MPP_BUILD_DIR, join("data/py/", name)))
        plt.savefig(path, dpi=100)

    if show_plot:
        plt.show()

    plt.close()
    return


def check_input(mpp, label):
    return


def setup_figure(nrows=1, ncols=3):
    [fig, axs] = plt.subplots(nrows, ncols)
    fig.set_figheight(nrows * 5)
    fig.set_figwidth(6 * ncols)
    fig.set_dpi(300)
    # fig.subplots_adjust(wspace=5)
    fig.tight_layout(pad=4, w_pad=4, h_pad=4)
    return axs


def convergence_fitting(y, max_start=20, min_end=80):
    n = len(y)
    values = [0, 0, 0, 0]
    for i in range(1, max_start):
        for j in range(min_end, n):
            x = np.arange(i, j)
            fit = np.polyfit(np.log(x), np.log(y[i:j]), 1)
            if fit[0] < values[2]:
                values = [i, j, fit[0], fit[1]]
    return values


def plot_opt_data(mpp, ax, type, plot_title, opt_data='L2(grad(J))', log_plot=False, log_fit=False, start=0,
                  max_start=20, min_end=80, x_axis='iteration'):
    best_values = [0, 0, 0, 0]
    for index, run_data in enumerate(mpp.data):
        if (opt_data in errors) and (run_data['Config Info']['EstimatorType'] != 'multilevel'):
            continue
        max_gradient_steps = len(run_data['Optimization Info'][opt_data])
        timePerStep = run_data['Optimization Info']['timePerStep'][:]
        timeStamps = np.zeros([max_gradient_steps, 1])
        timeStamps[0] = timePerStep[0]
        for k in range(1, max_gradient_steps):
            timeStamps[k] = timePerStep[k] + timeStamps[k - 1]
        iteration = np.arange(start, max_gradient_steps)
        if log_fit:
            values = convergence_fitting(run_data['Optimization Info'][opt_data][index], max_start, min_end)
            if values[2] < best_values[2]:
                best_values = values
            ax.plot(iteration, run_data['Optimization Info'][opt_data],
                    label=str(mpp.data['Config Info'][type][index]) + ', ' + str(round(values[2], 3)))
        else:
            if x_axis == 'iteration':
                ax.plot(iteration, run_data['Optimization Info'][opt_data],
                        label=str(plot_label(run_data, type)),
                        linewidth=1)
                ax.set_xlabel(r'$k$')
            elif x_axis == 'time':
                ax.plot(timeStamps, run_data['Optimization Info'][opt_data],
                        label=str(plot_label(run_data, type)),
                        linewidth=1)
                ax.set_xlabel(r'time')
            else:
                print('unknown x Axis label')
                return

            ##Todo: Labels need more clear information. rn only available for pre-defined cases.

    if log_fit:
        ax.plot(iteration[best_values[0]:best_values[1]],
                iteration[best_values[0]:best_values[1]] ** best_values[2] * np.exp(values[3]),
                label=str(round(values[2], 3)) + 'x')

    if log_plot:  ##Todo: check
        if opt_data == 'E(j)':
            ax.set_ylabel(r'$\log(j(z_h^k))$')
        elif opt_data == 'L2(z_k)':
            ax.set_ylabel(r'$\log(\Vert z_h^k\Vert_{L^2(D)})$')
        elif opt_data == 'E(L2(gradient))':
            ax.set_ylabel(r'$\log(\Vert \nabla j(z_h^k)\Vert_{L^2(D)})$')
        else:
            ax.set_ylabel(str(opt_data))
        ax.legend(title=str(plot_title), loc='lower left', fontsize='7')
    else:
        if opt_data == 'E(j)':
            ax.set_ylabel(r'$j(z_h^k)$')
        elif opt_data == 'L2(z_k)':
            ax.set_ylabel(r'$\Vert z_h^k\Vert_{L^2(D)}$')
        elif opt_data == 'E(L2(gradient))':
            ax.set_ylabel(r'$\Vert \nabla j(z_h^k)\Vert_{L^2(D)}$')
        else:
            ax.set_ylabel(str(opt_data))
        ax.legend(title=str(plot_title), loc='upper right', fontsize='7')

    if log_plot:
        ax.set_xscale('log')
        ax.set_yscale('log')

    ax.grid(which='major')
    return


def main(build_dir_substring='build', label='Samples', typeStr='estimator'):
    import mppy as mppy

    mpp = mppy.Mpp()

    current_file = abspath(__file__)
    desired_parent_substring = build_dir_substring
    current_directory = dirname(current_file)
    while current_directory != '/':
        if desired_parent_substring in basename(current_directory):
            mpp.dm.MPP_BUILD_DIR = current_directory
            print(f"The directory '{desired_parent_substring}' is in the parent hierarchy.")
            break
        current_directory = dirname(current_directory)
    else:
        mpp.dm.MPP_BUILD_DIR = abspath(join(current_file, '../../build/'))
        print(f"The directory '{desired_parent_substring}' is not in the parent hierarchy.")

    mpp.parse_json(json_file='all')
    plot_results(mpp, label, type=typeStr)


# Todo: Plots for StepSize Policies, Averaging, ADAM vs. SGD, MC vs. SGD
#   what should config define? which experiment to plot? or which qoi to plot (j, u, diff j, diff u)
#   change titles


if __name__ == '__main__':
    parser = argparse.ArgumentParser('Python interface to UQ results of M++')

    parser.add_argument('--build_dir', type=str, default='build', help='name of build directory')
    parser.add_argument('--update_plot_for_jobs', nargs='+', type=str, default=[],
                        help='use job names or \'all\' to update plots of job')
    parser.add_argument('--label', type=str, default='', help='label for the plot')
    args = parser.parse_args()

    main(args.build_dir, typeStr='level')
    print(0)

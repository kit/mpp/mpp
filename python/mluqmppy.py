from os.path import abspath, join, basename, dirname, realpath
from matplotlib.ticker import FormatStrFormatter
from scipy.optimize import curve_fit
from os import listdir
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import numpy as np
import matplotlib
import argparse

font = {'family': 'DejaVu Sans',
        'weight': 'normal',
        'size': 15}

matplotlib.rc('font', **font)


def log2tex(log_str):
    if isinstance(log_str, str): log_str = log_str.replace('Elliptic', '')
    LOG_TEX = {
        'false': 'Off',
        'true': 'On',
        'smoothing': r'\nu',
        'theta': r'\theta',
        'eta': r'\eta',
        'sigma': r'\sigma',
        'lambda': r'\lambda',
        'Epsilon': r'\epsilon',
        'Samples': r'M',
        'stochLevel': r'l_{sg}',
        'Parsed': r'x',
        'PointBlockGaussSeidel': r'PBGS',
        'space_time': r'MG_{st}',
        'time_space': r'MG_{ts}',
        'Processes': r'|\mathcal{P}|',
        'degree': r'\mathbf{p}',
        'logfile': r'',
        'Preconditioner': r'',
        'rkorder': r'',
        '-102.0': r'CN',
        '-2.0': r'IMPR',
        '-202.0': r'DIRK',
        'Model': r'',
        'ParallelEstimator': r'MS-FEM',
        'LinearSolver': r'',
    }
    if not isinstance(log_str, list):
        if str(log_str) in LOG_TEX.keys():
            return LOG_TEX[str(log_str)]
    return log_str


def label_string(run_data, label, add_str=''):
    if label == r'' and add_str == '':
        return None
    elif label == r'' and add_str != '':
        return r'${}$'.format(add_str)
    return_str = r'' if add_str == '' else r'${},\quad$'.format(add_str)

    if log2tex(label) == '':
        return return_str + r'${}$'.format(log2tex(run_data['Config Info'][label]))
    else:
        return return_str + r'${}={}$'.format(log2tex(label), log2tex(run_data['Config Info'][label]))


def assumption_fit(levels, level_data):
    l2_fit = np.polyfit(levels, np.log2(level_data), 1)
    exponent, intercept = l2_fit[0], l2_fit[1]
    approx = [np.power(2, exponent * level + intercept) for level in levels]
    return approx, exponent, intercept


def mean_rv_vs_level(mpp, ax, label):
    ax2 = ax.twinx()
    for index, run_data in enumerate(mpp.data):
        levels = run_data['Index Info']['Used Levels']
        objective = run_data['MLEstimatorConfig Info']['Objective']
        data_str = run_data['MLEstimatorConfig Info']['DataString']

        mean_qoi = run_data['Multilevel Info']['E(L2(forward))']
        mean_rdf_inner = run_data['Multilevel Info']['E(L2(forward))']
        mean_rdf_outer = run_data['Multilevel Info']['L2(E(forward))']
        mean_del_qoi = run_data['Multilevel Info']['E(\u2206L2(forward))']
        mean_del_rdf_inner = run_data['Multilevel Info']['E(L2(\u2206forward))']
        mean_del_rdf_outer = run_data['Multilevel Info']['L2(E(\u2206forward))']

        line = ax.plot(levels, mean_qoi, ls='dashed', linewidth=2, marker='o', markersize=10)
        color = line[0].get_color()

        approx_qoi, exponent_qoi, intercept_qoi = assumption_fit(levels[1:], np.abs(mean_del_qoi[1:]))
        qoi_label = r'\widehat{\alpha}_{Q}' + '={:03.2f}'.format(-exponent_qoi)
        ax.plot(levels[1:], mean_del_qoi[1:],
                label=label_string(run_data, label, qoi_label),
                marker='*', linestyle='None', markersize=10, color=color)
        ax.plot(levels[1:], approx_qoi, color=color, ls='dotted', linewidth=2, marker=None)

        if run_data['Build Info']['AGGREGATE_FOR_SOLUTION'] == 'ON':
            ax.plot(levels, mean_rdf_outer, ls='solid', linewidth=2, marker='*', markersize=8, color=color)
            ax.plot(levels, mean_rdf_inner, ls='None', linewidth=2, marker='.', markersize=8, alpha=0.2, color=color)
            approx_rdf, exponent_rdf, intercept_rdf = assumption_fit(levels[1:], mean_del_rdf_outer[1:])
            ax.plot(levels[1:], approx_rdf, color=color, ls='dashdot', linewidth=2, marker=None)
            alpha_rdf = r'\widehat{\alpha}_{\mathbf{u}}' + '={:03.2f}'.format(-exponent_rdf)

            ax.plot(levels[1:], np.abs(mean_del_rdf_outer[1:]),
                    label=label_string(run_data, label, alpha_rdf),
                    marker='o', linestyle='None', markersize=8, color=color)
            ax.plot(levels[1:], np.abs(mean_del_rdf_inner[1:]),
                    marker='.', linestyle='None', markersize=8, alpha=0.2, color=color)

    ax.set_xlabel(r'Level $\ell$')
    ax.set_title('Verification of assumption on numerical error decay       ')
    ax.set_xticks(mpp.data[-1]['Index Info']['Used Levels'])
    ax.set_ylabel(r'$'
                  r'\cdot\!\!\!-\!\!\!\cdot\!\!\!-\!\!\!\cdot'
                  r'\|E [\mathbf{v}_{\ell}]\|'
                  r'\quad\quad\quad\quad\quad\quad'
                  r'-\!\!\!\!-\!\!\!\!-'
                  r'\|E [\mathbf{u}_{\ell}]\|'
                  r'$')
    ax2.set_ylabel(r'$\cdots\cdots |E [\mathrm{Y}_\ell]|'
                   r'\quad\quad\quad\quad\quad \quad '
                   r'-\!\!-\!\!- E [\mathrm{Q}_{\ell}]'
                   r'$')
    ax2.set_yticklabels([])
    ax2.tick_params(axis='y', length=0)
    ax.set_yscale('log')
    ax.legend(loc='lower left', handletextpad=0.4, labelspacing=0.025, borderpad=0.05)
    ax.grid(which='major')


def svar_vs_level(mpp, ax, label):
    ax2 = ax.twinx()
    for index, run_data in enumerate(mpp.data):
        levels = run_data['Index Info']['Used Levels']
        objective = run_data['MLEstimatorConfig Info']['Objective']
        data_str = run_data['MLEstimatorConfig Info']['DataString']

        svar_qoi = run_data['Multilevel Info']['V(L2(forward))']
        svar_del_qoi = run_data['Multilevel Info']['V(\u2206L2(forward))']
        boch_rdf = run_data['Multilevel Info']['BL2(forward)'.format(objective)]
        svar_rdf_inner = run_data['Multilevel Info']['V(L2(forward))'.format(data_str)]
        svar_rdf_outer = run_data['Multilevel Info']['L2(V(forward))'.format(data_str)]
        boch_del_rdf = run_data['Multilevel Info']['BL2(\u2206forward)'.format(objective)]
        svar_del_rdf_inner = run_data['Multilevel Info']['V(L2(\u2206forward))'.format(objective)]
        svar_del_rdf_outer = run_data['Multilevel Info']['L2(V(\u2206forward))'.format(objective)]

        line = ax.plot(levels, svar_qoi, ls='dashed', linewidth=2, marker='o', markersize=10)
        color = line[0].get_color()

        approx_qoi, exponent_qoi, intercept_qoi = assumption_fit(levels[1:], np.abs(svar_del_qoi[1:]))
        qoi_label = r'\widehat{\beta}_{Q}' + '={:03.2f}'.format(-exponent_qoi)
        ax.plot(levels[1:], svar_del_qoi[1:],
                label=label_string(run_data, label, qoi_label),
                marker='*', linestyle='None', markersize=10, color=color)
        ax.plot(levels[1:], approx_qoi, color=color, ls='dotted', linewidth=2, marker=None)

        if run_data['Build Info']['AGGREGATE_FOR_SOLUTION'] == 'ON':
            ax.plot(levels, boch_rdf, ls='solid', linewidth=2, marker='*', markersize=8, color=color)
            ax.plot(levels, svar_rdf_outer, ls='None', linewidth=2, marker='.', markersize=8, alpha=0.2, color=color)
            ax.plot(levels, svar_rdf_inner, ls='None', linewidth=2, marker='.', markersize=8, alpha=0.2, color=color)
            approx_rdf, exponent_rdf, intercept_rdf = assumption_fit(levels[1:], boch_del_rdf[1:])
            ax.plot(levels[1:], approx_rdf, color=color, ls='dashdot', linewidth=2, marker=None)
            beta_rdf = r'\widehat{\beta}_{\mathbf{v}}' + '={:03.2f}'.format(-exponent_rdf)

            ax.plot(levels[1:], boch_del_rdf[1:], label=label_string(run_data, label, beta_rdf),
                    marker='o', linestyle='None', markersize=8, color=color)
            ax.plot(levels[1:], svar_del_rdf_outer[1:], marker='.',
                    linestyle='None', markersize=8, alpha=0.2, color=color)
            ax.plot(levels[1:], svar_del_rdf_inner[1:], marker='.',
                    linestyle='None', markersize=8, alpha=0.2, color=color)

    ax.set_xlabel(r'Level $\ell$')
    ax.set_title('Verification of assumptions on sampling error decay       ')
    ax.set_xticks(mpp.data[-1]['Index Info']['Used Levels'])
    ax.set_ylabel(r'$'
                  r'\cdot\!\!\!-\!\!\!\cdot\!\!\!-\!\!\!\cdot'
                  r'\|\mathbf{v}_{\ell}-{E}[\mathbf{v}_{\ell}]\|^2'
                  r'\quad\quad\quad'
                  r'-\!\!\!\!-\!\!\!\!-'
                  r'\|\mathbf{u}_{\ell}-{E}[\mathbf{u}_{\ell}]\|^2$'
                  r'')
    ax2.set_ylabel(r'$\cdots\cdots V [\mathrm{Y}_\ell]'
                   r'\quad\quad\quad\quad\quad \quad '
                   r'-\!\!-\!\!- V [\mathrm{Q}_{\ell}]'
                   r'$')
    ax2.set_yticklabels([])
    ax2.tick_params(axis='y', length=0)
    ax.set_yscale('log')
    ax.legend(loc='lower left', handletextpad=0.4, labelspacing=0.025, borderpad=0.05)
    ax.grid(which='major')


def plot_budget_line(mpp, budget_str):
    plot_budget = True
    budget = mpp.data[0]['MLEstimatorConfig Info'][budget_str]
    for index, run_data in enumerate(mpp.data[1:]):
        if budget != run_data['MLEstimatorConfig Info'][budget_str] or budget > 10e12 or budget == 0.0:
            plot_budget = False
            break
    return plot_budget


def cost_vs_levels(mpp, ax, label):
    ax2 = ax.twinx()

    if plot_budget_line(mpp, 'MemoBudget'):
        ax.axhline(y=mpp.data[0]['MLEstimatorConfig Info']['MemoBudget'], color='r', linewidth=2, label='Memory Budget')

    for index, run_data in enumerate(mpp.data):
        levels = run_data['Index Info']['Used Levels']
        mean_time = run_data['Costs Info']['E(TotalTimeCost)']
        mean_memo = run_data['Costs Info']['E(TotalMemoCost)']

        approx_ct, exponent_ct, intercept_ct = assumption_fit(levels, mean_time)
        gamma_mem = r'\widehat{\gamma}_{\mathrm{Mem}}' + r'={:03.2f}'.format(run_data['Exponents Info']['gammaMem'])
        gamma_ct = r'\widehat{\gamma}_{\mathrm{CT}}' + r'={:03.2f}'.format(exponent_ct)

        data_points = ax2.plot(levels, mean_time, alpha=1.0, marker='*', linestyle='None', markersize=10)
        color = data_points[0].get_color()

        ax2.plot(levels, approx_ct, color=color, alpha=1.0, ls='-', linewidth=2, marker=None,
                 label=label_string(run_data, label, gamma_ct))

        x_loc, width = x_loc_bar_plot(run_data, len(mpp.data), index)
        ax.bar(x_loc, mean_memo, width=width, align='edge', facecolor=color, alpha=0.7,
               label=label_string(run_data, label, gamma_mem))

        ax.axhline(y=run_data['SampleData Info']['TotalMemoCostSum'], linewidth=2, color=color)

        if plot_budget_line(mpp, 'MemoBudget'):
            ax.axhline(y=run_data['MLEstimatorConfig Info']['MemoBudget'] -
                         run_data['SampleData Info']['MemoryOffset'],
                       color=color, linewidth=2, linestyle='dashed')

    ax2.set_title('Verification of assumptions on cost growth')
    ax.set_xlabel(r'Level $\ell$')
    ax.set_xticks(mpp.data[-1]['Index Info']['Used Levels'])
    ax.set_yscale('log')
    ax.set_ylabel(r'$\widehat{\mathrm{C}}_{\ell, 0}^{\mathrm{Mem}}$  and '
                  r' $\widehat{\mathrm{C}}_{\max}^{\mathrm{Mem}}$  in  [MByte]')
    ax.legend(loc='upper left', handletextpad=0.4, labelspacing=0.025, borderpad=0.05)
    ax.grid(which='major')

    ax2.set_ylabel(r'$\widehat{\mathrm{C}}_{\ell}^{\mathrm{CT}}$  in  [s]')
    ax2.set_xlabel(r'Level $\ell$')
    ax2.set_xticks(mpp.data[-1]['Index Info']['Used Levels'])
    ax2.set_yscale('log')
    ax2.legend(loc='center left', handletextpad=0.4, labelspacing=0.025, borderpad=0.05)
    ax2.grid(which='major')


def skewness_qoi_plot(mpp, ax, label):
    for index, run_data in enumerate(mpp.data):
        line = ax.plot(run_data['Multilevel Info']['Used Levels'],
                       run_data['Multilevel Info']['S(Qf)'],
                       ls='-', label=label_string(run_data, label),
                       linewidth=2, marker='*', markersize=10)
        ax.plot(run_data['Multilevel Info']['Used Levels'][1:],
                run_data['Multilevel Info']['S(Qf-Qc)'][1:],
                ls=':', linewidth=2, marker='*', markersize=10,
                color=line[0].get_color())

    ax.set_xlabel(r'Level $\ell$')
    ax.set_ylabel(r'$- \mathbb{S}[\mathrm{Q}_{\ell}] \quad \cdots \mathbb{S}[\mathrm{Y}_{\ell}]$')
    if label != '':
        ax.legend(loc='lower left', handletextpad=0.4, labelspacing=0.025, borderpad=0.05)
    ax.set_xticks(mpp.data[-1]['Multilevel Info']['Used Levels'])
    ax.grid(which='major')


def kurtosis_vs_levels(mpp, ax, label):
    for index, run_data in enumerate(mpp.data):
        line = ax.plot(run_data['Multilevel Info']['Used Levels'],
                       run_data['Multilevel Info']['K(Qf)'],
                       label=label_string(run_data, label),
                       ls='-', linewidth=2, marker='*', markersize=10)
        ax.plot(run_data['Multilevel Info']['Used Levels'][1:],
                run_data['Multilevel Info']['K(Qf-Qc)'][1:],
                ls=':', linewidth=2, marker='*', markersize=10,
                color=line[0].get_color())

    ax.set_xlabel(r'Level $\ell$')
    ax.set_ylabel(r'$- \mathbb{K}[\mathrm{Q}_{\ell}] \quad '
                  r'\cdots \mathbb{K}[\mathrm{Y}_{\ell}]$')
    ax.set_yscale('log')
    if label != '':
        ax.legend(loc='lower left', handletextpad=0.4, labelspacing=0.025, borderpad=0.05)
    ax.set_xticks(mpp.data[-1]['Multilevel Info']['Used Levels'])
    ax.grid(which='major')


def x_loc_bar_plot(run_data, length, index):
    width = 1.0 / length - 0.01
    levels = run_data['Index Info']['Used Levels']
    x_loc = levels + np.ones(len(levels)) * width * (index - length / 2)
    return x_loc, width


def sample_plot(mpp, ax, label):
    for index, run_data in enumerate(mpp.data):
        samples = run_data['Index Info']['Used Samples']
        x_loc, width = x_loc_bar_plot(run_data, len(mpp.data), index)
        bar_plot = ax.bar(x_loc, samples, width=width, alpha=0.7, align='edge', label=label_string(run_data, label))

    ax.set_yscale('log')
    ax.set_xticks(mpp.data[-1]['Index Info']['Used Levels'])
    ax.grid(which='major')
    if label != '':
        ax.legend(loc='upper right', handletextpad=0.4, labelspacing=0.025, borderpad=0.05)
    ax.set_ylabel(r'Samples  $M_{\ell}$')
    ax.set_xlabel(r'Level $\ell$')


def total_cost_plot(mpp, ax, label):
    if plot_budget_line(mpp, 'TimeBudget'):
        ax.axhline(y=mpp.data[-1]['MLEstimatorConfig Info']['TimeBudget'], color='r', linewidth=2, label='Time Budget')

    for index, run_data in enumerate(mpp.data):
        x_loc, width = x_loc_bar_plot(run_data, len(mpp.data), index)
        total_time = run_data['Costs Info']['TotalTimeCost']
        bar_plot = ax.bar(x_loc, total_time, alpha=0.70, width=width, align='edge', label=label_string(run_data, label))
        color = bar_plot[0].get_facecolor()
        ax.axhline(y=run_data['SampleData Info']['TotalTimeCostSum'], linewidth=2, color=color)

    if label != '' and plot_budget_line(mpp, 'TimeBudget') is True:
        ax.legend(loc='upper left', handletextpad=0.4, labelspacing=0.025, borderpad=0.05)
    ax.set_xticks(mpp.data[-1]['Index Info']['Used Levels'])
    ax.grid(which='major')
    ax.set_ylabel(r'Computing time  $\mathrm{T}_{\ell}$  in  [s]')
    ax.set_xlabel(r'Level $\ell$')


def index_set_plot(mpp, ax, label):
    default_colors = list(mcolors.TABLEAU_COLORS.keys())
    for index, run_data in enumerate(mpp.data):
        comm_splits = run_data['Continuation Info']['CommSplits']
        levels = run_data['Index Info']['Used Levels']

        for budget_index, _ in enumerate(comm_splits):
            for cs_index, cs in enumerate(comm_splits[budget_index]):
                ax.plot(levels[cs_index] + 0.1 * index, cs, marker='o', markersize=10, color=default_colors[index])

    if label != '':
        ax.legend(loc='lower left', handletextpad=0.4, labelspacing=0.025, borderpad=0.05)
    ax.set_xticks(mpp.data[-1]['Index Info']['Used Levels'])
    ax.set_yticks(range(max(max(mpp.data[-1]['Continuation Info']['CommSplits'])) + 1))
    ax.grid(which='major')
    ax.set_ylabel(r'Communication split $s$')
    ax.set_xlabel(r'Level $\ell$')


def disc_vs_input(mpp, ax, label):
    for index, run_data in enumerate(mpp.data):
        epsilons = run_data['ContinuationData Info']['Epsilons']
        theta = run_data['MLEstimatorConfig Info']['Theta']
        rmse = run_data['ContinuationData Info']['RMSE(\u2206L2(forward))']
        mse = run_data['ContinuationData Info']['MSE(\u2206L2(forward))']
        sam = run_data['ContinuationData Info']['SAM(\u2206L2(forward))']
        num = run_data['ContinuationData Info']['NUM(\u2206L2(forward))']

        line = ax.plot(sam, num, label=label_string(run_data, label), ls='-', marker='*', markersize=10)

        for i, epsilon in enumerate(epsilons):
            eps_input = np.multiply(float(theta), np.power(epsilon, 2))
            eps_disc = np.multiply(1 - float(theta), np.power(epsilon, 2))

            ax.hlines(y=eps_disc, xmin=0.0, xmax=eps_input, ls=':', color=line[0].get_color())
            ax.vlines(x=eps_input, ymin=0.0, ymax=eps_disc, ls=':', color=line[0].get_color())

    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.set_xlabel(r'$\widehat{\mathrm{err}}^{\mathrm{sam}}$')
    ax.set_ylabel(r'$\widehat{\mathrm{err}}^{\mathrm{num}}$')

    if label != '':
        ax.legend(loc='lower right', handletextpad=0.4, labelspacing=0.025, borderpad=0.05)
    ax.grid(which='major')


def rel_budget_vs_total_error_plot(mpp, ax, label):
    for index, run_data in enumerate(mpp.data):
        rel_budget = run_data['ContinuationData Info']['RelTimeCosts']
        rmse = run_data['ContinuationData Info']['RMSE(\u2206L2(forward))']
        data_points = ax.plot(rel_budget, rmse, marker='*', linestyle='None', markersize=10)
        color = color=data_points[0].get_color()
        l2_fit = np.polyfit(np.log2(rel_budget)[1:], np.log2(rmse)[1:], 1)
        slope, intercept = l2_fit[0], l2_fit[1]
        approx = [np.power(2, intercept) * np.power(point, slope) for point in rel_budget]
        budget_str = r'\widehat{\delta} = ' + r'{{{:03.2f}}}'.format(-slope)
        ax.plot(rel_budget, approx, label=label_string(run_data, label, budget_str), color=color)

    ax.set_xscale('log')
    ax.set_yscale('log')

    ax.set_xlabel(r'$(\mathrm{T}_{\mathrm{B}} - \mathrm{T}_{i}) / \mathrm{T}_{\mathrm{B}}$')
    ax.set_ylabel(r'$\widehat{\mathrm{err}}^{\mathrm{RMSE}}$')
    ax.legend(loc='upper right', handletextpad=0.4, labelspacing=0.025, borderpad=0.05)
    ax.grid(which='both')


def rel_budget_vs_qoi_and_mse_plot(mpp, ax, label):
    return 0

    for index, run_data in enumerate(mpp.data):
        rel_budget = run_data['ContinuationData Info']['RelTimeCosts']
        ax.errorbar(rel_budget, run_data['Continuation Info']['Values'],
                    yerr=run_data['Continuation Info']['rmseYErrors'],
                    label=label_string(run_data, label), markersize=10,
                    marker='*', capsize=6, barsabove=True)

    ax.set_xlabel(r'$(\mathrm{T}_{\mathrm{B}, 0} - \mathrm{T}_{B, i}) / '
                  r'\mathrm{T}_{\mathrm{B}, 0}$')
    ax.set_ylabel(r'$\widehat{\mathrm{Q}} \,\,$ with $\,\,\widehat{\mathrm{err}}^{\mathrm{RMSE}}$')
    if label != '':
        ax.legend(loc='lower left', handletextpad=0.4, labelspacing=0.025, borderpad=0.05)
    ax.grid(which='major')
    ax.yaxis.set_major_formatter(FormatStrFormatter('%.3f'))


def rel_budget_vs_memory(mpp, ax, label):
    for index, run_data in enumerate(mpp.data):
        rel_budget = run_data['ContinuationData Info']['RelTimeCosts']
        data_points = ax.plot(rel_budget, run_data['Continuation Info']['Memory'],
                              marker='*', linestyle='None', markersize=10)

        l2_fit = np.polyfit(np.log2(rel_budget)[1:],
                            np.log2(run_data['Continuation Info']['Memory'])[1:], 1)
        slope, intercept = l2_fit[0], l2_fit[1]
        approx = [np.power(2, intercept) * np.power(point, slope) for point in rel_budget]

        ax.plot(rel_budget, approx, label=label_string(run_data, label),
                color=data_points[0].get_color())

    ax.set_xlabel(r'$(\mathrm{T}_{\mathrm{B}, 0} - \mathrm{T}_{B, i}) / '
                  r'\mathrm{T}_{\mathrm{B}, 0}$')
    ax.set_ylabel(r'$\mathrm{Mem}$ in [Mbyte]')
    if label != '':
        ax.legend(loc='upper left', handletextpad=0.4, labelspacing=0.025, borderpad=0.05)
    ax.grid(which='both')


def setup_single_figure():
    fig, axs = plt.subplots(1, 1)
    fig.set_figheight(7)
    fig.set_figwidth(9)
    fig.subplots_adjust(wspace=0.5)
    fig.tight_layout(pad=4, w_pad=10, h_pad=5)
    return fig, axs


def setup_figure(nrows, ncols=3):
    [fig, axs] = plt.subplots(nrows, ncols)
    fig.set_figheight(nrows * 5.5)
    fig.set_figwidth(ncols * 7)
    fig.subplots_adjust(wspace=0.5)
    fig.tight_layout(pad=4, w_pad=4, h_pad=2)
    axs = [axs] if nrows == 1 else axs
    axs = [axs] if ncols == 1 else axs
    return axs


def exponents_row(mpp, axs, row_index, label):
    mean_rv_vs_level(mpp, axs[row_index][0], label)
    svar_vs_level(mpp, axs[row_index][1], label)
    cost_vs_levels(mpp, axs[row_index][2], label)


def higher_moments_and_mem_row(mpp, axs, row_index, label):
    skewness_qoi_plot(mpp, axs[row_index][0], label)
    kurtosis_vs_levels(mpp, axs[row_index][1], label)
    rel_budget_vs_memory(mpp, axs[row_index][2], label)


def bmlmc_row(mpp, axs, row_index, label):
    sample_plot(mpp, axs[row_index][0], label)
    total_cost_plot(mpp, axs[row_index][1], label)
    rel_budget_vs_total_error_plot(mpp, axs[row_index][2], label)


def bar_plot_row(mpp, axs, row_index, label):
    index_set_plot(mpp, axs[row_index][0], label)
    sample_plot(mpp, axs[row_index][1], label)
    total_cost_plot(mpp, axs[row_index][2], label)


def rel_budget_row(mpp, axs, row_index, label):
    disc_vs_input(mpp, axs[row_index][0], label)
    rel_budget_vs_total_error_plot(mpp, axs[row_index][1], label)
    rel_budget_vs_qoi_and_mse_plot(mpp, axs[row_index][2], label)


def plot_results_2x3(mpp, label='', dpi=100):
    axs = setup_figure(2)
    exponents_row(mpp, axs, 0, label)
    bmlmc_row(mpp, axs, 1, label)
    plt.savefig(abspath(join(mpp.dm.MPP_PY_DATA_DIR, '2x3')), dpi=dpi)


def plot_results_3x3(mpp, label='', directory='', dpi=100):
    axs = setup_figure(3)
    exponents_row(mpp, axs, 0, '')
    bar_plot_row(mpp, axs, 1, label)
    rel_budget_row(mpp, axs, 2, label)
    name = '3x3' if label == '' else label
    directory = mpp.dm.MPP_PY_DATA_DIR if directory == '' else directory
    plt.savefig(abspath(join(directory, name)), dpi=dpi)


def plot_results_4x3(mpp, label='', dpi=100):
    axs = setup_figure(4)
    exponents_row(mpp, axs, 0, label)
    higher_moments_and_mem_row(mpp, axs, 1, label)
    bar_plot_row(mpp, axs, 2, label)
    rel_budget_row(mpp, axs, 3, label)
    plt.savefig(abspath(join(mpp.dm.MPP_PY_DATA_DIR, '4x3')), dpi=dpi)


def main(build_dir_substring='build', label=''):
    import mppy as mppy

    mpp = mppy.Mpp()

    current_file = abspath(__file__)
    desired_parent_substring = build_dir_substring
    current_directory = dirname(current_file)
    while current_directory != '/':
        if desired_parent_substring in basename(current_directory):
            mpp.dm.MPP_BUILD_DIR = current_directory
            print(f"The directory '{desired_parent_substring}' is in the parent hierarchy.")
            break
        current_directory = dirname(current_directory)
    else:
        mpp.dm.MPP_BUILD_DIR = abspath(join(current_file, '../../build/'))
        print(f"The directory '{desired_parent_substring}' is not in the parent hierarchy.")

    mpp.parse_json(json_file='all')
    plot_results_3x3(mpp, label)


def job2label(job):
    if job.find('lambda') != -1:
        return 'lambda'
    if job.find('scaling') != -1:
        return 'Processes'
    if job.find('eta') != -1 and job.find('theta') == -1:
        return 'eta'
    if job.find('solver') != -1:
        return 'LinearSolver'
    if job.find('disc') != -1:
        return 'Model'
    if job.find('qoi') != -1:
        return 'Quantity'
    if job.find('theta') != -1:
        return 'theta'
    if job.find('plot') != -1:
        return 'VtuPlot'
    if job.find('sigma') != -1:
        return 'sigma'
    if job.find('parallelization') != -1:
        return 'ParallelEstimator'
    if job.find('preconditioner') != -1:
        return 'Preconditioner'
    if job.find('smoothing-steps') != -1:
        return 'presmoothing'
    if job.find('smoothing') != -1 and job.find('smoothing-steps') == -1:
        return 'smoothing'
    if job.find('rkorder') != -1:
        return 'rkorder'
    if job.find('degree') != -1:
        return 'degree'
    if job.find('delayed-update') != -1:
        return 'DelayedUpdate'
    if job.find('smoother') != -1:
        return 'Smoother'
    if job.find('cfl') != -1:
        return 'CFL'
    return ''


def update_mpp_data(jobs=None, dpi=100, plot_config='00000'):
    import mppy as mppy

    data_dir = abspath(join(dirname(realpath(__file__)), '../mpp-data/mpp/'))
    if not isinstance(jobs, list):
        jobs = sorted(listdir(data_dir)) if jobs is None else [jobs]

    for job in jobs:
        mpp = mppy.Mpp()
        mpp.parse_json(json_file='all', directory=abspath(join(data_dir, join(job, 'json'))))

        try:
            if not mpp.data:
                raise ValueError('Mpp data is empty, please parse log file')
        except ValueError:
            print('ValueError for job ', job)
            return

        if plot_config == '2x3':
            plot_results_2x3(mpp, label=job2label(job), dpi=dpi)
        if plot_config == '3x3':
            plot_results_3x3(mpp, label='logfile', directory=abspath(join(data_dir, join(job, 'py'))), dpi=dpi)

        print('Success for job: {}'.format(job))


if __name__ == '__main__':
    parser = argparse.ArgumentParser('Python interface to UQ results of M++')

    parser.add_argument('--build_dir', type=str, default='build', help='name of build directory')
    parser.add_argument('--update_plot_for_jobs', nargs='+', type=str, default=[],
                        help='use job names or \'all\' to update plots of job')
    parser.add_argument('--label', type=str, default='', help='label for the plot')
    args = parser.parse_args()

    args.update_plot_for_jobs = [
        # 'mlmc-acoustic-time-stepping-theta-on-horeka',
        # 'bmlmc-spde-mixed-transport-node-scaling-on-horeka',
        # 'bmlmc-spde-mixed-transport-nu-on-horeka'
    ]

    if not args.update_plot_for_jobs:
        main(args.build_dir, label=args.label)
    else:
        update_mpp_data(jobs=args.update_plot_for_jobs, plot_config='3x3', dpi=250)
